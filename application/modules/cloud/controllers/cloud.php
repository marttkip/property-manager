<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// require_once "./application/modules/auth/controllers/auth.php";
class Cloud  extends MX_Controller
{	
	function __construct()
	{
		parent:: __construct();
		$this->load->model('cloud_model');
	}
	
	public function save_cloud_data()
	{
		$json = file_get_contents('php://input');
	   	$response = $this->cloud_model->save_visit_data($json);

		echo json_encode($response);
	}
	public function sync_up_petty_cash()
	{
		$json = file_get_contents('php://input');
	   	$response = $this->cloud_model->save_petty_cash_data($json);

		echo json_encode($response);

	}
	
	public function cron_sync_up()
	{
		//get all unsynced visits
		$unsynced_visits = $this->cloud_model->get_unsynced_visits();
		
		$patients = array();
		
		if($unsynced_visits->num_rows() > 0)
		{
			//delete all unsynced visits
			$this->db->where('sync_status', 0);
			if($this->db->delete('sync'))
			{
				foreach($unsynced_visits->result() as $res)
				{
					$sync_data = $res->sync_data;
					$sync_table_name = $res->sync_table_name;
					$branch_code = $res->branch_code;
					array_push($patients[$sync_table_name], $value);
					
					/*$response = $this->sync_model->syn_up_on_closing_visit($visit_id);
		
					if($response)
					{
					}*/
				}
				$patients['branch_code'] = $branch_code;
			}
		}
		
		//sync data
		$response = $this->cloud_model->send_unsynced_visits($patients);
		var_dump($response);
		if($response)
		{
		}
		
		else
		{
		}
	}

	public function update_lease_invoice()
	{
		$this->db->select('leases.lease_number,leases.rental_unit_id,leases.tenant_id,lease_invoice.lease_invoice_id');
		$this->db->where('account_id IS NULL AND leases.lease_id = lease_invoice.lease_id');
		$query = $this->db->get('lease_invoice,leases');

		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$lease_invoice_id = $value->lease_invoice_id;
				$lease_number = $value->lease_number;
				$rental_unit_id = $value->rental_unit_id;
				$tenant_id = $value->tenant_id;


				// update the lease invoice table

				$lease_invoice_update['account_id'] = $lease_number;
				$lease_invoice_update['rental_unit_id'] = $rental_unit_id;
				$lease_invoice_update['tenant_id'] = $tenant_id;
				$this->db->where('lease_invoice_id',$lease_invoice_id);
				$this->db->update('lease_invoice',$lease_invoice_update);

				// update the lease invoice table

				$invoice_update['lease_number'] = $lease_number;
				$invoice_update['rental_unit_id'] = $rental_unit_id;
				$invoice_update['tenant_id'] = $tenant_id;
				$this->db->where('lease_invoice_id',$lease_invoice_id);
				$this->db->update('invoice',$invoice_update);
			}
		}
	}


	public function update_lease_payments()
	{
		$this->db->select('leases.lease_number,leases.rental_unit_id,leases.tenant_id,payments.payment_id ');
		$this->db->where('account_id IS NULL AND leases.lease_id = payments.lease_id ');
		$query = $this->db->get('payments,leases');

		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$payment_id = $value->payment_id;
				$lease_number = $value->lease_number;
				$rental_unit_id = $value->rental_unit_id;
				$tenant_id = $value->tenant_id;

				// update the lease invoice table

				$payment_update['account_id'] = $lease_number;
				$payment_update['rental_unit_id'] = $rental_unit_id;
				$payment_update['tenant_id'] = $tenant_id;
				$this->db->where('payment_id',$payment_id);
				$this->db->update('payments',$payment_update);

				// update the lease invoice table

				$invoice_update['lease_number'] = $lease_number;
				$invoice_update['rental_unit_id'] = $rental_unit_id;
				$invoice_update['tenant_id'] = $tenant_id;
				$this->db->where('payment_id',$payment_id);
				$this->db->update('payment_item',$invoice_update);
			}
		}
	}

	public function update_lease_credit_notes()
	{
		$this->db->select('leases.lease_number,leases.rental_unit_id,leases.tenant_id,credit_notes.credit_note_id');
		$this->db->where('account_id IS NULL AND leases.lease_id = credit_notes.lease_id ');
		$query = $this->db->get('credit_notes,leases');

		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$credit_note_id = $value->credit_note_id;
				$lease_number = $value->lease_number;
				$rental_unit_id = $value->rental_unit_id;
				$tenant_id = $value->tenant_id;

				// update the lease invoice table

				$payment_update['account_id'] = $lease_number;
				$payment_update['rental_unit_id'] = $rental_unit_id;
				$payment_update['tenant_id'] = $tenant_id;
				$this->db->where('credit_note_id',$credit_note_id);
				$this->db->update('credit_notes',$payment_update);

				// update the lease invoice table

				$invoice_update['lease_number'] = $lease_number;
				$invoice_update['rental_unit_id'] = $rental_unit_id;
				$invoice_update['tenant_id'] = $tenant_id;
				$this->db->where('credit_note_id',$credit_note_id);
				$this->db->update('credit_note_item',$invoice_update);
			}
		}
	}

	public function update_lease_debit_notes()
	{
		$this->db->select('leases.lease_number,leases.rental_unit_id,leases.tenant_id,debit_notes.debit_note_id');
		$this->db->where('account_id IS NULL AND leases.lease_id = debit_notes.lease_id ');
		$query = $this->db->get('debit_notes,leases');

		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$debit_note_id = $value->debit_note_id;
				$lease_number = $value->lease_number;
				$rental_unit_id = $value->rental_unit_id;
				$tenant_id = $value->tenant_id;

				// update the lease invoice table

				$payment_update['account_id'] = $lease_number;
				$payment_update['rental_unit_id'] = $rental_unit_id;
				$payment_update['tenant_id'] = $tenant_id;
				$this->db->where('debit_note_id',$debit_note_id);
				$this->db->update('debit_notes',$payment_update);

				// update the lease invoice table

				$invoice_update['lease_number'] = $lease_number;
				$invoice_update['rental_unit_id'] = $rental_unit_id;
				$invoice_update['tenant_id'] = $tenant_id;
				$this->db->where('debit_note_id',$debit_note_id);
				$this->db->update('debit_note_item',$invoice_update);
			}
		}
	}

	public function update_property_billing()
	{
		$this->db->select('leases.lease_number,leases.rental_unit_id,leases.tenant_id,property_billing.property_billing_id');
		$this->db->where('property_billing.lease_number IS NULL AND leases.lease_id = property_billing.lease_id ');
		$query = $this->db->get('property_billing,leases');

		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$property_billing_id = $value->property_billing_id;
				$lease_number = $value->lease_number;
				$rental_unit_id = $value->rental_unit_id;
				$tenant_id = $value->tenant_id;

				// update the lease invoice table

				$payment_update['lease_number'] = $lease_number;
				$this->db->where('property_billing_id',$property_billing_id);
				$this->db->update('property_billing',$payment_update);

				// update the lease invoice table
			}
		}
	}

	public function update_items()
	{
		$sql = "SELECT *, COUNT(*)
			FROM mpesa_transactions

			WHERE mpesa_transactions.mpesa_id

			GROUP BY serial_number

			HAVING COUNT(*) > 1

			ORDER BY serial_number ASC
			";
		$query =$this->db->query($sql); 

		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$serial_number = $value->serial_number;

				$sql = 'DELETE from mpesa_transactions where serial_number = "'.$serial_number.'" AND mpesa_transactions.mpesa_id NOT IN (SELECT payment_item.mpesa_id FROM payment_item WHERE  mpesa_transactions.mpesa_id = payment_item.mpesa_id )';
				$this->db->query($sql); 
			}
		}
	}
}
?>