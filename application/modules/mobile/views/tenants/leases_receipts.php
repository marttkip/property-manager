<?php
$routes = '';
$balance = 0;
if($invoices->num_rows() > 0)
{
	foreach ($invoices->result() as $value) {
		# code...
    $transaction_date = $value->createdAt;
    $remarks = $value->transactionDescription;
    $transaction_category = $value->transactionCategory;
    $today = date('jS F Y',strtotime($transaction_date));

    $dr_amount= $value->total_cr_amount;
    $cr_amount = $value->cr_amount;

    $balance += $dr_amount;
    $balance -= $cr_amount;

		$routes .= '

                <li class="list-benefit">
  					      <a href="#" class="item-link item-content">
  					       	<div class="item-inner">
  					          <div class="item-title-row">
  					            <div class="item-title">'.$today.'</div>
  					            <div class="item-after">'.number_format($dr_amount).' </div>
  					          </div>
  					           <div class="item-text">
    					          	 <span>Description :- </span> '.$transaction_category.'
    					          </div>
  					        </div>
  					      </a>
  					    </li>

				   ';
	}
}
echo $routes;
?>
