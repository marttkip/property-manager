<?php
$routes = '';
if($leases->num_rows() > 0)
{
	foreach ($leases->result() as $key) {
		# code...
		$tenant_name = $key->tenant_name;
		$tenant_phone_number = $key->tenant_phone_number;
		$created = $key->created;
		$property_name = $key->property_name;
		$rental_unit_name = $key->rental_unit_name;
		$lease_id = $key->lease_id;
			$this->load->model('accounts/accounts_model');
		$tenants_response = $this->accounts_model->get_tenants_billings($lease_id);
		$total_arrears = $tenants_response['total_arrears'];
		$total_invoice_balance = $tenants_response['total_invoice_balance'];
		$invoice_date = $tenants_response['invoice_date'];

		$tenant_name = str_replace('%20', ' ', $tenant_name);
		$routes .= '
              <li class="list-benefit">
					      <a href="#" class="item-link item-content" onclick="get_lease_dashboard('.$lease_id.')">
					       	<div class="item-inner">
					          <div class="item-title-row">
					            <div class="item-title"><span><i class="fa fa-home"></i></span> House No. : '.$rental_unit_name.'</div>
					            <div class="item-after">Bal Ksh. '.number_format($total_arrears,2).'</div>
					          </div>
					          <div class="item-text">
					          	 <span>Property :- </span> '.$property_name.'
                        <span>Status :- </span> Active
					          </div>
					        </div>
					      </a>
					    </li>
				   ';
	}
}
echo $routes;
?>
