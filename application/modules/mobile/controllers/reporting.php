<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reporting extends MX_Controller {

	function __construct()
	{
		parent:: __construct();

		// Allow from any origin
		if (isset($_SERVER['HTTP_ORIGIN'])) {
			header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
			header('Access-Control-Allow-Credentials: true');
			header('Access-Control-Max-Age: 86400');    // cache for 1 day
		}

		// Access-Control headers are received during OPTIONS requests
		if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

			if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
				header("Access-Control-Allow-Methods: GET, POST, OPTIONS");

			if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
				header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

			exit(0);
		}

		$this->load->model('leases_model');
		$this->load->model('site/site_model');
		$this->load->model('accounts/accounts_model');
		$this->load->model('admin/admin_model');
		$this->load->model('admin/dashboard_model');
		$this->load->model('reporting/reporting_model');

		$this->load->model('email_model');
	}



	public function dashboard()
	{


		$v_data['date_item'] = date('M Y');

		$response['message'] = 'success';
		$response['result'] = $this->load->view('dashboard', $v_data,true);

		echo json_encode($response);
	}


	public function property_invoices()
	{


		$v_data['properties'] = $this->reporting_model->get_all_properties();

		$response['message'] = 'success';
		$response['result'] = $this->load->view('reports/property_invoices', $v_data,true);

		echo json_encode($response);
	}


}
?>