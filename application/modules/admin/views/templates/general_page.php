<?php

	if(!isset($contacts))
	{
		$contacts = $this->site_model->get_contacts();
	}
	$company_name = $contacts['company_name'];
	$data['contacts'] = $contacts;

?>
<!doctype html>
<html>
	<head>
        <?php echo $this->load->view('admin/includes/header', $contacts, TRUE); ?>
        <script src="<?php echo base_url()."assets/themes/admin/";?>bower_components/jquery/dist/jquery.min.js"></script>
    </head>

	<body class="hold-transition skin-blue fixed sidebar-mini">

    	<input type="hidden" id="base_url" value="<?php echo site_url();?>">
    	<input type="hidden" id="config_url" value="<?php echo site_url();?>">

    	<div class="wrapper">
            <!-- Top Navigation -->
            <?php echo $this->load->view('admin/includes/top_navigation', $data, TRUE); ?>
            <?php echo $this->load->view('admin/includes/sidebar', '', TRUE); ?>
             <!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper">
			    <!-- Content Header (Page header) -->
			   <!--  <section class="content-header">
			      <h1>
			        Dashboard
			        <small>Version 2.0</small>
			      </h1>
			      <ol class="breadcrumb">
			        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
			        <li class="active">Dashboard</li>
			      </ol>
			    </section> -->
			    <?php //echo $this->admin_model->create_breadcrumbs($title);?>
			    <!-- Main content -->
			    <section class="content">

					<?php echo $content;?>

                </section>
            </div>
          	<footer class="main-footer">
			    <div class="pull-right hidden-xs">
			      <b>Version</b> 1.0.0
			    </div>
			    <strong>Copyright &copy; <?php echo date('Y')?> <a href="#">Simplex Financials</a>.</strong> All rights
			    reserved.
			 </footer>

			  <!-- Control Sidebar -->
			  <aside class="control-sidebar control-sidebar-dark">
			   		<div id="sidebar-div"></div>
			   		<div class="text-center">
			   			<a href="#" class="btn btn-sm btn-danger" data-toggle="control-sidebar" ><i class="fa fa-gears"></i> CLOSE </a>
			   		</div>
			   		
			  </aside>
			  <!-- /.control-sidebar -->
			  <!-- Add the sidebar's background. This div must be placed
			       immediately after the control sidebar -->
			  <div class="control-sidebar-bg"></div>

			</div>


		<!-- Bootstrap 3.3.7 -->
		<script src="<?php echo base_url()."assets/themes/admin/";?>bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
		<!-- FastClick -->
		<script src="<?php echo base_url()."assets/themes/admin/";?>bower_components/fastclick/lib/fastclick.js"></script>
		<!-- AdminLTE App -->
		<script src="<?php echo base_url()."assets/themes/admin/";?>dist/js/adminlte.min.js"></script>


		<!-- Select2 -->
		<script src="<?php echo base_url()."assets/themes/admin/";?>bower_components/select2/dist/js/select2.full.min.js"></script>
		<!-- InputMask -->
		<script src="<?php echo base_url()."assets/themes/admin/";?>plugins/input-mask/jquery.inputmask.js"></script>
		<script src="<?php echo base_url()."assets/themes/admin/";?>plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
		<script src="<?php echo base_url()."assets/themes/admin/";?>plugins/input-mask/jquery.inputmask.extensions.js"></script>
		<!-- date-range-picker -->
		<script src="<?php echo base_url()."assets/themes/admin/";?>bower_components/moment/min/moment.min.js"></script>
		<script src="<?php echo base_url()."assets/themes/admin/";?>bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
		<!-- bootstrap datepicker -->
		<script src="<?php echo base_url()."assets/themes/admin/";?>bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
		<!-- bootstrap color picker -->
		<script src="<?php echo base_url()."assets/themes/admin/";?>bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
		<!-- bootstrap time picker -->
		<script src="<?php echo base_url()."assets/themes/admin/";?>plugins/timepicker/bootstrap-timepicker.min.js"></script>



		<!-- Sparkline -->
		<script src="<?php echo base_url()."assets/themes/admin/";?>bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
		<!-- jvectormap  -->
		<script src="<?php echo base_url()."assets/themes/admin/";?>plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
		<script src="<?php echo base_url()."assets/themes/admin/";?>plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
		<!-- SlimScroll -->
		<script src="<?php echo base_url()."assets/themes/admin/";?>bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
		<!-- ChartJS -->
		<script src="<?php echo base_url()."assets/themes/admin/";?>bower_components/chart.js/Chart.js"></script>
		<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
		<script src="<?php echo base_url()."assets/themes/admin/";?>dist/js/pages/dashboard2.js"></script>
		<!-- AdminLTE for demo purposes -->
		<script src="<?php echo base_url()."assets/themes/admin/";?>dist/js/demo.js"></script>

		<script src="<?php echo base_url()."assets/themes/admin/";?>bower_components/ckeditor/ckeditor.js"></script>
		<!-- Bootstrap WYSIHTML5 -->
		<script src="<?php echo base_url()."assets/themes/admin/";?>plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
		<script>
		  $(function () {
		    // Replace the <textarea id="editor1"> with a CKEditor
		    // instance, using default configuration.
		    CKEDITOR.replace('editor1')
		    //bootstrap WYSIHTML5 - text editor
		    $('.textarea').wysihtml5()
		  })
		</script>

		<script>
		  $(function () {
		    //Initialize Select2 Elements
		    $('.select2').select2()

		    //Datemask dd/mm/yyyy
		    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
		    //Datemask2 mm/dd/yyyy
		    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
		    //Money Euro
		    $('[data-mask]').inputmask()

		    //Date range picker
		    $('#reservation').daterangepicker()
		    //Date range picker with time picker
		    $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
		    //Date range as a button
		    $('#daterange-btn').daterangepicker(
		      {
		        ranges   : {
		          'Today'       : [moment(), moment()],
		          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
		          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
		          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
		          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
		          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
		        },
		        startDate: moment().subtract(29, 'days'),
		        endDate  : moment()
		      },
		      function (start, end) {
		        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
		      }
		    )

		    //Date picker
		    $('#datepicker').datepicker({
		      autoclose: true,
		      format: 'yyyy-mm-dd',
		    })
				$('#datepicker1').datepicker({
				 autoclose: true,
				 format: 'yyyy-mm-dd',
			 })
		    $('#datepicker2').datepicker({
		      autoclose: true,
		      format: 'yyyy-mm-dd',
		    })
				$('#datepicker4').datepicker({
				 autoclose: true,
				 format: 'yyyy-mm-dd',
			 })
				$('#datepicker3').datepicker({
				 autoclose: true,
				 format: 'yyyy-mm-dd',
			 })
				$('#datepicker5').datepicker({
				 autoclose: true,
				 format: 'yyyy-mm-dd',
			 })
				$('#datepicker6').datepicker({
				 autoclose: true,
				 format: 'yyyy-mm-dd',
			 })

		    //iCheck for checkbox and radio inputs
		    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
		      checkboxClass: 'icheckbox_minimal-blue',
		      radioClass   : 'iradio_minimal-blue'
		    })
		    //Red color scheme for iCheck
		    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
		      checkboxClass: 'icheckbox_minimal-red',
		      radioClass   : 'iradio_minimal-red'
		    })
		    //Flat red color scheme for iCheck
		    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
		      checkboxClass: 'icheckbox_flat-green',
		      radioClass   : 'iradio_flat-green'
		    })

		    //Colorpicker
		    $('.my-colorpicker1').colorpicker()
		    //color picker with addon
		    $('.my-colorpicker2').colorpicker()

		    //Timepicker
		    $('.timepicker').timepicker({
		      showInputs: false
		    })
		  })
		</script>
	</body>
</html>
