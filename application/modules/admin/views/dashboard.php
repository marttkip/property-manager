 <?php
$received_where = 'property_id > 0 ';
$received_table = 'property';
$total_properties = $this->dashboard_model->count_items($received_table, $received_where);



$received_where = 'rental_unit_status = 1';
$received_table = 'rental_unit';

$total_rental_units = $this->dashboard_model->count_items($received_table, $received_where);


$received_where = 'rental_unit_status = 1 AND rental_unit.rental_unit_id = tenant_unit.rental_unit_id AND leases.tenant_unit_id = tenant_unit.tenant_unit_id AND leases.lease_status = 1';
$received_table = 'rental_unit,tenant_unit,leases';

$occupied_rental_units = $this->dashboard_model->count_items($received_table, $received_where);



$received_where = 'rental_unit_status = 1 AND rental_unit.rental_unit_id = tenant_unit.rental_unit_id AND tenant_unit.tenant_unit_id NOT IN (SELECT leases.tenant_unit_id FROM leases WHERE leases.lease_status = 1)';
$received_table = 'rental_unit,tenant_unit';

$unoccupied_rental_units = $this->dashboard_model->count_items($received_table, $received_where);

// percentage


if($total_rental_units > 0 AND $unoccupied_rental_units > 0)
{
 $occupied_percentage = ($occupied_rental_units / $total_rental_units) * 100;
}
else
{
  $occupied_percentage = 0;
}



$received_where = 'lease_status = 1';
$received_table = 'leases';

$active_leases = $this->dashboard_model->count_items($received_table, $received_where);



// $received_where = 'v_property_month_invoices.invoice_month = "'.date('m').'" AND v_property_month_invoices.invoice_year = "'.date('Y').'" ';
// $received_select = 'v_property_month_invoices.total_invoice AS number';
// $received_table = 'v_property_month_invoices';
// $total_invoices = $this->dashboard_model->count_items_group($received_table, $received_where,$received_select);


// $received_where = 'v_property_month_collection.month = "'.date('m').'" AND v_property_month_collection.year = "'.date('Y').'" ';
// $received_select = 'v_property_month_collection.total_paid AS number';
// $received_table = 'v_property_month_collection';
// $total_payments = $this->dashboard_model->count_items_group($received_table, $received_where,$received_select);

// var_dump($total_payments); die();


$revenue_percentage = 1;//$total_payments/$total_invoices * 100;



// $chart_array = array();

$chartExpenses = '';
$chartIncomes = '';
$total_labels= '';

for ($i = 6; $i >= 0; $i--) {
  // code...
  $months = date("Y-m", strtotime( date( 'Y-m-d' )." -$i months"));
  $months_explode = explode('-', $months);
  $year = $months_explode[0];
  $month = $months_explode[1];
  $last_visit = date('M Y',strtotime($months));

  $total_amount = $this->reports_model->get_creditor_service_amounts(NULL,$month,$year);
  $total_tenant_inflow_amt = $this->reports_model->get_amount_collected_invoice_type(NULL,$month,$year);

  $total_labels .=  "'".$last_visit."',";

  if(empty($total_tenant_inflow_amt))
  {
    $total_tenant_inflow_amt = 0;
  }

  if(empty($total_amount))
  {
    $total_amount = 0;
  }

  $chartIncomes .= $total_tenant_inflow_amt.',';
  $chartExpenses .= $total_amount.',';
}

// var_dump($chartExpenses); die();



$received_where = 'property_id > 0';
$received_select = 'property_id,property_name';
$received_table = 'property';
$properties_query = $this->dashboard_model->get_content($received_table, $received_where,$received_select);
$properties_list = '';
if($properties_query->num_rows() > 0)
{
  foreach ($properties_query->result() as $key => $value) {
    # code...
    $property_id = $value->property_id;
    $property_name = $value->property_name;

    $properties_list .= ' <div class="progress-group">
                      <span class="progress-text">'.$property_name.'</span>
                      <span class="progress-number"><b>160</b>/200</span>

                      <div class="progress sm">
                        <div class="progress-bar progress-bar-aqua" style="width: 80%"></div>
                      </div>
                    </div>';
  }
}


$where = 'leases.tenant_unit_id = tenant_unit.tenant_unit_id AND tenant_unit.tenant_id = tenants.tenant_id AND tenant_unit.rental_unit_id = rental_unit.rental_unit_id AND rental_unit.property_id = property.property_id AND leases.lease_deleted = 0 AND MONTH(leases.lease_end_date) ="'.date('m').'" AND YEAR(leases.lease_end_date) ="'.date('Y').'"  ';

  $properties = $this->session->userdata('properties');

  if(isset($properties) AND !empty($properties))
  {
    $where .= $properties;
  }

  $all_leases_search = $this->session->userdata('all_due_leases_search');

  if(isset($all_leases_search) AND !empty($all_leases_search))
  {
    $where .= $all_leases_search;
  }
  // var_dump($where); die();
  $table = 'leases,rental_unit,tenant_unit,tenants,property';
  $select = '*';

  $total_due_leases = $this->dashboard_model->get_content($table, $where,$select,null,10);
  // var_dump($total_due_leases); die();


$leases_result = '';

//if users exist display them
if ($total_due_leases->num_rows() > 0)
{
  $count = 0;

  $leases_result .=
  '
  <table class="table table-bordered table-striped table-condensed">
    <thead>
      <tr>
        <th>#</th>
        <th>Contract No</th>
        <th>Property Name</th>
        <th>Unit Name</th>
        <th>Tenant Name</th>
        <th>Lease Start Date</th>
        <th>Lease End Date</th>
      </tr>
    </thead>
      <tbody>

  ';


  foreach ($total_due_leases->result() as $leases_row)
  {
    $lease_id = $leases_row->lease_id;
    $tenant_unit_id = $leases_row->tenant_unit_id;
    $property_name = $leases_row->property_name;
    $rental_unit_name = $leases_row->rental_unit_name;
    $tenant_name = $leases_row->tenant_name;
    $lease_start_date = $leases_row->lease_start_date;
    $lease_end_date = $leases_row->lease_end_date;

    $lease_duration = $leases_row->lease_duration;
    $rent_amount = $leases_row->rent_amount;
    $lease_number = $leases_row->lease_number;
    $arreas_bf = $leases_row->arrears_bf;
    $rent_calculation = $leases_row->rent_calculation;
    $deposit = $leases_row->deposit;
    $deposit_ext = $leases_row->deposit_ext;
    $lease_status = $leases_row->lease_status;
    $created = $leases_row->created;

    $lease_start_date = date('jS M Y',strtotime($lease_start_date));

    // $expiry_date  = date('jS M Y',strtotime($lease_start_date, mktime()) . " + 365 day");
    $expiry_date  = date('jS M Y', strtotime($lease_end_date));


    //create deactivated status display
    if($lease_status == 0)
    {
      $status = '<span class="label label-default"> Inactive Lease</span>';

      $button = '';
      $delete_button = '<td><a href="'.site_url().'delete-lease/'.$lease_id.'" class="btn btn-sm btn-danger" onclick="return confirm(\'Do you really want to cancel lease?\');" title="Cancel Lease"><i class="fa fa-trash"></i></a></td>';
    }
    //create activated status display
    else if($lease_status == 1)
    {
      $status = '<span class="label label-success">Active</span>';
      $button = '<td><a class="btn btn-default" href="'.site_url().'deactivate-rental-unit/'.$lease_id.'" onclick="return confirm(\'Do you want to deactivate '.$lease_number.'?\');" title="Deactivate '.$lease_number.'"><i class="fa fa-thumbs-down"></i></a></td>';
      $delete_button = '';

    }
    else if($lease_status == 2)
    {
      $status = '<span class="label label-success">Terminated Lease</span>';
      $button = '';
      $delete_button = '';
    }

    if(empty($lease_number))
    {
      $lease_start_date ='';
      $expiry_date = '';
      $status = '<span class="label label-warning">Inactive Lease</span>';
    }


    $count++;
    $leases_result .=
    '
      <tr>
        <td>'.$count.'</td>
        <td>'.$lease_number.'</td>
        <td>'.$property_name.'</td>
        <td>'.$rental_unit_name.'</td>
        <td>'.$tenant_name.'</td>
        <td>'.$lease_start_date.'</td>
        <td>'.$expiry_date.'</td>
      </tr>
    ';

  }

  $leases_result .=
  '
          </tbody>
        </table>
  ';
}

else
{
  $leases_result .= "There are no leases due";
}

$received_where = 'lease_invoice.lease_invoice_id = invoice.lease_invoice_id AND lease_invoice.invoice_month = "'.date('m').'"
AND lease_invoice.invoice_year = "'.date('Y').'" AND  lease_invoice.invoice_deleted = 0 AND leases.lease_id = lease_invoice.lease_id AND rental_unit.rental_unit_id = leases.rental_unit_id AND rental_unit.property_id = property.property_id AND (lease_invoice.invoice_date <= property.closing_date OR property.closing_date IS NULL OR property.closing_date = "0000-00-00") ';
$received_select = 'sum(invoice.invoice_amount) AS number';
$received_table = 'invoice,lease_invoice,leases,rental_unit,property';
$total_month_invoices = $this->dashboard_model->count_items_group($received_table, $received_where,$received_select);


$datestring=''.date('Y-m-d').' first day of last month';
$dt=date_create($datestring);
$first_date = $dt->format('Y-m-d');

$explode =  explode('-',$dt->format('Y-m'));
$year = $explode[0];
$month = $explode[1];

$received_where = 'lease_invoice.lease_invoice_id = invoice.lease_invoice_id AND lease_invoice.invoice_month = "'.$month.'"
AND lease_invoice.invoice_year = "'.$year.'" AND  lease_invoice.invoice_deleted = 0 AND leases.lease_id = lease_invoice.lease_id AND rental_unit.rental_unit_id = leases.rental_unit_id AND rental_unit.property_id = property.property_id AND (lease_invoice.invoice_date <= property.closing_date OR property.closing_date IS NULL OR property.closing_date = "0000-00-00") ';
$received_select = 'sum(invoice.invoice_amount) AS number';
$received_table = 'invoice,lease_invoice,leases,rental_unit,property';
$total_last_month_invoices = $this->dashboard_model->count_items_group($received_table, $received_where,$received_select);



$received_where = 'payments.payment_id = payment_item.payment_id AND payment_item.payment_month = "'.date('m').'"
AND payment_item.payment_year = "'.date('Y').'" AND  payments.cancel = 0 ';
$received_select = 'sum(payment_item.amount_paid) AS number';
$received_table = 'payment_item,payments';
$total_month_payments = $this->dashboard_model->count_items_group($received_table, $received_where,$received_select);



$last_received_where = 'payments.payment_id = payment_item.payment_id AND payment_item.payment_month = "'.$month.'"
AND payment_item.payment_year = "'.$year.'" AND  payments.cancel = 0 AND payments.payment_date >= "'.$first_date.'" AND payments.payment_date <="'.date('Y-m-d').'" ';
$last_received_select = 'sum(payment_item.amount_paid) AS number';
$last_received_table = 'payment_item,payments';
$total_month_payments_last_month = $this->dashboard_model->count_items_group($last_received_table, $last_received_where,$last_received_select);




// $percentage4 = ($total_month_invoices-$total_month_payments)/$total_month_invoices *100;
// $percentage4 = 100 - $percentage4;




$percentage = 0;
$arrow = '<i class="fa fa-caret-left"></i>';
$text = 'text-orange';
if($total_month_invoices > $total_last_month_invoices AND $total_last_month_invoices > 0)
{
  // this is an increase

   $percentage = (($total_month_invoices - $total_last_month_invoices)/$total_last_month_invoices) * 100;
   $arrow = '<i class="fa fa-caret-up"></i>';
   $text = 'text-green';
}

if($total_month_invoices < $total_last_month_invoices AND $total_month_invoices > 0 )
{
  $percentage = (($total_last_month_invoices - $total_month_invoices)/$total_month_invoices) * 100;
  $arrow = '<i class="fa fa-caret-down"></i>';
  $text = 'text-red';
}

// this month
  $total_expense_this_month = $this->reports_model->get_creditor_service_amounts(NULL,date('m'),date('Y'));

// last month

  $total_expense_last_month = $this->reports_model->get_creditor_service_amounts(NULL,$month,$year);

  $percentage2 = 0;
  $arrow2 = '<i class="fa fa-caret-left"></i>';
  $text2 = 'text-orange';
  if($total_expense_this_month > $total_expense_last_month)
  {
    // this is an increase
     $percentage2 = (($total_expense_this_month - $total_expense_last_month)/$total_expense_last_month) * 100;
     $arrow2 = '<i class="fa fa-caret-up"></i>';
     $text2 = 'text-green';
  }

  if($total_expense_this_month < $total_expense_last_month)
  {
    $percentage2 = (($total_expense_last_month - $total_expense_this_month)/$total_expense_this_month) * 100;
    $arrow2 = '<i class="fa fa-caret-down"></i>';
    $text2 = 'text-red';
  }
// var_dump($total_last_month_invoices);die();



$percentage3 = 0;
$arrow3 = '<i class="fa fa-caret-left"></i>';
$text3 = 'text-orange';
$total_this_month_collection = $total_month_payments - $total_month_payments_last_month;

if($total_month_payments > $total_month_payments_last_month AND $total_this_month_collection > 0)
{
  // this is an increase
   $percentage3 = (($total_month_payments - $total_month_payments_last_month)/$total_this_month_collection) * 100;
   $arrow3 = '<i class="fa fa-caret-up"></i>';
   $text3 = 'text-green';
}

if($total_month_payments < $total_month_payments_last_month)
{
  $percentage3 = (($total_month_payments_last_month - $total_month_payments)/$total_this_month_collection) * 100;

  $arrow3 = '<i class="fa fa-caret-down"></i>';
  $text3 = 'text-red';
}

$this_month_collection = $this->reports_model->get_amount_collected_invoice_type(NULL,date('m'),date('Y'));
$this_month_debt = $total_month_invoices - $this_month_collection;
$percentage4 = 0 ;
if($total_month_invoices > 0)
{

$percentage4 = (($this_month_debt /$total_month_invoices) * 100);

}

$text4 = 'text-black';
  $color = 'bg-black';
  // $percentage4 = 40;
if($percentage4 > 50)
{
    $text4 = 'text-red';
    $color = 'bg-red';
}

if($percentage4 > 30 AND $percentage4 <= 50)
{
    $text4 = 'text-orange';
    $color = 'bg-orange';
}
if($percentage4 <= 30)
{
    $text4 = 'text-green';
    $color = 'bg-green';
}



?>



<!-- Info boxes -->
  <div class="row">
       <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3><?php echo number_format($occupied_percentage);?> %</h3>

              <p>Rental Units Occupation</p>
            </div>
            <div class="icon">
              <i class="fa fa-building"></i>
            </div>
            <a href="<?php echo site_url().'property-manager/rental-units'?>" class="small-box-footer">
              More info <i class="fa fa-arrow-circle-right"></i>
            </a>
          </div>
        </div>

        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3><?php echo $active_leases;?></h3>

              <p>Active Leases</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <a href="<?php echo site_url().'lease-manager/leases'?>" class="small-box-footer">
              More info <i class="fa fa-arrow-circle-right"></i>
            </a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3>0</h3>

              <p>Complaints Feedback</p>
            </div>
            <div class="icon">
              <i class="ion ion-pie-graph"></i>
            </div>
            <a href="#" class="small-box-footer">
              More info <i class="fa fa-arrow-circle-right"></i>
            </a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box <?php echo $color;?>">
            <div class="inner">
              <h3><?php echo number_format($percentage4)?><sup style="font-size: 20px">%</sup></h3>

              <p>Month's Payable</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            <a href="<?php echo site_url().'accounts/tenants-invoices'?>" class="small-box-footer">
              More info <i class="fa fa-arrow-circle-right"></i>
            </a>
          </div>
        </div>
  </div>
  <!-- /.row -->

  <div class="row">
    <div class="col-md-12">
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Monthly Recap Report</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
            <div class="btn-group">
              <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
                <i class="fa fa-wrench"></i></button>
              <ul class="dropdown-menu" role="menu">
                <li><a href="#">Action</a></li>
                <li><a href="#">Another action</a></li>
                <li><a href="#">Something else here</a></li>
                <li class="divider"></li>
                <li><a href="#">Separated link</a></li>
              </ul>
            </div>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-12">
              <p class="text-center">
                <strong>Cash flow for past six months</strong>
              </p>

              <div class="chart">
                <!-- Sales Chart Canvas -->
                <canvas id="salesChart2" style="height: 180px;"></canvas>
              </div>
              <!-- /.chart-responsive -->
            </div>
            <!-- /.col -->
            <!-- <div class="col-md-4">
              <p class="text-center">
                <strong>Goal Completion</strong>
              </p>

              <div class="progress-group">
                <span class="progress-text">Add Products to Cart</span>
                <span class="progress-number"><b>160</b>/200</span>

                <div class="progress sm">
                  <div class="progress-bar progress-bar-aqua" style="width: 80%"></div>
                </div>
              </div>
              <div class="progress-group">
                <span class="progress-text">Complete Purchase</span>
                <span class="progress-number"><b>310</b>/400</span>

                <div class="progress sm">
                  <div class="progress-bar progress-bar-red" style="width: 80%"></div>
                </div>
              </div>
              <div class="progress-group">
                <span class="progress-text">Visit Premium Page</span>
                <span class="progress-number"><b>480</b>/800</span>

                <div class="progress sm">
                  <div class="progress-bar progress-bar-green" style="width: 80%"></div>
                </div>
              </div>
              <div class="progress-group">
                <span class="progress-text">Send Inquiries</span>
                <span class="progress-number"><b>250</b>/500</span>

                <div class="progress sm">
                  <div class="progress-bar progress-bar-yellow" style="width: 80%"></div>
                </div>
              </div>
            </div> -->
          </div>
        </div>
        <!-- ./box-body -->
        <div class="box-footer">
          <div class="row">
            <div class="col-sm-3 col-xs-6">
              <div class="description-block border-right">
                <span class="description-percentage <?php echo $text;?>"><?php echo $arrow;?> <?php echo number_format($percentage,0);?>%</span>
                <h5 class="description-header">Kes. <?php echo number_format($total_month_invoices,2)?> </h5>
                <span class="description-text">TOTAL OUTGOING INVOICES</span>
              </div>
              <!-- /.description-block -->
            </div>
            <!-- /.col -->
            <div class="col-sm-3 col-xs-6">
              <div class="description-block border-right">
                  <span class="description-percentage <?php echo $text2;?>"><?php echo $arrow2;?> <?php echo number_format($percentage2,0);?>%</span>
                <h5 class="description-header">Kes. <?php echo number_format($total_expense_this_month,2)?></h5>
                <span class="description-text">TOTAL EXPENSES</span>
              </div>
              <!-- /.description-block -->
            </div>
            <!-- /.col -->
            <div class="col-sm-3 col-xs-6">
              <div class="description-block border-right">
                <span class="description-percentage <?php echo $text3;?>"><?php echo $arrow3;?> <?php echo number_format($percentage3,0);?>%</span>
                <h5 class="description-header">Kes. <?php echo number_format($total_month_payments,2)?></h5>
                <span class="description-text">TOTAL MONTH'S COLLECTION</span>
              </div>
              <!-- /.description-block -->
            </div>
            <!-- /.col -->
            <div class="col-sm-3 col-xs-6">
              <div class="description-block">
                <span class="description-percentage <?php echo $text4;?>"><i class="fa fa-caret-down"></i> <?php echo number_format($percentage4,0)?>%</span>
                <h5 class="description-header">Kes. <?php echo number_format($this_month_debt,2)?></h5>
                <span class="description-text">GOAL COMPLETIONS</span>
              </div>
              <!-- /.description-block -->
            </div>
          </div>
          <!-- /.row -->
        </div>
        <!-- /.box-footer -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->

  <!-- Main row -->
  <div class="row">
    <!-- Left col -->
    <div class="col-md-12">
      <!-- MAP & BOX PANE -->
        <!-- TABLE: LATEST ORDERS -->
      <div class="box box-info">
        <div class="box-header with-border">
          <h3 class="box-title">Due Leases</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="table-responsive">
              <?php echo $leases_result;?>
          </div>
          <!-- /.table-responsive -->
        </div>
        <!-- /.box-body -->
        <div class="box-footer clearfix">
          <!-- <a href="javascript:void(0)" class="btn btn-sm btn-info btn-flat pull-left">Place New Order</a> -->
          <a href="<?php echo site_url().'lease-manager/due-leases'?>" class="btn btn-sm btn-default btn-flat pull-right">View All Due Leases</a>
        </div>
        <!-- /.box-footer -->
      </div>
      <!-- /.box -->
      <!-- /.box -->
      <div class="row">


        <div class="col-md-12">
          <!-- USERS LIST -->
          <div class="box box-danger">
            <div class="box-header with-border">
              <h3 class="box-title">Chat</h3>

              <div class="box-tools pull-right">
                <span class="label label-danger">8 New Members</span>
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                </button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <ul class="users-list clearfix">
                <li>
                  <img src="dist/img/user1-128x128.jpg" alt="User Image">
                  <a class="users-list-name" href="#">Alexander Pierce</a>
                  <span class="users-list-date">Today</span>
                </li>
                <li>
                  <img src="dist/img/user8-128x128.jpg" alt="User Image">
                  <a class="users-list-name" href="#">Norman</a>
                  <span class="users-list-date">Yesterday</span>
                </li>
                <li>
                  <img src="dist/img/user7-128x128.jpg" alt="User Image">
                  <a class="users-list-name" href="#">Jane</a>
                  <span class="users-list-date">12 Jan</span>
                </li>
                <li>
                  <img src="dist/img/user6-128x128.jpg" alt="User Image">
                  <a class="users-list-name" href="#">John</a>
                  <span class="users-list-date">12 Jan</span>
                </li>
                <li>
                  <img src="dist/img/user2-160x160.jpg" alt="User Image">
                  <a class="users-list-name" href="#">Alexander</a>
                  <span class="users-list-date">13 Jan</span>
                </li>
                <li>
                  <img src="dist/img/user5-128x128.jpg" alt="User Image">
                  <a class="users-list-name" href="#">Sarah</a>
                  <span class="users-list-date">14 Jan</span>
                </li>
                <li>
                  <img src="dist/img/user4-128x128.jpg" alt="User Image">
                  <a class="users-list-name" href="#">Nora</a>
                  <span class="users-list-date">15 Jan</span>
                </li>
                <li>
                  <img src="dist/img/user3-128x128.jpg" alt="User Image">
                  <a class="users-list-name" href="#">Nadia</a>
                  <span class="users-list-date">15 Jan</span>
                </li>
              </ul>
              <!-- /.users-list -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer text-center">
              <a href="javascript:void(0)" class="uppercase">View All Users</a>
            </div>
            <!-- /.box-footer -->
          </div>
          <!--/.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Area Chart</h3>

            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
              </button>
              <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div>
          </div>
          <div class="box-body">
            <div class="chart">
              <canvas id="areaChart" style="height:250px"></canvas>
            </div>
          </div>
          <!-- /.box-body -->
        </div>


    </div>
    <!-- /.col -->

    <!-- /.col -->
</div>
  <!-- /.row -->


<script type="text/javascript">
  $(function () {

  'use strict';

  /* ChartJS
   * -------
   * Here we will create a few charts using ChartJS
   */

  // -----------------------
  // - MONTHLY SALES CHART -
  // -----------------------

  // Get context with jQuery - using jQuery's .get() method.
  var salesChartCanvas = $('#salesChart2').get(0).getContext('2d');
  // This will get the first returned node in the jQuery collection.
  var salesChart       = new Chart(salesChartCanvas);

  var salesChartData = {
    labels  : [<?php echo $total_labels;?>],
    datasets: [
      {
        label               : 'Expense',
        fillColor           : 'rgb(210, 214, 222)',
        strokeColor         : 'rgb(210, 214, 222)',
        pointColor          : 'rgb(210, 214, 222)',
        pointStrokeColor    : '#c1c7d1',
        pointHighlightFill  : '#fff',
        pointHighlightStroke: 'rgb(220,220,220)',
        data                : [<?php echo $chartExpenses?>]
      },
      {
        label               : 'Income / Cash Flow',
        fillColor           : 'rgba(60,141,188,0.9)',
        strokeColor         : 'rgba(60,141,188,0.8)',
        pointColor          : '#3b8bba',
        pointStrokeColor    : 'rgba(60,141,188,1)',
        pointHighlightFill  : '#fff',
        pointHighlightStroke: 'rgba(60,141,188,1)',
        data                : [<?php echo $chartIncomes?>]
      }
    ]
  };

  var salesChartOptions = {
    // Boolean - If we should show the scale at all
    showScale               : true,
    // Boolean - Whether grid lines are shown across the chart
    scaleShowGridLines      : false,
    // String - Colour of the grid lines
    scaleGridLineColor      : 'rgba(0,0,0,.05)',
    // Number - Width of the grid lines
    scaleGridLineWidth      : 1,
    // Boolean - Whether to show horizontal lines (except X axis)
    scaleShowHorizontalLines: true,
    // Boolean - Whether to show vertical lines (except Y axis)
    scaleShowVerticalLines  : true,
    // Boolean - Whether the line is curved between points
    bezierCurve             : true,
    // Number - Tension of the bezier curve between points
    bezierCurveTension      : 0.3,
    // Boolean - Whether to show a dot for each point
    pointDot                : false,
    // Number - Radius of each point dot in pixels
    pointDotRadius          : 4,
    // Number - Pixel width of point dot stroke
    pointDotStrokeWidth     : 1,
    // Number - amount extra to add to the radius to cater for hit detection outside the drawn point
    pointHitDetectionRadius : 20,
    // Boolean - Whether to show a stroke for datasets
    datasetStroke           : true,
    // Number - Pixel width of dataset stroke
    datasetStrokeWidth      : 2,
    // Boolean - Whether to fill the dataset with a color
    datasetFill             : true,
    // String - A legend template
    legendTemplate          : '<ul class=\'<%=name.toLowerCase()%>-legend\'><% for (var i=0; i<datasets.length; i++){%><li><span style=\'background-color:<%=datasets[i].lineColor%>\'></span><%=datasets[i].label%></li><%}%></ul>',
    // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
    maintainAspectRatio     : true,
    // Boolean - whether to make the chart responsive to window resizing
    responsive              : true
  };

  // Create the line chart
  salesChart.Line(salesChartData, salesChartOptions);




  


  });




</script>
