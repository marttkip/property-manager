<?php
	$mandrill = '';
	$configuration_id = 0;
	
	if($configuration->num_rows() > 0)
	{
		$res = $configuration->row();
		$configuration_id = $res->configuration_id;
		$mandrill = $res->mandrill;
		$sms_key = $res->sms_key;
		$sms_user = $res->sms_user;
        $sms_suffix = $res->sms_suffix;
        $sms_from = $res->sms_from;
        $mpesa_api_key = $res->mpesa_api_key;
	}
    else
    {
        $configuration_id = '';
        $mandrill = '';
        $sms_key = '';
        $sms_user = '';
        $sms_suffix = '';
        $sms_from = '';
        $mpesa_api_key ='';

    }
			
?>
            	<div class="box">
                    <div class="box-header with-border">
                      <h3 class="box-title"><?php echo $title;?></h3>

                      <div class="box-tools pull-right">
                      </div>
                    </div>
                    <div class="box-body">
                    	<?php echo form_open('administration/edit-configuration/'.$configuration_id);?>
                       	<?php
							$success = $this->session->userdata('success_message');
							$error = $this->session->userdata('error_message');
							
							if(!empty($success))
							{
								echo '
									<div class="alert alert-success">'.$success.'</div>
								';
								
								$this->session->unset_userdata('success_message');
							}
							
							if(!empty($error))
							{
								echo '
									<div class="alert alert-danger">'.$error.'</div>
								';
								
								$this->session->unset_userdata('error_message');
							}
							
						?>
                    	
                        <div class="row" >
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Email API key: </label>
                                    
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control" name="mandrill" placeholder="Email API key" value="<?php echo $mandrill;?>">
                                    </div>
                                </div>
                                <br>
                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Mpesa API key: </label>
                                    
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control" name="mpesa_api_key" placeholder="Mpesa API key" value="<?php echo $mpesa_api_key;?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="col-lg-3 control-label">SMS Key: </label>
                                    
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control" name="sms_key" placeholder="SMS Key" value="<?php echo $sms_key;?>">
                                    </div>
                                </div>
                                <br>
                                <div class="form-group">
                                    <label class="col-lg-3 control-label">SMS User: </label>
                                    
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control" name="sms_user" placeholder="SMS User" value="<?php echo $sms_user;?>">
                                    </div>
                                </div>
                                <br>
                                 <div class="form-group">
                                    <label class="col-lg-3 control-label">SMS From: </label>
                                    
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control" name="sms_from" placeholder="SMS User" value="<?php echo $sms_from;?>">
                                    </div>
                                </div>
                                <br>
                                <div class="form-group">
                                    <label class="col-lg-3 control-label">SMS Suffix: </label>
                                    
                                    <div class="col-lg-9">
                                        <textarea class="form-control" name="sms_suffix" placeholder="SMS suffix" ><?php echo $sms_suffix;?></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row" style="margin-top:2%;">
                        	<div class="col-sm-4 col-sm-offset-4">
                            	<div class="center-align">
                                	<button class="btn btn-primary" type="submit"><i class='fa fa-pencil'></i> Update configuration</button>
                                </div>
                            </div>
                        </div>
                        <?php echo form_close();?>
                    </div>
                </div>