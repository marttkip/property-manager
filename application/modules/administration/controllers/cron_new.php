<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');
ini_set('memory_limit', '-1');

class Cron  extends MX_Controller
{
	function __construct()
	{
		parent:: __construct();
		$this->load->model('administration/sync_model');
	}

	public function sync_visits()
	{

		$date = date('Y-m-d');
		//Sync KDP
		$this->session->set_userdata('branch_code', 'KDP');
		$this->db->where('branch_code = "'.$this->session->userdata('branch_code').'" AND visit_date = "'.$date.'"');
		$query = $this->db->get('visit');

		if($query->num_rows() > 0)
		{
			foreach($query->result() as $res)
			{
				$visit_id = $res->visit_id;

				if($this->sync_model->syn_up_on_closing_visit($visit_id))
				{
				}
			}
		}

		//Sync KDPH
		$this->session->set_userdata('branch_code', 'KDPH');

		$this->db->where('branch_code = "'.$this->session->userdata('branch_code').'" AND visit_date = "'.$date.'"');
		$query = $this->db->get('visit');

		if($query->num_rows() > 0)
		{
			foreach($query->result() as $res)
			{
				$visit_id = $res->visit_id;

				if($this->sync_model->syn_up_on_closing_visit($visit_id))
				{
				}
			}
		}
	}





	public function sync_property_owners()
	{


		$this->db->where('sync_status = 0');
		$query = $this->db->get('em_landlords');

		// var_dump($query); die();

		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...



				$insert_array['llcode'] = $value->llcode;
				$insert_array['property_owner_name'] = $value->firstname.' '.$value->middlename.' '.$value->lastname;
				// $insert_array['tenant_postal_address'] = $value->postaladdress.' '.$value->address;
				$insert_array['created'] = $value->registeredon;
				$insert_array['property_owner_id_no'] = $value->idno;
				$insert_array['property_owner_phone'] = $value->mobile;
				$insert_array['postal_address'] = $value->postaladdress;
				$insert_array['address'] = $value->address;
				$insert_array['deductcommission'] = $value->deductcommission;
				$insert_array['property_owner_email'] = $value->email;
				$insert_array['property_owner_id'] = $value->id;
				$status = $value->status;
				if($status == 'Active')
				{

				$insert_array['property_owner_status'] = 1;
				}
				else
				{

				$insert_array['property_owner_status'] = 0;
				}




				// insert into tenants
				$this->db->insert('property_owners',$insert_array);

				// update the em_tenants
				$update_array['sync_status'] = 1;
				$this->db->where('id',$value->id);
				$this->db->update('em_landlords',$update_array);


			}
		}

	}


	public function sync_properties()
	{


		$this->db->where('sync_status = 0');
		$query = $this->db->get('em_plots');

		// var_dump($query); die();

		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$insert_array['property_location'] = $value->location;
				$insert_array['property_name'] = $value->name;
				// $insert_array['created'] = $value->registeredon;
				$insert_array['lr_no'] = $value->lrno;
				$insert_array['commission_type'] = $value->commissiontype;
				$insert_array['commission'] = $value->commission;
				$insert_array['property_type'] = $value->typeid;
				$insert_array['manage_from'] = $value->managefrom;
				$insert_array['total_units'] = $value->noofhouses;
				$insert_array['property_code'] = $value->code;
				$insert_array['estate'] = $value->estate;
				$insert_array['road'] = $value->road;
				$insert_array['penaltydate'] = $value->penaltydate;
				$insert_array['mgtfeevatable'] = $value->mgtfeevatable;
				$insert_array['mgtfeevatclasseid'] = $value->mgtfeevatclasseid;
				$insert_array['vatable'] = $value->vatable;
				$insert_array['vatclasseid'] = $value->vatclasseid;
				$insert_array['deductcommission'] = $value->deductcommission;
				$insert_array['percentage'] = $value->percentage;
				$insert_array['property_id'] = $value->id;
				$insert_array['property_owner_id'] = $value->landlordid;
				$insert_array['property_status'] = 1;

				// insert into tenants
				$this->db->insert('property',$insert_array);

				// update the em_tenants
				$update_array['sync_status'] = 1;
				$this->db->where('id',$value->id);
				$this->db->update('em_plots',$update_array);


			}
		}

	}


	public function sync_rental_units()
	{


		$this->db->where('sync_status = 0');
		$query = $this->db->get('em_houses');

		// var_dump($query); die();

		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$insert_array['rental_unit_name'] = $value->hseno;
				$insert_array['rental_unit_price'] = $value->amount;
				// $insert_array['created'] = $value->registeredon;
				$insert_array['property_id'] = $value->plotid;
				$insert_array['rental_unit_code'] = $value->hsecode;
				$insert_array['elecaccno'] = $value->elecaccno;
				$insert_array['elecserialno'] = $value->elecserialno;
				$insert_array['wateraccno'] = $value->wateraccno;
				$insert_array['waterserialno'] = $value->waterserialno;
				$insert_array['hsedescriptionid'] = $value->hsedescriptionid;

				$insert_array['deposit'] = $value->deposit;
				$insert_array['depositmgtfee'] = $value->depositmgtfee;

				$insert_array['depositmgtfeevatable'] = $value->depositmgtfeevatable;
				$insert_array['depositmgtfeevatclasseid'] = $value->depositmgtfeevatclasseid;
				$insert_array['depositmgtfeeperc'] = $value->depositmgtfeeperc;
				$insert_array['vatable'] = $value->vatable;
				$insert_array['housestatusid'] = $value->housestatusid;
				$insert_array['chargeable'] = $value->chargeable;
				$insert_array['penalty'] = $value->penalty;
				$insert_array['businessname'] = $value->businessname;
				$insert_array['type'] = $value->type;
				$insert_array['chargeable'] = $value->chargeable;
				$insert_array['rental_unit_id'] = $value->id;
				$insert_array['rental_unit_status'] = 1;

				// insert into tenants
				$this->db->insert('rental_unit',$insert_array);

				// update the em_tenants
				$update_array['sync_status'] = 1;
				$this->db->where('id',$value->id);
				$this->db->update('em_houses',$update_array);


			}
		}

	}
	public function sync_tenants()
	{

		$this->db->where('sync_status = 0');
		$query = $this->db->get('em_tenants');

		// var_dump($query); die();

		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...

				$insert_array['tenant_code'] = $value->code;
				$insert_array['tenant_name'] = $value->firstname.' '.$value->middlename.' '.$value->lastname;
				$insert_array['tenant_postal_address'] = $value->postaladdress.' '.$value->address;
				$insert_array['created'] = $value->registeredon;
				$insert_array['tenant_national_id'] = $value->idno;
				$insert_array['tenant_phone_number'] = $value->mobile;
				$insert_array['tenant_code'] = $value->code;
				$insert_array['tenant_next_of_kin'] = $value->nextofkin;
				$insert_array['tenant_next_of_kin_no'] = $value->nextofkinno;
				$insert_array['tenant_email'] = $value->email;
				$insert_array['tenant_id'] = $value->id;
				$insert_array['tenant_status'] = 1;




				// insert into tenants
				$this->db->insert('tenants',$insert_array);

				// update the em_tenants
				$update_array['sync_status'] = 1;
				$this->db->where('id',$value->id);
				$this->db->update('em_tenants',$update_array);


			}
		}

	}
	public function sync_leases()
	{

		$this->db->where('em_houserentings.sync_status = 0 AND em_houserentings.tenantid = tenants.tenant_id');
		$query = $this->db->get('em_houserentings,tenants');

		// var_dump($query); die();

		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...

				$insert_array2['tenant_id'] = $value->tenantid;
				$insert_array2['rental_unit_id'] = $value->houseid;
				$insert_array2['tenant_unit_id'] = $value->id;
				$this->db->insert('tenant_unit',$insert_array2);


				$insert_array['lease_start_date'] = $value->occupiedon;
				$insert_array['lease_end_date'] = $value->leaseends;
				$insert_array['vacated_on'] = $vacatedon = $value->vacatedon;
				$insert_array['lease_duration'] = $value->renewevery;
				$insert_array['increasetype'] = $value->increasetype;
				$insert_array['increaseby'] = $value->increaseby;
				$insert_array['increaseevery'] = $value->increaseevery;
				$insert_array['rentduedate'] = $value->rentduedate;
				$insert_array['tenant_id'] = $value->tenantid;
				$insert_array['rental_unit_id'] = $value->houseid;
				$insert_array['tenant_unit_id'] = $value->id;
				$insert_array['lease_id'] = $value->id;
				$insert_array['lease_number'] = $value->account_id;
				// $insert_array['lease_status'] = 1;



				$lease_status = $value->status;
				

				if($vacatedon != "0000-00-00")
				{
					$insert_array['lease_status'] = 4;
				}
				else
				{

					if($lease_status == 2)
					{
						$insert_array['lease_status'] = 4;
					}
					else if($lease_status == 0)
					{
						$insert_array['lease_status'] = 1;
					}
					else
					{
						$insert_array['lease_status'] = 0;
					}

					// $insert_array['lease_status'] = 1;
				}
				// insert into tenants
				$this->db->insert('leases',$insert_array);

				// update the em_tenants
				$update_array['sync_status'] = 1;
				$this->db->where('id',$value->id);
				$this->db->update('em_houserentings',$update_array);


			}
		}

	}
	public function sync_tenants_accounts()
	{

		$this->db->where('acctypeid = 32 AND sync_status = 0');
		$query = $this->db->get('fn_generaljournalaccounts');

		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$tenant_id = $value->refid;
				$account_id = $value->id;

				$update_array['account_id'] = $account_id;
				$this->db->where('tenant_id',$tenant_id);
				$this->db->update('tenants',$update_array);

				$update_array2['sync_status'] = 1;
				$this->db->where('id',$account_id);
				$this->db->update('fn_generaljournalaccounts',$update_array2);

			}


		}
	}
	public function sync_tenants_account_id()
	{
		$this->db->where('leases.lease_number IS NULL AND leases.tenant_id = fn_generaljournalaccounts.refid AND acctypeid = 32');
		$query = $this->db->get('leases,fn_generaljournalaccounts');

		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$tenant_id = $value->tenant_id;
				$lease_id = $value->lease_id;
				$tenant_unit_id = $value->tenant_unit_id;
				$account_id = $value->id;
				$account_id = $value->id;

				$update_array['lease_number'] = $account_id;
				$this->db->where('lease_id',$lease_id);
				$this->db->update('leases',$update_array);

			}
		}
	}

	public function sync_invoice_items()
	{

		
		$this->db->where('sync_status = 0');
		$query2 = $this->db->get('em_payables');

		if($query2->num_rows() > 0)
		{
			foreach ($query2->result() as $key => $value6) {
				# code...
				$invoice_amount = $value6->total;
				$invoice_type_id = $value6->paymenttermid;
				$remarks = $value6->remarks;
				$month = $value6->month;
				$year = $value6->year;
				$invoicedon = $value6->invoicedon;
				$houseid = $value6->houseid;
				$tenantid = $value6->tenantid;
				$documentno = $value6->documentno;

				// get the lease id for this tenant
				if($tenantid == 0 or $houseid == 0)
				{

				}
				else
				{

					$this->db->where('tenant_id ='.$tenantid.' AND rental_unit_id ='.$houseid);
					$query6 = $this->db->get('leases');

					if($query6->num_rows() > 0)
					{
						foreach ($query6->result() as $key => $value) {
							# code...
							$lease_id = $value->lease_id;
						}
					}


					$next_date = $year.'-'.$month.'-'.'01';
					$next_date = strtotime($next_date);
					$next_quarter = ceil(date('m', $next_date) / 3);
					$next_month = ($next_quarter * 3) - 2;
					$next_year = date('Y', $next_date);
					$next_quarter = 'AC'.$next_quarter.'-'.$next_year;

					// var_dump($invoice_type_id); die();
					if($month < 10)
					{
						$month = '0'.$month;
					}

					$insert_array = array(
											'lease_id' => $lease_id,
											'invoice_amount' => $invoice_amount,
											'invoice_type' => $invoice_type_id,
											'billing_schedule_quarter' => $next_quarter,
											'remarks' => $remarks,
											'year' => $year,
											'month' => $month,
											'created'=>$invoicedon,
											'document_no'=>$documentno,
											'tenant_id'=>$tenantid,
											'rental_unit_id'=>$houseid
									 	 );

					$this->db->insert('invoice',$insert_array);

					$update_array['sync_status'] = 1;
					$this->db->where('id',$value6->id);
					$this->db->update('em_payables',$update_array);

				}
				
			}

			
		}

	}
	public function sync_invoices()
	{
		$this->db->select('invoice.*');
		$this->db->where('invoice.sync_status = 0');
		$this->db->group_by('document_no');
		$query2 = $this->db->get('invoice');


		if($query2->num_rows() > 0)
		{
			foreach ($query2->result() as $key => $value) {
				# code...
				$lease_id = $value->lease_id;
				$tenant_id = $value->tenant_id;
				$rental_unit_id = $value->rental_unit_id;
				$document_no = $value->document_no;
				$invoice_id = $value->invoice_id;
				$month = $value->month;
				$year = $value->year;
				$this->db->select('fn_generaljournals.documentno,transactdate,jvno,remarks,memo,accountid,debit');
				$this->db->where('sync_status = 0 and transactionid = 2 and (accountid <> 6 OR accountid <> 10 OR accountid <> 6618) AND debit <> 0 AND documentno = '.$document_no.' ');
				$query = $this->db->get('fn_generaljournals');

				if($query->num_rows() > 0)
				{
					foreach ($query->result() as $key => $value) {
						# code...

						$invoice_number = $value->documentno;
						$invoice_date = $value->transactdate;
						$jvno = $value->jvno;
						$remarks = $value->remarks;
						$memo = $value->memo;
						$accountid = $value->accountid;
						$amount = $value->debit;
						$prefix = '';
						$suffix = '';

						$invoice_month = $month;
						$invoice_year = $year;

						
						$insertarray['invoice_date'] = $invoice_date;
						$insertarray['invoice_year'] = $invoice_year;
						$insertarray['invoice_month'] = $invoice_month;
						$insertarray['total_amount'] = $amount;
						$insertarray['document_number'] = $invoice_number;
						$insertarray['created_by'] = $this->session->userdata('personnel_id');
						$insertarray['created'] = date('Y-m-d');
						$insertarray['prefix'] = $prefix;
						$insertarray['suffix'] = $suffix;
						$insertarray['account_id'] = $accountid;
						$insertarray['memo'] = $memo;
						$insertarray['sent_status'] = 1;
						$insertarray['remarks'] = $remarks;
						$insertarray['lease_id'] = $lease_id;
						$insertarray['jvno'] = $jvno;
						$insertarray['rental_unit_id'] = $rental_unit_id;
						$insertarray['tenant_id'] = $tenant_id;

						// var_dump($insertarray); die();
						$this->db->insert('lease_invoice',$insertarray);
						$lease_invoice_id = $this->db->insert_id();


						$update_array6['sync_status'] = 1;
						$update_array6['lease_invoice_id'] = $lease_invoice_id;
						$this->db->where('document_no',$document_no);
						$this->db->update('invoice',$update_array6);

						$update_array['sync_status'] = 1;
						$this->db->where('id',$value->id);
						$this->db->update('fn_generaljournals',$update_array);
						
					}
				}

				
			}
		}

		
	}

	public function sync_payment_items()
	{
		$this->db->where('sync_status = 0');
		$query2 = $this->db->get('em_tenantpayments');

		if($query2->num_rows() > 0)
		{
			foreach ($query2->result() as $key => $value6) {
				# code...
				$amount_paid = $value6->amount;
				$invoice_type_id = $value6->paymenttermid;
				$remarks = $value6->memo;
				$paidon = $value6->paidon;
				$year = $value6->year;
				$month = $value6->month;
				$paymentmodeid = $value6->paymentmodeid;
				$bankid = $value6->bankid;

				if($month < 10)
				{
					$month = '0'.$month;
				}

				$houseid = $value6->houseid;
				$tenantid = $value6->tenantid;
				$documentno = $value6->documentno;

				// get the lease id for this tenant
				if($tenantid == 0 or $houseid == 0)
				{

				}
				else
				{

					$this->db->where('tenant_id ='.$tenantid.' AND rental_unit_id ='.$houseid);
					$query6 = $this->db->get('leases');

					if($query6->num_rows() > 0)
					{
						foreach ($query6->result() as $key => $value) {
							# code...
							$lease_id = $value->lease_id;
						}
					}

					// enter into the payments table

					$insert_array = array(
											'amount_paid' => $amount_paid,
											'invoice_type_id' => $invoice_type_id,
											'lease_id' => $lease_id,
											'payment_item_created'=>$paidon,
											'remarks'=>$remarks,
											'payment_month'=>$month,
											'payment_year'=>$year,
											'document_no'=>$documentno,
											'rental_unit_id'=>$houseid,
											'tenant_id'=>$tenantid,
											'bank_id' => $bankid,
											'payment_mode_id' => $paymentmodeid,

									 	 );
					$this->db->insert('payment_item',$insert_array);

					$update_array['sync_status'] = 1;
					$this->db->where('id',$value6->id);
					$this->db->update('em_tenantpayments',$update_array);
				}
				

			}
		}

	}



	



	public function sync_payments()
	{
		$this->db->select('payment_item.*');
		$this->db->where('payment_item.sync_status = 0');
		$this->db->group_by('document_no');
		$query2 = $this->db->get('payment_item');

		if($query2->num_rows() > 0)
		{
			foreach ($query2->result() as $key => $value) {
				# code...
				$lease_id = $value->lease_id;
				$tenant_id = $value->tenant_id;
				$rental_unit_id = $value->rental_unit_id;
				$document_no = $value->document_no;
				$payment_item_id = $value->payment_item_id;
				$payment_month = $value->payment_month;
				$payment_year = $value->payment_year;
				$bank_id = $value->bank_id;
				$payment_mode_id = $value->payment_mode_id;

				$this->db->where('fn_generaljournals.sync_status = 0 and fn_generaljournals.transactionid = 14 AND fn_generaljournals.accountid <> 6 AND fn_generaljournals.accountid <> 1 and fn_generaljournals.credit <> 0 AND fn_generaljournalaccounts.id = fn_generaljournals.accountid AND fn_generaljournalaccounts.acctypeid = 32 AND documentno= '.$document_no.'');
				$this->db->select('fn_generaljournals.documentno,fn_generaljournals.transactdate,fn_generaljournals.jvno,fn_generaljournals.remarks,fn_generaljournals.accountid,fn_generaljournals.memo,fn_generaljournals.credit,fn_generaljournals.chequeno,fn_generaljournals.id');
				$query = $this->db->get('fn_generaljournals,fn_generaljournalaccounts');
				
				if($query->num_rows() > 0)
				{
					foreach ($query->result() as $key => $value) {
						# code...

						$invoice_number = $value->documentno;
						$invoice_date = $value->transactdate;
						$jvno = $value->jvno;
						$remarks = $value->remarks;
						$memo = $value->memo;
						$accountid = $value->accountid;
						$chequeno = $value->chequeno;
						$amount = $value->credit;

						$prefix = '';
						$suffix = '';

						$invoice_month = $payment_month;
						$invoice_year = $payment_year;

						// create the invoice
						$insertarray['payment_date'] = $invoice_date;
						$insertarray['year'] = $invoice_year;
						$insertarray['month'] = $invoice_month;
						$insertarray['amount_paid'] = $amount;
						$insertarray['document_number'] = $invoice_number;
						$insertarray['created_by'] = $this->session->userdata('personnel_id');
						$insertarray['created'] = date('Y-m-d');
						$insertarray['prefix'] = $prefix;
						$insertarray['suffix'] = $suffix;
						$insertarray['account_id'] = $accountid;
						$insertarray['memo'] = $memo;
						$insertarray['transaction_code'] = $chequeno;
						$insertarray['remarks'] = $remarks;
						$insertarray['lease_id'] = $lease_id;
						$insertarray['tenant_id'] = $tenant_id;
						$insertarray['rental_unit_id'] = $rental_unit_id;
						$insertarray['jvno'] = $jvno;

						$insertarray['bank_id'] = $bank_id;
						$insertarray['payment_method_id'] = $payment_mode_id;


						// var_dump($insertarray); die();
						$this->db->insert('payments',$insertarray);
						$payment_id = $this->db->insert_id();

						$update_array14['sync_status'] = 1;
						$update_array14['payment_id'] = $payment_id;
						$this->db->where('document_no',$document_no);
						$this->db->update('payment_item',$update_array14);

						$update_array['sync_status'] = 1;
						$this->db->where('id',$value->id);
						$this->db->update('fn_generaljournals',$update_array);

					}
				}
			}
		}
	}


	

	public function update_other_payments()
	{
		$this->db->where('sync_status = 0 AND em_tenantpayments.tenantid = tenants.tenant_id');
		$query_details = $this->db->get('em_tenantpayments,tenants');
		// var_dump($query_details); die();
		if($query_details->num_rows() > 0)
		{
			foreach ($query_details->result() as $key => $value6) {
					# code...
					$amount_paid = $value6->amount;
					$invoice_type_id = $value6->paymenttermid;
					$remarks = $value6->memo;
					$document_number = $value6->documentno;
					$account_id = $value6->account_id;
					$invoice_date = $value6->paidon;
					$invoice_month = $value6->month;
					$invoice_year = $value6->year;

					if($invoice_month < 10)
					{
						$invoice_month = '0'.$invoice_month;
					}


					$this->db->where('lease_number = '.$account_id);
					$this->db->select('rental_unit_id,tenant_id,lease_id');
					$query_two = $this->db->get('leases');
					$lease_id = '';
					if($query_two->num_rows() > 0)
					{
						foreach ($query_two->result() as $key => $value3) {
							# code...
							$lease_id = $value3->lease_id;
							$tenant_id = $value3->tenant_id;
							$rental_unit_id = $value3->rental_unit_id;
						}
					}

					if($lease_id > 0)
					{

						$this->db->where('document_number = "'.$document_number.'"');
						$query2 = $this->db->get('payments');

						if($query2->num_rows() > 0)
						{
							foreach ($query2->result() as $key => $value2) {
								# code...
								$payment_id = $value2->payment_id;
							}
						}
						else
						{

							$prefix = '';
							$suffix = '';

							$exploded = explode('-', $invoice_date);

							$invoice_month = $exploded[1];
							$invoice_year = $exploded[0];
							// create the invoice
							$insertarray['payment_date'] = $invoice_date;
							$insertarray['year'] = $invoice_year;
							$insertarray['month'] = $invoice_month;
							$insertarray['amount_paid'] = $amount_paid;
							$insertarray['document_number'] = $document_number;
							$insertarray['created_by'] = $this->session->userdata('personnel_id');
							$insertarray['created'] = date('Y-m-d');
							$insertarray['prefix'] = $prefix;
							$insertarray['suffix'] = $suffix;
							$insertarray['account_id'] = $account_id;
							$insertarray['lease_id'] = $lease_id;
							$insertarray['rental_unit_id'] = $rental_unit_id;
							$insertarray['tenant_id'] = $tenant_id;

							$this->db->insert('payments',$insertarray);
							$payment_id = $this->db->insert_id();
						}


						$insert_array = array(
				 													'payment_id' => $payment_id,
				 													'amount_paid' => $amount_paid,
				 													'invoice_type_id' => $invoice_type_id,
				 													'lease_id' => $lease_id,
				 													'payment_item_created'=>$invoice_date,
				 													'remarks'=>$remarks,
				 													'payment_month'=>$invoice_month,
				 													'payment_year'=>$invoice_year
				 											 	 );
						$this->db->insert('payment_item',$insert_array);

						$update_array['sync_status'] = 1;
						$this->db->where('id',$value6->id);
						$this->db->update('em_tenantpayments',$update_array);

					}

			}
		}
	}
	public function sync_leases_amounts()
	{
		$this->db->where('leases.lease_number IS NOT NULL AND leases.lease_status = 1 AND leases.rental_unit_id > 0 AND sync_status = 0');
		$query = $this->db->get('leases');
		// var_dump($query);die();
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$tenant_id = $value->tenant_id;
				$lease_id = $value->lease_id;
				$tenant_unit_id = $value->tenant_unit_id;
				$rental_unit_id  = $value->rental_unit_id;
				// var_dump($rental_unit_id);die();

				$this->db->where('rental_unit_id',$rental_unit_id);
				$query2 = $this->db->get('rental_unit');

				if($query2->num_rows() > 0)
				{
					foreach ($query2->result() as $key => $value) {
						// code...
						 $rental_unit_price = $value->rental_unit_price;
					}

					// if($rental_unit_price > 0)
					// {
							$charge_to = 1;
							$invoice_type_id = 1;
							$billing_schedule_id = 1;
							$arrears = 0;
							$initial_reading = 0;
							$where_array = array(
													'charge_to'=>1,
													'invoice_type_id' => $invoice_type_id,
													'billing_schedule_id' => $billing_schedule_id,
													'lease_id' => $lease_id,
													'start_date' => date('Y-m-d'),
													'initial_reading' => $initial_reading,
													'arrears_bf' => $arrears,
													'billing_amount' => $rental_unit_price,
												);
							$query = $this->db->insert('property_billing',$where_array);

					// }

					$update_array['sync_status'] = 1;
					$this->db->where('lease_id',$lease_id);
					$this->db->update('leases',$update_array);

				}




			}
		}
	}
	public function sync_tenant_numbers()
	{
		$this->db->where('prefix is null AND tenant_code is not null');
		$query = $this->db->get('tenants');

		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$tenant_number = $value->tenant_code;
				$update_array['prefix'] = $prefix = substr($tenant_number,0,1);
				$update_array['suffix'] = str_replace($prefix,'',$tenant_number);
				$tenant_id = $value->tenant_id;
				$update_array['tenant_number'] = $tenant_number;
				// var_dump($update_array);die();
				$this->db->where('tenant_id',$tenant_id);
				$this->db->update('tenants',$update_array);

			}
		}
	}
	public function update_tenants_number()
	{
		$this->db->where('tenant_id > 0');
		$query = $this->db->get('tenants');

		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$tenant_number = $value->tenant_number;
				$tenant_id = $value->tenant_id;
				$update_array['tenant_code'] = $tenant_number;
				// var_dump($update_array);die();
				$this->db->where('tenant_id',$tenant_id);
				$this->db->update('tenants',$update_array);

			}
		}
	}


	public function update_tenants_payment_item()
	{
		$this->db->where('payment_id > 0');
		$query = $this->db->get('payments');

		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$payment_id = $value->payment_id;
				$lease_id = $value->lease_id;

				$update_array['lease_id'] = $lease_id;
				$this->db->where('payment_id',$payment_id);
				$this->db->update('payment_item',$update_array);

			}
		}
	}





	public function sync_credit_note()
	{

		$this->db->where('fn_generaljournals.transactionid = 23 AND fn_generaljournals.accountid <> 13416 and fn_generaljournals.credit <> 0 AND fn_generaljournalaccounts.id = fn_generaljournals.accountid
AND fn_generaljournalaccounts.acctypeid = 32 AND fn_generaljournals.sync_status = 0');
		$this->db->select('fn_generaljournals.documentno,fn_generaljournals.transactdate,fn_generaljournals.jvno,fn_generaljournals.remarks,fn_generaljournals.accountid,fn_generaljournals.memo,fn_generaljournals.credit,fn_generaljournals.chequeno,fn_generaljournals.id');
		$query = $this->db->get('fn_generaljournals,fn_generaljournalaccounts');
		// var_dump($query); die();

		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...

				$invoice_number = $value->documentno;
				$invoice_date = $value->transactdate;
				$jvno = $value->jvno;
				$remarks = $value->remarks;
				$memo = $value->memo;
				$accountid = $value->accountid;
				$chequeno = $value->chequeno;
				$amount = $value->credit;
				$prefix = '';
				$suffix = '';

				$this->db->where('lease_number = '.$accountid);
				$query_two = $this->db->get('leases');
				$lease_id = '';
				if($query_two->num_rows() > 0)
				{
					foreach ($query_two->result() as $key => $value3) {
						# code...
						$lease_id = $value3->lease_id;
					}
				}

					$exploded = explode('-', $invoice_date);

					$invoice_month = $exploded[1];
					$invoice_year = $exploded[0];

					// create the invoice
					$insertarray['credit_note_date'] = $invoice_date;
					$insertarray['year'] = $invoice_year;
					$insertarray['month'] = $invoice_month;
					$insertarray['credit_note_amount'] = $amount;
					$insertarray['document_number'] = $invoice_number;
					$insertarray['created_by'] = $this->session->userdata('personnel_id');
					$insertarray['created'] = date('Y-m-d');
					$insertarray['account_id'] = $accountid;
					$insertarray['memo'] = $memo;
					$insertarray['remarks'] = $remarks;
					$insertarray['lease_id'] = $lease_id;
					$insertarray['jvno'] = $jvno;

					// var_dump($insertarray); die();
					$this->db->insert('credit_notes',$insertarray);
					$lease_invoice_id = $this->db->insert_id();

					$update_array['sync_status'] = 1;
					$this->db->where('id',$value->id);
					$this->db->update('fn_generaljournals',$update_array);

			}
		}
	}


	public function sync_credit_note_items()
	{
		$this->db->where('sync_status = 0');
		$query = $this->db->get('credit_notes');

		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$credit_note_id = $value->credit_note_id;
				$invoice_year = $value->year;
				$invoice_month = $value->month;
				$document_number = $value->document_number;
				$lease_id = $value->lease_id;

				$this->db->where('documentno = "'.$document_number.'" AND sync_status = 0 AND type = "credit"');
				$query2 = $this->db->get('em_returns');

				if($query2->num_rows() > 0)
				{
					foreach ($query2->result() as $key => $value6) {
						# code...
						$amount_paid = $value6->amount;
						$invoice_type_id = $value6->paymenttermid;
						$remarks = $value6->remarks;
						$year = $value6->year;
						$month = $value6->month;
						if($month < 10)
						{
							$month = '0'.$month;
						}

						$paidon = $value6->invoicedon;
						// enter into the payments table

						$insert_array = array(
												'credit_note_id' => $credit_note_id,
												'credit_note_amount' => $amount_paid,
												'invoice_type_id' => $invoice_type_id,
												'lease_id' => $lease_id,
												'credit_note_item_created'=>$paidon,
												'remarks'=>$remarks,
												'credit_note_month'=>$month,
												'credit_note_year'=>$year
										 	 );
						$this->db->insert('credit_note_item',$insert_array);

						$update_array['sync_status'] = 1;
						$this->db->where('id',$value6->id);
						$this->db->update('em_returns',$update_array);

					}
				}

				$update_array3['sync_status'] = 1;
				$this->db->where('credit_note_id',$credit_note_id);
				$this->db->update('credit_notes',$update_array3);


			}



		}
	}




	public function sync_debit_note()
	{

		$this->db->where('fn_generaljournals.transactionid = 23 AND fn_generaljournals.accountid <> 6 and fn_generaljournals.debit <> 0 AND fn_generaljournalaccounts.id = fn_generaljournals.accountid
AND fn_generaljournalaccounts.acctypeid = 32 AND fn_generaljournals.sync_status = 0');
		$this->db->select('fn_generaljournals.documentno,fn_generaljournals.transactdate,fn_generaljournals.jvno,fn_generaljournals.remarks,fn_generaljournals.accountid,fn_generaljournals.memo,fn_generaljournals.debit,fn_generaljournals.chequeno,fn_generaljournals.id');
		$query = $this->db->get('fn_generaljournals,fn_generaljournalaccounts');
		// var_dump($query); die();

		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...

				$invoice_number = $value->documentno;
				$invoice_date = $value->transactdate;
				$jvno = $value->jvno;
				$remarks = $value->remarks;
				$memo = $value->memo;
				$accountid = $value->accountid;
				$chequeno = $value->chequeno;
				$amount = $value->debit;
				$prefix = '';
				$suffix = '';

				$this->db->where('lease_number = '.$accountid);
				$query_two = $this->db->get('leases');
				$lease_id = '';
				if($query_two->num_rows() > 0)
				{
					foreach ($query_two->result() as $key => $value3) {
						# code...
						$lease_id = $value3->lease_id;
					}
				}

					$exploded = explode('-', $invoice_date);

					$invoice_month = $exploded[1];
					$invoice_year = $exploded[0];

					// create the invoice
					$insertarray['debit_note_date'] = $invoice_date;
					$insertarray['year'] = $invoice_year;
					$insertarray['month'] = $invoice_month;
					$insertarray['debit_note_amount'] = $amount;
					$insertarray['document_number'] = $invoice_number;
					$insertarray['created_by'] = $this->session->userdata('personnel_id');
					$insertarray['created'] = date('Y-m-d');
					$insertarray['account_id'] = $accountid;
					$insertarray['memo'] = $memo;
					$insertarray['remarks'] = $remarks;
					$insertarray['lease_id'] = $lease_id;
					$insertarray['jvno'] = $jvno;

					// var_dump($insertarray); die();
					$this->db->insert('debit_notes',$insertarray);
					$lease_invoice_id = $this->db->insert_id();

					$update_array['sync_status'] = 1;
					$this->db->where('id',$value->id);
					$this->db->update('fn_generaljournals',$update_array);

			}
		}
	}


	public function sync_debit_note_items()
	{
		$this->db->where('sync_status = 0');
		$query = $this->db->get('debit_notes');

		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$debit_note_id = $value->debit_note_id;
				$invoice_year = $value->year;
				$invoice_month = $value->month;
				$document_number = $value->document_number;
				$lease_id = $value->lease_id;

				$this->db->where('documentno = "'.$document_number.'" AND sync_status = 0 AND type = "debit"');
				$query2 = $this->db->get('em_returns');

				if($query2->num_rows() > 0)
				{
					foreach ($query2->result() as $key => $value6) {
						# code...
						$amount_paid = $value6->amount;
						$invoice_type_id = $value6->paymenttermid;
						$remarks = $value6->remarks;
						$year = $value6->year;
						$month = $value6->month;
						if($month < 10)
						{
							$month = '0'.$month;
						}

						$paidon = $value6->invoicedon;
						// enter into the payments table

						$insert_array = array(
												'debit_note_id' => $debit_note_id,
												'debit_note_amount' => $amount_paid,
												'invoice_type_id' => $invoice_type_id,
												'lease_id' => $lease_id,
												'debit_note_item_created'=>$paidon,
												'remarks'=>$remarks,
												'debit_note_month'=>$month,
												'debit_note_year'=>$year
										 	 );
						$this->db->insert('debit_note_item',$insert_array);
						$update_array['sync_status'] = 1;
						$this->db->where('id',$value6->id);
						$this->db->update('em_returns',$update_array);

					}
				}

				$update_array3['sync_status'] = 1;
				$this->db->where('debit_note_id',$debit_note_id);
				$this->db->update('debit_notes',$update_array3);


			}



		}
	}



	// public function sync_invoice_items_old()
	// {

	// 	$this->db->where('sync_status = 0');
	// 	$query = $this->db->get('lease_invoice');

	// 	if($query->num_rows() > 0)
	// 	{
	// 		foreach ($query->result() as $key => $value) {
	// 			# code...
	// 			$lease_invoice_id = $value->lease_invoice_id;
	// 			$rental_unit_id = $value->rental_unit_id;
	// 			$tenant_id = $value->tenant_id;
	// 			$invoice_year = $value->invoice_year;
	// 			$invoice_month = $value->invoice_month;
	// 			$invoice_date = $value->invoice_date;
	// 			$document_number = $value->document_number;
	// 			$lease_id = $value->lease_id;

	// 			$this->db->where('documentno = "'.$document_number.'"  AND sync_status = 0');
	// 			$query2 = $this->db->get('em_payables');

	// 			if($query2->num_rows() > 0)
	// 			{
	// 				foreach ($query2->result() as $key => $value6) {
	// 					# code...
	// 					$invoice_amount = $value6->total;
	// 					$invoice_type_id = $value6->paymenttermid;
	// 					$remarks = $value6->remarks;
	// 					$month = $value6->month;
	// 					$year = $value6->year;
	// 					$invoicedon = $value6->invoicedon;

	// 					$next_date = $year.'-'.$month.'-'.'01';
	// 					$next_date = strtotime($next_date);
	// 					$next_quarter = ceil(date('m', $next_date) / 3);
	// 					$next_month = ($next_quarter * 3) - 2;
	// 					$next_year = date('Y', $next_date);
	// 					$next_quarter = 'AC'.$next_quarter.'-'.$next_year;

	// 					// var_dump($invoice_type_id); die();
	// 					if($month < 10)
	// 					{
	// 						$month = '0'.$month;
	// 					}

	// 					$insert_array = array(
	// 											'lease_id' => $lease_id,
	// 											'lease_invoice_id' => $lease_invoice_id,
	// 											'invoice_amount' => $invoice_amount,
	// 											'invoice_type' => $invoice_type_id,
	// 											'billing_schedule_quarter' => $next_quarter,
	// 											'remarks' => $remarks,
	// 											'year' => $year,
	// 											'month' => $month,
	// 											'created'=>$invoicedon
	// 									 	 );

	// 					$this->db->insert('invoice',$insert_array);

	// 					$update_array['sync_status'] = 1;
	// 					$this->db->where('id',$value6->id);
	// 					$this->db->update('em_payables',$update_array);
	// 				}

	// 				$update_array['sync_status'] = 1;
	// 				$this->db->where('lease_invoice_id',$lease_invoice_id);
	// 				$this->db->update('lease_invoice',$update_array);
	// 			}

	// 		}
	// 	}
	// }


	// public function syn_invoices()
	// {

	// 	$this->db->where('sync_status = 0 and transactionid = 2 and (accountid <> 6 OR accountid <> 10 OR accountid <> 6618) AND debit <> 0');
	// 	$query = $this->db->get('fn_generaljournals');

	// 	if($query->num_rows() > 0)
	// 	{
	// 		foreach ($query->result() as $key => $value) {
	// 			# code...

	// 			$invoice_number = $value->documentno;
	// 			$invoice_date = $value->transactdate;
	// 			$jvno = $value->jvno;
	// 			$remarks = $value->remarks;
	// 			$memo = $value->memo;
	// 			$accountid = $value->accountid;
	// 			$amount = $value->debit;
	// 			$prefix = '';
	// 			$suffix = '';

	// 			if(!empty($accountid))
	// 			{
	// 				$this->db->where('lease_number = '.$accountid);
	// 				$this->db->select('rental_unit_id,tenant_id,lease_id');
	// 				$query_two = $this->db->get('leases');
	// 				$lease_id = '';
	// 				if($query_two->num_rows() > 0)
	// 				{
	// 					foreach ($query_two->result() as $key => $value3) {
	// 						# code...
	// 						$lease_id = $value3->lease_id;
	// 						$rental_unit_id = $value3->rental_unit_id;
	// 						$tenant_id = $value3->tenant_id;
	// 					}
	// 				}
	// 				if(!empty($lease_id))
	// 				{
	// 					$exploded = explode('-', $invoice_date);

	// 					$invoice_month = $exploded[1];
	// 					$invoice_year = $exploded[0];

	// 					// if($invoice_month < 9)
	// 					// {
	// 					// 	$invoice_month = '0'.$invoice_month;
	// 					// }
	// 					// check if the invoice nuber exisits
	// 					// create the invoice
	// 					$insertarray['invoice_date'] = $invoice_date;
	// 					$insertarray['invoice_year'] = $invoice_year;
	// 					$insertarray['invoice_month'] = $invoice_month;
	// 					$insertarray['total_amount'] = $amount;
	// 					$insertarray['document_number'] = $invoice_number;
	// 					$insertarray['created_by'] = $this->session->userdata('personnel_id');
	// 					$insertarray['created'] = date('Y-m-d');
	// 					$insertarray['prefix'] = $prefix;
	// 					$insertarray['suffix'] = $suffix;
	// 					$insertarray['account_id'] = $accountid;
	// 					$insertarray['memo'] = $memo;
	// 					$insertarray['sent_status'] = 1;
	// 					$insertarray['remarks'] = $remarks;
	// 					$insertarray['lease_id'] = $lease_id;
	// 					$insertarray['jvno'] = $jvno;
	// 					$insertarray['rental_unit_id'] = $rental_unit_id;
	// 					$insertarray['tenant_id'] = $tenant_id;

	// 					// var_dump($insertarray); die();
	// 					$this->db->insert('lease_invoice',$insertarray);
	// 					$lease_invoice_id = $this->db->insert_id();

	// 					$update_array['sync_status'] = 1;
	// 					$this->db->where('id',$value->id);
	// 					$this->db->update('fn_generaljournals',$update_array);
	// 				}
	// 				else
	// 				{
	// 					$update_array['sync_status'] = 2;
	// 					$this->db->where('id',$value->id);
	// 					$this->db->update('fn_generaljournals',$update_array);
	// 				}
	// 			}



	// 		}
	// 	}
	// }

	// public function sync_payment_items()
	// {
	// 	$this->db->where('sync_status = 0');
	// 	$query = $this->db->get('payments');

	// 	if($query->num_rows() > 0)
	// 	{
	// 		foreach ($query->result() as $key => $value) {
	// 			# code...
	// 			$payment_id = $value->payment_id;
	// 			$invoice_year = $value->year;
	// 			$invoice_month = $value->month;
	// 			$rental_unit_id = $value->rental_unit_id;
	// 			$document_number = $value->document_number;
	// 			$lease_id = $value->lease_id;
	// 			$tenant_id = $value->tenant_id;

	// 			$this->db->where('documentno = "'.$document_number.'"  AND sync_status = 0');
	// 			$query2 = $this->db->get('em_tenantpayments');

	// 			if($query2->num_rows() > 0)
	// 			{
	// 				foreach ($query2->result() as $key => $value6) {
	// 					# code...
	// 					$amount_paid = $value6->amount;
	// 					$invoice_type_id = $value6->paymenttermid;
	// 					$remarks = $value6->memo;
	// 					$paidon = $value6->paidon;
	// 					$year = $value6->year;
	// 					$month = $value6->month;

	// 					if($month < 10)
	// 					{
	// 						$month = '0'.$month;
	// 					}
	// 					// enter into the payments table

	// 					$insert_array = array(
	// 											'payment_id' => $payment_id,
	// 											'amount_paid' => $amount_paid,
	// 											'invoice_type_id' => $invoice_type_id,
	// 											'lease_id' => $lease_id,
	// 											'payment_item_created'=>$paidon,
	// 											'remarks'=>$remarks,
	// 											'payment_month'=>$month,
	// 											'payment_year'=>$year
	// 									 	 );
	// 					$this->db->insert('payment_item',$insert_array);


	// 					$paymentmodeid = $value6->paymentmodeid;
	// 					$bankid = $value6->bankid;
	// 					$paidon = $value6->paidon;
	// 					$update_array2 = array(
	// 											'bank_id' => $bankid,
	// 											'payment_method_id' => $paymentmodeid
	// 										 );

	// 					$this->db->where('payment_id',$payment_id);
	// 					$this->db->update('payments',$update_array2);

	// 					$update_array['sync_status'] = 1;
	// 					$this->db->where('id',$value6->id);
	// 					$this->db->update('em_tenantpayments',$update_array);




	// 				}




	// 			}


	// 			$update_array3['sync_status'] = 1;
	// 			$this->db->where('payment_id',$payment_id);
	// 			$this->db->update('payments',$update_array3);


	// 		}



	// 	}
	// }


}
?>
