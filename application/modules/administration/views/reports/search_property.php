<?php

$month = $this->accounts_model->get_months();
$months_list = '<option value="0">Select a Type</option>';
foreach($month->result() as $res)
{
  $month_id = $res->month_id;
  $month_name = $res->month_name;
  if($month_id < 10)
  {
    $month_id = '0'.$month_id;
  }
  $month = date('M');

  if($month == $month_name)
  {
    $months_list .= '<option value="'.$month_id.'" selected>'.$month_name.'</option>';
  }
  else {
    $months_list .= '<option value="'.$month_id.'">'.$month_name.'</option>';
  }



}


$start = 2015;
$end_year = 2030;
$year_list = '<option value="0">Select a Type</option>';
for ($i=$start; $i < $end_year; $i++) {
  // code...
  $year= date('Y');

  if($year == $i)
  {
    $year_list .= '<option value="'.$i.'" selected>'.$i.'</option>';
  }
  else {
    $year_list .= '<option value="'.$i.'">'.$i.'</option>';
  }
}


?>
<div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Edit <?php echo $title;?></h3>

          <div class="box-tools pull-right">
             <!-- <a href="<?php echo site_url();?>lease-manager/tenants" class="btn btn-info pull-right">Back to tenants</a> -->
          </div>
        </div>
        <div class="box-body">
			<?php
			$error = $this->session->userdata('error_message');
			if(!empty($error))
			{
				?>
				<div class="alert alert-danger">
				<?php echo $error;?>
                </div>
                <?php
				$this->session->unset_userdata('error_message');
			}
            echo form_open("administration/reports/property", array("class" => "form-horizontal", 'target' => '_blank'));
            ?>
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label class="col-lg-4 control-label">Property</label>

                        <div class="col-lg-8">
                           <select id='property_id' name='property_id' class='form-control select2' required="required">
                              <option value=''>None - Please Select a property</option>
                              <?php echo $property_list;?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label class="col-md-3 control-label">Return or Ledger ? </label>
                        <div class="col-md-4">
                            <div class="radio">
                                <label>
                                    <input id="optionsRadios2" type="radio" name="type" value="0" checked="checked" >
                                     Return 
                                </label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="radio">
                                <label>
                                    <input id="optionsRadios2" type="radio" name="type" value="1"  >
                                     Ledger
                                </label>
                            </div>
                        </div>
                        
                        
                    </div>
                </div>
                <div class="col-md-3">
                   <div class="form-group" id="payment_method">
                        <label class="col-md-4 control-label">Year: </label>
                        <div class="col-md-8">
                          <select class="form-control select2" name="year"   required>

                            <?php echo $year_list;?>
                          </select>
                          </div>
                    </div>

                </div>


                 <div class="col-md-3">

                    <div class="form-group" id="payment_method">
                        <label class="col-md-4 control-label">Month: </label>

                        <div class="col-md-8">
                          <select class="form-control select2" name="month_id"   required>
                            <?php echo $months_list;?>
                          </select>
                          </div>
                      </div>
                      
                    

                </div>
            </div>
            <div class="row">
                <div class="form-group">
                    <div class="col-lg-8 col-lg-offset-4">
                        <div class="center-align">
                            <button type="submit" class="btn btn-info">Search</button>
                        </div>
                    </div>
                </div>
            </div>


            <?php
            echo form_close();
            ?>
        </div>
</div>
