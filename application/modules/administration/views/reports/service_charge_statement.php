<?php
$property_id = $this->accounts_model->get_property_id($rental_unit_id); 

$rental_units = $this->accounts_model->get_properties_rental_units($property_id);

$all_leases = $this->leases_model->get_owner_unit_detail($rental_unit_id);

foreach ($all_leases->result() as $leases_row)
{
	$rental_unit_id = $leases_row->rental_unit_id;
	$home_owner_name = $leases_row->home_owner_name;
	$home_owner_email = $leases_row->home_owner_email;
	$home_owner_phone_number = $leases_row->home_owner_phone_number;
	$rental_unit_name = $leases_row->rental_unit_name;
	$property_name = $leases_row->property_name;
	$home_owner_unit_id = $leases_row->home_owner_unit_id;

}


$parent_invoice_date = $this->accounts_model->get_max_owners_invoice_date($rental_unit_id,$invoice_month_parent,$invoice_year_parent);

$units_result = '';
$total_current = 0;
$total_service_item = 0;
$total_amount = 0;
$unknown_deposit = -25000;
if($rental_units->num_rows() > 0)
{
	$previous_amount = $paid_amount = $water_pump_dues = $current_service_invoice_amount = $total_dues = 0;

	foreach ($rental_units->result() as $item) {
		# code...
		$rental_unit_id = $item->rental_unit_id;

		$all_leases = $this->leases_model->get_owner_unit_detail($rental_unit_id);

		foreach ($all_leases->result() as $leases_row)
		{
			$rental_unit_id = $leases_row->rental_unit_id;
			$home_owner_name = $leases_row->home_owner_name;
			$home_owner_email = $leases_row->home_owner_email;
			$home_owner_phone_number = $leases_row->home_owner_phone_number;
			$rental_unit_name = $leases_row->rental_unit_name;
			$property_name = $leases_row->property_name;
			$home_owner_unit_id = $leases_row->home_owner_unit_id;

		}


		$this_month = date('m');
     	$amount_paid = 0;
	     	$payments = $this->accounts_model->get_this_months_payment_owners($rental_unit_id,$this_month);
	     	$current_items = ''; 
	     	$total_paid_amount = 0;

	     	if($payments->num_rows() > 0)
	     	{
	     		$counter = 0;
	     		$receipt_counter = '';
	     		foreach ($payments->result() as $value) {
	     			# code...
	     			$receipt_number = $value->receipt_number;
	     			$amount_paid = $value->amount_paid;

	     			if($counter > 0)
	     			{
	     				$addition = '#';
	     				// $receipt_counter .= $receipt_number.$addition;
	     			}
	     			else
	     			{
	     				$addition = ' ';
	     				
	     			}
	     			$receipt_counter .= $receipt_number.$addition;
	     			$total_paid_amount = $total_paid_amount + $amount_paid;
	     			$counter++;
	     		}
	     		$current_items = '';
	     	}
	     	else
	     	{
	     		$current_items = '';
	     	}


			$todays_date = date('Y-m-d');
			$todays_month = date('m');
			$todays_year = date('Y');
			// $total_payments = $this->accounts_model->get_total_owners_payments_before($rental_unit_id,$todays_year,$todays_month);


			// $total_invoices = $this->accounts_model->get_total_owners_invoices_before($rental_unit_id,$todays_year,$todays_month);
			// // $current_balance = $total_invoices - $total_payments;

			// $invoice_dated = $invoice_year_parent.'-'.$invoice_month_parent.'-01';

			// $total_payments = $this->accounts_model->get_total_owners_payments_before_payment($rental_unit_id,$invoice_dated);


			// $total_amount_paid = $this->accounts_model->get_total_owners_payments_after_payment($rental_unit_id,$invoice_dated);


			// $current_invoice_amount = $this->accounts_model->get_latest_owners_invoice_amount($rental_unit_id,date('Y'),date('m'));

			$starting_arreas = $arrears_amount =$this->accounts_model->get_all_owners_arrears($rental_unit_id);

			// $total_current_arrears = ($total_invoices - $total_payments) + $starting_arreas;
			

			// get the current invoice amount
			// $total_service = $current_invoice_amount ;



			// the total dues
		
			// $total_dues = ($total_current_arrears + $total_service )- $total_amount_paid;

		// var_dump($total_dues); die();

			$service_invoice_amount = $this->accounts_model->get_invoice_owners_brought_forward($rental_unit_id,$parent_invoice_date,4);
			$service_paid_amount = $this->accounts_model->get_paid_owners_brought_forward($rental_unit_id,$parent_invoice_date,4);

			$total_brought_forward = $service_invoice_amount - $service_paid_amount;
			
			$service_current_invoice =  $this->accounts_model->get_invoice_owners_current_forward($rental_unit_id,$parent_invoice_date,4);
			$service_current_payment =  $this->accounts_model->get_today_owners_paid_current_forward($rental_unit_id,$parent_invoice_date,4);

		
			$total_service = $service_current_invoice - $service_current_payment;
			// var_dump($parent_invoice_date); die();

			$account_invoice_amount = $this->accounts_model->get_invoice_owners_brought_forward($rental_unit_id,$parent_invoice_date);
			$account_paid_amount = $this->accounts_model->get_paid_owners_brought_forward($rental_unit_id,$parent_invoice_date);
			$account_todays_payment =  $this->accounts_model->get_today_owners_paid_current_forward($rental_unit_id,$parent_invoice_date);

			$total_brought_forward_account = $account_invoice_amount - $account_paid_amount - $account_todays_payment;

			$total_bill = $total_service + $total_arrears + $total_brought_forward_account + $starting_arreas;

			

		$units_result .='
						<tr>
								<td>'.$rental_unit_name.' </td>
								<td>'.ucwords(strtolower($home_owner_name)).'</td>
								<td>'.number_format($total_bill,2).'</td>
								<td>'.number_format($total_bill,2).'</td>
							</tr> 
						';

		$total_current = $total_current + $total_current_arrears;
		$total_service_item = $total_service_item + $total_service ;
		$total_amount = $total_amount + $total_bill;
	}
	$units_result .='
						<tr>
								<td colspan="2">Unknown deposits </td>
								<td>'.number_format($unknown_deposit,2).'</td>
								<td>'.number_format($unknown_deposit,2).'</td>
							</tr> 
						';

	$units_result .='
						<tr>
								<td colspan="2">Totals </td>
								<td><strong>'.number_format($total_amount+$unknown_deposit,2).'</strong></td>
								<td><strong>'.number_format($total_amount+$unknown_deposit,2).'</strong></td>
							</tr> 
						';
}

?>



<!-- summary details -->
<!DOCTYPE html>
<html lang="en">
    <head>
        <title><?php echo $contacts['company_name'];?> | STATEMENT</title>
        <!-- For mobile content -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" media="all"/>
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css" media="all"/>
        <style type="text/css">
			.receipt_spacing{letter-spacing:0px;}
			.center-align{margin:0 auto; text-align:center;}
			
			.receipt_bottom_border{border-bottom: #888888 medium solid; margin-bottom:1px;}
			.title-img{float:left; padding-left:30px;}
			img.logo{max-height:70px; margin:0 auto;}
			.align-right{margin:0 auto; text-align: right !important;}
			.align-center
			{
				/*padding: 50px;*/
    			text-align: center;
    			font-weight: bolder;
			}
			.table {
			  margin-bottom: 5px;
			  max-width: 100%;
			  width: 100%;
			  font-size: 10px !important;
			}
			.row {
  				margin-left: 0px !important;
    			margin-right: 0px !important;
			}
			

		</style>
		
    </head>
    <body class="receipt_spacing" >
    	<div class="row">
    		<div class="col-md-12">
		    	<div class="receipt_bottom_border">

		        	<table class="table table-condensed">
		                <tr>
		                    <th><h4><?php echo $contacts['company_name'];?> </h4></th>
		                    <th class="align-right">
		                        <img src="<?php echo base_url().'assets/logo/'.$contacts['logo'];?>" alt="<?php echo $contacts['company_name'];?>" class="img-responsive logo" style="float:right;"/>
		                    </th>
		                </tr>
		            </table>
		        </div>
		        <!-- Patient Details -->
		    	<div class="row receipt_bottom_border" style="margin-bottom: 10px;font-size: 10px;">
		        
		            <div class="col-xs-8 pull-left">
	               		<span>HARDY MANOR MANAGEMENT COMPANY LIMITED</span> <br>
	                    <span>BALANCE SUMMARY AS AT <?php echo $parent_invoice_date?></span>
		                   
		            </div>
		        </div>
		        <div class="col-xs-12">
			    	<table class="table table-hover table-bordered table-striped" style="font-size: 10px;">
			            <thead>
			                <tr>
			                  <td>A/C Name </td>
			                  <td>NAME</td>
			                  <td>S/CHARGE ARREARS</td>
			                  <td>TOTAL</td>
			                </tr>
			            </thead>
			            <tbody>
			                <?php echo $units_result;?>
			            </tbody>
			        </table>
		        </div>
		        <div class="col-xs-12">
		        	<div class="col-xs-4 ">
			        </div>
			        <div class="col-xs-3">
			        </div>
			        <div class="col-xs-5">
			        	 <h2 class="panel-title">Unknown Deposits</h2>

			        	<table class="table table-hover table-bordered table-striped" style="font-size: 10px;">
				            <thead>
				                <tr>
				                  <th>Date </th>
				                  <th>Description</th>
				                  <th>Amount</th>
				                </tr>
				            </thead>
				            <tbody>
				               <tr>
				               		<td>16-Nov-14</td>
				               		<td>EFT Transfer</td>
				               		<td>25,000</td>
				               </tr>
				                <tr>
				               		<td></td>
				               		<td>Total</td>
				               		<td>25,000</td>
				               </tr>
				            </tbody>
				        </table>
			        </div>
		        </div>
		        
		</div>
		</div>
        <p style="page-break-after:always;"></p>
		<!-- the statement -->

		<?php

		//  get the property id

		$property_id = $this->accounts_model->get_property_id($rental_unit_id); 

		$rental_units = $this->accounts_model->get_properties_rental_units($property_id);

		if($rental_units->num_rows() > 0)
		{

			foreach ($rental_units->result() as $key) {
				# code...
				$rental_unit_id = $key->rental_unit_id;

				$all_leases = $this->leases_model->get_owner_unit_detail($rental_unit_id);

				foreach ($all_leases->result() as $leases_row)
				{
					$rental_unit_id = $leases_row->rental_unit_id;
					$home_owner_name = $leases_row->home_owner_name;
					$home_owner_email = $leases_row->home_owner_email;
					$home_owner_phone_number = $leases_row->home_owner_phone_number;
					$rental_unit_name = $leases_row->rental_unit_name;
					$property_name = $leases_row->property_name;
					$home_owner_unit_id = $leases_row->home_owner_unit_id;

				}

				$bills = $this->accounts_model->get_all_owners_invoice_month($rental_unit_id);
				$payments = $this->accounts_model->get_all_owners_payments_lease($rental_unit_id);
				$starting_arreas = $arrears_amount =$this->accounts_model->get_all_owners_arrears($rental_unit_id);

				// var_dump($bills); die();
				$x=0;

				$bills_result = '';
				$last_date = '';
				$current_year = date('Y');
				$total_invoices = $bills->num_rows();
				$invoices_count = 0;
				$total_invoice_balance = 0;
				$total_arrears = 0;
				$total_payment_amount = 0;
				$result = '<table class="table table-hover table-bordered table-striped">
						            <thead>
						                <tr>
						                  <th>Date </th>
						                  <th>Description</th>
						                  <th>Debit (Kes)</th>
						                  <th>Credit (Kes)</th>
						                  <th>Balance (Kes)</th>
						                </tr>
						            </thead>
						            <tbody>
										<tr>
											<td colspan="2">Balance b/f</td>
											<td>'.number_format($starting_arreas,2).'</td>
										</tr>';
				if($bills->num_rows() > 0)
				{
					foreach ($bills->result() as $key_bills) {
						# code...
						$invoice_month = $key_bills->invoice_month;
					    $invoice_year = $key_bills->invoice_year;
						$invoice_date = $key_bills->invoice_date;
						$invoice_amount = $key_bills->invoice_amount;
						$balance_bf_id = $key_bills->balance_bf;
						$invoices_count++;
						if($payments->num_rows() > 0)
						{
							foreach ($payments->result() as $payments_key) {
								# code...
								$payment_date = $payments_key->payment_date;
								$payment_year = $payments_key->year;
								$payment_month = $payments_key->month;
								$payment_amount = $payments_key->amount_paid;

								if(($payment_date <= $invoice_date) && ($payment_date > $last_date) && ($payment_amount > 0) && ($balance_bf_id !=1))
								{
									$arrears_amount -= $payment_amount;
									if($payment_year >= $current_year)
									{
										$result .= 
										'
											<tr>
												<td>'.date('d M Y',strtotime($payment_date)).' </td>
												<td>Payment</td>
												<td></td>
												<td>'.number_format($payment_amount, 2).'</td>
												<td>'.number_format($arrears_amount, 2).'</td>
											</tr> 
										';
									}
									
									$total_payment_amount += $payment_amount;

								}
							}
						}
						//display disbursment if cheque amount > 0
						if($invoice_amount != 0)
						{
							$arrears_amount += $invoice_amount;
							$total_invoice_balance += $invoice_amount;
							
								
							if($invoice_year >= $current_year)
							{
								$result .= 
								'
									<tr>
										<td>'.date('d M Y',strtotime($invoice_date)).' </td>
										<td>'.$invoice_month.' '.$invoice_year.' Invoice</td>
										<td>'.number_format($invoice_amount, 2).'</td>
										<td></td>
										<td>'.number_format($arrears_amount, 2).'</td>
									</tr> 
								';
							}
						}
								
						//check if there are any more payments
						if($total_invoices == $invoices_count)
						{
							//get all loan deductions before date
							if($payments->num_rows() > 0)
							{
								foreach ($payments->result() as $payments_key) {
									# code...
									$payment_date = $payments_key->payment_date;
									$payment_year = $payments_key->year;
									$payment_month = $payments_key->month;
									$payment_amount = $payments_key->amount_paid;

									if(($payment_date > $invoice_date) &&  ($payment_amount > 0))
									{
										$arrears_amount -= $payment_amount;
										if($payment_year >= $current_year)
										{
											$result .= 
											'
												<tr>
													<td>'.date('d M Y',strtotime($payment_date)).' </td>
													<td>Payment</td>
													<td></td>
													<td>'.number_format($payment_amount, 2).'</td>
													<td>'.number_format($arrears_amount, 2).'</td>
												</tr> 
											';
										}
										
										$total_payment_amount += $payment_amount;

									}
								}
							}
						}
								$last_date = $invoice_date;
					}
				}	
				else
				{
					//get all loan deductions before date
					if($payments->num_rows() > 0)
					{
						foreach ($payments->result() as $payments_key) {
							# code...
							$payment_date = $payments_key->payment_date;
							$payment_year = $payments_key->year;
							$payment_month = $payments_key->month;
							$payment_amount = $payments_key->amount_paid;



							if(($payment_amount > 0))
							{
								$arrears_amount -= $payment_amount;
								if($payment_year >= $current_year)
								{
									$result .= 
									'
										<tr>
											<td>'.date('d M Y',strtotime($payment_date)).' </td>
											<td>Payment</td>
											<td></td>
											<td>'.number_format($payment_amount, 2).'</td>
											<td>'.number_format($arrears_amount, 2).'</td>
										</tr> 
									';
								}
								
								$total_payment_amount += $payment_amount;

							}
						}
					}
				}
								
				//display loan
				$result .= 
				'
					<tr>
						<th colspan="2">Total</th>
						<th>'.number_format($total_invoice_balance + $starting_arreas, 2).'</th>
						<th>'.number_format($total_payment_amount, 2).'</th>
						<th>'.number_format($arrears_amount, 2).'</th>
					</tr> 
					 </tbody>
				 </table>
						        
				';
				?>

				
				    	<div class="row">
				    		<div class="col-md-12">
						    	<div class="receipt_bottom_border">
						        	<table class="table table-condensed">
						                <tr>
						                    <th><h4><?php echo $contacts['company_name'];?> </h4></th>
						                    <th class="align-right">
						                        <img src="<?php echo base_url().'assets/logo/'.$contacts['logo'];?>" alt="<?php echo $contacts['company_name'];?>" class="img-responsive logo" style="float:right;"/>
						                    </th>
						                </tr>
						            </table>
						        </div>
						    	
						        
						        <!-- Patient Details -->
						    	<div class="row receipt_bottom_border" style="margin-bottom: 10px;">
						        
						            <div class="col-xs-7 pull-left">
						                <h2 class="panel-title">Personal Information</h2>
						                <table class="table table-hover">
						                    <tbody>
						                        <tr><td><span>Name :</span></td><td><?php echo $home_owner_name;?></td></tr>
						                        <tr><td><span>Phone :</span></td><td><?php echo $home_owner_phone_number;?></td></tr>
						                        <tr><td><span>Property :</span></td><td><?php echo $property_name;?></td></tr>
						                        <tr><td><span>Hse No. :</span></td><td><?php echo $rental_unit_name;?></td></tr>
						                        
						                    </tbody>
						                </table>
						            </div>
						             <div class="col-xs-5 pull-right">
						                <h2 class="panel-title">Invoice</h2>
						                <table class="table table-hover">
						                    <tbody>
						                        <tr><td><span>Date :</span></td><td><?php echo '2016-10-17';?></td></tr>
						                        
						                    </tbody>
						                </table>
						            </div>
						            
						        </div>
						        
						    	<div class="row receipt_bottom_border">
						        	<div class="col-xs-12 center-align">
						            	<strong>INDIVIDUAL STATEMENT</strong>
						            </div>
						        </div>

						    
						    	<div class="col-xs-12">
		                        	<?php echo $result;?>
						    	</div>
						               
						           
						        
						        <div class="align-right" style="font-size: 10px;">
						          
				                    <?php echo $contacts['company_name'];?> | <?php echo $contacts['location'];?>, <?php echo $contacts['building'];?>, <?php echo $contacts['floor'];?><br/>
				                     E-mail: <?php echo $contacts['email'];?>. Tel : <?php echo $contacts['phone'];?><br/>
				                     P.O. Box <?php echo $contacts['address'];?> <?php echo $contacts['post_code'];?>, <?php echo $contacts['city'];?><br/>
						               
						        </div>
						</div>
						</div>
				    
						<p style="page-break-after:always;"></p>


				<?php
			}

		}
		?>





		<!-- end of statement -->

    </body>
    
</html>
