<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once "./application/modules/admin/controllers/admin.php";
class Company_financial extends admin
{
    var $documents_path;


	function __construct()
	{
		parent:: __construct();
		$this->load->model('auth/auth_model');
		$this->load->model('financials_model');
    $this->load->model('company_financial_model');



		$this->load->model('admin/file_model');

		//path to image directory
		$this->documents_path = realpath(APPPATH . '../assets/documents/vehicles');


		$this->load->library('image_lib');

		if(!$this->auth_model->check_login())
		{
			redirect('login');
		}
	}


  /*
	*
	*	Default action is to show all the customer
	*
	*/
	public function index()
	{

		$data['title'] = 'Company Financials';
		$v_data['title'] = $data['title'];

		$data['content'] = $this->load->view('financials/financials/landing', $v_data, true);

		// $this->load->view('admin/templates/general_page', $data);
    // $data['content'] = $this->load->view('finance/transfer/write_cheques', $v_data, true);
    $this->load->view('admin/templates/general_page', $data);
	}

	public function profit_and_loss()
	{
		$data['title'] = 'Profit and Loss';
		$v_data['title'] = $data['title'];
		$data['content'] = $this->load->view('financials/financials/profit_and_loss', $v_data, true);
	    $this->load->view('admin/templates/general_page', $data);
	}

	public function balance_sheet()
	{
		$data['title'] = 'Balance Sheet';
		$v_data['title'] = $data['title'];
		$data['content'] = $this->load->view('financials/financials/balance_sheet', $v_data, true);
	    $this->load->view('admin/templates/general_page', $data);
	}
	public function aged_receivables()
	{
		$data['title'] = 'Aged Receivables';
		$v_data['title'] = $data['title'];
		$data['content'] = $this->load->view('financials/receivables/aged_receivables', $v_data, true);
	    $this->load->view('admin/templates/general_page', $data);
	}
	public function sales_taxes()
	{
		$data['title'] = 'Sales Taxes';
		$v_data['title'] = $data['title'];
		$data['content'] = $this->load->view('financials/taxes/sales_taxes', $v_data, true);
	    $this->load->view('admin/templates/general_page', $data);

	}

	public function customers_income()
	{
		$data['title'] = 'Income By Customer';
		$v_data['title'] = $data['title'];
		$data['content'] = $this->load->view('financials/income/customer_income', $v_data, true);
	    $this->load->view('admin/templates/general_page', $data);

	}

	public function vendor_expenses()
	{
		$data['title'] = 'Vendor Expenses';
		$v_data['title'] = $data['title'];
		$data['content'] = $this->load->view('financials/vendors/vendor_expenses', $v_data, true);
	    $this->load->view('admin/templates/general_page', $data);

	}

	public function aged_payables()
	{
		$data['title'] = 'Vendor Expenses';
		$v_data['title'] = $data['title'];
		$data['content'] = $this->load->view('financials/vendors/aged_payables', $v_data, true);
	    $this->load->view('admin/templates/general_page', $data);

	}
	public function general_ledger()
	{
		$data['title'] = 'General Ledger';
		$v_data['title'] = $data['title'];
		$data['content'] = $this->load->view('financials/other/general_ledger', $v_data, true);
	    $this->load->view('admin/templates/general_page', $data);

	}

	public function account_transactions()
	{
		$data['title'] = 'General Ledger';
		$v_data['title'] = $data['title'];
		$data['content'] = $this->load->view('financials/other/account_transactions', $v_data, true);
	    $this->load->view('admin/templates/general_page', $data);

	}

  public function mpesa()
  {
      $url = 'https://sandbox.safaricom.co.ke/oauth/v1/generate?grant_type=client_credentials';

      $curl = curl_init();
      curl_setopt($curl, CURLOPT_URL, $url);
      $credentials = base64_encode('wE2XiEqWDIwOH94xqtABTJGRgVGQuP04:xzNBaRpi61SdFUcL');
      curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Basic '.$credentials)); //setting a custom header
      curl_setopt($curl, CURLOPT_HEADER, true);
      curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

      $curl_response = curl_exec($curl);

      echo json_decode($curl_response);

  }



  public function creditor_statement($creditor_id)
  {

  	$where = 'creditor_account.transaction_type_id = transaction_type.transaction_type_id AND creditor_account.creditor_account_delete = 0 AND creditor_account.creditor_id = '.$creditor_id;
	$table = 'creditor_account, transaction_type';

	if(!empty($date_from) && !empty($date_to))
	{
		$where .= ' AND (creditor_account.creditor_account_date >= \''.$date_from.'\' AND creditor_account.creditor_account_date <= \''.$date_to.'\')';
		$search_title = 'Statement from '.date('jS M Y', strtotime($date_from)).' to '.date('jS M Y', strtotime($date_to)).' ';
	}

	else if(!empty($date_from))
	{
		$where .= ' AND creditor_account.creditor_account_date = \''.$date_from.'\'';
		$search_title = 'Statement of '.date('jS M Y', strtotime($date_from)).' ';
	}

	else if(!empty($date_to))
	{
		$where .= ' AND creditor_account.creditor_account_date = \''.$date_to.'\'';
		$search_title = 'Statement of '.date('jS M Y', strtotime($date_to)).' ';
		$date_from = $date_to;
	}

	else
	{
		// $where .= ' AND DATE_FORMAT(creditor_account.creditor_account_date, \'%m\') = \''.date('m').'\' AND DATE_FORMAT(creditor_account.creditor_account_date, \'%Y\') = \''.date('Y').'\'';
		$where .= '';
		$search_title = 'Statement for the month of '.date('M Y').' ';
		$date_from = date('Y-m-d');
	}
	// echo $where;die();
	$v_data['balance_brought_forward'] = 0;// $this->creditors_model->calculate_balance_brought_forward($date_from,$creditor_id);
	$creditor = $this->company_financial_model->get_creditor($creditor_id);
	$row = $creditor->row();
	$creditor_name = $row->creditor_name;
	$opening_balance = $row->opening_balance;
	$debit_id = $row->debit_id;
	$start_date = $row->start_date;
	// var_dump($opening_balance); die();
	$v_data['module'] = 1;
	$v_data['creditor_name'] = $creditor_name;
	$v_data['creditor_id'] = $creditor_id;
	$v_data['opening_balance'] = $opening_balance;
	$v_data['debit_id'] = $debit_id;


  	// $v_data['query'] = $this->company_financial_model->get_creditor_account($where, $table);
	$v_data['title'] = $creditor_name.' '.$search_title;
	$data['title'] = $creditor_name.' Statement';
	$data['content'] = $this->load->view('vendors/creditor_statement', $v_data, TRUE);
	$this->load->view('admin/templates/general_page', $data);
  }
  public function print_creditor_statement($creditor_id)
  {
    $where = 'creditor_account.transaction_type_id = transaction_type.transaction_type_id AND creditor_account.creditor_account_delete = 0 AND creditor_account.creditor_id = '.$creditor_id;
  	$table = 'creditor_account, transaction_type';

  	if(!empty($date_from) && !empty($date_to))
  	{
  		$where .= ' AND (creditor_account.creditor_account_date >= \''.$date_from.'\' AND creditor_account.creditor_account_date <= \''.$date_to.'\')';
  		$search_title = 'Statement from '.date('jS M Y', strtotime($date_from)).' to '.date('jS M Y', strtotime($date_to)).' ';
  	}

  	else if(!empty($date_from))
  	{
  		$where .= ' AND creditor_account.creditor_account_date = \''.$date_from.'\'';
  		$search_title = 'Statement of '.date('jS M Y', strtotime($date_from)).' ';
  	}

  	else if(!empty($date_to))
  	{
  		$where .= ' AND creditor_account.creditor_account_date = \''.$date_to.'\'';
  		$search_title = 'Statement of '.date('jS M Y', strtotime($date_to)).' ';
  		$date_from = $date_to;
  	}

  	else
  	{
  		// $where .= ' AND DATE_FORMAT(creditor_account.creditor_account_date, \'%m\') = \''.date('m').'\' AND DATE_FORMAT(creditor_account.creditor_account_date, \'%Y\') = \''.date('Y').'\'';
  		$where .= '';
  		$search_title = 'Statement for the month of '.date('M Y').' ';
  		$date_from = date('Y-m-d');
  	}
  	// echo $where;die();
  	$v_data['balance_brought_forward'] = 0;// $this->creditors_model->calculate_balance_brought_forward($date_from,$creditor_id);
    // var_dump($where);
    $creditor = $this->company_financial_model->get_creditor($creditor_id);
    $row = $creditor->row();
    $creditor_name = $row->creditor_name;
    $start_date = $row->start_date;
    $opening_balance = $row->opening_balance;
    $debit_id = $row->debit_id;

    $v_data['contacts'] = $this->site_model->get_contacts();
    $v_data['creditor_id'] = $creditor_id;
    $v_data['start_date'] = $start_date;
    $v_data['opening_balance'] = $opening_balance;
    $v_data['debit_id'] = $debit_id;


    	// $v_data['query'] = $this->company_financial_model->get_creditor_account($where, $table);
  	$v_data['title'] = $creditor_name.' '.$search_title;
  	$data['title'] = $creditor_name.' Statement';
    $this->load->view('vendors/print_creditor_account', $v_data);
  	// $this->load->view('admin/templates/general_page', $data);
  }
}
?>
