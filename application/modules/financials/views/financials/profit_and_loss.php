<?php echo $this->load->view('search/search_profit_and_loss','', true);?>
<?php

$income_rs = $this->company_financial_model->get_income_value('Revenue');
$income_result = '';
$total_income = 0;
if($income_rs->num_rows() > 0)
{
	foreach ($income_rs->result() as $key => $value) {
		# code...
		$total_amount = $value->total_amount;
		$transactionName = $value->transactionName;
		$total_income += $total_amount;
		$income_result .='<tr>
							<td class="text-left">'.$transactionName.'</td>
							<td class="text-right">'.number_format($total_amount,2).'</td>
							</tr>';
	}
	$income_result .='<tr>
							<td class="text-left"><b>Total Income</b></td>
							<td class="text-right"><b>'.number_format($total_income,2).'</b></td>
							</tr>';
}




$operation_rs = $this->company_financial_model->get_operational_cost_value('Expense');
$operation_result = '';
$total_operational_amount = 0;
if($operation_rs->num_rows() > 0)
{
	foreach ($operation_rs->result() as $key => $value) {
		# code...
		$total_amount = $value->total_amount;
		$transactionName = $value->accountName;
		$total_operational_amount += $total_amount;
		$operation_result .='<tr>
							<td class="text-left">'.$transactionName.'</td>
							<td class="text-right">'.number_format($total_amount,2).'</td>
							</tr>';
	}
	$operation_result .='<tr>
							<td class="text-left"><b>Total Operation Cost</b></td>
							<td class="text-right"><b>'.number_format($total_operational_amount,2).'</b></td>
							</tr>';
}



$cog_rs = $this->company_financial_model->get_cog_value('Expense');
$cog_result = '';
$total_cog = 0;
if($cog_rs->num_rows() > 0)
{
	foreach ($cog_rs->result() as $key => $value) {
		# code...
		$total_amount = $value->total_amount;
		$transactionName = $value->accountName;
		$total_cog += $total_amount;
		$cog_result .='<tr>
							<td class="text-left">'.$transactionName.'</td>
							<td class="text-right">'.number_format($total_amount,2).'</td>
							</tr>';
	}

	$cog_result .='<tr>
							<td class="text-left"><b>Total COG</b></td>
							<td class="text-right"><b>'.number_format($total_cog,2).'</b></td>
							</tr>';
}

?>

<div class="text-center">
	<h3 class="box-title">Income Statement</h3>
	<h5 class="box-title">Reporting period: <?php echo date('M j, Y', strtotime(date('Y-01-01')));?> to <?php echo date('M j, Y', strtotime(date('Y-m-d')));?></h5>
	<h6 class="box-title">Created <?php echo date('M j, Y', strtotime(date('Y-m-d')));?></h6>
</div>

<div class="box">
    <div class="box-body">
    	<h5 class="box-title">REVENUE</h5>
    	<table class="table  table-striped table-condensed">
			<thead>
				<tr>
        			<th class="text-left">Account</th>
					<th class="text-right">Balance</th>
				</tr>
			</thead>
			<tbody>
				<?php echo $income_result;?>
			</tbody>
		</table>


		<h5 class="box-title">COST OF GOODS SOLD</h5>
    	<table class="table  table-striped table-condensed">
			<thead>
				<tr>
        			<th class="text-left">Account</th>
					<th class="text-right">Balance</th>
				</tr>
			</thead>
			<tbody>
				<?php echo $cog_result?>
				<tr>
        			<th class="text-left"><strong>GROSS PROFIT</strong></th>
					<th class="text-right"><?php echo number_format($total_income - $total_cog,2)?></th>
				</tr>
			</tbody>
		</table>

		<h5 class="box-title">OPERATING EXPENSE</h5>
    	<table class="table  table-striped table-condensed">
			<thead>
				<tr>
        			<th class="text-left">Account</th>
					<th class="text-right">Balance</th>
				</tr>
			</thead>
			<tbody>
				<?php echo $operation_result?>
				<tr>
        			<th class="text-left"><strong>Operating Profit (Loss)</strong></th>
					<th class="text-right"><?php echo number_format($total_income - $total_cog - $total_operational_amount,2)?></th>
				</tr>

			</tbody>
		</table>

		<!-- <h5 class="box-title">INTEREST (INCOME), EXPENSE & TAXES</h5> -->
    	<table class="table  table-striped table-condensed">
			<thead>
				<tr>
        			<th class="text-left"></th>
					<th class="text-right"></th>
				</tr>
			</thead>
			<tbody>

				<tr>
        			<th class="text-left"><strong>NET Profit</strong></th>
					<th class="text-right"><?php echo number_format($total_income - $total_cog - $total_operational_amount,2)?></th>
				</tr>
			</tbody>
		</table>
    </div>
</div>
