<?php echo $this->load->view('search/search_balance_sheet','', true);?>
<?php
$bank_balances_rs = $this->company_financial_model->get_account_value();
$bank_balances_result = '';
$total_income = 0;
if($bank_balances_rs->num_rows() > 0)
{
	foreach ($bank_balances_rs->result() as $key => $value) {
		# code...
		$total_amount = $value->total_amount;
		$transactionName = $value->accountName;
		$total_income += $total_amount;
		$bank_balances_result .='<tr>
							<td class="text-left">'.$transactionName.'</td>
							<td class="text-right">'.number_format($total_amount,2).'</td>
							</tr>';
	}
	$bank_balances_result .='<tr>
							<td class="text-left"><b>Total Bank Balance</b></td>
							<td class="text-right"><b class="match">'.number_format($total_income,2).'</b></td>
							</tr>';
}

$accounts_receivable = $this->company_financial_model->get_accounts_receivables();
$accounts_payable = $this->company_financial_model->get_accounts_payable();
$wht_payable = $this->company_financial_model->get_total_wht_tax();
$vat_payable = $this->company_financial_model->get_total_vat_tax();

$total_assets = $accounts_receivable+$total_income;

$total_liability = $accounts_payable + $wht_payable +$wht_payable;
$current_year_earnings = $total_assets - $total_liability;
?>
<style>
	td .match
	{
		border-top: #000 2px solid !important;
	}
</style>
<div class="text-center">
	<h3 class="box-title">Balance Sheet</h3>
	<h5 class="box-title">Reporting as of: <?php echo date('M j, Y', strtotime(date('Y-m-d')));?></h5>
	<h6 class="box-title">Created <?php echo date('M j, Y', strtotime(date('Y-m-d')));?></h6>
</div>

<div class="box">
    <div class="box-body">
    	<h4 class="box-title">Asssets</h4>
    	<table class="table  table-striped table-condensed">
			<thead>
				<tr>
					<th class="text-left" colspan="2" style="background-color:#3c8dbc;color:#fff;">BANK</th>
				</tr>
				<tr>
        			<th class="text-left">Account</th>
					<th class="text-right">Balance</th>
				</tr>
			</thead>
			<tbody>
				<?php echo $bank_balances_result?>
			</tbody>

			<thead>
				<tr>
					<th class="text-left" colspan="2" style="background-color:#3c8dbc;color:#fff;">Current Asset</th>
				</tr>
				<tr>
        			<th class="text-left">Account</th>
					<th class="text-right">Balance</th>
				</tr>
			</thead>
			<tbody>
					<tr>
								<td >Cash on Hand</td>
						<td class="text-right">0.00</td>
					</tr>
					<tr>
								<td class="text-left">Accounts Receivables</td>
						<td class="text-right"><?php echo number_format($accounts_receivable,2);?></td>
					</tr>
					<tr>
								<td class="text-left ">Total Current Assets</td>
						<td class="text-right"><b class="match"><?php echo number_format($accounts_receivable,2);?></b></td>
					</tr>
					<tr>
								<td class="text-left ">Total Assets</td>
						<td class="text-right"><b class="match"><?php echo number_format($total_assets,2);?></b></td>
					</tr>

			</tbody>
		</table>


		<h3 class="box-title">Liabilities</h3>
		<table class="table  table-striped table-condensed">
			<thead>
				<tr>
					<th class="text-left" colspan="2" style="background-color:#3c8dbc;color:#fff;">Current Liability</th>
				</tr>
				<tr>
        			<th class="text-left">Account</th>
					<th class="text-right">Balance</th>
				</tr>
			</thead>
			<tbody>
					<tr>
								<td class="text-left">Accounts Payable</td>
						<td class="text-right"><?php echo number_format($accounts_payable,2)?></td>
					</tr>
					<tr>
								<td class="text-left">VAT Payable</td>
						<td class="text-right"><?php echo number_format($vat_payable,2);?></td>
					</tr>
					<tr>
								<td class="text-left">WHT Payable</td>
						<td class="text-right"><?php echo number_format($wht_payable,2);?></td>
					</tr>
					<tr>
								<td class="text-left">Total Current Liability</td>
						<td class="text-right"><b class="match"><?php echo number_format($total_liability,2);?></b></td>
					</tr>
					<tr>
								<td class="text-left">Total Liabilities</td>
								<td class="text-right"><b class="match"><?php echo number_format($total_liability,2);?></b></td>
					</tr>

			</tbody>
		</table>

		<h3 class="box-title">Equity</h3>
    	<table class="table  table-striped table-condensed">
			<thead>
				<tr>
        			<th class="text-left">Account</th>
							<th class="text-right">Balance</th>
				</tr>
			</thead>
			<tbody>
				<tr>
        			<td class="text-left">Owner Investments / Drawings</td>
							<td class="text-right">0.00</td>
				</tr>
				<tr>
        			<td class="text-left">Previous Year(s) Earnings</td>
					<td class="text-right">0.00</td>
				</tr>
				<tr>
							<td class="text-left">Current Year(s) Earnings</td>
					<td class="text-right">0.00</td>
				</tr>
				<tr>
							<td class="text-left">Total Equity</td>
					<td class="text-right"><?php echo number_format($current_year_earnings,2)?></td>
				</tr>
				<tr>
        			<td class="text-left">Total Liabilities and Earnings</td>
							<td class="text-right"><?php echo number_format($total_liability,2)?></td>
				</tr>
			</tbody>
		</table>
    </div>
</div>
