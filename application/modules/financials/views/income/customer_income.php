<?php echo $this->load->view('search/income_search','', true);?>

<?php
$all_properties = $this->company_financial_model->get_all_properties();
$receivables = '';
$total_balances = 0;
if($all_properties->num_rows() > 0)
{

	foreach ($all_properties->result() as $key => $value) {
		// code...
		$property_id = $value->property_id;
		$property_name = $value->property_name;
		$property_balance = $this->company_financial_model->get_property_balances($property_id);
		$receivables .= '	<tr>
													<td class="text-left">'.$property_name.'</td>
											<td class="text-right">'.number_format($property_balance,2).'</td>
										</tr>';
		$total_balances += $property_balance;
	}


}
$receivables .= '	<tr>
											<td class="text-left"><b>Total Expense</b></td>
									<td class="text-right"><b class="match">'.number_format($total_balances,2).'</b></td>
								</tr>';
?>
<style>
	td .match
	{
		border-top: #000 2px solid !important;
	}
</style>

<div class="text-center">
	<h3 class="box-title">Income by Customer</h3>
	<h5 class="box-title">Reporting period: <?php echo date('M j, Y', strtotime(date('Y-01-01')));?> to <?php echo date('M j, Y', strtotime(date('Y-m-d')));?></h5>
	<h6 class="box-title">Created <?php echo date('M j, Y', strtotime(date('Y-m-d')));?></h6>
</div>

<div class="box">
    <div class="box-body">
    	<!-- <h3 class="box-title">Revenue</h3> -->
    	<table class="table  table-striped table-condensed">
			<thead>
				<tr>
        			<th class="text-left">Customer</th>
					<th class="text-right">Income</th>
				</tr>
			</thead>
			<tbody>
				<?php echo $receivables?>
			</tbody>
		</table>


    </div>
</div>
