<?php

class Company_financial_model extends CI_Model
{

	public function get_income_value($transactionCategory)
	{
		//retrieve all users
		$this->db->from('v_general_ledger');
		$this->db->select('SUM(cr_amount) AS total_amount,transactionName');
		$this->db->where('transactionCategory = "'.$transactionCategory.'"');
		$this->db->group_by('transactionCode');
		$query = $this->db->get();

		return $query;
	}

	public function get_cog_value($transactionCategory)
	{
		//retrieve all users
		$this->db->from('v_general_ledger');
		$this->db->select('SUM(dr_amount) AS total_amount,accountName');
		$this->db->where('transactionCategory = "'.$transactionCategory.'" AND projectId > 0');
		$this->db->group_by('accountId');
		$query = $this->db->get();

		return $query;
	}


	public function get_operational_cost_value($transactionCategory)
	{
		//retrieve all users
		$this->db->from('v_general_ledger');
		$this->db->select('SUM(dr_amount) AS total_amount,accountName');
		$this->db->where('transactionCategory = "'.$transactionCategory.'" AND (projectId IS NULL OR projectId = 0)');
		$this->db->group_by('accountId');
		$query = $this->db->get();

		return $query;
	}


	public function get_payables_aging_report()
	{
		//retrieve all users
		$this->db->from('v_aged_payables');
		$this->db->select('*');
		// $this->db->where('');
		// $this->db->group_by('accountId');
		$query = $this->db->get();

		return $query;
	}

	public function get_receivables_aging_report()
	{
		//retrieve all users
		$this->db->from('v_aged_receivables');
		$this->db->select('*');
		// $this->db->where('');
		// $this->db->group_by('accountId');
		$query = $this->db->get();

		return $query;
	}



	public function get_account_value()
	{
		//retrieve all users
		$this->db->from('v_general_ledger');
		$this->db->select('(SUM(dr_amount) - SUM(cr_amount)) AS total_amount,accountName');
		$this->db->where('accountParentId = 1');
		$this->db->group_by('accountId');
		$query = $this->db->get();

		return $query;

	}


	public function get_accounts_receivables()
	{
		//retrieve all users
		$this->db->from('v_general_ledger');
		$this->db->select('(SUM(cr_amount) -SUM(dr_amount)) AS total_amount');
		$this->db->where('projectId > 0 AND (transactionCategory = "Revenue" OR transactionCategory = "Tenants Payments")');
		// $this->db->group_by('accountId');
		$query = $this->db->get();
		$query_row = $query->row();
		$total_invoices_balance = $query_row->total_amount;


		$this->db->from('v_general_ledger');
		$this->db->select('(SUM(cr_amount)) AS total_credit_note');
		$this->db->where('projectId > 0 AND transactionCategory = "Tenants Credit Notes"');
		// $this->db->group_by('accountId');
		$query = $this->db->get();
		$query_credit_row = $query->row();
		$total_credit_note = $query_credit_row->total_credit_note;

		return $total_invoices_balance - $total_credit_note;

	}
	public function get_accounts_payable()
	{
		//retrieve all users
		$this->db->from('v_general_ledger');
		$this->db->select('(SUM(dr_amount) - SUM(cr_amount)) AS total_amount');
		$this->db->where('recepientId > 0 AND (transactionClassification = "Creditors Invoices" OR transactionClassification = "Purchases")');
		// $this->db->group_by('accountId');
		$query = $this->db->get();
		$query_row = $query->row();
		$total_invoices_balance = $query_row->total_amount;


		$this->db->from('v_general_ledger');
		$this->db->select('(SUM(cr_amount)) AS total_payments');
		$this->db->where('recepientId > 0 AND transactionCategory = "Expense Payment"');
		// $this->db->group_by('accountId');
		$query = $this->db->get();
		$query_credit_row = $query->row();
		$total_payments = $query_credit_row->total_payments;

		return $total_invoices_balance - $total_payments;
	}


	public function get_total_wht_tax()
	{
		//retrieve all users
		$this->db->from('creditor_invoice_item');
		$this->db->select('SUM(vat_amount) AS total_amount');
		$this->db->where('vat_type_id = 2 AND vat_amount > 0');
		// $this->db->group_by('accountId');
		$query = $this->db->get();
		$query_row = $query->row();
		$total_invoices_balance = $query_row->total_amount;


		return $total_invoices_balance;

	}

	public function get_total_vat_tax()
	{
		//retrieve all users
		$this->db->from('creditor_invoice_item');
		$this->db->select('SUM(vat_amount) AS total_amount');
		$this->db->where('vat_type_id = 1 AND vat_amount > 0');
		// $this->db->group_by('accountId');
		$query = $this->db->get();
		$query_row = $query->row();
		$total_invoices_balance = $query_row->total_amount;




		return $total_invoices_balance;

	}

	public function get_all_vendors()
	{
		$this->db->from('creditor');
		$this->db->select('*');
		$this->db->where('creditor_id > 0');
		$query = $this->db->get();

		return $query;
	}

	public function get_creditor_expenses($creditor_id)
	{
		//retrieve all users
		$this->db->from('v_general_ledger');
		$this->db->select('(SUM(dr_amount) - SUM(cr_amount)) AS total_amount');
		$this->db->where('recepientId = '.$creditor_id.' AND (transactionClassification = "Creditors Invoices" OR transactionClassification = "Purchases")');
		// $this->db->group_by('accountId');
		$query = $this->db->get();
		$query_row = $query->row();
		$total_invoices_balance = $query_row->total_amount;

		$this->db->from('v_general_ledger');
		$this->db->select('(SUM(cr_amount)) AS total_payments');
		$this->db->where('recepientId = '.$creditor_id.'  AND transactionCategory = "Expense Payment"');
		// $this->db->group_by('accountId');
		$query = $this->db->get();
		$query_credit_row = $query->row();
		$total_payments = $query_credit_row->total_payments;

		return $total_invoices_balance - $total_payments;
	}



	public function get_all_properties()
	{
		$this->db->from('property');
		$this->db->select('*');
		$this->db->where('property_id > 0');
		$query = $this->db->get();

		return $query;
	}

	public function get_property_balances($property_id)
	{


		//retrieve all users
		$this->db->from('v_general_ledger');
		$this->db->select('(SUM(cr_amount) -SUM(dr_amount)) AS total_amount');
		$this->db->where('projectId = '.$property_id.' AND (transactionCategory = "Revenue" OR transactionCategory = "Tenants Payments")');
		// $this->db->group_by('accountId');
		$query = $this->db->get();
		$query_row = $query->row();
		$total_invoices_balance = $query_row->total_amount;


		$this->db->from('v_general_ledger');
		$this->db->select('(SUM(cr_amount)) AS total_credit_note');
		$this->db->where('projectId = '.$property_id.' AND transactionCategory = "Tenants Credit Notes"');
		// $this->db->group_by('accountId');
		$query = $this->db->get();
		$query_credit_row = $query->row();
		$total_credit_note = $query_credit_row->total_credit_note;

		return $total_invoices_balance - $total_credit_note;

	}

	public function get_total_receivable_wht_tax()
	{
		//retrieve all users
		$this->db->from('lease_invoice,invoice');
		$this->db->select('SUM(tax_amount) AS total_amount');
		$this->db->where('tax_amount > 0 AND lease_invoice.lease_invoice_id = invoice.lease_invoice_id');
		// $this->db->group_by('accountId');
		$query = $this->db->get();
		$query_row = $query->row();
		$total_invoices_balance = $query_row->total_amount;


		return $total_invoices_balance;

	}

	/*
	*	get a single creditor's details
	*	@param int $creditor_id
	*
	*/
	public function get_creditor($creditor_id)
	{
		//retrieve all users
		$this->db->from('creditor');
		$this->db->select('*');
		$this->db->where('creditor_id = '.$creditor_id);
		$query = $this->db->get();

		return $query;
	}


	public function get_creditor_statement($creditor_id,$start_date=null)
	{

		$search_status = $this->session->userdata('vendor_expense_search');
		$search_payments_add = '';
		$search_invoice_add = '';
		if($search_status == 1)
		{
			$date_from = $this->session->userdata('date_from_vendor_expense');
			$date_to = $this->session->userdata('date_to_vendor_expense');

			if(!empty($date_from) AND !empty($date_to))
			{
				$search_invoice_add =  ' AND (transactionDate >= \''.$date_from.'\' AND transactionDate <= \''.$date_to.'\') ';
			}
			else if(!empty($date_from))
			{
				$search_invoice_add = ' AND transactionDate = \''.$date_from.'\'';
			}
			else if(!empty($date_to))
			{
				$search_invoice_add = ' AND transactionDate = \''.$date_to.'\'';
			}
		}
		else
		{
			$search_invoice_add =  ' AND (transactionDate >= \''.date("Y-01-01").'\' AND transactionDate <= \''.date("Y-m-d").'\') ';

		}
		//retrieve all users
		$this->db->from('v_payable_ledger_by_date,creditor');
		$this->db->select('*');
		$this->db->where('creditor.creditor_id = v_payable_ledger_by_date.recepientId AND v_payable_ledger_by_date.transactionDate >= creditor.start_date AND recepientId = '.$creditor_id);
		$this->db->order_by('transactionDate','ASC');
		$query = $this->db->get();



		return $query;
	}


}
?>
