<?php
	if(count($contacts) > 0)
	{
		$email = $contacts['email'];
		$email2 = $contacts['email'];
		$facebook = $contacts['facebook'];
		$twitter = $contacts['twitter'];
		$linkedin = $contacts['linkedin'];
		$logo = $contacts['logo'];
		$building = $contacts['building'];
		$floor = $contacts['floor'];
		$location = $contacts['location'];
		$company_name = $contacts['company_name'];
		$phone = $contacts['phone'];
		
		if(!empty($facebook))
		{
			//$facebook = '<li class="facebook"><a href="'.$facebook.'" target="_blank" title="Facebook">Facebook</a></li>';
		}
		
	}
	else
	{
		$email = '';
		$facebook = '';
		$twitter = '';
		$linkedin = '';
		$logo = '';
		$company_name = '';
		$google = '';
	}
?>

				<!-- search -->
				<section id="header-search-panel">

					<div class="search">
						<div>
							<form action="index1.html#" id="header-search-form" method="get">
								<a class="close-search" href="index1.html#"><i class="ss-delete"></i></a>
								<input type="text" id="header-search" name="s" value="Start Typing & Hit Enter..." autocomplete="off" onFocus="if (this.value == 'Start Typing & Hit Enter...')this.value = '';" onBlur="if (this.value == '')this.value = 'Start Typing & Hit Enter...';" />
								<!-- Create a fake search button -->
								<span class="fake-submit-button">
									<input type="submit"  name="submit" value="submit" />
								</span>
							</form>
						</div>
					  <h6>&nbsp;</h6>
					</div>
				</section>
				<!--/ search -->

				<!-- top bar -->
				<div class="topbar"></div>

				<div class="container topbar-content">

					<div class="col-md-6">
						<ul>
							<li><i class="icon-clock-1"></i> We build great and powerfull strutures around the world</li>
						</ul>
					</div>
					<div class="col-md-6 right">
						<ul class="toplink">
							<li><a href="http://app.apexpayments.co.ke" target="_blank">Log in</a>
							</li>
							<li><a href="<?php echo site_url().'contact';?>">Feedback</a>
							</li>
							<!--<li><a href="index1.html#">Site Map</a>
							</li>
							<li class="last"><a href="index1.html#" id="header-search-button"><i class="icon-search-1"></i></a>
							</li>-->
						</ul>
					</div>
				</div>
				<!-- / top bar -->
				<!-- logo bar -->
				<div class="container header-nav">

					<div class="col-sm-3 col-xs-12">
						<div class="basic-wrapper">
							<a class="mobile-menu" data-toggle="collapse" data-target=".navbar-collapse"><i class='icon-menu'></i></a>
							<a class="logo" href="index1.html">
								<img src="<?php echo base_url()."assets/logo/".$logo;?>" alt="<?php echo $company_name;?>"/>
							</a>
						</div>
					</div>

					<div class="col-sm-9 col-xs-12">
						<ul class="top-contact">
							<li>
								<div class="icon">
									<i class="icon-phone-1"></i>
									<div class="content">
										<h3><?php echo $phone;?></h3>
										<p><?php echo $email;?></p>
									</div>
								</div>
							</li>

							<li>
								<div class="icon">
									<i class="icon-location"></i>
									<div class="content">
										<h3><?php echo $location;?></h3>
										<p><?php echo $building;?> <?php echo $floor;?></p>
									</div>
								</div>
							</li>

							<li>
								<div class="icon">
									<i class="icon-clock-1"></i>
									<div class="content">
										<h3>Mon - Fri 9:00 - 17:00</h3>
										<p>Sat - Sun CLOSED </p>
									</div>
								</div>
							</li>
						</ul>
					</div>

				</div>
				<!--/ logo bar -->

				<!-- nav bar -->
				<div class="bg-navbar"></div>

				<div class="container">

					<div class="collapse navbar-collapse right">

						<!-- navigation -->
						<ul class="nav navbar-nav">
							<?php echo $this->site_model->get_home_navigation();?>
						</ul>
						<!-- /navigation -->

					</div>
				</div>
				<!-- nav bar -->

			