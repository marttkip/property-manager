
        <!-- START MODAL -->
        <section class="modal fade modal-updates" role="dialog">
            <!-- The modal window opens when a user clicks button with class="btn-open" in "Updates" section -->
            <div class="modal-dialog">
                <div class="modal-content">
                    
                    <!-- START MODAL HEADER -->
                    <div class="modal-header">
                        <h2 class="modal-title"></h2>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <!-- END MODAL HEADER -->
                    
                    <!-- START MODAL BODY -->
                    <div class="modal-body">
                        <!-- JS CODE ADDS VIDEO HERE DYNAMICALLY -->
                    </div>
                    <!-- END MODAL BODY -->
                    
                </div>
            </div>
        </section>
        <!-- END MODAL -->
        
        <!-- START CONTACT SECTION -->
        <section class="section-contact" id="contacts">
            <div class="container">
                
                <!-- START SECTION HEADER -->
                <h2 class="reveal reveal-top">Contact Us</h2>
                <div class="subheading reveal reveal-top">
                    Send Us A message
                </div>
                <!-- END SECTION HEADER -->
                
                <div class="contact-form-wrapper reveal reveal-bottom">
                    
                    <!-- START CONTACT FORM -->
                    <form method="POST" action="#" accept-charset="UTF-8" class="contact-form" id="jvalidate" novalidate="novalidate">
                        
                        <div class="row">
                            <div class="col-sm-12 col-md-4">
                                <div class="field-holder">
                                    <input class="form-control input-lg field" placeholder="Names" name="name" type="text">
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-4">
                                <div class="field-holder">
                                    <input class="form-control input-lg field" placeholder="Email" name="email" type="email">
                                </div>
                            </div>
                            <div class="col-md-12 col-xs-12">
                                <div class="field-holder">
                                    <textarea class="form-control" placeholder="Message" name="message" cols="50" rows="10"></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-12">  
                                    <div class="alert alert-success" role="alert" style="display: none;">
                                        Message has been successfully sent.
                                    </div>
                                    <div class="alert alert-danger" role="alert" style="display: none;">
                                        <div class="alert-text">Server error</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <button class="btn btn-primary btn-contact" type="submit" value="SEND MESSAGE">SEND MESSAGE</button>
                            </div>
                        </div>
                    </form>
                    <!-- END CONTACT FORM -->
                    
                </div>

            </div>
        </section>
        <!-- END CONTACT SECTION -->
        