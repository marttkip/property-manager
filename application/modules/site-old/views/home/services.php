		<section class="section-light screenshots" id="screenshots">
            <div class="container-fluid">
                
                <!-- START SECTION HEADER -->
                <h2 class="reveal reveal-top">Screenshots</h2>
                <!-- END SECTION HEADER -->
                
                <!-- START WIDGET: CAROUSEL-->
                <div class="carousel-wrapper reveal reveal-bottom">
                    <div class="carousel-decoration" style="background-image: url('<?php echo base_url()."assets/themes/theme/";?>assets/img/decore/phone-black.png');"></div>

                    <div class="screenshot-carousel">
                        <!-- Set the URLs of your images to "background-image" attributes. Images set from remote server -->
                        <div class="image-holder" style="background-image: url('<?php echo base_url()."assets/themes/theme/";?>assets/img/decore/screnshot_1.jpg');"></div>
                        <div class="image-holder" style="background-image: url('<?php echo base_url()."assets/themes/theme/";?>assets/img/decore/screnshot_2.jpg');"></div>
                        <div class="image-holder" style="background-image: url('<?php echo base_url()."assets/themes/theme/";?>assets/img/decore/screnshot_3.jpg');"></div>
                        <div class="image-holder" style="background-image: url('<?php echo base_url()."assets/themes/theme/";?>assets/img/decore/screnshot_6.jpg');"></div>
                        <div class="image-holder" style="background-image: url('<?php echo base_url()."assets/themes/theme/";?>assets/img/decore/screnshot_4.jpg');"></div>
                        <div class="image-holder" style="background-image: url('<?php echo base_url()."assets/themes/theme/";?>assets/img/decore/screnshot_5.jpg');"></div>
                    </div>
                </div>
                <!-- END WIDGET: CAROUSEL-->

            </div>
        </section>
        