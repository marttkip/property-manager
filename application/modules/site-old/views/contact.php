 <?php
	if(count($contacts) > 0)
	{
		$email = $contacts['email'];
		$email2 = $contacts['email'];
		$facebook = $contacts['facebook'];
		$twitter = $contacts['twitter'];
		$linkedin = $contacts['linkedin'];
		$logo = $contacts['logo'];
		$company_name = $contacts['company_name'];
		$phone = $contacts['phone'];
		$address = $contacts['address'];
		$post_code = $contacts['post_code'];
		$city = $contacts['city'];
		$building = $contacts['building'];
		$floor = $contacts['floor'];
		$location = $contacts['location'];
		
		if(!empty($facebook))
		{
			//$facebook = '<li class="facebook"><a href="'.$facebook.'" target="_blank" title="Facebook">Facebook</a></li>';
		}
		
	}
	else
	{
		$email = '';
		$facebook = '';
		$twitter = '';
		$linkedin = '';
		$logo = '';
		$company_name = '';
		$google = '';
		$address = '';
		$post_code = '';
		$city = '';
		$building = '';
		$floor = '';
		$location = '';
	}
?>
			<!-- inner banner -->
            <section class="bg-white inner-banner pages">

                <div class="container pad-container">
                    <div class="col-md-12">
                        <h1><?php echo $title?></h1>
                    </div>
                </div>

            </section>

            <!-- intro section -->
            <section class="bg-white">
                <div class="container pad-container">

                    <div class="row col-md-12 text-center">
                        <h2 class="heading">Drop us a Line</h2>
                        <p>Feel free to get in touch with us.</p>
                    </div>

                    <div class="clearfix"></div>

                    <div class="row t-pad40">

                        <div class="col-md-6">

                            <div class="form-container">
                                <div class="response alert alert-success"></div>
                                <form id="contact-form" action="<?php echo site_url().'contact'?>" method="post">

                                    <div class="row b-mgr20">
                                        <div class="col-sm-12">
                                            <input name="sender_name" type="text" value="" placeholder="First Name *" />
                                        </div>
                                    </div>
                                    <div class="row b-mgr20">
                                        <div class="col-sm-12">
                                            <input name="email" type="text" value="" placeholder="Your Email Address" />
                                        </div>
                                    </div>
                                    <div class="row b-mgr20">
                                        <div class="col-sm-12">
                                            <textarea name="message" cols="40" rows="3" placeholder="Your Message"></textarea>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <input type="submit" class="btn-submit" value="Submit" />
                                        </div>
                                    </div>
                                    <p class="small">* We don’t share your information with anyone. Check out our <a href="#">Privacy Policy</a> for more details. </p>

                                </form>
                            </div>

                        </div>

                        <div class="col-md-6">

                            <div class="row l-pad40">

                                <div class="services-box-2">
                                    <i class="icon-picons-pin-2"></i>
                                    <div class="content">
                                        <p><?php echo $building;?> <?php echo $floor;?>, <?php echo $location;?>
                                            <br><?php echo $address;?>-<?php echo $post_code;?>, <?php echo $city;?>.</p>
                                    </div>
                                </div>

                                <div class="services-box-2 t-pad20">
                                    <i class="icon-picons-telephone-call"></i>
                                    <div class="content">
                                        <p><?php echo $phone;?></p>
                                    </div>
                                </div>

                                <div class="services-box-2 t-pad20">
                                    <i class="icon-picons-envelope"></i>
                                    <div class="content">
                                        <p><a href="mailto:<?php echo $email;?>"><?php echo $email;?></a>
                                        </p>
                                    </div>
                                </div>

                            </div>

                            <div class="row t-mgr30">

                                <div class="map-bg">

                                    <div class="map-wrapper">
										<iframe style="height:100%;width:100%;border:0;" height="300" frameborder="0" src="https://www.google.com/maps/embed/v1/place?q=Fogo+Gaucho,+Nairobi,+Nairobi+County,+Kenya&key=AIzaSyAN0om9mFmy1QN6Wf54tXAowK4eT0ZUPrU"></iframe>
                                    </div>
                                    <div class="container">
                                        <div class="row">
                                            <div class=" col-md-4 col-xs-offset-1 text-center">
                                                <p class="lead color-white fw600">GET IN TOUCH WITH US</p>
                                                <h3 class="color-white t-pad10">We 're located in <?php echo $building;?> <?php echo $floor;?>, <?php echo $location;?>, and are at your service</h3>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>
            </section>
            <!--/ intro section -->
             