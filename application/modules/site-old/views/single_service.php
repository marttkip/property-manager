			<!-- inner banner -->
            <section class="bg-white inner-banner pages">

                <div class="container pad-container">
                    <div class="col-md-12">
                        <h1><?php echo $title?></h1>
                    </div>
                </div>

            </section>

            <!-- leftside bar -->
            <section>

                <div class="container pad-container2">
				
                    <div class="row">

                        <div class="col-sm-4 sidebar">                           

                            <h3 class="title-left">Our Services</h3>
                            <ul class="circled">
								<?php
									$checking_items = '';
									if($services->num_rows() > 0)
									{   $count = 0;
										foreach($services->result() as $res)
										{
											$service_name = $res->service_name;
											$service_description = $res->service_description;
											$mini_desc = implode(' ', array_slice(explode(' ', $service_description), 0, 100));
											$maxi_desc = implode(' ', array_slice(explode(' ', $service_description), 0, 40));
											$web_name = $this->site_model->create_web_name($service_name);
				
											$count ++;
											
											if($web_name == $service_web_name)
											{
												echo '<li class="bg current">'.$service_name.'</li>';
											}
											
											else
											{
												echo '<li class="bg"><a href="'.site_url().'services/'.$web_name.'">'.$service_name.'</a></li>'; 
											}
											
										}
									}
								?>
                            </ul>
							<div class="sep30"></div>
                        </div>

                        <div class="col-sm-8 content">
							<?php
							if($services_item->num_rows() > 0)
							{   
								foreach($services_item->result() as $res_item)
								{   $service_name = $res_item->service_name;
									$service_description = $res_item->service_description;
									$service_id = $res_item->service_id;
									$created = $res_item->created;
								   
								}
							}
							?>
							<h3 class="heading"><?php echo $service_name;?></h3>

							<?php echo $service_description;?>
                        </div>

                    </div>

                </div>

            </section>
            <!--/ leftside bar -->