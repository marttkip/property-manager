<?php

class Accounts_model extends CI_Model
{
	/*
	*	Count all items from a table
	*	@param string $table
	* 	@param string $where
	*
	*/
	public function count_items($table, $where, $limit = NULL)
	{
		if($limit != NULL)
		{
			$this->db->limit($limit);
		}
		$this->db->from($table);
		$this->db->where($where);
		return $this->db->count_all_results();
	}

	/*
	*	Retrieve all tenants
	*	@param string $table
	* 	@param string $where
	*
	*/
	public function get_all_tenants($table, $where, $per_page, $page, $order, $order_method = 'ASC')
	{
		//retrieve all tenants
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		$query = $this->db->get('', $per_page, $page);

		return $query;
	}

	/*
	*	Retrieve all tenants
	*	@param string $table
	* 	@param string $where
	*
	*/
	public function get_all_unapproved_invoices($table, $where, $per_page, $page, $order, $order_method = 'ASC')
	{
		//retrieve all tenants
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		$this->db->group_by('invoice.lease_id');
		$query = $this->db->get('', $per_page, $page);

		return $query;
	}

	/*
	*	Retrieve all tenants
	*	@param string $table
	* 	@param string $where
	*
	*/
	public function get_all_lease_invoices($table, $where, $per_page, $page, $order, $order_method)
	{
		//retrieve all tenants
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		$this->db->group_by('invoice.invoice_month,invoice.invoice_year');
		$query = $this->db->get('', $per_page, $page);

		return $query;
	}
	/*
	*	Retrieve all tenants
	*	@param string $table
	* 	@param string $where
	*
	*/
	public function get_all_lease_payments($table, $where, $per_page, $page, $order, $order_method)
	{
		//retrieve all tenants
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		$this->db->group_by('payments.month,payments.year');
		$query = $this->db->get('', $per_page, $page);

		return $query;
	}
	public function get_all_owner_payments($table, $where, $per_page, $page, $order, $order_method)
	{
		//retrieve all tenants
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		$this->db->group_by('home_owners_payments.month,home_owners_payments.year');
		$query = $this->db->get('', $per_page, $page);

		return $query;
	}
	public function get_all_unapproved_payments($table, $where, $per_page, $page, $order, $order_method = 'ASC')
	{
		//retrieve all tenants
		$this->db->from($table);
		$this->db->select('*,payment_item.amount_paid AS paid_amount');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		$query = $this->db->get('', $per_page, $page);

		return $query;
	}

	/*
	*	Retrieve all tenants
	*	@param string $table
	* 	@param string $where
	*
	*/
	public function get_all_failed($table, $where, $per_page, $page, $order, $order_method = 'ASC')
	{
		//retrieve all tenants
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		$query = $this->db->get('', $per_page, $page);

		return $query;
	}

	/*
	*	Retrieve all tenants
	*	@param string $table
	* 	@param string $where
	*
	*/
	public function get_all_unit_owners($table, $where, $per_page, $page, $order, $order_method = 'ASC')
	{
		//retrieve all tenants
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		$query = $this->db->get('', $per_page, $page);

		return $query;
	}
	public function get_months_other_amount($lease_id,$month,$this_year)
	{

		$this->db->from('payments');
		$this->db->select('SUM(amount_paid) AS total_paid');
		$this->db->where('lease_id = '.$lease_id.' AND payment_status = 1 AND month = "'.$month.'" AND year ="'.$this_year.'"');
		$query = $this->db->get();
		$total_paid = 0;
		foreach ($query->result() as $key) {
			# code...
			$total_paid = $key->total_paid;
		}
		return $total_paid;
	}

	public function get_months_amount($lease_id)
	{

		$this->db->from('payments');
		$this->db->select('SUM(amount_paid) AS total_paid');
		$this->db->where('lease_id = '.$lease_id.' AND payment_status = 1 AND cancel = 0');

		$query = $this->db->get();
		$total_paid = 0;

		foreach ($query->result() as $key) {
			# code...
			$total_paid = $key->total_paid;
		}
		return $total_paid;
	}

	public function get_this_months_payment($lease_id,$month)
	{

		$this->db->from('payments');
		$this->db->select('*');
		$this->db->where('lease_id = '.$lease_id.' AND payment_status = 1 AND cancel = 0 AND month = "'.$month.'"');
		$query = $this->db->get();
		return $query;

	}
	public function get_total_payments_before($lease_id, $invoice_year,$invoice_month,$invoice_type_id = NULL)
	{
		if($invoice_type_id != NULL)
		{
			$add = 'AND payment_item.invoice_type_id = '.$invoice_type_id;
		}
		else
		{
			$add = '';
		}
		$date = $invoice_year.'-'.$invoice_month.'-01';
		$this->db->from('payment_item,payments');
		$this->db->select('SUM(payment_item.amount_paid) AS total_paid');
		$this->db->where('payments.lease_id = '.$lease_id.' AND payments.payment_id = payment_item.payment_id AND payments.payment_status = 1 AND cancel = 0 AND payments.payment_date < "'.$date.'" '.$add);
		$query = $this->db->get();
		$total_paid = 0;
		if($query->num_rows() >0)
		{
			$row=$query->row();
			$total_paid = $row->total_paid;
		}
		return $total_paid;
	}
	public function get_total_payments_after($lease_id, $invoice_year,$invoice_month,$invoice_type_id = NULL)
	{
		if($invoice_type_id != NULL)
		{
			$add = 'AND payment_item.invoice_type_id = '.$invoice_type_id;
		}
		else
		{
			$add = '';
		}
		$date = $invoice_year.'-'.$invoice_month.'-01';
		$this->db->from('payment_item,payments');
		$this->db->select('SUM(payment_item.amount_paid) AS total_paid');
		$this->db->where('payments.lease_id = '.$lease_id.' AND payments.payment_id = payment_item.payment_id AND payments.payment_status = 1 AND cancel = 0 AND payments.payment_date >= "'.$date.'" '.$add);
		$query = $this->db->get();
		$total_paid = 0;
		if($query->num_rows() >0)
		{
			$row=$query->row();
			$total_paid = $row->total_paid;
		}
		return $total_paid;
	}

	public function get_total_invoices_before($lease_id, $invoice_year,$invoice_month,$invoice_type_id = NULL)
	{
		if($invoice_type_id != NULL)
		{
			$add = 'AND invoice_type = '.$invoice_type_id;
		}
		else
		{
			$add = '';
		}
		$date = $invoice_year.'-'.$invoice_month.'-01';
		$where = 'lease_id = '.$lease_id.' AND invoice_date <= "'.$date.'"'.$add;
		// var_dump($where); die();


		$this->db->from('invoice');
		$this->db->select('SUM(invoice_amount) AS total_invoice');
		$this->db->where($where);
		$query = $this->db->get();
		$total_invoice = 0;
		if($query->num_rows() >0)
		{
			$row=$query->row();
			$total_invoice = $row->total_invoice;
		}
		return $total_invoice;
	}


	public function get_total_invoices_before_payment($lease_id, $payment_date,$invoice_type_id = NULL)
	{
		if($invoice_type_id != NULL)
		{
			$add = 'AND invoice.invoice_type = '.$invoice_type_id;
		}
		else
		{
			$add = '';
		}
		// $date = $invoice_year.'-'.$invoice_month.'-01';
		$where = 'lease_invoice.lease_id = '.$lease_id.' AND lease_invoice.lease_invoice_id = invoice.lease_invoice_id AND lease_invoice.invoice_deleted = 0 AND invoice.invoice_item_deleted = 0 AND lease_invoice.invoice_date < "'.$payment_date.'"'.$add;
		// var_dump($where); die();


		$this->db->from('invoice,lease_invoice');
		$this->db->select('SUM(invoice_amount) AS total_invoice');
		$this->db->where($where);
		$query = $this->db->get();
		$total_invoice = 0;
		if($query->num_rows() >0)
		{
			$row=$query->row();
			$total_invoice = $row->total_invoice;
		}
		return $total_invoice;
	}

	public function get_total_payments_before_payment($lease_id, $payment_date,$invoice_type_id)
	{
		if($invoice_type_id != NULL)
		{
			$add = 'AND payment_item.invoice_type_id = '.$invoice_type_id;
		}
		else
		{
			$add = '';
		}
		// $date = $invoice_year.'-'.$invoice_month.'-01';
		$where = 'payment_item.payment_id = payments.payment_id AND cancel = 0 AND payments.lease_id = '.$lease_id.' AND payments.payment_date < "'.$payment_date.'"'.$add;
		// var_dump($where); die();


		$this->db->from('payments,payment_item');
		$this->db->select('SUM(payment_item.amount_paid) AS total_payments');
		$this->db->where($where);
		$query = $this->db->get();
		$total_payments = 0;
		if($query->num_rows() >0)
		{
			$row=$query->row();
			$total_payments = $row->total_payments;
		}
		return $total_payments;
	}

	public function get_total_payments_before_payment_lease($lease_id, $payment_date,$invoice_type_id,$payment_id = null)
	{
		if($invoice_type_id != NULL)
		{
			$add = 'AND payment_item.invoice_type_id = '.$invoice_type_id;
		}
		else
		{
			$add = '';
		}

		if(!empty($payment_id))
		{
			$add .= ' AND payment_id < '.$payment_id;
		}
		// $date = $invoice_year.'-'.$invoice_month.'-01';
		$where = 'cancel = 0 AND payments.lease_id = '.$lease_id.' AND payments.payment_date <= "'.$payment_date.'"'.$add;
		// var_dump($where); die();


		$this->db->from('payments');
		$this->db->select('SUM(payments.amount_paid) AS total_payments');
		$this->db->where($where);
		$query = $this->db->get();
		$total_payments = 0;
		if($query->num_rows() >0)
		{
			$row=$query->row();
			$total_payments = $row->total_payments;
		}
		return $total_payments;
	}



	// owners
	public function get_total_owners_invoices_before_payment($rental_unit_id, $payment_date,$invoice_type_id = NULL)
	{
		if($invoice_type_id != NULL)
		{
			$add = 'AND invoice_type = '.$invoice_type_id;
		}
		else
		{
			$add = '';
		}
		// $date = $invoice_year.'-'.$invoice_month.'-01';
		$where = 'rental_unit_id = '.$rental_unit_id.' AND invoice_date < "'.$payment_date.'"'.$add;
		// var_dump($where); die();


		$this->db->from('home_owners_invoice');
		$this->db->select('SUM(invoice_amount) AS total_invoice');
		$this->db->where($where);
		$query = $this->db->get();
		$total_invoice = 0;
		if($query->num_rows() >0)
		{
			$row=$query->row();
			$total_invoice = $row->total_invoice;
		}
		return $total_invoice;
	}

	public function get_total_owners_payments_before_payment($rental_unit_id, $payment_date,$invoice_type_id = NULL)
	{
		if($invoice_type_id != NULL)
		{
			$add = 'AND home_owner_payment_item.invoice_type_id = '.$invoice_type_id;
		}
		else
		{
			$add = '';
		}
		// $date = $invoice_year.'-'.$invoice_month.'-01';
		$where = 'home_owner_payment_item.payment_id = home_owners_payments.payment_id AND cancel = 0  AND home_owners_payments.rental_unit_id = '.$rental_unit_id.' AND home_owners_payments.payment_date < "'.$payment_date.'"'.$add;
		// var_dump($where); die();


		$this->db->from('home_owners_payments,home_owner_payment_item');
		$this->db->select('SUM(home_owner_payment_item.amount_paid) AS total_payments');
		$this->db->where($where);
		$query = $this->db->get();
		$total_payments = 0;
		if($query->num_rows() >0)
		{
			$row=$query->row();
			$total_payments = $row->total_payments;
		}
		return $total_payments;
	}
	public function get_total_owners_payments_after_payment($rental_unit_id, $payment_date,$invoice_type_id = NULL)
	{
		if($invoice_type_id != NULL)
		{
			$add = 'AND home_owner_payment_item.invoice_type_id = '.$invoice_type_id;
		}
		else
		{
			$add = '';
		}
		// $date = $invoice_year.'-'.$invoice_month.'-01';
		$where = 'home_owner_payment_item.payment_id = home_owners_payments.payment_id AND cancel = 0 AND home_owners_payments.rental_unit_id = '.$rental_unit_id.' AND home_owners_payments.payment_date >= "'.$payment_date.'"'.$add;
		// var_dump($where); die();


		$this->db->from('home_owners_payments,home_owner_payment_item');
		$this->db->select('SUM(home_owner_payment_item.amount_paid) AS total_payments');
		$this->db->where($where);
		$query = $this->db->get();
		$total_payments = 0;
		if($query->num_rows() >0)
		{
			$row=$query->row();
			$total_payments = $row->total_payments;
		}
		return $total_payments;
	}

	// owner


	public function get_total_payments_end($lease_id, $invoice_year,$invoice_month,$invoice_type_id = NULL)
	{
		if($invoice_type_id != NULL)
		{
			$add = 'AND payment_item.invoice_type_id = '.$invoice_type_id;
		}
		else
		{
			$add = '';
		}

		// $date = $invoice_year.'-'.$invoice_month.'-30';
		$date = $invoice_year.'-'.$invoice_month.'-30';
		// var_dump($date); die();
		$where = 'payments.lease_id = '.$lease_id.' AND payments.payment_id = payment_item.payment_id AND payments.payment_status = 1 AND cancel = 0 AND payments.payment_date < "'.$date.'" '.$add;

		// var_dump($where); die();

		$this->db->from('payment_item,payments');
		$this->db->select('SUM(payment_item.amount_paid) AS total_paid');
		$this->db->where($where);
		$query = $this->db->get();
		$total_paid = 0;
		if($query->num_rows() >0)
		{
			$row=$query->row();
			$total_paid = $row->total_paid;
		}
		return $total_paid;
	}

	public function get_total_invoices_end($lease_id, $invoice_year,$invoice_month,$invoice_type_id = NULL)
	{
		if($invoice_type_id != NULL)
		{
			$add = 'AND invoice_type = '.$invoice_type_id;
		}
		else
		{
			$add = '';
		}
		$date = $invoice_year.'-'.$invoice_month.'-30';
		$where = 'lease_id = '.$lease_id.' AND invoice_date <= "'.$date.'"'.$add;

		// if($invoice_type_id == 12)
		// {

		// var_dump($where); die();
		// }


		$this->db->from('invoice');
		$this->db->select('SUM(invoice_amount) AS total_invoice');
		$this->db->where($where);
		$query = $this->db->get();
		$total_invoice = 0;
		if($query->num_rows() >0)
		{
			$row=$query->row();
			$total_invoice = $row->total_invoice;
		}
		return $total_invoice;
	}



	public function get_latest_invoice_amount_type($lease_id,$year,$month,$invoice_type)
	{

		$this->db->from('invoice');
		$this->db->select('SUM(invoice_amount) AS total_invoice');
		$this->db->where('lease_id = '.$lease_id.' AND invoice_month = "'.$month.'" AND invoice_year = "'.$year.'" AND invoice_type ='.$invoice_type);
		$query = $this->db->get();
		$total_invoice = 0;
		if($query->num_rows() >0)
		{
			$row=$query->row();
			$total_invoice = $row->total_invoice;
		}
		return $total_invoice;

	}

	public function get_latest_invoice_amount($lease_id,$year,$month)
	{

		$this->db->from('invoice');
		$this->db->select('SUM(invoice_amount) AS total_invoice');
		$this->db->where('lease_id = '.$lease_id.' AND invoice_month = "'.$month.'" AND invoice_year = "'.$year.'"');
		$query = $this->db->get();
		$total_invoice = 0;
		if($query->num_rows() >0)
		{
			$row=$query->row();
			$total_invoice = $row->total_invoice;
		}
		return $total_invoice;

	}

	public function get_latest_home_owner_invoice_amount($rental_unit_id,$year,$month)
	{

		$this->db->from('home_owners_invoice');
		$this->db->select('SUM(invoice_amount) AS total_invoice');
		$this->db->where('rental_unit_id = '.$rental_unit_id.' AND invoice_month = "'.$month.'" AND invoice_year = "'.$year.'"');
		$query = $this->db->get();
		$total_invoice = 0;
		if($query->num_rows() >0)
		{
			$row=$query->row();
			$total_invoice = $row->total_invoice;
		}
		return $total_invoice;

	}

	// home owners

	public function get_total_owners_payments_before($rental_unit_id, $invoice_year, $invoice_month,$invoice_type_id = NULL)
	{
		if($invoice_type_id != NULL)
		{
			$add = 'AND home_owner_payment_item.invoice_type_id = '.$invoice_type_id;
		}
		else
		{
			$add = '';
		}
		$date = $invoice_year.'-'.$invoice_month.'-01';
		// var_dump($date);die();
		$where = 'home_owners_payments.rental_unit_id = '.$rental_unit_id.' AND home_owners_payments.payment_id = home_owner_payment_item.payment_id AND home_owners_payments.payment_status = 1 AND cancel = 0 AND home_owners_payments.payment_date < "'.$date.'" '.$add;
		// var_dump($where);die();
		$this->db->from('home_owner_payment_item,home_owners_payments');
		$this->db->select('SUM(home_owner_payment_item.amount_paid) AS total_paid');
		$this->db->where($where);
		$query = $this->db->get();
		$total_paid = 0;
		if($query->num_rows() >0)
		{
			$row=$query->row();
			$total_paid = $row->total_paid;
		}
		if(empty($total_paid))
		{
			$total_paid = 0;
		}

		return $total_paid;
	}

	public function get_total_owners_invoices_before($rental_unit_id, $invoice_year,$invoice_month,$invoice_type_id = NULL)
	{
		if($invoice_type_id != NULL)
		{
			$add = 'AND invoice_type = '.$invoice_type_id;
		}
		else
		{
			$add = '';

		}
		$date = $invoice_year.'-'.$invoice_month.'-01';
		$where = 'rental_unit_id = '.$rental_unit_id.' AND invoice_date < "'.$date.'"'.$add;
		// var_dump($where); die();
		$this->db->from('home_owners_invoice');
		$this->db->select('SUM(invoice_amount) AS total_invoice');
		$this->db->where($where);
		$query = $this->db->get();
		$total_invoice = 0;
		if($query->num_rows() >0)
		{
			$row=$query->row();
			$total_invoice = $row->total_invoice;
		}
		if(empty($total_invoice))
		{
			$total_invoice = 0;
		}

		return $total_invoice;
	}

	public function get_latest_owners_invoice_amount($rental_unit_id,$year,$month)
	{
		$where = 'rental_unit_id = '.$rental_unit_id.' AND invoice_month = "'.$month.'" AND invoice_year = "'.$year.'"';
		// var_dump($where); die();
		$this->db->from('home_owners_invoice');
		$this->db->select('SUM(invoice_amount) AS total_invoice');
		$this->db->where($where);
		$query = $this->db->get();
		$total_invoice = 0;
		if($query->num_rows() >0)
		{
			$row=$query->row();
			$total_invoice = $row->total_invoice;
		}
		if(empty($total_invoice))
		{
			$total_invoice = 0;
		}
		return $total_invoice;

	}

	// end of home owners


	public function get_months_last_amount($lease_id,$rent_amount,$arrears_bf,$lease_start_date)
	{
		$this->db->from('payments');
		// $this->db->select('balance_cf AS balance');
		$this->db->select('SUM(amount_paid) AS total_paid');
		$this->db->where('lease_id = '.$lease_id.' AND cancel = 0 AND payment_status = 1');
		$this->db->order_by('lease_id', 'DESC');
		$this->db->limit(1);
		$query = $this->db->get();
		$total_paid = 0;

		foreach ($query->result() as $key) {
			# code...
			$total_paid = $key->total_paid;
		}
		if($total_paid == NULL)
		{
			$total_paid = 0;
		}
		// var_dump($lease_id);die();

		$date1 = $lease_start_date;

		$date2 = date('Y-m-d');

		$ts1 = strtotime($date1);
		$ts2 = strtotime($date2);

		$year1 = date('Y',$ts1);
		$year2 = date('Y',$ts2);

		$month1 = date('m',$ts1);
		$month2 = date('m',$ts2);
		$total_months_comb = $month2 - $month2;
		$total_months = ($year2-$year1) * 12 + $total_months_comb;


		if($total_months == 0)
		{
			$total_months = 1;
		}
		$total_months = $total_months+1;
		// var_dump($total_months);

		if($total_months > 1)
		{
			$balance = $rent_amount - $total_paid;
		}
		else
		{
			$balance = $arrears_bf - $total_paid;
		}
		return $balance;
	}

	public function get_cancel_actions()
	{
		$this->db->where('cancel_action_status', 1);
		$this->db->order_by('cancel_action_name');

		return $this->db->get('cancel_action');
	}
	public function get_lease_payments($lease_id,$limit=null)
	{
		$this->db->where('leases.lease_id = payments.lease_id AND payments.lease_id = '.$lease_id.'  AND payments.cancel = 0');
		if($limit)
		{
			$this->db->limit($limit);
		}
		$this->db->order_by('payments.payment_date','DESC');
		$this->db->join('payment_method','payment_method.payment_method_id = payments.payment_method_id','left');

		return $this->db->get('payments,leases');
	}
	public function get_lease_mpesa_payments($mpesa_id,$limit=null)
	{
		$this->db->where('leases.lease_id = payments.lease_id AND payment_item.mpesa_id = '.$mpesa_id.'  AND payments.cancel = 0 AND payment_item.payment_id = payments.payment_id');
		if($limit)
		{
			$this->db->limit($limit);
		}
		$this->db->order_by('payments.payment_date','DESC');
		$this->db->join('payment_method','payment_method.payment_method_id = payments.payment_method_id','left');

		return $this->db->get('payments,leases,payment_item');
	}
	public function get_lease_pardons($lease_id)
	{
		$this->db->where('leases.lease_id = pardon_payments.lease_id AND pardon_payments.lease_id = '.$lease_id.' AND pardon_payments.pardon_delete = 0 AND YEAR(pardon_payments.pardon_date) <= '.date('Y'));

		return $this->db->get('pardon_payments,leases');
	}
	public function get_lease_pardons_owner($rental_unit_id)
	{
		$this->db->where('rental_unit_id = '.$rental_unit_id);

		return $this->db->get('pardon_owner_payments');
	}
	public function get_lease_payments_owners($rental_unit_id)
	{
		$this->db->where('rental_unit.rental_unit_id = home_owners_payments.rental_unit_id  AND home_owners_payments.cancel = 0  AND home_owners_payments.rental_unit_id = '.$rental_unit_id.' AND rental_unit.rental_unit_status = 1 ');

		return $this->db->get('home_owners_payments,rental_unit');
	}
	public function receipt_payment($lease_id,$personnel_id = NULL){
		$amount = $this->input->post('amount_paid');
		$payment_method=$this->input->post('payment_method');
		$type_of_account=$this->input->post('type_of_account');



		if($payment_method == 1)
		{
			$transaction_code = $this->input->post('bank_name');
		}
		else if($payment_method == 5)
		{
			//  check for mpesa code if inserted
			$transaction_code = $this->input->post('mpesa_code');
		}
		else
		{
			$transaction_code = '';
		}

		// calculate the points to get
		$payment_date = $this->input->post('payment_date');


		// $this->get_points_to_award($lease_id,$payment_date);
		// end of point calculation
		$date_check = explode('-', $payment_date);
		$month = $date_check[1];
		$year = $date_check[0];

		if($type_of_account == 1)
		{
			$receipt_number = $this->create_receipt_number();
		}
		else
		{
			$receipt_number = $this->create_owners_receipt_number();
		}

		$data = array(
			'payment_method_id'=>$payment_method,
			'amount_paid'=>$amount,
			'personnel_id'=>$this->session->userdata("personnel_id"),
			'transaction_code'=>$transaction_code,
			'payment_date'=>$this->input->post('payment_date'),
			'receipt_number'=>$receipt_number,
			'document_number'=>$receipt_number,
			'paid_by'=>$this->input->post('paid_by'),
			'payment_created'=>date("Y-m-d"),
			'year'=>$year,
			'month'=>$month,
			'confirm_number'=> $receipt_number,
			'payment_created_by'=>$this->session->userdata("personnel_id"),
			'approved_by'=>$personnel_id,'date_approved'=>date('Y-m-d')
		);

		if($type_of_account == 1)
		{
			$data['lease_id'] = $lease_id;

			if($this->db->insert('payments', $data))
			{

				$payment_id = $this->db->insert_id();

				// send email and sms for receipting
				// $this->send_receipt_notification($lease_id,$confirm_number,$payment_id,$amount);

				$service_charge_amount = $this->input->post('service_charge_amount');
				$water_amount = $this->input->post('water_amount');
				$rent_amount = $this->input->post('rent_amount');
				$penalty_fee = $this->input->post('penalty_fee');
				$fixed_charge = $this->input->post('fixed_charge');
				$bought_water = $this->input->post('bought_water');
				$insurance = $this->input->post('insurance');
				$sinking_funds = $this->input->post('sinking_funds');
				$painting_charge = $this->input->post('painting_charge');
				$fixed_charge = $this->input->post('fixed_charge');
				$deposit_charge = $this->input->post('deposit_charge');
				$legal_fees = $this->input->post('legal_fees');

				$invoice_number = '';//$this->get_invoice_number();

				if(!empty($service_charge_amount))
				{
					// insert for service charge
					$service = array(
										'payment_id'=>$payment_id,
										'amount_paid'=> $service_charge_amount,
										'invoice_type_id' => 4,
										'payment_item_status' => 1,
										'lease_id' => $lease_id,
										'payment_item_created' => date('Y-m-d')
									);
					$this->db->insert('payment_item',$service);
				}

				if(!empty($water_amount))
				{
					$service = array(
										'payment_id'=>$payment_id,
										'amount_paid'=> $water_amount,
										'invoice_type_id' => 2,
										'payment_item_status' => 1,
										'lease_id' => $lease_id,
										'payment_item_created' => date('Y-m-d')
									);
					$this->db->insert('payment_item',$service);
					// update the invoice table
					// using invoice_type_id
					$invoice_id = $this->get_last_invoice_id(2);



				}
				if(!empty($rent_amount))
				{
					$service = array(
										'payment_id'=>$payment_id,
										'amount_paid'=> $rent_amount,
										'invoice_type_id' => 1,
										'lease_id' => $lease_id,
										'payment_item_status' => 1,
										'payment_item_created' => date('Y-m-d')
									);
					$this->db->insert('payment_item',$service);


				}
				if(!empty($penalty_fee))
				{
					$service = array(
										'payment_id'=>$payment_id,
										'amount_paid'=> $penalty_fee,
										'invoice_type_id' => 1,
										'payment_item_status' => 1,
										'payment_item_created' => date('Y-m-d')
									);
					$this->db->insert('payment_item',$service);
				}
				if(!empty($fixed_charge))
				{
					$service = array(
										'payment_id'=>$payment_id,
										'amount_paid'=> $fixed_charge,
										'invoice_type_id' => 12,
										'payment_item_status' => 1,
										'payment_item_created' => date('Y-m-d')
									);
					$this->db->insert('payment_item',$service);
				}
				if(!empty($deposit_charge))
				{
					$service = array(
										'payment_id'=>$payment_id,
										'amount_paid'=> $deposit_charge,
										'lease_id' => $lease_id,
										'invoice_type_id' => 13,
										'payment_item_status' => 1,
										'payment_item_created' => date('Y-m-d')
									);
					$this->db->insert('payment_item',$service);
				}

				if(!empty($bought_water))
				{
					$service = array(
										'payment_id'=>$payment_id,
										'amount_paid'=> $bought_water,
										'lease_id' => $lease_id,
										'invoice_type_id' => 10,
										'payment_item_status' => 1,
										'payment_item_created' => date('Y-m-d')
									);
					$this->db->insert('payment_item',$service);
				}
				if(!empty($sinking_funds))
				{
					$service = array(
										'payment_id'=>$payment_id,
										'amount_paid'=> $sinking_funds,
										'invoice_type_id' => 8,
										'payment_item_status' => 1,
										'lease_id' => $lease_id,
										'payment_item_created' => date('Y-m-d')
									);
					$this->db->insert('payment_item',$service);
				}
				if(!empty($painting_charge))
				{
					$service = array(
										'payment_id'=>$payment_id,
										'amount_paid'=> $painting_charge,
										'lease_id' => $lease_id,
										'invoice_type_id' => 7,
										'payment_item_status' => 1,
										'payment_item_created' => date('Y-m-d')
									);
					$this->db->insert('payment_item',$service);
				}
				if(!empty($painting_charge))
				{
					$service = array(
										'payment_id'=>$payment_id,
										'amount_paid'=> $painting_charge,
										'lease_id' => $lease_id,
										'invoice_type_id' => 7,
										'payment_item_status' => 1,
										'payment_item_created' => date('Y-m-d')
									);
					$this->db->insert('payment_item',$service);
				}
				if(!empty($legal_fees))
				{
					$service = array(
										'payment_id'=>$payment_id,
										'amount_paid'=> $legal_fees,
										'lease_id' => $lease_id,
										'invoice_type_id' => 17,
										'payment_item_status' => 1,
										'payment_item_created' => date('Y-m-d')
									);
					$this->db->insert('payment_item',$service);
				}


				return TRUE;
			}
			else{
				return FALSE;
			}
		}
		else if($type_of_account == 0)
		{
			$data['rental_unit_id'] = $lease_id;

			if($this->db->insert('home_owners_payments', $data))
			{

				$payment_id = $this->db->insert_id();

				// send email and sms for receipting
				// $this->send_receipt_notification($lease_id,$confirm_number,$payment_id,$amount);

				$service_charge_amount = $this->input->post('service_charge_amount');
				$water_amount = $this->input->post('water_amount');
				$rent_amount = $this->input->post('rent_amount');
				$penalty_fee = $this->input->post('penalty_fee');

				$invoice_number = '';//$this->get_invoice_number();

				if(!empty($service_charge_amount))
				{
					// insert for service charge
					$service = array(
										'payment_id'=>$payment_id,
										'amount_paid'=> $service_charge_amount,
										'invoice_type_id' => 4,
										'payment_item_status' => 1,
										'payment_item_created' => date('Y-m-d')
									);
					$this->db->insert('home_owner_payment_item',$service);
				}

				if(!empty($water_amount))
				{
					$service = array(
										'payment_id'=>$payment_id,
										'amount_paid'=> $water_amount,
										'invoice_type_id' => 2,
										'payment_item_status' => 1,
										'payment_item_created' => date('Y-m-d')
									);
					$this->db->insert('home_owner_payment_item',$service);
					// update the invoice table
					// using invoice_type_id
					$invoice_id = $this->get_last_invoice_id(2);



				}
				if(!empty($rent_amount))
				{
					$service = array(
										'payment_id'=>$payment_id,
										'amount_paid'=> $rent_amount,
										'invoice_type_id' => 1,
										'payment_item_status' => 1,
										'payment_item_created' => date('Y-m-d')
									);
					$this->db->insert('home_owner_payment_item',$service);


				}
				if(!empty($penalty_fee))
				{
					$service = array(
										'payment_id'=>$payment_id,
										'amount_paid'=> $penalty_fee,
										'invoice_type_id' => 5,
										'payment_item_status' => 1,
										'invoice_number' => $invoice_number,
										'payment_item_created' => date('Y-m-d')
									);
					$this->db->insert('home_owner_payment_item',$service);
				}
				if(!empty($bought_water))
				{
					$service = array(
										'payment_id'=>$payment_id,
										'amount_paid'=> $bought_water,
										'invoice_type_id' => 10,
										'payment_item_status' => 1,
										'invoice_number' => $invoice_number,
										'payment_item_created' => date('Y-m-d')
									);
					$this->db->insert('home_owner_payment_item',$service);
				}
				if(!empty($painting_charge))
				{
					$service = array(
										'payment_id'=>$payment_id,
										'amount_paid'=> $painting_charge,
										'invoice_type_id' => 7,
										'payment_item_status' => 1,
										'invoice_number' => $invoice_number,
										'payment_item_created' => date('Y-m-d')
									);
					$this->db->insert('home_owner_payment_item',$service);
				}
				if(!empty($sinking_funds))
				{
					$service = array(
										'payment_id'=>$payment_id,
										'amount_paid'=> $sinking_funds,
										'invoice_type_id' => 8,
										'payment_item_status' => 1,
										'invoice_number' => $invoice_number,
										'payment_item_created' => date('Y-m-d')
									);
					$this->db->insert('home_owner_payment_item',$service);
				}
				if(!empty($insurance))
				{
					$service = array(
										'payment_id'=>$payment_id,
										'amount_paid'=> $insurance,
										'invoice_type_id' => 9,
										'payment_item_status' => 1,
										'invoice_number' => $invoice_number,
										'payment_item_created' => date('Y-m-d')
									);
					$this->db->insert('home_owner_payment_item',$service);
				}
				if(!empty($fixed_charge))
				{
					$service = array(
										'payment_id'=>$payment_id,
										'amount_paid'=> $fixed_charge,
										'invoice_type_id' => 9,
										'payment_item_status' => 1,
										'invoice_number' => $invoice_number,
										'payment_item_created' => date('Y-m-d')
									);
					$this->db->insert('home_owner_payment_item',$service);
				}
				return TRUE;
			}
			else{
				return FALSE;
			}
		}
	}

	public function receipt_pardon($lease_id)
	{
		$table = "pardon_payments";
		$prefix = "PA-".date('Y')."-";
		$column_name = "document_number";

		$document_number = $this->create_document_numbers($table,$prefix,$column_name);
		$data = array(
			'pardon_date'=>$this->input->post('pardon_date'),
			'created_by'=>$this->session->userdata("personnel_id"),
			'document_number'=>$document_number,
			'pardon_reason'=>$this->input->post('pardon_reason'),
			'pardon_amount'=>$this->input->post('pardon_amount')
		);

		$data['lease_id'] = $lease_id;
		// var_dump($data); die();
		if($this->db->insert('pardon_payments', $data))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	public function receipt_owner_pardon($home_owner_unit_id, $rental_unit_id)
	{
		$table = "pardon_owner_payments";
		$prefix = "HAP-PP-";
		$column_name = "document_number";

		$document_number = $this->create_document_numbers($table,$prefix,$column_name);
		$data = array(
			'pardon_date'=>$this->input->post('pardon_date'),
			'created_by'=>$this->session->userdata("personnel_id"),
			'document_number'=>$document_number,
			'pardon_reason'=>$this->input->post('pardon_reason'),
			'pardon_amount'=>$this->input->post('pardon_amount')
		);

		$data['rental_unit_id'] = $rental_unit_id;
		$data['home_owner_unit_id'] = $home_owner_unit_id;
		// var_dump($data); die();
		if($this->db->insert('pardon_owner_payments', $data))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	public function get_last_invoice_id($invoice_type_id)
	{
		$this->db->from('invoice');
		$this->db->select('invoice_id');
		$this->db->where('invoice_id ='.$invoice_type_id);
		$this->db->limit(1);
		$this->db->order_by('invoice_id','DESC');
		$invoice_query = $this->db->get();
		if($invoice_query->num_rows() > 0)
		{
			foreach ($invoice_query->result() as $key) {
				# code...
				$invoice_id = $key->invoice_id;
			}
		}
		else
		{
			$invoice_id = 0;
		}
	}
	public function get_invoice_number()
	{
		//select product code
		$preffix = $this->session->userdata('branch_code');
		$this->db->from('invoice');
		$this->db->where("invoice_number LIKE '".$preffix."%'");
		$this->db->select('MAX(invoice_number) AS number');
		$query = $this->db->get();//echo $query->num_rows();

		if($query->num_rows() > 0)
		{
			$result = $query->result();
			$number =  $result[0]->number;
			$real_number = str_replace($preffix, "", $number);
			$real_number++;//go to the next number
			$number = $preffix.sprintf('%03d', $real_number);
		}
		else{//start generating receipt numbers
			$number = $preffix.sprintf('%03d', 1);
		}

		return $number;
	}
	function create_receipt_number()
	{
		//select product code
		$preffix = "HA-RT-";
		$this->db->from('payments');
		$this->db->where("payment_id > 0");
		$this->db->select('MAX(document_number) AS number');
		$query = $this->db->get();//echo $query->num_rows();

		if($query->num_rows() > 0)
		{
			$result = $query->result();
			$number =  $result[0]->number;

			$number++;//go to the next number
		}
		else{//start generating receipt numbers
			$number = 1;
		}

		return $number;
	}
	function create_document_numbers($table,$preffix,$column_name)
	{
		//select product code
		$this->db->from(''.$table.'');
		$this->db->where("".$column_name." LIKE '".$preffix."%'");
		$this->db->select('MAX('.$column_name.') AS number');
		$query = $this->db->get();//echo $query->num_rows();

		if($query->num_rows() > 0)
		{
			$result = $query->result();
			$number =  $result[0]->number;
			$real_number = str_replace($preffix, "", $number);
			$real_number++;//go to the next number
			$number = $preffix.sprintf('%03d', $real_number);
		}
		else{//start generating receipt numbers
			$number = $preffix.sprintf('%03d', 1);
		}

		return $number;
	}

	function create_owners_receipt_number()
	{
		//select product code
		$preffix = "HA-RO-";
		$this->db->from('home_owners_payments');
		$this->db->where("receipt_number LIKE '".$preffix."%'  AND payment_status = 1");
		$this->db->select('MAX(receipt_number) AS number');
		$query = $this->db->get();//echo $query->num_rows();

		if($query->num_rows() > 0)
		{
			$result = $query->result();
			$number =  $result[0]->number;
			$real_number = str_replace($preffix, "", $number);
			$real_number++;//go to the next number
			$number = $preffix.sprintf('%03d', $real_number);
		}
		else{//start generating receipt numbers
			$number = $preffix.sprintf('%03d', 1);
		}

		return $number;
	}
	function get_points_to_award($lease_id,$payment_date)
	{
		$points_date = explode('-', $payment_date);
		$checked_date = $payment_date[2];
		$checked_month = $payment_date[1];


		// check for payments on the lease on that month
		$this->db->from('leases');
		$this->db->select('tenant_unit_id');
		$this->db->where('lease_status = 1 AND lease_id ='.$lease_id);
		$leases_query = $this->db->get();

		if($leases_query->num_rows() > 0)
		{

			foreach ($leases_query->result() as $value) {
				# code...
				$tenant_unit_id = $value->tenant_unit_id;

				// get the tenant_unit value

				$this->db->from('tenant_unit');
				$this->db->select('points');
				$tenant_unit_query = $this->db->get();

				if($tenant_unit_query->num_rows() > 0)
				{
					foreach ($tenant_unit_query->result() as $items) {
						# code...
						$old_points = $items->points;
					}
					// update
				}
				else
				{
					$old_points = 0;
				}
			}
		}
		else
		{
			$old_points = 0;
		}
		$where = 'points_category_status = 1 AND (points_category_date_from >= '.$checked_date.' AND  points_category_date_to <= '.$checked_date.')';
		$this->db->from('points_category');
		$this->db->select('*');
		$this->db->where($where);
		$query = $this->db->get();
		// var_dump($query->num_rows()); die();
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key) {
				# code...
				$allocate_points = $key->points;
			}


		}
		else
		{
			$allocate_points  = 0;
		}
		$points = $old_points + $allocate_points;
		$data = array(
					'points' => $points,
				);
		$this->db->where('tenant_unit_id', $tenant_unit_id);
		$this->db->update('tenant_unit', $data);




		return TRUE;

	}
	function get_payment_methods()
	{

		return $this->db->get('payment_method');
	}

	function get_bank_accounts()
	{

		return $this->db->get('fn_banks');
	}

	function get_invoice_types()
	{

		return $this->db->get('invoice_type');
	}
	function get_months()
	{

		return $this->db->get('month');
	}


	function get_lease_invoices($lease_id)
	{
		$this->db->where('lease_invoice.invoice_deleted = 0 AND lease_id = '.$lease_id);
		return $this->db->get('lease_invoice');
	}

	function get_lease_invoice_items($lease_invoice_id)
	{
		$this->db->where('invoice_type.invoice_type_id = invoice.invoice_type AND lease_invoice_id = '.$lease_invoice_id);
		return $this->db->get('invoice,invoice_type');
	}

	function get_lease_invoice_amount($lease_invoice_id,$invoice_type_id)
	{

		$this->db->where('invoice.invoice_type = '.$invoice_type_id.' AND lease_invoice_id = '.$lease_invoice_id);
		$query = $this->db->get('invoice');

		$query_row = $query->row();

		return $query_row->invoice_amount;
	}

	function get_lease_paid_amount($lease_invoice_id,$invoice_type_id)
	{

		$this->db->where('payment_item.invoice_type_id = '.$invoice_type_id.' AND invoice_id = '.$lease_invoice_id);
		$this->db->select('SUM(amount_paid) AS total_paid');
		$query = $this->db->get('payment_item');

		$query_row = $query->row();

		return $query_row->total_paid;
	}


	function get_lease_credit_notes_amount($lease_invoice_id,$invoice_type_id)
	{

		$this->db->where('credit_note_item.invoice_type_id = '.$invoice_type_id.' AND invoice_id = '.$lease_invoice_id);
		$this->db->select('SUM(credit_note_amount) AS total_paid');
		$query = $this->db->get('credit_note_item');

		$query_row = $query->row();

		return $query_row->total_paid;
	}
	/*
	*	Retrieve all administrators
	*
	*/
	public function get_active_tenants()
	{
		$this->db->from('personnel');
		$this->db->select('*');
		$query = $this->db->get();

		return $query;
	}

	/*
	*	Retrieve all front end tenants
	*
	*/
	public function get_all_front_end_tenants()
	{
		$this->db->from('tenants');
		$this->db->select('*');
		$this->db->where('tenant_level_id = 2');
		$query = $this->db->get();

		return $query;
	}


	public function get_all_countries()
	{
		//retrieve all tenants
		$query = $this->db->get('country');

		return $query;
	}

	/*
	*	Add a new tenant to the database
	*
	*/
	public function add_tenant()
	{
		$data = array(
				'tenant_name'=>ucwords(strtolower($this->input->post('tenant_name'))),
				'tenant_email'=>$this->input->post('tenant_email'),
				'tenant_number'=>$this->create_tenant_number(),
				'tenant_national_id'=>$this->input->post('tenant_national_id'),
				'tenant_phone_number'=>$this->input->post('tenant_phone_number'),
				'created'=>date('Y-m-d H:i:s'),
				'tenant_status'=>1,
				'created_by'=>$this->session->userdata('personnel_id')
			);

		if($this->db->insert('tenants', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}

	public function add_tenant_to_unit($rental_unit_id)
	{
		$this->db->where('tenant_unit_status = 1 AND rental_unit_id = '.$rental_unit_id.'');
		$this->db->from('tenant_unit');
		$this->db->select('*');
		$query = $this->db->get();

		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key) {
				# code...
				$tenant_unit_id = $key->tenant_unit_id;
				$tenant_unit_status = $key->tenant_unit_status;
					// update the details the status to 1
				$update_array = array('tenant_unit_status'=>0);
				$this->db->where('tenant_unit_id = '.$tenant_unit_id);
				$this->db->update('tenant_unit',$update_array);
			}
			$insert_array = array(
							'tenant_id'=>$this->input->post('tenant_id'),
							'rental_unit_id'=>$rental_unit_id,
							'created'=>date('Y-m-d'),
							'created_by'=>$this->session->userdata('personnel_id'),
							'tenant_unit_status'=>1,
							);
			$this->db->insert('tenant_unit',$insert_array);
			return TRUE;
		}
		else
		{
			// create the tenant unit number
			$insert_array = array(
							'tenant_id'=>$this->input->post('tenant_id'),
							'rental_unit_id'=>$rental_unit_id,
							'created'=>date('Y-m-d'),
							'created_by'=>$this->session->userdata('personnel_id'),
							'tenant_unit_status'=>1,
							);
			$this->db->insert('tenant_unit',$insert_array);
			$tenant_unit_id = $this->db->insert_id();

			return TRUE;
		}
	}
	public function create_tenant_number()
	{
		//select product code
		$this->db->where('branch_code = "'.$this->session->userdata('branch_code').'"');
		$this->db->from('tenants');
		$this->db->select('MAX(tenant_number) AS number');
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			$result = $query->result();
			$number =  $result[0]->number;
			$number++;//go to the next number
			if($number == 1){
				$number = "".$this->session->userdata('branch_code')."-000001";
			}

			if($number == 1)
			{
				$number = "".$this->session->userdata('branch_code')."-000001";
			}

		}
		else{//start generating receipt numbers
			$number = "".$this->session->userdata('branch_code')."-000001";
		}
		return $number;
	}

	/*
	*	Add a new front end tenant to the database
	*
	*/
	public function add_frontend_tenant()
	{
		$data = array(
				'tenant_name'=>ucwords(strtolower($this->input->post('tenant_name'))),
				'tenant_email'=>$this->input->post('tenant_email'),
				'tenant_national_id'=>$this->input->post('tenant_national_id'),
				'tenant_password'=>md5(123456),
				'tenant_phone_number'=>$this->input->post('tenant_phone_number'),
				'created'=>date('Y-m-d H:i:s'),
				'tenant_status'=>1,
				'created_by'=>$this->session->userdata('personnel_id'),
			);

		if($this->db->insert('tenants', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}

	/*
	*	Edit an existing tenant
	*	@param int $tenant_id
	*
	*/
	public function edit_tenant($tenant_id)
	{
		$data = array(
				'tenant_name'=>ucwords(strtolower($this->input->post('tenant_name'))),
				'tenant_email'=>$this->input->post('tenant_email'),
				'tenant_national_id'=>$this->input->post('tenant_national_id'),
				'tenant_phone_number'=>$this->input->post('tenant_phone_number'),
				'tenant_status'=>1,
				'modified_by'=>$this->session->userdata('personnel_id'),
			);

		//check if tenant wants to update their password
		$pwd_update = $this->input->post('admin_tenant');
		if(!empty($pwd_update))
		{
			if($this->input->post('old_password') == md5($this->input->post('current_password')))
			{
				$data['password'] = md5($this->input->post('new_password'));
			}

			else
			{
				$this->session->set_userdata('error_message', 'The current password entered does not match your password. Please try again');
			}
		}

		$this->db->where('tenant_id', $tenant_id);

		if($this->db->update('tenants', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}

	/*
	*	Edit an existing tenant
	*	@param int $tenant_id
	*
	*/
	public function edit_frontend_tenant($tenant_id)
	{
		$data = array(
				'tenant_name'=>ucwords(strtolower($this->input->post('tenant_name'))),
				'other_names'=>ucwords(strtolower($this->input->post('last_name'))),
				'phone'=>$this->input->post('phone')
			);

		//check if tenant wants to update their password
		$pwd_update = $this->input->post('admin_tenant');
		if(!empty($pwd_update))
		{
			if($this->input->post('old_password') == md5($this->input->post('current_password')))
			{
				$data['password'] = md5($this->input->post('new_password'));
			}

			else
			{
				$this->session->set_userdata('error_message', 'The current password entered does not match your password. Please try again');
			}
		}

		$this->db->where('tenant_id', $tenant_id);

		if($this->db->update('tenants', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}

	/*
	*	Edit an existing tenant's password
	*	@param int $tenant_id
	*
	*/
	public function edit_password($tenant_id)
	{
		if($this->input->post('slug') == md5($this->input->post('current_password')))
		{
			if($this->input->post('new_password') == $this->input->post('confirm_password'))
			{
				$data['password'] = md5($this->input->post('new_password'));

				$this->db->where('tenant_id', $tenant_id);

				if($this->db->update('tenants', $data))
				{
					$return['result'] = TRUE;
				}
				else{
					$return['result'] = FALSE;
					$return['message'] = 'Oops something went wrong and your password could not be updated. Please try again';
				}
			}
			else{
					$return['result'] = FALSE;
					$return['message'] = 'New Password and Confirm Password don\'t match';
			}
		}

		else
		{
			$return['result'] = FALSE;
			$return['message'] = 'You current password is not correct. Please try again';
		}

		return $return;
	}

	/*
	*	Retrieve a single tenant
	*	@param int $tenant_id
	*
	*/
	public function get_tenant($tenant_id)
	{
		//retrieve all tenants
		$this->db->from('tenants');
		$this->db->select('*');
		$this->db->where('tenant_id = '.$tenant_id);
		$query = $this->db->get();

		return $query;
	}

	/*
	*	Retrieve a single tenant by their email
	*	@param int $email
	*
	*/
	public function get_tenant_by_email($email)
	{
		//retrieve all tenants
		$this->db->from('tenants');
		$this->db->select('*');
		$this->db->where('email = \''.$email.'\'');
		$query = $this->db->get();

		return $query;
	}



	/*
	*	Delete an existing tenant
	*	@param int $tenant_id
	*
	*/
	public function delete_tenant($tenant_id)
	{
		if($this->db->delete('tenants', array('tenant_id' => $tenant_id)))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}

	/*
	*	Activate a deactivated tenant
	*	@param int $tenant_id
	*
	*/
	public function activate_tenant($tenant_id)
	{
		$data = array(
				'activated' => 1
			);
		$this->db->where('tenant_id', $tenant_id);

		if($this->db->update('tenants', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}

	/*
	*	Deactivate an activated tenant
	*	@param int $tenant_id
	*
	*/
	public function deactivate_tenant($tenant_id)
	{
		$data = array(
				'activated' => 0
			);
		$this->db->where('tenant_id', $tenant_id);

		if($this->db->update('tenants', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}

	/*
	*	Reset a tenant's password
	*	@param string $email
	*
	*/
	public function reset_password($email)
	{
		//reset password
		$result = md5(date("Y-m-d H:i:s"));
		$pwd2 = substr($result, 0, 6);
		$pwd = md5($pwd2);

		$data = array(
				'password' => $pwd
			);
		$this->db->where('email', $email);

		if($this->db->update('tenants', $data))
		{
			//email the password to the tenant
			$tenant_details = $this->tenants_model->get_tenant_by_email($email);

			$tenant = $tenant_details->row();
			$tenant_name = $tenant->tenant_name;

			//email data
			$receiver['email'] = $this->input->post('email');
			$sender['name'] = 'Fad Shoppe';
			$sender['email'] = 'info@fadshoppe.com';
			$message['subject'] = 'You requested a password change';
			$message['text'] = 'Hi '.$tenant_name.'. Your new password is '.$pwd;

			//send the tenant their new password
			if($this->email_model->send_mail($receiver, $sender, $message))
			{
				return TRUE;
			}

			else
			{
				return FALSE;
			}
		}
		else
		{
			return FALSE;
		}
	}

	public function create_web_name($field_name)
	{
		$web_name = str_replace(" ", "-", strtolower($field_name));

		return $web_name;
	}
	public function change_password()
	{

		$data = array(
				'personnel_password' => md5($this->input->post('new_password'))
			);
		$this->db->where('personnel_password = "'.md5($this->input->post('current_password')).'" AND personnel_id ='.$this->session->userdata('personnel_id'));

		if($this->db->update('personnel', $data))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	public function get_tenancy_details($tenant_id,$rental_unit_id)
	{
		$this->db->from('tenant_unit');
		$this->db->select('*');
		$this->db->where('tenant_id = '.$tenant_id.' AND rental_unit_id ='.$rental_unit_id);
		$query = $this->db->get();

		return $query;
	}

	public function check_for_account($rental_unit_id)
	{

		$this->db->from('tenant_unit');
		$this->db->select('*');
		$this->db->where('tenant_unit_status = 1 AND rental_unit_id ='.$rental_unit_id);
		$query = $this->db->get();

		if($query->num_rows() > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}

	public function get_tenant_list($table, $where, $order)
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by($order,'asc');
		$query = $this->db->get('');

		return $query;
	}

	public function update_invoices_old()
	{


		$accounts_search = $this->session->userdata('all_accounts_search');
		$online_where = '';
		if(!empty($accounts_search))
		{
			$online_where = $accounts_search;

		}
		else
		{
			$online_where = '';
		}

		$table = 'tenants,tenant_unit,rental_unit,leases,property';
		$where = 'tenants.tenant_id = tenant_unit.tenant_id AND tenant_unit.rental_unit_id = rental_unit.rental_unit_id  AND tenant_unit.tenant_unit_id = leases.tenant_unit_id AND leases.lease_status = 1 AND leases.rental_unit_id = rental_unit.rental_unit_id AND rental_unit.property_id = property.property_id '.$online_where;


		$this->db->from($table);
		$this->db->select('leases.*,property.property_id');
		$this->db->where($where);
		$query = $this->db->get();

		if($query->num_rows() > 0)
		{
			// get the lease information

			foreach ($query->result() as $key) {
				# code...
				$lease_id = $key->lease_id;

				$lease_start_date = $key->lease_start_date;
				$rent_amount = $key->rent_amount;
				$inital_rent_amount = $key->rent_amount;
				$property_id = $key->property_id;
				$arreas_bf = $key->arrears_bf;

				// get the year and the month
				$date = explode('-', $lease_start_date);
				$month = $date[1];
				$year = $date[0];
				$x=0;
				$todays_month = date('m');



				$datestring=''.$lease_start_date.' first day of last month';
				$dt=date_create($datestring);
				$previous = $dt->format('Y-m');
				$previous_date = explode('-', $previous);
				$previous_year = $previous_date[0];
				$previous_month = $previous_date[1];

				// capture the invoice amount

				// if($month > $todays_month)
				// {
				// 	$month = $previous_month;
				// }

				// var_dump($todays_month); die();
				for ($m=$month; $m<=$todays_month; $m++) {
					// echo $m; die();
					// var_dump($m);

					$number = strlen($m);
					// check on the invoice table if the month exist for this

					if($m == 1)
					{
						$prev_month_debt = 1;

					}
					else
					{
						$prev_month_debt = $m-1;
					}
					if($prev_month_debt == 1 OR $prev_month_debt == 2 OR $prev_month_debt == 3 OR $prev_month_debt == 4 OR $prev_month_debt == 5 OR $prev_month_debt == 6 OR $prev_month_debt == 7 OR $prev_month_debt == 8 OR $prev_month_debt == 9)
					{
						$prev_month_debt = '0'.$prev_month_debt;
					}
					else
					{
						$prev_month_debt = $prev_month_debt;
					}


					if($number == 2)
					{
						$todaym = $m;
					}
					else
					{
						if($m == 1 OR $m == 2 OR $m == 3 OR $m == 4 OR $m == 5 OR $m == 6 OR $m == 7 OR $m == 8 OR $m == 9)
						{
							$todaym = '0'.$m;
						}
						else
						{
							$todaym = $m;
						}
					}
					$todaym = str_replace('00', '0', $todaym);
						// for the service charge

					// AND property_billing.billing_schedule_id = 1
					$this->db->from('invoice_type,property_billing');
					$this->db->select('*');
					$this->db->where('invoice_type.invoice_type_id = property_billing.invoice_type_id AND property_billing.charge_to = 1  AND property_billing.property_billing_status = 1 AND property_billing.property_id = '.$property_id);

					$invoice_type_query = $this->db->get();



					// previous items


					$start_date = strtotime('3 months ago');
					$start_quarter = ceil(date('m', $start_date) / 3);
					$start_month = ($start_quarter * 3) - 2;
					$start_year = date('Y', $start_date);


					$prev_quarter = 'AC'.$start_quarter.'-'.$start_year;

					// current items

					$current_date = strtotime(date('Y-m-d'));
					$current_quarter = ceil(date('m', $current_date) / 3);
					$current_month = ($current_quarter * 3) - 2;
					$current_year = date('Y', $current_date);

					$curr_quarter = 'AC'.$current_quarter.'-'.$current_year;

					// end of current querter
					if($invoice_type_query->num_rows() > 0)
					{

						foreach ($invoice_type_query->result() as $invoice_key) {

							$invoice_type_id = $invoice_key->invoice_type_id;
							$billing_amount = $invoice_key->billing_amount;

							$billing_schedule_id = $invoice_key->billing_schedule_id;


							if($billing_schedule_id == 2)
							{
								$where = 'invoice_type = '.$invoice_type_id.'  AND lease_id = '.$lease_id.' AND billing_schedule_quarter = "'.$curr_quarter.'"';
								$this->db->from('invoice');
								$this->db->select('*');
								$this->db->where($where);
							}
							else
							{
								$this->db->from('invoice');
								$this->db->select('*');
								$this->db->where('invoice_type = '.$invoice_type_id.' AND invoice_month = "'.$todaym.'" AND invoice_year = '.$year.' AND lease_id = '.$lease_id.'');
							}

							$service_charge_query = $this->db->get();
							// var_dump($where); die();

							if($service_charge_query->num_rows() == 0 )
							{

								if($invoice_type_id == 1)
					     		{
					     			if($rent_amount == 0)
					     			{
					     				$invoice_amount = $rent_amount;
					     			}
					     			else
					     			{
					     				$invoice_amount = $rent_amount;
					     			}

					     		}
					     		else
					     		{
					     			$invoice_amount = $billing_amount;
					     		}



					     		if($m== 1 OR $m == 2 OR $m == 3 OR $m == 4 OR $m == 5 OR $m == 6 OR $m == 7 OR $m == 8 OR $m == 9)
								{
									$todaym2 = '0'.$m;
								}
								else
								{
									$todaym2 = $m;
								}
								$todaym2 = str_replace('00', '0', $todaym2);

								var_dump($invoice_amount); die();
								if($invoice_amount > 0)
								{


									// var_dump($invoice_amount); die();
									// if($lease_id == 1 OR $lease_id == 3 OR $lease_id == 4 OR $lease_id == 171 OR $lease_id == 13 OR $lease_id == 19 OR $lease_id == 20 OR $lease_id == 21 OR $lease_id == 24 OR $lease_id == 32 OR $lease_id == 34 OR $lease_id == 46 OR $lease_id == 50 OR $lease_id == 51 OR $lease_id == 52 OR $lease_id == 54 OR $lease_id == 71 OR $lease_id == 90 OR $lease_id == 75 OR $lease_id == 76 OR $lease_id == 78 OR $lease_id == 80 OR $lease_id == 164)
									// {
										// var_dump($m);die();
										// if($invoice_amount == 450 )
										// {
										// 	$month = date('m');
										// 	$year = date('Y');
										// 	$insert_array = array(
										// 					'lease_id' => $lease_id,
										// 					'invoice_date' => date('Y-m-d'),
										// 					'invoice_month' => $month,
										// 					'invoice_year' => $year,
										// 					'invoice_amount' => $invoice_amount,
										// 					'invoice_type' => $invoice_type_id,
										// 					'invoice_status' => 1,
										// 					'billing_schedule_quarter' => $curr_quarter
										// 			 	 );

										// 	$this->db->insert('invoice',$insert_array);
										// }
									// }
									// else
									// {
									// 	if($invoice_amount == 450)
									// 	{

									// 	}
									// 	else
									// 	{
											$insert_array = array(
																'lease_id' => $lease_id,
																'invoice_date' => date('Y-m-d'),
																'invoice_month' => $todaym2,
																'invoice_year' => $year,
																'invoice_amount' => $invoice_amount,
																'invoice_type' => $invoice_type_id,
																'invoice_status' => 1,
																'billing_schedule_quarter' => $curr_quarter
														 	 );

											$this->db->insert('invoice',$insert_array);
									// 	}


									// }


								}


							}
						}
					}
					//  add an in

				}
			}


		}
		else
		{

		}
	}

	public function update_invoices()
	{


		$accounts_search = $this->session->userdata('all_accounts_search');
		$online_where = '';
		if(!empty($accounts_search))
		{
			$online_where = $accounts_search;

		}
		else
		{
			$online_where = '';
		}

		$table = 'tenants,tenant_unit,rental_unit,leases,property';
		$where = 'tenants.tenant_id = tenant_unit.tenant_id AND tenant_unit.rental_unit_id = rental_unit.rental_unit_id  AND tenant_unit.tenant_unit_id = leases.tenant_unit_id AND leases.lease_status = 1 AND leases.lease_deleted = 0 AND rental_unit.property_id = property.property_id AND property.property_deleted = 0 '.$online_where;


		$accounts_search = $this->session->userdata('all_accounts_search');

		if(!empty($accounts_search))
		{
			$where .= $accounts_search;

		}

		$this->db->from($table);
		$this->db->select('leases.*,property.property_id,tenants.tenant_name,tenants.tenant_phone_number');
		$this->db->where($where);
		$query = $this->db->get();

		// var_dump($query); die();
		if($query->num_rows() > 0)
		{
			// get the lease information
			foreach ($query->result() as $key) {
				# code...
				$lease_id = $key->lease_id;
				$lease_start_date = $key->lease_start_date;
				$rent_amount = $key->rent_amount;
				$inital_rent_amount = $key->rent_amount;
				$property_id = $key->property_id;
				$rental_unit_id = $key->rental_unit_id;
				$arreas_bf = $key->arrears_bf;
				$lease_number = $key->lease_number;
				$rental_unit_id = $key->rental_unit_id;
				$tenant_id = $key->tenant_id;
				$lease_number = $key->lease_number;
				$tenant_name = $key->tenant_name;
				$tenant_phone_number = $key->tenant_phone_number;

				// get the year and the month
				$date = explode('-', $lease_start_date);
				$month = $date[1];
				$year = $date[0];
				$x=0;
				$todays_month = date('m');

				// create a document number

				$prefix = 'INV-'.date('Y').'-'.$property_id;
				$suffix = $this->create_form_document_number($prefix);

				// $prefix = $prefix;
				if($suffix < 100)
				{
					if($suffix < 10)
					{
						$document_number = $prefix.'-00'.$suffix;
					}
					else
					{
						$document_number = $prefix.'-0'.$suffix;
					}
				}
				else
				{
					$document_number = $prefix.'-'.$suffix;
				}

				$document_number = $this->accounts_model->create_invoice_number();
				$this->db->from('lease_invoice');
				$this->db->select('lease_invoice.*');
				$this->db->where('lease_id',$lease_id);
				$lease_items = $this->db->get();
				$checked = $lease_items->num_rows();

				// var_dump($checked); die();
				if($checked == 0)
				{
					$insertarray['invoice_date'] = $date_invoiced = $lease_start_date;
					$insertarray['invoice_year'] = $year_invoiced = $year;
					$insertarray['invoice_month'] = $month_invoiced = $month;

				}
				else
				{
					$insertarray['invoice_date'] = $date_invoiced = date('Y-m-d');
					$insertarray['invoice_year'] = $year_invoiced = date('Y');
					$insertarray['invoice_month'] = $month_invoiced = date('m');
				}
				$insertarray['lease_id'] = $lease_id;
				$insertarray['rental_unit_id'] = $rental_unit_id;
				$insertarray['tenant_id'] = $tenant_id;

				$insertarray['account_id'] = $lease_number;

				$insertarray['document_number'] = $document_number;
				$insertarray['created_by'] = $this->session->userdata('personnel_id');
				$insertarray['created'] = date('Y-m-d');

				$insertarray['prefix'] = $prefix;
				$insertarray['suffix'] = $suffix;

				$datestring=''.$lease_start_date.' first day of last month';
				$dt=date_create($datestring);
				$previous = $dt->format('Y-m');
				$previous_date = explode('-', $previous);
				$previous_year = $previous_date[0];
				$previous_month = $previous_date[1];

				$todaym2 = date('m');
				$todaym = str_replace('00', '0', $todaym);
					// for the service charge



				$this->db->from('invoice_type,property_billing');
				$this->db->select('*');
				$this->db->where('invoice_type.invoice_type_id = property_billing.invoice_type_id AND property_billing.charge_to = 1 AND billing_schedule_id < 4  AND property_billing.property_billing_status = 1  AND property_billing.invoice_type_id <> 2 AND property_billing.property_billing_deleted = 0 AND property_billing.lease_id = '.$lease_id);

				$invoice_type_query = $this->db->get();


				$start_date = strtotime('3 months ago');
				$start_quarter = ceil(date('m', $start_date) / 3);
				$start_month = ($start_quarter * 3) - 2;
				$start_year = date('Y', $start_date);


				$prev_quarter = 'AC'.$start_quarter.'-'.$start_year;

				// current items

				$current_date = strtotime(date('Y-m-d'));
				$current_quarter = ceil(date('m', $current_date) / 3);
				$current_month = ($current_quarter * 3) - 2;
				$current_year = date('Y', $current_date);

				$invoice_date_item = date('Y-m-d');

				$curr_quarter = 'AC'.$current_quarter.'-'.$current_year;

				// end of current querter
				if($invoice_type_query->num_rows() > 0)
				{
					$this->db->insert('lease_invoice',$insertarray);
					$lease_invoice_id = $this->db->insert_id();
					$total_amount_invoiced = 0;
					// var_dump($lease_invoice_id); die();
					foreach ($invoice_type_query->result() as $invoice_key){

						$invoice_type_id = $invoice_key->invoice_type_id;
						$invoice_type_name = $invoice_key->invoice_type_name;
						$billing_amount = $invoice_key->billing_amount;
						$tax_amount = $invoice_key->tax_amount;
						$billing_schedule_id = $invoice_key->billing_schedule_id;

						if($billing_schedule_id == 2)
						{

							$datestring=''.$invoice_date_item.' first day of next month';
							$dt=date_create($datestring);
							$next = $dt->format('Y-m-d');
							$next_date = explode('-', $next);
							$next_year = $next_date[0];
							$next_month = $next_date[1];
							$next_date = $next_year.'-'.$next_month.'-'.'01';
							$next_date = strtotime($next_date);
							$next_quarter = ceil(date('m', $next_date) / 3);
							$next_month = ($next_quarter * 3) - 2;
							$next_year = date('Y', $next_date);

							$next_quarter = 'AC'.$next_quarter.'-'.$next_year;

							$where = 'invoice.invoice_type = '.$invoice_type_id.' AND lease_invoice.invoice_deleted = 0 AND lease_invoice.lease_invoice_id = invoice.lease_invoice_id AND lease_invoice.lease_id = '.$lease_id.' AND invoice.billing_schedule_quarter = "'.$next_quarter.'"';

							$this->db->from('invoice,lease_invoice');
							$this->db->select('*');
							$this->db->where($where);

						}
						else if($billing_schedule_id == 3)
						{
							$next_quarter = $curr_quarter;
							$this->db->from('invoice,lease_invoice');
							$this->db->select('*');
							$this->db->where('invoice.invoice_type = '.$invoice_type_id.' AND lease_invoice.invoice_deleted = 0 AND lease_invoice.lease_invoice_id = invoice.lease_invoice_id AND lease_invoice.lease_id = '.$lease_id.'');
						}
						else
						{
							$next_quarter = $curr_quarter;
							$this->db->from('invoice,lease_invoice');
							$this->db->select('*');
							$this->db->where('invoice.invoice_type = '.$invoice_type_id.' AND lease_invoice.invoice_deleted = 0 AND lease_invoice.lease_invoice_id = invoice.lease_invoice_id AND lease_invoice.invoice_month = "'.$todaym2.'" AND lease_invoice.invoice_year = '.$current_year.' AND lease_invoice.lease_id = '.$lease_id.'');
						}

						$service_charge_query = $this->db->get();

						if($service_charge_query->num_rows() == 0 )
						{

							$invoice_amount = $billing_amount;

							$todaym2 = str_replace('00', '0', $todaym2);

							$concate = $year_invoiced.'-'.$month_invoiced;
							$date = date('F Y',strtotime($concate));

							$remarks = $invoice_type_name.' for '.$date;

							if($invoice_amount > 0)
							{

								$insert_array = array(
												'lease_id' => $lease_id,
												'year' => $year_invoiced,
												'month' => $month_invoiced,
												'lease_invoice_id' => $lease_invoice_id,
												'invoice_amount' => $invoice_amount,
												'invoice_type' => $invoice_type_id,
												'billing_schedule_quarter' => $next_quarter,
												'invoice_item_status'=>1,
												'tax_amount'=>$tax_amount,
												'created'=>$date_invoiced,
												'personnel_id'=>$this->session->userdata('personnel_id'),
												'created_by'=>$this->session->userdata('personnel_id'),
												'tenant_id'=>$tenant_id,
												'rental_unit_id'=>$rental_unit_id,
												'lease_number'=>$lease_number,
												'remarks'=>$remarks

										 	 );
								$this->db->insert('invoice',$insert_array);
								$total_amount_invoiced += $invoice_amount;

							}
							// if($invoice_amount > 0)
							// {

							// 	$insert_array = array(
							// 					'lease_id' => $lease_id,
							// 					'year' => $year_invoiced,
							// 					'month' => $month_invoiced,
							// 					'lease_invoice_id' => $lease_invoice_id,
							// 					'invoice_amount' => $invoice_amount,
							// 					'invoice_type' => $invoice_type_id,
							// 					'billing_schedule_quarter' => $next_quarter,
							// 					'invoice_item_status'=>1,
							// 					'tax_amount'=>$tax_amount
							// 			 	 );
							// 	$this->db->insert('invoice',$insert_array);
							// 	$total_amount_invoiced += $invoice_amount;

							// }


						}
					}

					$update_array_lease['total_amount'] = $total_amount_invoiced;
					$this->db->where('lease_invoice_id',$lease_invoice_id);
					$this->db->update('lease_invoice',$update_array_lease);

					$explode = explode(' ',$tenant_name);
					$first_name = $explode[0];

					$parent_invoice_date = $date_invoiced;


					$total_credit_note = $this->accounts_model->get_rent_credit_note_brought_forward($lease_id,$parent_invoice_date,1,$month_invoiced,$year_invoiced,$rental_unit_id);
					$total_debit_note =$this->accounts_model->get_rent_debit_note_brought_forward($lease_id,$parent_invoice_date,1,$month_invoiced,$year_invoiced,$rental_unit_id);


					$rent_payment_amount = $this->accounts_model->get_rent_payments_brought_forward($lease_id,$parent_invoice_date,1,$month_invoiced,$year_invoiced,$rental_unit_id);

					$rent_invoice_amount =$this->accounts_model->get_rent_invoices_brought_forward($lease_id,$parent_invoice_date,1,$month_invoiced,$year_invoiced,$rental_unit_id);

					$brought_forward = ($rent_invoice_amount+$total_debit_note) - ($rent_payment_amount+$total_credit_note);

					$rent_current_invoice =  $this->accounts_model->get_current_month_invoice($lease_id,$month_invoiced,$year_invoiced,1,$previous_invoice_date,$parent_invoice_date);
					$rent_current_payment = $this->accounts_model->get_current_months_payments($lease_id,$month_invoiced,$year_invoiced,1,$previous_invoice_date,$parent_invoice_date);

					$total_current_variance = $rent_current_invoice - $rent_current_payment;

					$total_balance = $brought_forward + $total_current_variance;


					$message = "Dear ".$first_name.".Kindly note that your outstanding rent is KES ".$total_balance.".\nDue on 05/".$month_invoiced."/".$year_invoiced.".\nKindly pay. M-pesa paybill 338100. For help please call 0702777717. Allan & Bradley Co. LTD";

					$array_message['message'] = $message;
					$array_message['date_created'] = date('Y-m-d');
					$array_message['rental_unit_id'] = $rental_unit_id;
					$array_message['type_of_account'] = 1;
					$array_message['sms_invoice_year'] = $year_invoiced;
					$array_message['sms_invoice_month'] = $month_invoiced;
					$array_message['sms_type'] = 10;
					$array_message['sms_sent'] = 0;
					$array_message['phone_number'] = $tenant_phone_number;
					$array_message['client_name'] = $tenant_name;
					$array_message['lease_invoice_id'] = $lease_invoice_id;
					$array_message['lease_id'] = $lease_id;


					$this->db->insert('sms',$array_message);



				}
				//  add an in


			}


		}
		else
		{

		}
	}

	public function create_form_document_number($prefix)
	{
		//select product code
		$this->db->where('prefix LIKE  \'%'.$prefix.'%\'');
		$this->db->from('lease_invoice');
		$this->db->select('MAX(suffix) AS number');
		$this->db->order_by('lease_invoice_id','DESC');
		// $this->db->limit(1);
		$query = $this->db->get();
		// var_dump($query); die();
		if($query->num_rows() > 0)
		{
			$result = $query->result();
			$number =  $result[0]->number;
			$number++;

		}
		else{
			$number = 1;
		}
		// var_dump($number);die();
		return $number;
	}


	public function create_invoice_number()
	{
		//select product code
		$this->db->where('lease_invoice_id > 0');
		$this->db->from('lease_invoice');
		$this->db->select('MAX(document_number) AS number');
		$this->db->order_by('lease_invoice_id','DESC');
		// $this->db->limit(1);
		$query = $this->db->get();
		// var_dump($query); die();
		if($query->num_rows() > 0)
		{
			$result = $query->result();
			$number =  $result[0]->number;
			// var_dump($number);die();
			$number++;

		}
		else{
			$number = 1;
		}
		// var_dump($number);die();
		return $number;
	}

	public function update_payments_to_sept()
	{

		$home_owner_property_search = $this->session->userdata('home_owner_property_search');
		$online_where = '';
		if(!empty($home_owner_property_search))
		{
			$online_where = $home_owner_property_search;

		}
		else
		{
			$online_where = '';
		}

		$table = 'home_owners,rental_unit,home_owner_unit,property';
		$where = 'property.property_id = rental_unit.property_id AND home_owners.home_owner_id = home_owner_unit.home_owner_id AND home_owner_unit.rental_unit_id = rental_unit.rental_unit_id '.$online_where;


		// var_dump($where); die();


		$this->db->from($table);
		$this->db->select('home_owner_unit.*,property.property_id,rental_unit.rental_unit_id');
		$this->db->where($where);
		// $this->db->group_by('home_owner_unit.home_owner_id');
		$query = $this->db->get();


		if($query->num_rows() > 0)
		{
			// get the lease information

			foreach ($query->result() as $key) {
				# code...
				$rental_unit_id = $key->rental_unit_id;


				$where_update = 'invoice_amount = 34500 AND invoice_date = "2016-10-01" AND invoice_type = 4 AND rental_unit_id = '.$rental_unit_id;

				$array  = array('invoice_date' => "2016-09-30");

				$this->db->where($where_update);
				$this->db->update('home_owners_invoice',$array);
			}
		}
	}

	public function update_home_owner_invoices()
	{

		$home_owner_property_search = $this->session->userdata('home_owner_property_search');
		$online_where = '';
		if(!empty($home_owner_property_search))
		{
			$online_where = $home_owner_property_search;

		}
		else
		{
			$online_where = '';
		}

		$table = 'home_owners,rental_unit,home_owner_unit,property';
		$where = 'property.property_id = rental_unit.property_id AND home_owners.home_owner_id = home_owner_unit.home_owner_id AND home_owner_unit.rental_unit_id = rental_unit.rental_unit_id '.$online_where;


		$this->db->from($table);
		$this->db->select('home_owner_unit.*,property.property_id,rental_unit.rental_unit_id');
		$this->db->where($where);
		// $this->db->group_by('home_owner_unit.home_owner_id');
		$query = $this->db->get();


		if($query->num_rows() > 0)
		{
			// get the lease information

			foreach ($query->result() as $key) {
				# code...
				$rental_unit_id = $key->rental_unit_id;
				$lease_start_date = $key->lease_start_date;
				$property_id = $key->property_id;
				// $service_charge_amount = 30000;

				// get the year and the month
				$date = explode('-', $lease_start_date);
				$month = $date[1];
				$year = $date[0];
				$x=0;
				$todays_month = date('m');



				$datestring=''.$lease_start_date.' first day of last month';
				$dt=date_create($datestring);
				$previous = $dt->format('Y-m');
				$previous_date = explode('-', $previous);
				$previous_year = $previous_date[0];
				$previous_month = $previous_date[1];

				// for the service charge
				$this->db->from('invoice_type,property_billing');
				$this->db->select('*');
				$this->db->where('invoice_type.invoice_type_id = property_billing.invoice_type_id AND property_billing.charge_to = 0 AND billing_schedule_id <> 4 AND property_billing.property_billing_status = 1 AND property_billing.property_id = '.$property_id);

				$invoice_type_query = $this->db->get();

				// previous items


				$start_date = strtotime('3 months ago');
				$start_quarter = ceil(date('m', $start_date) / 3);
				$start_month = ($start_quarter * 3) - 2;
				$start_year = date('Y', $start_date);


				$prev_quarter = 'AC'.$start_quarter.'-'.$start_year;

				// current items
				$todaym2 = date('m');
				$today_date = date('d');
				$today_year = date('Y');
				$todaym2 = str_replace('00', '0', $todaym2);
				$invoice_date_item = $today_year.'-'.$todaym2.'-'.$today_date;


				$current_date = strtotime($invoice_date_item);
				$current_quarter = ceil(date('m', $current_date) / 3);
				$current_month = ($current_quarter * 3) - 2;
				$current_year = date('Y', $current_date);

				$curr_quarter = 'AC'.$current_quarter.'-'.$today_year;

				// end of current querter


				if($invoice_type_query->num_rows() > 0)
				{

					foreach ($invoice_type_query->result() as $invoice_key) {

						$invoice_type_id = $invoice_key->invoice_type_id;
						$billing_amount = $invoice_key->billing_amount;

						$billing_schedule_id = $invoice_key->billing_schedule_id;



						if($billing_schedule_id == 2)
						{

							//  get the next quater

							$datestring=''.$invoice_date_item.' first day of next month';
							$dt=date_create($datestring);
							$next = $dt->format('Y-m-d');
							$next_date = explode('-', $next);
							$next_year = $next_date[0];
							$next_month = $next_date[1];
							$next_date = $next_year.'-'.$next_month.'-'.'01';
							$next_date = strtotime($next_date);
							$next_quarter = ceil(date('m', $next_date) / 3);
							$next_month = ($next_quarter * 3) - 2;
							$next_year = date('Y', $next_date);

							$next_quarter = 'AC'.$next_quarter.'-'.$next_year;


							$this->db->from('home_owners_invoice');
							$this->db->select('*');
							$this->db->where('invoice_type = '.$invoice_type_id.' AND rental_unit_id = '.$rental_unit_id.' AND billing_schedule_quarter ="'.$next_quarter.'"');


						}
						else
						{

							$next_quarter = $curr_quarter;
							$this->db->from('home_owners_invoice');
							$this->db->select('*');
							$this->db->where('invoice_type = '.$invoice_type_id.' AND invoice_month = "'.$todaym.'" AND invoice_year = '.$year.' AND rental_unit_id = '.$rental_unit_id.'');
						}

						$service_charge_query = $this->db->get();


						if($service_charge_query->num_rows() == 0)
						{


							if($invoice_type_id == 1)
				     		{
				     			if($rent_amount == 0)
				     			{

				     				$invoice_amount = $rent_amount;
				     			}
				     			else
				     			{
				     				$invoice_amount = ($rent_amount);
				     			}

				     		}
				     		else
				     		{
				     			$invoice_amount = ($billing_amount);
				     		}
				     		// var_dump($today_year); die();


							$insert_array = array(
													'rental_unit_id' => $rental_unit_id,
													'invoice_date' => $invoice_date_item,
													'invoice_month' => $todaym2,
													'invoice_year' => $today_year,
													'invoice_amount' => $invoice_amount,
													'invoice_type' => $invoice_type_id,
													'invoice_status' => 1,
													'billing_schedule_quarter' => $next_quarter
											 	 );


							$this->db->insert('home_owners_invoice',$insert_array);

						}
					}
				}
				//  add an in
			}
		}

	}
	public function update_home_owner_invoices_old()
	{

		$home_owner_property_search = $this->session->userdata('home_owner_property_search');
		$online_where = '';
		if(!empty($home_owner_property_search))
		{
			$online_where = $home_owner_property_search;

		}
		else
		{
			$online_where = '';
		}

		$table = 'home_owners,rental_unit,home_owner_unit,property';
		$where = 'property.property_id = rental_unit.property_id AND home_owners.home_owner_id = home_owner_unit.home_owner_id AND home_owner_unit.rental_unit_id = rental_unit.rental_unit_id '.$online_where;


		$this->db->from($table);
		$this->db->select('home_owner_unit.*,property.property_id,rental_unit.rental_unit_id');
		$this->db->where($where);
		// $this->db->group_by('home_owner_unit.home_owner_id');
		$query = $this->db->get();


		if($query->num_rows() > 0)
		{
			// get the lease information

			foreach ($query->result() as $key) {
				# code...
				$rental_unit_id = $key->rental_unit_id;
				$lease_start_date = $key->lease_start_date;
				$property_id = $key->property_id;
				// $service_charge_amount = 30000;

				// get the year and the month
				$date = explode('-', $lease_start_date);
				$month = $date[1];
				$year = $date[0];
				$x=0;
				$todays_month = date('m');



				$datestring=''.$lease_start_date.' first day of last month';
				$dt=date_create($datestring);
				$previous = $dt->format('Y-m');
				$previous_date = explode('-', $previous);
				$previous_year = $previous_date[0];
				$previous_month = $previous_date[1];


				for ($m=$month; $m<=$todays_month; $m++) {

					// echo $m; die();

					$number = strlen($m);
					// check on the invoice table if the month exist for this

					if($m == 1)
					{
						$prev_month_debt = 1;

					}
					else
					{
						$prev_month_debt = $m-1;
					}
					if($prev_month_debt == 1 OR $prev_month_debt == 2 OR $prev_month_debt == 3 OR $prev_month_debt == 4 OR $prev_month_debt == 5 OR $prev_month_debt == 6 OR $prev_month_debt == 7 OR $prev_month_debt == 8 OR $prev_month_debt == 9)
					{
						$prev_month_debt = '0'.$prev_month_debt;
					}
					else
					{
						$prev_month_debt = $prev_month_debt;
					}


					if($number == 2)
					{
						$todaym = $m;
					}
					else
					{
						if($m == 1 OR $m == 2 OR $m == 3 OR $m == 4 OR $m == 5 OR $m == 6 OR $m == 7 OR $m == 8 OR $m == 9)
						{
							$todaym = '0'.$m;
						}
						else
						{
							$todaym = $m;
						}
					}
					$todaym = str_replace('00', '0', $todaym);
						// for the service charge


					$this->db->from('invoice_type,property_billing');
					$this->db->select('*');
					$this->db->where('invoice_type.invoice_type_id = property_billing.invoice_type_id AND property_billing.charge_to = 0 AND property_billing.property_billing_status = 1 AND property_billing.property_id = '.$property_id);

					$invoice_type_query = $this->db->get();



					// previous items


					$start_date = strtotime('3 months ago');
					$start_quarter = ceil(date('m', $start_date) / 3);
					$start_month = ($start_quarter * 3) - 2;
					$start_year = date('Y', $start_date);


					$prev_quarter = 'AC'.$start_quarter.'-'.$start_year;

					// current items
					if($m== 1 OR $m == 2 OR $m == 3 OR $m == 4 OR $m == 5 OR $m == 6 OR $m == 7 OR $m == 8 OR $m == 9)
					{
						$todaym2 = '0'.$m;
					}
					else
					{
						$todaym2 = $m;
					}
					$todaym2 = str_replace('00', '0', $todaym2);
					$invoice_date_item = $year.'-'.$todaym2.'-01';

					$current_date = strtotime($invoice_date_item);
					$current_quarter = ceil(date('m', $current_date) / 3);
					$current_month = ($current_quarter * 3) - 2;
					$current_year = date('Y', $current_date);

					$curr_quarter = 'AC'.$current_quarter.'-'.$current_year;

					// end of current querter


					if($invoice_type_query->num_rows() > 0)
					{


						foreach ($invoice_type_query->result() as $invoice_key) {

							$invoice_type_id = $invoice_key->invoice_type_id;
							$billing_amount = $invoice_key->billing_amount;

							$billing_schedule_id = $invoice_key->billing_schedule_id;



							if($billing_schedule_id == 2)
							{
								$this->db->from('home_owners_invoice');
								$this->db->select('*');
								$this->db->where('invoice_type = '.$invoice_type_id.' AND rental_unit_id = '.$rental_unit_id.' AND billing_schedule_quarter ="'.$curr_quarter.'"');
							}
							else
							{
								$this->db->from('home_owners_invoice');
								$this->db->select('*');
								$this->db->where('invoice_type = '.$invoice_type_id.' AND invoice_month = "'.$todaym.'" AND invoice_year = '.$year.' AND rental_unit_id = '.$rental_unit_id.'');
							}

							$service_charge_query = $this->db->get();


							if($service_charge_query->num_rows() == 0)
							{


								if($invoice_type_id == 1)
					     		{
					     			if($rent_amount == 0)
					     			{

					     				$invoice_amount = $rent_amount;
					     			}
					     			else
					     			{
					     				$invoice_amount = ($rent_amount);
					     			}

					     		}
					     		else
					     		{
					     			$invoice_amount = ($billing_amount);
					     		}


								// var_dump($invoice_amount); die();

								$insert_array = array(
														'rental_unit_id' => $rental_unit_id,
														'invoice_date' => $invoice_date_item,
														'invoice_month' => $todaym2,
														'invoice_year' => $year,
														'invoice_amount' => $invoice_amount,
														'invoice_type' => $invoice_type_id,
														'invoice_status' => 1,
														'billing_schedule_quarter' => $curr_quarter
												 	 );

								$this->db->insert('home_owners_invoice',$insert_array);
							}
						}
					}
					//  add an in

				}

			}
		}

	}
	public function get_current_arrears($lease_id,$month=NULL,$year=NULL,$invoice_status = NULL)
	{
		if(empty($month) || empty($year) || $month == NULL || $year == NULL)
		{
			$current_date = date('Y-m-d');
			$date = explode('-', $current_date);
			$month = $date[1];
			$year = $date[0];
		}
		// var_dump($year);die();

		// if($month == 1 OR $month == 2 OR $month == 3 OR $month == 4 OR $month == 5 OR $month == 6 OR $month == 7 OR $month == 8 OR $month == 9)
		// {
		// 	$todaymonth = $month;
		// }
		// var_dump($month); die();
		if(!empty($invoice_status))
		{
			$invoice_status = $invoice_status;
		}
		else
		{
			$invoice_status = 1;
		}
		$this->db->from('invoice');
		$this->db->select('*');
		$this->db->where('invoice_month = "'.$month.'" and invoice_status = '.$invoice_status.' and invoice_year = '.$year.' AND lease_id = '.$lease_id);
		$invoice_query2 = $this->db->get();

		$arrears_bf = 0;
		// var_dump($invoice_query2->num_rows());
		if($invoice_query2->num_rows() > 0)
		{
			foreach ($invoice_query2->result() as $prev_key) {
				$arrears_bf = $arrears_bf + $prev_key->arrears_bf;
			}
		}
		return $arrears_bf;
	}
	public function get_total_current_bills($lease_id)
	{
		$current_date = date('Y-m-d');

		$datestring=''.$current_date.' first day of last month';
		$dt=date_create($datestring);
		$previous = $dt->format('Y-m');
		$previous_date = explode('-', $previous);
		$previous_year = $previous_date[0];
		$previous_month = $previous_date[1];


		$date = explode('-', $current_date);
		$month = $date[1];
		$year = $date[0];

		// if($month == 1 OR $month == 2 OR $month == 3 OR $month == 4 OR $month == 5 OR $month == 6 OR $month == 7 OR $month == 8 OR $month == 9)
		// {
		// 	$todaymonth = $month;
		// }
		// var_dump($month); die();

		$this->db->from('invoice');
		$this->db->select('*');
		$this->db->where('invoice_month = "'.$month.'" and invoice_status = 1 and invoice_year = '.$year.' AND lease_id = '.$lease_id);
		$invoice_query2 = $this->db->get();

		$invoice_amount = 0;
		// var_dump($invoice_query2->num_rows());
		if($invoice_query2->num_rows() > 0)
		{
			foreach ($invoice_query2->result() as $prev_key) {
				$invoice_amount = $invoice_amount + $prev_key->invoice_amount;
			}
		}
		return $invoice_amount;
	}

	public function get_total_current_bills_owners($rental_unit_id)
	{
		$current_date = date('Y-m-d');

		$datestring=''.$current_date.' first day of last month';
		$dt=date_create($datestring);
		$previous = $dt->format('Y-m');
		$previous_date = explode('-', $previous);
		$previous_year = $previous_date[0];
		$previous_month = $previous_date[1];


		$date = explode('-', $current_date);
		$month = $date[1];
		$year = $date[0];

		if($month == 1 OR $month == 2 OR $month == 3 OR $month == 4 OR $month == 5 OR $month == 6 OR $month == 7 OR $month == 8 OR $month == 9)
		{
			$todaymonth = $month;
		}
		else
		{
			$todaymonth = $month;
		}
		// var_dump($todaymonth); die();

		$this->db->from('home_owners_invoice');
		$this->db->select('*');
		$this->db->where('invoice_month = "'.$todaymonth.'" and invoice_status = 1 and invoice_year = '.$year.' AND rental_unit_id = '.$rental_unit_id);
		$invoice_query2 = $this->db->get();

		$invoice_amount = 0;
		// var_dump($invoice_query2->num_rows());
		if($invoice_query2->num_rows() > 0)
		{
			foreach ($invoice_query2->result() as $prev_key) {
				$invoice_amount = $invoice_amount + $prev_key->invoice_amount;
			}
		}
		return $invoice_amount;
	}
	public function get_months_last_arrears($month,$year,$lease_id)
	{
		// $where = 'invoice_month = "'.$month.'"  and invoice_year = '.$year.' AND lease_id = '.$lease_id.'';
		$where = 'lease_invoice.lease_id = '.$lease_id.' AND lease_invoice.lease_invoice_id = invoice.lease_invoice_id AND lease_invoice.invoice_deleted = 0 AND invoice.invoice_item_deleted = 0 AND lease_invoice.invoice_month = "'.$month.'"  and lease_invoice.invoice_year = '.$year.'';
		// var_dump($where); die();
		$this->db->from('invoice,lease_invoice');
		$this->db->select('*');
		$this->db->where($where);
		$invoice_query2 = $this->db->get();
		$arrears_bf = 0;
		if($invoice_query2->num_rows() > 0)
		{
			foreach ($invoice_query2->result() as $prev_key) {
				# code...
				$arrears_bf = $prev_key->arrears_bf;
			}
		}
		return $arrears_bf;
	}
	public function get_current_balance($lease_id)
	{
		$current_date = date('Y-m-d');

		// $this->db->from('leases');
		// $this->db->select('*');
		// $this->db->where('lease_id = '.$lease_id);
		// $balance_query = $this->db->get();
		// $lease_row = $balance_query->row();

		// $lease_start_date = $lease_row->lease_start_date;


		$date = explode('-', $current_date);
		$month = $date[1];
		$year = $date[0];



		if($month == 1 OR $month == 2 OR $month == 3 OR $month == 4 OR $month == 5 OR $month == 6 OR $month == 7 OR $month == 8 OR $month == 9)
		{
			$todaymonth = $month;
		}

		$this->db->from('invoice');
		$this->db->select('*');
		$this->db->where('invoice_month = "'.$todaymonth.'" AND invoice_status = 1 AND invoice_year = '.$year.' AND lease_id = '.$lease_id);
		$invoice_query2 = $this->db->get();
		$invoice_amount = 0;
		if($invoice_query2->num_rows() > 0)
		{
			foreach ($invoice_query2->result() as $prev_key) {
				# code...
				$current_invoice = $prev_key->invoice_amount;
				$arrears_bf = $prev_key->arrears_bf;
				$invoice_amount = $invoice_amount + ($current_invoice + $arrears_bf);
			}
		}

		//  check for payments also
		$this->db->from('payments');
		$this->db->select('*');
		$this->db->where('month = "'.$todaymonth.'" and year = '.$year.' AND lease_id = '.$lease_id);
		$payment_query2 = $this->db->get();
		$amount_paid = 0;
		if($payment_query2->num_rows() > 0)
		{
			foreach ($payment_query2->result() as $prev_key) {
				# code...
				$amount_paid = $prev_key->amount_paid;
			}
		}

		return $invoice_amount - $amount_paid;
	}
	public function get_months_last_debt($lease_id,$month)
	{
		$where = 'lease_id = '.$lease_id.' AND payment_status = 1 AND month = "'.$month.'"';
		// var_dump($where);die();
		$this->db->from('payments');
		// $this->db->select('balance_cf AS balance');
		$this->db->select('SUM(amount_paid) AS total_paid');
		$this->db->where($where);
		$this->db->order_by('lease_id', 'DESC');
		$this->db->limit(1);
		$query = $this->db->get();
		$total_paid = 0;

		foreach ($query->result() as $key) {
			# code...
			$total_paid = $key->total_paid;
		}
		if($total_paid == NULL)
		{
			$total_paid = 0;
		}

		return $total_paid;
	}

	public function get_months_payments($lease_id,$month,$year)
	{
		$where = 'lease_id = '.$lease_id.' AND payment_status = 1 AND month = "'.$month.'" AND  year = "'.$year.'"';

		$this->db->from('payments');
		$this->db->select('SUM(amount_paid) AS total_paid');
		$this->db->where($where);
		$this->db->order_by('lease_id', 'DESC');
		$this->db->limit(1);
		$query = $this->db->get();
		$total_paid = 0;

		foreach ($query->result() as $key) {
			# code...
			$total_paid = $key->total_paid;
		}
		if($total_paid == NULL)
		{
			$total_paid = 0;
		}

		return $total_paid;
	}
	public function get_months_home_owners_payments($rental_unit_id,$month,$year)
	{
		$where = 'rental_unit_id = '.$rental_unit_id.' AND payment_status = 1 AND cancel = 0 AND month = "'.$month.'" AND  year = "'.$year.'"';

		$this->db->from('home_owners_payments');
		$this->db->select('SUM(amount_paid) AS total_paid');
		$this->db->where($where);
		$this->db->order_by('rental_unit_id', 'DESC');
		$this->db->limit(1);
		$query = $this->db->get();
		$total_paid = 0;

		foreach ($query->result() as $key) {
			# code...
			$total_paid = $key->total_paid;
		}
		if($total_paid == NULL)
		{
			$total_paid = 0;
		}

		return $total_paid;
	}


	public function payments_import_template()
	{
		$this->load->library('Excel');

		$title = 'Tenants payments Import Template';
		$count=1;
		$row_count=0;
		$report[$row_count][0] = 'Rental Unit Code';
		$report[$row_count][1] = 'Receipt Number';
		$report[$row_count][2] = 'Amount Paid';
		$report[$row_count][3] = 'Paid By';
		$report[$row_count][4] = 'Payment Date';
		$report[$row_count][5] = 'Payment Code';
		$report[$row_count][6] = 'Type of Account (Tenant 1, Owner 0)';

		$row_count++;

		//create the excel document
		$this->excel->addArray ( $report );
		$this->excel->generateXML ($title);
	}


	public function import_csv_payroll($upload_path)
	{
		//load the file model
		$this->load->model('admin/file_model');
		/*
			-----------------------------------------------------------------------------------------
			Upload csv
			-----------------------------------------------------------------------------------------
		*/
		$response = $this->file_model->upload_csv($upload_path, 'import_csv');

		if($response['check'])
		{
			$file_name = $response['file_name'];

			$array = $this->file_model->get_array_from_csv($upload_path.'/'.$file_name);
			// var_dump($array); die();
			$response2 = $this->sort_account_data($array);

			if($this->file_model->delete_file($upload_path."\\".$file_name, $upload_path))
			{
			}

			return $response2;
		}
		else
		{
			$this->session->set_userdata('error_message', $response['error']);
			return FALSE;
		}
	}
	public function sort_account_data($array)
	{
		$total_rows = count($array);
		$total_columns = count($array[0]);
		// var_dump($total_columns);die();
		//if products exist in array
		if(($total_rows > 0) && ($total_columns == 7))
		{
			//retrieve the data from array
			for($r = 1; $r < $total_rows; $r++)
			{

				$rental_unit_name= $rental_unit_name = $array[$r][0];
				$items['receipt_number'] = $receipt_number = $array[$r][1];
				$items['amount_paid'] = $amount_paid = $array[$r][2];
				$items['paid_by']  = $paid_by = $array[$r][3];
				$payment_date = $array[$r][4];
				$items['payment_date'] = $payment_date = date("Y-m-d", strtotime($payment_date));
				$expleded = explode('-', $payment_date);
							$month = $expleded[1];
							$year = $expleded[0];
				$payment_code  = $paid_by = $array[$r][5];
				$type_of_account  = $paid_by = $array[$r][6];

				// get the current active lease

				//  get the invoice code item
				$invoice_type_id = $this->get_payment_code_id($payment_code);

				$rental_unit_id = $this->get_rental_unit_id($rental_unit_name);

				$lease_id = $this->get_active_lease_id($rental_unit_id);

				$items['payment_method_id'] = 1;
				$items['personnel_id'] =  $this->session->userdata("personnel_id");
				$items['transaction_code'] = '';
				$items['payment_created'] = date("Y-m-d");
				$items['year'] = $year;
				$items['month'] = $month;
				$items['payment_created_by'] = $this->session->userdata("personnel_id");
				$invoice_number = $this->get_invoice_number();

				if($type_of_account == 1)
				{
					$receipt_number = $this->create_receipt_number();
				}
				else
				{
					$receipt_number = $this->create_owners_receipt_number();
				}
				$items['receipt_number'] = $receipt_number;

				if($lease_id > 0 && $type_of_account == 1)
				{
					$items['lease_id'] =  $lease_id;


					if($this->db->insert('payments', $items))
					{
						$payment_id = $this->db->insert_id();
						$service = array(
									'lease_id'=>$lease_id,
									'payment_id'=>$payment_id,
									'amount_paid'=> $amount_paid,
									'invoice_type_id' => $invoice_type_id,
									'payment_item_status' => 1,
									'payment_item_created' => date('Y-m-d')
								);
						$this->db->insert('payment_item',$service);

					}

				}
				else if($rental_unit_id > 0 && $type_of_account == 0)
				{
					$items['rental_unit_id'] =  $rental_unit_id;

					if($this->db->insert('home_owners_payments', $items))
					{
						$payment_id = $this->db->insert_id();
						$service = array(
									'payment_id'=>$payment_id,
									'rental_unit_id'=>$rental_unit_id,
									'amount_paid'=> $amount_paid,
									'invoice_type_id' => $invoice_type_id,
									'payment_item_status' => 0,
									'payment_item_created' => date('Y-m-d')
								);
						$this->db->insert('home_owner_payment_item',$service);
					}
				}
				else
				{
					//  insert into the table of not synced payments
					$unimport['amount_paid'] = $amount_paid;
					$unimport['property_id'] = $this->input->post('property_id');
					$unimport['rental_unit_name'] = $rental_unit_name;
					$unimport['payment_code'] = $payment_code;
					$unimport['type_of_account'] = $type_of_account;
					$unimport['paid_by'] = $paid_by;
					$unimport['payment_date'] = $payment_date;

					$this->db->insert('import_payment_upload',$unimport);
				}

			}

			$return['response'] = 'success';
			$return['check'] = TRUE;
		}

		//if no products exist
		else
		{
			$return['response'] = 'Member data not found ';
			$return['check'] = FALSE;
		}

		return $return;
	}
	public function get_rental_unit_id($rental_unit_name)
	{
		$this->db->from('rental_unit');
		$this->db->select('*');
		$this->db->where('rental_unit_name = "'.$rental_unit_name.'"');

		$query = $this->db->get();

		$result = $query->result();

		$rental_unit_id = 0;

		if($query->num_rows() > 0 )
		{
			$result = $query->result();
			$rental_unit_id = $result[0]->rental_unit_id;
		}

		return $rental_unit_id;

	}
	public function get_payment_code_id($invoice_type_code)
	{
		$this->db->from('invoice_type');
		$this->db->select('*');
		$this->db->where('invoice_type_code = "'.$invoice_type_code.'"');

		$query = $this->db->get();

		$result = $query->result();

		$invoice_type_id = 0;

		if($query->num_rows() > 0 )
		{
			$result = $query->result();
			$invoice_type_id = $result[0]->invoice_type_id;
		}

		return $invoice_type_id;

	}

	public function get_active_lease_id($rental_unit_id)
	{
		$this->db->from('leases');
		$this->db->select('*');
		$this->db->where('lease_status = 1 AND rental_unit_id = "'.$rental_unit_id.'"');

		$query = $this->db->get();

		$result = $query->result();
		$lease_id = 0;

		if($query->num_rows() > 0 )
		{
			$result = $query->result();
			$lease_id = $result[0]->lease_id;
		}

		return $lease_id;
	}
	public function get_all_invoice_month_old($lease_id)
	{
		// var_dump($lease_id); die();
		$this->db->from('invoice');
		$this->db->select('*');
		$this->db->where('lease_id = '.$lease_id.' AND invoice_status = 1');
		$this->db->order_by('invoice_month','DESC');
		$this->db->group_by('invoice_month,invoice_year');
		$query = $this->db->get();
		return $query;
	}
	public function get_all_invoice_month($lease_id,$type= null,$year = NULL)
	{
		// var_dump($lease_id); die();
		if(!empty($type))
		{
			if($type == 1)
			{
				$add_where = ' AND invoice_type <> 2';
			}
			else
			{
				$add_where = ' AND invoice_type = 2';
			}
			$add_where = '';
		}
		else
		{
			$add_where ='';
		}

		if(!empty($year))
		{
			$add_where .= ' AND invoice_year ='.$year;
		}
		else
		{
			$add_where .= '';
		}
		$this->db->from('invoice,lease_invoice');
		$this->db->select('SUM(invoice.invoice_amount) AS total_invoice,lease_invoice.invoice_month,lease_invoice.invoice_year,lease_invoice.invoice_date,lease_invoice.document_number,lease_invoice.sent_status,lease_invoice.lease_invoice_id');
		$this->db->where('lease_invoice.lease_id = '.$lease_id.' AND lease_invoice.lease_invoice_id = invoice.lease_invoice_id AND lease_invoice.invoice_deleted = 0 AND lease_invoice.invoice_date <> "0000-00-00" '.$add_where);
		$this->db->order_by('lease_invoice.invoice_date','ASC');
		$this->db->group_by('lease_invoice.invoice_date');
		$query = $this->db->get();
		return $query;
	}



	public function get_tenant_lease_invoices($lease_id,$type= null)
	{
		// var_dump($lease_id); die();
		if(!empty($type))
		{
			if($type == 1)
			{
				$add_where = ' AND invoice_type <> 2';
			}
			else
			{
				$add_where = ' AND invoice_type = 2';
			}
			$add_where ='';
		}
		else
		{
			$add_where ='';
		}
		$this->db->from('invoice,lease_invoice');
		$this->db->select('SUM(invoice.invoice_amount) AS total_invoice,lease_invoice.invoice_month,lease_invoice.invoice_year,lease_invoice.invoice_date,lease_invoice.document_number,lease_invoice.sent_status,lease_invoice.lease_invoice_id,lease_invoice.sent_status');
		$this->db->where('lease_invoice.lease_id = '.$lease_id.' AND lease_invoice.lease_invoice_id = invoice.lease_invoice_id AND lease_invoice.invoice_deleted = 0 AND lease_invoice.invoice_date <> "0000-00-00" '.$add_where);
		$this->db->order_by('lease_invoice.invoice_date','ASC');
		$this->db->group_by('lease_invoice.invoice_date');
		$query = $this->db->get();
		return $query;
	}
	public function get_all_invoice_current_month($lease_id,$last_month,$last_year)
	{
		// var_dump($lease_id); die();

		//  get the dates correctly

		$invoice_date = $last_year.'-'.$last_month.'-'.date('d');

		// 'lease_id = '.$lease_id.' AND invoice_date <> "0000-00-00" AND invoice_month < "'.$last_month.'" AND invoice_year <= "'.$last_year.'" '
		$where = 'lease_id = '.$lease_id.' AND invoice_date <> "0000-00-00" AND invoice_date <= "'.$invoice_date.'" ';

		$this->db->from('invoice');
		$this->db->select('SUM(invoice_amount) AS total_invoice,invoice_month,invoice_year,invoice_date');
		$this->db->where($where);

		$this->db->order_by('invoice_month','ASC');
		$this->db->group_by('invoice_month,invoice_year');
		$query = $this->db->get();
		return $query;
	}
	public function get_all_payments_lease($lease_id)
	{
		$this->db->from('payments');
		$this->db->select('*');
		$this->db->where('lease_id = '.$lease_id.'  AND payment_status = 1 AND cancel = 0');
		$this->db->order_by('payment_date','ASC');
		$query = $this->db->get();
		return $query;
	}
	public function get_all_pardons_lease($lease_id,$year=null)
	{
		if(!empty($year))
		{
			$add = ' AND pardon_payments.pardon_date ='.$year;
		}
		else
		{
			$add = '';
		}
		$this->db->from('pardon_payments');
		$this->db->select('*');
		$this->db->where('lease_id = '.$lease_id.' '.$add.' AND pardon_status = 1 AND pardon_delete = 0');
		$this->db->order_by('pardon_date','ASC');
		$query = $this->db->get();
		return $query;
	}



	public function get_all_owner_pardons_lease($rental_unit_id)
	{
		$this->db->from('pardon_owner_payments');
		$this->db->select('*');
		$this->db->where('rental_unit_id = '.$rental_unit_id.'  AND pardon_status = 1 AND pardon_delete = 0');
		$this->db->order_by('pardon_date','ASC');
		$query = $this->db->get();
		return $query;
	}
	public function get_all_payments_current_lease($lease_id,$last_month,$last_year)
	{
		// 'lease_id = '.$lease_id.' AND payment_status = 1 AND month < "'.$last_month.'" AND year <= "'.$last_year.'" AND cancel = 0'
		$payment_date =  $last_year.'-'.$last_month.'-'.date('d');

		$where = 'lease_id = '.$lease_id.' AND payment_status = 1 AND payment_date <= "'.$payment_date.'" AND cancel = 0';
		$this->db->from('payments');
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by('payment_date','ASC');
		$query = $this->db->get();
		return $query;
	}

	// home owners
	public function get_all_owners_invoice_month($rental_unit_id)
	{
		// var_dump($rental_unit_id); die();
		$this->db->from('home_owners_invoice');
		$this->db->select('SUM(invoice_amount) AS invoice_amount,invoice_month,invoice_year,invoice_date,balance_bf,rental_unit_id');
		$this->db->where('rental_unit_id = '.$rental_unit_id.' AND invoice_date <> "0000-00-00"');
		$this->db->order_by('invoice_date','ASC');
		$this->db->group_by('invoice_month,invoice_year');
		$query = $this->db->get();
		return $query;

	}

	// public function get_all_owners_invoice_month($rental_unit_id)
	// {
	// 	// var_dump($rental_unit_id); die();
	// 	$this->db->from('home_owners_invoice');
	// 	$this->db->select('invoice_amount,invoice_month,invoice_year,invoice_date');
	// 	$this->db->where('rental_unit_id = '.$rental_unit_id.' AND invoice_date <> "0000-00-00" AND balance_bf != "1" ');
	// 	$this->db->order_by('invoice_month','ASC');
	// 	$this->db->group_by('invoice_month,invoice_year');
	// 	$query = $this->db->get();
	// 	return $query;
	// }
	public function get_all_owners_payments_lease($rental_unit_id)
	{
		$this->db->from('home_owners_payments');
		$this->db->select('*');
		$this->db->where('rental_unit_id = '.$rental_unit_id.' AND cancel = 0 AND payment_status = 1');
		$this->db->order_by('payment_date','ASC');
		$query = $this->db->get();
		return $query;
	}
	// all home owners
	public function get_all_home_owners_invoice_month($rental_unit_id)
	{
		// var_dump($rental_unit_id); die();
		$this->db->from('home_owners,home_owner_unit,home_owners_invoice');
		$this->db->select('home_owners_invoice.*, home_owners.home_owner_id');
		$this->db->where('home_owners_invoice.rental_unit_id = '.$rental_unit_id.' AND home_owners_invoice.invoice_status = 1 AND home_owner_unit.rental_unit_id = home_owners_invoice.rental_unit_id AND home_owners.home_owner_id = home_owner_unit.home_owner_id');
		$this->db->order_by('home_owners_invoice.invoice_month','DESC');
		$this->db->group_by('home_owners_invoice.invoice_month,invoice_year');
		$query = $this->db->get();
		return $query;
	}
	public function get_months_invoices_old($lease_id,$month)
	{
		$where = 'lease_id = '.$lease_id.' AND invoice_status = 1 AND invoice_month = "'.$month.'"';
		$this->db->from('invoice');
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by('invoice_date', 'DESC');
		$query = $this->db->get();
		return $query;
	}
	public function get_months_invoices($lease_id,$month)
	{
		$where = 'lease_invoice.lease_id = '.$lease_id.' AND lease_invoice.invoice_date <> "0000-00-00" AND lease_invoice.invoice_month = "'.$month.'" AND lease_invoice.lease_invoice_id = invoice.lease_invoice_id AND invoice.invoice_item_deleted = 0 AND lease_invoice.invoice_deleted = 0';
		$this->db->from('invoice,lease_invoice');
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by('lease_invoice.invoice_date', 'ASC');
		$query = $this->db->get();
		return $query;
	}
	public function get_months_invoices_home_owners($rental_unit_id,$month)
	{
		$where = 'rental_unit_id = '.$rental_unit_id.' AND invoice_status = 1 AND invoice_month = "'.$month.'"';
		$this->db->from('home_owners_invoice');
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by('invoice_date', 'DESC');
		$query = $this->db->get();
		return $query;
	}
	public function send_arrears($lease_id)
	{
		//  get the tenant name and the rental unit name

		$where = 'leases.lease_id = '.$lease_id.' AND leases.tenant_unit_id = tenant_unit.tenant_unit_id AND tenant_unit.tenant_id = tenants.tenant_id AND tenant_unit.rental_unit_id = rental_unit.rental_unit_id';
		$this->db->from('leases,tenant_unit,tenants,rental_unit');
		$this->db->select('tenant_name,rental_unit_name,tenant_phone_number');
		$this->db->where($where);
		$query = $this->db->get();
		if($query->num_rows > 0)
		{
			$rows = $query->row();

			$tenant_name = $rows->tenant_name;
			$rental_unit_name = $rows->rental_unit_name;
			$tenant_phone_number = $rows->tenant_phone_number;

			$tenant_explode = explode(' ', $tenant_name);
			$name = $tenant_explode[0];

			$current_arrears = $this->get_current_arrears($lease_id);
			$current_balance = $this->get_current_balance($lease_id);

			if($current_balance > 0)
			{
				$date = date('Y-m-d');
				// compose the message

				$message = 'Hello, '.$name.', Your current water dues for '.$rental_unit_name.' as at '.$date.' is KES. '.number_format($current_balance).'';
				if($this->sms($tenant_phone_number,$message,$tenant_name))
				{
					return TRUE;
				}
				else
				{
					return FALSE;
				}
			}
			else
			{
				// compose the message
				$message = 'Hello, '.$tenant_name.', Thank you for your continued support.';
				if($this->sms($tenant_phone_number,$message,$tenant_name))
				{
					return TRUE;
				}
				else
				{
					return FALSE;
				}
			}

		}





	}

	public function sms($phone,$message,$name,$title=NULL)
	{
        // This will override any configuration parameters set on the config file
		// max of 160 characters
		// to get a unique name make payment of 8700 to Africastalking/SMSLeopard
		// unique name should have a maximum of 11 characters

		if (substr($phone, 0, 1) === '0') 
		{
			$phone = ltrim($phone, '0');
			$phone = '+254'.$phone;
		}
		
		$phone= str_replace(' ', '', $phone);

		$phone_number = $phone;
		// var_dump($phone_number);die();

		// get items

		$configuration = $this->admin_model->get_configuration();

		$mandrill = '';
		$configuration_id = 0;

		if($configuration->num_rows() > 0)
		{
			$res = $configuration->row();
			$configuration_id = $res->configuration_id;
			$mandrill = $res->mandrill;
			$sms_key = $res->sms_key;
			$sms_user = $res->sms_user;
	        $sms_suffix = $res->sms_suffix;
	        $sms_from = $res->sms_from;
		}
	    else
	    {
	        $configuration_id = '';
	        $mandrill = '';
	        $sms_key = '';
	        $sms_user = '';
	        $sms_from = '';
	        $sms_suffix = '';

	    }

	    $actual_message = $message.' '.$sms_suffix;
	    // var_dump($actual_message); die();
		// get the current branch code
        $params = array('username' => $sms_user, 'apiKey' => $sms_key);

        $this->load->library('AfricasTalkingGateway', $params);
		// var_dump($params)or die();
        // Send the message


		try
		{
        	$results = $this->africastalkinggateway->sendMessage($phone_number, $actual_message, $sms_from);
			// var_dump($results);die();
			foreach($results as $result) {
				$date_today = date('Y-m-d');
				$date_exploded = explode('-', $date_today);
				$_year = $date_exploded[0];
				$_month = $date_exploded[1];
				// status is either "Success" or "error message"
				// echo " Number: " .$result->number;
				// echo " Status: " .$result->status;
				// echo " MessageId: " .$result->messageId;
				// echo " Cost: "   .$result->cost."\n";
				if($result->status == 'Success')
				{
					$array = array(
									'messaging_tenant_phone_number'=> $phone_number,
									'messaging_tenant_name'=> $name,
									'message'=> $actual_message,
									'sent_status' => 1,
									'message_cost' => $result->cost,
									'message_category_id' => 1,
									'branch_code' => $this->session->userdata('branch_code'),
									'date_created' => date('Y-m-d'),
									'statement_sent_name'=>$title,
									'statement_month'=>$_month,
									'statement_year'=>$_year

								);
					$this->db->insert('messaging',$array);
				}
				else
				{
					$array = array(
									'messaging_tenant_phone_number'=> $phone_number,
									'messaging_tenant_name'=> $name,
									'message'=> $actual_message,
									'sent_status' => 0,
									'message_category_id' =>  1,
									'branch_code' => $this->session->userdata('branch_code'),
									'date_created' => date('Y-m-d'),
									'message_cost' => 0,
									'statement_sent_name'=>$title,
									'statement_month'=>$_month,
									'statement_year'=>$_year
								);
					$this->db->insert('messaging',$array);
				}
			}

			return TRUE;

		}

		catch(AfricasTalkingGatewayException $e)
		{
			// echo "Encountered an error while sending: ".$e->getMessage();
			return FALSE;
		}
    }
    public function get_payment_details($payment_id)
	{
		$this->db->select('payments.*');
		$this->db->where('leases.lease_id = payments.lease_id AND payment_item.payment_id = payments.payment_id AND cancel = 0 AND payments.payment_id = '.$payment_id);
		$this->db->join('payment_method','payment_method.payment_method_id = payments.payment_method_id','left');
		$this->db->join('fn_banks','payments.bank_id = fn_banks.id','left');
		$this->db->group_by('payments.payment_id');
		return $this->db->get('payments, leases,payment_item');
	}
	 public function get_owners_payment_details($payment_id)
	{
		$this->db->where('cancel = 0 AND home_owners_payments.payment_id = '.$payment_id);

		return $this->db->get('home_owners_payments');
	}

	public function get_personnel($personnel_id)
	{
		if(empty($personnel_id))
		{
			//redirect('login');
			$personnel = '-';
		}

		else
		{
			$this->db->select('personnel.personnel_fname, personnel.personnel_onames');
			$this->db->from('personnel');
			$this->db->where('personnel.personnel_id = '.$personnel_id);

			$query = $this->db->get();

			if($query->num_rows() > 0)
			{
				$row = $query->row();
				$personnel = $row->personnel_onames.' '.$row->personnel_fname;
			}

			else
			{
				$personnel = '-';
			}

			return $personnel;
		}
	}
	public function get_total_bills($lease_id)
	{
		// $where = 'lease_id = '.$lease_id.'';
		// // var_dump($where);die();
		// $this->db->from('leases');
		// // $this->db->select('balance_cf AS balance');
		// $this->db->select('SUM(arrears_bf) AS arrears');
		// $this->db->where($where);
		// $this->db->limit(1);
		// $query = $this->db->get();

		// $total_paid = 0;

		// foreach ($query->result() as $key) {
		// 	# code...
		// 	$arrears = $key->arrears;
		// }
		// if($arrears == NULL)
		// {
		// 	$arrears = 0;
		// }

		$bills = $this->accounts_model->get_all_invoice_month($lease_id);
		$total_bill = 0;
		if($bills->num_rows() > 0)
		{
			$total_bill = 0;
			foreach ($bills->result() as $key_bills) {
				# code...
				$invoice_month_number = $key_bills->invoice_month;

				$invoice_month = date('F', mktime(0,0,0,$invoice_month_number, 1, date('Y')));
				// $month = date('F', mktime(0,0,0,$m, 1, date('Y')));
				$this_month = date('m');
	     		$this_year = date('Y');

	     		$total_paid = $this->accounts_model->get_months_last_debt($lease_id,$invoice_month_number);
	     		$last_bal = $this->accounts_model->get_months_last_arrears($invoice_month_number,$this_year,$lease_id);

	     		$months_invoice_query = $this->accounts_model->get_months_invoices($lease_id,$invoice_month_number);

	     		$total_rental_bill = 0;
	     		$total_service_charge = 0;
	     		foreach ($months_invoice_query->result() as $invoice_key) {

	     			$invoice_type = $invoice_key->invoice_type;
	     			if($invoice_type == 1)
	     			{
	     				$rental_invoice_amount = $invoice_key->invoice_amount;
	     				// rental bill
	     				 $total_rental_bill = $total_rental_bill + $rental_invoice_amount;
	     			}
	     			else
	     			{
	     				$service_charge_amount = $invoice_key->invoice_amount;
	     				// service charge
	     				 $total_service_charge = $total_service_charge + $service_charge_amount;
	     			}

	     			$total_bill = $total_bill + ($total_rental_bill +$total_service_charge);
	     		}

			}
		}
		return $total_bill;
	}
	public function get_total_payments($lease_id)
	{
		$where = 'lease_id = '.$lease_id.' AND payment_status = 1 ';
		// var_dump($where);die();
		$this->db->from('payments');
		// $this->db->select('balance_cf AS balance');
		$this->db->select('SUM(amount_paid) AS total_paid');
		$this->db->where($where);
		$this->db->order_by('lease_id', 'DESC');
		$this->db->limit(1);
		$query = $this->db->get();
		$total_paid = 0;

		foreach ($query->result() as $key) {
			# code...
			$total_paid = $key->total_paid;
		}
		if($total_paid == NULL)
		{
			$total_paid = 0;
		}

		return $total_paid;
	}

	public function get_service_charge_debt($lease_id,$month_from,$month_to,$year_from,$year_to,$charge_type)
	{
		$where = 'lease_id = '.$lease_id.' AND invoice_type_id = '.$charge_type.'  AND payment_item_status = 1 AND payment_month BETWEEN "'.$month_from.'" AND "'.$month_from.'"  AND payment_year BETWEEN "'.$year_from.'" AND "'.$year_to.'"';
		// var_dump($where);die();
		$this->db->from('payment_item');
		// $this->db->select('balance_cf AS balance');
		$this->db->select('SUM(amount_paid) AS total_paid');
		$this->db->where($where);
		$this->db->order_by('lease_id', 'DESC');
		$this->db->limit(1);
		$query = $this->db->get();
		$total_paid = 0;

		foreach ($query->result() as $key) {
			# code...
			$total_paid = $key->total_paid;
		}
		if($total_paid == NULL)
		{
			$total_paid = 0;
		}

		return $total_paid;
	}

	public function get_service_charge_debt_home_owner($rental_unit_id,$month,$year,$charge_type)
	{
		$where = 'rental_unit_id = '.$rental_unit_id.' AND invoice_type_id = '.$charge_type.'  AND payment_item_status = 1 AND payment_month = "'.$month.'" AND payment_year = "'.$year.'"';
		// var_dump($where);die();
		$this->db->from('home_owner_payment_item');
		$this->db->select('SUM(amount_paid) AS total_paid');
		$this->db->where($where);
		$this->db->order_by('rental_unit_id', 'DESC');
		$this->db->limit(1);
		$query = $this->db->get();
		$total_paid = 0;

		foreach ($query->result() as $key) {
			# code...
			$total_paid = $key->total_paid;
		}
		if($total_paid == NULL)
		{
			$total_paid = 0;
		}

		return $total_paid;
	}
	public function get_cummulated_balance($lease_id,$invoice_type_id)
	{
		$current_date = date('Y-m-d');

		$date = explode('-', $current_date);
		$month = $date[1];
		$year = $date[0];

		$this->db->from('invoice');
		$this->db->select('*');
		$this->db->where('invoice_month = "'.$month.'" and invoice_year = '.$year.' AND lease_id = '.$lease_id.' AND invoice_type = '.$invoice_type_id);
		$this->db->order_by('invoice_month','DESC');
		$this->db->limit(1);
		$invoice_query2 = $this->db->get();

		$invoice_amount = 0;
		// var_dump($invoice_query2->num_rows());
		if($invoice_query2->num_rows() > 0)
		{
			foreach ($invoice_query2->result() as $prev_key) {
				$invoice_amount = $invoice_amount + $prev_key->invoice_amount;
			}
		}
		return $invoice_amount;
	}
	public function get_cummulated_balance_home_owners($rental_unit_id,$invoice_type_id)
	{
		$current_date = date('Y-m-d');

		$date = explode('-', $current_date);
		$month = $date[1];
		$year = $date[0];

		$this->db->from('home_owners_invoice');
		$this->db->select('*');
		$this->db->where('invoice_month = "'.$month.'" and invoice_year = '.$year.' AND rental_unit_id = '.$rental_unit_id.' AND invoice_type = '.$invoice_type_id);
		$this->db->order_by('invoice_month','DESC');
		$this->db->limit(1);
		$invoice_query2 = $this->db->get();

		$invoice_amount = 0;
		// var_dump($invoice_query2->num_rows());
		if($invoice_query2->num_rows() > 0)
		{
			foreach ($invoice_query2->result() as $prev_key) {
				$invoice_amount = $invoice_amount + $prev_key->invoice_amount;
			}
		}
		return $invoice_amount;
	}
	public function get_payments_detail($payment_id,$invoice_type_id)
	{
		$this->db->select("amount_paid");
		$this->db->where('payment_id = '.$payment_id.'  AND invoice_type_id = '.$invoice_type_id.'');
		$query = $this->db->get('payment_item');
		if($query->num_rows())
		{
			foreach ($query->result() as $key) {
				# code...
				$amount_paid = $key->amount_paid;
			}
		}
		else
		{
			$amount_paid = 0;
		}
		return $amount_paid;

	}
	public function get_owners_payments_detail($payment_id,$invoice_type_id)
	{
		$this->db->select("amount_paid");
		$this->db->where('payment_id = '.$payment_id.' AND invoice_type_id = '.$invoice_type_id.'');
		$query = $this->db->get('home_owner_payment_item');
		if($query->num_rows())
		{
			foreach ($query->result() as $key) {
				# code...
				$amount_paid = $key->amount_paid;
			}
		}
		else
		{
			$amount_paid = 0;
		}
		return $amount_paid;
	}
	public function get_invoiced_amount($invoice_type_id)
	{
		$this->db->select("invoice_amount");
		$this->db->where('invoice_status = 1 AND invoice_type = '.$invoice_type_id.'');
		$this->db->order_by("invoice_id",'DESC');
		$this->db->limit(1);
		$query = $this->db->get('invoice');
		if($query->num_rows())
		{
			foreach ($query->result() as $key) {
				# code...
				$invoice_amount = $key->invoice_amount;
			}
		}
		else
		{
			$invoice_amount = 0;
		}
		return $invoice_amount;
	}
	public function get_owners_invoiced_amount($invoice_type_id)
	{
		$this->db->select("invoice_amount");
		$this->db->where('invoice_status = 1 AND invoice_type = '.$invoice_type_id.'');
		$this->db->order_by("invoice_id",'DESC');
		$this->db->limit(1);
		$query = $this->db->get('home_owners_invoice');
		if($query->num_rows())
		{
			foreach ($query->result() as $key) {
				# code...
				$invoice_amount = $key->invoice_amount;
			}
		}
		else
		{
			$invoice_amount = 0;
		}
		return $invoice_amount;
	}
	public function get_payments_amount($invoice_type_id)
	{
		$this->db->select("SUM(amount_paid) AS amount_paid");
		$this->db->where('invoice_type_id = '.$invoice_type_id.'');
		$query = $this->db->get('payment_item');
		if($query->num_rows())
		{
			foreach ($query->result() as $key) {
				# code...
				$amount_paid = $key->amount_paid;
			}
		}
		else
		{
			$amount_paid = 0;
		}
		return $amount_paid;
	}
	public function get_owners_payments_amount($invoice_type_id)
	{
		$this->db->select("SUM(amount_paid) AS amount_paid");
		$this->db->where('invoice_type_id = '.$invoice_type_id.'');
		$query = $this->db->get('home_owner_payment_item');
		if($query->num_rows())
		{
			foreach ($query->result() as $key) {
				# code...
				$amount_paid = $key->amount_paid;
			}
		}
		else
		{
			$amount_paid = 0;
		}
		return $amount_paid;
	}

	public function get_invoice_amount($invoice_id,$invoice_type_id)
	{
		$this->db->select("invoice_amount AS amount_paid");
		$this->db->where('lease_invoice.lease_invoice_id = invoice.lease_invoice_id AND lease_invoice.invoice_deleted = 0 AND invoice.lease_invoice_id = '.$invoice_id.' AND invoice_type = '.$invoice_type_id);
		$query = $this->db->get('invoice,lease_invoice');
		if($query->num_rows())
		{
			foreach ($query->result() as $key) {
				# code...
				$amount_paid = $key->amount_paid;
			}
		}
		else
		{
			$amount_paid = 0;
		}
		return $amount_paid;
	}

	public function get_sum_invoice_amount_new($payment_date,$invoice_type_id,$lease_id,$year,$month,$payment_id)
	{

	  $invoice_date_checked = ' AND (invoice.year <  "'.$year.'" OR (invoice.year = '.$year.' AND invoice.month <= "'.$month.'") )';

		$this->db->select("SUM(invoice_amount)  AS debits");
		$this->db->where('invoice.lease_invoice_id = lease_invoice.lease_invoice_id AND lease_invoice.lease_id = '.$lease_id.' AND invoice.invoice_type = '.$invoice_type_id.$invoice_date_checked);
		$query = $this->db->get('lease_invoice,invoice');
		if($query->num_rows())
		{
			foreach ($query->result() as $key) {
				# code...
				$invoice_amount = $key->debits;
			}
		}
		else
		{
			$invoice_amount = 0;
		}

		$invoice_date_checked = ' AND (debit_note_item.debit_note_year <  "'.$year.'" OR (debit_note_item.debit_note_year = '.$year.' AND debit_note_item.debit_note_month <= "'.$month.'") ) ';
	
		$where = 'debit_notes.debit_note_id = debit_note_item.debit_note_id AND debit_note_item.lease_id = '.$lease_id.' and debit_note_item.invoice_type_id =  '.$invoice_type_id.$invoice_date_checked;
		$this->db->from('debit_note_item,debit_notes');
		$this->db->select('SUM(debit_note_item.debit_note_amount) AS total_debit_note');
		$this->db->where($where);
		$query = $this->db->get();
		$total_debit_note = 0;
		if($query->num_rows() >0)
		{
			$row=$query->row();
			$total_debit_note = $row->total_debit_note;
		}


		$invoice_date_checked = ' AND (payment_item.payment_year <  "'.$year.'" OR (payment_item.payment_year = '.$year.' AND payment_item.payment_month <= "'.$month.'") )';

		$this->db->select("SUM(payment_item.amount_paid)  AS debits");
		$this->db->where('payment_item.payment_id = payments.payment_id AND payment_item.payment_id < '.$payment_id.' AND payments.lease_id = '.$lease_id.'  AND payment_item.invoice_type_id = '.$invoice_type_id.$invoice_date_checked);
		$query = $this->db->get('payments,payment_item');
		if($query->num_rows())
		{
			foreach ($query->result() as $key) {
				# code...
				$payment_amount = $key->debits;
			}
		}
		else
		{
			$payment_amount = 0;
		}

	
		$invoice_date_checked = ' AND (credit_note_item.credit_note_year <  "'.$year.'" OR (credit_note_item.credit_note_year = '.$year.' AND credit_note_item.credit_note_month <= "'.$month.'") )';

		$where = 'credit_notes.credit_note_id = credit_note_item.credit_note_id AND credit_note_item.lease_id = '.$lease_id.'  and credit_note_item.invoice_type_id =  '.$invoice_type_id.$invoice_date_checked;
		$this->db->from('credit_note_item,credit_notes');
		$this->db->select('SUM(credit_note_item.credit_note_amount) AS credit_note_amount');
		$this->db->where($where);
		$query = $this->db->get();
		$credit_note_amount = 0;
		if($query->num_rows() >0)
		{
			$row=$query->row();
			$credit_note_amount = $row->credit_note_amount;
		}

		return ($invoice_amount + $total_debit_note) - ($payment_amount + $credit_note_amount);
	}


	public function get_sum_invoice_amount($payment_date,$invoice_type_id,$lease_id)
	{
		$this->db->select("SUM(dr_amount) - SUM(cr_amount) AS debits");
		$this->db->where('transactionDate < "'.$payment_date.'" AND lease_id = '.$lease_id.' AND accountId = '.$invoice_type_id);
		$query = $this->db->get('v_transactions');
		if($query->num_rows())
		{
			foreach ($query->result() as $key) {
				# code...
				$invoice_amount = $key->debits;
			}
		}
		else
		{
			$invoice_amount = 0;
		}


		//
		// $this->db->select("SUM(cr_amount) AS credits");
		// $this->db->where('transactionDate < "'.$payment_date.'" AND lease_id = '.$lease_id.'  AND accountId = '.$invoice_type_id);
		// $query = $this->db->get('v_transactions');
		// if($query->num_rows())
		// {
		// 	foreach ($query->result() as $key) {
		// 		# code...
		// 		$payment_amount = $key->credits;
		// 	}
		// }
		// else
		// {
		// 	$payment_amount = 0;
		// }


		return $invoice_amount;
	}



	public function get_payments_invoice_types($payment_id)
	{
		$this->db->select("invoice_type.invoice_type_id,invoice_type.invoice_type_name,invoice_id,payment_item.remarks,payment_item.payment_item_id,payment_item.amount_paid,payment_item.payment_year,payment_item.payment_month,payment_item.payment_id");
		$this->db->where('payment_item.invoice_type_id = invoice_type.invoice_type_id AND payment_item.payment_id = '.$payment_id.'');
		$query = $this->db->get('invoice_type,payment_item');
		return $query;
	}

	public function get_owners_payments_invoice_types($payment_id)
	{
		$this->db->select("invoice_type.invoice_type_id,invoice_type.invoice_type_name");
		$this->db->where('invoice_type.invoice_type_id IN(SELECT invoice_type_id FROM home_owner_payment_item WHERE payment_id = '.$payment_id.')');
		$query = $this->db->get('invoice_type');
		return $query;
	}

	public function get_owners_property_invoice_types($property_id,$charge_to)
	{
		$this->db->select("invoice_type.invoice_type_id,invoice_type.invoice_type_name");
		$this->db->where('invoice_type.invoice_type_id IN(SELECT invoice_type_id FROM property_billing WHERE property_id = '.$property_id.' AND charge_to= '.$charge_to.')');
		$query = $this->db->get('invoice_type');
		return $query;
	}

	public function get_statement_items($lease_id)
	{
		$query = $this->db->query("SELECT X.* FROM ( SELECT 'INVOICE' AS PTYPE, `invoice_amount` AS amount,  `invoice_type` AS invoice_type_id, `invoice_date` AS date FROM `invoice` WHERE lease_id = ".$lease_id." UNION SELECT 'PAYMENT' AS PTYPE, `amount_paid` AS amount, 'invoice_type' AS  invoice_type_id, `payment_date` AS date FROM `payments` WHERE lease_id = ".$lease_id." ) X ORDER BY X.`date` ASC");
		return $query;
	}
	public function get_invoice_type_name($invoice_type_id)
	{
		$this->db->select("invoice_type.invoice_type_name");
		$this->db->where('invoice_type.invoice_type_id = '.$invoice_type_id.'');
		$query = $this->db->get('invoice_type');
		$invoice_type_name = '';
		if($query->num_rows())
		{
			foreach ($query->result() as $key) {
				# code...
				$invoice_type_name = $key->invoice_type_name;
			}
		}
		return $invoice_type_name;
	}
	public function get_invoices_month($lease_id,$month,$year,$lease_invoice_id=null)
	{
		$where_date = '';
		if(!empty($lease_invoice_id))
		{
			$where_date = ' AND lease_invoice.lease_invoice_id = "'.$lease_invoice_id.'" ';
		}

		 $where = 'lease_invoice.lease_id = '.$lease_id.' AND lease_invoice.invoice_month = "'.$month.'" AND lease_invoice.invoice_year= "'.$year.'" '.$where_date.' AND lease_invoice.lease_invoice_id = invoice.lease_invoice_id AND invoice_type.invoice_type_id = invoice.invoice_type';
		$this->db->select("*");
		$this->db->where($where);
		$this->db->group_by('invoice.invoice_type');
		$query = $this->db->get('invoice,invoice_type,lease_invoice');
		return $query;

	}

	public function get_total_invoices_per_month($lease_id)
	{
		 $where = 'lease_id = '.$lease_id.' AND invoice_type.invoice_type_id = property_billing.invoice_type_id';
		$this->db->select("*");
		$this->db->where($where);
		$query = $this->db->get('property_billing,invoice_type');
		return $query;

	}
	public function get_payments_month($lease_id,$month,$year)
	{
		 $where = 'payments.lease_id = '.$lease_id.' AND payments.payment_id = payment_item.payment_id AND payments.month = "'.$month.'" AND payments.year = "'.$year.'" AND invoice_type.invoice_type_id = payment_item.invoice_type_id';
		 // echo $where; die();
		$this->db->select("payments.payment_date,payments.amount_paid AS total_paid,payments.receipt_number,payments.paid_by, payment_item.*,invoice_type.*");
		$this->db->where($where);
		$query = $this->db->get('payments,payment_item,invoice_type');
		return $query;

	}
	public function get_payments_month_payment($payment_id,$invoice_type_id){
		 $where = 'payment_item.payment_id = '.$payment_id.' AND invoice_type.invoice_type_id = payment_item.invoice_type_id AND payment_item.invoice_type_id = '.$invoice_type_id;
		 // echo $where; die();
		$this->db->select("payment_item.*,invoice_type.*");
		$this->db->where($where);
		$query = $this->db->get('payment_item,invoice_type');

		// $where = 'payment_item.payment_id = '.$payment_id;
		//  // echo $where; die();
		// $this->db->select("payment_item.*,invoice_type.*");
		// $this->db->join('invoice_type', 'invoice_type.invoice_type_id = payment_item.invoice_type_id g', 'left');
		// $this->db->where($where);
		// $query = $this->db->get('payment_item');
		return $query;

	}
	public function get_owner_payments_month_payment($payment_id,$invoice_type_id){
		 $where = 'home_owner_payment_item.payment_id = '.$payment_id.' AND invoice_type.invoice_type_id = home_owner_payment_item.invoice_type_id AND home_owner_payment_item.invoice_type_id = '.$invoice_type_id;
		 // echo $where; die();
		$this->db->select("home_owner_payment_item.*,invoice_type.*");
		$this->db->where($where);
		$query = $this->db->get('home_owner_payment_item,invoice_type');

		// $where = 'payment_item.payment_id = '.$payment_id;
		//  // echo $where; die();
		// $this->db->select("payment_item.*,invoice_type.*");
		// $this->db->join('invoice_type', 'invoice_type.invoice_type_id = payment_item.invoice_type_id g', 'left');
		// $this->db->where($where);
		// $query = $this->db->get('payment_item');
		return $query;

	}
	public  function get_payments_done_month($lease_id,$month,$year)
	{
		 $where = 'payments.lease_id = '.$lease_id.' AND payments.month = "'.$month.'" AND payments.year = "'.$year.'"';
		 // echo $where; die();
		$this->db->select("*");
		$this->db->where($where);
		$query = $this->db->get('payments');
		return $query;

	}
	public  function get_owner_payments_done_month($rental_unit_id,$month,$year)
	{
		 $where = 'home_owners_payments.rental_unit_id = '.$rental_unit_id.' AND home_owners_payments.month = "'.$month.'" AND home_owners_payments.year = "'.$year.'"';
		 // echo $where; die();
		$this->db->select("*");
		$this->db->where($where);
		$query = $this->db->get('home_owners_payments');
		return $query;

	}
	public function get_invoices_month_home_owners($rental_unit_id,$month,$year)
	{
		$this->db->select("*");
		$this->db->where('rental_unit_id = '.$rental_unit_id.' AND invoice_month = "'.$month.'" AND invoice_year= "'.$year.'" AND invoice_type > 1');
		// $this->db->where('rental_unit_id = '.$rental_unit_id.' AND invoice_month = "'.$month.'" AND invoice_year= "'.$year.'" AND invoice_type > 1');
		$query = $this->db->get('home_owners_invoice');
		return $query;

	}
	public function get_water_readings($invoice_id)
	{

		$this->db->select("*");
		$this->db->where('water_management.invoice_id = '.$invoice_id.' AND invoice.invoice_id = water_management.invoice_id');
		$query = $this->db->get('water_management,invoice');
		return $query;

	}
	public function get_total_bill($invoice_id,$invoice_type_id,$invoice_date)
	{
		$this->db->select("property_invoice_amount");
		$this->db->where('property_invoice.property_invoice_id = invoice.property_invoice_id AND property_invoice.property_invoice_date = "'.$invoice_date.'" AND property_invoice.invoice_type_id = '.$invoice_type_id.' AND invoice.invoice_id = '.$invoice_id.'');
		$query = $this->db->get('invoice,property_invoice');

		$amount = 0;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key) {
				# code...
				$amount = $key->property_invoice_amount;
			}
		}
		return $amount;
	}
	public function get_all_invoices($month,$year)
	{
		$this->db->select("lease_id");
		$this->db->where('invoice.invoice_month = "'.$month.'" AND invoice.invoice_amount > 0 AND invoice.invoice_year = '.$year.' AND invoice.invoice_item_deleted = 0 AND sent_status = 0');
		$this->db->group_by("document_number");
		$query = $this->db->get('invoice');

		return $query;
	}

	public function get_all_owners_lists()
	{

		$home_owner_property_search = $this->session->userdata('home_owner_property_search');
		// var_dump($home_owner_property_search); die();
		$where = 'property.property_id = rental_unit.property_id AND home_owners.home_owner_id = home_owner_unit.home_owner_id AND home_owner_unit.rental_unit_id = rental_unit.rental_unit_id AND home_owner_unit.home_owner_unit_status =  1';

		if(!empty($home_owner_property_search))
		{
			$where .= $home_owner_property_search;
		}

		$this->db->select("*");
		$this->db->where($where);
		$this->db->group_by("home_owners.home_owner_id");

		$query = $this->db->get('rental_unit,property,home_owners,home_owner_unit');

		return $query;
	}
	public function get_client_latest_invoice($home_owner_id,$rental_unit_id)
	{

		// var_dump($home_owner_property_search); die();
		$where = 'property.property_id = rental_unit.property_id AND home_owners.home_owner_id = home_owner_unit.home_owner_id AND home_owner_unit.rental_unit_id = rental_unit.rental_unit_id AND home_owner_unit.home_owner_unit_status =  1  AND home_owner_unit.rental_unit_id = home_owners_invoice.rental_unit_id AND home_owners_invoice.invoice_status = 1 AND home_owners_invoice.rental_unit_id ='.$rental_unit_id;



		$this->db->select("*");
		$this->db->where($where);
		$this->db->group_by("home_owners_invoice.invoice_date");

		$query = $this->db->get('rental_unit,property,home_owners,home_owner_unit,home_owners_invoice
			');
		$invoice_date = FALSE;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key) {
				# code...
				$invoice_date = $key->invoice_date;
			}

		}

		return $invoice_date;

	}
	public function get_all_invoices_home_owners($month,$year)
	{

		$home_owner_property_search = $this->session->userdata('home_owner_property_search');
		// var_dump($home_owner_property_search); die();
		$where = 'property.property_id = rental_unit.property_id AND home_owners.home_owner_id = home_owner_unit.home_owner_id AND home_owner_unit.rental_unit_id = rental_unit.rental_unit_id AND home_owner_unit.rental_unit_id = home_owners_invoice.rental_unit_id AND home_owners_invoice.invoice_month = "'.$month.'" AND home_owners_invoice.invoice_amount > 0 AND home_owners_invoice.invoice_year = '.$year.'';

		if(!empty($home_owner_property_search))
		{
			$where .= $home_owner_property_search;

		}
		// var_dump($where); die();
		$this->db->select("home_owners.home_owner_id,home_owners_invoice.rental_unit_id");
		$this->db->where($where);
		$this->db->group_by("home_owners.home_owner_id");

		$query = $this->db->get('home_owners_invoice,rental_unit,property,home_owners,home_owner_unit');

		return $query;
	}
	public function send_receipt_notification($lease_id,$confirm_number,$payment_id,$amount)
	{
		$all_leases = $this->leases_model->get_lease_detail($lease_id);
		foreach ($all_leases->result() as $leases_row)
		{
			$lease_id = $leases_row->lease_id;
			$tenant_unit_id = $leases_row->tenant_unit_id;
			$property_name = $leases_row->property_name;
			// $units_name = $leases_row->units_name;
			$rental_unit_name = $leases_row->rental_unit_name;
			$tenant_name = $leases_row->tenant_name;
			$tenant_email = $leases_row->tenant_email;
			$tenant_phone_number = $leases_row->tenant_phone_number;
			$lease_start_date = $leases_row->lease_start_date;
			$lease_duration = $leases_row->lease_duration;
			$rent_amount = $leases_row->rent_amount;
			$lease_number = $leases_row->lease_number;
			$arrears_bf = $leases_row->arrears_bf;
			$rent_calculation = $leases_row->rent_calculation;
			$deposit = $leases_row->deposit;
			$deposit_ext = $leases_row->deposit_ext;
			$tenant_phone_number = $leases_row->tenant_phone_number;
			$tenant_national_id = $leases_row->tenant_national_id;
			$lease_status = $leases_row->lease_status;
			$tenant_status = $leases_row->tenant_status;
			$created = $leases_row->created;

			$lease_start_date = date('jS M Y',strtotime($lease_start_date));

			// $expiry_date  = date('jS M Y',strtotime($lease_start_date, mktime()) . " + 365 day");
			$expiry_date  = date('jS M Y', strtotime(''.$lease_start_date.'+1 years'));

		}

		$data = array('payment_id' => $payment_id, 'tenant_unit_id' => $tenant_unit_id, 'lease_id' => $lease_id);
		$data['contacts'] = $this->site_model->get_contacts();
		$data['lease_payments'] = $this->accounts_model->get_lease_payments($lease_id);

		$data['payment_details'] = $this->accounts_model->get_payment_details($payment_id);
		$data['payment_idd'] = $payment_id;
		$contacts = $data['contacts'];

		$html = $this->load->view('cash_office/receipt', $data, TRUE);

		$this->load->library('mpdf');
		$title = $confirm_number.'-'.$rental_unit_name.'-Receipt.pdf';
		$invoice_path = $this->invoice_path;
		$invoice = $invoice_path.'/'.$title;

		$mpdf=new mPDF();
		$mpdf->WriteHTML($html);
		$mpdf->Output($title, 'F');


		$message['subject'] = $tenant_name.' '.$rental_unit_name.' Receipt';
		$message['text'] = ' <p>Dear '.$tenant_name.' </p>';
		$sender_email = $contacts['email'];
		$shopping = "";
		$from = $contacts['email'];

		$button = '';
		if(!empty($tenant_email))
		{
			$sender['email'] = $contacts['email'];
			$sender['name'] = $contacts['company_name'];
			$receiver['email'] = $tenant_email;
			$receiver['name'] = $tenant_name;
			$payslip = $title;

			$response = $this->email_model->send_sendgrid_mail($receiver, $sender, $message, $payslip);

		}
		if(!empty($tenant_phone_number))
		{
			$date = date('jS M Y',strtotime(date('Y-m-d')));
			$message = ''.$confirm_number.', Your payment for '.$rental_unit_name.'  of KES. '.number_format($amount).' has been received. Thank you';
			$this->accounts_model->sms($tenant_phone_number,$message,$tenant_name);

		}

	}


	// home owners items

	public function get_this_months_payment_owners($rental_unit_id,$month)
	{

		$this->db->from('home_owners_payments');
		$this->db->select('*');
		$this->db->where('rental_unit_id = '.$rental_unit_id.' AND payment_status = 1 AND month = "'.$month.'"');
		$query = $this->db->get();
		return $query;

	}

	public function get_current_arrears_home_owners($rental_unit_id,$month=NULL,$year=NULL)
	{
		if(empty($month) || empty($year) || $month == NULL || $year == NULL)
		{
			$current_date = date('Y-m-d');
			$date = explode('-', $current_date);
			$month = $date[1];
			$year = $date[0];
		}
		// var_dump($year);

		if($month == 1 OR $month == 2 OR $month == 3 OR $month == 4 OR $month == 5 OR $month == 6 OR $month == 7 OR $month == 8 OR $month == 9)
		{
			$todaymonth = $month;
		}
		else
		{
			$todaymonth = $month;
		}
		// var_dump($todaymonth); die();

		$this->db->from('home_owners_invoice');
		$this->db->select('*');
		$this->db->where('invoice_month = "'.$todaymonth.'" and invoice_status = 1 and invoice_year = '.$year.' AND rental_unit_id = '.$rental_unit_id);
		$invoice_query2 = $this->db->get();

		$arrears_bf = 0;
		// var_dump($invoice_query2->num_rows());
		if($invoice_query2->num_rows() > 0)
		{
			foreach ($invoice_query2->result() as $prev_key) {
				$arrears_bf = $arrears_bf + $prev_key->arrears_bf;
			}
		}
		return $arrears_bf;
	}
	public function get_total_payments_home_owners($rental_unit_id)
	{
		$where = 'rental_unit_id = '.$rental_unit_id.' AND payment_status = 1 ';
		// var_dump($where);die();
		$this->db->from('home_owners_payments');
		// $this->db->select('balance_cf AS balance');
		$this->db->select('SUM(amount_paid) AS total_paid');
		$this->db->where($where);
		$this->db->order_by('rental_unit_id', 'DESC');
		$this->db->limit(1);
		$query = $this->db->get();
		$total_paid = 0;

		foreach ($query->result() as $key) {
			# code...
			$total_paid = $key->total_paid;
		}
		if($total_paid == NULL)
		{
			$total_paid = 0;
		}

		return $total_paid;
	}
	public function get_invoice_template($charge_to,$property_id)
	{
		$this->db->select('invoice_structure.invoice_file_name');
		$this->db->where('invoice_structure.invoice_structure_id = property_invoice_structure. invoice_structure_id AND property_invoice_structure.charge_to = '.$charge_to.' AND property_invoice_structure.property_id ='.$property_id);
		$query = $this->db->get('property_invoice_structure,invoice_structure');
		$file_name_item = 'default_invoice';
		if($query->num_rows()  == 1)
		{

			foreach($query->result() as $key) {
				# code...
				$file_name_item = $key->invoice_file_name;
			}
		}
		return $file_name_item;
	}
	public function get_rate_charged($invoice_type_id,$charge_to,$property_id)
	{
		$this->db->select('property_billing.billing_amount');
		$this->db->where('property_billing.charge_to = '.$charge_to.' AND property_billing.property_id ='.$property_id.' AND property_billing.invoice_type_id ='.$invoice_type_id);
		$query = $this->db->get('property_billing');
		$rate_amount = 0;
		if($query->num_rows()  == 1)
		{

			foreach($query->result() as $key) {
				# code...
				$rate_amount = $key->billing_amount;
			}
		}
		return $rate_amount;
	}

	public function update_payment_item($import_payment_id)
	{
		$items['amount_paid'] = $amount_paid = $this->input->post('amount_paid');
		$rental_unit_name = $this->input->post('rental_unit_name');

		$payment_code = $this->input->post('payment_code');
		$type_of_account = $this->input->post('type_of_account');
		$items['paid_by'] = $paid_by = $this->input->post('paid_by');
		$items['payment_date'] = $payment_date = $this->input->post('payment_date');

		$expleded = explode('-', $payment_date);

		$month = $expleded[1];
		$year = $expleded[0];


		// get the current active lease

		//  get the invoice code item
		$invoice_type_id = $this->get_payment_code_id($payment_code);

		$rental_unit_id = $this->get_rental_unit_id($rental_unit_name);

		$lease_id = $this->get_active_lease_id($rental_unit_id);

		$items['payment_method_id'] = 1;
		$items['personnel_id'] =  $this->session->userdata("personnel_id");
		$items['transaction_code'] = '';
		$items['payment_created'] = date("Y-m-d");
		$items['year'] = $year;
		$items['month'] = $month;
		$items['payment_created_by'] = $this->session->userdata("personnel_id");
		$invoice_number = $this->get_invoice_number();
		//
		// var_dump($rental_unit_id);die();

		if($type_of_account == 1 OR !empty($rental_unit_id))
		{
			$items['lease_id'] =  $lease_id;


			if($this->db->insert('payments', $items))
			{
				$payment_id = $this->db->insert_id();
				$service = array(
							'payment_id'=>$payment_id,
							'amount_paid'=> $amount_paid,
							'invoice_type_id' => $invoice_type_id,
							'payment_item_status' => 0,
							'payment_item_created' => date('Y-m-d')
						);
				$this->db->insert('payment_item',$service);


				//  get the paid month amount and year

				//  end of getting the paid amount month and money

				// start delete from the import payment table
				$update_array = array('import_payment_upload_deleted' => 1);
				$this->db->where('import_payment_id',$import_payment_id);
				$this->db->update('import_payment_upload', $update_array);
			}

		}
		else if($type_of_account == 0 OR !empty($lease_id))
		{
			$items['rental_unit_id'] =  $rental_unit_id;

			if($this->db->insert('home_owners_payments', $items))
			{
				$payment_id = $this->db->insert_id();
				$service = array(
							'payment_id'=>$payment_id,
							'amount_paid'=> $amount_paid,
							'invoice_type_id' => $invoice_type_id,
							'payment_item_status' => 0,
							'payment_item_created' => date('Y-m-d')
						);
				$this->db->insert('home_owner_payment_item',$service);

				$update_array = array('import_payment_upload_deleted' => 1);
				$this->db->where('import_payment_id',$import_payment_id);
				$this->db->update('import_payment_upload', $update_array);
			}
		}
	}
	public function check_if_sms_sent($rental_unit_id,$phone_number,$todays_year,$todays_month,$acoount_type,$sms_type = NULL)
	{
		if(!empty($sms_type))
		{
			$add = ' AND sms_type = '.$sms_type;
		}
		else
		{
			$add = '';
		}
		$this->db->select('*');
		$this->db->where('rental_unit_id = '.$rental_unit_id. ' AND phone_number = "'.$phone_number.'" AND sms_invoice_year = '.$todays_year.' AND sms_invoice_month = '.$todays_month.' AND type_of_account = '.$acoount_type.''.$add);
		$sms_query_sent = $this->db->get('sms');
		if($sms_query_sent->num_rows() > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	public function check_if_email_sent($rental_unit_id,$email,$todays_year,$todays_month,$acoount_type,$email_type = NULL)
	{
		if(!empty($email_type))
		{
			$add = ' AND email_type = '.$email_type;
		}
		else
		{
			$add = '';
		}
		$this->db->select('*');
		$this->db->where('rental_unit_id = '.$rental_unit_id. ' AND email= "'.$email.'" AND invoice_year = '.$todays_year.' AND invoice_month = "'.$todays_month.'" AND type_of_account = '.$acoount_type.''.$add);
		$sms_query_sent = $this->db->get('email');
		if($sms_query_sent->num_rows() > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	public function get_property_invoice_types($lease_id,$charge_to,$type=null)
	{
		if(!empty($type))
		{
			if($type == 1)
			{
				$add_where = ' AND property_billing.invoice_type_id <> 2';
			}
			else
			{
				$add_where = ' AND property_billing.invoice_type_id = 2';
			}

		}
		else
		{
			$add_where = '';
		}
		$this->db->from('property_billing,invoice_type');
		$this->db->select('*');
		$this->db->where('property_billing.invoice_type_id = invoice_type.invoice_type_id AND  property_billing.lease_id = '.$lease_id.'  AND property_billing.charge_to = '.$charge_to.' '.$add_where);

		return $this->db->get();



	}

	public function get_water_record_id($invoice_id)
	{
		$this->db->from('water_management');
		$this->db->select('*');
		$this->db->where('invoice_id = '.$invoice_id.'');

		$query = $this->db->get();
		$record_id = 0;

		if($query->num_rows() > 0)
		{

			foreach ($query->result() as $key) {
				# code...
				$record_id = $key->record_id;
			}

		}
		return $record_id;
	}

	public function update_water_management($record_id,$rate)
	{
		$current_reading = $this->input->post('water_curr_reading');
		$previous_reading = $this->input->post('water_prev_reading');

		$consumption = $current_reading - $previous_reading;

		$charge = $consumption * $rate;

		$update_array = array('current_reading'=>$current_reading,'prev_reading'=>$previous_reading,'units_consumed'=>$consumption,'total_due'=>$charge);
		$this->db->where('record_id',$record_id);
		if($this->db->update('water_management',$update_array))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	public function get_property_id($rental_unit_id)
	{
		$this->db->where('rental_unit_id ='.$rental_unit_id);
		$query = $this->db->get('rental_unit');

		$property_id = 0;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key) {
				# code...
				$property_id = $key->property_id;
			}
		}
		return $property_id;
	}
	public function get_properties_rental_units($property_id)
	{
		$this->db->where('property_id ='.$property_id);
		$query = $this->db->get('rental_unit');

		return $query;
	}
	public function get_all_owners_arrears($rental_unit_id)
	{
		$this->db->select('balance AS invoice_amount');
		$this->db->where('rental_unit_id ='.$rental_unit_id);
		$query = $this->db->get('home_owner_unit');
		$arrears_amt = 0;
		if($query->num_rows() > 0)
		{
			foreach($query->result () as $amts)
			{
				$arrears_amt = $amts->invoice_amount;
			}
		}
		return $arrears_amt;
	}
	// public function get_all_owners_arrears($rental_unit_id)
	// {
	// 	$this->db->select('invoice_amount');
	// 	$this->db->where('balance_bf = 1 AND rental_unit_id ='.$rental_unit_id);
	// 	$query = $this->db->get('home_owners_invoice');
	// 	$arrears_amt = 0;
	// 	if($query->num_rows() > 0)
	// 	{
	// 		foreach($query->result () as $amts)
	// 		{
	// 			$arrears_amt = $amts->invoice_amount;
	// 		}
	// 	}
	// 	return $arrears_amt;
	// }

	public function cancel_payment($payment_id)
	{
		$data = array(
			"cancel_action_id" => $this->input->post('cancel_action_id'),
			"cancel_description" => $this->input->post('cancel_description'),
			"cancelled_by" => $this->session->userdata('personnel_id'),
			"cancelled_date" => date("Y-m-d H:i:s"),
			"cancel" => 1
		);

		$this->db->where('payment_id', $payment_id);
		if($this->db->update('payments', $data))
		{
			return TRUE;
		}

		else
		{
			return FALSE;
		}
	}
	public function cancel_pardon($pardon_id)
	{
		$data = array(
			"cancel_action_id" => $this->input->post('cancel_action_id'),
			"cancel_description" => $this->input->post('cancel_description'),
			"cancelled_by" => $this->session->userdata('personnel_id'),
			"cancelled_date" => date("Y-m-d H:i:s"),
			"cancel" => 1,
			"pardon_delete" => 1
		);

		$this->db->where('pardon_id', $pardon_id);
		if($this->db->update('pardon_payments', $data))
		{
			return TRUE;
		}

		else
		{
			return FALSE;
		}
	}

	public function cancel_owners_pardon($pardon_id)
	{
		$data = array(
			"pardon_delete" => 1
		);

		$this->db->where('pardon_id', $pardon_id);
		if($this->db->update('pardon_owner_payments', $data))
		{
			return TRUE;
		}

		else
		{
			return FALSE;
		}
	}
	public function cancel_owner_payment($payment_id)
	{
		$data = array(
			"cancel_action_id" => $this->input->post('cancel_action_id'),
			"cancel_description" => $this->input->post('cancel_description'),
			"cancelled_by" => $this->session->userdata('personnel_id'),
			"cancelled_date" => date("Y-m-d H:i:s"),
			"cancel" => 1
		);

		$this->db->where('payment_id', $payment_id);
		if($this->db->update('home_owners_payments', $data))
		{
			return TRUE;
		}

		else
		{
			return FALSE;
		}
	}

	function get_quarter($i=0) {
		$y = date('Y');
		$m = date('m');
		if($i > 0) {
			for($x = 0; $x < $i; $x++) {
				if($m <= 3) { $y--; }
				$diff = $m % 3;
				$m = ($diff > 0) ? $m - $diff:$m-3;
				if($m == 0) { $m = 12; }
			}
		}
		switch($m) {
			case $m >= 1 && $m <= 3:
				$start = $y.'-01-01 00:00:01';
				$end = $y.'-03-31 00:00:00';
				break;
			case $m >= 4 && $m <= 6:
				$start = $y.'-04-01 00:00:01';
				$end = $y.'-06-30 00:00:00';
				break;
			case $m >= 7 && $m <= 9:
				$start = $y.'-07-01 00:00:01';
				$end = $y.'-09-30 00:00:00';
				break;
			case $m >= 10 && $m <= 12:
				$start = $y.'-10-01 00:00:01';
				$end = $y.'-12-31 00:00:00';
		    		break;
		}
		return array(
			'start' => $start,
			'end' => $end,
			'start_nix' => strtotime($start),
			'end_nix' => strtotime($end)
		);
	}

	public function get_prev_all_quater_payments($lease_id, $from_date ,$invoice_type_id = NULL)
	{
		if($invoice_type_id != NULL)
		{
			$add = 'AND payment_item.invoice_type_id = '.$invoice_type_id;
		}
		else
		{
			$add = '';
		}

		// $date = $invoice_year.'-'.$invoice_month.'-30';
		// $date = $invoice_year.'-08-30';
		// var_dump($date); die();
		$where = 'payments.lease_id = '.$lease_id.' AND payments.payment_id = payment_item.payment_id AND payments.payment_status = 1 AND cancel = 0 AND (payments.payment_date < "'.$from_date.'") '.$add;

		// var_dump($where); die();

		$this->db->from('payment_item,payments');
		$this->db->select('SUM(payment_item.amount_paid) AS total_paid');
		$this->db->where($where);
		$query = $this->db->get();
		$total_paid = 0;
		if($query->num_rows() >0)
		{
			$row=$query->row();
			$total_paid = $row->total_paid;
		}
		return $total_paid;
	}

	public function get_prev_all_owners_quater_payments($rental_unit_id, $from_date ,$invoice_type_id = NULL)
	{
		if($invoice_type_id != NULL)
		{
			$add = 'AND home_owner_payment_item.invoice_type_id = '.$invoice_type_id;
		}
		else
		{
			$add = '';
		}

		// $date = $invoice_year.'-'.$invoice_month.'-30';
		// $date = $invoice_year.'-08-30';
		// var_dump($date); die();
		$where = 'home_owners_payments.rental_unit_id = '.$rental_unit_id.' AND home_owners_payments.payment_id = home_owner_payment_item.payment_id AND home_owners_payments.payment_status = 1 AND cancel = 0 AND (home_owners_payments.payment_date < "'.$from_date.'") '.$add;

		// var_dump($where); die();

		$this->db->from('home_owner_payment_item,home_owners_payments');
		$this->db->select('SUM(home_owner_payment_item.amount_paid) AS total_paid');
		$this->db->where($where);
		$query = $this->db->get();
		$total_paid = 0;
		if($query->num_rows() >0)
		{
			$row=$query->row();
			$total_paid = $row->total_paid;
		}
		return $total_paid;
	}


	public function get_prev_quater_payments($lease_id, $start_date,$end_date,$invoice_type_id = NULL)
	{
		if($invoice_type_id != NULL)
		{
			$add = 'AND payment_item.invoice_type_id = '.$invoice_type_id;
		}
		else
		{
			$add = '';
		}

		// $date = $invoice_year.'-'.$invoice_month.'-30';
		// $date = $invoice_year.'-08-30';
		// var_dump($date); die();
		$where = 'payments.lease_id = '.$lease_id.' AND payments.payment_id = payment_item.payment_id AND payments.payment_status = 1 AND cancel = 0 AND payments.payment_date BETWEEN "'.$start_date.'" AND "'.$end_date.'" '.$add;

		// var_dump($where); die();

		$this->db->from('payment_item,payments');
		$this->db->select('SUM(payment_item.amount_paid) AS total_paid');
		$this->db->where($where);
		$query = $this->db->get();
		$total_paid = 0;
		if($query->num_rows() >0)
		{
			$row=$query->row();
			$total_paid = $row->total_paid;
		}
		return $total_paid;
	}

	public function get_prev_owners_quater_payments($rental_unit_id, $start_date,$end_date,$invoice_type_id = NULL)
	{
		if($invoice_type_id != NULL)
		{
			$add = 'AND home_owner_payment_item.invoice_type_id = '.$invoice_type_id;
		}
		else
		{
			$add = '';
		}

		// $date = $invoice_year.'-'.$invoice_month.'-30';
		// $date = $invoice_year.'-08-30';
		// var_dump($date); die();
		$where = 'home_owners_payments.rental_unit_id = '.$rental_unit_id.' AND home_owners_payments.payment_id = home_owner_payment_item.payment_id AND home_owners_payments.payment_status = 1 AND cancel = 0 AND home_owners_payments.payment_date BETWEEN "'.$start_date.'" AND "'.$end_date.'" '.$add;

		// var_dump($where); die();

		$this->db->from('home_owner_payment_item,home_owners_payments');
		$this->db->select('SUM(home_owner_payment_item.amount_paid) AS total_paid');
		$this->db->where($where);
		$query = $this->db->get();
		$total_paid = 0;
		if($query->num_rows() >0)
		{
			$row=$query->row();
			$total_paid = $row->total_paid;
		}
		return $total_paid;
	}
	public function get_curr_quater_payment_amount($lease_id, $start_date,$end_date,$invoice_type_id = NULL)
	{
		if($invoice_type_id != NULL)
		{
			$add = 'AND payment_item.invoice_type_id = '.$invoice_type_id;
		}
		else
		{
			$add = '';
		}

		// $date = $invoice_year.'-'.$invoice_month.'-30';
		// $date = $invoice_year.'-08-30';
		// var_dump($date); die();
		$where = 'payments.lease_id = '.$lease_id.' AND payments.payment_id = payment_item.payment_id AND payments.payment_status = 1 AND cancel = 0 AND payments.payment_date > "'.$start_date.'" '.$add;

		// var_dump($where); die();

		$this->db->from('payment_item,payments');
		$this->db->select('SUM(payment_item.amount_paid) AS total_paid');
		$this->db->where($where);
		$query = $this->db->get();
		$total_paid = 0;
		if($query->num_rows() >0)
		{
			$row=$query->row();
			$total_paid = $row->total_paid;
		}
		return $total_paid;
	}

	public function get_curr_quater_owners_payment_amount($rental_unit_id, $start_date,$end_date,$invoice_type_id = NULL)
	{
		if($invoice_type_id != NULL)
		{
			$add = 'AND home_owner_payment_item.invoice_type_id = '.$invoice_type_id;
		}
		else
		{
			$add = '';
		}

		// $date = $invoice_year.'-'.$invoice_month.'-30';
		// $date = $invoice_year.'-08-30';
		// var_dump($date); die();
		$where = 'home_owners_payments.rental_unit_id = '.$rental_unit_id.' AND home_owners_payments.payment_id = home_owner_payment_item.payment_id AND home_owners_payments.payment_status = 1 AND cancel = 0 AND home_owners_payments.payment_date > "'.$start_date.'" '.$add;

		// var_dump($where); die();

		$this->db->from('home_owner_payment_item,home_owners_payments');
		$this->db->select('SUM(home_owner_payment_item.amount_paid) AS total_paid');
		$this->db->where($where);
		$query = $this->db->get();
		$total_paid = 0;
		if($query->num_rows() >0)
		{
			$row=$query->row();
			$total_paid = $row->total_paid;
		}
		return $total_paid;
	}
	public function get_prev_quater_invoices($lease_id, $start_date,$end_date,$invoice_type_id = NULL)
	{
		if($invoice_type_id != NULL)
		{
			$add = 'AND invoice_type = '.$invoice_type_id;
		}
		else
		{
			$add = '';
		}
		// $date = $invoice_year.'-'.$invoice_month.'-30';

		$where = 'lease_id = '.$lease_id.' AND invoice_date BETWEEN  "'.$start_date.'" AND "'.$end_date.'" '.$add;


		$this->db->from('invoice');
		$this->db->select('SUM(invoice_amount) AS total_invoice');
		$this->db->where($where);
		$query = $this->db->get();
		$total_invoice = 0;
		if($query->num_rows() >0)
		{
			$row=$query->row();
			$total_invoice = $row->total_invoice;
		}
		return $total_invoice;
	}
	public function get_prev_owners_quater_invoices($rental_unit_id, $start_date,$end_date,$invoice_type_id = NULL)
	{
		if($invoice_type_id != NULL)
		{
			$add = 'AND invoice_type = '.$invoice_type_id;
		}
		else
		{
			$add = '';
		}
		// $date = $invoice_year.'-'.$invoice_month.'-30';

		$where = 'rental_unit_id = '.$rental_unit_id.' AND invoice_date BETWEEN  "'.$start_date.'" AND "'.$end_date.'" '.$add;


		$this->db->from('home_owners_invoice');
		$this->db->select('SUM(invoice_amount) AS total_invoice');
		$this->db->where($where);
		$query = $this->db->get();
		$total_invoice = 0;
		if($query->num_rows() >0)
		{
			$row=$query->row();
			$total_invoice = $row->total_invoice;
		}
		return $total_invoice;
	}

	public function get_prev_all_quater_invoices($lease_id, $from_date,$invoice_type_id = NULL)
	{
		if($invoice_type_id != NULL)
		{
			$add = 'AND invoice_type = '.$invoice_type_id;
		}
		else
		{
			$add = '';
		}
		// $date = $invoice_year.'-'.$invoice_month.'-30';

		$where = 'lease_id = '.$lease_id.' AND invoice_date < "'.$from_date.'"'.$add;
		// var_dump($where); die();


		$this->db->from('invoice');
		$this->db->select('SUM(invoice_amount) AS total_invoice');
		$this->db->where($where);
		$query = $this->db->get();
		$total_invoice = 0;
		if($query->num_rows() >0)
		{
			$row=$query->row();
			$total_invoice = $row->total_invoice;
		}
		return $total_invoice;
	}

	public function get_prev_all_owners_quater_invoices($rental_unit_id, $from_date,$invoice_type_id = NULL)
	{
		if($invoice_type_id != NULL)
		{
			$add = 'AND invoice_type = '.$invoice_type_id;
		}
		else
		{
			$add = '';
		}
		// $date = $invoice_year.'-'.$invoice_month.'-30';

		$where = 'rental_unit_id = '.$rental_unit_id.' AND invoice_date < "'.$from_date.'"'.$add;
		// var_dump($where); die();


		$this->db->from('home_owners_invoice');
		$this->db->select('SUM(invoice_amount) AS total_invoice');
		$this->db->where($where);
		$query = $this->db->get();
		$total_invoice = 0;
		if($query->num_rows() >0)
		{
			$row=$query->row();
			$total_invoice = $row->total_invoice;
		}
		return $total_invoice;
	}

	public function get_curr_quater_invoice_amount($lease_id, $start_date,$end_date,$invoice_type_id = NULL)
	{
		if($invoice_type_id != NULL)
		{
			$add = 'AND invoice_type = '.$invoice_type_id;
		}
		else
		{
			$add = '';
		}
		// $date = $invoice_year.'-'.$invoice_month.'-30';

		// $where = 'lease_id = '.$lease_id.' AND ( invoice_date >= "'.$start_date.'" AND invoice_date <= "'.$end_date.'") '.$add;
		$where = 'lease_id = '.$lease_id.' AND ( invoice_date >= "'.$start_date.'" ) '.$add;
		// var_dump($where); die();


		$this->db->from('invoice');
		$this->db->select('SUM(invoice_amount) AS total_invoice');
		$this->db->where($where);
		$query = $this->db->get();
		$total_invoice = 0;
		if($query->num_rows() >0)
		{
			$row=$query->row();
			$total_invoice = $row->total_invoice;
		}
		return $total_invoice;
	}

	public function get_curr_owners_quater_invoice_amount($rental_unit_id, $start_date,$end_date,$invoice_type_id = NULL)
	{
		if($invoice_type_id != NULL)
		{
			$add = 'AND invoice_type = '.$invoice_type_id;
		}
		else
		{
			$add = '';
		}
		// $date = $invoice_year.'-'.$invoice_month.'-30';

		// $where = 'rental_unit_id = '.$rental_unit_id.' AND ( invoice_date >= "'.$start_date.'" AND invoice_date <= "'.$end_date.'") '.$add;
		$where = 'rental_unit_id = '.$rental_unit_id.' AND ( invoice_date >= "'.$start_date.'" ) '.$add;
		// var_dump($where); die();


		$this->db->from('home_owners_invoice');
		$this->db->select('SUM(invoice_amount) AS total_invoice');
		$this->db->where($where);
		$query = $this->db->get();
		$total_invoice = 0;
		if($query->num_rows() >0)
		{
			$row=$query->row();
			$total_invoice = $row->total_invoice;
		}
		return $total_invoice;
	}




	// orners

	public function get_prev_quater_owners_payments($rental_unit_id, $start_date,$end_date,$invoice_type_id = NULL)
	{
		if($invoice_type_id != NULL)
		{
			$add = 'AND home_owner_payment_item.invoice_type_id = '.$invoice_type_id;
		}
		else
		{
			$add = '';
		}

		// $date = $invoice_year.'-'.$invoice_month.'-30';
		// $date = $invoice_year.'-08-30';
		// var_dump($date); die();
		$where = 'home_owners_payments.rental_unit_id = '.$rental_unit_id.' AND home_owners_payments.payment_id = home_owner_payment_item.payment_id AND home_owners_payments.payment_status = 1 AND cancel = 0 AND (home_owners_payments.payment_date <= "'.$end_date.'")'.$add;

		// var_dump($where); die();

		$this->db->from('home_owner_payment_item,home_owners_payments');
		$this->db->select('SUM(home_owner_payment_item.amount_paid) AS total_paid');
		$this->db->where($where);
		$query = $this->db->get();
		$total_paid = 0;
		if($query->num_rows() >0)
		{
			$row=$query->row();
			$total_paid = $row->total_paid;
		}
		return $total_paid;
	}

	public function get_prev_quater_owners_invoices($rental_unit_id, $start_date,$end_date,$invoice_type_id = NULL)
	{
		if($invoice_type_id != NULL)
		{
			$add = 'AND invoice_type = '.$invoice_type_id;
		}
		else
		{
			$add = '';
		}
		// $date = $invoice_year.'-'.$invoice_month.'-30';

		$where = 'rental_unit_id = '.$rental_unit_id.' AND invoice_date <= "'.$end_date.'"'.$add;
		// var_dump($where); die();


		$this->db->from('home_owners_invoice');
		$this->db->select('SUM(invoice_amount) AS total_invoice');
		$this->db->where($where);
		$query = $this->db->get();
		$total_invoice = 0;
		if($query->num_rows() >0)
		{
			$row=$query->row();
			$total_invoice = $row->total_invoice;
		}
		return $total_invoice;
	}

	public function get_curr_quater_owners_invoice_amount($rental_unit_id, $start_date,$end_date,$invoice_type_id = NULL)
	{
		if($invoice_type_id != NULL)
		{
			$add = 'AND invoice_type = '.$invoice_type_id;
		}
		else
		{
			$add = '';
		}
		// $date = $invoice_year.'-'.$invoice_month.'-30';

		$where = 'rental_unit_id = '.$rental_unit_id.' AND ( invoice_date >= "'.$start_date.'" AND invoice_date <= "'.$end_date.'") '.$add;
		// var_dump($where); die();


		$this->db->from('home_owners_invoice');
		$this->db->select('SUM(invoice_amount) AS total_invoice');
		$this->db->where($where);
		$query = $this->db->get();
		$total_invoice = 0;
		if($query->num_rows() >0)
		{
			$row=$query->row();
			$total_invoice = $row->total_invoice;
		}
		return $total_invoice;
	}


	public function get_lease_pardons_month($lease_id, $invoice_year,$invoice_month)
	{

		// $date = $invoice_year.'-'.$invoice_month.'-30';
		$date = $invoice_year.'-'.$invoice_month.'-30';
		// var_dump($date); die();
		$where = 'pardon_payments.lease_id = '.$lease_id.' AND pardon_payments.pardon_status = 1 AND pardon_delete = 0 AND pardon_payments.pardon_date < "'.$date.'" ';

		// var_dump($where); die();

		$this->db->from('pardon_payments');
		$this->db->select('SUM(pardon_amount) AS total_paid');
		$this->db->where($where);
		$query = $this->db->get();
		$total_paid = 0;
		if($query->num_rows() >0)
		{
			$row=$query->row();
			$total_paid = $row->total_paid;
		}
		return $total_paid;
	}


	// owners

	public function get_invoice_brought_forward($rental_unit_id, $invoice_date,$invoice_type_id = NULL)
	{
		if($invoice_type_id != NULL)
		{
			$add = 'AND invoice_type = '.$invoice_type_id;
		}
		else
		{
			$add = '';
		}
		// $date = $invoice_year.'-'.$invoice_month.'-30';

		$where = 'rental_unit_id = '.$rental_unit_id.' AND invoice_date < "'.$invoice_date.'"'.$add;
		// var_dump($where); die();


		$this->db->from('home_owners_invoice');
		$this->db->select('SUM(invoice_amount) AS total_invoice');
		$this->db->where($where);
		$query = $this->db->get();
		$total_invoice = 0;
		if($query->num_rows() >0)
		{
			$row=$query->row();
			$total_invoice = $row->total_invoice;
		}
		return $total_invoice;
	}
	public function get_paid_brought_forward($rental_unit_id,$invoice_date,$invoice_type_id = NULL)
	{
		if($invoice_type_id != NULL)
		{
			$add = 'AND home_owner_payment_item.invoice_type_id = '.$invoice_type_id;
		}
		else
		{
			$add = '';
		}

		// $date = $invoice_year.'-'.$invoice_month.'-30';
		// $date = $invoice_year.'-08-30';
		// var_dump($date); die();
		$where = 'home_owners_payments.rental_unit_id = '.$rental_unit_id.' AND home_owners_payments.payment_id = home_owner_payment_item.payment_id AND home_owners_payments.payment_status = 1 AND cancel = 0 AND (home_owners_payments.payment_date < "'.$invoice_date.'")'.$add;

		// var_dump($where); die();

		$this->db->from('home_owner_payment_item,home_owners_payments');
		$this->db->select('SUM(home_owner_payment_item.amount_paid) AS total_paid');
		$this->db->where($where);
		$query = $this->db->get();
		$total_paid = 0;
		if($query->num_rows() >0)
		{
			$row=$query->row();
			$total_paid = $row->total_paid;
		}
		return $total_paid;
	}
	public function get_invoice_current_forward($rental_unit_id, $invoice_date,$invoice_type_id = NULL)
	{
		if($invoice_type_id != NULL)
		{
			$add = 'AND invoice_type = '.$invoice_type_id;
		}
		else
		{
			$add = '';
		}
		// $date = $invoice_year.'-'.$invoice_month.'-30';

		$where = 'rental_unit_id = '.$rental_unit_id.' AND invoice_date >= "'.$invoice_date.'"'.$add;



		$this->db->from('home_owners_invoice');
		$this->db->select('SUM(invoice_amount) AS total_invoice');
		$this->db->where($where);
		$query = $this->db->get();
		$total_invoice = 0;
		if($query->num_rows() >0)
		{
			$row=$query->row();
			$total_invoice = $row->total_invoice;
		}
		return $total_invoice;
	}

	public function get_paid_current_forward($rental_unit_id,$invoice_date,$invoice_type_id = NULL)
	{
		if($invoice_type_id != NULL)
		{
			$add = 'AND home_owner_payment_item.invoice_type_id = '.$invoice_type_id;
		}
		else
		{
			$add = '';
		}

		// $date = $invoice_year.'-'.$invoice_month.'-30';
		// $date = $invoice_year.'-08-30';
		// var_dump($date); die();
		$where = 'home_owners_payments.rental_unit_id = '.$rental_unit_id.' AND home_owners_payments.payment_id = home_owner_payment_item.payment_id AND home_owners_payments.payment_status = 1 AND cancel = 0 AND (home_owners_payments.payment_date >= "'.$invoice_date.'")'.$add;

		// var_dump($where); die();

		$this->db->from('home_owner_payment_item,home_owners_payments');
		$this->db->select('SUM(home_owner_payment_item.amount_paid) AS total_paid');
		$this->db->where($where);
		$query = $this->db->get();
		$total_paid = 0;
		if($query->num_rows() >0)
		{
			$row=$query->row();
			$total_paid = $row->total_paid;
		}
		return $total_paid;
	}
	public function get_today_paid_current_forward($rental_unit_id,$invoice_date,$invoice_type_id = NULL)
	{
		if($invoice_type_id != NULL)
		{
			$add = 'AND home_owner_payment_item.invoice_type_id = '.$invoice_type_id;
		}
		else
		{
			$add = '';
		}

		// $date = $invoice_year.'-'.$invoice_month.'-30';
		// $date = $invoice_year.'-08-30';
		// var_dump($date); die();
		$where = 'home_owners_payments.rental_unit_id = '.$rental_unit_id.' AND home_owners_payments.payment_id = home_owner_payment_item.payment_id AND home_owners_payments.payment_status = 1 AND cancel = 0 AND (home_owners_payments.payment_date = "'.$invoice_date.'")'.$add;

		// var_dump($where); die();

		$this->db->from('home_owner_payment_item,home_owners_payments');
		$this->db->select('SUM(home_owner_payment_item.amount_paid) AS total_paid');
		$this->db->where($where);
		$query = $this->db->get();
		$total_paid = 0;
		if($query->num_rows() >0)
		{
			$row=$query->row();
			$total_paid = $row->total_paid;
		}
		return $total_paid;
	}
	// end of owners


	// teannts
	public function get_invoice_tenants_brought_forward($lease_id, $invoice_date=NULL,$invoice_type_id = NULL)
	{
		if($invoice_type_id != NULL)
		{
			// $add = 'AND invoice.invoice_type = '.$invoice_type_id;
			$add = '';
		}
		else
		{
			$add = '';
		}
		// $date = $invoice_year.'-'.$invoice_month.'-30';
		if(!empty($invoice_date))
		{

			$date_add = ' AND lease_invoice.invoice_date < "'.$invoice_date.'"';

		}
		else
		{
			$date_add = '';
		}

		// $where = 'lease_invoice.lease_invoice_id = invoice.lease_invoice_id AND lease_invoice.lease_id = '.$lease_id.' '.$date_add.' '.$add;
		// $this->db->from('invoice,lease_invoice');
		// $this->db->select('SUM(invoice.invoice_amount) AS total_invoice');

		$where = 'invoice.lease_invoice_id = lease_invoice.lease_invoice_id AND lease_invoice.lease_id = '.$lease_id.' '.$date_add.' '.$add;
		$this->db->from('lease_invoice,invoice');
		$this->db->select('SUM(invoice.invoice_amount) AS total_invoice');
		$this->db->where($where);
		$query = $this->db->get();
		$total_invoice = 0;
		if($query->num_rows() >0)
		{
			$row=$query->row();
			$total_invoice = $row->total_invoice;
		}
		return $total_invoice;
	}

	public function get_invoice_owners_brought_forward($rental_unit_id, $invoice_date,$invoice_type_id = NULL)
	{
		if($invoice_type_id != NULL)
		{
			$add = 'AND invoice_type = '.$invoice_type_id;
		}
		else
		{
			$add = '';
		}
		// $date = $invoice_year.'-'.$invoice_month.'-30';

		$where = 'rental_unit_id = '.$rental_unit_id.' AND invoice_date < "'.$invoice_date.'"'.$add;
		// var_dump($where); die();


		$this->db->from('home_owners_invoice');
		$this->db->select('SUM(invoice_amount) AS total_invoice');
		$this->db->where($where);
		$query = $this->db->get();
		$total_invoice = 0;
		if($query->num_rows() >0)
		{
			$row=$query->row();
			$total_invoice = $row->total_invoice;
		}
		return $total_invoice;
	}

	public function get_paid_tenants_brought_forward_report($lease_id,$invoice_date=NULL,$invoice_type_id = NULL)
	{



		if($invoice_type_id != NULL)
		{

			$add = 'AND payment_item.invoice_type_id = '.$invoice_type_id;
			$date_add = 'AND payments.payment_id = payment_item.payment_id AND (payment_item_created < "'.$invoice_date.'")';
			$table_add = 'payment_item,payments';
			$amount_cal = 'SUM(payments.amount_paid) AS total_paid';
			$where = 'payments.lease_id = '.$lease_id.'   '.$date_add.' '.$add;
		}
		else
		{

			$add = ' AND payments.payment_status = 1 AND cancel = 0 ';
			$date_add = '  AND (payments.payment_date < "'.$invoice_date.'")';
			$table_add = 'payments';
			$amount_cal = 'SUM(payments.amount_paid) AS total_paid';
			$where = 'payments.lease_id = '.$lease_id.'   '.$date_add.' '.$add;
		}

		$this->db->from($table_add);
		$this->db->select($amount_cal);
		$this->db->where($where);
		$query = $this->db->get();
		$total_paid = 0;
		if($query->num_rows() >0)
		{
			$row=$query->row();
			$total_paid = $row->total_paid;
		}
		return $total_paid;
	}
	public function get_paid_tenants_brought_forward($lease_id,$invoice_date=NULL,$invoice_type_id = NULL)
	{
		if($invoice_type_id != NULL)
		{
			$add = 'AND payments.payment_id = payment_item.payment_id AND payment_item.invoice_type_id = '.$invoice_type_id;
			$table_add = 'payments,payment_item';
			$amount_cal = 'SUM(payment_item.amount_paid) AS total_paid';
		}
		else
		{
			$add = ' AND payments.payment_status = 1 AND cancel = 0 ';
			$table_add = 'payments';
			$amount_cal = 'SUM(payments.amount_paid) AS total_paid';
		}

		if(!empty($invoice_date))
		{
			$date_add = '  AND (payments.payment_date < "'.$invoice_date.'")';
		}
		else
		{
			$date_add = '';
		}
		$where = 'payments.lease_id = '.$lease_id.'   '.$date_add.' '.$add;



		$this->db->from($table_add);
		$this->db->select($amount_cal);
		$this->db->where($where);
		$query = $this->db->get();
		$total_paid = 0;
		if($query->num_rows() >0)
		{
			$row=$query->row();
			$total_paid = $row->total_paid;
		}
		return $total_paid;
	}


	public function get_pardons_tenants_brought_forward($lease_id,$invoice_date=NULL,$invoice_type_id = NULL)
	{
		if($invoice_type_id != NULL)
		{
			$add = 'AND credit_notes.credit_note_id = credit_note_item.credit_note_id AND credit_note_item.invoice_type_id = '.$invoice_type_id;
			$table_add = 'credit_notes,credit_note_item';
			$amount_cal = 'SUM(credit_note_item.credit_note_amount) AS total_paid';
		}
		else
		{
			$add = ' AND credit_notes.credit_note_status = 1 AND cancel = 0 ';
			$table_add = 'credit_notes';
			$amount_cal = 'SUM(credit_notes.credit_note_amount) AS total_paid';
		}

		if(!empty($invoice_date))
		{
			$date_add = '  AND (credit_notes.credit_note_date < "'.$invoice_date.'")';
		}
		else
		{
			$date_add = '';
		}
		$where = 'credit_notes.lease_id = '.$lease_id.'   '.$date_add.' '.$add;



		$this->db->from($table_add);
		$this->db->select($amount_cal);
		$this->db->where($where);
		$query = $this->db->get();
		$total_paid = 0;
		if($query->num_rows() >0)
		{
			$row=$query->row();
			$total_paid = $row->total_paid;
		}
		return $total_paid;
	}


	public function get_paid_tenants_brought_forward_item($lease_id,$invoice_date=NULL,$invoice_type_id = NULL)
	{

		if($invoice_type_id != NULL)
		{
			$add = ' AND payment_item.payment_item_status = 1 AND  payment_item.invoice_type_id = '.$invoice_type_id;
			$table_add = 'payment_item';
			$amount_cal = 'SUM(payment_item.amount_paid) AS total_paid';
		}
		else
		{
			$add = ' AND payments.payment_status = 1 AND cancel = 0 ';
			$table_add = 'payments';
			$amount_cal = 'SUM(payments.amount_paid) AS total_paid';
		}

		if(!empty($invoice_date))
		{
			$date_add = '  AND (payments.payment_date < "'.$invoice_date.'")';
		}
		else
		{
			$date_add = '';
		}
		$where = 'payment_item.lease_id = '.$lease_id.'   '.$date_add.' '.$add;

		// if($invoice_type_id == 13)
		// {
		// 	var_dump($where); die();
		// }


		$this->db->from($table_add);
		$this->db->select($amount_cal);
		$this->db->where($where);
		$query = $this->db->get();
		$total_paid = 0;
		if($query->num_rows() >0)
		{
			$row=$query->row();
			$total_paid = $row->total_paid;
		}
		return $total_paid;
	}
	public function get_paid_owners_brought_forward($rental_unit_id,$invoice_date,$invoice_type_id = NULL)
	{
		if($invoice_type_id != NULL)
		{
			$add = 'AND home_owners_payments.payment_id = home_owner_payment_item.payment_id  AND home_owner_payment_item.invoice_type_id = '.$invoice_type_id;
			$table_add = 'home_owners_payments,home_owner_payment_item';
			$amount_cal = 'SUM(home_owner_payment_item.amount_paid) AS total_paid';
		}
		else
		{
			$add = '';
			$table_add = 'home_owners_payments';
			$amount_cal = 'SUM(home_owners_payments.amount_paid) AS total_paid';
		}

		// $date = $invoice_year.'-'.$invoice_month.'-30';
		// $date = $invoice_year.'-08-30';
		// var_dump($date); die();
		$where = 'home_owners_payments.rental_unit_id = '.$rental_unit_id.'  AND home_owners_payments.payment_status = 1 AND cancel = 0 AND (home_owners_payments.payment_date < "'.$invoice_date.'")'.$add;

		// var_dump($where); die();
		$this->db->from($table_add);
		$this->db->select($amount_cal);
		$this->db->where($where);
		$query = $this->db->get();
		$total_paid = 0;
		if($query->num_rows() >0)
		{
			$row=$query->row();
			$total_paid = $row->total_paid;
		}
		return $total_paid;
	}
	public function get_invoice_tenants_current_forward($lease_id, $invoice_date,$invoice_type_id = NULL)
	{
		if($invoice_type_id != NULL)
		{
			$add = 'AND invoice.invoice_type = '.$invoice_type_id;
		}
		else
		{
			$add = '';
		}
		// $date = $invoice_year.'-'.$invoice_month.'-30';

		$where = 'lease_invoice.lease_invoice_id = invoice.lease_invoice_id AND lease_invoice.lease_id = '.$lease_id.' AND lease_invoice.invoice_date = "'.$invoice_date.'"'.$add;
		// var_dump($where); die();


		$this->db->from('invoice,lease_invoice');
		$this->db->select('SUM(invoice_amount) AS total_invoice');
		$this->db->where($where);
		$query = $this->db->get();
		$total_invoice = 0;
		if($query->num_rows() >0)
		{
			$row=$query->row();
			$total_invoice = $row->total_invoice;
		}
		return $total_invoice;
	}

	public function get_invoice_tenants_current_month_forward($lease_id, $invoice_month,$invoice_year,$invoice_type_id = NULL,$start_date=null,$end_date=null)
	{
		if($invoice_type_id != NULL)
		{
			$add = 'AND invoice_type = '.$invoice_type_id;
		}
		else
		{
			$add = '';
		}

		// if(!empty($start_date) AND !empty($end_date))
		// {
		// 	$invoice_date_checked = ' AND lease_invoice.invoice_date >= "'.$start_date.'" AND lease_invoice.invoice_date <= "'.$end_date.'" ';
		// }
		// else
		// {
		// 	$invoice_date_checked = ' AND lease_invoice.invoice_month = "'.$invoice_month.'" AND lease_invoice.invoice_year = "'.$invoice_year.'"';
		// }
		$invoice_date_checked = 'AND MONTH(created) = "'.$invoice_month.'" AND YEAR(created) = "'.$invoice_year.'" ';
		// $ex
		// $date = $invoice_year.'-'.$invoice_month.'-30';

		$where = 'invoice.lease_id = '.$lease_id.' '.$invoice_date_checked.' '.$add;
		// var_dump($where); die();


		$this->db->from('invoice');
		$this->db->select('SUM(invoice_amount) AS total_invoice');
		$this->db->where($where);
		$query = $this->db->get();
		$total_invoice = 0;
		if($query->num_rows() >0)
		{
			$row=$query->row();
			$total_invoice = $row->total_invoice;
		}
		return $total_invoice;
	}

	public function get_invoice_owners_current_forward($rental_unit_id, $invoice_date,$invoice_type_id = NULL)
	{
		if($invoice_type_id != NULL)
		{
			$add = 'AND invoice_type = '.$invoice_type_id;
		}
		else
		{
			$add = '';
		}
		// $date = $invoice_year.'-'.$invoice_month.'-30';

		$where = 'rental_unit_id = '.$rental_unit_id.' AND invoice_date = "'.$invoice_date.'"'.$add;
		// var_dump($where); die();


		$this->db->from('home_owners_invoice');
		$this->db->select('SUM(invoice_amount) AS total_invoice');
		$this->db->where($where);
		$query = $this->db->get();
		$total_invoice = 0;
		if($query->num_rows() >0)
		{
			$row=$query->row();
			$total_invoice = $row->total_invoice;
		}
		return $total_invoice;
	}

	public function get_paid_tenants_current_forward($lease_id,$invoice_date,$invoice_type_id = NULL)
	{
	if($invoice_type_id != NULL)
		{
			$add = 'AND payments.payment_id = payment_item.payment_id AND payment_item.invoice_type_id = '.$invoice_type_id;
			$table_add = 'payments,payment_item';
			$amount_cal = 'SUM(payment_item.amount_paid) AS total_paid';
		}
		else
		{
			$add = '';
			$table_add = 'payments';
			$amount_cal = 'SUM(payments.amount_paid) AS total_paid';
		}

		// $date = $invoice_year.'-'.$invoice_month.'-30';
		// $date = $invoice_year.'-08-30';
		// var_dump($date); die();
		$where = 'payments.lease_id = '.$lease_id.' AND payments.payment_status = 1 AND cancel = 0 AND (payments.payment_date >= "'.$invoice_date.'")'.$add;


		$this->db->from($table_add);
		$this->db->select($amount_cal);
		$this->db->where($where);

		$query = $this->db->get();
		$total_paid = 0;
		if($query->num_rows() >0)
		{
			$row=$query->row();
			$total_paid = $row->total_paid;
		}
		return $total_paid;
	}
	public function get_today_tenants_paid_current_forward($lease_id,$invoice_date,$invoice_type_id = NULL)
	{
		if($invoice_type_id != NULL)
		{
			$add = 'AND payment_item.invoice_type_id = '.$invoice_type_id;
		}
		else
		{
			$add = '';
		}

		// $date = $invoice_year.'-'.$invoice_month.'-30';
		// $date = $invoice_year.'-08-30';
		// var_dump($date); die();
		$where = 'payments.lease_id = '.$lease_id.' AND payments.payment_id = payment_item.payment_id AND payments.payment_status = 1 AND cancel = 0 AND (payments.payment_date = "'.$invoice_date.'")'.$add;

		// var_dump($where); die();

		$this->db->from('payment_item,payments');
		$this->db->select('SUM(payment_item.amount_paid) AS total_paid');
		$this->db->where($where);
		$query = $this->db->get();
		$total_paid = 0;
		if($query->num_rows() >0)
		{
			$row=$query->row();
			$total_paid = $row->total_paid;
		}
		return $total_paid;
	}

	public function get_today_tenants_paid_current_month_forward($lease_id,$invoice_month,$invoice_year,$invoice_type_id = NULL,$start_date = null,$end_date=null)
	{
		if($invoice_type_id != NULL)
		{
			$add = 'AND payment_item.invoice_type_id = '.$invoice_type_id;
		}
		else
		{
			$add = '';
		}

		if(!empty($invoice_month) AND !empty($invoice_year))
		{
			$invoice_date_checked = ' AND YEAR(payment_item_created) = "'.$invoice_year.'" AND MONTH(payment_item_created) = "'.$invoice_month.'" ';
		}
		else
		{
			$invoice_date_checked = '';
		}

		$where = 'payment_item.lease_id = '.$lease_id.' '.$invoice_date_checked.' '.$add;

		//var_dump($where); die();

		$this->db->from('payment_item');
		$this->db->select('SUM(payment_item.amount_paid) AS total_paid');
		$this->db->where($where);
		$query = $this->db->get();
		$total_paid = 0;
		if($query->num_rows() >0)
		{
			$row=$query->row();
			$total_paid = $row->total_paid;
		}
		return $total_paid;
	}
	public function get_today_tenants_paid_current_month_forward_old($lease_id,$invoice_month,$invoice_year,$invoice_type_id = NULL,$start_date = null,$end_date=null)
	{
		if($invoice_type_id != NULL)
		{
			$add = 'AND payment_item.invoice_type_id = '.$invoice_type_id;
		}
		else
		{
			$add = '';
		}

		if(!empty($invoice_month) AND !empty($invoice_year))
		{
			$invoice_date_checked = ' AND payments.year = "'.$invoice_year.'" AND payments.month = "'.$invoice_month.'" ';
		}
		else
		{
			$invoice_date_checked = '';
		}

		$where = 'payments.payment_id = payment_item.payment_id AND payments.lease_id = '.$lease_id.'  AND payments.payment_status = 1 AND cancel = 0 '.$invoice_date_checked.' '.$add;

		//var_dump($where); die();

		$this->db->from('payments,payment_item');
		$this->db->select('SUM(payment_item.amount_paid) AS total_paid');
		$this->db->where($where);
		$query = $this->db->get();
		$total_paid = 0;
		if($query->num_rows() >0)
		{
			$row=$query->row();
			$total_paid = $row->total_paid;
		}
		return $total_paid;
	}
	// end of tenants


	public function get_today_owners_paid_current_forward($rental_unit_id,$invoice_date,$invoice_type_id = NULL)
	{
		if($invoice_type_id != NULL)
		{
			$add = 'AND home_owner_payment_item.invoice_type_id = '.$invoice_type_id;
		}
		else
		{
			$add = '';
		}

		// $date = $invoice_year.'-'.$invoice_month.'-30';
		// $date = $invoice_year.'-08-30';
		// var_dump($date); die();
		$where = 'home_owners_payments.rental_unit_id = '.$rental_unit_id.' AND home_owners_payments.payment_id = home_owner_payment_item.payment_id AND home_owners_payments.payment_status = 1 AND cancel = 0 AND (home_owners_payments.payment_date = "'.$invoice_date.'")'.$add;

		// var_dump($where); die();

		$this->db->from('home_owner_payment_item,home_owners_payments');
		$this->db->select('SUM(home_owner_payment_item.amount_paid) AS total_paid');
		$this->db->where($where);
		$query = $this->db->get();
		$total_paid = 0;
		if($query->num_rows() >0)
		{
			$row=$query->row();
			$total_paid = $row->total_paid;
		}
		return $total_paid;
	}

	public function get_tenants_billings($lease_id)
	{
		$bills = $this->accounts_model->get_all_invoice_month($lease_id);

		$payments = $this->accounts_model->get_all_payments_lease($lease_id);
		$pardons = $this->accounts_model->get_all_pardons_lease($lease_id);

		// var_dump($payments); die();
		$x=0;

		$bills_result = '';
		$last_date = '';
		$current_year = date('Y');
		$total_invoices = $bills->num_rows();
		$invoices_count = 0;
		$total_invoice_balance = 0;
		$total_arrears = 0;
		$total_payment_amount = 0;
		$result = '';
		$total_pardon_amount = 0;
		if($bills->num_rows() > 0)
		{
			foreach ($bills->result() as $key_bills) {
				# code...
				$invoice_month = $key_bills->invoice_month;
			    $invoice_year = $key_bills->invoice_year;
				$invoice_date = $key_bills->invoice_date;
				$invoice_amount = $key_bills->total_invoice;
				$invoices_count++;
				if($payments->num_rows() > 0)
				{
					foreach ($payments->result() as $payments_key) {
						# code...
						$payment_date = $payments_key->payment_date;
						$payment_year = $payments_key->year;
						$payment_month = $payments_key->month;
						$payment_amount = $payments_key->amount_paid;

						if(($payment_date <= $invoice_date) && ($payment_date > $last_date) )
						{
							$total_arrears -= $payment_amount;
							// var_dump($payment_year); die();
							// if($payment_year >= $current_year)
							// {
								$result .=
								'
									<tr>
										<td>'.date('d M Y',strtotime($payment_date)).' </td>
										<td>Payment</td>
										<td></td>
										<td>'.number_format($payment_amount, 2).'</td>
										<td>'.number_format($total_arrears, 2).'</td>
										<td></td>
									</tr>
								';
							// }

							$total_payment_amount += $payment_amount;

						}
					}
				}
				if($pardons->num_rows() > 0)
				{
					foreach ($pardons->result() as $pardons_key) {
						# code...
						$pardon_date = $pardons_key->pardon_date;
						$pardon_explode = explode('-', $pardon_date);
						$pardon_year = $pardon_explode[0];
						$pardon_month = $pardon_explode[1];
						$pardon_amount = $pardons_key->pardon_amount;

						if(($pardon_date <= $invoice_date) && ($pardon_date > $last_date) && ($pardon_amount > 0))
						{
							$total_arrears -= $pardon_amount;
							// if($pardon_year >= $current_year)
							// {
								$result .=
								'
									<tr>
										<td>'.date('d M Y',strtotime($pardon_date)).' </td>
										<td>Pardon</td>
										<td></td>
										<td>'.number_format($pardon_amount, 2).'</td>
										<td>'.number_format($total_arrears, 2).'</td>
										<td></td>
									</tr>
								';
							// }

							$total_pardon_amount += $pardon_amount;

						}
					}
				}
				//display disbursment if cheque amount > 0
				if($invoice_amount != 0)
				{
					$total_arrears += $invoice_amount;
					$total_invoice_balance += $invoice_amount;

					// if($invoice_year >= $current_year)
					// {
						$result .=
						'
							<tr>
								<td>'.date('d M Y',strtotime($invoice_date)).' </td>
								<td>'.$invoice_month.' '.$invoice_year.' Invoice</td>
								<td>'.number_format($invoice_amount, 2).'</td>
								<td></td>
								<td>'.number_format($total_arrears, 2).'</td>
								<td><a href="'.site_url().'invoice/'.$lease_id.'/'.$invoice_month.'/'.$invoice_year.'/'.$invoice_date.'" target="_blank" class="btn btn-sm btn-warning">Invoice</a></td>
							</tr>
						';
					// }
				}

				//check if there are any more payments
				if($total_invoices == $invoices_count)
				{
					//get all loan deductions before date
					if($payments->num_rows() > 0)
					{
						foreach ($payments->result() as $payments_key) {
							# code...
							$payment_date = $payments_key->payment_date;
							$payment_year = $payments_key->year;
							$payment_month = $payments_key->month;
							$payment_amount = $payments_key->amount_paid;

							if(($payment_date > $invoice_date) &&  ($payment_amount > 0))
							{
								$total_arrears -= $payment_amount;
								// if($payment_year >= $current_year)
								// {
									$result .=
									'
										<tr>
											<td>'.date('d M Y',strtotime($payment_date)).' </td>
											<td>Payment</td>
											<td></td>
											<td>'.number_format($payment_amount, 2).'</td>
											<td>'.number_format($total_arrears, 2).'</td>
											<td></td>
										</tr>
									';
								// }

								$total_payment_amount += $payment_amount;

							}
						}
					}

					if($pardons->num_rows() > 0)
					{
						foreach ($pardons->result() as $pardons_key) {
							# code...
							$pardon_date = $pardons_key->pardon_date;
							$pardon_explode = explode('-', $pardon_date);
							$pardon_year = $pardon_explode[0];
							$pardon_month = $pardon_explode[1];
							$pardon_amount = $pardons_key->pardon_amount;

							if(($pardon_date > $invoice_date) &&  ($pardon_amount > 0))
							{
								$total_arrears -= $pardon_amount;
								// if($pardon_year >= $current_year)
								// {
									$result .=
									'
										<tr>
											<td>'.date('d M Y',strtotime($pardon_date)).' </td>
											<td>Pardon</td>
											<td></td>
											<td>'.number_format($pardon_amount, 2).'</td>
											<td>'.number_format($total_arrears, 2).'</td>
											<td></td>
										</tr>
									';
								// }

								$total_pardon_amount += $pardon_amount;

							}
						}
					}
				}
						$last_date = $invoice_date;
			}
		}
		else
		{
			//get all loan deductions before date
			if($payments->num_rows() > 0)
			{
				foreach ($payments->result() as $payments_key) {
					# code...
					$payment_date = $payments_key->payment_date;
					$payment_year = $payments_key->year;
					$payment_month = $payments_key->month;
					$payment_amount = $payments_key->amount_paid;

					if(($payment_amount > 0))
					{
						$total_arrears -= $payment_amount;
						// if($payment_year >= $current_year)
						// {
							$result .=
							'
								<tr>
									<td>'.date('d M Y',strtotime($payment_date)).' </td>
									<td>Payment</td>
									<td></td>
									<td>'.number_format($payment_amount, 2).'</td>
									<td>'.number_format($total_arrears, 2).'</td>
									<td></td>
								</tr>
							';
						// }

						$total_payment_amount += $payment_amount;

					}
				}
			}
			if($pardons->num_rows() > 0)
			{
				foreach ($pardons->result() as $pardons_key) {
					# code...
					$pardon_date = $pardons_key->pardon_date;
					$pardon_explode = explode('-', $pardon_date);
					$pardon_year = $pardon_explode[0];
					$pardon_month = $pardon_explode[1];
					$pardon_amount = $pardons_key->pardon_amount;

					if(($pardon_amount > 0))
					{
						$total_arrears -= $pardon_amount;
						// if($pardon_year >= $current_year)
						// {
							$result .=
							'
								<tr>
									<td>'.date('d M Y',strtotime($pardon_date)).' </td>
									<td>Pardon</td>
									<td></td>
									<td>'.number_format($pardon_amount, 2).'</td>
									<td>'.number_format($total_arrears, 2).'</td>
									<td></td>
								</tr>
							';
						// }

						$total_pardon_amount += $pardon_amount;

					}
				}
			}

		}

		//display loan
		$result .=
		'
			<tr>
				<th colspan="2">Total</th>
				<th>'.number_format($total_invoice_balance, 2).'</th>
				<th>'.number_format($total_payment_amount, 2).'</th>
				<th>'.number_format($total_arrears, 2).'</th>
				<td></td>
			</tr>
		';

		$invoice_date = $this->get_max_invoice_date($lease_id);

		$account_invoice_amount = $this->accounts_model->get_invoice_tenants_brought_forward($lease_id,$invoice_date);
		$account_paid_amount = $this->accounts_model->get_paid_tenants_brought_forward($lease_id,$invoice_date);
		$account_todays_payment =  $this->accounts_model->get_today_tenants_paid_current_forward($lease_id,$invoice_date);
		$total_brought_forward_account = $account_invoice_amount - $account_paid_amount;


		$response['total_arrears'] = $total_arrears;
		$response['invoice_date'] = $invoice_date;
		$response['result'] = $result;
		$response['total_invoice_balance'] = $total_brought_forward_account;
		$response['total_payment_amount'] = $total_payment_amount;
		$response['total_pardon_amount'] = $total_pardon_amount;

		// var_dump($response); die();

		return $response;
	}
	public function get_owners_billings($rental_unit_id,$home_owner_id)
	{


		$bills = $this->accounts_model->get_all_owners_invoice_month($rental_unit_id);
		$payments = $this->accounts_model->get_all_owners_payments_lease($rental_unit_id);
		$starting_arreas = $arrears_amount =$this->accounts_model->get_all_owners_arrears($rental_unit_id);
		$pardons = $this->accounts_model->get_all_owner_pardons_lease($rental_unit_id);

		// var_dump($bills->result()); die();

		$x=0;

		$bills_result = '';
		$last_date = '';
		$current_year = date('Y');
		$total_invoices = $bills->num_rows();
		$invoices_count = 0;
		$total_invoice_balance = 0 + $starting_arreas;
		$total_arrears = 0;
		$total_payment_amount = 0;
		$total_pardon_amount =0;
		$result = '';
		if($bills->num_rows() > 0)
		{
			foreach ($bills->result() as $key_bills) {
				# code...
				$invoice_month = $key_bills->invoice_month;
			    $invoice_year = $key_bills->invoice_year;
				$invoice_date = $key_bills->invoice_date;
				$invoice_amount = $key_bills->invoice_amount;
				$balance_bf_id = $key_bills->balance_bf;
				$document_number = $key_bills->document_number;
				$invoices_count++;

				if($payments->num_rows() > 0)
				{
					foreach ($payments->result() as $payments_key) {
						# code...
						$payment_date = $payments_key->payment_date;
						$payment_year = $payments_key->year;
						$payment_month = $payments_key->month;
						$payment_amount = $payments_key->amount_paid;

						if(($payment_date <= $invoice_date) && ($payment_date > $last_date)  && ($balance_bf_id !=1))
						{
							$arrears_amount -= $payment_amount;
							// if($payment_year >= $current_year)
							// {
								$result .=
								'
									<tr>
										<td>'.date('d M Y',strtotime($payment_date)).' </td>
										<td>Payment</td>
										<td></td>
										<td>'.number_format($payment_amount, 2).'</td>
										<td>'.number_format($arrears_amount, 2).'</td>
										<td></td>
									</tr>
								';
							// }

							$total_payment_amount += $payment_amount;

						}
					}
				}
				if($pardons->num_rows() > 0)
				{
					foreach ($pardons->result() as $pardons_key) {
						# code...
						$pardon_date = $pardons_key->pardon_date;
						$pardon_explode = explode('-', $pardon_date);
						$pardon_year = $pardon_explode[0];
						$pardon_month = $pardon_explode[1];
						$pardon_amount = $pardons_key->pardon_amount;

						if(($pardon_date <= $invoice_date) && ($pardon_date > $last_date) && ($pardon_amount > 0))
						{
							$total_arrears -= $pardon_amount;
							// if($pardon_year >= $current_year)
							// {
								$result .=
								'
									<tr>
										<td>'.date('d M Y',strtotime($pardon_date)).' </td>
										<td>Pardon</td>
										<td></td>
										<td>'.number_format($pardon_amount, 2).'</td>
										<td>'.number_format($total_arrears, 2).'</td>
										<td></td>
									</tr>
								';
							// }

							$total_pardon_amount += $pardon_amount;

						}
					}
				}

				//display disbursment if cheque amount > 0
				if($invoice_amount != 0)
				{
					$arrears_amount += $invoice_amount;
					$total_invoice_balance += $invoice_amount;


					// if($invoice_year >= $current_year)
					// {
						$result .=
						'
							<tr>
								<td>'.date('d M Y',strtotime($invoice_date)).' </td>
								<td>'.$document_number.' Invoice</td>
								<td>'.number_format($invoice_amount, 2).'</td>
								<td></td>
								<td>'.number_format($arrears_amount, 2).'</td>
								<td><a href="'.site_url().'owners-invoice/'.$home_owner_id.'/'.$rental_unit_id.'/'.$invoice_month.'/'.$invoice_year.'" target="_blank" class="btn btn-sm btn-warning pull-right"  style="margin-top:-5px;margin-right:5px;">Invoice</a></td>
							</tr>
						';
					// }
				}

				//check if there are any more payments
				if($total_invoices == $invoices_count)
				{
					//get all loan deductions before date
					if($payments->num_rows() > 0)
					{
						foreach ($payments->result() as $payments_key) {
							# code...
							$payment_date = $payments_key->payment_date;
							$payment_year = $payments_key->year;
							$payment_month = $payments_key->month;
							$payment_amount = $payments_key->amount_paid;

							if(($payment_date > $invoice_date) &&  ($payment_amount > 0))
							{
								$arrears_amount -= $payment_amount;
								// if($payment_year >= $current_year)
								// {
									$result .=
									'
										<tr>
											<td>'.date('d M Y',strtotime($payment_date)).' </td>
											<td>Payment</td>
											<td></td>
											<td>'.number_format($payment_amount, 2).'</td>
											<td>'.number_format($arrears_amount, 2).'</td>
											<td></td>
										</tr>
									';
								// }

								$total_payment_amount += $payment_amount;

							}
						}
					}
					if($pardons->num_rows() > 0)
					{
						foreach ($pardons->result() as $pardons_key) {
							# code...
							$pardon_date = $pardons_key->pardon_date;
							$pardon_explode = explode('-', $pardon_date);
							$pardon_year = $pardon_explode[0];
							$pardon_month = $pardon_explode[1];
							$pardon_amount = $pardons_key->pardon_amount;

							if(($pardon_date > $invoice_date) &&  ($pardon_amount > 0))
							{
								$total_arrears -= $pardon_amount;
								// if($pardon_year >= $current_year)
								// {
									$result .=
									'
										<tr>
											<td>'.date('d M Y',strtotime($pardon_date)).' </td>
											<td>Pardon</td>
											<td></td>
											<td>'.number_format($pardon_amount, 2).'</td>
											<td>'.number_format($total_arrears, 2).'</td>
											<td></td>
										</tr>
									';
								// }

								$total_pardon_amount += $pardon_amount;

							}
						}
					}
				}

						$last_date = $invoice_date;
			}
		}
		else
		{
			//get all loan deductions before date
			if($payments->num_rows() > 0)
			{
				foreach ($payments->result() as $payments_key) {
					# code...
					$payment_date = $payments_key->payment_date;
					$payment_year = $payments_key->year;
					$payment_month = $payments_key->month;
					$payment_amount = $payments_key->amount_paid;



					if(($payment_amount > 0))
					{
						$arrears_amount -= $payment_amount;
						// if($payment_year >= $current_year)
						// {
							$result .=
							'
								<tr>
									<td>'.date('d M Y',strtotime($payment_date)).' </td>
									<td>Payment</td>
									<td></td>
									<td>'.number_format($payment_amount, 2).'</td>
									<td>'.number_format($arrears_amount, 2).'</td>
									<td></td>
								</tr>
							';
						// }

						$total_payment_amount += $payment_amount;

					}
				}
			}
			if($pardons->num_rows() > 0)
			{
				foreach ($pardons->result() as $pardons_key) {
					# code...
					$pardon_date = $pardons_key->pardon_date;
					$pardon_explode = explode('-', $pardon_date);
					$pardon_year = $pardon_explode[0];
					$pardon_month = $pardon_explode[1];
					$pardon_amount = $pardons_key->pardon_amount;

					if(($pardon_date > $invoice_date) &&  ($pardon_amount > 0))
					{
						$total_arrears -= $pardon_amount;
						// if($pardon_year >= $current_year)
						// {
							$result .=
							'
								<tr>
									<td>'.date('d M Y',strtotime($pardon_date)).' </td>
									<td>Pardon</td>
									<td></td>
									<td>'.number_format($pardon_amount, 2).'</td>
									<td>'.number_format($total_arrears, 2).'</td>
									<td></td>
								</tr>
							';
						// }

						$total_pardon_amount += $pardon_amount;

					}
				}
			}


		}
		$parent_invoice_date = $this->accounts_model->get_max_owners_invoice_date($rental_unit_id);

		$account_invoice_amount = $this->accounts_model->get_invoice_owners_brought_forward($rental_unit_id,$parent_invoice_date);
		$account_paid_amount = $this->accounts_model->get_paid_owners_brought_forward($rental_unit_id,$parent_invoice_date);
		$account_todays_payment =  $this->accounts_model->get_today_owners_paid_current_forward($rental_unit_id,$parent_invoice_date);



		$total_brought_forward_account = $account_invoice_amount - $account_paid_amount + $starting_arreas ;

		//display loan
		$result .=
		'
			<tr>
				<th colspan="2">Total</th>
				<th>'.number_format($total_invoice_balance , 2).'</th>
				<th>'.number_format($total_payment_amount, 2).'</th>
				<th>'.number_format($arrears_amount, 2).'</th>
				<td></td>
			</tr>
		';
		if($rental_unit_id == 2026)
		{
			// var_dump($starting_arreas); die();
		}

		$response['total_arrears'] = $arrears_amount;
		$response['invoice_date'] = $invoice_date;
		$response['total_pardon_amount'] = $total_pardon_amount;
		$response['result'] = $result;
		$response['total_invoice_balance'] = $total_brought_forward_account;
		$response['total_payment_amount'] = $total_payment_amount;
		$response['starting_arreas'] = $starting_arreas;

		return $response;
	}

	public function get_max_invoice_date($lease_id,$invoice_month = NULL,$invoice_year=NULL)
	{
		if($invoice_month != NULL || $invoice_year != NULL)
		{
			$add = 'AND lease_invoice.invoice_month = "'.$invoice_month.'" AND lease_invoice.invoice_year = '.$invoice_year.'';
		}
		else
		{
			$add = '';
		}
		$where = 'lease_invoice.lease_id = '.$lease_id.' '.$add;

		$this->db->from('invoice,lease_invoice');
		$this->db->select('MAX(lease_invoice.invoice_date) AS invoice_date');
		$this->db->where($where);
		$query = $this->db->get();
		$invoice_date = '';
		if($query->num_rows() >0)
		{
			$row=$query->row();
			$invoice_date = $row->invoice_date;
		}
		return $invoice_date;
	}

	public function get_max_invoice_id($lease_id,$invoice_month = NULL,$invoice_year=NULL)
	{
		if($invoice_month != NULL || $invoice_year != NULL)
		{
			$add = 'AND invoice_month = "'.$invoice_month.'" AND invoice_year = '.$invoice_year.'';
		}
		else
		{
			$add = '';
		}
		$where = 'lease_id = '.$lease_id.' '.$add;

		$this->db->from('invoice');
		$this->db->select('MAX(invoice_id) AS invoice_id');
		$this->db->where($where);
		$query = $this->db->get();
		$invoice_id = '';
		if($query->num_rows() >0)
		{
			$row=$query->row();
			$invoice_id = $row->invoice_id;
		}
		return $invoice_id;
	}




	public function get_max_quater_invoice_date($lease_id,$invoicedate)
	{
		// start


		// So thats whats you do and you always refuse to have a conversation with me on video .. then you like show yourself to other people ......

		$todays_date = date('Y-m-d');
		$current_date = strtotime($todays_date);
		$current_quarter = ceil(date('m', $current_date) / 3);
		$current_month = ($current_quarter * 3) - 2;
		$current_year = date('Y');

		$curr_quarter = 'AC'.$current_quarter.'-'.$current_year;


		$exploded = explode(":", $invoicedate);
		$start_date = $exploded[0];
		$end_date = $exploded[1];
		if($start_date != NULL || $end_date != NULL)
		{
			$add = 'AND billing_schedule_quarter = "'.$curr_quarter.'" ';
		}
		else
		{
			$add = '';
		}
		$where = 'lease_id = '.$lease_id.' '.$add;

		// var_dump($where); die();

		$this->db->from('invoice');
		$this->db->select('MAX(invoice_date) AS invoice_date');
		$this->db->where($where);
		$query = $this->db->get();
		$invoice_date = '';
		if($query->num_rows() >0)
		{
			$row=$query->row();
			$invoice_date = $row->invoice_date;
		}
		return $invoice_date;
	}

	public function get_max_owner_invoice_date($rental_unit_id,$invoicedate)
	{
		// start


		// So thats whats you do and you always refuse to have a conversation with me on video .. then you like show yourself to other people ......

		$todays_date = date('Y-m-d');
		$current_date = strtotime($todays_date);
		$current_quarter = ceil(date('m', $current_date) / 3);
		$current_month = ($current_quarter * 3) - 2;
		$current_year = date('Y');

		$curr_quarter = 'AC'.$current_quarter.'-'.$current_year;


		$exploded = explode(":", $invoicedate);
		$start_date = $exploded[0];
		$end_date = $exploded[1];
		if($start_date != NULL || $end_date != NULL)
		{
			// $add = 'AND billing_schedule_quarter = "'.$curr_quarter.'" ';
			$add ='';
		}
		else
		{
			$add = '';
		}
		$where = 'rental_unit_id = '.$rental_unit_id.' '.$add;

		// var_dump($where); die();

		$this->db->from('home_owners_invoice');
		$this->db->select('MAX(invoice_date) AS invoice_date');
		$this->db->where($where);
		$query = $this->db->get();
		$invoice_date = '';
		if($query->num_rows() >0)
		{
			$row=$query->row();
			$invoice_date = $row->invoice_date;
		}
		return $invoice_date;
	}
	public function get_max_owners_invoice_date($rental_unit_id,$invoice_month = NULL,$invoice_year=NULL)
	{
		if($invoice_month != NULL || $invoice_year != NULL)
		{
			$add = 'AND invoice_month = "'.$invoice_month.'" AND invoice_year = '.$invoice_year.'';
		}
		else
		{
			$add = '';
		}
		$where = 'rental_unit_id = '.$rental_unit_id.' '.$add;

		$this->db->from('home_owners_invoice');
		$this->db->select('MAX(invoice_date) AS invoice_date');
		$this->db->where($where);
		$query = $this->db->get();
		$invoice_date = '';
		if($query->num_rows() >0)
		{
			$row=$query->row();
			$invoice_date = $row->invoice_date;
		}
		return $invoice_date;
	}
	public function get_bill_property_rate($invoice_type,$charge_to,$property_id)
	{
		$where = 'invoice_type_id = '.$invoice_type.' AND property_id = '.$property_id.' AND  charge_to = '.$charge_to;

		$this->db->from('property_billing');
		$this->db->select('billing_amount');
		$this->db->where($where);
		$query = $this->db->get();
		$billing_amount = 0;
		if($query->num_rows() >0)
		{
			$row=$query->row();
			$billing_amount = $row->billing_amount;
		}
		return $billing_amount;
	}

	public function get_current_quarter_dates()
	{
		$current_month = date('m');
		$current_year = date('Y');
		if($current_month>=1 && $current_month<=3)
		{
		$start_date = $current_year.'-01-01';
		$end_date = $current_year.'-04-01';
		}
		else  if($current_month>=4 && $current_month<=6)
		{
		$start_date = $current_year.'-04-01';  // timestamp or 1-April 12:00:00 AM
		$end_date = $current_year.'-07-01';  // timestamp or 1-July 12:00:00 AM means end of 30 June
		}
		else  if($current_month>=7 && $current_month<=9)
		{
		$start_date = $current_year.'-07-01';  // timestamp or 1-July 12:00:00 AM
		$end_date = $current_year.'-10-01';  // timestamp or 1-October 12:00:00 AM means end of 30 September
		}
		else  if($current_month>=10 && $current_month<=12)
		{
		$start_date = $current_year.'-10-01';  // timestamp or 1-October 12:00:00 AM
		$end_date = $current_year.'-01-01';  // timestamp or 1-January Next year 12:00:00 AM means end of 31 December this year
		}

		return $start_date.":".$end_date;
	}

	public function update_water_invoice($lease_id,$rental_unit_id)
	{
		$current_reading = $this->input->post('current_reading'.$lease_id);
		$previous_reading = $this->input->post('previous_reading'.$lease_id);
		$water_charges = $this->input->post('water_charges'.$lease_id);

		$units_consumed = $current_reading - $previous_reading;
		$invoice_date =date('Y-m-d');

		$invoice_month = date('m');
		$invoice_year = date('Y');



		if($lease_id > 0)
		{
			$invoice_type_id = 2;

			$datestring=''.$invoice_date.' first day of next month';
			$dt=date_create($datestring);
			$next = $dt->format('Y-m-d');
			$next_date = explode('-', $next);
			$next_year = $next_date[0];
			$next_month = $next_date[1];
			$next_date = $next_year.'-'.$next_month.'-'.'01';
			$next_date = strtotime($next_date);
			$next_quarter = ceil(date('m', $next_date) / 3);
			$next_month = ($next_quarter * 3) - 2;
			$next_year = date('Y', $next_date);

			$next_quarter = 'AC'.$next_quarter.'-'.$next_year;


			$total_due = $water_charges * ($current_reading - $previous_reading);

			// check

			$array_check = array('invoice_month'=>$invoice_month,'invoice_year'=>$invoice_year,'invoice_date'=>$invoice_date,'invoice_type'=>2,'lease_id'=>$lease_id);

			// var_dump($array_check); die();
			$this->db->where($array_check);
			$query_check = $this->db->get('invoice');

			if($query_check->num_rows() > 0)
			{
				foreach ($query_check->result() as $key_check) {
					# code...
					$invoice_idd = $key_check->invoice_id;

					// update
					// $where_update  = array('invoice_status' => 0);
					$this->db->where('invoice_id',$invoice_idd);
					$this->db->delete('invoice');

					$this->db->where('invoice_id',$invoice_idd);
					$this->db->delete('water_management');

				}
			}
			$invoice_number = $this->accounts_model->get_invoice_number();
			$insert_array = array(
							'lease_id' => $lease_id,
							'invoice_date' => $invoice_date,
							'invoice_month' => $invoice_month,
							'invoice_year' => $invoice_year,
							'invoice_amount' => $total_due,
							'arrears_bf' => $current_reading,
							'invoice_number' => $invoice_number,
							'invoice_type' => $invoice_type_id,
							'property_invoice_id' => 1,
							'billing_schedule_quarter'=> $next_quarter
						 );

			if($this->db->insert('invoice',$insert_array))
			{

				$invoice_id = $this->db->insert_id();
				if($invoice_type_id == 2 OR $invoice_type_id == 3)
				{
					$service_charge_insert = array(
											"house_number" => $lease_id,
											"prev_reading" => $previous_reading,
											"current_reading" => $current_reading,
											"units_consumed" => $units_consumed,
											"total_due" => $total_due,
											"prev_bill" => 0,
											"created" => date("Y-m-d"),
											"created_by" => $this->session->userdata('personnel_id'),
											"branch_code" => $this->session->userdata('branch_code'),
											'invoice_id' => $invoice_id
										);
					$this->db->insert('water_management', $service_charge_insert);
				}
				return TRUE;
			}

			else
			{
				return FALSE;

			}
		}

	}


	public function get_total_invoices_month($lease_id)
	{

		$parent_invoice_date = $this->accounts_model->get_max_invoice_date($lease_id);

		$water_charge = 0;
		$service_bf = 0;
		$water_charge = 0;
		$water_bf = 0;
		$penalty_charge =0;
		$total_service_charge =0;
		$total_bill = 0;
		$total_rent = 0;
		$total_water = 0;
		$total_deposit = 0;
		$total_brought_forward_account = 0;
		$deposit_invoice_amount = 0;
		if(!empty($parent_invoice_date))
		{
			$str = explode("-", $parent_invoice_date);

			$invoice_month = $str[1];
			$invoice_year = $str[0];
			$lease_invoice = $this->accounts_model->get_total_invoices_per_month($lease_id);

			if($lease_invoice->num_rows() > 0)
			{


				foreach ($lease_invoice->result() as $key_invoice) {
					# code...
					// $invoice_date = $key_invoice->invoice_date;
					// $invoice_id = $key_invoice->invoice_id;
					$invoice_type = $key_invoice->invoice_type_id;



					// $invoice_date_date = date('jS F Y',strtotime($invoice_date));

					if($invoice_type == 2)
					{
						// service charge
						$invoice_type_name = $key_invoice->invoice_type_name;



						$water_invoice_amount = $this->accounts_model->get_invoice_tenants_brought_forward($lease_id,NULL,$invoice_type);
						$water_paid_amount = $this->accounts_model->get_paid_tenants_brought_forward($lease_id,NULL,$invoice_type);

						$total_water = $water_invoice_amount - $water_paid_amount;


					}


					if($invoice_type == 4)
					{
						// service charge
						$invoice_type_name = $key_invoice->invoice_type_name;



						$service_charge_invoice_amount = $this->accounts_model->get_invoice_tenants_brought_forward($lease_id,NULL,$invoice_type);
						$service_charge_paid_amount = $this->accounts_model->get_paid_tenants_brought_forward($lease_id,NULL,$invoice_type);

						$total_service_charge = $service_charge_invoice_amount - $service_charge_paid_amount;




					}

					if($invoice_type == 1)
					{
						// service charge
						$invoice_type_name = $key_invoice->invoice_type_name;



						$rent_invoice_amount = $this->accounts_model->get_invoice_tenants_brought_forward($lease_id,NULL,$invoice_type);
						$rent_paid_amount = $this->accounts_model->get_paid_tenants_brought_forward_item($lease_id,NULL,$invoice_type);

						$total_rent = $rent_invoice_amount - $rent_paid_amount;




					}


					if($invoice_type == 5)
					{
						// penalty

						$invoice_type_name = $key_invoice->invoice_type_name;
						$penalty_invoice_amount = $this->accounts_model->get_invoice_tenants_brought_forward($lease_id,NULL,$invoice_type);
						$penalty_paid_amount = $this->accounts_model->get_paid_tenants_brought_forward_item($lease_id,NULL,$invoice_type);

						$total_penalty = $penalty_invoice_amount - $penalty_paid_amount;


					}

					if($invoice_type == 13)
					{
						// service charge
						$invoice_type_name = $key_invoice->invoice_type_name;


						$deposit_invoice_amount = $this->accounts_model->get_invoice_tenants_brought_forward($lease_id,$parent_invoice_date,$invoice_type);
						$deposit_paid_amount = $this->accounts_model->get_paid_tenants_brought_forward_item($lease_id,NULL,$invoice_type);

						$total_deposit = $deposit_invoice_amount - $deposit_paid_amount;
						// var_dump($deposit_paid_amount); die();


					}
					$tenants_response = $this->accounts_model->get_tenants_billings($lease_id);
					$total_pardon_amount = $tenants_response['total_pardon_amount'];

					$total_bill = $total_water + $total_deposit + $total_brought_forward_account + $total_rent + $total_service_charge;


				}
			}
		}
		$response['total_water'] = $total_water;
		$response['total_rent'] = $total_rent;
		$response['total_deposit'] = $total_deposit;
		$response['total_service_charge'] = $total_service_charge;
		$response['deposit_invoice_amount'] = $deposit_invoice_amount;
		$response['total_brought_forward_account'] = $total_brought_forward_account;
		$response['total_bill'] = $total_bill;
		return $response;

	}


	public function get_rent_and_service_charge($lease_id)
	{
		$bills = $this->accounts_model->get_all_invoice_month($lease_id);
		$payments = $this->accounts_model->get_all_payments_lease_rent($lease_id,null);
		// var_dump($payments); die();
		$pardons = $this->accounts_model->get_all_pardons_lease($lease_id);

		// var_dump($payments); die();
		$x=0;

		$bills_result = '';
		$last_date = '';
		$current_year = date('Y');
		$total_invoices = $bills->num_rows();
		$invoices_count = 0;
		$total_invoice_balance = 0;
		$total_arrears = 0;
		$total_payment_amount = 0;
		$result = '';
		$total_pardon_amount = 0;
		if($bills->num_rows() > 0)
		{
			foreach ($bills->result() as $key_bills) {
				# code...
				$invoice_month = $key_bills->invoice_month;
			    $invoice_year = $key_bills->invoice_year;
				$invoice_date = $key_bills->invoice_date;
				$invoice_amount = $key_bills->total_invoice;
				$document_number = $key_bills->document_number;
				$lease_invoice_id = $key_bills->lease_invoice_id;
				$invoices_count++;
				if($payments->num_rows() > 0)
				{
					foreach ($payments->result() as $payments_key) {
						# code...
						$payment_date = $payments_key->payment_date;
						$payment_year = $payments_key->year;
						$payment_month = $payments_key->month;
						$payment_amount = $payments_key->amount_paid;
						$receipt_number = $payments_key->document_number;


						if(($payment_date <= $invoice_date) && ($payment_date > $last_date) )
						{
							$total_arrears -= $payment_amount;
							// var_dump($payment_year); die();
							// if($payment_year >= $current_year)
							// {
								$result .=
								'
									<tr>
										<td>'.date('d M Y',strtotime($payment_date)).' </td>
										<td>'.$receipt_number.'</td>
										<td></td>
										<td>'.number_format($payment_amount, 2).'</td>
										<td>'.number_format($total_arrears, 2).'</td>
										<td></td>
									</tr>
								';
							// }

							$total_payment_amount += $payment_amount;

						}
					}
				}
				if($pardons->num_rows() > 0)
				{
					foreach ($pardons->result() as $pardons_key) {
						# code...
						$pardon_date = $pardons_key->pardon_date;
						$pardon_explode = explode('-', $pardon_date);
						$pardon_year = $pardon_explode[0];
						$pardon_month = $pardon_explode[1];
						$pardon_amount = $pardons_key->pardon_amount;
						$pardon_number = $pardons_key->document_number;

						if(($pardon_date <= $invoice_date) && ($pardon_date > $last_date) && ($pardon_amount > 0))
						{
							$total_arrears -= $pardon_amount;
							// if($pardon_year >= $current_year)
							// {
								$result .=
								'
									<tr>
										<td>'.date('d M Y',strtotime($pardon_date)).' </td>
										<td>'.$pardon_number.'</td>
										<td></td>
										<td>'.number_format($pardon_amount, 2).'</td>
										<td>'.number_format($total_arrears, 2).'</td>
										<td></td>
									</tr>
								';
							// }

							$total_pardon_amount += $pardon_amount;

						}
					}
				}
				//display disbursment if cheque amount > 0
				if($invoice_amount != 0)
				{
					$total_arrears += $invoice_amount;
					$total_invoice_balance += $invoice_amount;

					// if($invoice_year >= $current_year)
					// {
						$result .=
						'
							<tr>
								<td>'.date('d M Y',strtotime($invoice_date)).' </td>
								<td>'.$document_number.'</td>
								<td>'.number_format($invoice_amount, 2).'</td>
								<td></td>
								<td>'.number_format($total_arrears, 2).'</td>
								<td><a href="'.site_url().'invoice/'.$lease_id.'/'.$invoice_month.'/'.$invoice_year.'/'.$lease_invoice_id.'" target="_blank" class="btn btn-xs btn-warning">Invoice</a></td>
							</tr>
						';
					// }
				}

				//check if there are any more payments
				if($total_invoices == $invoices_count)
				{
					//get all loan deductions before date
					if($payments->num_rows() > 0)
					{
						foreach ($payments->result() as $payments_key) {
							# code...
							$payment_date = $payments_key->payment_date;
							$payment_year = $payments_key->year;
							$payment_month = $payments_key->month;
							$payment_amount = $payments_key->amount_paid;
							$receipt_number = $payments_key->document_number;

							if(($payment_date > $invoice_date) &&  ($payment_amount > 0))
							{
								$total_arrears -= $payment_amount;
								// if($payment_year >= $current_year)
								// {
									$result .=
									'
										<tr>
											<td>'.date('d M Y',strtotime($payment_date)).' </td>
											<td>'.$receipt_number.'</td>
											<td></td>
											<td>'.number_format($payment_amount, 2).'</td>
											<td>'.number_format($total_arrears, 2).'</td>
											<td></td>
										</tr>
									';
								// }

								$total_payment_amount += $payment_amount;

							}
						}
					}

					if($pardons->num_rows() > 0)
					{
						foreach ($pardons->result() as $pardons_key) {
							# code...
							$pardon_date = $pardons_key->pardon_date;
							$pardon_explode = explode('-', $pardon_date);
							$pardon_year = $pardon_explode[0];
							$pardon_month = $pardon_explode[1];
							$pardon_amount = $pardons_key->pardon_amount;
							$pardon_number = $pardons_key->document_number;

							if(($pardon_date > $invoice_date) &&  ($pardon_amount > 0))
							{
								$total_arrears -= $pardon_amount;
								// if($pardon_year >= $current_year)
								// {
									$result .=
									'
										<tr>
											<td>'.date('d M Y',strtotime($pardon_date)).' </td>
											<td>'.$pardon_number.'</td>
											<td></td>
											<td>'.number_format($pardon_amount, 2).'</td>
											<td>'.number_format($total_arrears, 2).'</td>
											<td></td>
										</tr>
									';
								// }

								$total_pardon_amount += $pardon_amount;

							}
						}
					}
				}
						$last_date = $invoice_date;
			}
		}
		else
		{
			//get all loan deductions before date
			if($payments->num_rows() > 0)
			{
				foreach ($payments->result() as $payments_key) {
					# code...
					$payment_date = $payments_key->payment_date;
					$payment_year = $payments_key->year;
					$payment_month = $payments_key->month;
					$payment_amount = $payments_key->amount_paid;
					$receipt_number = $payments_key->document_number;

					if(($payment_amount > 0))
					{
						$total_arrears -= $payment_amount;
						// if($payment_year >= $current_year)
						// {
							$result .=
							'
								<tr>
									<td>'.date('d M Y',strtotime($payment_date)).' </td>
									<td>'.$receipt_number.'</td>
									<td></td>
									<td>'.number_format($payment_amount, 2).'</td>
									<td>'.number_format($total_arrears, 2).'</td>
									<td></td>
								</tr>
							';
						// }

						$total_payment_amount += $payment_amount;

					}
				}
			}
			if($pardons->num_rows() > 0)
			{
				foreach ($pardons->result() as $pardons_key) {
					# code...
					$pardon_date = $pardons_key->pardon_date;
					$pardon_explode = explode('-', $pardon_date);
					$pardon_year = $pardon_explode[0];
					$pardon_month = $pardon_explode[1];
					$pardon_amount = $pardons_key->pardon_amount;
					$pardon_number = $pardons_key->document_number;

					if(($payment_amount > 0))
					{
						$total_arrears -= $pardon_amount;
						// if($pardon_year >= $current_year)
						// {
							$result .=
							'
								<tr>
									<td>'.date('d M Y',strtotime($pardon_date)).' </td>
									<td>'.$pardon_number.'</td>
									<td></td>
									<td>'.number_format($pardon_amount, 2).'</td>
									<td>'.number_format($total_arrears, 2).'</td>
									<td></td>
								</tr>
							';
						// }

						$total_pardon_amount += $pardon_amount;

					}
				}
			}

		}

		//display loan
		$result .=
		'
			<tr>
				<th colspan="2">Total</th>
				<th>'.number_format($total_invoice_balance, 2).'</th>
				<th>'.number_format($total_payment_amount, 2).'</th>
				<th>'.number_format($total_arrears, 2).'</th>
				<td></td>
			</tr>
		';

		$invoice_date = $this->get_max_invoice_date($lease_id);

		$account_invoice_amount = $this->accounts_model->get_invoice_tenants_brought_forward($lease_id,$invoice_date);
		$account_paid_amount = $this->accounts_model->get_paid_tenants_brought_forward($lease_id,$invoice_date);
		$account_todays_payment =  $this->accounts_model->get_today_tenants_paid_current_forward($lease_id,$invoice_date);
		$total_brought_forward_account = $account_invoice_amount - $account_paid_amount;



		$response['total_arrears'] = $total_arrears;
		$response['invoice_date'] = $invoice_date;
		$response['result'] = $result;
		$response['total_invoice_balance'] = $total_brought_forward_account;
		$response['total_payment_amount'] = $total_payment_amount;
		$response['total_pardon_amount'] = $total_pardon_amount;

		// var_dump($response); die();

		return $response;
	}

	public function get_all_payments_lease_rent($lease_id,$type,$year=null)
	{
		// var_dump($lease_id); die();
		if(!empty($type))
		{
			if($type == 1)
			{
				$add_where = ' AND payment_item.invoice_type_id <> 2';
			}
			else
			{
				$add_where = ' AND payment_item.invoice_type_id = 2';
			}
		}
		else
		{
			$add_where ='';
		}

		if(!empty($year))
		{
			$add_where .= ' AND YEAR(payments.payment_date) ='.$year;
		}
		else
		{
			$add_where .='';
		}

		$this->db->from('payments,payment_item');
		$this->db->select('payments.*');
		$this->db->where('payments.lease_id = '.$lease_id.'  AND payments.payment_status = 1 AND cancel = 0 AND payments.payment_id = payment_item.payment_id '.$add_where);
		$this->db->order_by('payments.payment_date','ASC');
		$this->db->group_by('payments.payment_id','ASC');
		$query = $this->db->get();
		return $query;
	}

	///

	public function activate_payment($payment_id)
	{
		$data = array(
				'confirmation_status' => 0
			);
		$this->db->where('payment_id', $payment_id);

		if($this->db->update('payments', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}

	/*
	*	Deactivate an payment_status payment
	*	@param int $payment_id
	*
	*/
	public function deactivate_payment($payment_id)
	{
		$data = array(
				'confirmation_status' => 1
			);
		$this->db->where('payment_id', $payment_id);

		if($this->db->update('payments', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
		public function get_today_agency_tenants_paid_current_month_forward($lease_id,$invoice_month,$invoice_year,$invoice_type_id = NULL,$start_date = null,$end_date=null)
	{
		if($invoice_type_id != NULL)
		{
			$add = 'AND payment_item.invoice_type_id = '.$invoice_type_id;
		}
		else
		{
			$add = '';
		}

		if(!empty($start_date) AND !empty($end_date))
		{
			$invoice_date_checked = ' AND payments.payment_date >= "'.$start_date.'" AND payments.payment_date <= "'.$end_date.'" ';
		}
		else
		{
			$invoice_date_checked = ' AND payments.month = "'.$invoice_month.'" AND payments.year = "'.$invoice_year.'"';
		}

		// $date = $invoice_year.'-'.$invoice_month.'-30';
		// $date = $invoice_year.'-08-30';
		// var_dump($date); die();
		$where = 'payments.lease_id = '.$lease_id.'  AND payments.confirmation_status = 0 AND payments.payment_status = 1 AND cancel = 0 '.$invoice_date_checked.' '.$add;

		//var_dump($where); die();

		$this->db->from('payments');
		$this->db->select('SUM(amount_paid) AS total_paid');
		$this->db->where($where);
		$query = $this->db->get();
		$total_paid = 0;
		if($query->num_rows() >0)
		{
			$row=$query->row();
			$total_paid = $row->total_paid;
		}
		return $total_paid;
	}
	// end of tenants



	// print lease statement
	public function print_lease_statement($lease_number,$lease_id=NULL)
	{
		$this->db->from('v_transactions_by_date');
		$this->db->select('*');
		$this->db->where('lease_id = '.$lease_id);
		$query = $this->db->get();

		$result = '';
		$balance = 0;
		$count = 0;
		$total_dr_amount = 0;
		$total_cr_amount = 0;
		$total_invoice = 0;
		$total_payments = 0;
		$total_debits = 0;
		$total_credits = 0;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				// code...

				$transaction_date = $value->createdAt;
				$remarks = $value->transactionDescription;
				$transaction_category = $value->transactionCategory;
				$today = date('d M Y',strtotime($transaction_date));

				$dr_amount= $value->dr_amount;
				$cr_amount = $value->cr_amount;

				$balance += $dr_amount;
				$balance -= $cr_amount;
				$total_dr_amount += $dr_amount;
				$total_cr_amount += $cr_amount;
				if($transaction_category == 'Outgoing Invoice')
				{
					$total_invoice += $dr_amount;
				}


				if($transaction_category == 'Tenant Payments')
				{
					$total_payments += $cr_amount;
				}


				if($transaction_category == 'Credit Note')
				{
					$total_credits += $cr_amount;
				}

				if($transaction_category == 'Debit Note')
				{
					$total_debits += $dr_amount;
				}
				$count++;
				$result .=
									'
										<tr>
											<td>'.$count.'</td>
											<td>'.$today.'</td>
											<td>'.$remarks.'</td>
											<td>'.$transaction_category.'</td>
											<td>'.number_format($dr_amount,2).'</td>
											<td>'.number_format($cr_amount, 2).'</td>
											<td>'.number_format($balance, 2).'</td>
										</tr>
									';
			}

			$result .=
								'
									<tr>
										<th colspan="4" style="text-align:right">Totals</th>
										<th>'.number_format($total_dr_amount,2).'</th>
										<th>'.number_format($total_cr_amount, 2).'</th>
										<th>'.number_format($balance, 2).'</th>
									</tr>
								';
		}
		// var_dump($query->num_rows());die();
		$response['result'] = $result;
		$response['total_invoice'] = $total_invoice;
		$response['total_pardon_amount'] = $total_credits;
		$response['total_payment_amount'] = $total_payments;
		$response['total_debits_amount'] = $total_debits;
		$response['total_arrears'] = $balance;
		return $response;

	}

	public function print_lease_statement_old($lease_id,$year=NULL)
	{
		$bills = $this->accounts_model->get_all_invoice_month($lease_id);
		$payments = $this->accounts_model->get_all_payments_lease_rent($lease_id,null);
		// var_dump($payments); die();
		$pardons = $this->accounts_model->get_all_pardons_lease($lease_id);

		// var_dump($payments); die();
		$x=0;

		$bills_result = '';
		$last_date = '';
		$current_year = date('Y');
		$total_invoices = $bills->num_rows();
		$invoices_count = 0;
		$total_invoice_balance = 0;
		$total_arrears = 0;
		$total_payment_amount = 0;
		$result = '';
		$total_pardon_amount = 0;
		if($bills->num_rows() > 0)
		{
			foreach ($bills->result() as $key_bills) {
				# code...
				$invoice_month = $key_bills->invoice_month;
			    $invoice_year = $key_bills->invoice_year;
				$invoice_date = $key_bills->invoice_date;
				$invoice_amount = $key_bills->total_invoice;
				$document_number = $key_bills->document_number;
				$lease_invoice_id = $key_bills->lease_invoice_id;
				$invoices_count++;
				if($payments->num_rows() > 0)
				{
					foreach ($payments->result() as $payments_key) {
						# code...
						$payment_date = $payments_key->payment_date;
						$payment_year = $payments_key->year;
						$payment_month = $payments_key->month;
						$payment_amount = $payments_key->amount_paid;
						$receipt_number = $payments_key->document_number;


						if(($payment_date <= $invoice_date) && ($payment_date > $last_date) )
						{
							$total_arrears -= $payment_amount;
							// var_dump($payment_year); die();
							// if($payment_year >= $current_year)
							// {
								$result .=
								'
									<tr>
										<td>'.date('d M Y',strtotime($payment_date)).' </td>
										<td>'.$receipt_number.'</td>
										<td>Payment</td>
										<td></td>
										<td>'.number_format($payment_amount, 2).'</td>
										<td>'.number_format($total_arrears, 2).'</td>
									</tr>
								';
							// }

							$total_payment_amount += $payment_amount;

						}
					}
				}
				if($pardons->num_rows() > 0)
				{
					foreach ($pardons->result() as $pardons_key) {
						# code...
						$pardon_date = $pardons_key->pardon_date;
						$pardon_explode = explode('-', $pardon_date);
						$pardon_year = $pardon_explode[0];
						$pardon_month = $pardon_explode[1];
						$pardon_amount = $pardons_key->pardon_amount;
						$pardon_number = $pardons_key->document_number;

						if(($pardon_date <= $invoice_date) && ($pardon_date > $last_date) && ($pardon_amount > 0))
						{
							$total_arrears -= $pardon_amount;
							// if($pardon_year >= $current_year)
							// {
								$result .=
								'
									<tr>
										<td>'.date('d M Y',strtotime($pardon_date)).' </td>
										<td>'.$pardon_number.'</td>
										<td>Credit Note</td>
										<td></td>
										<td>'.number_format($pardon_amount, 2).'</td>
										<td>'.number_format($total_arrears, 2).'</td>
									</tr>
								';
							// }

							$total_pardon_amount += $pardon_amount;

						}
					}
				}
				//display disbursment if cheque amount > 0
				if($invoice_amount != 0)
				{
					$total_arrears += $invoice_amount;
					$total_invoice_balance += $invoice_amount;

					// if($invoice_year >= $current_year)
					// {
						$result .=
						'
							<tr>
								<td>'.date('d M Y',strtotime($invoice_date)).' </td>
								<td>'.$document_number.'</td>
								<td>Invoice</td>
								<td>'.number_format($invoice_amount, 2).'</td>
								<td></td>
								<td>'.number_format($total_arrears, 2).'</td>
							</tr>
						';
					// }
				}

				//check if there are any more payments
				if($total_invoices == $invoices_count)
				{
					//get all loan deductions before date
					if($payments->num_rows() > 0)
					{
						foreach ($payments->result() as $payments_key) {
							# code...
							$payment_date = $payments_key->payment_date;
							$payment_year = $payments_key->year;
							$payment_month = $payments_key->month;
							$payment_amount = $payments_key->amount_paid;
							$receipt_number = $payments_key->document_number;
							if(($payment_date > $invoice_date) &&  ($payment_amount > 0))
							{
								$total_arrears -= $payment_amount;
								// if($payment_year >= $current_year)
								// {
									$result .=
									'
										<tr>
											<td>'.date('d M Y',strtotime($payment_date)).' </td>
											<td>'.$receipt_number.'</td>
											<td>Payment</td>
											<td></td>
											<td>'.number_format($payment_amount, 2).'</td>
											<td>'.number_format($total_arrears, 2).'</td>
										</tr>
									';
								// }

								$total_payment_amount += $payment_amount;

							}
						}
					}

					if($pardons->num_rows() > 0)
					{
						foreach ($pardons->result() as $pardons_key) {
							# code...
							$pardon_date = $pardons_key->pardon_date;
							$pardon_explode = explode('-', $pardon_date);
							$pardon_year = $pardon_explode[0];
							$pardon_month = $pardon_explode[1];
							$pardon_amount = $pardons_key->pardon_amount;
							$pardon_number = $pardons_key->document_number;

							if(($pardon_date > $invoice_date) &&  ($pardon_amount > 0))
							{
								$total_arrears -= $pardon_amount;
								// if($pardon_year >= $current_year)
								// {
									$result .=
									'
										<tr>
											<td>'.date('d M Y',strtotime($pardon_date)).' </td>
											<td>'.$pardon_number.'</td>
											<td>Credit Note</td>
											<td></td>
											<td>'.number_format($pardon_amount, 2).'</td>
											<td>'.number_format($total_arrears, 2).'</td>
										</tr>
									';
								// }

								$total_pardon_amount += $pardon_amount;

							}
						}
					}
				}
						$last_date = $invoice_date;
			}
		}
		else
		{
			//get all loan deductions before date
			if($payments->num_rows() > 0)
			{
				foreach ($payments->result() as $payments_key) {
					# code...
					$payment_date = $payments_key->payment_date;
					$payment_year = $payments_key->year;
					$payment_month = $payments_key->month;
					$payment_amount = $payments_key->amount_paid;
					$receipt_number = $payments_key->document_number;


					if(($payment_amount > 0))
					{
						$total_arrears -= $payment_amount;
						// if($payment_year >= $current_year)
						// {
							$result .=
							'
								<tr>
									<td>'.date('d M Y',strtotime($payment_date)).' </td>
									<td>'.$receipt_number.'</td>
									<td>Payment</td>
									<td></td>
									<td>'.number_format($payment_amount, 2).'</td>
									<td>'.number_format($total_arrears, 2).'</td>
								</tr>
							';
						// }

						$total_payment_amount += $payment_amount;

					}
				}
			}
			if($pardons->num_rows() > 0)
			{
				foreach ($pardons->result() as $pardons_key) {
					# code...
					$pardon_date = $pardons_key->pardon_date;
					$pardon_explode = explode('-', $pardon_date);
					$pardon_year = $pardon_explode[0];
					$pardon_month = $pardon_explode[1];
					$pardon_amount = $pardons_key->pardon_amount;
					$pardon_number = $pardons_key->document_number;

					if(($payment_amount > 0))
					{
						$total_arrears -= $pardon_amount;
						// if($pardon_year >= $current_year)
						// {
							$result .=
							'
								<tr>
									<td>'.date('d M Y',strtotime($pardon_date)).' </td>
									<td>'.$pardon_number.'</td>
									<td>Credit Note</td>
									<td></td>
									<td>'.number_format($pardon_amount, 2).'</td>
									<td>'.number_format($total_arrears, 2).'</td>
								</tr>
							';
						// }

						$total_pardon_amount += $pardon_amount;

					}
				}
			}

		}

		//display loan
		$result .=
		'
			<tr>
				<th colspan="3">Total</th>
				<th>'.number_format($total_invoice_balance, 2).'</th>
				<th>'.number_format($total_payment_amount, 2).'</th>
				<th>'.number_format($total_arrears, 2).'</th>
			</tr>
		';

		$invoice_date = $this->get_max_invoice_date($lease_id);

		$account_invoice_amount = $this->accounts_model->get_invoice_tenants_brought_forward($lease_id,$invoice_date);
		$account_paid_amount = $this->accounts_model->get_paid_tenants_brought_forward($lease_id,$invoice_date);
		$account_todays_payment =  $this->accounts_model->get_today_tenants_paid_current_forward($lease_id,$invoice_date);
		$total_brought_forward_account = $account_invoice_amount - $account_paid_amount;



		$response['total_arrears'] = $total_arrears;
		$response['invoice_date'] = $invoice_date;
		$response['result'] = $result;
		$response['total_invoice_balance'] = $total_brought_forward_account;
		$response['total_invoice'] = $total_invoice_balance;
		$response['total_payment_amount'] = $total_payment_amount;
		$response['total_pardon_amount'] = $total_pardon_amount;

		$response['total_pardon_amount'] = $total_pardon_amount;

		return $response;
	}
	public function get_lease_balance($lease_id)
	{
		$this->db->from('v_lease_balances');
		$this->db->select('*');
		$this->db->where('lease_id = '.$lease_id.'');
		$query = $this->db->get();
		$total_invoice_amount = 0;
		$total_paid_amount = 0;
		$total_waived_amount = 0;
		$balance = 0;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$total_invoice_amount = $value->total_invoice_amount;
				$total_paid_amount = $value->total_paid_amount;
				$total_waived_amount = $value->total_waived_amount;
				$balance = $value->balance;
			}
		}


		$response['balance'] = $balance;
		$response['paid'] = $total_paid_amount;
		$response['waived'] = $total_waived_amount;
		$response['invoiced'] = $total_invoice_amount;

		return $response;
	}


	public function create_lease_bill($lease_id)
	{
		$invoice_date = $this->input->post('invoice_date');
		$invoice_amount = $this->input->post('invoice_amount');
		$remarks = $this->input->post('remarks');
		$invoice_type_id = $this->input->post('invoice_type_id');

		$current_date = $invoice_date;
		$current_quarter = ceil(date('m', $current_date) / 3);
		$current_month = ($current_quarter * 3) - 2;
		$current_year = date('Y', $current_date);
		$curr_quarter = 'AC'.$current_quarter.'-'.$current_year;

		$insert_array = array(
								'lease_id' => $lease_id,
								'invoice_amount' => $invoice_amount,
								'invoice_type' => $invoice_type_id,
								'remarks' => $remarks,
								'created' => $invoice_date,
								'created_by'=>$this->session->userdata('personnel_id'),
								'personnel_id'=>$this->session->userdata('personnel_id'),
								'billing_schedule_quarter' => $curr_quarter
							 );
		$this->db->insert('invoice',$insert_array);
		return TRUE;
	}

	/*
	*	Retrieve all tenants
	*	@param string $table
	* 	@param string $where
	*
	*/
	public function get_unreconcilled_payments($table, $where, $per_page=null, $page=null, $order=null, $order_method = 'ASC')
	{
		//retrieve all tenants
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		$query = $this->db->get('', $per_page, $page);

		return $query;
	}

	public function get_all_leases_rental_units($table, $where, $per_page, $page, $order, $order_method = 'ASC',$month,$year)
	{
		//retrieve all tenants

		$this->db->from($table);
		$this->db->select('`rental_unit`.rental_unit_id,rental_unit.rental_unit_name,v_tenant_leases.*');
		$this->db->where($where);
		$this->db->join('v_tenant_leases','v_tenant_leases.rental_unit_id = rental_unit.rental_unit_id AND v_tenant_leases.lease_id IN (SELECT lease_invoice.lease_id FROM lease_invoice WHERE invoice_month = "'.$month.'" AND invoice_year = "'.$year.'" AND lease_invoice.invoice_deleted = 0 AND (lease_invoice.invoice_date <= v_tenant_leases.notice_date OR v_tenant_leases.notice_date IS NULL) AND (lease_invoice.invoice_date <= property.closing_date OR property.closing_date IS NULL OR property.closing_date = "0000-00-00")  GROUP BY lease_invoice.lease_id)','LEFT');
		$this->db->order_by($order, $order_method);
		$query = $this->db->get('');

		return $query;
	}



	// teannts
	public function get_rent_invoices_brought_forward($lease_id, $invoice_date=NULL,$invoice_type_id = NULL,$month=null,$year=null,$rental_unit_id=null)
	{

		if(!empty($month))
		{
				// $invoice_date_checked = 'AND transactionDate < "'.$invoice_date.'" ';
				// $invoice_date_checked = ' AND (invoice.year <  "'.$year.'" OR (invoice.year = '.$year.' AND month < "'.$month.'") ) AND invoice.rental_unit_id ='.$rental_unit_id;
			$invoice_date_checked = ' AND (invoice.year <  "'.$year.'" OR (invoice.year = '.$year.' AND month < "'.$month.'") ) ';
		}
		else {
			$invoice_date_checked = '';
		}


		// $where = 'v_transactions.lease_id = '.$lease_id.' AND accountId = 1 '.$invoice_date_checked;
		// $this->db->from('v_transactions');
		// $this->db->select('SUM(dr_amount) AS total_invoice');
		// $this->db->where($where);
		// $query = $this->db->get();
		// $total_invoice = 0;
		// if($query->num_rows() >0)
		// {
		// 	$row=$query->row();
		// 	$total_invoice = $row->total_invoice;
		// }
		// return $total_invoice;


		$where = 'lease_invoice.lease_invoice_id = invoice.lease_invoice_id AND lease_invoice.invoice_deleted = 0 AND lease_invoice.lease_id = '.$lease_id.'  and invoice_type =  '.$invoice_type_id.$invoice_date_checked;
		$this->db->from('invoice,lease_invoice');
		$this->db->select('SUM(invoice.invoice_amount) AS total_invoice');
		$this->db->where($where);
		$query = $this->db->get();
		$total_invoice = 0;
		if($query->num_rows() >0)
		{
			$row=$query->row();
			$total_invoice = $row->total_invoice;
		}
		return $total_invoice;
	}


	public function get_rent_credit_note_brought_forward($lease_id, $credit_note_date=NULL,$invoice_type_id = NULL,$month=null,$year=null,$rental_unit_id=null)
	{

		if(!empty($month))
		{
				// $invoice_date_checked = 'AND transactionDate < "'.$invoice_date.'" ';
			// $invoice_date_checked = ' AND (credit_note_item.credit_note_year <  "'.$year.'" OR (credit_note_item.credit_note_year = '.$year.' AND credit_note_item.credit_note_month < "'.$month.'") ) AND credit_note_item.rental_unit_id ='.$rental_unit_id;
				$invoice_date_checked = ' AND (credit_note_item.credit_note_year <  "'.$year.'" OR (credit_note_item.credit_note_year = '.$year.' AND credit_note_item.credit_note_month < "'.$month.'") )';
		}
		else {
			$invoice_date_checked = '';
		}



		$where = 'credit_notes.credit_note_id = credit_note_item.credit_note_id AND credit_notes.lease_id = '.$lease_id.'  and invoice_type_id =  '.$invoice_type_id.$invoice_date_checked;
		$this->db->from('credit_note_item,credit_notes');
		$this->db->select('SUM(credit_note_item.credit_note_amount) AS total_invoice');
		$this->db->where($where);
		$query = $this->db->get();
		$total_invoice = 0;
		if($query->num_rows() >0)
		{
			$row=$query->row();
			$total_invoice = $row->total_invoice;
		}
		return $total_invoice;
	}

	public function get_rent_debit_note_brought_forward($lease_id, $debit_note_date=NULL,$invoice_type_id = NULL,$month=null,$year=null,$rental_unit_id=null)
	{

		if(!empty($month))
		{
				// $invoice_date_checked = 'AND transactionDate < "'.$invoice_date.'" ';

				$invoice_date_checked = ' AND (debit_note_item.debit_note_year <  "'.$year.'" OR (debit_note_item.debit_note_year = '.$year.' AND debit_note_item.debit_note_month < "'.$month.'") ) ';
				// $invoice_date_checked = ' AND (debit_note_item.debit_note_year <  "'.$year.'" OR (debit_note_item.debit_note_year = '.$year.' AND debit_note_item.debit_note_month < "'.$month.'") ) AND debit_note_item.rental_unit_id ='.$rental_unit_id;
		}
		else {
			$invoice_date_checked = '';
		}

		$where = 'debit_notes.debit_note_id = debit_note_item.debit_note_id AND debit_notes.lease_id = '.$lease_id.' AND debit_note_item.debit_note_item_created < "'.$debit_note_date.'" and invoice_type_id =  '.$invoice_type_id.$invoice_date_checked;
		$this->db->from('debit_note_item,debit_notes');
		$this->db->select('SUM(debit_note_item.debit_note_amount) AS total_invoice');
		$this->db->where($where);
		$query = $this->db->get();
		$total_invoice = 0;
		if($query->num_rows() >0)
		{
			$row=$query->row();
			$total_invoice = $row->total_invoice;
		}
		return $total_invoice;
	}

	public function get_rent_payments_brought_forward($lease_id,$invoice_date=NULL,$invoice_type_id = NULL,$month=null,$year=null,$rental_unit_id=null)
	{
		// $where = 'v_transactions.lease_id = '.$lease_id.' AND v_transactions.transactionDate < "'.$invoice_date.'" AND accountId = 1 AND transactionCategory="Payments"';
		// $this->db->from('v_transactions');
		// $this->db->select('SUM(dr_amount) AS total_invoice');

		if(!empty($month))
		{
				// $invoice_date_checked = 'AND transactionDate < "'.$invoice_date.'" ';
				$invoice_date_checked = ' AND (payment_item.payment_year <  "'.$year.'" OR (payment_item.payment_year = '.$year.' AND payment_item.payment_month < "'.$month.'") ) ';
				// $invoice_date_checked = ' AND (payment_item.payment_year <  "'.$year.'" OR (payment_item.payment_year = '.$year.' AND payment_item.payment_month < "'.$month.'") ) AND payment_item.rental_unit_id ='.$rental_unit_id;
		}
		else {
			$invoice_date_checked = '';
		}

		// $where = 'v_transactions.lease_id = '.$lease_id.' AND accountId = 1 '.$invoice_date_checked;
		// $this->db->from('v_transactions');
		// $this->db->select('(SUM(dr_amount)- SUM(cr_amount)) AS total_invoice');
		// $this->db->where($where);
		// $query = $this->db->get();
		// $total_invoice = 0;
		// if($query->num_rows() >0)
		// {
		// 	$row=$query->row();
		// 	$total_invoice = $row->total_invoice;
		// }
		//
		// // var_dump($total_invoice); die();
		// return $total_invoice;

		$where = 'payments.payment_id = payment_item.payment_id AND payments.lease_id = '.$lease_id.'  AND invoice_type_id = 1 '.$invoice_date_checked;
		$this->db->from('payments,payment_item');
		$this->db->select('SUM(payment_item.amount_paid) AS total_invoice');
		$this->db->where($where);
		$query = $this->db->get();
		$total_invoice = 0;
		if($query->num_rows() >0)
		{
			$row=$query->row();
			$total_invoice = $row->total_invoice;
		}
		return $total_invoice;
	}

	public function get_current_month_invoice($lease_id, $invoice_month,$invoice_year,$invoice_type_id = NULL,$start_date=null,$end_date=null)
	{
		if($invoice_type_id != NULL)
		{
			$add = 'AND invoice_type = '.$invoice_type_id;
		}
		else
		{
			$add = '';
		}
		// $invoice_date_checked = 'AND MONTH(created) = "'.$invoice_month.'" AND YEAR(created) = "'.$invoice_year.'" ';
		$invoice_date_checked = 'AND month = "'.$invoice_month.'" AND year = "'.$invoice_year.'" ';
		$where = 'lease_invoice.lease_invoice_id = invoice.lease_invoice_id AND lease_invoice.invoice_deleted = 0 AND lease_invoice.lease_id = '.$lease_id.' '.$invoice_date_checked.' '.$add;
		$this->db->from('invoice,lease_invoice');
		$this->db->select('SUM(invoice_amount) AS total_invoice');
		$this->db->where($where);
		$query = $this->db->get();
		$total_invoice = 0;
		if($query->num_rows() >0)
		{
			$row=$query->row();
			$total_invoice = $row->total_invoice;
		}


		if($invoice_type_id != NULL)
		{
			$added = 'AND invoice_type_id = '.$invoice_type_id;
		}
		else
		{
			$added = '';
		}

		$credit_note_date_checked = 'AND credit_note_month = "'.$invoice_month.'" AND credit_note_year = "'.$invoice_year.'" ';
		$where = 'credit_notes.credit_note_id = credit_note_item.credit_note_id AND credit_notes.lease_id = '.$lease_id.' '.$credit_note_date_checked.' '.$added;
		$this->db->from('credit_note_item,credit_notes');
		$this->db->select('SUM(credit_note_item.credit_note_amount) AS total_invoice');
		$this->db->where($where);
		$query = $this->db->get();
		$total_credit_note = 0;
		if($query->num_rows() >0)
		{
			$row=$query->row();
			$total_credit_note = $row->total_invoice;
		}





		return $total_invoice - $total_credit_note;
	}





	public function get_current_months_payments($lease_id,$invoice_month,$invoice_year,$invoice_type_id = NULL,$start_date = null,$end_date=null)
	{

		$add = 'AND payment_item.invoice_type_id = '.$invoice_type_id;
		$invoice_date_checked = ' AND payment_year = "'.$invoice_year.'" AND payment_month = "'.$invoice_month.'" ';
		$where = 'payments.payment_id = payment_item.payment_id AND payments.lease_id = '.$lease_id.' '.$invoice_date_checked.' '.$add;
		$this->db->from('payments,payment_item');
		$this->db->select('SUM(payment_item.amount_paid) AS total_paid');
		$this->db->where($where);
		$query = $this->db->get();
		$total_paid = 0;
		if($query->num_rows() >0)
		{
			$row=$query->row();
			$total_paid = $row->total_paid;
		}
		return $total_paid;
	}


	public function get_current_months_debits($lease_id,$invoice_month,$invoice_year,$invoice_type_id = NULL,$start_date = null,$end_date=null)
	{

		// $add = 'AND debit_note_item.invoice_type_id = '.$invoice_type_id;
		$invoice_date_checked = ' AND debit_note_year = "'.$invoice_year.'" AND debit_note_month = "'.$invoice_month.'" ';
		$where = 'debit_notes.debit_note_id = debit_note_item.debit_note_id AND debit_notes.lease_id = '.$lease_id.' AND debit_note_item.debit_note_item_created < "'.$debit_note_date.'" and invoice_type_id =  '.$invoice_type_id.$invoice_date_checked;
		$this->db->from('debit_note_item,debit_notes');
		$this->db->select('SUM(debit_note_item.debit_note_amount) AS total_invoice');
		$this->db->where($where);
		$query = $this->db->get();
		$total_paid = 0;
		if($query->num_rows() >0)
		{
			$row=$query->row();
			$total_paid = $row->total_paid;
		}
		return $total_paid;
	}
		


	public function payment_item_add($lease_id)
	{
		$amount = $this->input->post('amount');
		$invoice_type_id=$this->input->post('invoice_type_id');
		// $invoice_id=$this->input->post('invoice_id');
		$payment_month=$this->input->post('payment_month');
		$payment_year=$this->input->post('payment_year');
		$lease_number=$this->input->post('lease_number');
		$rental_unit_id=$this->input->post('rental_unit_id');
		$tenant_id=$this->input->post('tenant_id');
		// insert for service charge

		$this->db->where('lease_invoice_id',$invoice_id);
		$query = $this->db->get('lease_invoice');

		$row = $query->row();

		$invoice_date = $row->invoice_date;


		$this->db->where('invoice_type_id',$invoice_type_id);
		$query_type = $this->db->get('invoice_type');
		$row_two = $query_type->row();
		$invoice_type_name = $row_two->invoice_type_name;
		$concate = $payment_year.'-'.$payment_month;
		$date = date('M Y',strtotime($concate));

		$remarks = 'Payment for '.$date.' '.$invoice_type_name;



		$service = array(
							'payment_id'=>0,
							'amount_paid'=> $amount,
							'payment_month'=> $payment_month,
							'rental_unit_id'=> $rental_unit_id,
							'tenant_id'=> $tenant_id,
							'lease_number'=> $lease_number,
							'payment_year'=> $payment_year,
							'invoice_type_id' => $invoice_type_id,
							'payment_item_status' => 0,
							'lease_id' => $lease_id,
							'mpesa_id' => $this->input->post('mpesa_id'),
							'personnel_id' => $this->session->userdata('personnel_id'),
							'payment_item_created' => date('Y-m-d'),
							'remarks'=>$remarks
						);
		$payment_id = $this->input->post('payment_id');
		if(!empty($payment_id))
		{
			$service['payment_id'] = $payment_id;
		}
		// var_dump($service); die();
		$this->db->insert('payment_item',$service);
		return TRUE;

	}


	public function confirm_payment($lease_id,$personnel_id = NULL)
	{
		$amount = $this->input->post('total_amount');
		$payment_method=$this->input->post('payment_method');
		$type_of_account=$this->input->post('type_of_account');
		$rental_unit_id = $this->input->post('rental_unit_id');
		$tenant_id=$this->input->post('tenant_id');
		$lease_number = $this->input->post('lease_number');

		// var_dump($_POST);die();
		if($payment_method == 2 || $payment_method == 3 || $payment_method == 5)
		{
			$transaction_code = $this->input->post('reference_number');
			$bank_id = $this->input->post('bank_id');
		}
		else
		{
			$transaction_code = '';
			$bank_id = 1;
		}

		// calculate the points to get
		$payment_date = $this->input->post('payment_date');

		$date_check = explode('-', $payment_date);
		$month = $date_check[1];
		$year = $date_check[0];


		$receipt_number = $this->create_receipt_number();

		$data = array(
			'payment_method_id'=>$payment_method,
			'bank_id'=>$bank_id,
			'amount_paid'=>$amount,
			'personnel_id'=>$this->session->userdata("personnel_id"),
			'transaction_code'=>$transaction_code,
			'payment_date'=>$this->input->post('payment_date'),
			'receipt_number'=>$transaction_code,
			'paid_by'=>$this->input->post('paid_by'),
			'payment_created'=>date("Y-m-d"),
			'rental_unit_id'=>$rental_unit_id,
			'account_id'=>$lease_number,
			'tenant_id'=>$tenant_id,
			'year'=>$year,
			'month'=>$month,
			'payment_created_by'=>$this->session->userdata("personnel_id"),
			'approved_by'=>$personnel_id,'date_approved'=>date('Y-m-d')
		);
		if(empty($payment_id))
		{
			$data['confirm_number'] = $receipt_number;
			$data['document_number'] = $receipt_number;
		}
		if($type_of_account == 1)
		{
			$data['lease_id'] = $lease_id;		
			$payment_id = $this->input->post('payment_id');

			if(!empty($payment_id))
			{
				$this->db->where('payment_id',$payment_id);
				if($this->db->update('payments', $data))
				{
					
					$total_visits = sizeof($_POST['tenant_payments_items']);
					// var_dump($total_visits);die();
			  		//check if any checkboxes have been ticked
			  		if($total_visits > 0)
			  		{
			  			for($r = 0; $r < $total_visits; $r++)
			  			{
			  				$visit = $_POST['tenant_payments_items'];
			  				$payment_item_id = $visit[$r];
			  				// var_dump($visit);die();
			  				//check if card is held
				  			$service = array(
											'payment_id'=>$payment_id,
											'payment_item_created' =>$this->input->post('payment_date'),
											'payment_item_status'=>1,
											'rental_unit_id'=>$rental_unit_id,
											'lease_id'=>$lease_id,
											'lease_number'=>$lease_number,
											'tenant_id'=>$tenant_id,
											'document_no'=>$receipt_number
										);
							$this->db->where('payment_item_id',$payment_item_id);
							$this->db->update('payment_item',$service);

			  			}
			  		}


					return TRUE;
				}
				else{
					return FALSE;
				}
			}
			else
			{
				if($this->db->insert('payments', $data))
				{
					$payment_id = $this->db->insert_id();


					$total_visits = sizeof($_POST['tenant_payments_items']);

			  		//check if any checkboxes have been ticked
			  		if($total_visits > 0)
			  		{
			  			for($r = 0; $r < $total_visits; $r++)
			  			{
			  				$visit = $_POST['tenant_payments_items'];
			  				$payment_item_id = $visit[$r];
			  				//check if card is held
				  			$service = array(
											'payment_id'=>$payment_id,
											'payment_item_created' =>$this->input->post('payment_date'),
											'payment_item_status'=>1,
											'rental_unit_id'=>$rental_unit_id,
											'lease_id'=>$lease_id,
											'lease_number'=>$lease_number,
											'tenant_id'=>$tenant_id,
											'document_no'=>$receipt_number
										);
							$this->db->where('payment_item_id',$payment_item_id);
							$this->db->update('payment_item',$service);

			  			}
			  		}

					


					return TRUE;
				}
				else{
					return FALSE;
				}
			}
		}
		else if($type_of_account == 0)
		{
			return FALSE;

		}
	}

	public function add_credit_note_item($lease_id)
	{
		$amount = $this->input->post('amount');
		$invoice_type_id=$this->input->post('invoice_type_id');
		$invoice_id=$this->input->post('invoice_id');

		$tenant_id=$this->input->post('tenant_id');
		$rental_unit_id=$this->input->post('rental_unit_id');
		$lease_number=$this->input->post('lease_number');

		$this->db->where('lease_invoice_id',$invoice_id);
		$query = $this->db->get('lease_invoice');

		$row = $query->row();

		$invoice_date = $row->invoice_date;


		$this->db->where('invoice_type_id',$invoice_type_id);
		$query_type = $this->db->get('invoice_type');
		$row_two = $query_type->row();
		$invoice_type_name = $row_two->invoice_type_name;

		$date = date('M Y',strtotime($invoice_date));

		$remarks = 'Credit Note for '.$date.' '.$invoice_type_name;

		$service = array(
							'credit_note_id'=>0,
							'credit_note_amount'=> $amount,
							'invoice_type_id' => $invoice_type_id,
							'credit_note_item_status' => 0,
							'lease_id' => $lease_id,
							'personnel_id' => $this->session->userdata('personnel_id'),
							'credit_note_item_created' => date('Y-m-d'),
							'invoice_id'=>$invoice_id,
							'lease_id'=>$lease_id,
							'rental_unit_id'=>$rental_unit_id,
							'tenant_id'=>$tenant_id,
							'lease_number'=>$lease_number,
							'remarks'=>$remarks
						);
		$this->db->insert('credit_note_item',$service);
		return TRUE;

	}

	public function get_lease_credit_notes($lease_id,$limit=null)
	{
		$this->db->where('leases.lease_id = credit_notes.lease_id AND credit_notes.lease_id = '.$lease_id.'  AND credit_notes.cancel = 0');
		if($limit)
		{
			$this->db->limit($limit);
		}
		$this->db->order_by('credit_notes.credit_note_date','DESC');
		return $this->db->get('credit_notes,leases');
	}

	public function confirm_credit_note($lease_id,$personnel_id = NULL)
	{
		$amount = $this->input->post('total_amount');
		$rental_unit_id = $this->input->post('rental_unit_id');

		// calculate the points to get
		$credit_note_date = $this->input->post('credit_note_date');

		$reason = $this->input->post('reason');


		$date_check = explode('-', $credit_note_date);
		$month = $date_check[1];
		$year = $date_check[0];
		$invoice_month = $this->input->post('invoice_month');
		$invoice_year = $this->input->post('invoice_year');

		$tenant_id = $this->input->post('tenant_id');
		$rental_unit_id = $this->input->post('rental_unit_id');
		$lease_number = $this->input->post('lease_number');

		$receipt_number = $this->create_credit_note_number();

		$data = array(
			'credit_note_amount'=>$amount,
			'personnel_id'=>$this->session->userdata("personnel_id"),
			'credit_note_date'=>$this->input->post('credit_note_date'),
			'document_number'=>$receipt_number,
			'credit_note_created'=>date("Y-m-d"),
			'year'=>$invoice_year,
			'month'=>$invoice_month,
			'credit_note_created_by'=>$this->session->userdata("personnel_id"),
			'approved_by'=>$personnel_id,
			'rental_unit_id'=>$rental_unit_id,
			'tenant_id'=>$tenant_id,
			'account_id'=>$lease_number,
			'date_approved'=>date('Y-m-d'),
			'remarks'=>$reason,
			'bank_id'=>20
		);
		$data['lease_id'] = $lease_id;

		if($this->db->insert('credit_notes', $data))
		{
			$credit_note_id = $this->db->insert_id();
			$service = array(
								'credit_note_id'=>$credit_note_id,
								'credit_note_item_created' =>$this->input->post('credit_note_date'),
								'credit_note_item_status'=>1,
								'credit_note_year'=>$invoice_year,
								'credit_note_month'=>$invoice_month,
								'rental_unit_id'=>$rental_unit_id,
								'document_no'=>$receipt_number,
								'remarks'=>$reason,
								'tenant_id'=>$tenant_id,
								'lease_number'=>$lease_number,
								'lease_id'=>$lease_id
							);
			$this->db->where('credit_note_item_status = 0 AND lease_id = '.$lease_id.' AND rental_unit_id = '.$rental_unit_id.' and personnel_id = '.$this->session->userdata('personnel_id'));
			$this->db->update('credit_note_item',$service);


			return TRUE;
		}

	}


	function create_credit_note_number()
	{
		//select product code
		$preffix = "HA-RT-";
		$this->db->from('credit_notes');
		$this->db->where("credit_note_id > 0");
		$this->db->select('MAX(document_number) AS number');
		$query = $this->db->get();//echo $query->num_rows();

		if($query->num_rows() > 0)
		{
			$result = $query->result();
			$number =  $result[0]->number;

			$number++;//go to the next number
		}
		else{//start generating receipt numbers
			$number = 1;
		}

		return $number;
	}





	// debit notes


	public function add_debit_note_item($lease_id)
	{
		$amount = $this->input->post('amount');
		$invoice_type_id=$this->input->post('invoice_type_id');
		// $session_number=$this->input->post('session_number');
		// insert for service charge
		$service = array(
							'debit_note_id'=>0,
							'debit_note_amount'=> $amount,
							'invoice_type_id' => $invoice_type_id,
							'debit_note_item_status' => 0,
							'lease_id' => $lease_id,
							'personnel_id' => $this->session->userdata('personnel_id'),
							'debit_note_item_created' => date('Y-m-d')
						);
		$this->db->insert('debit_note_item',$service);
		return TRUE;

	}

	public function get_lease_debit_notes($lease_id,$limit=null)
	{
		$this->db->where('leases.lease_id = debit_notes.lease_id AND debit_notes.lease_id = '.$lease_id.'  AND debit_notes.cancel = 0');
		if($limit)
		{
			$this->db->limit($limit);
		}
		$this->db->order_by('debit_notes.debit_note_date','DESC');
		return $this->db->get('debit_notes,leases');
	}

	public function confirm_debit_note($lease_id,$personnel_id = NULL)
	{
		$amount = $this->input->post('total_amount');
		$rental_unit_id = $this->input->post('rental_unit_id');
		$lease_number=$this->input->post('lease_number');
		$tenant_id=$this->input->post('tenant_id');

		// calculate the points to get
		$debit_note_date = $this->input->post('debit_note_date');

		$date_check = explode('-', $debit_note_date);
		$month = $date_check[1];
		$year = $date_check[0];


		$receipt_number = $this->create_debit_note_number();

		$data = array(
			'debit_note_amount'=>$amount,
			'personnel_id'=>$this->session->userdata("personnel_id"),
			'debit_note_date'=>$this->input->post('debit_note_date'),
			'document_number'=>$receipt_number,
			'debit_note_created'=>$debit_note_date,
			'year'=>$year,
			'month'=>$month,
			'debit_note_created_by'=>$this->session->userdata("personnel_id"),
			'approved_by'=>$personnel_id,
			'rental_unit_id'=>$rental_unit_id,
			'account_id'=>$lease_number,
			'tenant_id'=>$tenant_id,
			'date_approved'=>date('Y-m-d')
		);
		$data['lease_id'] = $lease_id;

		if($this->db->insert('debit_notes', $data))
		{
			$debit_note_id = $this->db->insert_id();
			$service = array(
								'debit_note_id'=>$debit_note_id,
								'debit_note_item_created' =>$this->input->post('debit_note_date'),
								'debit_note_item_status'=>1,
								'debit_note_year'=>$year,
								'debit_note_month'=>$month,
								'rental_unit_id'=>$rental_unit_id,
								'lease_number'=>$lease_number,
								'tenant_id'=>$tenant_id,
								'lease_id'=>$lease_id
							);
			$this->db->where('debit_note_item_status = 0 AND lease_id = '.$lease_id.' and personnel_id = '.$this->session->userdata('personnel_id'));
			$this->db->update('debit_note_item',$service);


			return TRUE;
		}

	}


	function create_debit_note_number()
	{
		//select product code
		$preffix = "HA-RT-";
		$this->db->from('debit_notes');
		$this->db->where("debit_note_id > 0");
		$this->db->select('MAX(document_number) AS number');
		$query = $this->db->get();//echo $query->num_rows();

		if($query->num_rows() > 0)
		{
			$result = $query->result();
			$number =  $result[0]->number;

			$number++;//go to the next number
		}
		else{//start generating receipt numbers
			$number = 1;
		}

		return $number;
	}

	public function get_invoice_items($lease_invoice_id)
	{
		$this->db->where('lease_invoice.lease_invoice_id = invoice.lease_invoice_id AND lease_invoice.lease_invoice_id = '.$lease_invoice_id);
		$this->db->group_by('lease_invoice.lease_invoice_id');
		$this->db->order_by('lease_invoice.invoice_date','DESC');
		return $this->db->get('invoice,lease_invoice');
	}


	// lease invoices

	public function get_lease_invoice($lease_id,$limit=null,$invoice_cat=null)
	{
		$where = 'lease_invoice.lease_invoice_id = invoice.lease_invoice_id AND lease_invoice.invoice_deleted = 0 AND lease_invoice.lease_id = '.$lease_id;
		if($limit)
		{
			$this->db->limit($limit);
		}

		if($invoice_cat)
		{
			$where .=' AND invoice_cat ='.$invoice_cat;
		}
		$this->db->where($where);
		$this->db->group_by('lease_invoice.lease_invoice_id');
		$this->db->order_by('lease_invoice.invoice_date','DESC');
		return $this->db->get('invoice,lease_invoice');
	}

	public function add_invoice_item($lease_id)
	{
		$amount = $this->input->post('amount');
		$invoice_type_id=$this->input->post('invoice_type_id');

		$this->db->where('invoice_type.invoice_type_id = property_billing.invoice_type_id ANd property_billing.property_billing_deleted = 0 AND invoice_type.invoice_type_id = '.$invoice_type_id.' AND property_billing.lease_id = '.$lease_id);
		$query_one = $this->db->get('invoice_type,property_billing');
		$query_row = $query_one->row();
		$tax_amount = $query_row->tax_amount;

		$invoice_type_id=$this->input->post('invoice_type_id');


		$this->db->where('invoice_type.invoice_type_id = '.$invoice_type_id);
		$query_two = $this->db->get('invoice_type');
		$query_three = $query_two->row();
		$invoice_type_name = $query_three->invoice_type_name;

		$service = array(
							
							'invoice_amount'=> $amount,
							'invoice_type' => $invoice_type_id,
							'invoice_item_status' => 0,
							'lease_id' => $lease_id,
							'personnel_id' => $this->session->userdata('personnel_id'),
							'created' => date('Y-m-d'),
							'tax_amount'=>$tax_amount,
							'remarks'=>$invoice_type_name
						);
		$lease_invoice_id = $this->input->post('lease_invoice_id');

		if(!empty($lease_invoice_id))
		{
			$service['lease_invoice_id'] = $lease_invoice_id;
		}
		else
		{
			$service['lease_invoice_id'] = 0;
		}

		$this->db->insert('invoice',$service);
		return TRUE;

	}

	public function confirm_invoices($lease_id,$personnel_id = NULL)
	{
		$amount = $this->input->post('total_amount');
		$rental_unit_id = $this->input->post('rental_unit_id');

		// calculate the points to get
		$invoice_date = $this->input->post('invoice_date');
		$invoice_cat = $this->input->post('invoice_cat');
		$lease_number = $this->input->post('lease_number');
		if(empty($invoice_cat))
		{
			$invoice_cat = 0;
		}

		$date_check = explode('-', $invoice_date);
		$month = $date_check[1];
		$year = $date_check[0];

		$invoice_month = $this->input->post('invoice_month');
		$invoice_year = $this->input->post('invoice_year');
		$tenant_id = $this->input->post('tenant_id');

		$document_number = $this->create_invoice_number();

		// var_dump($checked); die();

		$insertarray['invoice_date'] = $invoice_date;
		$insertarray['invoice_year'] = $invoice_year;
		$insertarray['invoice_month'] = $invoice_month;
		$insertarray['lease_id'] = $lease_id;
	
		$insertarray['total_amount'] = $amount;
		$insertarray['rental_unit_id'] = $rental_unit_id;
		$insertarray['created_by'] = $this->session->userdata('personnel_id');
		$insertarray['created'] = date('Y-m-d');
		$insertarray['prefix'] = '';
		$insertarray['suffix'] = '';
		$insertarray['lease_id'] = $lease_id;
		$insertarray['tenant_id'] = $tenant_id;
		$insertarray['account_id'] = $lease_number;
		$insertarray['invoice_cat'] = $invoice_cat;

		$lease_invoice_id = $this->input->post('lease_invoice_id');
		
		if(!empty($lease_invoice_id))
		{


		}
		else
		{
			$insertarray['document_number'] = $document_number;
		}

		if(!empty($lease_invoice_id))
		{
			$this->db->where('lease_invoice_id',$lease_invoice_id);
			if($this->db->update('lease_invoice', $insertarray))
			{
				
				$total_visits = sizeof($_POST['tenant_invoice_items']);
				// var_dump($total_visits);die();
		  		//check if any checkboxes have been ticked
		  		if($total_visits > 0)
		  		{
		  			for($r = 0; $r < $total_visits; $r++)
		  			{
		  				$visit = $_POST['tenant_invoice_items'];
		  				$invoice_id = $visit[$r];
		  				// var_dump($visit);die();
		  				//check if card is held


			  			$service = array(
											'lease_invoice_id'=>$lease_invoice_id,
											'created' =>$invoice_date,
											'invoice_item_status'=>1,
											'year'=>$invoice_year,
											'month'=>$invoice_month,
											'billing_schedule_quarter'=>$curr_quarter,
											'rental_unit_id'=>$rental_unit_id,
											'tenant_id'=>$tenant_id,
											'lease_number'=>$lease_number,
											'lease_id'=>$lease_id
										);


						$this->db->where('invoice_id',$invoice_id);
						$this->db->update('invoice',$service);

		  			}
		  		}


				return TRUE;
			}
			else{
				return FALSE;
			}
		}
		else
		{
			
			if($this->db->insert('lease_invoice', $insertarray))
			{
				$lease_invoice_id = $this->db->insert_id();
				$current_date = strtotime($date_check);
				$current_quarter = ceil(date('m', $current_date) / 3);
				$current_month = ($current_quarter * 3) - 2;
				$current_year = date('Y', $current_date);

				$curr_quarter = 'AC'.$current_quarter.'-'.$current_year;


				$total_visits = sizeof($_POST['tenant_invoice_items']);

		  		//check if any checkboxes have been ticked
		  		if($total_visits > 0)
		  		{
		  			for($r = 0; $r < $total_visits; $r++)
		  			{
		  				$visit = $_POST['tenant_invoice_items'];
		  				$invoice_id = $visit[$r];
						$service = array(
											'lease_invoice_id'=>$lease_invoice_id,
											'created' =>$invoice_date,
											'invoice_item_status'=>1,
											'year'=>$invoice_year,
											'month'=>$invoice_month,
											'billing_schedule_quarter'=>$curr_quarter,
											'rental_unit_id'=>$rental_unit_id,
											'tenant_id'=>$tenant_id,
											'lease_number'=>$lease_number,
											'lease_id'=>$lease_id
										);
						$this->db->where('invoice_id',$invoice_id);
						$this->db->update('invoice',$service);
					}
				}

				return TRUE;
			}
			else{
				return FALSE;
			}
		}
		

	}
	public function get_deposits_paid($lease_id)
	{

		$this->db->from('payment_item,payments');
		$this->db->select('SUM(payments.amount_paid) AS total_paid');
		$this->db->where('payments.lease_id = '.$lease_id.' AND payments.payment_id = payment_item.payment_id AND (invoice_type_id =15 OR invoice_type_id = 4 OR invoice_type_id =2)');
		$query = $this->db->get();
		$total_paid = 0;
		foreach ($query->result() as $key) {
			# code...
			$total_paid = $key->total_paid;
		}
		return $total_paid;

	}

	public function get_all_invoice_items($lease_id)
	{
		$this->db->from('v_transactions,invoice_type');
		$this->db->select('SUM(dr_amount) - SUM(cr_amount) AS total_amount,invoice_type_name,v_transactions.accountId');
		$this->db->where('v_transactions.accountId = invoice_type.invoice_type_id AND v_transactions.lease_id = '.$lease_id.'');
		$this->db->group_by('accountId');
		$query = $this->db->get();

		return $query;
	}

	public function get_expenses_payable($lease_id)
	{

		$this->db->from('lease_invoice,invoice');
		$this->db->select('SUM(lease_invoice.total_amount) AS total_paid');
		$this->db->where('lease_invoice.lease_id = '.$lease_id.' AND invoice_cat = 1');
		$query = $this->db->get();
		$total_paid = 0;
		foreach ($query->result() as $key) {
			# code...
			$total_paid = $key->total_paid;
		}



		return $total_paid;

	}

	public function get_amount_reconcilled($mpesa_id)
	{
		$this->db->from('payment_item,payments');
		$this->db->select('SUM(payment_item.amount_paid) AS total_paid');
		$this->db->where('payments.payment_id = payment_item.payment_id AND payment_item.mpesa_id = '.$mpesa_id.' AND payments.cancel = 0');
		$query = $this->db->get();
		$total_paid = 0;
		foreach ($query->result() as $key) {
			# code...
			$total_paid = $key->total_paid;
		}



		return $total_paid;
	}
	public function get_unallocated_mpesa($mpesa_id)
	{
		$this->db->from('payment_item');
		$this->db->select('SUM(payment_item.amount_paid) AS total_paid');
		$this->db->where('payment_item.mpesa_id = '.$mpesa_id.'');
		$query = $this->db->get();
		$total_paid = 0;
		foreach ($query->result() as $key) {
			# code...
			$total_paid = $key->total_paid;
		}
		return $total_paid;
	}

	public function cancel_mpesa_payment($mpesa_id)
	{
		$data = array(
			"cancel_action_id" => $this->input->post('cancel_action_id'),
			"cancel_description" => $this->input->post('cancel_description'),
			"cancelled_by" => $this->session->userdata('personnel_id'),
			"cancelled_date" => date("Y-m-d"),
			"mpesa_status" => 1
		);

		$this->db->where('mpesa_id', $mpesa_id);
		if($this->db->update('mpesa_transactions', $data))
		{
			return TRUE;
		}

		else
		{
			return FALSE;
		}
	}


	// teannts
	public function get_tenant_lease_brought_forward($lease_id, $invoice_date=NULL,$invoice_type_id = NULL,$month=null,$year=null,$rental_unit_id=null)
	{

		if(!empty($month))
		{
				// $invoice_date_checked = 'AND transactionDate < "'.$invoice_date.'" ';
				$invoice_date_checked = ' AND (transaction_year <  "'.$year.'" OR (transaction_year = '.$year.' AND transaction_month < "'.$month.'") )';
		}
		else {
			$invoice_date_checked = '';
		}


		$where = 'lease_id = '.$lease_id.'  and accountId =  '.$invoice_type_id.$invoice_date_checked;
		$this->db->from('v_transactions');
		$this->db->select('SUM(dr_amount) - SUM(cr_amount)  AS total_invoice');
		$this->db->where($where);
		$query = $this->db->get();
		$total_invoice = 0;
		if($query->num_rows() >0)
		{
			$row=$query->row();
			$total_invoice = $row->total_invoice;
		}
		return $total_invoice;
	}

	public function get_lease_balances($lease_id,$invoice_type_id,$todays_month,$todays_year,$rental_unit_id,$parent_invoice_date,$previous_invoice_date)
	{
		$total_credit_note = $this->accounts_model->get_rent_credit_note_brought_forward($lease_id,$parent_invoice_date,1,$todays_month,$todays_year,$rental_unit_id);
		$total_debit_note =$this->accounts_model->get_rent_debit_note_brought_forward($lease_id,$parent_invoice_date,1,$todays_month,$todays_year,$rental_unit_id);


		$rent_payment_amount = $this->accounts_model->get_rent_payments_brought_forward($lease_id,$parent_invoice_date,1,$todays_month,$todays_year,$rental_unit_id);

		$rent_invoice_amount =$this->accounts_model->get_rent_invoices_brought_forward($lease_id,$parent_invoice_date,1,$todays_month,$todays_year,$rental_unit_id);

		// $brought_forward = $this->accounts_model->get_tenant_lease_brought_forward($lease_id,$parent_invoice_date,1,$todays_month,$todays_year,$rental_unit_id);

		$brought_forward = ($rent_invoice_amount+$total_debit_note) - ($rent_payment_amount+$total_credit_note);

		// var_dump($previous_invoice_date);die();
		$rent_current_invoice =  $this->accounts_model->get_current_month_invoice($lease_id,$todays_month,$todays_year,1,$previous_invoice_date,$parent_invoice_date);
		$rent_current_payment = $this->accounts_model->get_current_months_payments($lease_id,$todays_month,$todays_year,1,$previous_invoice_date,$parent_invoice_date);

		$total_current_variance = $rent_current_invoice - $rent_current_payment;

		$total_balance = $brought_forward + $total_current_variance;

		return $total_balance;
	}


	/*
	*	Retrieve all tenants
	*	@param string $table
	* 	@param string $where
	*
	*/
	public function get_all_invoices_transactions($table, $where, $per_page, $page, $order, $order_method = 'ASC')
	{
		//retrieve all tenants
		$this->db->from($table);
		$this->db->select('lease_invoice.*,invoice.*,invoice.remarks AS item_remarks');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		$this->db->group_by('lease_invoice.lease_invoice_id');
		$query = $this->db->get('', $per_page, $page);

		return $query;
	}

	/*
	*	Retrieve all tenants
	*	@param string $table
	* 	@param string $where
	*
	*/
	public function get_all_transactions($table, $where, $per_page, $page, $order, $order_method = 'ASC')
	{
		//retrieve all tenants
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		$query = $this->db->get('', $per_page, $page);

		return $query;
	}


	public function get_all_payment_transactions($table, $where, $per_page, $page, $order, $order_method = 'ASC')
	{
		//retrieve all tenants
		$this->db->from($table);
		$this->db->select('*,payments.year AS payment_year, payments.month AS payment_month,SUM(payment_item.amount_paid) AS total_amount');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		$this->db->group_by('payments.payment_id');
		$this->db->join('payment_method','payment_method.payment_method_id = payments.payment_method_id','left');
		$this->db->join('fn_banks','payments.bank_id = fn_banks.id','left');
		$query = $this->db->get('', $per_page, $page);

		return $query;
	}


	
}
?>
