<?php

$result = '';

//if users exist display them
if ($query->num_rows() > 0)
{
	$count = 0;

	$result .=
	'
	<table class="table table-bordered table-striped table-condensed">
		<thead>
			<tr>
				<th>#</th>
				<th>Date Paid</th>
				<th>Serial Number</th>
				<th>Phone Number</th>
				<th>Sender Name</th>
				<th>Account </th>
				<th>Amount </th>
				<th>Recon </th>
			</tr>
		</thead>
		  <tbody>

	';


	foreach ($query->result() as $leases_row)
	{
		$created = $leases_row->created;
		$mpesa_id = $leases_row->mpesa_id;
		$serial_number = $leases_row->serial_number;
		$account_number = $leases_row->account_number;
		$sender_name = $leases_row->sender_name;
		$sender_phone = $leases_row->sender_phone;
		$amount = $leases_row->amount;
		$account_number = $leases_row->account_number;

		$amount_recon = $this->accounts_model->get_amount_reconcilled($mpesa_id);

		$sender_name = str_replace('%20', ' ', $sender_name);

		// $expiry_date  = date('jS M Y',strtotime($lease_start_date, mktime()) . " + 365 day");
		$date_sent  = date('jS M Y', strtotime($created));
		$count++;



		$result .=
					'
						<tr>
							<td>'.$count.'</td>
							<td>'.$date_sent.'</td>
							<td>'.$serial_number.'</td>
							<td>'.$sender_phone.'</td>
							<td>'.$sender_name.'</td>
							<td>'.$account_number.'</td>
							<td>'.number_format($amount ,2).'</td>
							<td>'.number_format($amount_recon ,2).'</td>

						</tr>
					';


	}

	$result .=
	'
				  </tbody>
				</table>
	';
}

else
{
	$result .= "There are no leases created";
}
?>
<!DOCTYPE html>
<html lang="en">
      <head>
        <title><?php echo $contacts['company_name'];?> | Property Statement</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" media="all"/>
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css" media="all"/>
        <style type="text/css">
				body{font-style: times new roman;}
			.receipt_spacing{letter-spacing:0px;}
			.center-align{margin:0 auto; text-align:center;}

			.receipt_bottom_border{border-bottom: #888888 medium solid; margin-bottom:1px;}
			.title-img{float:left; padding-left:30px;}
			img.logo{max-height:70px; margin:0 auto;}
			.align-right{margin:0 auto; text-align: right !important;}
			.align-center
			{
				/*padding: 50px;*/
    			text-align: center;
    			font-weight: bolder;
			}
			.table {
			  font-size: 12px !important;
			  margin-bottom: 15px;
			  max-width: 100%;
			  width: 100%;
				border-top: #000 solid 1px;


			}
			.row {
  				margin-left: 0px !important;
    			margin-right: 0px !important;
			}
			.table-condensed > thead > tr > th, .table-condensed > tbody > tr > th, .table-condensed > tfoot > tr > th, .table-condensed > thead > tr > td, .table-condensed > tbody > tr > td, .table-condensed > tfoot > tr > td {
			  padding: 2px;
				border: #000 solid 1px;
			}
			h3, .h3 {
			  font-size: 18px !important;
			}
		</style>
    </head>
     <body class="receipt_spacing">
				<div class="row " >
          <div class="col-md-12 " >
   				 <div class="col-md-6 left-align" style="text-align:left;">
                <img src="<?php echo base_url().'assets/logo/'.$contacts['logo'];?>" alt="<?php echo $contacts['company_name'];?>" class="img-responsive logo"/>
   				 </div>
           <div class="col-md-6" style="text-align:right;">
             <?php echo $company_name;?><br>
              <?php echo $contacts['location'];?>,<br/>
               Address : P.O. Box <?php echo $contacts['address'];?> <?php echo $contacts['post_code'];?>, <?php echo $contacts['city'];?> <br/>
               Phone:  <?php echo $contacts['phone'];?>
               <br/> <?php echo $contacts['email'];?>.<br/>
           </div>
         </div>
   		 </div>
        <div class="row">
            <div class="col-xs-12">
            	<tr>
                    <th class="align-center"><h3><?php echo $title;?> </h3></th>
                </tr>
                <?php echo $result;?>
            </div>
        </div>

        <div class="row" >
       		<div class="col-xs-12 align-center" style="border-top:1px #000 solid;">
       			<span style="font-size:10px; " > <?php echo $contacts['company_name'];?> | <?php echo $contacts['location'];?>, <?php echo $contacts['building'];?>, <?php echo $contacts['floor'];?>
                     E-mail: <?php echo $contacts['email'];?>.<br/> Tel : <?php echo $contacts['phone'];?>
                     P.O. Box <?php echo $contacts['address'];?> <?php echo $contacts['post_code'];?>, <?php echo $contacts['city'];?></span>
            </div>
       	</div>
     </body>
    </html>
