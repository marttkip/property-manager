<?php
	$all_leases = $this->leases_model->get_lease_detail($lease_id);
	$items = '';
	$items_charge ='';
	$total_bill = 0;
	$service_charge = 0;
	$penalty_charge =0;
	$water_charge =0;
	foreach ($all_leases->result() as $leases_row)
	{
		$lease_id = $leases_row->lease_id;
		$tenant_unit_id = $leases_row->tenant_unit_id;
		$property_name = $leases_row->property_name;
		// $units_name = $leases_row->units_name;
		$rental_unit_name = $leases_row->rental_unit_name;
		$tenant_name = $leases_row->tenant_name;
		$lease_start_date = $leases_row->lease_start_date;
		$lease_duration = $leases_row->lease_duration;
		$rent_amount = $leases_row->rent_amount;
		$lease_number = $leases_row->lease_number;
		$arrears_bf = $leases_row->arrears_bf;
		$rent_calculation = $leases_row->rent_calculation;
		$deposit = $leases_row->deposit;
		$deposit_ext = $leases_row->deposit_ext;
		$tenant_phone_number = $leases_row->tenant_phone_number;
		$tenant_national_id = $leases_row->tenant_national_id;
		$lease_status = $leases_row->lease_status;
		$tenant_status = $leases_row->tenant_status;
		$created = $leases_row->created;
		$message_prefix = $leases_row->message_prefix;

		$lease_start_date = date('jS M Y',strtotime($lease_start_date));
		
		// $expiry_date  = date('jS M Y',strtotime($lease_start_date, mktime()) . " + 365 day");
		$expiry_date  = date('jS M Y', strtotime(''.$lease_start_date.'+1 years'));
		
	}

	$today = date('jS F Y H:i a',strtotime(date("Y:m:d h:i:s")));
	
	$served_by = $this->accounts_model->get_personnel($this->session->userdata('personnel_id'));
	// get when the lease was active

	// get all the invoiced months
	// var_dump($lease_invoice); die();
	
	if($lease_invoice->num_rows() > 0)
	{
		$electricity_charge = 0;
		$electricity_bf = 0;
		$water_charge = 0;
		$service_bf = 0;
		$water_charge = 0;
		$water_bf = 0;
		$penalty_charge =0;
		$penalty_bf =0;
		$borehole = 0;
		$prev_reading = 0;
		$current_reading =0;
		$elec_prev_reading = 0;
		$elec_current_reading =0;
		$water_bill_lamp = 0;
		$elect_bill_lamp = 0;

		foreach ($lease_invoice->result() as $key_invoice) {
			# code...
			$invoice_date = $key_invoice->invoice_date;
			$invoice_id = $key_invoice->invoice_id;
			$invoice_type = $key_invoice->invoice_type;

			
			$invoice_date_date = date('jS F Y',strtotime($invoice_date));
			
			if($invoice_type == 2)
			{
				// service charge
				$water_charge = $key_invoice->invoice_amount;
				$water_bf = $key_invoice->arrears_bf;
				$items .= '
							 '.form_open("import/update-invoice-item/".$invoice_id, array("class" => "form-horizontal", "role" => "form")).'
								 <div class="row">
						        	<div class="col-xs-2">
						        		'.$rental_unit_name.'
						        	</div>
						        	<div class="col-xs-2">
						        		'.number_format($water_bf).'
						        	</div>
						        	<div class="col-xs-2">
						        		<input type="text" class="form-control" name="" value="'.($water_charge - $water_bf).'" >
						        	</div>
						        	<div class="col-xs-3 center-align">
						        		'.number_format($water_charge).'
						        	</div>
						        	<div class="col-xs-2 pull-right">
						        		<button class="btn btn-warning btn-sm  fa fa-check" onclick="return confirm("Do you want to make this change ?")""></button>
						        		<a class="btn btn-sm btn-danger fa fa-trash " href="'.site_url().'accounts/delete_payment_item/'.$invoice_id.'"></a>
						        	</div>
						        </div>
						    '.form_close().'
							';
			}


			if($invoice_type == 5)
			{
				// penalty

				$penalty_charge = $key_invoice->invoice_amount;
				$penalty_bf = $key_invoice->arrears_bf;
				$items_charge .= 			
								'
								 <div class="row">
						        	<div class="col-xs-2">
						        		'.$rental_unit_name.'
						        	</div>
						        	<div class="col-xs-2">
						        		'.number_format($penalty_bf).'
						        	</div>
						        	<div class="col-xs-2 ">
						        		'.number_format(($penalty_charge - $penalty_bf) ).'
						        	</div>
						        	<div class="col-xs-3 pull-right">
						        		'.number_format($penalty_charge).'
						        	</div>
						        </div>
								';
			}

			$total_bill = $total_bill + $water_charge + $penalty_charge;
		}
	}



?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <title><?php echo $contacts['company_name'];?> | INVOICE</title>
        <!-- For mobile content -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" media="all"/>
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css" media="all"/>
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/";?>fontawesome/css/font-awesome.css">
		
        <style type="text/css">
			.receipt_spacing{letter-spacing:0px;}
			.center-align{margin:0 auto; text-align:center;}
			
			.receipt_bottom_border{border-bottom: #888888 medium solid; margin-bottom:1px;}
			.title-img{float:left; padding-left:30px;}
			img.logo{max-height:70px; margin:0 auto;}
			.align-right{margin:0 auto; text-align: right !important;}
			.align-center
			{
				/*padding: 50px;*/
    			text-align: center;
    			font-weight: bolder;
			}
			.table {
			  margin-bottom: 5px;
			  max-width: 100%;
			  width: 100%;
			}
			.row {
  				margin-left: 0px !important;
    			margin-right: 0px !important;
			}
			.title
			{
				font-weight: bold !important;
			}
		</style>
    </head>
    <body class="receipt_spacing">
        <div class="row">
	        <div class="col-xs-12">
		    	<div class="receipt_bottom_border">
		        	<table class="table table-condensed">
		                <tr>
		                    <th><h4><?php echo $contacts['company_name'];?> </h4></th>
		                    <th class="align-right">
		                        <img src="<?php echo base_url().'assets/logo/'.$contacts['logo'];?>" alt="<?php echo $contacts['company_name'];?>" class="img-responsive logo" style="float:right;"/>
		                    </th>
		                </tr>
		            </table>
		        </div>
		    	
		        
		        <!-- Patient Details -->
		    	<div class="row receipt_bottom_border" style="margin-bottom: 10px;">
		        
		            <div class="col-xs-6 ">
		                <h2 class="panel-title pull-left">Personal Information</h2>
		                <br>

		               	<div class="row pull-left">
	               			<div class="col-xs-12 ">
				        		<strong>Name</strong>
				        		<?php echo $tenant_name;?>
				        	</div>
				        </div>
				        <div class="row pull-left">
				        	<div class="col-xs-12">
				        		<strong> Phone </strong>
				        		<?php echo $tenant_phone_number;?>
				        	</div>
				        </div>
				        <div class="row pull-left">
				        	<div class="col-xs-12  ">
				        		<strong>Property</strong>
				        		<?php echo $property_name;?>
				        	</div>
				        </div>
		            </div>
		             <div class="col-xs-4">
		                <h2 class="panel-title">Invoice</h2>

		                <div class="row pull-left">
	               			<div class="col-xs-12 ">
				        		<strong>Date</strong>
				        		<?php echo date('jS M Y',strtotime(date('Y-m-d')));?>
				        	</div>
				        </div>
		            </div>
		        </div>
		        <div class="row">
			        <div class="col-xs-12">
			        	 <div class="row receipt_bottom_border">
				        	<div class="col-xs-12" id="title">
				            	<strong>A. WATER DUES</strong>
				            </div>
				        </div>
				        <div class="row">
				        	<div class="col-xs-2 title">
				        		Apartment
				        	</div>
				        	<div class="col-xs-2 title">
				        		B/F KES
				        	</div>
				        	<div class="col-xs-2 title">
				        		Current Dues
				        	</div>
				        	<div class="col-xs-3 title center-align">
				        		Total Dues
				        	</div>
				        	<div class="col-xs-2 title pull-right">
				        		Actions
				        	</div>
				        </div>
				        <?php echo $items;?>
			        </div>
			   	</div>
			   	<div class="row">
			        <div class="col-xs-12">
			        	<div class="row receipt_bottom_border">
				        	<div class="col-xs-12"  id="title">
				            	<strong>B. Penalties</strong>
				            </div>
				        </div>
				       
			        	<table class="table table-hover">
				           
				            <tbody>
				               	<?php echo $items_charge;?>
				            </tbody>
				        </table>
			        </div>
			     </div>
			    <div class="row" >
			    	<div class="col-xs-12" style="padding-top: 10px;padding-bottom: 5px;">
			    		<div class="row receipt_bottom_border">
				        	<div class="col-xs-12"  id="title">
				            	<strong>Total Dues </strong>
				            </div>
				        </div>
				        <div class="row">
				        	<div class="col-xs-8 pull-left">
				        		Total Dues  A+B 
				        	</div>
				        	<div class="col-xs-3 pull-right">
				        	  <strong> <?php echo number_format($total_bill);?> </strong>
				        	</div>
				        </div>
				    </div>
			    </div>
		       
		       <div class="row " style="border-top:1px #000 solid; ">
		       		<div class="col-xs-8 pull-left">
		       			<strong style="text-align:left;"> <?php echo $message_prefix;?></strong>
		       		</div>

		       			
		            
		       </div>
		       <div class="row" >
		       		<div class="col-xs-12 align-center" style="border-top:1px #000 solid;">
		       			<span style="font-size:10px; " > <?php echo $contacts['company_name'];?> | <?php echo $contacts['location'];?>, <?php echo $contacts['building'];?>, <?php echo $contacts['floor'];?>
		                     E-mail: <?php echo $contacts['email'];?>.<br/> Tel : <?php echo $contacts['phone'];?>
		                     P.O. Box <?php echo $contacts['address'];?> <?php echo $contacts['post_code'];?>, <?php echo $contacts['city'];?></span>
		            </div>
		       </div>
		       
		</div>
		</div>
        
    </body>
    
</html>

