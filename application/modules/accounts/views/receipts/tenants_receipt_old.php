<?php

$company_name = $contacts['company_name'];


$all_leases = $this->leases_model->get_lease_detail($lease_id);
$items = '';
$items_charge ='';
$total_bill = 0;
$service_charge = 0;
$penalty_charge =0;
$water_charge =0;
foreach ($all_leases->result() as $leases_row)
{
	$lease_id = $leases_row->lease_id;
	$tenant_unit_id = $leases_row->tenant_unit_id;
	$property_name = $leases_row->property_name;
	// $units_name = $leases_row->units_name;
	$rental_unit_name = $leases_row->rental_unit_name;
	$tenant_name = $leases_row->tenant_name;
	$lease_start_date = $leases_row->lease_start_date;
	$lease_duration = $leases_row->lease_duration;
	$rent_amount = $leases_row->rent_amount;
	$lease_number = $leases_row->lease_number;
	$arrears_bf = $leases_row->arrears_bf;
	$rent_calculation = $leases_row->rent_calculation;
	$deposit = $leases_row->deposit;
	$deposit_ext = $leases_row->deposit_ext;
	$tenant_phone_number = $leases_row->tenant_phone_number;
	$tenant_email = $leases_row->tenant_email;
	$tenant_national_id = $leases_row->tenant_national_id;
	$lease_status = $leases_row->lease_status;
	$tenant_status = $leases_row->tenant_status;
	$created = $leases_row->created;
	$account_id = $leases_row->account_id;
	$property_code = $leases_row->property_code;
	$message_prefix = $leases_row->message_prefix;

	$lease_start_date = date('jS M Y',strtotime($lease_start_date));

	// $expiry_date  = date('jS M Y',strtotime($lease_start_date, mktime()) . " + 365 day");
	$expiry_date  = date('jS M Y', strtotime(''.$lease_start_date.'+1 years'));

}

$today = date('jS F Y H:i a',strtotime(date("Y:m:d h:i:s")));

$served_by = $this->accounts_model->get_personnel($this->session->userdata('personnel_id'));



$due_date = date('jS F Y', strtotime($invoice_date. ' + 6 days'));

$tenants_response = $this->accounts_model->get_lease_balance($lease_id);
$total_pardon_amount = $tenants_response['total_pardon'];


$account_invoice_amount = $this->accounts_model->get_invoice_tenants_brought_forward($lease_id,$parent_invoice_date);
$account_paid_amount = $this->accounts_model->get_paid_tenants_brought_forward($lease_id,$parent_invoice_date);
$account_todays_payment =  $this->accounts_model->get_today_tenants_paid_current_forward($lease_id,$parent_invoice_date);
$total_brought_forward_account = $account_invoice_amount - $account_paid_amount - $account_todays_payment - $total_pardon_amount;

$invoice_result = '';
$bf ='';
if($total_brought_forward > 0)
{
	$bf = '
			<tr>
		        <td colspan="3">Balance B /F</td>
		        <td>'.number_format($total_brought_forward).'</td>
		      </tr>
			';
}
$invoice_result = $bf.$result;

$total_bill = $total_rent + $total_deposit + $total_brought_forward_account + $total_service_charge + $total_variance;



// $lease_pardons = $this->accounts_model->get_lease_pardons($lease_id);
// 	$total_pardons = 0;
// 	if($lease_pardons->num_rows() > 0)
// 	{
//
// 		foreach ($lease_pardons->result() as $key_pardons) {
// 			# code...
// 			$document_number = $key_pardons->document_number;
// 			$pardon_amount = $key_pardons->pardon_amount;
// 			$pardon_id = $key_pardons->pardon_id;
// 			$created_by = $key_pardons->created_by;
// 			$payment_date = $key_pardons->payment_date;
// 			$pardon_date = $key_pardons->pardon_date;
// 			$pardon_reason = $key_pardons->pardon_reason;
//
// 			$total_pardons = $total_pardons + $pardon_amount;
// 		}
// 	}




	$today = date('jS F Y H:i a',strtotime(date("Y:m:d h:i:s")));
	// var_dump($lease_payments->result()); die();
	if($lease_payments->num_rows() > 0)
	{
		$y = 0;
		foreach ($lease_payments->result() as $key)
		{
			$payment_id = $key->payment_id;
			$receipt_number = $key->receipt_number;
			$amount_paid = $key->amount_paid;
			$paid_by = $key->paid_by;
			$payment_date = $date_of_payment = $key->payment_date;
			$payment_created = $key->payment_created;
			$payment_created_by = $key->payment_created_by;
			$transaction_code = $key->transaction_code;
			$document_number = $key->document_number;
			$payment_method_name = $key->payment_method;
			$invoice_month_number = $key->month;

			$payment_date = date('jS M Y',strtotime($payment_date));
			$payment_created = date('jS M Y',strtotime($payment_created));
			$y++;
		}
	}




		$invoice_month = date('F', mktime(0,0,0,$invoice_month_number, 1, date('Y')));
		// $month = date('F', mktime(0,0,0,$m, 1, date('Y')));
		$this_month = date('m');
		$this_year = date('Y');

		$total_paid = $this->accounts_model->get_months_last_debt($lease_id,$invoice_month_number);
		$last_bal = $this->accounts_model->get_months_last_arrears($invoice_month_number,$this_year,$lease_id);

		$months_invoice_query = $this->accounts_model->get_months_invoices($lease_id,$invoice_month_number);
		$total_bill = 0;
		$total_rental_bill = 0;
		$total_service_charge = 0;
		// var_dump($months_invoice_query->result()); die();
		foreach ($months_invoice_query->result() as $invoice_key) {

			$invoice_type = $invoice_key->invoice_type;
			if($invoice_type == 1)
			{
				$rental_invoice_amount = $invoice_key->invoice_amount;
				// rental bill
				 $total_rental_bill = $total_rental_bill + $rental_invoice_amount;
			}
			else
			{
				$service_charge_amount = $invoice_key->invoice_amount;
				// service charge
				 $total_service_charge = $total_service_charge + $service_charge_amount;
			}

			$total_bill = $total_bill + ($total_rental_bill +$total_service_charge);
		}
	$served_by = $this->accounts_model->get_personnel($this->session->userdata('personnel_id'));


$invoice_types = $this->accounts_model->get_payments_invoice_types($payment_idd);


// var_dump($invoice_types); die();
$payment_result = '';
$total_balance = 0;
$total_due= 0;
$total_paid = 0;
if($invoice_types->num_rows() >0)
{
	$x = 0;

	foreach ($invoice_types->result() as $types) {
		# code...
		$invoice_type_name = $types->invoice_type_name;
		$invoice_type_id = $types->invoice_type_id;
		$invoice_id = $types->invoice_id;
		$remarks = $types->remarks;
		$amount_due = $this->accounts_model->get_sum_invoice_amount($date_of_payment,$invoice_type_id,$lease_id);
		$amount_paid = $this->accounts_model->get_payments_detail($payment_idd,$invoice_type_id);
		$paid_amount = $this->accounts_model->get_payments_amount($invoice_type_id);
		// -8000 +2000)
		$invoice_amount = $invoiced_amount + $amount_paid;

		$total_amount = $total_amount + $amount_paid;
		$total_due = $total_due + $amount_due;
		$total_paid = $total_paid + $amount_paid;

		$x++;
		$payment_result .= '
						<tr>
					        <td>'.$x.'</td>
					        <td>'.$invoice_type_name.' : '.$remarks.'</td>
							<td>'.number_format($amount_due,2) .'</td>
					        <td>'.number_format($amount_paid,2) .'</td>
					      </tr>
						';

	}
	$items_balance = $total_due - $total_paid;
	// $invoiced_amount = $this->accounts_model->get_total_invoices_before_payment($lease_id,$date_of_payment,$invoice_type_id= NULL);

	// $payed_amount = $this->accounts_model->get_total_payments_before_payment_lease($lease_id,$date_of_payment,$invoice_type_id=NULL,$payment_id);
	// $items_balance = $invoiced_amount - $payed_amount;
	$payment_result .= '
					<tr>
								<td></td>
								<td></td>
								<td>'.number_format($total_due,2) .'</td>
								<td>'.number_format($total_amount,2) .'</td>
								<td>'.number_format($items_balance,2) .'</td>
							</tr>
					';


}


?>


<!DOCTYPE html>
<html lang="en">
      <head>
        <title><?php echo $contacts['company_name'];?> | Tenant Invoice</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" media="all"/>
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css" media="all"/>
        <style type="text/css">
			.receipt_spacing{letter-spacing:0px;}
			.center-align{margin:0 auto; text-align:center;}

			.receipt_bottom_border{border-bottom: #000 medium solid; margin-bottom:1px;}
			.title-img{float:left; padding-left:30px;}
			img.logo{max-height:70px; margin:0 auto;}
			.align-right{margin:0 auto; text-align: right !important;}
			.align-center
			{
				/*padding: 50px;*/
    			text-align: center;
    			font-weight: bolder;
			}
			.table {
			  font-size: 11px !important;
			  margin-bottom: 0px !important;
			  max-width: 100%;
			  width: 100%;

			}
			.row {
  				margin-left: 0px !important;
    			margin-right: 0px !important;
			}
			.table-condensed > thead > tr > th, .table-condensed > tbody > tr > th, .table-condensed > tfoot > tr > th, .table-condensed > thead > tr > td, .table-condensed > tbody > tr > td, .table-condensed > tfoot > tr > td {
			  padding: 1px !important;

			}
			table tr, th, td
			{
				/* border: #000 solid 2px !important; */
				padding: 2px !important;
			}
			h3, .h3 {
			  font-size: 12px !important;
			}
      table
      {
        border: #000 solid 2px !important;
      }
		</style>
    </head>
     <body class="receipt_spacing">

		 <div class="row " >
			 <div class="col-md-12">
					<p><strong>Receipt No: <?php echo $document_number;?></strong></p>
				</div>
				 <div class="col-md-12 center-align">
						 <?php echo $company_name;?><br>
              <?php echo $contacts['location'];?>,<br/>
               Address : P.O. Box <?php echo $contacts['address'];?> <?php echo $contacts['post_code'];?>, <?php echo $contacts['city'];?> <br/>
               Phone:  <?php echo $contacts['phone'];?>
					 </div>
			 </div>


        <!-- Table row -->
        <div class="row">
          <div class="col-md-12 table-responsive">
						<table class="table table-hover table-bordered">
							<thead>
				      <tr>
				        <td> NAME : <?php echo $tenant_number?> <?php echo strtoupper($tenant_name);?><br>
		              PROPERY CODE: <?php echo $property_code?> UNIT NO: <?php echo $rental_unit_name?>  <br></td>
								<td>	DATE : <?php echo strtoupper($payment_date);?><br>
									 PROPERY Name: <?php echo $property_name?> <br>Served By: <?php echo $served_by;?></td>
				      </tr>
				      </thead>
        	  	<tbody>
							</tbody>
						</table>
            <table class="table table-hover table-bordered">
							<thead>
				      <tr>
				        <th>#</th>
				        <th>Description</th>
								<th>AMOUNT DUE</th>
				        <th>AMOUNT PAID</th>
								<th>BALANCE</th>
				      </tr>
				      </thead>
        	  	<tbody>
        	  		<?php echo $payment_result;?>
                <tr>
                  <td colspan=6> NOTE : In case payment is made through cheque the receipt is subject to realisation of the cheque :<br>
							      <b>Payment Mode :</b> <?php echo $payment_method_name?> <br></td>
                </tr>
        	  	</tbody>
        	</table>
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
     </body>
</html>
