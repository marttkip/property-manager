<?php

$company_name = $contacts['company_name'];


$all_leases = $this->leases_model->get_lease_detail($lease_id);
$items = '';
$items_charge ='';
$total_bill = 0;
$service_charge = 0;
$penalty_charge =0;
$water_charge =0;
foreach ($all_leases->result() as $leases_row)
{
	$lease_id = $leases_row->lease_id;
	$tenant_unit_id = $leases_row->tenant_unit_id;
	$property_name = $leases_row->property_name;
	// $units_name = $leases_row->units_name;
	$rental_unit_name = $leases_row->rental_unit_name;
	$tenant_name = $leases_row->tenant_name;
	$lease_start_date = $leases_row->lease_start_date;
	$lease_duration = $leases_row->lease_duration;
	$rent_amount = $leases_row->rent_amount;
	$lease_number = $leases_row->lease_number;
	$arrears_bf = $leases_row->arrears_bf;
	$rent_calculation = $leases_row->rent_calculation;
	$deposit = $leases_row->deposit;
	$deposit_ext = $leases_row->deposit_ext;
	$tenant_phone_number = $leases_row->tenant_phone_number;
	$tenant_email = $leases_row->tenant_email;
	$tenant_national_id = $leases_row->tenant_national_id;
	$lease_status = $leases_row->lease_status;
	$tenant_status = $leases_row->tenant_status;
	$created = $leases_row->created;
	$account_id = $leases_row->account_id;
	$message_prefix = $leases_row->message_prefix;

	$lease_start_date = date('jS M Y',strtotime($lease_start_date));

	// $expiry_date  = date('jS M Y',strtotime($lease_start_date, mktime()) . " + 365 day");
	$expiry_date  = date('jS M Y', strtotime(''.$lease_start_date.'+1 years'));

}

$today = date('jS F Y H:i a',strtotime(date("Y:m:d h:i:s")));

$served_by = $this->accounts_model->get_personnel($this->session->userdata('personnel_id'));



$due_date = date('jS F Y', strtotime($invoice_date. ' + 6 days'));

$tenants_response = $this->accounts_model->get_lease_balance($lease_id);
$total_pardon_amount = $tenants_response['total_pardon'];


$account_invoice_amount = $this->accounts_model->get_invoice_tenants_brought_forward($lease_id,$parent_invoice_date);
$account_paid_amount = $this->accounts_model->get_paid_tenants_brought_forward($lease_id,$parent_invoice_date);
$account_todays_payment =  $this->accounts_model->get_today_tenants_paid_current_forward($lease_id,$parent_invoice_date);
$total_brought_forward_account = $account_invoice_amount - $account_paid_amount - $account_todays_payment - $total_pardon_amount;

$invoice_result = '';
$bf ='';
if($total_brought_forward > 0)
{
	$bf = '
			<tr>
		        <td colspan="3">Balance B /F</td>
		        <td>'.number_format($total_brought_forward).'</td>
		      </tr>
			';
}
$invoice_result = $bf.$result;

$total_bill = $total_rent + $total_deposit + $total_brought_forward_account + $total_service_charge + $total_variance;



$lease_pardons = $this->accounts_model->get_lease_pardons($lease_id);
	$total_pardons = 0;
	if($lease_pardons->num_rows() > 0)
	{

		foreach ($lease_pardons->result() as $key_pardons) {
			# code...
			$document_number = $key_pardons->document_number;
			$pardon_amount = $key_pardons->pardon_amount;
			$pardon_id = $key_pardons->pardon_id;
			$created_by = $key_pardons->created_by;
			$payment_date = $key_pardons->payment_date;
			$pardon_date = $key_pardons->pardon_date;
			$pardon_reason = $key_pardons->pardon_reason;

			$total_pardons = $total_pardons + $pardon_amount;
		}
	}




	$today = date('jS F Y H:i a',strtotime(date("Y:m:d h:i:s")));

	if($lease_payments->num_rows() > 0)
	{
		$y = 0;
		foreach ($lease_payments->result() as $key)
		{
			$payment_id = $key->payment_id;
			$receipt_number = $key->receipt_number;
			$amount_paid = $key->amount_paid;
			$paid_by = $key->paid_by;
			$payment_date = $date_of_payment = $key->payment_date;
			$payment_created = $key->payment_created;
			$payment_created_by = $key->payment_created_by;
			$transaction_code = $key->transaction_code;
			$document_number = $key->document_number;
			$invoice_month_number = $key->month;

			$payment_date = date('jS M Y',strtotime($payment_date));
			$payment_created = date('jS M Y',strtotime($payment_created));
			$y++;
		}
	}




		$invoice_month = date('F', mktime(0,0,0,$invoice_month_number, 1, date('Y')));
		// $month = date('F', mktime(0,0,0,$m, 1, date('Y')));
		$this_month = date('m');
		$this_year = date('Y');

		$total_paid = $this->accounts_model->get_months_last_debt($lease_id,$invoice_month_number);
		$last_bal = $this->accounts_model->get_months_last_arrears($invoice_month_number,$this_year,$lease_id);

		$months_invoice_query = $this->accounts_model->get_months_invoices($lease_id,$invoice_month_number);
		$total_bill = 0;
		$total_rental_bill = 0;
		$total_service_charge = 0;
		// var_dump($months_invoice_query->result()); die();
		foreach ($months_invoice_query->result() as $invoice_key) {

			$invoice_type = $invoice_key->invoice_type;
			if($invoice_type == 1)
			{
				$rental_invoice_amount = $invoice_key->invoice_amount;
				// rental bill
				 $total_rental_bill = $total_rental_bill + $rental_invoice_amount;
			}
			else
			{
				$service_charge_amount = $invoice_key->invoice_amount;
				// service charge
				 $total_service_charge = $total_service_charge + $service_charge_amount;
			}

			$total_bill = $total_bill + ($total_rental_bill +$total_service_charge);
		}
	$served_by = $this->accounts_model->get_personnel($this->session->userdata('personnel_id'));


$invoice_types = $this->accounts_model->get_payments_invoice_types($payment_idd);


// var_dump($invoice_types); die();
$payment_result = '';
if($invoice_types->num_rows() >0)
{
	$x = 0;
	$total_balance = 0;
	foreach ($invoice_types->result() as $types) {
		# code...
		$invoice_type_name = $types->invoice_type_name;
		$invoice_type_id = $types->invoice_type_id;
		$amount_paid = $this->accounts_model->get_payments_detail($payment_idd,$invoice_type_id);
		$paid_amount = $this->accounts_model->get_payments_amount($invoice_type_id);
		// -8000 +2000)
		$invoice_amount = $invoiced_amount + $amount_paid;

		$total_amount = $total_amount + $amount_paid;

		$x++;
		$payment_result .= '
						<tr>
					        <td>'.$x.'</td>
					        <td>'.$invoice_type_name.'</td>
					        <td>'.number_format($amount_paid,2) .'</td>
					      </tr>
						';

	}

	$invoiced_amount = $this->accounts_model->get_total_invoices_before_payment($lease_id,$date_of_payment,$invoice_type_id= NULL);

	$payed_amount = $this->accounts_model->get_total_payments_before_payment_lease($lease_id,$date_of_payment,$invoice_type_id=NULL,$payment_id);
	$items_balance = $invoiced_amount - $payed_amount;


}


?>

<div class="row invoice-info">

  <!-- /.col -->
  <div class="col-sm-4 invoice-col">

    <address>
      <b>NAME : <?php echo strtoupper($tenant_name);?></b><br>
	  <b>TENANT CODE: <?php echo $account_id?> </b> <br>
	  <b>UNIT NO : <?php echo strtoupper($property_name).' '.$rental_unit_name;?></b><br>
	  <b>RECEIPTED ON : <?php echo $payment_date;?></b><br>
	  <b>SERVED BY : <?php echo strtoupper($served_by);?></b><br>
    </address>
  </div>


  <!-- /.col -->
   <div class="col-sm-4 invoice-col">
    <h2><b>Receipt No : </b> <?php echo $document_number;?> </h2>
  </div>

  <div class="col-sm-4 invoice-col">

    <address>
      <strong><?php echo $company_name;?></strong><br>
     	<?php echo $contacts['location'];?>, <?php echo $contacts['building'];?>, <?php echo $contacts['floor'];?><br/>
        Address : P.O. Box <?php echo $contacts['address'];?> <?php echo $contacts['post_code'];?>, <?php echo $contacts['city'];?> <br/>
        Office Line:  <?php echo $contacts['phone'];?>
        <br/> E-mail: <?php echo $contacts['email'];?>.<br/>
    </address>
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->

<!-- Table row -->
<div class="row">
  <div class="col-xs-12 table-responsive">
    <table class="table table-striped">
      <thead>
      <tr>
        <th>#</th>
        <th>Description</th>
        <th>Subtotal</th>
      </tr>
      </thead>
      <tbody>
     	<?php echo $payment_result;?>
      </tbody>
    </table>
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->

<div class="row">
  <!-- accepted payments column -->
  <div class="col-xs-6">
    <!-- <p class="lead">Payment Methods:</p> -->
    <!-- <img src="../../dist/img/credit/visa.png" alt="Visa">
    <img src="../../dist/img/credit/mastercard.png" alt="Mastercard">
    <img src="../../dist/img/credit/american-express.png" alt="American Express">
    <img src="../../dist/img/credit/paypal2.png" alt="Paypal"> -->

    <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
      NOTE : In case payment is made through cheque the receipt is subject to realisation of the cheque :<br>
      <b>Payment Mode :</b> Bank Slip <br/>
      <?php echo $message_prefix;?>
    </p>
  </div>
  <!-- /.col -->
  <div class="col-xs-6">
    <!-- <p class="lead">Amount Due <?php echo $due_date?></p> -->

    <div class="table-responsive">
      <table class="table">
        <tr>
          <th style="width:50%">Total Paid:</th>
          <td>Ksh. <?php echo number_format($total_amount,2);?></td>
        </tr>

        <tr>
          <th>Balance:</th>
          <td><strong>Ksh <?php echo number_format($items_balance - $total_amount - $total_pardons ,2);?></strong> </td>
        </tr>
      </table>
    </div>
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->
