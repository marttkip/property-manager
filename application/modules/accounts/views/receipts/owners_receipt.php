<?php
$all_leases = $this->leases_model->get_lease_detail_owners_receipt($home_owner_unit_id);
// var_dump($all_leases); die();
foreach ($all_leases->result() as $leases_row)
{
	$rental_unit_id = $leases_row->rental_unit_id;
	$home_owner_name = $leases_row->home_owner_name;
	$home_owner_id = $leases_row->home_owner_id;
	$home_owner_email = $leases_row->home_owner_email;
	$home_owner_phone_number = $leases_row->home_owner_phone_number;
	$rental_unit_name = $leases_row->rental_unit_name;
	$property_name = $leases_row->property_name;
	$property_id = $leases_row->property_id;
	$message_prefix = $leases_row->message_prefix;
	
}

$today = date('jS F Y H:i a',strtotime(date("Y:m:d h:i:s")));
	
$served_by = $this->accounts_model->get_personnel($this->session->userdata('personnel_id'));
// get when the lease was active

// get all the invoiced months
// var_dump($lease_invoice); die();

	
if($lease_payments->num_rows() > 0)
{
	$y = 0;
	foreach ($lease_payments->result() as $key) 
	{
		$payment_idd = $key->payment_id;
		$receipt_number = $key->receipt_number;
		$amount_paid = $key->amount_paid;
		$paid_by = $key->paid_by;
		$payment_date = $date_of_payment = $key->payment_date;
		$payment_created = $key->payment_created;
		$payment_created_by = $key->payment_created_by;
		$transaction_code = $key->transaction_code;
		$invoice_month_number = $key->month;
		
		$payment_date = date('jS M Y',strtotime($payment_date));
		$payment_created = date('jS M Y',strtotime($payment_created));
		$y++;
	}
}


$todays_date = date('Y-m-d');
$todays_month = date('m');
$todays_year = date('Y');
$total_payments = $this->accounts_model->get_total_owners_payments_before($rental_unit_id,$todays_year,$todays_month);
$total_invoices = $this->accounts_model->get_total_owners_invoices_before($rental_unit_id,$todays_year,$todays_month);
$current_balance = $total_invoices - $total_payments;

$current_invoice_amount = $this->accounts_model->get_latest_owners_invoice_amount($rental_unit_id,date('Y'),date('m'));

$total_current_invoice = $current_invoice_amount + $current_balance;


$served_by = $this->accounts_model->get_personnel($this->session->userdata('personnel_id'));


$invoice_types = $this->accounts_model->get_owners_property_invoice_types($property_id,0);
// var_dump($invoice_types); die();
$payment_result = '';
$total_balance = 0;
$total_amount_paid =0;
if($invoice_types->num_rows() >0)
{
	$x = 0;
	foreach ($invoice_types->result() as $types) {
		# code...
		$invoice_type_name = $types->invoice_type_name;
		$invoice_type_id = $types->invoice_type_id;
		$amount_paid = $this->accounts_model->get_owners_payments_detail($payment_idd,$invoice_type_id);


		$invoiced_amount = $this->accounts_model->get_owners_invoiced_amount($invoice_type_id);

		$paid_amount = $this->accounts_model->get_owners_payments_amount($invoice_type_id);
		// -8000 +2000)
		$invoice_amount = $invoiced_amount + $amount_paid;


		$invoiced_amount = $this->accounts_model->get_total_owners_invoices_before_payment($rental_unit_id,$date_of_payment,$invoice_type_id);
		// var_dump($rental_unit_id); die();
		$payed_amount = $this->accounts_model->get_total_owners_payments_before_payment($rental_unit_id,$date_of_payment,$invoice_type_id);
		$items_balance = $invoiced_amount - $payed_amount;
		$starting_arreas = $arrears_amount =$this->accounts_model->get_all_owners_arrears($rental_unit_id);

		$balance = $items_balance - $amount_paid + $starting_arreas;

		$owners_response = $this->accounts_model->get_owners_billings($rental_unit_id,$home_owner_id);
		$arrears_amount = $owners_response['total_arrears'];

		$x++;
		// $payment_result .=
		// 					'
		// 					<div class="row ">
		// 			        	<div class="col-xs-4">
		// 			        		'.$invoice_type_name.'
		// 			        	</div>
		// 			        	<div class="col-xs-3">
		// 			        		'.number_format($amount_paid) .'
		// 			        	</div>
		// 			        	<div class="col-xs-3 pull-right">
		// 			        		'.number_format($balance).'
		// 			        	</div>
		// 			        </div>

		// 					';
		$total_amount_paid = $total_amount_paid + $amount_paid;
		$total_balance = $total_balance + $balance;

	}
	$payment_result .=
							'
							<div class="row ">
								<div class="col-xs-3">
					        		'.number_format($total_amount_paid+$total_balance) .'
					        	</div>
					        	<div class="col-xs-3 center-align">
					        		'.number_format($total_amount_paid) .'
					        	</div>
					        	<div class="col-xs-3 pull-right">
					        		'.number_format($total_balance - $starting_arreas).'
					        	</div>
					        </div>

							';
}

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <title><?php echo $contacts['company_name'];?> | Receipt</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" media="all"/>
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css" media="all"/>
        <style type="text/css">
			.receipt_spacing{letter-spacing:0px;}
			.center-align{margin:0 auto; text-align:center;}
			
			.receipt_bottom_border{border-bottom: #888888 medium solid; margin-bottom:1px;}
			.title-img{float:left; padding-left:30px;}
			img.logo{max-height:70px; margin:0 auto;}
			.align-right{margin:0 auto; text-align: right !important;}
			.align-center
			{
				/*padding: 50px;*/
    			text-align: center;
    			font-weight: bolder;
			}
			.table {
			  margin-bottom: 5px;
			  max-width: 100%;
			  width: 100%;
			}
			.row {
  				margin-left: 0px !important;
    			margin-right: 0px !important;
			}
		</style>
    </head>
    <body class="receipt_spacing">
    	<div class="row">
    		<div class="col-xs-10  col-xs-offset-1">
		    	<div class="receipt_bottom_border">
		        	<table class="table table-condensed">
		                <tr>
		                    <th><h4><?php echo $contacts['company_name'];?> RECEIPT</h4></th>
		                    <th class="align-right">
		                        <img src="<?php echo base_url().'assets/logo/'.$contacts['logo'];?>" alt="<?php echo $contacts['company_name'];?>" class="img-responsive logo" style="float:right;"/>
		                    </th>
		                </tr>
		            </table>
		        </div>
		    	
		        
		        <!-- Patient Details -->
		    	<div class="row receipt_bottom_border" style="margin-bottom: 10px; font-size: 12">
		        	<div class="col-xs-6 ">
               			<div class="col-xs-12 ">
			        		<strong>Name</strong>
			        		<?php echo $home_owner_name;?>
			        	</div>
			        	<div class="col-xs-12">
			        		<strong> Phone </strong>
			        		<?php echo $home_owner_phone_number;?>
			        	</div>
			        	<div class="col-xs-12  ">
			        		<strong>Property</strong>
			        		<?php echo $property_name;?>
			        	</div>
			        	<div class="col-xs-12  ">
			        		<strong>Apartment Name</strong>
			        		<?php echo $rental_unit_name;?>
			        	</div>
			        </div>
		            <div class="col-xs-4">

		                 <div class="row ">
	               			<div class="col-xs-12 ">
				        		<strong>Receipt No. </strong>
				        		<?php echo $receipt_number;?>
				        	</div>
				        </div>

		                <div class="row ">
	               			<div class="col-xs-12 ">
				        		<strong>Date</strong>
				        		<?php echo date('jS M Y',strtotime($date_of_payment));?>
				        	</div>
				        </div>
		            </div>
		        </div>
		        
		    	<div class="row receipt_bottom_border">
		        	<div class="col-md-12 center-align">
		            	<strong>RECEIPTED ITEMS</strong>
		            </div>
		        </div>
		        <div class="row">
			        <div class="col-xs-12">
				        <div class="row">
				        	<div class="col-xs-4">
				        		<strong>Bal BF</strong>
				        	</div>
				        	<div class="col-xs-3">
				        		<strong>Paid Amount (KES)</strong>
				        	</div>
				        	<div class="col-xs-3 pull-right">
				        		<strong>Balance (KES)</strong>
				        	</div>
				        </div>
				        <?php echo $payment_result;?>
			        </div>
			   	</div>
		        <div class="row " style="border-top:1px #000 solid; ">
		       		<div class="col-xs-8 pull-left">
		       			<strong style="text-align:left;"> <?php echo $message_prefix;?></strong>
		       		</div>
		       	</div>
		       	<div class="row" >
		       		<div class="col-xs-12 align-center" style="border-top:1px #000 solid;">
		       			<span style="font-size:10px; " > <?php echo $contacts['company_name'];?> | <?php echo $contacts['location'];?>, <?php echo $contacts['building'];?>, <?php echo $contacts['floor'];?>
		                     E-mail: <?php echo $contacts['email'];?>.<br/> Tel : <?php echo $contacts['phone'];?>
		                     P.O. Box <?php echo $contacts['address'];?> <?php echo $contacts['post_code'];?>, <?php echo $contacts['city'];?></span>
		            </div>
		       	</div>
		     </div>
		</div>
		</div>
        
    </body>
    
</html>