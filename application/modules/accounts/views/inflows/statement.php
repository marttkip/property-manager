<!-- search -->
<?php echo $this->load->view('search_inflow_account', '', TRUE);
$property_id = set_value('property_id');
$inflow_service_id = set_value('inflow_service_id');
?>
<!-- end search -->
<div class="row">
    <div class="col-md-12">

        <section class="panel">
            <header class="panel-heading">
                
                <h2 class="panel-title"><?php echo $title;?></h2>
                <a href="<?php echo site_url();?>accounts/inflows" class="btn btn-sm btn-warning pull-right" style="margin-top: -25px; "><i class="fa fa-arrow-left"></i> Back to inflows</a>
                <a href="<?php echo base_url().'accounts/inflows/print_inflow_account/'.$inflow_id.'/'.$date_from.'/'.$date_to;?>" class="btn btn-sm btn-success pull-right"  style="margin-top: -25px;margin-right: 5px;" target="_blank"><i class="fa fa-print"></i> Print</a>
                <button type="button" class="btn btn-sm btn-primary pull-right"  data-toggle="modal" data-target="#record_inflow_account" style="margin-top: -25px; margin-right: 5px;"><i class="fa fa-plus"></i> Record</button>

                	
            </header>
            
            <div class="panel-body">
                <div class="pull-left">
                <!--<a href="<?php echo base_url().'administration/sync_app_creditor_account';?>" class="btn btn-sm btn-info"><i class="fa fa-sign-out"></i> Sync</a>-->

                <?php
                	 $search_creditor = $this->session->userdata('item_inflow_search');

					if(!empty($search_creditor))
					{
						echo '<a href="'.site_url().'accounts/inflows/close_inflow_search/'.$inflow_id.'" class="btn btn-sm btn-danger">Close Search</a>';
					
					}
                ?>
                </div>
                <div class="modal fade" id="record_inflow_account" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">Record Transaction</h4>
                            </div>
                            <div class="modal-body">
                                <?php echo form_open("accounts/inflows/record_inflow_account/".$inflow_id, array("class" => "form-horizontal"));?>
                                <input type="hidden" name="creditor_id" value="<?php echo $creditor_id;?>">
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Transaction date: </label>
                                    
                                    <div class="col-md-8">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </span>
                                            <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="creditor_account_date" placeholder="Transaction date">
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Type *</label>
                                    
                                    <div class="col-md-8">
                                        <select class="form-control" name="transaction_type_id">
                                            <option value="">-- Select type --</option>
                                            <option value="1">Payment</option>
                                           
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Property *</label>
                                    
                                    <div class="col-md-8">
                                        <select class="form-control" name="property_id">
                                            <option value="">-- Select property --</option>
                                            <?php
											if($properties->num_rows() > 0)
											{
												$property = $properties->result();
												
												foreach($property as $res)
												{
													$db_property_id = $res->property_id;
													$property_name = $res->property_name;
													
													if($db_property_id == $property_id)
													{
														echo '<option value="'.$db_property_id.'" selected>'.$property_name.'</option>';
													}
													
													else
													{
														echo '<option value="'.$db_property_id.'">'.$property_name.'</option>';
													}
												}
											}
										?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Transaction Code *</label>
                                    
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" name="transaction_code" placeholder="Code e.g cheque number/MPESA code"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Description *</label>
                                    
                                    <div class="col-md-8">
                                        <select class="form-control" name="inflow_service_id">
                                            <option value="">-- Select Service --</option>
                                            <?php
											if($inflow_services->num_rows() > 0)
											{
												$service = $inflow_services->result();
												
												foreach($service as $res)
												{
													$db_creditor_service_id = $res->inflow_service_id;
													$creditor_service_name = $res->inflow_service_name;
													
													if($db_creditor_service_id == $creditor_service_id)
													{
														echo '<option value="'.$db_creditor_service_id.'" selected>'.$creditor_service_name.'</option>';
													}
													
													else
													{
														echo '<option value="'.$db_creditor_service_id.'">'.$creditor_service_name.'</option>';
													}
												}
											}
										?>
                                        </select>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Amount *</label>
                                    
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" name="inflow_account_amount" placeholder="Amount"/>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-md-8 col-md-offset-4">
                                        <div class="center-align">
                                            <button type="submit" class="btn btn-primary">Save record</button>
                                        </div>
                                    </div>
                                </div>
                                <?php echo form_close();?>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
                
                
			<?php
			$error = $this->session->userdata('error_message');
			$success = $this->session->userdata('success_message');
			
			if(!empty($error))
			{
				echo '<div class="alert alert-danger">'.$error.'</div>';
				$this->session->unset_userdata('error_message');
			}
			
			if(!empty($success))
			{
				echo '<div class="alert alert-success">'.$success.'</div>';
				$this->session->unset_userdata('success_message');
			}
					
			$result =  '';
			
			//if users exist display them
			if ($query->num_rows() > 0)
			{
				//get creditor opening balance
				$credit_opening_balance = 0;
				$debit_opening_balance = 0;
				$result .= 
				'
					<table class="table table-hover table-bordered ">
					  <thead>
						<tr>
						  <th style="text-align:center" rowspan=2>#</th>
						  <th style="text-align:center" rowspan=2>Transaction Date</th>
						  <th rowspan=2>Description</th>
						  <th style="text-align:center" rowspan=2>Property</th>
						  <th style="text-align:center" rowspan=2>Transaction Code</th>
						  <th colspan=2 style="text-align:center;">Amount</th>
						
						</tr>
						<tr>
						  <th style="text-align:center">Debit</th>
						  <th style="text-align:center">Credit</th>
						</tr>
					  </thead>
					  <tbody>
					  	<tr>
							<td>'.$count.'</td>
							<td style="text-align:center"></td>
							<td colspan=3>Creditor Opening Balance</td>
							<td style="text-align:center">'.number_format(0,2).'</td>
							<td style="text-align:center">'.number_format(0,2).'</td>
						</tr> 
				';
				
				$count = 0;
				// var_dump($balance_brought_forward); die();
				if($balance_brought_forward > 0)
				{
					$debit = 0;
					$credit = '';
					$total_debit = 0;
					$total_credit = 0;
					$count++;
				
					$result .= 
					'
						<tr>
							<td>'.$count.'</td>
							<td style="text-align:center"></td>
							<td colspan=3>Balance brought forward</td>
							<td style="text-align:center">'.$debit.'</td>
							<td style="text-align:center">'.$credit.'</td>
						</tr> 
					';
				}
				
				else if($balance_brought_forward < 0)
				{
					$balance_brought_forward *= -1;
					$debit = '';
					$credit = number_format($balance_brought_forward, 2);
					$total_debit = 0;
					$total_credit = $balance_brought_forward;
					$count++;
				
					$result .= 
					'
						<tr>
							<td>'.$count.'</td>
							<td style="text-align:center"></td>
							<td colspan=3>Balance brought forward</td>
							<td style="text-align:center">'.$debit.'</td>
							<td style="text-align:center">'.$credit.'</td>
						</tr> 
					';
				}
				else
				{
					$total_debit = 0;
					$total_credit = 0;
				}
				
				foreach ($query->result() as $row)
				{
					$creditor_account_id = $row->inflow_account_id;
					$creditor_service_id = $row->inflow_service_id;

					$creditor_account_amount = $row->inflow_account_amount;
					$transaction_type_id = $row->transaction_type_id;
					$property_id = $row->property_id;
					$creditor_account_date = $row->inflow_account_date;
					$transaction_code = $row->transaction_code;
					$creditor_account_description = $this->inflows_model->get_inflow_service_name($creditor_service_id);
					$property_name = $this->inflows_model->get_property_name($property_id);
			
					if($transaction_type_id == 1)
					{
						$debit = number_format($creditor_account_amount,2);
						$credit = '';
						$total_debit += $creditor_account_amount;
					}
					else if($transaction_type_id == 2)
					{
						$credit = number_format($creditor_account_amount,2);
						$debit = '';
						$total_credit += $creditor_account_amount;
					}
					
					else
					{
						$debit = '';
						$credit = '';
					}
					
					$count++;
					$result .= 
					'
						<tr>
							<td>'.$count.'</td>
							<td style="text-align:center">'.date('jS M Y', strtotime($creditor_account_date)).'</td>
							<td>'.$creditor_account_description.'</td>
							<td>'.$property_name.'</td>
							<td>'.$transaction_code.'</td>
							<td style="text-align:center">'.$debit.'</td>
							<td style="text-align:center">'.$credit.'</td>
							<td><a href="'.site_url().'accounts/creditors/delete_creditor/'.$creditor_account_id.'/'.$inflow_id.'" class="btn btn-sm btn-danger">Delete</a></td>
						</tr> 
					';
				}
				$result .= 
					'
						<tr>
							<td colspan="5" style="text-align:right">Total</td>
							<td style="text-align:center; font-weight:bold;">'.number_format($total_debit + $debit_opening_balance,2).'</td>
							<td style="text-align:center; font-weight:bold;">'.number_format($total_credit + $credit_opening_balance,2).'</td>
						</tr> 
					';
					$balance =  ($total_debit + $debit_opening_balance) - ($total_credit + $credit_opening_balance);
						$result .= 
						'
							<tr>
								<td colspan="5" style="text-align:right; font-weight:bold;">Balance</td>
								<td colspan="2" style="text-align:center; font-weight:bold;">'.number_format($balance,2).'</td>
							</tr> 
						';
				$result .= 
				'
							  </tbody>
							</table>
				';
			}
			
			else
			{
				$result .= "There are no items";
			}
			
			echo $result;
?>
          	</div>
		</section>
    </div>
</div>