<?php
//creditor data
$inflow_name = set_value('inflow_name');
$contact_name = set_value('contact_name');
$contact_phone = set_value('contact_phone');
$location = set_value('location');
$description = set_value('description');

?>
          <section class="panel">
                <header class="panel-heading">
                    <h2 class="panel-title"><?php echo $title;?></h2>
                </header>
                <div class="panel-body">
                	<div class="row" style="margin-bottom:20px;">
                        <div class="col-lg-12">
                            <a href="<?php echo site_url();?>accounts/infkows" class="btn btn-info pull-right">Back to Inflows</a>
                        </div>
                    </div>
                        
                    <!-- Adding Errors -->
                    <?php
						$success = $this->session->userdata('success_message');
						$error = $this->session->userdata('error_message');
						
						if(!empty($success))
						{
							echo '
								<div class="alert alert-success">'.$success.'</div>
							';
							
							$this->session->unset_userdata('success_message');
						}
						
						if(!empty($error))
						{
							echo '
								<div class="alert alert-danger">'.$error.'</div>
							';
							
							$this->session->unset_userdata('error_message');
						}
			
						$validation_errors = validation_errors();
						
						if(!empty($validation_errors))
						{
							echo '<div class="alert alert-danger"> Oh snap! '.$validation_errors.' </div>';
						}
                    
						$validation_errors = validation_errors();
						
						if(!empty($validation_errors))
						{
							echo '<div class="alert alert-danger"> Oh snap! '.$validation_errors.' </div>';
						}
                    ?>
                    
                    <?php echo form_open($this->uri->uri_string(), array("class" => "form-horizontal", "role" => "form"));?>
<div class="row">
	<div class="col-md-6">
        
        <div class="form-group">
            <label class="col-lg-5 control-label">Inflow Name: </label>
            
            <div class="col-lg-7">
            	<input type="text" class="form-control" name="inflow_name" placeholder="Inflow Name" value="<?php echo $inflow_name;?>">
            </div>
        </div>
        
        
        <div class="form-group">
            <label class="col-lg-5 control-label">Contact Phone: </label>
            
            <div class="col-lg-7">
            	<input type="text" class="form-control" name="contact_phone" placeholder="Phone" value="<?php echo $contact_phone;?>">
            </div>
        </div>
        
       
        
      
        
	</div>
    
    <div class="col-md-6">
      
        <div class="form-group">
            <label class="col-lg-5 control-label">Contact Names: </label>
            
            <div class="col-lg-7">
            	<input type="text" class="form-control" name="contact_name" placeholder="Contact Names" value="<?php echo $contact_name;?>">
            </div>
        </div>
        
         <div class="form-group">
            <label class="col-lg-5 control-label">Location: </label>
            
            <div class="col-lg-7">
            	<input type="text" class="form-control" name="location" placeholder="Location" value="<?php echo $location;?>">
            </div>
        </div>
        
    </div>
</div>

<div class="row" style="margin-top:10px;">
	<div class="col-md-12">
        
        <div class="form-group">
            <label class="col-lg-2 control-label">Description: </label>
            
            <div class="col-lg-9">
            	<textarea class="form-control" name="description" rows="5"><?php echo $description;?></textarea>
            </div>
        </div>
    </div>
</div>

<div class="row" style="margin-top:10px;">
	<div class="col-md-12">
        <div class="form-actions center-align">
            <button class="submit btn btn-primary" type="submit">
                Add Inflow
            </button>
        </div>
    </div>
</div>
                    <?php echo form_close();?>
                </div>
            </section>