 <section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Search</h2>
    </header>
    
    <!-- Widget content -->
    <div class="panel-body">
        <div class="padd">
            <?php
            echo form_open("accounts/inflows/search_inflow_account/".$inflow_id, array("class" => "form-horizontal"));
            ?>
            <div class="row">
            	<div class="col-md-4">
                    <div class="form-group">
                        <label class="col-md-4 control-label">Property: </label>
                        <div class="col-md-8">
                            <select class="form-control" name="property_id">
                                <option value="">-- Select property --</option>
                                <?php
                                if($properties->num_rows() > 0)
								{
									$property = $properties->result();
									
									foreach($property as $res)
									{
										$db_property_id = $res->property_id;
										$property_name = $res->property_name;
										echo '<option value="'.$db_property_id.'">'.$property_name.'</option>';
									}
								}
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="col-md-4 control-label">Date from: </label>
                        
                        <div class="col-md-8">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </span>
                                <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="date_from" placeholder="Date from">
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="col-md-4">
                    
                    <div class="form-group">
                        <label class="col-md-4 control-label">Date to: </label>
                        
                        <div class="col-md-8">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </span>
                                <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="date_to" placeholder="Date to">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="center-align">
                <button type="submit" class="btn btn-info btn-sm">Search</button>
            </div>
            <?php
            echo form_close();
            ?>
        </div>
    </div>
</section>