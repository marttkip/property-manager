<div class="box">
    <div class="box-header with-border">
      <h3 class="box-title">Search Tenants accounts</h3>

      <div class="box-tools pull-right">

      </div>
    </div>
    <div class="box-body">
	<?php
    echo form_open("search-accounts", array("class" => "form-horizontal"));
    ?>
    <div class="row">
        <div class="col-md-6">


            <div class="form-group">
                <label class="col-lg-4 control-label">Tenant Name: </label>

              <div class="col-lg-8">
                    <select id='tenant_id' name='tenant_id' class='form-control select2 '>
                      <option value=''>None - Please Select a tenant</option>
                      <?php echo $tenants_list;?>
                    </select>
              </div>
            </div>

        </div>
        <div class="col-md-6">

             <div class="form-group">
                <div class="col-lg-8 col-lg-offset-4">
                	<div class="center-align">
                   		<button type="submit" class="btn btn-info">Search</button>
    				</div>
                </div>
            </div>
        </div>
    </div>


    <?php
    echo form_close();
    ?>
  </div>
</div>
