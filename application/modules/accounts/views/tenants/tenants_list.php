
<?php
if($rental_unit_id == NULL OR $rental_unit_id == 0)
{
echo $this->load->view('search/tenants_search','', true);

}
else
{

}
 ?>

<?php

		$result = '';
		$items = '';
		if($rental_unit_id == NULL OR $rental_unit_id == 0)
		{
			$items = '';

		}else {
			# code...
			$items = '<th><a >Lease Status</a></th>';
		}
		//if tenants exist display them
		if ($tenants->num_rows() > 0)
		{
			$count = $page;

			$result .=
			'
			<table class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
						<th>#</th>
            <th><a >Tenant Code</a></th>
						<th><a >Name</a></th>
						<th><a >National Id</a></th>
						<th><a >Phone Number</a></th>
						<th><a >Email Address</a></th>
						<th><a >Profile Status</a></th>
						'.$items.'
						<th colspan="5">Actions</th>
					</tr>
				</thead>
				  <tbody>
			';
			foreach ($tenants->result() as $row)
			{
				$tenant_id = $row->tenant_id;
				$tenant_name = $row->tenant_name;
				//create deactivated status display
				if($row->tenant_status == 0)
				{
					$status = '<span class="label label-danger">Deactivated</span>';
					$button = '<a class="btn btn-info" href="'.site_url().'activate-tenant/'.$tenant_id.'" onclick="return confirm(\'Do you want to activate '.$tenant_name.'?\');" title="Activate '.$tenant_name.'"><i class="fa fa-thumbs-up"></i></a>';
				}
				//create activated status display
				else if($row->tenant_status == 1)
				{
					$status = '<span class="label label-success">Active</span>';
					$button = '<a class="btn btn-default" href="'.site_url().'deactivate-tenant/'.$tenant_id.'" onclick="return confirm(\'Do you want to deactivate '.$tenant_name.'?\');" title="Deactivate '.$tenant_name.'"><i class="fa fa-thumbs-down"></i></a>';
				}
				if($rental_unit_id == NULL  OR $rental_unit_id == 0)
				{
					$tenancy_status = '';
					$lease_info = '';
				}
				else
				{
					// check the tenancy status
					$tenancy_query = $this->tenants_model->get_tenancy_details($tenant_id,$rental_unit_id);
					if($tenancy_query->num_rows() > 0)
					{
							foreach ($tenancy_query->result() as $key) {
								# code...
								$tenant_unit_status =$key->tenant_unit_status;
							}

							if($tenant_unit_status == 1)
							{
								$tenancy_status = '<td><span class="label label-success">Active</span></td>';
							}
							else
							{
								$tenancy_status = '<td><span class="label label-warning">Not active</span></td>';
							}

					}
					else
					{
						$tenancy_status = '<td><span class="label label-warning">Not active</span></td>';
					}
					$lease_info = '
						<td>
							<a  class="btn btn-sm btn-primary" id="open_lease_details'.$tenant_id.'" onclick="get_tenant_leases('.$tenant_id.')" ><i class="fa fa-folder"></i> Open Lease Info</a>
							<a  class="btn btn-sm btn-warning" id="close_lease_details'.$tenant_id.'" style="display:none;" onclick="close_tenant_leases('.$tenant_id.')"><i class="fa fa-folder"></i> Close Lease Info</a>
						</td>';
				}

        // <td>
        //   <a  class="btn btn-sm btn-success" id="open_tenant_info'.$tenant_id.'" onclick="get_tenant_info('.$tenant_id.');" ><i class="fa fa-folder"></i> Tenant Details</a>
        //   <a  class="btn btn-sm btn-warning" id="close_tenant_info'.$tenant_id.'" style="display:none;" onclick="close_tenant_info('.$tenant_id.')"><i class="fa fa-folder-open"></i> Close Tenant Info</a>
        // </td>
				$count++;
				$result .=
				'
					<tr>
						<td>'.$count.'</td>
            <td>'.$row->tenant_number.' </td>
						<td>'.$row->tenant_name.' </td>
						<td>'.$row->tenant_national_id.' </td>
						<td>'.$row->tenant_phone_number.' </td>
						<td>'.$row->tenant_email.' </td>
						<td>'.$status.'</td>
						'.$tenancy_status.'
            <td><a href="'.site_url().'tenancy-detail/'.$tenant_id.'/1" class="btn btn-sm btn-primary" title="Tenant Details '.$tenant_name.'">Tenancy Details</a>
					</tr>
				';
        // <td><a href="'.site_url().'tenant-history/'.$tenant_id.'" class="btn btn-sm btn-warning"><i class="fa fa-pencil"></i> view history</a></td>
				$v_data['tenant_id'] = $tenant_id;


			}

			$result .=
			'
						  </tbody>
						</table>
			';
		}

		else
		{
			$result .= "There are no tenants";
		}


			$link = '<a href="'.site_url().'rental-management/rental-units" class="btn btn-sm btn-default " >Back to rental units</a>';




?>
<div class="box box-danger">
    <div class="box-header with-border">
      <h3 class="box-title"><?php echo $title;?></h3>

      <div class="box-tools pull-right">


      </div>
    </div>
    <div class="box-body">


        <div class="col-md-12">
			<div class="table-responsive">

				<?php

				$success = $this->session->userdata('success_message');

				if(!empty($success))
				{
					echo '<div class="alert alert-success"> <strong>Success!</strong> '.$success.' </div>';
					$this->session->unset_userdata('success_message');
				}

				$error = $this->session->userdata('error_message');

				if(!empty($error))
				{
					echo '<div class="alert alert-danger"> <strong>Oh snap!</strong> '.$error.' </div>';
					$this->session->unset_userdata('error_message');
				}

				$search =  $this->session->userdata('all_accounts_search');
				if(!empty($search))
				{
					echo '<a href="'.site_url().'close_search_accounts" class="btn btn-sm btn-warning">Close Search</a>';
				}

				echo $result;

				?>

            </div>
             <div class="widget-foot">

				<?php if(isset($links)){echo $links;}?>

                <div class="clearfix"></div>

            </div>

         </div>

	</div>
</div>
<script type="text/javascript">
	$(function() {
	    $("#tenant_id").customselect();
	});
	$(document).ready(function(){
		$(function() {
			$("#tenant_id").customselect();
		});
	});

	function get_new_tenant(){

		var myTarget2 = document.getElementById("new_tenant");
		var myTarget3 = document.getElementById("new_tenant_allocation");
		var button = document.getElementById("open_new_tenant");
		var button2 = document.getElementById("close_new_tenant");

		myTarget2.style.display = '';
		button.style.display = 'none';
		myTarget3.style.display = 'none';
		button2.style.display = '';
	}
	function close_new_tenant(){

		var myTarget2 = document.getElementById("new_tenant");
		var button = document.getElementById("open_new_tenant");
		var button2 = document.getElementById("close_new_tenant");
		var myTarget3 = document.getElementById("new_tenant_allocation");

		myTarget2.style.display = 'none';
		button.style.display = '';
		myTarget3.style.display = 'none';
		button2.style.display = 'none';
	}


	function assign_new_tenant(){

		var myTarget2 = document.getElementById("new_tenant_allocation");
		var myTarget3 = document.getElementById("new_tenant");
		var button = document.getElementById("assign_new_tenant");
		var button2 = document.getElementById("close_assign_new_tenant");

		myTarget2.style.display = '';
		button.style.display = 'none';
		myTarget3.style.display = 'none';
		button2.style.display = '';
	}
	function close_assign_new_tenant(){

		var myTarget2 = document.getElementById("new_tenant_allocation");
		var button = document.getElementById("assign_new_tenant");
		var myTarget3 = document.getElementById("new_tenant");
		var button2 = document.getElementById("close_assign_new_tenant");

		myTarget2.style.display = 'none';
		button.style.display = '';
		myTarget3.style.display = 'none';
		button2.style.display = 'none';
	}


	// lease details


	function get_tenant_leases(tenant_id){

		var myTarget2 = document.getElementById("lease_details"+tenant_id);
		var myTarget3 = document.getElementById("new_tenant_allocation");
		var myTarget4 = document.getElementById("new_tenant");
		var button = document.getElementById("open_lease_details"+tenant_id);
		var button2 = document.getElementById("close_lease_details"+tenant_id);

		myTarget2.style.display = '';
		button.style.display = 'none';
		myTarget3.style.display = 'none';
		myTarget4.style.display = 'none';
		button2.style.display = '';
	}
	function close_tenant_leases(tenant_id){

		var myTarget2 = document.getElementById("lease_details"+tenant_id);
		var button = document.getElementById("open_lease_details"+tenant_id);
		var button2 = document.getElementById("close_lease_details"+tenant_id);
		var myTarget3 = document.getElementById("new_tenant_allocation");
		var myTarget4 = document.getElementById("new_tenant");

		myTarget2.style.display = 'none';
		button.style.display = '';
		myTarget3.style.display = 'none';
		myTarget4.style.display = 'none';
		button2.style.display = 'none';
	}

	// tenant_info
	function get_tenant_info(tenant_id){

		var myTarget2 = document.getElementById("tenant_info"+tenant_id);
		var myTarget3 = document.getElementById("new_tenant_allocation");
		var myTarget4 = document.getElementById("new_tenant");
		var button = document.getElementById("open_tenant_info"+tenant_id);
		var button2 = document.getElementById("close_tenant_info"+tenant_id);

		myTarget2.style.display = '';
		button.style.display = 'none';
		myTarget3.style.display = 'none';
		myTarget4.style.display = 'none';
		button2.style.display = '';
	}
	function close_tenant_info(tenant_id){

		var myTarget2 = document.getElementById("tenant_info"+tenant_id);
		var button = document.getElementById("open_tenant_info"+tenant_id);
		var button2 = document.getElementById("close_tenant_info"+tenant_id);
		var myTarget3 = document.getElementById("new_tenant_allocation");
		var myTarget4 = document.getElementById("new_tenant");

		myTarget2.style.display = 'none';
		button.style.display = '';
		myTarget3.style.display = 'none';
		myTarget4.style.display = 'none';
		button2.style.display = 'none';
	}

  </script>
