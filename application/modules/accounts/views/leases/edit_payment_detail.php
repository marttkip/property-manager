<?php
  $all_leases = $this->leases_model->get_lease_detail($lease_id);
  	foreach ($all_leases->result() as $leases_row)
  	{
  		$lease_id = $leases_row->lease_id;
  		$tenant_id = $leases_row->tenant_id;
  		$tenant_unit_id = $leases_row->tenant_unit_id;
  		$property_name = $leases_row->property_name;
  		$property_id = $leases_row->property_id;
  		$rental_unit_name = $leases_row->rental_unit_name;
  		$tenant_name = $leases_row->tenant_name;
  		$lease_start_date = $leases_row->lease_start_date;
  		$lease_duration = $leases_row->lease_duration;
  		$rent_amount = $leases_row->rent_amount;
  		$lease_number = $leases_row->lease_number;
  		$arreas_bf = $leases_row->arrears_bf;
  		$rent_calculation = $leases_row->rent_calculation;
  		$deposit = $leases_row->deposit;
  		$deposit_ext = $leases_row->deposit_ext;
  		$tenant_phone_number = $leases_row->tenant_phone_number;
  		$tenant_national_id = $leases_row->tenant_national_id;
  		$lease_status = $leases_row->lease_status;
      $rental_unit_id = $leases_row->rental_unit_id;
  		$tenant_status = $leases_row->tenant_status;
  		$created = $leases_row->created;

  		$lease_start_date = date('jS M Y',strtotime($lease_start_date));

  		// $expiry_date  = date('jS M Y',strtotime($lease_start_date, mktime()) . " + 365 day");
  		$expiry_date  = date('jS M Y', strtotime(''.$lease_start_date.'+1 years'));
  		//create deactivated status display
  		if($lease_status == 0)
  		{
  			$status = '<span class="label label-default"> Deactivated</span>';

  			$button = '';
  			$delete_button = '';
  		}
  		//create activated status display
  		else if($lease_status == 1)
  		{
  			$status = '<span class="label label-success">Active</span>';
  			$button = '<td><a class="btn btn-default" href="'.site_url().'deactivate-rental-unit/'.$lease_id.'" onclick="return confirm(\'Do you want to deactivate '.$lease_number.'?\');" title="Deactivate '.$lease_number.'"><i class="fa fa-thumbs-down"></i></a></td>';
  			$delete_button = '<td><a href="'.site_url().'deactivate-rental-unit/'.$lease_id.'" class="btn btn-sm btn-danger" onclick="return confirm(\'Do you really want to delete '.$lease_number.'?\');" title="Delete '.$lease_number.'"><i class="fa fa-trash"></i></a></td>';

  		}

  		//create deactivated status display
  		if($tenant_status == 0)
  		{
  			$status_tenant = '<span class="label label-default">Deactivated</span>';
  		}
  		//create activated status display
  		else if($tenant_status == 1)
  		{
  			$status_tenant = '<span class="label label-success">Active</span>';
  		}
  	}

    $invoice_type = $this->accounts_model->get_invoice_types();
    $invoice_types = '<option value="">Select a Type</option>';
    foreach($invoice_type->result() as $res)
    {
      $invoice_type_id = $res->invoice_type_id;
      $invoice_type_name = $res->invoice_type_name;

      $invoice_types .= '<option value="'.$invoice_type_id.'">'.$invoice_type_name.'</option>';

    }

    $month = $this->accounts_model->get_months();
    $months_list = '<option value="">Select a Type</option>';
    foreach($month->result() as $res)
    {
      $month_id = $res->month_id;
      $month_name = $res->month_name;
      if($month_id < 10)
      {
        $month_id = '0'.$month_id;
      }
      $month = date('M');

      if($month == $month_name)
      {
        $months_list .= '<option value="'.$month_id.'" selected>'.$month_name.'</option>';
      }
      else {
        $months_list .= '<option value="'.$month_id.'">'.$month_name.'</option>';
      }



    }


    $start = 2015;
    $end_year = 2030;
    $year_list = '<option value="">Select a Type</option>';
    for ($i=$start; $i < $end_year; $i++) {
      // code...
      $year= date('Y');

      if($year == $i)
      {
        $year_list .= '<option value="'.$i.'" selected>'.$i.'</option>';
      }
      else {
        $year_list .= '<option value="'.$i.'">'.$i.'</option>';
      }
    }
    // // var_dump($year_list);die();
    // $item_list = $this->accounts_model->get_all_invoice_items($lease_id);
    // $list = '';

    // if($item_list->num_rows() > 0)
    // {
    //   foreach ($item_list->result() as $key => $value) {
    //     // code...
    //     $invoice_type_name = $value->invoice_type_name;
    //     $total_amount = $value->total_amount;

    //     $list .= '<tr><td><span>'.$invoice_type_name.' :</span></td><td>'.number_format($total_amount,2).' </td></tr>';
    //   }
    // }

    $payment_result = $this->accounts_model->get_payment_details($payment_id);


    if($payment_result->num_rows() > 0)
    {
    	foreach ($payment_result->result() as $key => $value) {
    		# code...
    		$bank_id = $value->bank_id;
    		$payment_method_idd = $value->payment_method_id;
    		$payment_date = $value->payment_date;
    		$receipt_number = $value->receipt_number;
    	}
    }
// var_dump($payment_method_idd); die();
?>
  <input type="hidden" name="payment_method_idd" id="payment_method_idd" value="<?php echo $payment_method_idd?>">
<div class="row">
	<div class="col-md-12">	
		<div class="col-md-8">
		  <div class="box box-success">
		     
		      <div class="box-body">
		        <table class="table table-bordered table-striped table-condensed">
		          <thead>
		            <tr>
		              <th >Name</th>
		              <th >Contract</th>
		              <th >Contract</th>
		              <th >Property</th>
		              <th >Lease Start date</th>
		              <th >Expiry</th>
		            </tr>
		          </thead>
		            <tbody>
		              <tr>
		              	<td><?php echo $tenant_name;?> </td>
		              	<td><?php echo $lease_number;?></td>
		              	<td><?php echo $property_name;?> - <?php echo $rental_unit_name;?></td>
		              	<td> <?php echo $lease_start_date;?></td>
		                <td>
		                <?php echo $lease_end_date;?>
		                </td>
		              </tr>
		          
		            </tbody>
		        </table>
		      </div>
		  </div>
		</div>

		<div class="col-md-4">
		  <div class="box box-success">
		     	<div class="box-body">
		      	<div class="row">
			      	<div class="col-md-12">
			      		<a href="<?php echo site_url().'view-tenant-payments/'.$lease_number.'/'.$lease_id;?>" class="btn btn-info btn-md col-md-12" > 
			      		<i class="fa fa-arrow-left"></i> Back to payments </a>
			      	</div>
			    </div>
			    
		       
		      </div>
		  </div>
		</div>
	</div>
</div>
<div class="row">
  <div class="col-md-12">
  <?php
    $error = $this->session->userdata('error_message');
    $success = $this->session->userdata('success_message');

    if(!empty($error))
    {
      echo '<div class="alert alert-danger">'.$error.'</div>';
      $this->session->unset_userdata('error_message');
    }

    if(!empty($success))
    {
      echo '<div class="alert alert-success">'.$success.'</div>';
      $this->session->unset_userdata('success_message');
    }
   ?>
  </div>
</div>
<div class="row">
	<div class="col-md-12">
	  <div class="box box-success">
	      <div class="box-header with-border">
	        <h3 class="box-title">Edit Payment</h3>

	        <div class="box-tools pull-right">
	          
	        </div>
	      </div>
	      <div class="box-body">

	        <?php echo form_open("accounts/add_payment_item/".$tenant_unit_id."/".$lease_id, array("class" => "form-horizontal"));?>
	            <input type="hidden" name="type_of_account" value="1">
	            <input type="hidden" name="tenant_unit_id" id="tenant_unit_id" value="<?php echo $tenant_unit_id;?>">
	            <input type="hidden" name="lease_id" id="lease_id" value="<?php echo $lease_id?>">
	            <input type="hidden" name="payment_id" id="payment_id" value="<?php echo $payment_id?>">
	            <input type="hidden" name="lease_number" id="lease_number" value="<?php echo $lease_number?>">
	            <input type="hidden" name="redirect_url" value="<?php echo $this->uri->uri_string()?>">
	            <input type="hidden" name="actual_balance" id="actual_balance">
	            <div class="col-md-12">
	              <div class="col-md-3">
	                <div class="form-group" id="payment_method">
	                  <label class="col-md-3 control-label">Type: </label>
	                  <div class="col-md-9">
	                    <select class="form-control select2" name="invoice_type_id" required="required">
	                      <?php echo $invoice_types;?>
	                    </select>
	                    </div>
	                </div>
	              </div>
	              <div class="col-md-2">
	                <div class="form-group">
	                  <label class="col-md-3 control-label">Amount: </label>
	                  <div class="col-md-9">
	                    <input type="number" class="form-control" name="amount" id="paying_amount" placeholder=""  autocomplete="off"  required>
	                  </div>
	                </div>
	              </div>
	              <div class="col-md-3">
	                <div class="form-group" id="payment_method">
	                  <label class="col-md-4 control-label">Month: </label>

	                  <div class="col-md-7">
	                    <select class="form-control select2" name="payment_month"   required>
	                      <?php echo $months_list;?>
	                    </select>
	                    </div>
	                </div>
	              </div>
	              <div class="col-md-2">
	                <div class="form-group" id="payment_method">
	                  <label class="col-md-4 control-label">Year: </label>
	                  <div class="col-md-7">
	                    <select class="form-control select2" name="payment_year"   required>

	                      <?php echo $year_list;?>
	                    </select>
	                    </div>
	                </div>
	              </div>
	              <div class="col-md-2">
	                  <button class="btn btn-info btn-sm" type="submit" onclick="return confirm('Do you want to proceed with this action ?')">Add Item </button>
	              </div>
	            </div>
	        <?php echo form_close();?>
	        <?php echo form_open("accounts/confirm_payment/".$tenant_unit_id."/".$lease_id, array("class" => "form-horizontal","id" => "ConfirmForm"));?>
	        <?php
	        $tenant_where = 'payment_item.invoice_type_id = invoice_type.invoice_type_id AND payment_item.lease_id = '.$lease_id.'  AND payment_item.payment_id = '.$payment_id;
	    		$tenant_table = 'payment_item,invoice_type';
	    		$tenant_order = 'invoice_type_name';
	        $select = 'payment_item.*,invoice_type.*,payment_item.remarks as item_remarks';

	    		$tenant_query = $this->tenants_model->get_tenant_list($tenant_table, $tenant_where, $tenant_order,$select);

	        $result_payment ='<table class="table table-bordered table-striped table-condensed">
	                            <thead>
	                              <tr>
	                              <th ></th>
	                                <th >#</th>

	                                <th >Type</th>
	                                <th >Remarks</th>
	                                <th >Amount</th>
	                                <th colspan="2" >Action</th>
	                              </tr>
	                            </thead>
	                              <tbody>';
	        $total_amount = 0;
	        // var_dump($tenant_query->result()); die();
	        if($tenant_query->num_rows() > 0)
	        {
	          $x = 0;

	          foreach ($tenant_query->result() as $key => $value) {
	            // code...
	            $invoice_type_name = $value->invoice_type_name;
	            $amount_paid = $value->amount_paid;
	            $payment_item_id = $value->payment_item_id;
	            $remarks = $value->item_remarks;
	            $total_amount += $amount_paid;


              $checkbox_data = array(
					                    'name'        => 'tenant_payments_items[]',
					                    'id'          => 'checkbox'.$payment_item_id,
					                    'class'          => 'css-checkbox  lrg ',
					                    'checked'=>'checked',
					                    'value'       => $payment_item_id
					                  );


	            $x++;
	            // $result_payment .= form_open("accounts/update_payments_item/".$payment_item_id."/".$lease_id."/".$tenant_id."/3", array("class" => "form-horizontal"));
	            $result_payment .= '<tr>
            							<td>'.form_checkbox($checkbox_data).'<label for="checkbox'.$payment_item_id.'" name="checkbox79_lbl" class="css-label lrg klaus"></label>'.'</td>
	                                    <td>'.$x.'</td>
	                                    <td>'.$invoice_type_name.'</td>
	                                    <td>'.$remarks.'</td>
	                                    <input type="hidden" class="form-control" name="payment_id" id="payment_id" value = "'.$payment_id.'" required>
	                                    <td><input class="form-control" name="invoice_item_amount'.$payment_item_id.'" value="'.$amount_paid.'"></td>
	                                    
	                                    <td><a href="'.site_url().'delete-payment-item/'.$payment_item_id.'/'.$lease_id.'/'.$tenant_id.'/'.$payment_id.'" type="submit" class="btn btn-sm btn-danger" onclick="return confirm(\'Are you sure you want to remove this entry ? \')" ><i class="fa fa-trash"></i></a></td>
	                                </tr>';
	            // $result_payment .=form_close();
	          }

	          // display button

	          $display = TRUE;
	        }
	        else {
	          $display = FALSE;
	        }

	        $result_payment .='</tbody>
	                        </table>';
	        ?>

	        <?php echo $result_payment;?>
	        <br>
	        <?php
	        if($display)
	        {
	          ?>
	          <div class="row">
	            <div class="col-md-12">
	              
	                <div class="col-md-6">
	                </div>
	                <div class="col-md-6">
	                    <!-- <h2 class="pull-right"> KES. <?php echo number_format($total_amount,2);?></h2> -->
	                    
	                    <input type="hidden" name="type_of_account" value="1">
	                    <input type="hidden" name="tenant_unit_id" id="tenant_unit_id" value="<?php echo $tenant_unit_id;?>">
	                    <input type="hidden" name="tenant_id" id="tenant_id" value="<?php echo $tenant_id;?>">
	                    <input type="hidden" name="lease_id" id="lease_id" value="<?php echo $lease_id?>">
	                    <input type="hidden" name="lease_number" id="lease_number" value="<?php echo $lease_number?>">
	                    <input type="hidden" name="redirect_url" value="<?php echo $this->uri->uri_string()?>">
	                    <input type="hidden" name="rental_unit_id" id="lease_id" value="<?php echo $rental_unit_id?>">
	                  <div class="form-group">
	                    <label class="col-md-4 control-label">Payment Date: </label>

	                    <div class="col-md-7">
	                       <div class="input-group">
	                            <span class="input-group-addon">
	                                <i class="fa fa-calendar"></i>
	                            </span>
	                            <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="payment_date" placeholder="Payment Date" id="datepicker" value="<?php echo $payment_date?>" required>
	                        </div>
	                    </div>
	                  </div>
	                  <div class="form-group" id="payment_method">
	                    <label class="col-md-4 control-label">Payment Method: </label>

	                    <div class="col-md-7">
	                      <select class="form-control" name="payment_method" onchange="check_payment_type(this.value)" required>
	                        <!-- <option value="">Select a payment method</option> -->
	                                              <?php
	                          $method_rs = $this->accounts_model->get_payment_methods();

	                          foreach($method_rs->result() as $res)
	                          {
	                            $payment_method_id = $res->payment_method_id;
	                            $payment_method = $res->payment_method;

	                            if($payment_method_idd == $payment_method_id)
	                            {
	                            	 echo '<option value="'.$payment_method_id.'" selected="selected">'.$payment_method.'</option>';
	                            }
	                            else
	                            {
	                            	 echo '<option value="'.$payment_method_id.'">'.$payment_method.'</option>';
	                            }

	                           

	                          }

	                        ?>
	                      </select>
	                      </div>
	                  </div>
	                  <div id="mpesa_div" class="form-group" style="display:none;" >
	                    <label class="col-md-4 control-label"> Mpesa TX Code: </label>

	                    <div class="col-md-7">
	                      <input type="text" class="form-control" name="mpesa_code" placeholder="" value="<?php echo $receipt_number?>" >
	                    </div>
	                  </div>
	                  <div id="cheque_div" class="form-group" style="display:none;" >
	                    <div class="form-group" >
	                      <label class="col-md-4 control-label"> Bank: </label>

	                      <div class="col-md-7">
	                        <select class="form-control " name="bank_id" >
	                          <option value="">Select a bank</option>
	                                                <?php
	                            $bank_rs = $this->accounts_model->get_bank_accounts();

	                            foreach($bank_rs->result() as $res)
	                            {
	                              $id = $res->id;
	                              $name = $res->name;
	                              if($bank_id == $id)
	                              {
	                              	 echo '<option value="'.$id.'" selected="selected">'.$name.'</option>';
	                              }
	                              else
	                              {
	                              	 echo '<option value="'.$id.'">'.$name.'</option>';
	                              }
	                             

	                            }

	                          ?>
	                        </select>
	                      </div>
	                    </div>
	                    <div class="form-group" >
	                      <label class="col-md-4 control-label"> Reference Number: </label>
	                      <div class="col-md-7">
	                        <input type="text" class="form-control" name="reference_number" value="<?php echo $receipt_number?>" >
	                      </div>
	                    </div>
	                  </div>
	                  <div class="form-group">
	                    <label class="col-md-4 control-label">Total Amount: </label>

	                    <div class="col-md-7">
	                      <input type="number" class="form-control" name="total_amount" placeholder=""  autocomplete="off" value="<?php echo $total_amount;?>" readonly>
	                    </div>
	                  </div>
	                  <div class="form-group">
	                    <label class="col-md-4 control-label">Paid in By: </label>

	                    <div class="col-md-7">
	                      <input type="text" class="form-control" name="paid_by" placeholder="<?php echo $tenant_name;?>" value="<?php echo $tenant_name;?>" autocomplete="off" >
	                    </div>
	                  </div>
	                  <div class="col-md-12">
	                      <div class="text-center">
	                        <button class="btn btn-info btn-sm " type="submit" id="confirmBTN" onclick="return confirm('Are you sure you want to complete this transaction ? ')">Complete Payment </button>
	                      </div>
	                  </div>
	                </div>
	             
	            </div>
	          </div>
	          <?php
	        }
	        ?>
	         <?php echo form_close();?>

	      </div>
	  </div>
	</div>
</div>


<script>

 $(document).ready(function () {

    $("#ConfirmForm").submit(function (e) {

        //stop submitting the form to see the disabled button effect
        // e.preventDefault();

        //disable the submit button
        $("#confirmBTN").attr("disabled", true);

        //disable a normal button
        // $("#btnTest").attr("disabled", true);

        return true;

    });
});

$(function() {
     var payment_method_idd = document.getElementById("payment_method_idd").value;
	    // alert(payment_method_idd);
	  check_payment_type(payment_method_idd);

});
function display_payment_model()
{
  $('#modal-defaults').modal('show');
  $('#datepicker').datepicker({
    autoclose: true,
    format: 'yyyy-mm-dd',
  })
   
}

function check_payment_type(payment_type_id){

  var myTarget1 = document.getElementById("cheque_div");

  var myTarget2 = document.getElementById("mpesa_div");

  var myTarget3 = document.getElementById("insuarance_div");

  if(payment_type_id == 1)
  {
    // this is a cash

    myTarget1.style.display = 'none';
    myTarget2.style.display = 'none';
    myTarget3.style.display = 'none';
  }
  else if(payment_type_id == 2 || payment_type_id == 3 )
  {
    // cheque
    myTarget1.style.display = 'block';
    myTarget2.style.display = 'none';
    myTarget3.style.display = 'none';
  }

  else
  {
    myTarget1.style.display = 'none';
    myTarget2.style.display = 'none';
    myTarget3.style.display = 'none';
  }

}

function get_invoice_details(lease_invoice_id)
{

  var url = "<?php echo site_url();?>accounts/get_lease_invoice_items/"+lease_invoice_id;

  $.get( url, function( data )
  {
      $( "#invoice_items_list" ).html( data );
      // $(".custom-select").customselect();
  });
}

function get_invoice_item_balance(invoice_type_id)
{

   // var lease_invoice_id = document.getElementById("invoice_items_list").value;

   var e = document.getElementById("lease_invoice_id");
   var lease_invoice_id = e.options[e.selectedIndex].value;
   var url = "<?php echo site_url();?>accounts/get_lease_invoice_balance/"+lease_invoice_id+"/"+invoice_type_id;
   // alert(url);
    // $.get( url, function( data )
    // {
        // $( "#invoice_items_list" ).html( data );
        // alert(data);
        // document.getElementById("paying_amount").value = "data";

        // var item = ""+data+"";
        // $("#paying_amount").val(item);
        // document.getElementById("actual_balance").value = ""+data+"";

        // $(".custom-select").select2();
    // });


    $.ajax({
      type:'POST',
      url: url,
      data:{lease_invoice_id:lease_invoice_id},
      dataType: 'text',
      success:function(data){
         var data = jQuery.parseJSON(data);

        document.getElementById("actual_balance").value = data.balance;
        document.getElementById("paying_amount").value = data.balance;



      },
      error: function(xhr, status, error) {
      alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

      }
      });

}
</script>