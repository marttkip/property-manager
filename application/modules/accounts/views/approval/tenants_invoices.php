<?php

$result = '';
        
//if users exist display them
if ($query->num_rows() > 0)
{
    $count = $page;
    
    $result .= 
    '
    <table class="table table-bordered table-striped table-condensed">
        <thead>
            <tr>
                <th>#</th>
                <th>Unit name</th>
                <th>Tenant Name</th>
                <th>Invoice Month</th>
                <th>Bal B/F</th>
                <th>Current Dues</th>
                <th>Total Dues</th>
                <th colspan="2" style="center-align">Actions</th>
            </tr>
        </thead>
          <tbody>
          
    ';
    
    
    foreach ($query->result() as $leases_row)
    {
        $rental_unit_name = $leases_row->rental_unit_name;
        $lease_id = $leases_row->lease_id;
        $property_idd = $leases_row->property_id;
        $tenant_name = $leases_row->tenant_name;
        $tenant_email = $leases_row->tenant_email;
        $tenant_id = $leases_row->tenant_id;
        $property_name = $leases_row->property_name;
        $invoice_month = $leases_row->invoice_month;
        $invoice_year = $leases_row->invoice_year;
        $tenant_phone_number = $leases_row->tenant_phone_number;

        $current_date = date('Y-m-d');
		$date = explode('-', $current_date);
		$month = $date[1];
		$year = $date[0];

		$current_arrears = $this->accounts_model->get_current_arrears($lease_id,$month,$year,3);  

        $invoice_month  = date('M', strtotime($current_date));


        // get the invoices for this month in an editable formart

        // end of getting the months invoice 
                
        $count++;
       	$result .= 
				'
					<tr>
						<td>'.$count.'</td>
						<td>'.$rental_unit_name.' </td>
						<td>'.$tenant_name.' </td>
						<td>'.$invoice_month.'-'.$invoice_year.'</td>
						<td>'.number_format($current_arrears).'</td>
						<td>'.number_format($current_arrears).'</td>
						<td>'.number_format($current_arrears).'</td>
						<td>
							<a  class="btn btn-sm btn-primary" id="open_lease'.$lease_id.'" onclick="get_lease_details('.$lease_id.');" ><i class="fa fa-folder"></i> Invoice Details</a>
							<a  class="btn btn-sm btn-info" id="close_lease'.$lease_id.'" style="display:none;" onclick="close_lease_details('.$lease_id.')"><i class="fa fa-folder-open"></i> Close Invoice Info</a>
						</td>
						<td>
							<a href="'.site_url().'edit-invoice/'.$lease_id.'/'.$month.'/'.$year.'" class="btn btn-sm btn-warning" target="_blank"><i class="fa fa-pencil"></i> Edit Invoice</a>
						</td>
					</tr> 
				';

    }
    			
    $result .= 
    '
                  </tbody>
                </table>
    ';
}

else
{
    $result .= "There are no items for correction";
}
?>  


<section class="panel">
       
        <header class="panel-heading">
            <h4 class="page-title"><?php echo $title;?></h4>
        </header>  
        <!-- Widget content -->
         <div class="panel-body">
          <div class="padd">
            
            <div class="row">
                <div class="col-md-12">
            		<?php
            		$error = $this->session->userdata('error_message');
            		$success = $this->session->userdata('success_message');
            		
            		if(!empty($error))
            		{
            			echo '<div class="alert alert-danger">'.$error.'</div>';
            			$this->session->unset_userdata('error_message');
            		}
            		
            		if(!empty($success))
            		{
            			echo '<div class="alert alert-success">'.$success.'</div>';
            			$this->session->unset_userdata('success_message');
            		}
            		?>

                    <!-- put the table here -->

                    <div class="table-responsive">
                
                        <?php echo $result;?>
                
                    </div>
                    <!-- get the table here -->
               </div>

            </div>
		</div>
		<div class="panel-footer">
        	<?php if(isset($links)){echo $links;}?>
        </div>
      </div>
</section>
	<script type="text/javascript">
		function get_lease_details(lease_id){

			var myTarget2 = document.getElementById("lease_details"+lease_id);
			var button = document.getElementById("open_lease"+lease_id);
			var button2 = document.getElementById("close_lease"+lease_id);

			myTarget2.style.display = '';
			button.style.display = 'none';
			button2.style.display = '';
		}
		function close_lease_details(lease_id){

			var myTarget2 = document.getElementById("lease_details"+lease_id);
			var button = document.getElementById("open_lease"+lease_id);
			var button2 = document.getElementById("close_lease"+lease_id);

			myTarget2.style.display = 'none';
			button.style.display = '';
			button2.style.display = 'none';
		}

  </script>