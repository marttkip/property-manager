<?php

$result = '';
        
//if users exist display them
if ($query->num_rows() > 0)
{
    $count = $page;
    
    $result .= 
    '
     <table class="table table-bordered table-striped table-condensed">
        <thead>
            <tr>
                <th>#</th>
                <th><a>Property</a></th>
                <th><a>Unit name</a></th>
                <th><a>Paid Amount</a></th>
                <th><a>Payment Date</a></th>
                <th><a>Account</a></th>
                <th><a>By</a></th>
                <th colspan="2">Actions</th>
            </tr>
        </thead>
          <tbody>
          
    ';
    
    
     foreach ($query->result() as $leases_row)
    {
        $payment_item_id = $leases_row->payment_item_id;
        $amount_paid = $leases_row->amount_paid;
         $property_idd = $leases_row->property_id;
        $rental_unit_name = $leases_row->rental_unit_name;
        $payment_date = $leases_row->payment_date;
         $invoice_type_idd = $leases_row->invoice_type_id;
        $type_of_account = 1;
        $paid_by = $leases_row->paid_by;
        $paid_amount = $leases_row->paid_amount;
        $payment_id = $leases_row->payment_id;

        $properties = $this->property_model->get_active_property();
        $rs8 = $properties->result();
        $property_item = '<select class="form-control" name="property_id">';
        foreach ($rs8 as $property_rs) :
            $property_id = $property_rs->property_id;
            $property_name = $property_rs->property_name;
            $property_location = $property_rs->property_location;

            if($property_idd == $property_id)
            {
                 $property_item .="<option value='".$property_id."' selected>".$property_name."</option>";
            }
            else
            {
                 $property_item .="<option value='".$property_id."'>".$property_name."</option>";
            }
           

        endforeach;
        $property_item .= '</select>';



        $invoice_type_order = 'invoice_type_id';
        $invoice_type_table = 'invoice_type';
        $invoice_type_where = 'invoice_type_id > 0';

        $invoice_type_query = $this->property_model->get_active_list($invoice_type_table, $invoice_type_where, $invoice_type_order);
        $rs9 = $invoice_type_query->result();

        $invoice_type_list = '<select class="form-control" name="payment_code">';
        foreach ($rs9 as $invoice_type_rs) :
            $invoice_type_id = $invoice_type_rs->invoice_type_id;
            $invoice_type_name = $invoice_type_rs->invoice_type_name;
            $invoice_type_code = $invoice_type_rs->invoice_type_code;

            if($invoice_type_idd == $invoice_type_id)
            {
                 $invoice_type_list .="<option value='".$invoice_type_id."' selected>".$invoice_type_name."</option>";
            }
            else
            {
                 $invoice_type_list .="<option value='".$invoice_type_id."'>".$invoice_type_name."</option>";
            }

        endforeach;

        $invoice_type_list .= '<select>';


        if($type_of_account == 1)
        {

            $account_type = '
                                <div class="radio">
                                    <label>
                                        <input id="optionsRadios1" type="radio" checked value="1" name="type_of_account">
                                        Tenant
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input id="optionsRadios2" type="radio" value="0" name="type_of_account">
                                        Owner
                                    </label>
                                </div>
                            ';

            
        }
        else
        {
             $account_type = '
                                <div class="radio">
                                    <label>
                                        <input id="optionsRadios1" type="radio"  value="1" name="type_of_account">
                                        Tenant
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input id="optionsRadios2" checked type="radio" value="0" name="type_of_account">
                                        Owner
                                    </label>
                                </div>
                            ';
        }
                
        $count++;
        $result .= 

        '
            '.form_open("accounts/update_payment/".$payment_id."/".$payment_item_id, array("class" => "form-horizontal", "role" => "form")).'
            <tr>
                <td>'.$count.'</td>
                <td>'.$property_item.'</td>
                <td><input type="text" class="form-control" name="rental_unit_name" value="'.$rental_unit_name.'"></td>
                <td><input type="text" class="form-control" name="paid_amount" value="'.$paid_amount.'"></td>
                <td>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </span>
                        <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="payment_date" value="'.$payment_date.'" placeholder="Payment date">
                    </div>
                </td>
                <td>'.$invoice_type_list.'</td>
                <td>'.$account_type.'</td>
                <td><input type="text" class="form-control" name="paid_by" value="'.$paid_by.'"></td>
                <td><button class="btn btn-sm btn-warning fa fa-check" ></button></td>
                <td>  
                    <a class="btn btn-sm btn-danger fa fa-trash " href="'.site_url().'accounts/delete_payment_item/'.$payment_item_id.'"></a>
                </td>    
            </tr> 
            '.form_close().'
        ';
    }
    			
    $result .= 
    '
                  </tbody>
                </table>
    ';
}

else
{
    $result .= "There are no items for correction";
}
?>  


<section class="panel">
       
        <header class="panel-heading">
            <h4 class="page-title"><?php echo $title;?></h4>
        </header>  
        <!-- Widget content -->
         <div class="panel-body">
          <div class="padd">
            
            <div class="row">
                <div class="col-md-12">
            		<?php
            		$error = $this->session->userdata('error_message');
            		$success = $this->session->userdata('success_message');
            		
            		if(!empty($error))
            		{
            			echo '<div class="alert alert-danger">'.$error.'</div>';
            			$this->session->unset_userdata('error_message');
            		}
            		
            		if(!empty($success))
            		{
            			echo '<div class="alert alert-success">'.$success.'</div>';
            			$this->session->unset_userdata('success_message');
            		}
            		?>

                    <!-- put the table here -->
                      <?php echo $result;?>
                    <!-- get the table here -->
               </div>

            </div>
		</div>
		<div class="panel-footer">
        	<?php if(isset($links)){echo $links;}?>
        </div>
      </div>
</section>
	<script type="text/javascript">
		function get_lease_details(lease_id){

			var myTarget2 = document.getElementById("lease_details"+lease_id);
			var button = document.getElementById("open_lease"+lease_id);
			var button2 = document.getElementById("close_lease"+lease_id);

			myTarget2.style.display = '';
			button.style.display = 'none';
			button2.style.display = '';
		}
		function close_lease_details(lease_id){

			var myTarget2 = document.getElementById("lease_details"+lease_id);
			var button = document.getElementById("open_lease"+lease_id);
			var button2 = document.getElementById("close_lease"+lease_id);

			myTarget2.style.display = 'none';
			button.style.display = '';
			button2.style.display = 'none';
		}

  </script>