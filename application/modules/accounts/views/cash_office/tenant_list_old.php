<?php echo $this->load->view('search/tenants_search','', true); ?>
<?php

$result = '';

//if users exist display them
if ($query->num_rows() > 0)
{
	$count = $page;

	$result .=
	'
	<table class="table table-bordered table-striped table-condensed">
		<thead>
			<tr>
				<th>#</th>
				<th><a>Property Name</a></th>
				<th><a>Unit Name</a></th>
				<th><a>Contract no</a></th>
				<th><a>Tenant Name</a></th>
				<th><a>Phone Number</a></th>
				<th><a>Email</a></th>
				<th><a>Acc Bal</a></th>
				<th colspan="5">Actions</th>
			</tr>
		</thead>
		  <tbody>

	';


	foreach ($query->result() as $leases_row)
	{
		$lease_id = $leases_row->lease_id;
		$tenant_unit_id = $leases_row->tenant_unit_id;
		$rental_unit_id = $leases_row->unit_id;
		$rental_unit_name = $leases_row->rental_unit_name;
		$tenant_name = $leases_row->tenant_name;
		$tenant_email = $leases_row->tenant_email;
		$tenant_phone_number = $leases_row->tenant_phone_number;
		$lease_start_date = $leases_row->lease_start_date;
		$lease_duration = $leases_row->lease_duration;
		$rent_amount = $leases_row->rent_amount;
		$lease_number = $leases_row->lease_number;
		$arreas_bf = $leases_row->arrears_bf;
		$rent_calculation = $leases_row->rent_calculation;
		$property_name = $leases_row->property_name;
		$deposit = $leases_row->deposit;
		$deposit_ext = $leases_row->deposit_ext;
		$lease_status = $leases_row->lease_status;
		$points = 0;//$leases_row->points;
		$created = $leases_row->created;
		$account_id = $leases_row->account_id;

// var_dump($rental_unit_id); die();


		// $expiry_date  = date('jS M Y',strtotime($lease_start_date, mktime()) . " + 365 day");
		$expiry_date  = date('jS M Y', strtotime(''.$lease_start_date.'+1 years'));


		//create deactivated status display
		if($lease_status == 0)
		{
			$status = '<span class="label label-default"> Deactivated</span>';

			$button = '';
			$delete_button = '';
		}
		//create activated status display
		else if($lease_status == 1)
		{
			$status = '<span class="label label-success">Active</span>';
			$button = '<td><a class="btn btn-default" href="'.site_url().'deactivate-rental-unit/'.$lease_id.'" onclick="return confirm(\'Do you want to deactivate '.$lease_number.'?\');" title="Deactivate '.$lease_number.'"><i class="fa fa-thumbs-down"></i></a></td>';
			$delete_button = '<td><a href="'.site_url().'deactivate-rental-unit/'.$lease_id.'" class="btn btn-sm btn-danger" onclick="return confirm(\'Do you really want to delete '.$lease_number.'?\');" title="Delete '.$lease_number.'"><i class="fa fa-trash"></i></a></td>';

		}

			$lease_start_date = date('jS M Y',strtotime($lease_start_date));

	     	$this_month = date('m');
	     	$amount_paid = 0;


				$todays_date = date('Y-m-d');
				$todays_month = date('m');
				$todays_year = date('Y');
				$parent_invoice_date = date('Y-m-d', strtotime($todays_year.'-'.$todays_month.'-01'));
				$previous_invoice_date = date('Y-m-d', strtotime($parent_invoice_date.'-1 month'));


				$total_arrears = $this->accounts_model->get_lease_balances($lease_id,$parent_invoice_date,1,$todays_month,$todays_year,$rental_unit_id,$parent_invoice_date,$previous_invoice_date);
				// $total_arrears = $tenants_response['balance'];

				// $current_invoice_amount = $this->accounts_model->get_latest_invoice_amount($lease_id,date('Y'),date('m'));
				// $total_current_invoice = $current_invoice_amount + $current_balance - $total_paid_amount;

				$count++;
				//check if email for the current_month has been sent for that rental unit
				// $all_sms_notifications_sent = $this->accounts_model->check_if_sms_sent($rental_unit_id,$tenant_phone_number,$todays_year,$todays_month,1);
				// $all_email_notifications_sent = $this->accounts_model->check_if_email_sent($rental_unit_id,$tenant_email,$todays_year,$todays_month,1);


				$send_notification_button = '<td><a class="btn btn-sm btn-default" href="'.site_url().'send-arrears/'.$lease_id.'" onclick="return confirm(\'Do you want to send an sms of the arrears ?\')"> Send Message</a></td>';


				$result .=
				'
					<tr>
						<td>'.$count.'</td>
						<td>'.$property_name.'</td>
						<td>'.$rental_unit_name.'</td>
						<td>'.$account_id.'</td>
						<td>'.$tenant_name.'</td>
						<td>'.$tenant_phone_number.'</td>
						<td>'.$tenant_email.'</td>
						<td>'.number_format($total_arrears ,2).'</td>
						<td><a class="btn btn-sm btn-warning" href="'.site_url().'accounts/payments/'.$tenant_unit_id.'/'.$lease_id.'" > <i class="fa fa-folder"></i> Account Detail</a></td>

					</tr>
				';
		$v_data['lease_id'] = $lease_id;
		$v_data['amount_paid'] = $amount_paid;
		$v_data['current_balance'] = $current_balance;
		$v_data['balance_bf'] = $current_balance;

	}

	$result .=
	'
				  </tbody>
				</table>
	';
}

else
{
	$result .= "There are no tickets";
}


$accounts_search_title = $this->session->userdata('accounts_search_title');
?>
<!-- href="<?php echo site_url();?>accounts/update_invoices" -->

<div class="box">
    <div class="box-header with-border">
      <h3 class="box-title"><?php echo $title;?></h3>

      <div class="box-tools pull-right">
      	 <a  href="<?php echo site_url();?>accounts/update_invoices"   class="btn btn-sm btn-success" onclick="return confirm('Do you want to update invoices for <?php echo $accounts_search_title;?> ?')"> <i class="fa fa-recycle"></i> Update invoices </a>
		<a href="<?php echo site_url();?>accounts/send_notifications"  class="btn btn-sm btn-default" onclick="return confirm('Do you want to send notifications for <?php echo $accounts_search_title;?> ?')"> <i class="fa fa-outbox"></i> Send Bulk Notifications</a>
      </div>
    </div>
    <div class="box-body">

        	<?php
            $success = $this->session->userdata('success_message');

			if(!empty($success))
			{
				echo '<div class="alert alert-success"> <strong>Success!</strong> '.$success.' </div>';
				$this->session->unset_userdata('success_message');
			}

			$error = $this->session->userdata('error_message');

			if(!empty($error))
			{
				echo '<div class="alert alert-danger"> <strong>Oh snap!</strong> '.$error.' </div>';
				$this->session->unset_userdata('error_message');
			}
			$search =  $this->session->userdata('all_accounts_search');
			if(!empty($search))
			{
				echo '<a href="'.site_url().'close_search_accounts" class="btn btn-sm btn-warning">Close Search</a>';
			}

			?>
			<div class="table-responsive">

				<?php echo $result;?>

            </div>
             <div class="panel-footer">
	        	<?php if(isset($links)){echo $links;}?>
	        </div>
		</div>

	</div>

	<script type="text/javascript">
		function get_lease_details(lease_id){

			var myTarget2 = document.getElementById("lease_details"+lease_id);
			var button = document.getElementById("open_lease"+lease_id);
			var button2 = document.getElementById("close_lease"+lease_id);

			myTarget2.style.display = '';
			button.style.display = 'none';
			button2.style.display = '';
		}
		function close_lease_details(lease_id){

			var myTarget2 = document.getElementById("lease_details"+lease_id);
			var button = document.getElementById("open_lease"+lease_id);
			var button2 = document.getElementById("close_lease"+lease_id);

			myTarget2.style.display = 'none';
			button.style.display = '';
			button2.style.display = 'none';
		}

  </script>
