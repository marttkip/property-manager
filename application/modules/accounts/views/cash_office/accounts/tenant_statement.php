<div class="row">
	<div class="col-md-12">
		<div class="box">
		    <div class="box-header with-border">
		      <h3 class="box-title">Tenant Statement</h3>

		      <div class="box-tools pull-right">
		      	 	<a href="<?php echo site_url();?>print-tenant-statement/<?php echo $lease_id;?>/<?php echo date('Y');?>" target="_blank" class="btn btn-sm btn-default" > <i class="fa fa-print"></i> Tenant's Statement</a>
		      </div>
		    </div>
		    <div class="box-body">
				<table class="table table-hover table-bordered col-md-12">
					<thead>
						<tr>
							<th>Date</th>
							<th>Description</th>
							<th>Invoices</th>
							<th>Payments</th>
							<th>Arrears</th>
							<th></th>
						</tr>
					</thead>
				  	<tbody>
				  		<?php echo $tenant_response['result'];?>
				  	</tbody>
				</table>
			</div>
		</div>
	</div>
</div>