
<div class="row">
<div class="col-md-12">
	<div class="box">
	    <div class="box-header with-border">
	      <h3 class="box-title"> Account Invoices for Year <?php echo date('Y');?></h3>
	      <div class="box-tools pull-right">
	      	<!-- <button type="button" class="btn btn-sm btn-default" data-toggle="modal" data-target="#modal-pardons">
                Record Pardon
         	</button> -->
	      </div>
	    </div>
	    <div class="box-body">
			<table class="table table-hover table-bordered col-md-12">
				<thead>
					<tr>
						<th>#</th>
						<th>Invoice number</th>
						<th>Invoice Date</th>
						<th>Invoiced Amount </th>
						<th>Status</th>
						<th colspan="4">Actions</th>
					</tr>
				</thead>
				<tbody>
					<?php
					if($lease_invoices->num_rows() > 0)
					{
						$y = 0;
						foreach ($lease_invoices->result() as $key_pardons) {
							# code...
							$document_number = $key_pardons->document_number;
							$invoice_amount = $key_pardons->total_invoice;
							$invoice_date = $date_invoiced = $key_pardons->invoice_date;
							$invoice_year = $key_pardons->invoice_year;
							$invoice_month = $key_pardons->invoice_month;
							$lease_invoice_id = $key_pardons->lease_invoice_id;
							$sent_status = $key_pardons->sent_status;

							if($sent_status == 1)
							{
								$status = '<span class="label label-success">Sent</span>';
								$button = '';
							}
							else
							{
								$status = '<span class="label label-warning">Not sent</span>';
								// $button = '<td>
			     //                            	<a href="'.site_url().'invoice/'.$lease_id.'/'.$invoice_month.'/'.$invoice_year.'/'.$document_number.'" target="_blank" class="btn btn-sm btn-success" ><i class="fa fa-print"></i> Edit</a>
			     //                            </td>
			     //                            <td>
			     //                            	<a class="btn btn-sm btn-danger" onclick="delete_invoice('.$lease_id.','.$lease_invoice_id.')"><i class="fa fa-trash"></i></a>
			     //                            </td>';
								$button = '';
							}

							$invoice_date = date('jS M Y',strtotime($invoice_date));
							$y++;
							?>
							<tr>
								<td><?php echo $y?></td>
								<td><?php echo $document_number?></td>
								<td><?php echo $invoice_date;?></td>
								<td><?php echo number_format($invoice_amount,2);?></td>
								<td><?php echo $status?></td>
                                <td>
                                	<a href="<?php echo site_url().'invoice/'.$lease_id.'/'.$invoice_month.'/'.$invoice_year.'/'.$lease_invoice_id.''?>" target="_blank" class="btn btn-sm btn-success"><i class="fa fa-print"></i> Print</a>
                                </td>
                                <td>
                                	<a href="<?php echo site_url().'send-invoice/'.$lease_id.'/'.$invoice_month.'/'.$invoice_year.'/'.$lease_invoice_id.''?>" class="btn btn-sm btn-warning"><i class="fa fa-print"></i> Send</a>
                                </td>
                                <?php echo $button?>
							</tr>
							<?php

						}
					}
					?>

				</tbody>
			</table>
		</div>
	</div>
</div>
</div>
