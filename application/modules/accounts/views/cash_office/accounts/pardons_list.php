
<div class="row">
<div class="col-md-12">
	<div class="box">
	    <div class="box-header with-border">
	      <h3 class="box-title"> Account Pardons <?php echo date('Y');?></h3>

	      <div class="box-tools pull-right">
	      	<button type="button" class="btn btn-sm btn-default" onclick="display_pardon_model()" >
                Record Pardon
         	</button>
	      </div>
	    </div>
	    <div class="box-body">
	    	<div class="modal fade" id="modal-pardons">
		          <div class="modal-dialog">
		            <div class="modal-content">
		              <div class="modal-header">
		                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		                  <span aria-hidden="true">&times;</span></button>
		                <h4 class="modal-title">Record New Pardon</h4>
		              </div>
		              	<div class="modal-body">
		                	<div class="row">
								<div class="col-md-12">
									<div class="box box-primary">
							            <div class="box-body">
							            	<form id="add_pardons" method="post" class="form-horizontal">
							            	<?php //echo form_open("accounts/create_pardon/".$tenant_unit_id."/".$lease_id, array("class" => "form-horizontal"));?>

												<input type="hidden" name="type_of_account" value="1">
												<input type="hidden" name="tenant_unit_id" id="tenant_unit_id" value="<?php echo $tenant_unit_id;?>">
												<input type="hidden" name="lease_id" id="lease_id" value="<?php echo $lease_id?>">
												<input type="hidden" name="redirect_url" value="<?php echo $this->uri->uri_string()?>">
												<div class="form-group">
													<label class="col-md-4 control-label">Pardon Amount: </label>
												  
													<div class="col-md-7">
														<input type="number" class="form-control" name="pardon_amount" placeholder="" autocomplete="off" required>
													</div>
												</div>
												<div class="form-group">
													<label class="col-md-4 control-label">Pardon Reason: </label>
												  
													<div class="col-md-7">
														<textarea class="form-control" name="pardon_reason" autocomplete="off"></textarea>
													</div>
												</div>

												<div class="form-group">
													<label class="col-md-4 control-label">Pardon Date: </label>
												  
													<div class="col-md-7">
														 <div class="input-group">
									                        <span class="input-group-addon">
									                            <i class="fa fa-calendar"></i>
									                        </span>
									                        <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="pardon_date" placeholder="Pardon Date" id="datepicker" required>
									                    </div>
													</div>
												</div>
												
												<div class="center-align">
													<button class="btn btn-info btn-sm" type="submit">Add Pardon Information</button>
												</div>
												<?php echo form_close();?>
							            		
										</div>
									</div>
								</div>
							</div>
						</div>
		            
		              <div class="modal-footer">
		                <button type="button" class="btn btn-default " data-dismiss="modal">Close</button>
		                <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
		              </div>
		            </div>
		            <!-- /.modal-content -->
		          </div>
		          <!-- /.modal-dialog -->
		        </div>

	    	
			<table class="table table-hover table-bordered col-md-12">
				<thead>
					<tr>
						<th>#</th>
						<th>Document number</th>
						<th>Pardoned Date</th>
						<th>Pardoned Amount </th>
						<th>Pardoned By</th>
						<th>Reason</th>
						<th colspan="1">Actions</th>
					</tr>
				</thead>
				<tbody>
					<?php
					if($lease_pardons->num_rows() > 0)
					{
						$y = 0;
						foreach ($lease_pardons->result() as $key_pardons) {
							# code...
							$document_number = $key_pardons->document_number;
							$pardon_amount = $key_pardons->pardon_amount;
							$pardon_id = $key_pardons->pardon_id;
							$created_by = $key_pardons->created_by;
							$payment_date = $key_pardons->payment_date;
							$pardon_date = $key_pardons->pardon_date;
							$pardon_reason = $key_pardons->pardon_reason;
							$payment_explode = explode('-', $payment_date);

							$pardon_date = date('jS M Y',strtotime($pardon_date));
							$y++;
							$message = $this->site_model->create_web_name('Your payment of Ksh. '.number_format($pardon_amount, 0).' has been received for Hse No. '.$rental_unit_name);
							
							?>
							<tr>
								<td><?php echo $y?></td>
								<td><?php echo $document_number?></td>
								<td><?php echo $pardon_date;?></td>
								<td><?php echo number_format($pardon_amount,2);?></td>
								<td><?php echo $created_by;?></td>
								<td><?php echo $pardon_reason;?></td>
								
                                <td>
                              		<button type="button" class="btn btn-sm btn-default" data-toggle="modal" data-target="#refund_payment<?php echo $pardon_id;?>"><i class="fa fa-times"></i></button>
                                	
                                	<div class="modal fade" id="refund_payment<?php echo $pardon_id;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
									    <div class="modal-dialog" role="document">
									        <div class="modal-content">
									            <div class="modal-header">
									            	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
									            	<h4 class="modal-title" id="myModalLabel">Cancel Pardons</h4>
									            </div>
									            <div class="modal-body">
									            	<?php echo form_open("accounts/cancel_pardon/".$pardon_id, array("class" => "form-horizontal"));?>

									            	<input type="hidden" name="redirect_url" value="<?php echo $this->uri->uri_string();?>">
									                <div class="form-group">
									                    <label class="col-md-4 control-label">Action: </label>
									                    
									                    <div class="col-md-8">
									                        <select class="form-control" name="cancel_action_id">
									                        	<option value="">-- Select action --</option>
									                            <?php
									                                if($cancel_actions->num_rows() > 0)
									                                {
									                                    foreach($cancel_actions->result() as $res)
									                                    {
									                                        $cancel_action_id = $res->cancel_action_id;
									                                        $cancel_action_name = $res->cancel_action_name;
									                                        
									                                        echo '<option value="'.$cancel_action_id.'">'.$cancel_action_name.'</option>';
									                                    }
									                                }
									                            ?>
									                        </select>
									                    </div>
									                </div>
									                
									                <div class="form-group">
									                    <label class="col-md-4 control-label">Description: </label>
									                    
									                    <div class="col-md-8">
									                        <textarea class="form-control" name="cancel_description"></textarea>
									                    </div>
									                </div>
									                
									                <div class="row">
									                	<div class="col-md-8 col-md-offset-4">
									                    	<div class="center-align">
									                        	<button type="submit" class="btn btn-primary">Save action</button>
									                        </div>
									                    </div>
									                </div>
									                <?php echo form_close();?>
									            </div>
									            <div class="modal-footer">
									                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
									            </div>
									        </div>
									    </div>
									</div>
                                </td>
							</tr>
							<?php

						}
					}
					?>
					
				</tbody>
			</table>
		</div>
	</div>
</div>
</div>