<?php
$property_invoices = $this->accounts_model->get_property_invoice_types($lease_id,1,1);
$inputs = '';
if($property_invoices->num_rows() > 0)
{
	// var_dump($property_invoices->result()); die();
	foreach ($property_invoices->result() as $key_types) {
		# code...
		$property_invoice_type_id = $key_types->invoice_type_id;

		if($property_invoice_type_id == 1)
		{
			$inputs .='
						<div class="form-group">
							<label class="col-md-4 control-label">Rent Amount: </label>

							<div class="col-md-7">
								<input type="number" class="form-control" name="rent_amount" placeholder="'.$grand_rent_bill.'" value="'.$grand_rent_bill.'" autocomplete="off" >
							</div>
						</div>
					   ';
		}
		else if($property_invoice_type_id == 2)
		{
			$inputs .='<div class="form-group">
							<label class="col-md-4 control-label">W/C Amount: </label>

							<div class="col-md-7">
								<input type="number" class="form-control" name="water_amount" placeholder="'.$grand_water_bill.'" autocomplete="off" >
							</div>
						</div>';
		}
		else if($property_invoice_type_id == 3)
		{
			$inputs .='';
		}
		else if($property_invoice_type_id == 4)
		{
			$inputs .='<div class="form-group">
							<label class="col-md-4 control-label">S/C Amount: </label>

							<div class="col-md-7">
								<input type="number" class="form-control" name="service_charge_amount" value="'.$grand_service_charge_bill.'" placeholder="'.$grand_service_charge_bill.'" autocomplete="off">
							</div>
						</div>';
		}
		else if($property_invoice_type_id == 5)
		{
			$inputs .='<div class="form-group">
							<label class="col-md-4 control-label">Penalty: </label>

							<div class="col-md-7">
								<input type="number" class="form-control" name="penalty_fee" placeholder="'.$grand_penalty_bill.'" autocomplete="off" >
							</div>
						</div>';
		}
		else if($property_invoice_type_id == 6)
		{
			$inputs .='';
		}
		else if($property_invoice_type_id == 7)
		{
			$inputs .='<div class="form-group">
							<label class="col-md-4 control-label">Painting Charge: </label>

							<div class="col-md-7">
								<input type="number" class="form-control" name="painting_charge" placeholder="" autocomplete="off">
							</div>
						</div>';
		}
		else if($property_invoice_type_id == 8)
		{
			$inputs .='<div class="form-group">
							<label class="col-md-4 control-label">Sinking Funds: </label>

							<div class="col-md-7">
								<input type="number" class="form-control" name="sinking_funds" placeholder="" autocomplete="off">
							</div>
						</div>
					';
		}
		else if($property_invoice_type_id == 9)
		{
			$inputs .='<div class="form-group">
							<label class="col-md-4 control-label">Insurance: </label>

							<div class="col-md-7">
								<input type="number" class="form-control" name="insurance" placeholder="" autocomplete="off">
							</div>
						</div>';
		}
		else if($property_invoice_type_id == 12)
		{
			$inputs .='<div class="form-group">
							<label class="col-md-4 control-label">Fixed Charge: </label>

							<div class="col-md-7">
								<input type="number" class="form-control" name="fixed_charge" placeholder="" autocomplete="off">
							</div>
						</div>';
		}
		else if($property_invoice_type_id == 10)
		{
			$inputs .='<div class="form-group">
							<label class="col-md-4 control-label">Bought Water: </label>

							<div class="col-md-7">
								<input type="number" class="form-control" name="bought_water" placeholder="" autocomplete="off">
							</div>
						</div>';
		}
		else if($property_invoice_type_id == 13)
		{
			$inputs .='<div class="form-group">
							<label class="col-md-4 control-label">Deposit: </label>

							<div class="col-md-7">
								<input type="number" class="form-control" name="deposit_charge" placeholder="" autocomplete="off">
							</div>
						</div>';
		}
		else if($property_invoice_type_id == 17)
		{
			$inputs .='<div class="form-group">
							<label class="col-md-4 control-label">Legal Fees: </label>

							<div class="col-md-7">
								<input type="number" class="form-control" name="legal_fees" placeholder="" autocomplete="off">
							</div>
						</div>';
		}

		else if($property_invoice_type_id == 11)
		{
			$inputs .='';
		}
	}
}
?>
<div class="box">
    <div class="box-header with-border">
      <h3 class="box-title">Payment Details <?php echo date('Y');?></h3>
      <div class="box-tools pull-right">
      	 	<button type="button" class="btn btn-sm btn-default"  onclick="display_payment_model()">
                Record Payment
         	</button>
      </div>
    </div>
    <div class="box-body">
    		<div class="modal fade" id="modal-defaults">
			  	<div class="modal-dialog">
			    <div class="modal-content">
			      <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span></button>
			        <h4 class="modal-title">Record New Payment</h4>
			      </div>
			      	<div class="modal-body">
			        	<div class="row">
							<div class="col-md-12">
								<div class="box box-primary">
						            <div class="box-body">
						            		<form id="add_payments" method="post" class="form-horizontal">
						            		<?php //echo form_open("accounts/make_payments/".$tenant_unit_id."/".$lease_id, array("class" => "form-horizontal"));?>
												<input type="hidden" name="type_of_account" value="1">
												<input type="hidden" name="tenant_unit_id" id="tenant_unit_id" value="<?php echo $tenant_unit_id;?>">
												<input type="hidden" name="lease_id" id="lease_id" value="<?php echo $lease_id?>">
												<input type="hidden" name="redirect_url" value="<?php echo $this->uri->uri_string()?>">
												<div class="form-group" id="payment_method">
													<label class="col-md-4 control-label">Payment Method: </label>

													<div class="col-md-7">
														<select class="form-control select2" name="payment_method" onchange="check_payment_type(this.value)" required>
															<option value="0">Select a payment method</option>
			                                            	<?php
															  $method_rs = $this->accounts_model->get_payment_methods();

																foreach($method_rs->result() as $res)
																{
																  $payment_method_id = $res->payment_method_id;
																  $payment_method = $res->payment_method;

																	echo '<option value="'.$payment_method_id.'">'.$payment_method.'</option>';

																}

														  ?>
														</select>
													  </div>
												</div>
												<div id="mpesa_div" class="form-group" style="display:none;" >
													<label class="col-md-4 control-label"> Mpesa TX Code: </label>

													<div class="col-md-7">
														<input type="text" class="form-control" name="mpesa_code" placeholder="">
													</div>
												</div>


												<div id="cheque_div" class="form-group" style="display:none;" >
													<label class="col-md-4 control-label"> Bank Name: </label>

													<div class="col-md-7">
														<input type="text" class="form-control" name="bank_name" placeholder="Barclays">
													</div>
												</div>


												<div class="form-group">
													<label class="col-md-4 control-label">Total Amount: </label>

													<div class="col-md-7">
														<input type="number" class="form-control" name="amount_paid" placeholder=""  autocomplete="off" required>
													</div>
												</div>

												<!-- where you put all the item that are to be paid for in this property -->
												<?php echo $inputs;?>
												<!-- end of the items to be paid for in this property -->
												<div class="form-group">
													<label class="col-md-4 control-label">Paid in By: </label>

													<div class="col-md-7">
														<input type="text" class="form-control" name="paid_by" placeholder="<?php echo $tenant_name;?>" value="<?php echo $tenant_name;?>" autocomplete="off" >
													</div>
												</div>

												<div class="form-group">
													<label class="col-md-4 control-label">Payment Date: </label>

													<div class="col-md-7">
														 <div class="input-group">
							                                <span class="input-group-addon">
							                                    <i class="fa fa-calendar"></i>
							                                </span>
							                                <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="payment_date" placeholder="Payment Date" id="datepicker" required>
							                            </div>
													</div>
												</div>

												<div class="center-align">
													<button class="btn btn-info btn-sm" type="submit">Add Payment Information</button>
												</div>
											<?php echo form_close();?>
									</div>
								</div>
							</div>
						</div>
					</div>

			      <div class="modal-footer">
			        <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
			        <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
			      </div>
			    </div>
			    <!-- /.modal-content -->
			  </div>
			  	<!-- /.modal-dialog -->
				</div>

        <!-- /.modal -->
		<table class="table table-hover table-bordered col-md-12">
			<thead>
				<tr>
					<th>#</th>
					<th>Payment Date</th>
					<th>Receipt Number</th>
					<th>Amount Paid</th>
					<th>Paid By</th>
					<th>Receipted Date</th>
					<th>Receipted By</th>
					<th>Reconcilled Status</th>
					<th colspan="3">Actions</th>
				</tr>
			</thead>
			<tbody>
				<?php
				if($lease_payments->num_rows() > 0)
				{
					$y = 0;
					foreach ($lease_payments->result() as $key) {
						# code...
						$receipt_number = $key->receipt_number;
						$amount_paid = $key->amount_paid;
						$payment_id = $key->payment_id;
						$paid_by = $key->paid_by;
						$payment_date = $key->payment_date;
						$document_number = $key->document_number;
						$payment_created = $key->payment_created;
						$payment_created_by = $key->payment_created_by;
						$confirmation_status = $key->confirmation_status;

						if($confirmation_status == 0)
						{
							$confirmation_status_text = "Reconciled";
							$button = '<a class="btn btn-sm btn-danger" href="'.site_url().'accounts/deactivate_payments/'.$payment_id.'/'.$lease_id.'" onclick="return confirm(\'Do you want to Deactivate this payment Kes.'.$amount_paid.'?\');" title="Deactivate For Company Return'.$amount_paid.'"> Deactivate </a>';


						}
						else
						{
							$confirmation_status_text = "Not Reconciled";
							$button = '<a class="btn btn-sm btn-info" href="'.site_url().'accounts/activate_payments/'.$payment_id.'/'.$lease_id.'" onclick="return confirm(\'Do you want to Activate this payment Kes.'.$amount_paid.'?\');" title="Activate For Company Return'.$amount_paid.'"> Activate</a>';


						}
						$payment_explode = explode('-', $payment_date);

						$payment_date = date('jS M Y',strtotime($payment_date));
						$payment_created = date('jS M Y',strtotime($payment_created));
						$y++;
						$message = $this->site_model->create_web_name('Your payment of Ksh. '.number_format($amount_paid, 0).' has been received for Hse No. '.$rental_unit_name);

						?>
						<tr>
							<td><?php echo $y?></td>
							<td><?php echo $payment_date;?></td>
							<td><?php echo $document_number?></td>
							<td><?php echo number_format($amount_paid,2);?></td>
							<td><?php echo $paid_by;?></td>
							<td><?php echo $payment_date;?></td>
							<td><?php echo $payment_created_by;?></td>
							<td><?php echo $confirmation_status_text;?></td>
							<!-- <td><?php echo $button; ?></td> -->

							<td><a href="<?php echo site_url().'cash-office/print-receipt/'.$payment_id.'/'.$tenant_unit_id.'/'.$lease_id;?>" class="btn btn-sm btn-primary" target="_blank">Receipt</a></td>
                            <td><a href="<?php echo site_url().'send-tenants-receipt/'.$payment_id.'/'.$tenant_unit_id.'/'.$lease_id;?>" class="btn btn-sm btn-success">Send SMS</a></td>
                            <td>
                          		<button type="button" class="btn btn-sm btn-default" data-toggle="modal" data-target="#refund_payment<?php echo $payment_id;?>"><i class="fa fa-times"></i></button>

                            	<div class="modal fade" id="refund_payment<?php echo $payment_id;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
															    <div class="modal-dialog" role="document">
															        <div class="modal-content">
															            <div class="modal-header">
															            	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
															            	<h4 class="modal-title" id="myModalLabel">Cancel payment</h4>
															            </div>
															            <div class="modal-body">
															            	<?php echo form_open("accounts/cancel_payment/".$payment_id, array("class" => "form-horizontal"));?>

															            	<input type="hidden" name="redirect_url" value="<?php echo $this->uri->uri_string();?>">
															                <div class="form-group">
															                    <label class="col-md-4 control-label">Action: </label>

															                    <div class="col-md-8">
															                        <select class="form-control" name="cancel_action_id">
															                        	<option value="">-- Select action --</option>
															                            <?php
															                                if($cancel_actions->num_rows() > 0)
															                                {
															                                    foreach($cancel_actions->result() as $res)
															                                    {
															                                        $cancel_action_id = $res->cancel_action_id;
															                                        $cancel_action_name = $res->cancel_action_name;

															                                        echo '<option value="'.$cancel_action_id.'">'.$cancel_action_name.'</option>';
															                                    }
															                                }
															                            ?>
															                        </select>
															                    </div>
															                </div>

															                <div class="form-group">
															                    <label class="col-md-4 control-label">Description: </label>

															                    <div class="col-md-8">
															                        <textarea class="form-control" name="cancel_description"></textarea>
															                    </div>
															                </div>

															                <div class="row">
															                	<div class="col-md-8 col-md-offset-4">
															                    	<div class="center-align">
															                        	<button type="submit" class="btn btn-primary">Save action</button>
															                        </div>
															                    </div>
															                </div>
															                <?php echo form_close();?>
															            </div>
															            <div class="modal-footer">
															                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
															            </div>
															        </div>
															    </div>
															</div>
                            </td>
						</tr>
						<?php

					}
				}
				?>

			</tbody>
		</table>
	</div>
</div>
