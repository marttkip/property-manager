<?php
$items = '';
$items_charge ='';
$total_bill = 0;
$service_charge = 0;
$total_penalty =0;
$penalty_charge =0;
$query = $this->leases_model->get_lease_detail_owners($home_owner_id);
// $invoice_month_parent = $invoice_month;
// $invoice_year_parent = $invoice_year;

foreach ($query->result() as $leases_row)
{
	$rental_unit_idd = $leases_row->rental_unit_id;
	if($rental_unit_id == $rental_unit_idd)
	{
		
	$rental_unit_name = $leases_row->rental_unit_name;

	}
	$property_name = $leases_row->property_name;
	$property_id = $leases_row->property_id;
	$message_prefix = $leases_row->message_prefix;
	$home_owner_name = $leases_row->home_owner_name;
	$home_owner_phone_number = $leases_row->home_owner_phone_number;
	$home_owner_name = $leases_row->home_owner_name;
	$home_owner_email = $leases_row->home_owner_email;
	$home_owner_phone_number = $leases_row->home_owner_phone_number;
	$home_owner_email = $leases_row->home_owner_email;
	// $amount = 450;
	// $invoice_month = date('m');
	// $invoice_year = date('Y');
	// $total_amount = $total_amount + $amount;
	
}
$lease_invoice = $this->accounts_model->get_invoices_month_home_owners($rental_unit_id,$invoice_month_parent,$invoice_year_parent);

$parent_invoice_date = $this->accounts_model->get_max_owners_invoice_date($rental_unit_id,$invoice_month_parent,$invoice_year_parent);
$property_invoices = $this->accounts_model->get_property_invoice_types($property_id,0);




$starting_arrears = $this->accounts_model->get_all_owners_arrears($rental_unit_id);
// var_dump($starting_arrears); die();

if($lease_invoice->num_rows() > 0)
{
	
	$service_charge = 0;
	$service_bf = 0;
	$penalty_charge =0;
	$penalty_bf =0;
	// var_dump($lease_invoice->result()); die();
	foreach ($lease_invoice->result() as $key_item) {
		# code...
		$invoice_type = $key_item->invoice_type_id;
		$billing_schedule_quarter = str_replace('AC', 'Q', $key_item->billing_schedule_quarter);

	}
}
if($property_invoices->num_rows() > 0)
{
	
	$service_charge = 0;
	$service_bf = 0;
	$penalty_charge =0;
	$penalty_bf =0;
	// var_dump($property_invoices->result()); die();
	foreach ($property_invoices->result() as $key_invoice) {
		# code...
		$invoice_type = $key_invoice->invoice_type_id;


		if($invoice_type == 4)
		{
			
			$service_invoice_amount = $this->accounts_model->get_invoice_owners_brought_forward($rental_unit_id,$parent_invoice_date,$invoice_type);
			$service_paid_amount = $this->accounts_model->get_paid_owners_brought_forward($rental_unit_id,$parent_invoice_date,$invoice_type);

			$total_service_charge_forward = $service_invoice_amount - $service_paid_amount;
			
			$service_current_invoice =  $this->accounts_model->get_invoice_owners_current_forward($rental_unit_id,$parent_invoice_date,$invoice_type);
			$service_current_payment =  $this->accounts_model->get_today_owners_paid_current_forward($rental_unit_id,$parent_invoice_date,$invoice_type);
		
			$total_service = $service_current_invoice - $service_current_payment;
		

		}


		if($invoice_type == 15)
		{
			// penalty

			$arrears_invoice_amount = $this->accounts_model->get_invoice_owners_brought_forward($rental_unit_id,$parent_invoice_date,$invoice_type);
			$arrears_paid_amount = $this->accounts_model->get_paid_owners_brought_forward($rental_unit_id,$parent_invoice_date,$invoice_type);

			$total_arrears_forward = $arrears_invoice_amount - $arrears_paid_amount;
			
			$arrears_current_invoice =  $this->accounts_model->get_invoice_owners_current_forward($rental_unit_id,$parent_invoice_date,$invoice_type);
			$arrears_current_payment =  $this->accounts_model->get_today_owners_paid_current_forward($rental_unit_id,$parent_invoice_date,$invoice_type);
		
			$total_arrears = $arrears_current_invoice - $arrears_current_payment;
			// var_dump($arrears_invoice_amount); die();
		}
		if($invoice_type == 5)
		{
			// penalty

			$penalty_invoice_amount = $this->accounts_model->get_invoice_owners_brought_forward($rental_unit_id,$parent_invoice_date,$invoice_type);
			$penalty_paid_amount = $this->accounts_model->get_paid_owners_brought_forward($rental_unit_id,$parent_invoice_date,$invoice_type);

			$total_penalty_forward = $penalty_invoice_amount - $penalty_paid_amount;
			
			$penalty_current_invoice =  $this->accounts_model->get_invoice_owners_current_forward($rental_unit_id,$parent_invoice_date,$invoice_type);
			$penalty_current_payment =  $this->accounts_model->get_today_owners_paid_current_forward($rental_unit_id,$parent_invoice_date,$invoice_type);
		
			$total_penalty = $penalty_current_invoice - $penalty_current_payment;
			// var_dump($arrears_invoice_amount); die();
		}
	}
}

$account_invoice_amount = $this->accounts_model->get_invoice_owners_brought_forward($rental_unit_id,$parent_invoice_date);
$account_paid_amount = $this->accounts_model->get_paid_owners_brought_forward($rental_unit_id,$parent_invoice_date);
$account_todays_payment =  $this->accounts_model->get_today_owners_paid_current_forward($rental_unit_id,$parent_invoice_date);


$total_brought_forward_account = $account_invoice_amount - $account_paid_amount - $account_todays_payment + $starting_arrears;
// var_dump($starting_arrears);die();
$total_bill = $total_service + $total_arrears  + $total_arrears_forward + $total_service_charge_forward +$total_penalty + $total_penalty_forward + $starting_arrears;

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title><?php echo $contacts['company_name'];?> | INVOICE</title>
        <!-- For mobile content -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" media="all"/>
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css" media="all"/>
        <style type="text/css">
			.receipt_spacing{letter-spacing:0px;}
			.center-align{margin:0 auto; text-align:center;}
			
			.receipt_bottom_border{border-bottom: #888888 medium solid; margin-bottom:1px;}
			.row .col-xs-12 table {
				border:solid #000 !important;
				border-width:1px 0 0 1px !important;
				/*font-size:10px;*/
			}
			.row .col-xs-12 th, .row .col-xs-12 td {
				border:solid #000 !important;
				border-width:0 1px 1px 0 !important;
			}
			.table thead > tr > th, .table tbody > tr > th, .table tfoot > tr > th, .table thead > tr > td, .table tbody > tr > td, .table tfoot > tr > td
			{
				 padding: 2px;
			}
			.align-center
			{
				/*padding: 50px;*/
    			text-align: center;
    			font-weight: bolder;
			}
			
			.row .col-xs-12 .title-item{float:left;width: 130px; font-weight:bold; text-align:right; padding-right: 20px;}
			.title-img{float:left; padding-left:30px;}
			img.logo{max-height:70px; margin:0 auto;}
			.align-right{margin:0 auto; text-align: right !important;}
			.row {
			    margin-left: 0;
			    margin-right: 0;
			}
			.panel-title
			{
				/*font-size: 13px !important;*/
			}
			.table {
			  margin-bottom: 5px;
			  max-width: 100%;
			  width: 100%;
			}
			.table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td{border-top: none !important;}
			.table {
			  margin-bottom: 5px;
			  max-width: 100%;
			  width: 100%;
			}
			.row {
  				margin-left: 0px !important;
    			margin-right: 0px !important;
			}
			body
			{
				font-size: 10px;
			}
			p
			{
				line-height:6px;
			}
		</style>
    </head>
    <body class="receipt_spacing">
        <div class="row">
	        <div class="col-xs-9  col-xs-offset-1">
		    	<div class="receipt_bottom_border">
		        	<table class="table table-condensed">
		                <tr>
		                    <th><h4><?php echo $contacts['company_name'];?> </h4></th>
		                    <th class="align-right">
		                        <img src="<?php echo base_url().'assets/logo/'.$contacts['logo'];?>" alt="<?php echo $contacts['company_name'];?>" class="img-responsive logo" style="float:right;"/>
		                    </th>
		                </tr>
		            </table>
		        </div>
		    	
		        
		        <!-- Patient Details -->
		    	 <div class="row receipt_bottom_border" style="margin-bottom: 10px;margin-top: 5px; font-size: 10px;">
		        
		            <div class="col-xs-6 ">
		                
		                <p><strong> Name </strong> : <?php echo $home_owner_name;?></p>
		                <p><strong> Phone </strong> :<?php echo $home_owner_phone_number;?></p>
		                <p><strong> Unit </strong> : <?php echo $property_name;?> - <?php echo $rental_unit_name;?></p>

		               
		            </div>
		            <div class="col-xs-6">
		             	<p><strong> Invoice # </strong> : <?php echo $document_number = 'U'.$rental_unit_id.''.$invoice_month_parent.'-'.$invoice_year_parent.'';?></p>
		             	<p><strong> Invoice Date </strong> : <?php echo date('jS M Y',strtotime($parent_invoice_date));?></p>
		             	<p><strong> Billing for  </strong> : <?php echo date('M Y',strtotime($parent_invoice_date));?></p>
		            </div>
		        </div>
		      	<div class="row">
		      		<div class="row">
				        	<div class="col-xs-4 receipt_bottom_border">
				        		<strong>A. Balance B/F </strong> 
				        	</div>
				    </div>
			        <div class="col-xs-12">
			        	<div class="col-xs-4">
			        		
			        	</div>
			        	<div class="col-xs-4">
			        	</div>
			        	<div class="col-xs-4 pull-right">
			        		<strong>Kes. <?php echo number_format($total_brought_forward_account,2);?> </strong>
			        	</div>
			        	
			        </div>
			   	</div> 
			   	<div class="row">
			        <div class="row">
				        	<div class="col-xs-4 receipt_bottom_border">
				        		<strong>B. SERVICE CHARGE </strong> 
				        	</div>
				    </div>
			        <div class="col-xs-12">
			        	<div class="col-xs-4">
			        		Current Bill
			        	</div>
			        	<div class="col-xs-4">
			        	</div>
			        	<div class="col-xs-4 pull-right">
			        		<strong>Kes. <?php echo number_format($total_service,2);?> </strong>
			        	</div>
			        	
			        </div>
				        
			   	</div> 

			  
			   	<div class="row">
		        	 <div class="row">
			        	<div class="col-xs-4 receipt_bottom_border">
			        		<strong>C. PENALTY </strong> 
			        	</div>
			        </div>
			       	<div class="col-xs-12">
			        	<div class="col-xs-4">
			        	 Penalty
			        	</div>
			        	<div class="col-xs-4" >
			        	</div>
			        	<div class="col-xs-4 pull-right">
			        		<strong> Kes. <?php echo number_format($total_penalty,2);?></strong>
			        	</div>
			        </div>
			   	</div>
			   	<div class="row">
			        <div class="row">
			        	<div class="col-xs-4 receipt_bottom_border">
			        		<strong>TOTAL DUES </strong> 
			        	</div>
			        </div>
			        <div class="col-xs-12">
			        	<div class="col-xs-4">
			        	</div>
			        	<div class="col-xs-4" >
			        	</div>
			        	<div class="col-xs-4 pull-right " style="border-top:2px #000 solid !important;border-bottom:2px #000 solid !important; margin-bottom:5px;">
			        		<strong> Kes. <?php echo number_format($total_bill,2);?></strong>
			        	</div>
			        </div>
			   	</div>
		       
		       <div class="row" style="border-top:1px #000 solid;">
		       		<div class="col-xs-12 pull-left">
		       			<strong >  NOTE : Payment should be made by 15th of the month, failure will  lead to water disconnection.
		       			</strong><br>
		       			<strong > <?php echo $message_prefix;?></strong>
		            </div>
		       </div>
		       <div class="row" >
		       		<div class="col-xs-12 align-center" style="border-top:1px #000 solid;">
		       			<span style="font-size:10px; " > <?php echo $contacts['company_name'];?> | <?php echo $contacts['location'];?>, <?php echo $contacts['building'];?>, <?php echo $contacts['floor'];?>
		                     E-mail: <?php echo $contacts['email'];?>.<br/> Tel : <?php echo $contacts['phone'];?>
		                     P.O. Box <?php echo $contacts['address'];?> <?php echo $contacts['post_code'];?>, <?php echo $contacts['city'];?></span>
		            </div>
		       </div>
		       
		</div>
		</div>
        
    </body>
    
</html>