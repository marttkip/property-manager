<?php

$company_name = $contacts['company_name'];


$all_leases = $this->leases_model->get_lease_detail($lease_id);
$items = '';
$items_charge ='';
$total_bill = 0;
$service_charge = 0;
$penalty_charge =0;
$water_charge =0;
foreach ($all_leases->result() as $leases_row)
{
	$lease_id = $leases_row->lease_id;
	$tenant_unit_id = $leases_row->tenant_unit_id;
	$property_name = $leases_row->property_name;
	// $units_name = $leases_row->units_name;
	$rental_unit_name = $leases_row->rental_unit_name;
	$tenant_name = $leases_row->tenant_name;
	$lease_start_date = $leases_row->lease_start_date;
	$lease_duration = $leases_row->lease_duration;
	$rent_amount = $leases_row->rent_amount;
	$lease_number = $leases_row->lease_number;
	$arrears_bf = $leases_row->arrears_bf;
	$rent_calculation = $leases_row->rent_calculation;
	$deposit = $leases_row->deposit;
	$deposit_ext = $leases_row->deposit_ext;
	$tenant_phone_number = $leases_row->tenant_phone_number;
	$tenant_email = $leases_row->tenant_email;
	$tenant_national_id = $leases_row->tenant_national_id;
	$lease_status = $leases_row->lease_status;
	$tenant_status = $leases_row->tenant_status;
	$tenant_number = $leases_row->tenant_number;
  $account_id = $leases_row->account_id;
	$created = $leases_row->created;
	$message_prefix = $leases_row->message_prefix;

	$lease_start_date = date('jS M Y',strtotime($lease_start_date));

	// $expiry_date  = date('jS M Y',strtotime($lease_start_date, mktime()) . " + 365 day");
	$expiry_date  = date('jS M Y', strtotime(''.$lease_start_date.'+1 years'));

}

$today = date('jS F Y H:i a',strtotime(date("Y:m:d h:i:s")));

$served_by = $this->accounts_model->get_personnel($this->session->userdata('personnel_id'));
// get when the lease was active

$lease_invoice = $this->accounts_model->get_invoices_month($lease_id,$invoice_month,$invoice_year,$lease_invoice_id);
// get all the invoiced months
// $parent_invoice_date = $this->accounts_model->get_max_invoice_date($lease_id,$invoice_month,$invoice_year);
$result = '';
// var_dump($lease_invoice); die();

if($lease_invoice->num_rows() > 0)
{
	$water_charge = 0;
	$service_bf = 0;
	$water_charge = 0;
	$water_bf = 0;
	$penalty_charge =0;
	$penalty_bf =0;
	$water_payments = 0;
	$total_penalty = 0;
	$total_water = 0;
	$total_variance = 0;
	$count = 0;
	foreach ($lease_invoice->result() as $key_invoice) {
		# code...


		$invoice_date = $parent_invoice_date = $key_invoice->invoice_date;
		$lease_invoice_id = $key_invoice->lease_invoice_id;
		$invoice_type = $key_invoice->invoice_type;
		$invoice_type_name = $key_invoice->invoice_type_name;
		$document_number = $key_invoice->document_number;


		$invoice_date_date = date('jS F Y',strtotime($invoice_date));

		// if($invoice_type == 1)
		// {
			$count++;
			// service charge
			$invoice_type_name = $key_invoice->invoice_type_name;
			$rent_invoice_amount = $this->accounts_model->get_invoice_tenants_brought_forward($lease_id,$parent_invoice_date,$invoice_type);
			$rent_paid_amount = $this->accounts_model->get_paid_tenants_brought_forward($lease_id,$parent_invoice_date,$invoice_type);

			$total_brought_forward = $rent_invoice_amount - $rent_paid_amount;

			$rent_current_invoice =  $this->accounts_model->get_invoice_tenants_current_forward($lease_id,$parent_invoice_date,$invoice_type);
			$rent_current_payment =  $this->accounts_model->get_today_tenants_paid_current_forward($lease_id,$parent_invoice_date,$invoice_type);

			$total_rent += $rent_current_invoice;

			$result .= '
						<tr>
					        <td>'.$count.'</td>
					        <td>'.$property_name.'</td>
					        <td>'.$rental_unit_name.'</td>
					        <td>'.$invoice_type_name.'</td>
					        <td>'.number_format($rent_current_invoice,2).'</td>
					        <td>'.number_format($rent_current_invoice,2).'</td>
					      </tr>
						';


	}
}

$due_date = date('jS F Y', strtotime($invoice_date. ' + 6 days'));

// $tenants_response = $this->accounts_model->get_lease_balance($lease_id);
// $total_pardon_amount = $tenants_response['total_pardon'];

$account_invoice_amount = $this->accounts_model->get_invoice_tenants_brought_forward($lease_id,$parent_invoice_date);

$total_pardon_amount = $this->accounts_model->get_pardons_tenants_brought_forward($lease_id,$parent_invoice_date);
$account_paid_amount = $this->accounts_model->get_paid_tenants_brought_forward($lease_id,$parent_invoice_date);
// var_dump($total_pardon_amount); die();
$account_todays_payment =  $this->accounts_model->get_today_tenants_paid_current_forward($lease_id,$parent_invoice_date);

if($account_paid_amount == null)
{
	$account_paid_amount = 0;
}
if($account_todays_payment == null)
{
	$account_todays_payment = 0;
}
if(empty($total_pardon_amount))
{
	$total_pardon_amount = 0;
}
$total_brought_forward_account = $account_invoice_amount - $account_paid_amount - $account_todays_payment - $total_pardon_amount;
// var_dump($total_brought_forward_account);die();
$invoice_result = '';
$bf ='';
if($total_brought_forward_account > 0)
{
  $total_rent += $total_brought_forward_account;
	$bf = '
			<tr>
        <td></td>

		        <td colspan="3">Arrears</td>
		        <td>'.number_format($total_brought_forward_account,2).'</td>
		        <td>'.number_format($total_brought_forward_account,2).'</td>
		      </tr>
			';
}
$invoice_result = $bf.$result;

$invoice_result .= '
    <tr>
      <td colspan=5><b>Total Amount Due</b></td>
      <td><strong>'.number_format($total_rent,2).'</strong></td>
    </tr>
    ';

$total_bill = $total_rent + $total_deposit + $total_brought_forward_account + $total_service_charge + $total_variance;


?>

<!DOCTYPE html>
<html lang="en">
      <head>
        <title><?php echo $contacts['company_name'];?> | Tenant Invoice</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" media="all"/>
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css" media="all"/>
        <style type="text/css">
			.receipt_spacing{letter-spacing:0px;}
			.center-align{margin:0 auto; text-align:center;}

			.receipt_bottom_border{border-bottom: #000 medium solid; margin-bottom:1px;}
			.title-img{float:left; padding-left:30px;}
			img.logo{max-height:70px; margin:0 auto;}
			.align-right{margin:0 auto; text-align: right !important;}
			.align-center
			{
				/*padding: 50px;*/
    			text-align: center;
    			font-weight: bolder;
			}
			.table {
			  font-size: 11px !important;
			  margin-bottom: 15px;
			  max-width: 100%;
			  width: 100%;

			}
			.row {
  				margin-left: 0px !important;
    			margin-right: 0px !important;
			}
			.table-condensed > thead > tr > th, .table-condensed > tbody > tr > th, .table-condensed > tfoot > tr > th, .table-condensed > thead > tr > td, .table-condensed > tbody > tr > td, .table-condensed > tfoot > tr > td {
			  padding: 2px;

			}
			table tr, th, td
			{
				border: #000 solid 2px !important;
			}
			h3, .h3 {
			  font-size: 18px !important;
			}
      table
      {
        border: #000 solid 2px !important;
      }
		</style>
    </head>
     <body class="receipt_spacing">
			 <div class="row">
				 <div class="col-xs-12">
						 <img src="<?php echo base_url().'assets/logo/'.$contacts['logo'];?>" alt="<?php echo $contacts['company_name'];?>" class="img-responsive logo"/>
					 </div>
			 </div>
			 <br>
		 <div class="row " >
				 <div class="col-md-12 center-align">
						 <h2><strong>TENANT INVOICE</strong></h2>
					 </div>
			 </div>
       <div class="row " >
  				 <div class="col-md-6 left-align" style="text-align:left;">
             <b>NAME : <?php echo strtoupper($tenant_name);?><br>
              TENANT CODE: <?php echo $tenant_number?>  <br>
              INVOICE NO : <?php echo $document_number;?><br>
              INVOICED ON : <?php echo $invoice_date_date;?><br>
              SERVED BY : <?php echo $served_by?> <br></b>
  				 </div>
          <div class="col-md-6" style="text-align:right;">
            <strong><?php echo $company_name;?><br>
             <?php echo $contacts['location'];?>,<br/>
              Address : P.O. Box <?php echo $contacts['address'];?> <?php echo $contacts['post_code'];?>, <?php echo $contacts['city'];?> <br/>
              Phone:  <?php echo $contacts['phone'];?>
              <br/> <?php echo $contacts['email'];?>.<br/>
            PIN: P051309725G</strong>
          </div>
  		 </div>

        <!-- Table row -->
        <div class="row">
          <div class="col-md-12 table-responsive">
            <table class="table table-hover table-bordered">
        		<thead>
        			<tr>
                <th>#</th>
        				<th>Plot/Landlord</th>
        				<th>Hse No</th>
                <th>Desc</th>
        				<th>Amount</th>
        				<th>Total</th>
        			</tr>
        		</thead>
        	  	<tbody>
        	  		<?php echo $invoice_result;?>
                <tr>
                  <td colspan=6><b>  NOTE : Kindly take note that Accounts are due from the date hereof and you are hereby asked to remit the sum of Kshs <?php echo number_format($total_rent,2)?> in settlement of the invoice hereof.<br></b></td>
                </tr>


                <tr>
                  <td colspan=6><b> <?php echo $contacts['company_name']?><br><br> <br></b>
                    <b>Sign ..........................................................<br></b>

                  </td>
                </tr>
        	  	</tbody>
        	</table>
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->

        <div class="row" >
       		<div class="col-xs-12 align-center" style="border-top:1px #000 solid;">
       			<span style="font-size:10px; " > QUOTE OUR TENANT CODE WHEN REPLYING</span>
            </div>
       	</div>
     </body>
</html>
