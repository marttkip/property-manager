<?php
	$all_leases = $this->leases_model->get_lease_detail_owners($rental_unit_id);


	foreach ($all_leases->result() as $leases_row)
	{
		$rental_unit_id = $leases_row->rental_unit_id;
		$home_owner_name = $leases_row->home_owner_name;
		$home_owner_email = $leases_row->home_owner_email;
		$home_owner_phone_number = $leases_row->home_owner_phone_number;
		$rental_unit_name = $leases_row->rental_unit_name;
		$property_name = $leases_row->property_name;
		$message_prefix = $leases_row->message_prefix;
		
	}

	$today = date('jS F Y H:i a',strtotime(date("Y:m:d h:i:s")));
	
	$served_by = $this->accounts_model->get_personnel($this->session->userdata('personnel_id'));
	// get when the lease was active

	// get all the invoiced months
	// var_dump($lease_invoice); die();
	
	if($lease_invoice->num_rows() > 0)
	{
		
		$service_charge = 0;
		$service_bf = 0;
		$penalty_charge =0;
		$penalty_bf =0;

		foreach ($lease_invoice->result() as $key_invoice) {
			# code...
			$invoice_date = $key_invoice->invoice_date;
			$invoice_id = $key_invoice->invoice_id;
			$invoice_type = $key_invoice->invoice_type;

			
			$invoice_date_date = date('jS F Y',strtotime($invoice_date));
			

			if($invoice_type == 4)
			{
				// service charge
				$service_charge = $key_invoice->invoice_amount;
				$service_bf = $key_invoice->arrears_bf;
			}


			if($invoice_type == 5)
			{
				// penalty

				$penalty_charge = $key_invoice->invoice_amount;
				$penalty_bf = $key_invoice->arrears_bf;
			}
		}
	}


	$total_bill = number_format(($service_charge + $penalty_charge),2);


?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <title><?php echo $contacts['company_name'];?> | INVOICE</title>
        <!-- For mobile content -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" media="all"/>
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css" media="all"/>
        <style type="text/css">
			.receipt_spacing{letter-spacing:0px;}
			.center-align{margin:0 auto; text-align:center;}
			
			.receipt_bottom_border{border-bottom: #888888 medium solid; margin-bottom:1px;}
			.row .col-xs-12 table {
				border:solid #000 !important;
				border-width:1px 0 0 1px !important;
				/*font-size:10px;*/
			}
			.row .col-xs-12 th, .row .col-xs-12 td {
				border:solid #000 !important;
				border-width:0 1px 1px 0 !important;
			}
			.table thead > tr > th, .table tbody > tr > th, .table tfoot > tr > th, .table thead > tr > td, .table tbody > tr > td, .table tfoot > tr > td
			{
				 padding: 2px;
			}
			.align-center
			{
				/*padding: 50px;*/
    			text-align: center;
    			font-weight: bolder;
			}
			
			.row .col-xs-12 .title-item{float:left;width: 130px; font-weight:bold; text-align:right; padding-right: 20px;}
			.title-img{float:left; padding-left:30px;}
			img.logo{max-height:70px; margin:0 auto;}
			.align-right{margin:0 auto; text-align: right !important;}
			.row {
			    margin-left: 0;
			    margin-right: 0;
			}
			.panel-title
			{
				/*font-size: 13px !important;*/
			}
			.table {
			  margin-bottom: 5px;
			  max-width: 100%;
			  width: 100%;
			}
			.table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td{border-top: none !important;}
		</style>
    </head>
    <body class="receipt_spacing">
    	<div class="row">
    		<div class="col-md-12">
		    	<div class="receipt_bottom_border">
		        	<table class="table table-condensed">
		                <tr>
		                    <th><h4><?php echo $contacts['company_name'];?> </h4></th>
		                    <th class="align-right">
		                        <img src="<?php echo base_url().'assets/logo/'.$contacts['logo'];?>" alt="<?php echo $contacts['company_name'];?>" class="img-responsive logo" style="float:right;"/>
		                    </th>
		                </tr>
		            </table>
		        </div>
		    	
		        
		        <!-- Patient Details -->
		    	 <!-- Patient Details -->
		    	<div class="row receipt_bottom_border" style="margin-bottom: 10px;">
		        
		            <div class="col-xs-5 pull-left">
		                <h2 class="panel-title">Personal Information</h2>
		                <table class="table table-hover">
		                    <tbody>
		                        <tr><td><span>Name :</span></td><td><?php echo $home_owner_name;?></td></tr>
		                        <tr><td><span>Phone :</span></td><td><?php echo $home_owner_phone_number;?></td></tr>
		                        <tr><td><span>Property :</span></td><td><?php echo $property_name;?></td></tr>
		                        <tr><td><span>Hse No. :</span></td><td><?php echo $rental_unit_name;?></td></tr>
		                        
		                    </tbody>
		                </table>
		            </div>
		             <div class="col-xs-5 pull-right">
		                <h2 class="panel-title">Invoice</h2>
		                <table class="table table-hover">
		                    <tbody>
		                        <tr><td><span>Date :</span></td><td><?php echo $invoice_date_date;?></td></tr>
		                        
		                    </tbody>
		                </table>
		            </div>
		        </div>
		    	<div class="row">
			        <div class="col-md-12">
			        	<div class="row receipt_bottom_border">
				        	<div class="col-xs-12">
				            	<strong> A. SERVICE CHARGE DUES</strong>
				            </div>
				        </div>
			        	<table class="table table-hover ">
				            <tbody>
				               <tr>
				               	<td >Balance B/F</td><td class="pull-right"> Kes. <?php echo number_format($service_bf,2);?></td>
				               </tr>
				               <tr>
				               	<td >Current Billing</td><td class="pull-right"> Kes. <?php echo number_format($service_charge-$service_bf,2);?></td>
				               </tr>
				               <tr>
				               	<td ><strong>Total SC Due</strong> </td><td class="pull-right"><strong> Kes. <?php echo number_format($service_charge,2);?> </strong> </td>
				               </tr>
				               
				            </tbody>
				        </table>
			        	
			        </div>
		    	</div>
			   	<div class="row">
			        <div class="col-md-12">
			        	<div class="row receipt_bottom_border">
				        	<div class="col-xs-12">
				            	<strong>B. Penalties</strong>
				            </div>
				        </div>
				       
			        	<table class="table table-hover ">
				           
				            <tbody>
				               	<tr>
				               		<td >Penalty Dues</td><td class="pull-right">Kes. <?php echo number_format($penalty_charge,2);?></td>
				               	</tr>
				            </tbody>
				        </table>
			        </div>
			     </div>
			     <br/>
			    <div class="row">
			    	<div class="col-md-12">
				    	<table class="table table-hover ">
				            <tbody>
				               <tr>
				               	<td><strong> Total Dues A+B </strong> </td><td class="pull-right" style="border-top:1px #000 solid;"> <strong>Kes. <?php echo number_format($total_bill,2);?> </strong> </td>
				               </tr>
				            </tbody>
				        </table>
				    </div>
			    </div>
		       
		        <div class="row" style="border-top:1px #000 solid;">
		       		<div class="col-xs-6 pull-left">
		       			<strong > <?php echo $message_prefix;?>.</strong>
		            </div>
		       </div>
		       <div class="row" style="border-top:1px #000 solid;" >
		       		<div class="col-xs-12 align-center" >
		       			<span style="font-size:10px; " > <?php echo $contacts['company_name'];?> | <?php echo $contacts['location'];?>, <?php echo $contacts['building'];?>, <?php echo $contacts['floor'];?>
		                     E-mail: <?php echo $contacts['email'];?>.<br/> Tel : <?php echo $contacts['phone'];?>
		                     P.O. Box <?php echo $contacts['address'];?> <?php echo $contacts['post_code'];?>, <?php echo $contacts['city'];?></span>
		            </div>
		       </div>
		</div>
		</div>
        
    </body>
    
</html>