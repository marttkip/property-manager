<?php
	$all_leases = $this->leases_model->get_lease_detail($lease_id);
	foreach ($all_leases->result() as $leases_row)
	{
		$lease_id = $leases_row->lease_id;
		$tenant_unit_id = $leases_row->tenant_unit_id;
		$property_name = $leases_row->property_name;
		$property_id = $leases_row->property_id;
		// $units_name = $leases_row->units_name;
		$rental_unit_name = $leases_row->rental_unit_name;
		$tenant_name = $leases_row->tenant_name;
		$lease_start_date = $leases_row->lease_start_date;
		$lease_duration = $leases_row->lease_duration;
		$rent_amount = $leases_row->rent_amount;
		$lease_number = $leases_row->lease_number;
		$arrears_bf = $leases_row->arrears_bf;
		$rent_calculation = $leases_row->rent_calculation;
		$deposit = $leases_row->deposit;
		$deposit_ext = $leases_row->deposit_ext;
		$tenant_phone_number = $leases_row->tenant_phone_number;
		$tenant_national_id = $leases_row->tenant_national_id;
		$lease_status = $leases_row->lease_status;
		$tenant_status = $leases_row->tenant_status;
		$created = $leases_row->created;
		$message_prefix = $leases_row->message_prefix;

		$lease_start_date = date('jS M Y',strtotime($lease_start_date));
		
		// $expiry_date  = date('jS M Y',strtotime($lease_start_date, mktime()) . " + 365 day");
		$expiry_date  = date('jS M Y', strtotime(''.$lease_start_date.'+1 years'));
		
	}

	$today = date('jS F Y H:i a',strtotime(date("Y:m:d h:i:s")));
	
	$served_by = $this->accounts_model->get_personnel($this->session->userdata('personnel_id'));
	// get when the lease was active
	$parent_invoice_date = $this->accounts_model->get_max_invoice_date($lease_id,$invoice_month,$invoice_year);

	// get all the invoiced months
	// var_dump($lease_invoice); die();

	$insurance_arrears  =0;
	$insurance_invoices_bf  =0;
	$total_insurance = 0;

	$common_arrears  =0;
	$common_invoices_bf  =0;
	$total_common = 0;

	$penalty_arrears  =0;
	$penalty_invoices_bf  =0;
	$total_penalty = 0;

	$sinking_arrears  =0;
	$sinking_invoices_bf  =0;
	$total_sinking = 0;

	$water_arrears  =0;
	$water_invoices_bf  =0;
	$total_water = 0;

	$bought_arrears  =0;
	$bought_invoices_bf  =0;
	$total_bought = 0;

	$painting_arrears  =0;
	$painting_invoices_bf  =0;
	$total_painting = 0;

	$total_bill = 0;
	$water_rate =0;
	
	if($lease_invoice->num_rows() > 0)
	{
		$electricity_charge = 0;
		$electricity_bf = 0;
		$fixed_charge = 0;
		$fixed_bf = 0;
		$water_charge = 0;
		$water_bf = 0;
		$penalty_charge =0;
		$penalty_bf =0;
		$borehole = 0;
		$prev_reading = 0;
		$current_reading =0;
		$elec_prev_reading = 0;
		$elec_current_reading =0;
		$water_bill_lamp = 0;
		$elect_bill_lamp = 0;

		$current_fixed_invoice_amount = 0;
		$current_water_invoice_amount = 0;
		$current_penalty_invoice_amount = 0;

// var_dump($lease_invoice->result()); die();
		foreach ($lease_invoice->result() as $key_invoice) {
			# code...
			$invoice_date = $key_invoice->invoice_date;
			$invoice_id = $key_invoice->invoice_id;
			$invoice_type = $key_invoice->invoice_type;
			$billing_schedule_quarter = $key_invoice->billing_schedule_quarter;
			// $invoice_explode = explode('-', $invoice_date);
			// $invoice_year_year = 

			
			$invoice_date_date = date('jS F Y',strtotime($invoice_date));
			
			if($invoice_type == 12)
			{

				$fixed_invoice_amount = $this->accounts_model->get_invoice_tenants_brought_forward($lease_id,$parent_invoice_date,$invoice_type);
				$fixed_paid_amount = $this->accounts_model->get_paid_tenants_brought_forward($lease_id,$parent_invoice_date,$invoice_type);

				$total_brought_forward = $fixed_invoice_amount - $fixed_paid_amount;
				
				$fixed_current_invoice =  $this->accounts_model->get_invoice_tenants_current_forward($lease_id,$parent_invoice_date,$invoice_type);
				$fixed_current_payment =  $this->accounts_model->get_today_tenants_paid_current_forward($lease_id,$parent_invoice_date,$invoice_type);
			
				$total_fixed = $fixed_current_invoice - $fixed_current_payment;
			}


			if($invoice_type == 5)
			{
				// penalty

				$datestring=''.$invoice_date.' first day of last month';
				$dt=date_create($datestring);
				$prev_date = $dt->format('Y-m');
				$prev_array = explode('-', $prev_date);
				$prev_month = $prev_array[1];
				$prev_year = $prev_array[0];


				$penalty_invoice_amount = $this->accounts_model->get_invoice_tenants_brought_forward($lease_id,$parent_invoice_date,$invoice_type);
				$penalty_paid_amount = $this->accounts_model->get_paid_tenants_brought_forward($lease_id,$parent_invoice_date,$invoice_type);

				$total_brought_forward = $penalty_invoice_amount - $penalty_paid_amount;
				
				$penalty_current_invoice =  $this->accounts_model->get_invoice_tenants_current_forward($lease_id,$parent_invoice_date,$invoice_type);
				$penalty_current_payment =  $this->accounts_model->get_today_tenants_paid_current_forward($lease_id,$parent_invoice_date,$invoice_type);
			
				$total_penalty = $penalty_current_invoice - $penalty_current_payment;


				// var_dump($total_penalty); die();

 
			}

			if($invoice_type == 14)
			{
				// penalty


				$variance_invoice_amount = $this->accounts_model->get_invoice_tenants_brought_forward($lease_id,$parent_invoice_date,$invoice_type);
				$variance_paid_amount = $this->accounts_model->get_paid_tenants_brought_forward($lease_id,$parent_invoice_date,$invoice_type);

				$total_brought_forward = $variance_invoice_amount - $variance_paid_amount;
				
				$variance_current_invoice =  $this->accounts_model->get_invoice_tenants_current_forward($lease_id,$parent_invoice_date,$invoice_type);
				$variance_current_payment =  $this->accounts_model->get_today_tenants_paid_current_forward($lease_id,$parent_invoice_date,$invoice_type);
			
				$total_variance = $variance_current_invoice - $variance_current_payment;


				// var_dump($total_penalty); die();

 
			}

			if($invoice_type == 3)
			{
				// penalty


				//  get the amount changed
				$electricity_invoice_amount = $this->accounts_model->get_invoice_tenants_brought_forward($lease_id,$parent_invoice_date,$invoice_type);
				$electricity_paid_amount = $this->accounts_model->get_paid_tenants_brought_forward($lease_id,$parent_invoice_date,$invoice_type);

				$total_brought_forward = $electricity_invoice_amount - $electricity_paid_amount;
				
				$electricity_current_invoice =  $this->accounts_model->get_invoice_tenants_current_forward($lease_id,$parent_invoice_date,$invoice_type);
				$electricity_current_payment =  $this->accounts_model->get_today_tenants_paid_current_forward($lease_id,$parent_invoice_date,$invoice_type);
			
				$total_electricity = $electricity_current_invoice - $electricity_current_payment;


				// var_dump($total_penalty); die();

 
			}

			if($invoice_type == 2)
			{
				// water
				$water_charge = $key_invoice->invoice_amount;
				$water_bf = $key_invoice->arrears_bf;

				$water_invoice_amount = $this->accounts_model->get_invoice_tenants_brought_forward($lease_id,$parent_invoice_date,$invoice_type);
				$water_paid_amount = $this->accounts_model->get_paid_tenants_brought_forward($lease_id,$parent_invoice_date,$invoice_type);

				$total_brought_forward = $water_invoice_amount - $water_paid_amount;
				
				$water_current_invoice =  $this->accounts_model->get_invoice_tenants_current_forward($lease_id,$parent_invoice_date,$invoice_type);
				$water_current_payment =  $this->accounts_model->get_today_tenants_paid_current_forward($lease_id,$parent_invoice_date,$invoice_type);
			
				$total_water = $water_current_invoice - $water_current_payment;
				// var_dump($parent_invoice_date); die();
				// var_dump($invoice_id); die();
				$water_readings = $this->accounts_model->get_water_readings($invoice_id);


				$water_rate = $this->accounts_model->get_bill_property_rate($invoice_type,1,$property_id);

				if($water_readings->num_rows() > 0)
				{
					foreach ($water_readings->result() as $key) {
						# code...
						$prev_reading = $key->prev_reading;
						$current_reading = $key->current_reading;
						$water_rate = $this->accounts_model->get_rate_charged($invoice_type,1,$property_id);

					}
				}
				$current_water = $water_rate * ($current_reading - $prev_reading);
			}
		}
	}
	// echo $total_water;
	

	//  get the amount changed
	$account_invoice_amount = $this->accounts_model->get_invoice_tenants_brought_forward($lease_id,$parent_invoice_date);
	$account_paid_amount = $this->accounts_model->get_paid_tenants_brought_forward($lease_id,$parent_invoice_date);
	$account_todays_payment =  $this->accounts_model->get_today_tenants_paid_current_forward($lease_id,$parent_invoice_date);
	// end of amount changed
	
 
	$balance_checked = $this->accounts_model->get_lease_pardons_month($lease_id,$invoice_year,$invoice_month);
	// var_dump($parent_invoice_date); die();
	$total_brought_forward_account = $account_invoice_amount - $account_paid_amount - $account_todays_payment  ;

	$total_bill = ($total_water + $total_fixed  + $total_electricity + $total_variance + $total_brought_forward_account + $total_penalty) - $balance_checked;

	// var_dump($current_fixed_invoice_amount); die();
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <title><?php echo $contacts['company_name'];?> | INVOICE</title>
        <!-- For mobile content -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" media="all"/>
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css" media="all"/>
        <style type="text/css">
			.receipt_spacing{letter-spacing:0px;}
			.center-align{margin:0 auto; text-align:center;}
			
			.receipt_bottom_border{border-bottom: #888888 medium solid; margin-bottom:1px;}
			.row .col-xs-12 table {
				border:solid #000 !important;
				border-width:1px 0 0 1px !important;
				/*font-size:10px;*/
			}
			.row .col-xs-12 th, .row .col-xs-12 td {
				border:solid #000 !important;
				border-width:0 1px 1px 0 !important;
			}
			.table thead > tr > th, .table tbody > tr > th, .table tfoot > tr > th, .table thead > tr > td, .table tbody > tr > td, .table tfoot > tr > td
			{
				 padding: 2px;
			}
			.align-center
			{
				/*padding: 50px;*/
    			text-align: center;
    			font-weight: bolder;
			}
			
			.row .col-xs-12 .title-item{float:left;width: 130px; font-weight:bold; text-align:right; padding-right: 20px;}
			.title-img{float:left; padding-left:30px;}
			img.logo{max-height:70px; margin:0 auto;}
			.align-right{margin:0 auto; text-align: right !important;}
			.row {
			    margin-left: 0;
			    margin-right: 0;
			}
			.panel-title
			{
				/*font-size: 13px !important;*/
			}
			.table {
			  margin-bottom: 5px;
			  max-width: 100%;
			  width: 100%;
			}
			.table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td{border-top: none !important;}
			.table {
			  margin-bottom: 5px;
			  max-width: 100%;
			  width: 100%;
			}
			.row {
  				margin-left: 0px !important;
    			margin-right: 0px !important;
			}
			body
			{
				font-size: 10px;
			}
			p
			{
				line-height:6px;
			}
		</style>
    </head>
    <body class="receipt_spacing">
        <div class="row">
	        <div class="col-xs-9  col-xs-offset-1">
		    	<div class="receipt_bottom_border">
		        	<table class="table table-condensed">
		                <tr>
		                    <th><h4><?php echo $contacts['company_name'];?> </h4></th>
		                    <th class="align-right">
		                        <img src="<?php echo base_url().'assets/logo/'.$contacts['logo'];?>" alt="<?php echo $contacts['company_name'];?>" class="img-responsive logo" style="float:right;"/>
		                    </th>
		                </tr>
		            </table>
		        </div>
		    	
		        
		        <!-- Patient Details -->
		    	 <div class="row receipt_bottom_border" style="margin-bottom: 10px;margin-top: 5px; font-size: 10px;">
		        
		            <div class="col-xs-6 ">
		                
		                <p><strong> Name </strong> : <?php echo $tenant_name;?></p>
		                <p><strong> Phone </strong> :<?php echo $tenant_phone_number;?></p>
		                <p><strong> Unit </strong> : <?php echo $property_name;?> - <?php echo $rental_unit_name;?></p>

		               
		            </div>
		            <div class="col-xs-6">
		             	<p><strong> Invoice # </strong> : <?php echo $receipt_number='June 2017';?></p>
		             	<p><strong> Invoice Date </strong> : <?php echo date('jS M Y',strtotime($invoice_date));?></p>
		             	<p><strong> Billing for  </strong> : <?php echo $billing_schedule_quarter;?></p>
		            </div>
		        </div>
		        <div class="row">
			        <div class="row">
				        	<div class="col-xs-4 receipt_bottom_border">
				        		<strong>A. WATER CHARGE </strong> 
				        	</div>
				    </div>
			        <div class="col-xs-12">
			        	<div class="col-xs-4">
			        		Balance B/F
			        	</div>
			        	<div class="col-xs-4">
			        	</div>
			        	<div class="col-xs-4 pull-right">
			        		<strong>Kes. <?php echo number_format($total_brought_forward_account - $balance_checked,2);?> </strong>
			        	</div>
			        	
			        </div>
			        <div class="col-xs-12">
			        	<div class="col-xs-4">
			        		Current Reading
			        	</div>
			        	<div class="col-xs-4">
			        		<?php echo $current_reading;?>
			        	</div>
			        	<div class="col-xs-4 pull-right">
			        		
			        	</div>
			        	
			        </div>
			        <div class="col-xs-12">
			        	<div class="col-xs-4">
			        		Previous Reading
			        	</div>
			        	<div class="col-xs-4">
			        		<?php echo $prev_reading;?>
			        	</div>
			        	<div class="col-xs-4 pull-right">
			        		
			        	</div>
			        </div>
			        <div class="col-xs-12">
			        	<div class="col-xs-4">
			        		Units Consumed
			        	</div>
			        	<div class="col-xs-4" style="border-top:2px #000 solid !important;">
			        		<?php echo $current_reading - $prev_reading;?>
			        	</div>
			        	<div class="col-xs-4 pull-right">
			        		
			        	</div>
			        </div>

			        <div class="col-xs-12">
			        	<div class="col-xs-4">
			        		Rate
			        	</div>
			        	<div class="col-xs-4" >
			        		<?php echo $water_rate;?>
			        	</div>
			        	<div class="col-xs-4 pull-right">
			        		
			        	</div>
			        </div>

			        <div class="col-xs-12">
			        	<div class="col-xs-4">
			        		Current Billing
			        	</div>
			        	<div class="col-xs-4" >
			        	</div>
			        	<div class="col-xs-4 pull-right" style="border-top:2px #000 solid !important;">
			        		<strong> Kes. <?php echo number_format($water_rate * ($current_reading - $prev_reading) ,2);?></strong>
			        	</div>
			        </div>
				        
			   	</div>

			   	 <div class="row">
		        	<div class="row">
			        	<div class="col-xs-4 receipt_bottom_border">
			        		<strong>B. ELECTRICITY </strong> 
			        	</div>
			        </div>

			        <div class="col-xs-12">
			        	<div class="col-xs-4">
			        		(Bill split based on %age of water units consumed)
			        	</div>
			        	<div class="col-xs-4" >
			        	</div>
			        	<div class="col-xs-4 pull-right">
			        		<strong> Kes. <?php echo number_format($total_electricity,2);?></strong>
			        	</div>
			        </div>
				        
			   	</div>

			   	<div class="row">
		        	 <div class="row">
			        	<div class="col-xs-4 receipt_bottom_border">
			        		<strong>C. FIXED CHARGE </strong> 
			        	</div>
			        </div>
			       	<div class="col-xs-12">
			        	<div class="col-xs-4">
			        	</div>
			        	<div class="col-xs-4" >
			        	</div>
			        	<div class="col-xs-4 pull-right">
			        		<strong> Kes. <?php echo number_format($total_fixed,2);?></strong>
			        	</div>
			        </div>
			   	</div>
			   	<div class="row">
		        	 <div class="row">
			        	<div class="col-xs-4 receipt_bottom_border">
			        		<strong>D. GAP </strong> 
			        	</div>
			        </div>
			       	<div class="col-xs-12">
			        	<div class="col-xs-4">
			        		(NWSC units consumed - actual meter reading)
			        	</div>
			        	<div class="col-xs-4" >
			        	</div>
			        	<div class="col-xs-4 pull-right">
			        		<strong> Kes. <?php echo number_format($total_variance,2);?></strong>
			        	</div>
			        </div>
			   	</div>
			   	<div class="row">
			        <div class="row">
			        	<div class="col-xs-4 receipt_bottom_border">
			        		<strong>E. PENALTIES </strong> 
			        	</div>
			        </div>
			        <div class="col-xs-12">
			        	<div class="col-xs-4">
			        	</div>
			        	<div class="col-xs-4" >
			        	</div>
			        	<div class="col-xs-4 pull-right">
			        		<strong> Kes. <?php echo number_format($total_penalty,2);?></strong>
			        	</div>
			        </div>
			   	</div>

			   	<div class="row">
			        <div class="row">
			        	<div class="col-xs-4 receipt_bottom_border">
			        		<strong>TOTAL DUES </strong> 
			        	</div>
			        </div>
			        <div class="col-xs-12">
			        	<div class="col-xs-4">
			        	</div>
			        	<div class="col-xs-4" >
			        	</div>
			        	<div class="col-xs-4 pull-right " style="border-top:2px #000 solid !important;border-bottom:2px #000 solid !important; margin-bottom:5px;">
			        		<strong> Kes. <?php echo number_format($total_bill,2);?></strong>
			        	</div>
			        </div>
			   	</div>
		       
		       <div class="row" style="border-top:1px #000 solid;">
		       		<div class="col-xs-12 pull-left">
		       			<strong >Water arrears and current bill should be paid by the 20th of the month failure willlead to water disconnection <br><?php echo $message_prefix;?></strong>
		            </div>
		       </div>
		       <div class="row" >
		       		<div class="col-xs-12 align-center" style="border-top:1px #000 solid;">
		       			<span style="font-size:10px; " > <?php echo $contacts['company_name'];?> | <?php echo $contacts['location'];?>, <?php echo $contacts['building'];?>, <?php echo $contacts['floor'];?>
		                     E-mail: <?php echo $contacts['email'];?>.<br/> Tel : <?php echo $contacts['phone'];?>
		                     P.O. Box <?php echo $contacts['address'];?> <?php echo $contacts['post_code'];?>, <?php echo $contacts['city'];?></span>
		            </div>
		       </div>
		       
		</div>
		</div>
        
    </body>
    
</html>