<?php
$items = '';
$total_amount = 0;
foreach ($query->result() as $leases_row)
{
	$rental_unit_id = $leases_row->rental_unit_id;
	$rental_unit_name = $leases_row->rental_unit_name;
	$property_name = $leases_row->property_name;
	$amount = 450;
	$total_amount = $total_amount + $amount;
	$items .= '
				<tr>
               		<td>'.$rental_unit_name.'</td> <td class="pull-right">Kes. '.$amount.' </td>
               </tr>
				';

	
}


?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <title><?php echo $contacts['company_name'];?> | INVOICE</title>
        <!-- For mobile content -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" media="all"/>
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css" media="all"/>
        <style type="text/css">
			.receipt_spacing{letter-spacing:0px;}
			.center-align{margin:0 auto; text-align:center;}
			
			.receipt_bottom_border{border-bottom: #888888 medium solid; margin-bottom:1px;}
			.row .col-xs-12 table {
				border:solid #000 !important;
				border-width:1px 0 0 1px !important;
				/*font-size:10px;*/
			}
			.row .col-xs-12 th, .row .col-xs-12 td {
				border:solid #000 !important;
				border-width:0 1px 1px 0 !important;
			}
			.table thead > tr > th, .table tbody > tr > th, .table tfoot > tr > th, .table thead > tr > td, .table tbody > tr > td, .table tfoot > tr > td
			{
				 padding: 2px;
			}
			.align-center
			{
				/*padding: 50px;*/
    			text-align: center;
    			font-weight: bolder;
			}
			
			.row .col-xs-12 .title-item{float:left;width: 130px; font-weight:bold; text-align:right; padding-right: 20px;}
			.title-img{float:left; padding-left:30px;}
			img.logo{max-height:70px; margin:0 auto;}
			.align-right{margin:0 auto; text-align: right !important;}
			.row {
			    margin-left: 0;
			    margin-right: 0;
			}
			.panel-title
			{
				/*font-size: 13px !important;*/
			}
			.table {
			  margin-bottom: 5px;
			  max-width: 100%;
			  width: 100%;
			}
			.table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td{border-top: none !important;}
		</style>
    </head>
    <body class="receipt_spacing">
        <div class="row">
	        <div class="col-xs-9  col-xs-offset-1">
		    	<div class="receipt_bottom_border">
		        	<table class="table table-condensed">
		                <tr>
		                    <th><h4><?php echo $contacts['company_name'];?> </h4></th>
		                    <th class="align-right">
		                        <img src="<?php echo base_url().'assets/logo/'.$contacts['logo'];?>" alt="<?php echo $contacts['company_name'];?>" class="img-responsive logo" style="float:right;"/>
		                    </th>
		                </tr>
		            </table>
		        </div>
		    	
		        
		        <!-- Patient Details -->
		    	<div class="row receipt_bottom_border" style="margin-bottom: 10px;">
		        
		            <div class="col-xs-6">
		                <h2 class="panel-title">Personal Information</h2>
		                <table class="table table-hover">
		                    <tbody>
		                        <tr><td><span>Name :</span></td><td><?php echo 'BECAM PROPERTIES';?></td></tr>
		                        <tr><td><span>Phone :</span></td><td><?php echo '715770467';?></td></tr>
		                        <tr><td><span>Property :</span></td><td><?php echo $property_name;?></td></tr>
		                        
		                    </tbody>
		                </table>
		            </div>
		             <div class="col-xs-6">
		                <h2 class="panel-title">Invoice</h2>
		                <table class="table table-hover">
		                    <tbody>
		                    	<!-- <tr><td><span>Invoice# :</span></td><td><?php echo date('jS M Y',strtotime(date('Y-m-d')));?></td></tr> -->
		                        <tr><td><span>Date :</span></td><td><?php echo date('jS M Y',strtotime(date('Y-m-d')));?></td></tr>
		                        
		                    </tbody>
		                </table>
		            </div>
		        </div>
		        <div class="row">
			        <div class="col-md-12">
			        	 <div class="row receipt_bottom_border">
				        	<div class="col-xs-12">
				            	<strong>A. WATER DUES</strong>
				            </div>
				        </div>
				        <table class="table table-hover" >
				            <tbody>
				            	<?php echo $items;?>
				            </tbody>
				        </table>
		               	  <table class="table table-hover" >
				            <tbody>
				               <tr>
				               	<td colspan="2" >Balance B/F</td><td class="pull-right">Kes. <?php echo "0";?> </td>
				               </tr>
				               <tr >
				               	<td colspan="2">Water Dues  </td><td class="pull-right" style="border-top:2px #000 solid !important;">Kes. <?php echo $total_amount;?></td>
				               </tr>
				            </tbody>
				        </table>
			        </div>
			   	</div>
			   	<div class="row">
			        <div class="col-md-12">
			        	<div class="row receipt_bottom_border">
				        	<div class="col-xs-12">
				            	<strong>B. Penalties</strong>
				            </div>
				        </div>
				       
			        	<table class="table table-hover">
				           
				            <tbody>
				               	<tr>
				               		<td colspan="2" class="col-md-6">Penalty Dues</td><td class="pull-right">Kes. <?php echo number_format(0);?></td>
				               	</tr>
				            </tbody>
				        </table>
			        </div>
			     </div>
			    <div class="row">
			    	<div class="col-md-12">
				    	<table class="table table-hover">
				            <tbody>
				               <tr>
				               	<td colspan="2" ><strong> Total Dues A+B </strong> </td><td class="pull-right" style="border-top:2px #000 solid !important;border-bottom:2px #000 solid !important;"><strong> Kes. <?php echo $total_amount;?></strong> </td>
				               </tr>
				            </tbody>
				        </table>
				    </div>
			    </div>
		       
		       <div class="row" style="border-top:1px #000 solid;">
		       		<div class="col-xs-6 pull-left">
		       			<strong > MPESA Equity Paybill no 247247 <br/> A/C No. 0550262356997.</strong>
		            </div>
		       </div>
		       <div class="row" >
		       		<div class="col-xs-12 align-center" style="border-top:1px #000 solid;">
		       			<span style="font-size:10px; " > <?php echo $contacts['company_name'];?> | <?php echo $contacts['location'];?>, <?php echo $contacts['building'];?>, <?php echo $contacts['floor'];?>
		                     E-mail: <?php echo $contacts['email'];?>.<br/> Tel : <?php echo $contacts['phone'];?>
		                     P.O. Box <?php echo $contacts['address'];?> <?php echo $contacts['post_code'];?>, <?php echo $contacts['city'];?></span>
		            </div>
		       </div>
		       
		</div>
		</div>
        
    </body>
    
</html>