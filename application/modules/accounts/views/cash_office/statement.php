<?php
	$all_leases = $this->leases_model->get_lease_detail($lease_id);
	foreach ($all_leases->result() as $leases_row)
	{
		$lease_id = $leases_row->lease_id;
		$tenant_unit_id = $leases_row->tenant_unit_id;
		$property_name = $leases_row->property_name;
		// $units_name = $leases_row->units_name;
		$rental_unit_name = $leases_row->rental_unit_name;
		$tenant_name = $leases_row->tenant_name;
		$lease_start_date = $leases_row->lease_start_date;
		$lease_duration = $leases_row->lease_duration;
		$rent_amount = $leases_row->rent_amount;
		$lease_number = $leases_row->lease_number;
		$arrears_bf = $leases_row->arrears_bf;
		$rent_calculation = $leases_row->rent_calculation;
		$deposit = $leases_row->deposit;
		$deposit_ext = $leases_row->deposit_ext;
		$tenant_phone_number = $leases_row->tenant_phone_number;
		$tenant_national_id = $leases_row->tenant_national_id;
		$lease_status = $leases_row->lease_status;
		$tenant_status = $leases_row->tenant_status;
		$created = $leases_row->created;

		$lease_start_date = date('jS M Y',strtotime($lease_start_date));
		
		// $expiry_date  = date('jS M Y',strtotime($lease_start_date, mktime()) . " + 365 day");
		$expiry_date  = date('jS M Y', strtotime(''.$lease_start_date.'+1 years'));
		
	}

	$today = date('jS F Y H:i a',strtotime(date("Y:m:d h:i:s")));
	
	$served_by = $this->accounts_model->get_personnel($this->session->userdata('personnel_id'));
	// get when the lease was active

	// get all the invoiced months

	$bills = $this->accounts_model->get_statement_items($lease_id);
	// var_dump($bills); die();
	$payment_result = '';
	if($bills->num_rows() > 0)
	{
		$x =0;
		$total_invoice = 0;
		$total_paid = 0;
		foreach ($bills->result() as $key_result) {
			# code...
			$ptype = $key_result->PTYPE;
			$invoice_type_id = $key_result->invoice_type_id;
			
			if($invoice_type_id > 0)
			{
				$invoice_type_name = $this->accounts_model->get_invoice_type_name($invoice_type_id);
				$description = 'Invoice for '.$invoice_type_name.' Due';
			}
			else{
				# code...
				$description = 'Payment';
			}
			
			
			$date = $key_result->date;
			$invoice_amount = '';
			$paid_amount = '';
			$invoiced = 0;
			$paid = 0;
			if($ptype == 'INVOICE')
			{
				$amount = $key_result->amount;
				$invoice_amount = $amount;
			}
			else
			{
				$amount = $key_result->amount;
				$paid_amount = $amount;
			}

			if($invoice_amount == '')
			{
				$invoice = 0;
			}
			else
			{
				$invoiced = $invoice_amount;
			}
			if($paid_amount == '')
			{
				$paid = 0;
			}
			else
			{
				$paid = $paid_amount;
			}
			$total_invoice = $total_invoice + $invoiced;
			$total_paid = $total_paid + $paid;
			$balance = $total_invoice - $total_paid;
			$x++;
			$payment_result .=
							  '
							  	<tr>
							  		<td>'.$x.'</td>
							  		<td>'.$date.'</td>
							  		<td>'.$description.'</td>
							  		<td>'.$invoice_amount.'</td>
							  		<td>'.$paid_amount.'</td>
							  		<td>'.number_format($balance).'</td>
							  	</tr>
							  ';

		}

	}

	
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <title><?php echo $contacts['company_name'];?> | STATEMENT</title>
        <!-- For mobile content -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" media="all"/>
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css" media="all"/>
        <style type="text/css">
			.receipt_spacing{letter-spacing:0px; font-size: 12px;}
			.center-align{margin:0 auto; text-align:center;}
			
			.receipt_bottom_border{border-bottom: #888888 medium solid; margin-bottom:20px;}
			.row .col-xs-12 table {
				border:solid #000 !important;
				border-width:1px 0 0 1px !important;
				font-size:10px;
			}
			.row .col-xs-12 th, .row .col-xs-12 td {
				border:solid #000 !important;
				border-width:0 1px 1px 0 !important;
			}
			.table thead > tr > th, .table tbody > tr > th, .table tfoot > tr > th, .table thead > tr > td, .table tbody > tr > td, .table tfoot > tr > td
			{
				 padding: 2px;
			}
			
			.row .col-xs-12 .title-item{float:left;width: 130px; font-weight:bold; text-align:right; padding-right: 20px;}
			.title-img{float:left; padding-left:30px;}
			img.logo{max-height:70px; margin:0 auto;}
			.align-right{margin:0 auto; text-align: right !important;}
		</style>
    </head>
    <body class="receipt_spacing">
    	<div class="row">
    		<div class="col-md-12">
		    	<div class="receipt_bottom_border">
		        	<table class="table table-condensed">
		                <tr>
		                    <th><h4><?php echo $contacts['company_name'];?> </h4></th>
		                    <th class="align-right">
		                        <img src="<?php echo base_url().'assets/logo/'.$contacts['logo'];?>" alt="<?php echo $contacts['company_name'];?>" class="img-responsive logo" style="float:right;"/>
		                    </th>
		                </tr>
		            </table>
		        </div>
		    	
		        
		        <!-- Patient Details -->
		    	<div class="row receipt_bottom_border" style="margin-bottom: 10px;">
		        
		            <div class="col-xs-6">
		                <h2 class="panel-title">Tenant's Details</h2>
		                <table class="table table-hover table-bordered">
		                    <tbody>
		                        <tr><td><span>Tenant Name :</span></td><td><?php echo $tenant_name;?></td></tr>
		                        <tr><td><span>Tenant Phone :</span></td><td><?php echo $tenant_phone_number;?></td></tr>
		                        <tr><td><span>Property Name :</span></td><td><?php echo $property_name;?></td></tr>
		                        <tr><td><span>Hse No. :</span></td><td><?php echo $rental_unit_name;?></td></tr>
		                        
		                    </tbody>
		                </table>
		            </div>
		             <div class="col-xs-6">
		                <h2 class="panel-title">Statement</h2>
		                <table class="table table-hover table-bordered">
		                    <tbody>
		                        <tr><td><span>Date :</span></td><td><?php echo date('jS M Y',strtotime(date('Y-m-d')));?></td></tr>
		                        
		                    </tbody>
		                </table>
		            </div>
		            
		        </div>
		        
		    	<div class="row receipt_bottom_border">
		        	<div class="col-xs-12 center-align">
		            	<strong>INDIVIDUAL STATEMENT</strong>
		            </div>
		        </div>

		    
		    	<table class="table table-hover table-bordered table-striped">
		            <thead>
		                <tr>
		                  <th>#</th>
		                  <th>Date </th>
		                  <th>Description</th>
		                  <th>Charges (Kes)</th>
		                  <th>Payments (Kes)</th>
		                  <th>Balance (Kes)</th>
		                </tr>
		            </thead>
		            <tbody>
		                <?php echo $payment_result;?>
		            </tbody>
		        </table>
		        
		        
		        <table class="table table-condensed">
		            <tr>
		                <th class="align-right">
		                    <?php echo $contacts['company_name'];?> | <?php echo $contacts['location'];?>, <?php echo $contacts['building'];?>, <?php echo $contacts['floor'];?><br/>
		                     E-mail: <?php echo $contacts['email'];?>. Tel : <?php echo $contacts['phone'];?><br/>
		                     P.O. Box <?php echo $contacts['address'];?> <?php echo $contacts['post_code'];?>, <?php echo $contacts['city'];?><br/>
		                </th>
		            </tr>
		        </table>
		</div>
		</div>
        
    </body>
    
</html>