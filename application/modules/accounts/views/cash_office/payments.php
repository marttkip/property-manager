<?php
$all_leases = $this->leases_model->get_lease_detail($lease_id);
	foreach ($all_leases->result() as $leases_row)
	{
		$lease_id = $leases_row->lease_id;
		$tenant_id = $leases_row->tenant_id;
		$tenant_unit_id = $leases_row->tenant_unit_id;
		$property_name = $leases_row->property_name;
		$property_id = $leases_row->property_id;
		$rental_unit_name = $leases_row->rental_unit_name;
		$tenant_name = $leases_row->tenant_name;
		$lease_start_date = $leases_row->lease_start_date;
		$lease_duration = $leases_row->lease_duration;
		$rent_amount = $leases_row->rent_amount;
		$lease_number = $leases_row->lease_number;
		$arreas_bf = $leases_row->arrears_bf;
		$rent_calculation = $leases_row->rent_calculation;
		$deposit = $leases_row->deposit;
		$deposit_ext = $leases_row->deposit_ext;
		$tenant_phone_number = $leases_row->tenant_phone_number;
		$tenant_national_id = $leases_row->tenant_national_id;
		$lease_status = $leases_row->lease_status;
		$tenant_status = $leases_row->tenant_status;
		$created = $leases_row->created;

		$lease_start_date = date('jS M Y',strtotime($lease_start_date));

		// $expiry_date  = date('jS M Y',strtotime($lease_start_date, mktime()) . " + 365 day");
		$expiry_date  = date('jS M Y', strtotime(''.$lease_start_date.'+1 years'));
		//create deactivated status display
		if($lease_status == 0)
		{
			$status = '<span class="label label-default"> Deactivated</span>';

			$button = '';
			$delete_button = '';
		}
		//create activated status display
		else if($lease_status == 1)
		{
			$status = '<span class="label label-success">Active</span>';
			$button = '<td><a class="btn btn-default" href="'.site_url().'deactivate-rental-unit/'.$lease_id.'" onclick="return confirm(\'Do you want to deactivate '.$lease_number.'?\');" title="Deactivate '.$lease_number.'"><i class="fa fa-thumbs-down"></i></a></td>';
			$delete_button = '<td><a href="'.site_url().'deactivate-rental-unit/'.$lease_id.'" class="btn btn-sm btn-danger" onclick="return confirm(\'Do you really want to delete '.$lease_number.'?\');" title="Delete '.$lease_number.'"><i class="fa fa-trash"></i></a></td>';

		}

		//create deactivated status display
		if($tenant_status == 0)
		{
			$status_tenant = '<span class="label label-default">Deactivated</span>';
		}
		//create activated status display
		else if($tenant_status == 1)
		{
			$status_tenant = '<span class="label label-success">Active</span>';
		}
	}




?>


<?php

// $tenant_response = $this->accounts_model->get_rent_and_service_charge($lease_id);

// $grand_rent_bill = $this->accounts_model->get_cummulated_balance($lease_id,1);
// $grand_water_bill = $this->accounts_model->get_cummulated_balance($lease_id,2);
// $grand_electricity_bill = $this->accounts_model->get_cummulated_balance($lease_id,3);
// $grand_service_charge_bill = $this->accounts_model->get_cummulated_balance($lease_id,4);
// $grand_penalty_bill = $this->accounts_model->get_cummulated_balance($lease_id,5);






?>

<input type="hidden" name="tenant_id" id="tenant_id" value="<?php echo $tenant_id?>">
<input type="hidden" name="tenant_unit_id" id="tenant_unit_id" value="<?php echo $tenant_unit_id?>">
<input type="hidden" name="lease_id" id="lease_id" value="<?php echo $lease_id?>">
<div class="box box-danger">
    <!-- <div class="box-header">
      <h3 class="box-title">Loading state</h3>
    </div> -->
    <div class="box-body">
		<div class="nav-tabs-custom">
		    <ul class="nav nav-tabs">
		      <li class="active"><a href="#tab_1" data-toggle="tab" onclick="get_tenant_summary()"><?php echo $tenant_name;?> Summary </a></li>
		      <li><a href="#tab_2" data-toggle="tab" onclick="get_lease_details()">Lease Detail</a></li>
		      <li><a href="#tab_3" data-toggle="tab" onclick="get_invoices_list()">Invoices</a></li>
		      <li><a href="#tab_4" data-toggle="tab" onclick="get_pardons_list()">Pardons</a></li>
		      <li><a href="#tab_5" data-toggle="tab" onclick="get_payments_list();">Payments</a></li>
		      <li><a href="#tab_6" data-toggle="tab" onclick="get_tenant_statement()">Tenant Statement</a></li>
		      <!-- <li><a href="#tab_7" data-toggle="tab" onclick="get_chats()">Chat</a></li> -->
		      <li class="pull-right"><a href="<?php echo site_url().'accounts/tenants-accounts'?>" class="text-muted"><i class="fa fa-arrow-left"></i> Back to Tenants Lists</a></li>
		    </ul>
		    <div class="tab-content">
		      <div class="tab-pane active" id="tab_1">
		      		<div id="tenant-summary"></div>
		      </div>
		      <div class="tab-pane" id="tab_2">
		      		<div id="lease-detail"></div>
		      </div>
		      <div class="tab-pane" id="tab_3">
		      	 	<div id="invoices-list"></div>
		      </div>
		      <div class="tab-pane" id="tab_4">
		      		<div id="pardons-list"></div>
		      </div>
		      <!-- /.tab-pane -->
		      <div class="tab-pane" id="tab_5">

		      		<div id="payments-list"></div>
		      </div>
		      <!-- /.tab-pane -->
		      <div class="tab-pane" id="tab_6">
		        	 <div id="tenant-statement"></div>
		      </div>
		      <!-- /.tab-pane -->
		      <!-- <div class="tab-pane" id="tab_7">

		      		<div id="tenant-chat"></div>

		      </div> -->
		    </div>
		    <!-- /.tab-content -->

		</div>
		<div class="overlay" id="overlay-loader" style="display: none" >
		  <i class="fa fa-refresh fa-spin"></i>
		</div>
	</div>




  <!-- END OF ROW -->
<script type="text/javascript">
 // 	$(document).ready(function(){


	// });
	$(function() {
		get_tenant_summary();
	});
  	// payments funcations

  	function display_payment_model()
  	{
  		$('#modal-defaults').modal('show');
  		$('#datepicker').datepicker({
	      autoclose: true,
	      format: 'yyyy-mm-dd',
	    })
  	}
  	function display_pardon_model()
  	{
  		$('#modal-pardons').modal('show');
  		$('#datepicker').datepicker({
	      autoclose: true,
	      format: 'yyyy-mm-dd',
	    })
  	}

   function get_payments_list()
   {
	    var lease_id = document.getElementById("lease_id").value;
	    var tenant_id = document.getElementById("tenant_id").value;
	    var tenant_unit_id = document.getElementById("tenant_unit_id").value;
		  var myTarget3 = document.getElementById("overlay-loader");
      	 myTarget3.style.display = 'block';
	     var XMLHttpRequestObject = false;

	      if (window.XMLHttpRequest) {

	          XMLHttpRequestObject = new XMLHttpRequest();
	      }

	      else if (window.ActiveXObject) {
	          XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
	      }

	      var config_url = document.getElementById("config_url").value;
	      var url = config_url+"accounts/display_lease_payments/"+lease_id+"/"+tenant_id+"/"+tenant_unit_id;
	      // alert(url);
	      if(XMLHttpRequestObject) {

	          XMLHttpRequestObject.open("GET", url);

	          XMLHttpRequestObject.onreadystatechange = function(){

	              if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

	                  document.getElementById("payments-list").innerHTML=XMLHttpRequestObject.responseText;
	              }
	          }

	          XMLHttpRequestObject.send(null);
	      }
	       myTarget3.style.display = 'none';
	}


	// pardons functions

	function get_pardons_list()
    {
	    var lease_id = document.getElementById("lease_id").value;
	    var tenant_id = document.getElementById("tenant_id").value;
	    var tenant_unit_id = document.getElementById("tenant_unit_id").value;
	    var myTarget3 = document.getElementById("overlay-loader");
      	myTarget3.style.display = 'block';
	     var XMLHttpRequestObject = false;

	      if (window.XMLHttpRequest) {

	          XMLHttpRequestObject = new XMLHttpRequest();
	      }

	      else if (window.ActiveXObject) {
	          XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
	      }

	      var config_url = document.getElementById("config_url").value;
	      var url = config_url+"accounts/display_lease_pardons/"+lease_id+"/"+tenant_id+"/"+tenant_unit_id;
	      // alert(url);
	      if(XMLHttpRequestObject) {

	          XMLHttpRequestObject.open("GET", url);

	          XMLHttpRequestObject.onreadystatechange = function(){

	              if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

	                  document.getElementById("pardons-list").innerHTML=XMLHttpRequestObject.responseText;
	              }
	          }

	          XMLHttpRequestObject.send(null);
	      }
	      myTarget3.style.display = 'none';
	}



	// invoices lists

	function get_invoices_list()
    {
	    var lease_id = document.getElementById("lease_id").value;
	    var tenant_id = document.getElementById("tenant_id").value;
	    var tenant_unit_id = document.getElementById("tenant_unit_id").value;
	    var myTarget3 = document.getElementById("overlay-loader");
      	myTarget3.style.display = 'block';

	     var XMLHttpRequestObject = false;

	      if (window.XMLHttpRequest) {

	          XMLHttpRequestObject = new XMLHttpRequest();
	      }

	      else if (window.ActiveXObject) {
	          XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
	      }

	      var config_url = document.getElementById("config_url").value;
	      var url = config_url+"accounts/display_lease_invoices/"+lease_id+"/"+tenant_id+"/"+tenant_unit_id;
	      // alert(url);
	      if(XMLHttpRequestObject) {

	          XMLHttpRequestObject.open("GET", url);

	          XMLHttpRequestObject.onreadystatechange = function(){

	              if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

	                  document.getElementById("invoices-list").innerHTML=XMLHttpRequestObject.responseText;
	              }
	          }

	          XMLHttpRequestObject.send(null);
	      }
	      myTarget3.style.display = 'none';
	}


	// tenant summary lists
	function get_tenant_summary()
    {
	    var lease_id = document.getElementById("lease_id").value;
	    var tenant_id = document.getElementById("tenant_id").value;
	    var tenant_unit_id = document.getElementById("tenant_unit_id").value;
	    var myTarget3 = document.getElementById("overlay-loader");
      	myTarget3.style.display = 'block';

	     var XMLHttpRequestObject = false;

	      if (window.XMLHttpRequest) {

	          XMLHttpRequestObject = new XMLHttpRequest();
	      }

	      else if (window.ActiveXObject) {
	          XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
	      }

	      var config_url = document.getElementById("config_url").value;
	      var url = config_url+"accounts/display_tenant_summary/"+lease_id+"/"+tenant_id+"/"+tenant_unit_id;
	      // alert(url);
	      if(XMLHttpRequestObject) {

	          XMLHttpRequestObject.open("GET", url);

	          XMLHttpRequestObject.onreadystatechange = function(){

	              if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

	                  document.getElementById("tenant-summary").innerHTML=XMLHttpRequestObject.responseText;
	              }
	          }

	          XMLHttpRequestObject.send(null);
	      }
	      myTarget3.style.display = 'none';
	}
	// get tenant statement
	function get_tenant_statement()
    {
	    var lease_id = document.getElementById("lease_id").value;
	    var tenant_id = document.getElementById("tenant_id").value;
	    var tenant_unit_id = document.getElementById("tenant_unit_id").value;
	   	var myTarget3 = document.getElementById("overlay-loader");
      	myTarget3.style.display = 'block';
	     var XMLHttpRequestObject = false;

	      if (window.XMLHttpRequest) {

	          XMLHttpRequestObject = new XMLHttpRequest();
	      }

	      else if (window.ActiveXObject) {
	          XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
	      }

	      var config_url = document.getElementById("config_url").value;
	      var url = config_url+"accounts/display_tenant_statement/"+lease_id+"/"+tenant_id+"/"+tenant_unit_id;
	      // alert(url);
	      if(XMLHttpRequestObject) {

	          XMLHttpRequestObject.open("GET", url);

	          XMLHttpRequestObject.onreadystatechange = function(){

	              if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

	                  document.getElementById("tenant-statement").innerHTML=XMLHttpRequestObject.responseText;
	              }
	          }

	          XMLHttpRequestObject.send(null);
	      }
	      myTarget3.style.display = 'none';
	}

	function get_lease_details()
    {
	    var lease_id = document.getElementById("lease_id").value;
	    var tenant_id = document.getElementById("tenant_id").value;
	    var tenant_unit_id = document.getElementById("tenant_unit_id").value;
	   	var myTarget3 = document.getElementById("overlay-loader");

      	myTarget3.style.display = 'block';
	     var XMLHttpRequestObject = false;

	      if (window.XMLHttpRequest) {

	          XMLHttpRequestObject = new XMLHttpRequest();
	      }

	      else if (window.ActiveXObject) {
	          XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
	      }

	      var config_url = document.getElementById("config_url").value;
	      var url = config_url+"accounts/display_lease_detail/"+lease_id+"/"+tenant_id+"/"+tenant_unit_id;
	      // alert(url);
	      if(XMLHttpRequestObject) {

	          XMLHttpRequestObject.open("GET", url);

	          XMLHttpRequestObject.onreadystatechange = function(){

	              if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

	                  document.getElementById("lease-detail").innerHTML=XMLHttpRequestObject.responseText;
	              }
	          }

	          XMLHttpRequestObject.send(null);
	      }
	      myTarget3.style.display = 'none';
	}

	$(document).on("submit","form#add_payments",function(e)
	{
		e.preventDefault();

		var form_data = new FormData(this);

		var config_url = $('#config_url').val();
		var lease_id = document.getElementById("lease_id").value;
		var tenant_unit_id = document.getElementById("tenant_unit_id").value;

		var url = config_url+"accounts/make_payments/"+tenant_unit_id+"/"+lease_id;

		$.ajax({
		type:'POST',
		url: url,
		data:form_data,
		dataType: 'json',
		processData: false,
		contentType: false,
		success:function(data){
		  // var data = jQuery.parseJSON(data);

		  if(data.status == "success")
			{
				$('#modal-defaults').modal('hide');
				 get_payments_list();
			}
			else
			{
				alert('Please ensure you have added included all the items');
			}

		},
		error: function(xhr, status, error) {
		alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

		}
		});
	});
	$(document).on("submit","form#add_pardons",function(e)
	{
		e.preventDefault();

		var form_data = new FormData(this);
		var config_url = $('#config_url').val();
		var lease_id = document.getElementById("lease_id").value;
		var tenant_unit_id = document.getElementById("tenant_unit_id").value;

		var url = config_url+"accounts/create_pardon/"+tenant_unit_id+"/"+lease_id;

		$.ajax({
		type:'POST',
		url: url,
		data:form_data,
		dataType: 'text',
		processData: false,
		contentType: false,
		success:function(data){
		  var data = jQuery.parseJSON(data);

		  if(data.message == "success")
			{
				$('#modal-pardons').modal('hide');
				 get_pardons_list();
			}
			else
			{
				alert('Please ensure you have added included all the items');
			}

		},
		error: function(xhr, status, error) {
		alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);

		}
		});
	});

	function delete_invoice(lease_id,invoice_id)
	{
		// var res = confirm('Do you want to remove this invoice ?');
		// if(res)
		// {

			var config_url = $('#config_url').val();
			// var lease_id = document.getElementById("lease_id").value;
			// var tenant_unit_id = document.getElementById("tenant_unit_id").value;

			var url = config_url+"accounts/delete_invoice/"+lease_id+"/"+invoice_id;

			$.ajax({
			type:'POST',
			url: url,
			data: {lease_id:lease_id,invoice_id: invoice_id},
			dataType: 'text',
			processData: false,
			contentType: false,
			success:function(data){
			  var data = jQuery.parseJSON(data);

			  	if(data.message == "success")
				{
					// $('#modal-pardons').modal('hide');
					 get_invoices_list();
				}
				else
				{
					alert('Please ensure you have added included all the items');
					 get_invoices_list();
				}

			},
			error: function(xhr, status, error) {
			alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
			 get_invoices_list();
			}
			});

		// }


	}

	// chat room

	// tenant summary lists
	function get_chats()
    {
	    var lease_id = document.getElementById("lease_id").value;
	    var tenant_id = document.getElementById("tenant_id").value;
	    var tenant_unit_id = document.getElementById("tenant_unit_id").value;
	    var myTarget3 = document.getElementById("overlay-loader");
      	myTarget3.style.display = 'block';

	     var XMLHttpRequestObject = false;

	      if (window.XMLHttpRequest) {

	          XMLHttpRequestObject = new XMLHttpRequest();
	      }

	      else if (window.ActiveXObject) {
	          XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
	      }

	      var config_url = document.getElementById("config_url").value;
	      var url = config_url+"accounts/display_chats/"+lease_id+"/"+tenant_id+"/"+tenant_unit_id;
	      // alert(url);
	      if(XMLHttpRequestObject) {

	          XMLHttpRequestObject.open("GET", url);

	          XMLHttpRequestObject.onreadystatechange = function(){

	              if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

	                  document.getElementById("tenant-chat").innerHTML=XMLHttpRequestObject.responseText;
	              }
	          }

	          XMLHttpRequestObject.send(null);
	      }
	      myTarget3.style.display = 'none';


	}
</script>
