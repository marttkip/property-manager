<?php
	$all_leases = $this->leases_model->get_lease_detail($lease_id);
	foreach ($all_leases->result() as $leases_row)
	{
		$lease_id = $leases_row->lease_id;
		$tenant_unit_id = $leases_row->tenant_unit_id;
		$property_name = $leases_row->property_name;
		// $units_name = $leases_row->units_name;
		$rental_unit_name = $leases_row->rental_unit_name;
		$tenant_name = $leases_row->tenant_name;
		$lease_start_date = $leases_row->lease_start_date;
		$lease_duration = $leases_row->lease_duration;
		$rent_amount = $leases_row->rent_amount;
		$lease_number = $leases_row->lease_number;
		$arrears_bf = $leases_row->arrears_bf;
		$rent_calculation = $leases_row->rent_calculation;
		$deposit = $leases_row->deposit;
		$deposit_ext = $leases_row->deposit_ext;
		$tenant_phone_number = $leases_row->tenant_phone_number;
		$tenant_national_id = $leases_row->tenant_national_id;
		$lease_status = $leases_row->lease_status;
		$tenant_status = $leases_row->tenant_status;
		$created = $leases_row->created;
		$message_prefix = $leases_row->message_prefix;

		$lease_start_date = date('jS M Y',strtotime($lease_start_date));
		
		// $expiry_date  = date('jS M Y',strtotime($lease_start_date, mktime()) . " + 365 day");
		$expiry_date  = date('jS M Y', strtotime(''.$lease_start_date.'+1 years'));
		
	}

	$today = date('jS F Y H:i a',strtotime(date("Y:m:d h:i:s")));
	
	$served_by = $this->accounts_model->get_personnel($this->session->userdata('personnel_id'));
	// get when the lease was active

	// get all the invoiced months
	// var_dump($lease_invoice); die();
	$parent_invoice_date = $this->accounts_model->get_max_invoice_date($lease_id,$invoice_month,$invoice_year);


	$lease_invoice = $this->accounts_model->get_invoices_month($lease_id,$invoice_month,$invoice_year);
	// var_dump($lease_invoice); die();
	if($lease_invoice->num_rows() > 0)
	{
		$electricity_charge = 0;
		$electricity_bf = 0;
		$service_charge = 0;
		$service_bf = 0;
		$water_charge = 0;
		$water_bf = 0;
		$penalty_charge =0;
		$penalty_bf =0;
		$borehole = 0;
		$prev_reading = 0;
		$current_reading =0;
		$elec_prev_reading = 0;
		$elec_current_reading =0;
		$water_bill_lamp = 0;
		$elect_bill_lamp = 0;
		$bought_water_charge = 0;
		$bought_water_bf = 0;
		$common_areas_charge = 0;
		$common_areas_bf = 0;
		$painting_charge = 0;
		$painting_bf = 0;
		$sinking_charge = 0;
		$sinking_bf = 0;
		$insurance_charge = 0;
		$insurance_bf = 0;
		$current_insurance_invoice_amount = 0;
		$current_service_invoice_amount = 0;
		$current_bought_invoice_amount = 0;
		$current_water_invoice_amount = 0;
		$current_painitng_invoice_amount = 0;


		$insurance_arrears  =0;
		$insurance_invoices_bf  =0;
		$total_insurance = 0;

		$common_arrears  =0;
		$common_invoices_bf  =0;
		$total_common = 0;

		$penalty_arrears  =0;
		$penalty_invoices_bf  =0;
		$total_penalty = 0;

		$sinking_arrears  =0;
		$sinking_invoices_bf  =0;
		$total_sinking = 0;

		$water_arrears  =0;
		$water_invoices_bf  =0;
		$total_water = 0;

		$bought_arrears  =0;
		$bought_invoices_bf  =0;
		$total_bought = 0;

		$painting_arrears  =0;
		$painting_invoices_bf  =0;
		$total_painting = 0;

		$service_invoices_bf = 0;
		foreach ($lease_invoice->result() as $key_invoice) {
			# code...
			$invoice_date = $key_invoice->invoice_date;
			$invoice_id = $key_invoice->invoice_id;
			$invoice_type = $key_invoice->invoice_type;
			$billing_schedule_quarter = $key_invoice->billing_schedule_quarter;
			$invoice_array = explode('-', $invoice_date);
			$invoice_month = $invoice_array[1];
			$invoice_year = $invoice_array[0];

			// quater dates
			$quater_details = $this->accounts_model->get_quarter(0);
			$current_quater_start = date('Y-m-d',strtotime($quater_details['start']));
			$current_quater_end = date('Y-m-d',strtotime($quater_details['end']));
			// current quater 
			$quater_details_previous = $this->accounts_model->get_quarter(1);
			$previous_quater_start = date('Y-m-d',strtotime($quater_details_previous['start']));
			$previous_quater_end = date('Y-m-d',strtotime($quater_details_previous['end']));

			if($invoice_type == 3)
			{


			$water_bill_lamp = $this->accounts_model->get_total_bill($invoice_id,$invoice_type,$invoice_date);
			$elect_bill_lamp = $this->accounts_model->get_total_bill($invoice_id,$invoice_type,$invoice_date);
			}
			$invoice_date_date = date('jS F Y',strtotime($invoice_date));
			if($invoice_type == 3)
			{
				// electricity
				$electricity_charge = $key_invoice->invoice_amount;
				$electricity_bf = $key_invoice->arrears_bf;

				$elec_readings = $this->accounts_model->get_water_readings($invoice_id);

				if($elec_readings->num_rows() > 0)
				{
					foreach ($elec_readings->result() as $key_elec) {
						# code...
						$elec_prev_reading = $key_elec->prev_reading;
						$elec_current_reading = $key_elec->current_reading;
					}
				}
			}

			if($invoice_type == 4)
			{
				$service_invoice_amount = $this->accounts_model->get_invoice_tenants_brought_forward($lease_id,$parent_invoice_date,$invoice_type);
				$service_paid_amount = $this->accounts_model->get_paid_tenants_brought_forward($lease_id,$parent_invoice_date,$invoice_type);

				$total_brought_forward = $service_invoice_amount - $service_paid_amount;
				
				$service_current_invoice =  $this->accounts_model->get_invoice_tenants_current_forward($lease_id,$parent_invoice_date,$invoice_type);
				$service_current_payment =  $this->accounts_model->get_today_tenants_paid_current_forward($lease_id,$parent_invoice_date,$invoice_type);
			
				$total_service = $service_current_invoice - $service_current_payment;


			}

			if($invoice_type == 2)
			{
				// water
				$water_invoice_amount = $this->accounts_model->get_invoice_tenants_brought_forward($lease_id,$parent_invoice_date,$invoice_type);
				$water_paid_amount = $this->accounts_model->get_paid_tenants_brought_forward($lease_id,$parent_invoice_date,$invoice_type);

				$total_brought_forward = $water_invoice_amount - $water_paid_amount;
				
				$water_current_invoice =  $this->accounts_model->get_invoice_tenants_current_forward($lease_id,$parent_invoice_date,$invoice_type);
				$water_current_payment =  $this->accounts_model->get_today_tenants_paid_current_forward($lease_id,$parent_invoice_date,$invoice_type);
			
				$total_water = $water_current_invoice - $water_current_payment;

			

				$water_readings = $this->accounts_model->get_water_readings($invoice_id);

				if($water_readings->num_rows() > 0)
				{
					foreach ($water_readings->result() as $key) {
						# code...
						$prev_reading = $key->prev_reading;
						$current_reading = $key->current_reading;
					}
				}
			}

			if($invoice_type == 10)
			{
				// water
				$bought_water_invoice_amount = $this->accounts_model->get_invoice_tenants_brought_forward($lease_id,$parent_invoice_date,$invoice_type);
				$bought_water_paid_amount = $this->accounts_model->get_paid_tenants_brought_forward($lease_id,$parent_invoice_date,$invoice_type);

				$total_brought_forward = $bought_water_invoice_amount - $bought_water_paid_amount;
				
				$bought_water_current_invoice =  $this->accounts_model->get_invoice_tenants_current_forward($lease_id,$parent_invoice_date,$invoice_type);
				$bought_water_current_payment =  $this->accounts_model->get_today_tenants_paid_current_forward($lease_id,$parent_invoice_date,$invoice_type);
			
				$total_bought_water = $bought_water_current_invoice - $bought_water_current_payment;	

			}
			if($invoice_type == 7)
			{
				$painting_invoice_amount = $this->accounts_model->get_invoice_tenants_brought_forward($lease_id,$parent_invoice_date,$invoice_type);
				$painting_paid_amount = $this->accounts_model->get_paid_tenants_brought_forward($lease_id,$parent_invoice_date,$invoice_type);

				$total_brought_forward = $painting_invoice_amount - $painting_paid_amount;
				
				$painting_current_invoice =  $this->accounts_model->get_invoice_tenants_current_forward($lease_id,$parent_invoice_date,$invoice_type);
				$painting_current_payment =  $this->accounts_model->get_today_tenants_paid_current_forward($lease_id,$parent_invoice_date,$invoice_type);
			
				$total_painting = $painting_current_invoice - $painting_current_payment;
		
			}
			if($invoice_type == 8)
			{
				
				$sinking_invoice_amount = $this->accounts_model->get_invoice_tenants_brought_forward($lease_id,$parent_invoice_date,$invoice_type);
				$sinking_paid_amount = $this->accounts_model->get_paid_tenants_brought_forward($lease_id,$parent_invoice_date,$invoice_type);

				$total_brought_forward = $sinking_invoice_amount - $sinking_paid_amount;
				
				$sinking_current_invoice =  $this->accounts_model->get_invoice_tenants_current_forward($lease_id,$parent_invoice_date,$invoice_type);
				$sinking_current_payment =  $this->accounts_model->get_today_tenants_paid_current_forward($lease_id,$parent_invoice_date,$invoice_type);
			
				$total_sinking = $sinking_current_invoice - $sinking_current_payment;


		
			}
			if($invoice_type == 9)
			{
				$insurance_invoice_amount = $this->accounts_model->get_invoice_tenants_brought_forward($lease_id,$parent_invoice_date,$invoice_type);
				$insurance_paid_amount = $this->accounts_model->get_paid_tenants_brought_forward($lease_id,$parent_invoice_date,$invoice_type);

				$total_brought_forward = $insurance_invoice_amount - $insurance_paid_amount;
				
				$insurance_current_invoice =  $this->accounts_model->get_invoice_tenants_current_forward($lease_id,$parent_invoice_date,$invoice_type);
				$insurance_current_payment =  $this->accounts_model->get_today_tenants_paid_current_forward($lease_id,$parent_invoice_date,$invoice_type);
			
				$total_insurance = $insurance_current_invoice - $insurance_current_payment;
		
			}
			if($invoice_type == 11)
			{
				// water
				$common_invoice_amount = $this->accounts_model->get_invoice_tenants_brought_forward($lease_id,$parent_invoice_date,$invoice_type);
				$common_paid_amount = $this->accounts_model->get_paid_tenants_brought_forward($lease_id,$parent_invoice_date,$invoice_type);

				$total_brought_forward = $common_invoice_amount - $common_paid_amount;
				
				$common_current_invoice =  $this->accounts_model->get_invoice_tenants_current_forward($lease_id,$parent_invoice_date,$invoice_type);
				$common_current_payment =  $this->accounts_model->get_today_tenants_paid_current_forward($lease_id,$parent_invoice_date,$invoice_type);
			
				$total_common = $common_current_invoice - $common_current_payment;
		
			}

			if($invoice_type == 5)
			{
				// penalty
				$penalty_invoice_amount = $this->accounts_model->get_invoice_tenants_brought_forward($lease_id,$parent_invoice_date,$invoice_type);
				$penalty_paid_amount = $this->accounts_model->get_paid_tenants_brought_forward($lease_id,$parent_invoice_date,$invoice_type);

				$total_brought_forward = $penalty_invoice_amount - $penalty_paid_amount;
				
				$penalty_current_invoice =  $this->accounts_model->get_invoice_tenants_current_forward($lease_id,$parent_invoice_date,$invoice_type);
				$penalty_current_payment =  $this->accounts_model->get_today_tenants_paid_current_forward($lease_id,$parent_invoice_date,$invoice_type);
			
				$total_penalty = $penalty_current_invoice - $penalty_current_payment;
			}

			
		}
		// if($total_insurance < 0)
		// {
		// 	$total_insurance = 0;
		// }
		$total_billing_water = $total_water + $total_bought ;//110 * ($current_reading - $prev_reading) + 10436;
		// var_dump($total_billing_water); die();
		$total_bill = $total_service +$total_penalty + $total_insurance + $total_painting + $total_sinking + $total_billing_water;
		// var_dump($total_bill); die();
	}
	$ncc_rate = 112;
	
	$tenants_response = $this->accounts_model->get_tenants_billings($lease_id);
	$total_pardon_amount = $tenants_response['total_pardon_amount'];


	
	$account_invoice_amount = $this->accounts_model->get_invoice_tenants_brought_forward($lease_id,$parent_invoice_date);
	$account_paid_amount = $this->accounts_model->get_paid_tenants_brought_forward($lease_id,$parent_invoice_date);
	$account_todays_payment =  $this->accounts_model->get_today_tenants_paid_current_forward($lease_id,$parent_invoice_date);
	$total_brought_forward_account = $account_invoice_amount - $account_paid_amount - $account_todays_payment - $total_pardon_amount;
	// var_dump($account_paid_amount); die();
	// var_dump($parent_invoice_date); die();

	$total_bill = $total_service +$total_penalty + $total_insurance + $total_painting + $total_sinking + $total_bought_water + $total_brought_forward_account +$total_water;







?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <title><?php echo $contacts['company_name'];?> | INVOICE</title>
        <!-- For mobile content -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" media="all"/>
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css" media="all"/>
        <style type="text/css">
			.receipt_spacing{letter-spacing:0px;}
			.center-align{margin:0 auto; text-align:center;}
			
			.receipt_bottom_border{border-bottom: #888888 medium solid; margin-bottom:1px;}
			.row .col-xs-12 table {
				border:solid #000 !important;
				border-width:1px 0 0 1px !important;

				/*font-size:10px;*/
			}
			.row .col-xs-12 th, .row .col-xs-12 td {
				border:solid #000 !important;
				border-width:0 1px 1px 0 !important;
			}
			.table thead > tr > th, .table tbody > tr > th, .table tfoot > tr > th, .table thead > tr > td, .table tbody > tr > td, .table tfoot > tr > td
			{
				 padding: 2px;
			}
			.align-center
			{
				/*padding: 50px;*/
    			text-align: center;
    			font-weight: bolder;
			}
			
			.row .col-xs-12 .title-item{float:left;width: 130px; font-weight:bold; text-align:right; padding-right: 20px;}
			.title-img{float:left; padding-left:30px;}
			img.logo{max-height:70px; margin:0 auto;}
			.align-right{margin:0 auto; text-align: right !important;}
			.row {
			    margin-left: 0;
			    margin-right: 0;
			}
			.panel-title
			{
				/*font-size: 13px !important;*/
			}
			.table {
			  margin-bottom: 5px;
			  max-width: 100%;
			  width: 100%;
			}
			.table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td{border-top: none !important;}

			.receipt_spacing{letter-spacing:0px;}
			.center-align{margin:0 auto; text-align:center;}
			
			.receipt_top_border{border-top: #888888 medium solid; margin-top:1px;}
			.receipt_bottom_border{border-bottom: #888888 medium solid; margin-bottom:1px;}
			.title-img{float:left; padding-left:30px;}
			img.logo{max-height:70px; margin:0 auto;}
			.align-right{margin:0 auto; text-align: right !important;}
			.align-center
			{
				/*padding: 50px;*/
    			text-align: center;
    			font-weight: bolder;
			}
			.table {
			  margin-bottom: 5px;
			  max-width: 100%;
			  width: 100%;
			}
			.row {
  				margin-left: 0px !important;
    			margin-right: 0px !important;
    			margin-bottom: 10px;
			}
			body
			{
				font-size: 10px;
			}
			p
			{
				line-height:6px;
			}
		</style>
    </head>
    <body class="receipt_spacing">
        <div class="row">
	        <div class="col-xs-10  col-xs-offset-1">
		    	<div class="receipt_bottom_border">
		        	<table class="table table-condensed">
		                <tr>
		                    <th><h4><?php echo $contacts['company_name'];?> </h4></th>
		                    <th class="align-right">
		                        <img src="<?php echo base_url().'assets/logo/'.$contacts['logo'];?>" alt="<?php echo $contacts['company_name'];?>" class="img-responsive logo" style="float:right;"/>
		                    </th>
		                </tr>
		            </table>
		        </div>
		    	
		        
		        <!-- Patient Details -->
		    	  <div class="row receipt_bottom_border" style="margin-bottom: 10px;margin-top: 5px; font-size: 10px;">
		        	<div class="col-xs-6 ">
		                
		                <p><strong> Name </strong> : <?php echo $tenant_name;?></p>
		                <p><strong> Phone </strong> :<?php echo $tenant_phone_number;?></p>
		                <p><strong> Unit </strong> : <?php echo $property_name;?> - <?php echo $rental_unit_name;?></p>

		               
		            </div>
		            <div class="col-xs-6">
		             	<p><strong> Invoice # </strong> : <?php echo $receipt_number;?></p>
		             	<p><strong> Invoice Date </strong> : <?php echo date('jS M Y',strtotime($invoice_date));?></p>
		             	<p><strong> Billing for  </strong> : <?php echo $billing_schedule_quarter;?></p>

		                
		            </div>
		        </div>
		        <div class="row">
			        <div class="col-xs-12">
			        	<div class="col-xs-4 receipt_bottom_border">
			        		<strong>A. Balance B/F </strong> 
			        	</div>
				    </div>
			        <div class="col-xs-12">
			        	<div class="col-xs-4">
			        		Balance B/F
			        	</div>
			        	<div class="col-xs-4">
			        	</div>
			        	<div class="col-xs-4 pull-right">
			        		Kes. <?php echo number_format($total_brought_forward_account,2);?>
			        	</div>
			        	
			        </div>  
			   	</div>
			   	<div class="row">
			        <div class="col-xs-12">
			        	<div class="col-xs-4 receipt_bottom_border">
			        		<strong>B. Service Charge </strong> 
			        	</div>
			        	<div class="col-xs-4" >

			        	</div>
			        	<div class="col-xs-4 pull-right">
			        		 Kes. <?php echo number_format($total_service,2);?>
			        	</div>
			        </div>
			   	</div>

			   	<div class="row">
			        <div class="col-xs-12">
			        	<div class="col-xs-4 receipt_bottom_border">
			        		<strong>C. Water Dues </strong> 
			        	</div>
			        	<div class="col-xs-4" >
			        		<strong style="text-decoration:underline;">Billed Water</strong>
			               	  <table class="table table-hover" >
					            <tbody>
					            		
					               <tr >
					               	<td >Current Reading </td><td ><?php echo $current_reading;?></td> 
					               </tr>
					               <tr>
					               	<td >Previous Reading</td><td ><?php echo $prev_reading;?> </td>
					               </tr>
					               <tr>
					               	<td >Units Consumed</td><td  style="border-top:2px #000 solid !important;" ><?php echo $current_reading - $prev_reading;?> </td>
					               </tr>
					               <tr>
					               	<td >Rate  </td><td style="border-bottom:2px #000 solid !important;">Kes. <?php echo $ncc_rate;?></td>
					               	</tr>
					               <tr>
					               
					               </tr>
					              
					            </tbody>
					        </table>
					        <strong style="text-decoration:underline;">Bought Water</strong>
			               	  <table class="table table-hover" >
					            <tbody>
					               <tr >
					               	<td >Litres <br/> (Split Based on usage of units consumed) </td><td ><?php echo number_format($total_bought_water/0.5,2);?></td>
					               </tr>
					               <tr>
					               	<td >Rate  </td><td style="border-bottom:2px #000 solid !important;">Kes. <?php echo "0.5";?></td>

					               </tr>
					            </tbody>
					        </table>

			        	</div>
			        	<div class="col-xs-4 pull-right">
			        		 Kes. <?php echo number_format($total_water+$total_bought_water,2);?>
			        	</div>
			        </div>
			   	</div>
		        <div class="row">
			        <div class="col-xs-12">
			        	<div class="col-xs-4 receipt_bottom_border">
			        		<strong>D. Painting Charge </strong> 
			        	</div>
			        	<div class="col-xs-4" >

			        	</div>
			        	<div class="col-xs-4 pull-right">
			        		 Kes. <?php echo number_format($total_painting,2);?>
			        	</div>
			        </div>
			   	</div>
			   	<div class="row">
			        <div class="col-xs-12">
			        	<div class="col-xs-4 receipt_bottom_border">
			        		<strong>E. Sinking Fund Charge </strong> 
			        	</div>
			        	<div class="col-xs-4" >

			        	</div>
			        	<div class="col-xs-4 pull-right">
			        		 Kes. <?php echo number_format($total_sinking,2);?>
			        	</div>
			        </div>
			   	</div>
			   	<div class="row receipt_bottom_border">
			        <div class="col-xs-12 ">
			        	<div class="col-xs-4 ">
			        		<strong>F. Insurance Charge </strong> 
			        	</div>
			        	<div class="col-xs-4" >

			        	</div>
			        	<div class="col-xs-4 pull-right">
			        		 Kes. <?php echo number_format($total_insurance,2);?>
			        	</div>
			        </div>
			   	</div>

			   	<div class="row">
			        <div class="col-xs-12 receipt_bottom_border">
			        	<div class="col-xs-4 ">
			        		<strong>Total Dues A+B+C+D+E+F </strong> 
			        	</div>
			        	<div class="col-xs-4" >

			        	</div>
			        	<div class="col-xs-4 pull-right" >
			        		<strong> Kes. <?php echo number_format($total_bill);?></strong>
			        	</div>
			        </div>
			   	</div>
		       
		       
		       <?php

		       	$bills = $this->accounts_model->get_statement_items($lease_id);
				// var_dump($bills); die();
				$payment_result = '';
				if($bills->num_rows() > 0)
				{
					$x =0;
					$total_invoice = 0;
					$total_paid = 0;
					foreach ($bills->result() as $key_result) {
						# code...
						$ptype = $key_result->PTYPE;
						$invoice_type_id = $key_result->invoice_type_id;
						
						if($invoice_type_id > 0)
						{
							$invoice_type_name = $this->accounts_model->get_invoice_type_name($invoice_type_id);
							$description = 'Invoice for '.$invoice_type_name.' Due';
						}
						else{
							# code...
							$description = 'Payment';
						}
						
						
						$date = $key_result->date;
						$invoice_amount = '';
						$paid_amount = '';
						$invoiced = 0;
						$paid = 0;
						if($ptype == 'INVOICE')
						{
							$amount = $key_result->amount;
							$invoice_amount = $amount;
						}
						else
						{
							$amount = $key_result->amount;
							$paid_amount = $amount;
						}

						if($invoice_amount == '')
						{
							$invoice = 0;
						}
						else
						{
							$invoiced = $invoice_amount;
						}
						if($paid_amount == '')
						{
							$paid = 0;
						}
						else
						{
							$paid = $paid_amount;
						}
						$total_invoice = $total_invoice + $invoiced;
						$total_paid = $total_paid + $paid;
						$balance = $total_invoice - $total_paid;
						$x++;
						$payment_result .=
										  '
										  	<tr>
										  		<td>'.$x.'</td>
										  		<td>'.$date.'</td>
										  		<td>'.$description.'</td>
										  		<td>'.$invoice_amount.'</td>
										  		<td>'.$paid_amount.'</td>
										  		<td>'.number_format($balance).'</td>
										  	</tr>
										  ';

					}

				}

		       ?>
		       <div class="row" style="border-top:1px #000 solid;">
		       		<div class="col-xs-10 pull-left">
		       			<strong > <?php echo $message_prefix;?></strong>
		            </div>
		       </div>
		       <div class="row" >
		       		<div class="col-xs-12 align-center" style="border-top:1px #000 solid;">
		       			<span style="font-size:10px; " > <?php echo $contacts['company_name'];?> | <?php echo $contacts['location'];?>, <?php echo $contacts['building'];?>, <?php echo $contacts['floor'];?>
		                     E-mail: <?php echo $contacts['email'];?>.<br/> Tel : <?php echo $contacts['phone'];?>
		                     P.O. Box <?php echo $contacts['address'];?> <?php echo $contacts['post_code'];?>, <?php echo $contacts['city'];?></span>
		            </div>
		       </div>
		       
		</div>
		</div>
        
    </body>
    
</html>