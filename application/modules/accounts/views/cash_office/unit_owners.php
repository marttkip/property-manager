<?php echo $this->load->view('search/owners_search','', true); ?>
<?php
		
		$result = '';
		
		//if users exist display them
		if ($query->num_rows() > 0)
		{
			$count = $page;
			
			$result .= 
			'
			<table class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
						<th>#</th>
						<th><a> Unit Name</a></th>
						<th><a>Owner Name</a></th>
						<th><a>Owner Phone</a></th>
						<th><a>Owner Email</a></th>
						<th><a>Bal B/F</a></th>
						<th><a>Last Receipt</a></th>
						<th><a>Amount Paid </a></th>
						<th><a>Curr Balance</a></th>
						<th colspan="2">Actions</th>
					</tr>
				</thead>
				  <tbody>
				  
			';
			
			
			foreach ($query->result() as $row)
			{
				$rental_unit_id = $row->rental_unit_id;
				$home_owner_id = $row->home_owner_id;
				$rental_unit_name = $row->rental_unit_name;
				$rental_unit_price = $row->rental_unit_price;
				$home_owner_name = $row->home_owner_name;
				$home_owner_phone_number = $row->home_owner_phone_number;
				$home_owner_email = $row->home_owner_email;
				$home_owner_national_id = $row->home_owner_national_id;
				$lease_start_date = $row->lease_start_date;

				$created = $row->created;
				$rental_unit_status = $row->rental_unit_status;
				
				//status
				if($rental_unit_status == 1)
				{
					$status = 'Active';
				}
				else
				{
					$status = 'Disabled';
				}
				
				// get tenancy status 

				//create deactivated status display
				if($rental_unit_status == 0)
				{
					$status = '<span class="label label-default"> Deactivated</span>';
					$button = '<a class="btn btn-info" href="'.site_url().'activate-rental-unit/'.$rental_unit_id.'" onclick="return confirm(\'Do you want to activate '.$rental_unit_name.'?\');" title="Activate '.$rental_unit_name.'"><i class="fa fa-thumbs-up"></i></a>';
				}
				//create activated status display
				else if($rental_unit_status == 1)
				{
					$status = '<span class="label label-success">Active</span>';
					$button = '<a class="btn btn-default" href="'.site_url().'deactivate-rental-unit/'.$rental_unit_id.'" onclick="return confirm(\'Do you want to deactivate '.$rental_unit_name.'?\');" title="Deactivate '.$rental_unit_name.'"><i class="fa fa-thumbs-down"></i></a>';
				}
			
				$lease_start_date = date('jS M Y',strtotime($lease_start_date));
	     	
		     	$this_month = date('m');
		     	$amount_paid = 0;
			     	$payments = $this->accounts_model->get_this_months_payment_owners($rental_unit_id,$this_month);
			     	$current_items = '<td> -</td>
			     			  <td>-</td>'; 
			     	$total_paid_amount = 0;

			     	if($payments->num_rows() > 0)
			     	{
			     		$counter = 0;
			     		$receipt_counter = '';
			     		foreach ($payments->result() as $value) {
			     			# code...
			     			$receipt_number = $value->receipt_number;
			     			$amount_paid = $value->amount_paid;

			     			if($counter > 0)
			     			{
			     				$addition = '#';
			     				// $receipt_counter .= $receipt_number.$addition;
			     			}
			     			else
			     			{
			     				$addition = ' ';
			     				
			     			}
			     			$receipt_counter .= $receipt_number.$addition;
			     			$total_paid_amount = $total_paid_amount + $amount_paid;
			     			$counter++;
			     		}
			     		$current_items = '<td>'.$receipt_number.'</td>
			     					<td>'.number_format($amount_paid,0).'</td>';
			     	}
			     	else
			     	{
			     		$current_items = '<td> -</td>
			     					<td>-</td>';
			     	}


					$todays_date = date('Y-m-d');
					$todays_month = date('m');
					$todays_year = date('Y');

					$owners_response = $this->accounts_model->get_owners_billings($rental_unit_id,$home_owner_id);
					$arrears_amount = $owners_response['total_arrears'];
					$total_invoice_balance = $owners_response['total_invoice_balance'];
					$invoice_date = $owners_response['invoice_date'];
					//  get the amount changed
					//  get the amount changed
					$invoice_amount = $this->accounts_model->get_invoice_brought_forward($rental_unit_id,$invoice_date,$invoice_type);
					$paid_amount = $this->accounts_model->get_paid_brought_forward($rental_unit_id,$invoice_date,$invoice_type);
					$todays_payment =  $this->accounts_model->get_today_paid_current_forward($rental_unit_id,$invoice_date,$invoice_type);
					$total_brought_forward = $invoice_amount - $paid_amount - $todays_payment;
					// end of amount changed
					// var_dump($paid_amount); die();

					// get current invoice
					$current_invoice =  $this->accounts_model->get_invoice_current_forward($rental_unit_id,$invoice_date,$invoice_type);
					$current_payment =  $this->accounts_model->get_paid_current_forward($rental_unit_id,$invoice_date,$invoice_type);

					
					$phone_array = explode('/', $home_owner_phone_number);
					$total_rows = count($phone_array);

					$home_owner_phone_number = $phone_array[0];


					$all_sms_notifications_sent = $this->accounts_model->check_if_sms_sent($rental_unit_id,$home_owner_phone_number,$todays_year,$todays_month,0);

					$email_array = explode('/', $home_owner_email);
					$total_rows = count($email_array);

					$home_owner_email = $email_array[0];



					$all_email_notifications_sent = $this->accounts_model->check_if_email_sent($rental_unit_id,$home_owner_email,$todays_year,$todays_month,0);
					if(($all_sms_notifications_sent==FALSE) &&($all_email_notifications_sent==FALSE))
					{
						$send_notification_button = '<a href="'.site_url().'send-owners/'.$home_owner_id.'/'.$rental_unit_id.'" class="btn btn-sm btn-default" ><i class="fa fa-folder"></i> Send Notification</a>';	
					}
					else
					{
						$send_notification_button = '-';
					}
	
				$count++;
				$result .= 
				'
					<tr>
						<td>'.$count.'</td>
						<td>'.$rental_unit_name.'</td>
						<td>'.$home_owner_name.'</td>
						<td>'.$home_owner_phone_number.'</td>
						<td>'.$home_owner_email.'</td>
						<td>'.number_format($total_invoice_balance ,0).'</td>
						'.$current_items.'
						<td>'.number_format($arrears_amount,0).'</td>
						<td><a href="'.site_url().'owners-payments/'.$home_owner_id.'/'.$rental_unit_id.'"  class="btn btn-sm btn-success" ><i class="fa fa-folder"></i> Payments</a></td>
						<td>'.$send_notification_button.'</td>
					</tr> 
				';
			}
			
			$result .= 
			'
						  </tbody>
						</table>
			';
		}
		
		else
		{
			$result .= "There are no rental units added";
		}
		$owners_search_title = $this->session->userdata('owners_search_title');
?>

						<section class="panel">
							<header class="panel-heading">						
								<h2 class="panel-title"><?php echo $title;?></h2>

								<a href="<?php echo site_url();?>accounts/update_home_owner_invoices" style="margin-top:-24px;margin-right:5px;" class="btn btn-sm btn-success pull-right" onclick="return confirm('Do you want to update invoices for <?php echo $owners_search_title;?> ?')"> <i class="fa fa-recycle" ></i> Update invoices home owner</a>

								<a href="<?php echo site_url();?>accounts/send_notifications_home_owners" style="margin-top:-24px;margin-right:5px;" class="btn btn-sm btn-default pull-right" onclick="return confirm('Do you want to send notifications for <?php echo $owners_search_title;?> ?')"> <i class="fa fa-outbox"></i> Send Bulk Notifications Owners</a>
							</header>
							<div class="panel-body">
                            	<?php
                                $success = $this->session->userdata('success_message');
		
								if(!empty($success))
								{
									echo '<div class="alert alert-success"> <strong>Success!</strong> '.$success.' </div>';
									$this->session->unset_userdata('success_message');
								}
								
								$error = $this->session->userdata('error_message');
								
								if(!empty($error))
								{
									echo '<div class="alert alert-danger"> <strong>Oh snap!</strong> '.$error.' </div>';
									$this->session->unset_userdata('error_message');
								}

								$search =  $this->session->userdata('home_owner_property_search');
								if(!empty($search))
								{
									echo '<a href="'.site_url().'accounts/close_home_owners_search" class="btn btn-sm btn-warning">Close Search</a>';
								}
								?>
                            	<div class="row" style="margin-bottom:20px;">
                                    <!--<div class="col-lg-2 col-lg-offset-8">
                                        <a href="<?php echo site_url();?>human-resource/export-personnel" class="btn btn-sm btn-success pull-right">Export</a>
                                    </div>-->
                                    <div class="col-lg-12">
                                    </div>
                                </div>
								<div class="table-responsive">
                                	
									<?php echo $result;?>
							
                                </div>
							</div>
                            <div class="panel-footer">
                            	<?php if(isset($links)){echo $links;}?>
                            </div>
						</section>