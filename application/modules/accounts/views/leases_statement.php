<?php

$company_name = $contacts['company_name'];


$all_leases = $this->leases_model->get_lease_detail($lease_id);
$items = '';
$items_charge ='';
$total_bill = 0;
$service_charge = 0;
$penalty_charge =0;
$water_charge =0;
foreach ($all_leases->result() as $leases_row)
{
	$lease_id = $leases_row->lease_id;
	$tenant_unit_id = $leases_row->tenant_unit_id;
	$property_name = $leases_row->property_name;
	// $units_name = $leases_row->units_name;
	$rental_unit_name = $leases_row->rental_unit_name;
	$tenant_name = $leases_row->tenant_name;
	$lease_start_date = $leases_row->lease_start_date;
	$lease_duration = $leases_row->lease_duration;
	$rent_amount = $leases_row->rent_amount;
	$lease_number = $leases_row->lease_number;
	$arrears_bf = $leases_row->arrears_bf;
	$rent_calculation = $leases_row->rent_calculation;
	$deposit = $leases_row->deposit;
	$deposit_ext = $leases_row->deposit_ext;
	$tenant_phone_number = $leases_row->tenant_phone_number;
	$tenant_email = $leases_row->tenant_email;
	$tenant_national_id = $leases_row->tenant_national_id;
	$lease_status = $leases_row->lease_status;
	$tenant_status = $leases_row->tenant_status;
  $account_id = $leases_row->account_id;
	$created = $leases_row->created;
	$tenant_code = $leases_row->tenant_code;
	$message_prefix = $leases_row->message_prefix;

	$lease_start_date = $lease_start_date_norm = date('jS M Y',strtotime($lease_start_date));
	// var_dump($lease_duration);die();

	if(empty($lease_duration))
	{
		$lease_duration = 36;
		$expiry_date  = '-';//date('jS M Y', strtotime(''.$lease_start_date_norm.'+ '.$lease_duration.' month'));
	}
	else {
		$expiry_date  = date('jS M Y', strtotime(''.$lease_start_date_norm.'+ '.$lease_duration.' month'));
	}
	// $expiry_date  = date('jS M Y',strtotime($lease_start_date, mktime()) . " + 365 day");


}


?>

<!DOCTYPE html>
<html lang="en">
      <head>
        <title><?php echo $contacts['company_name'];?> | Property Statement</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" media="all"/>
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css" media="all"/>
				<style type="text/css">
						body {
								font-family: "tohoma";
								font-size: 14px;
								line-height: 0;
								color: #333333;
								background-color: #ffffff;
						}
						.receipt_spacing {
								letter-spacing: 0px;
								font-size: 14px;
						}
						.center-align{margin:0 auto; text-align:center;}

						.receipt_bottom_border{border-bottom: #888888 medium solid;}
						.row .col-md-12 table {
							border:solid #000 !important;
							border-width:1px 0 0 1px !important;
							/* font-size:10px; */
						}
						.row .col-md-12 th, .row .col-md-12 td {
							border:1px solid #000 !important;
							border-width:0 1px 1px 0 !important;
						}
						.table thead > tr > th, .table tbody > tr > th, .table tfoot > tr > th, .table thead > tr > td, .table tbody > tr > td, .table tfoot > tr > td
						{
							 padding: 1px;
								 border:1px solid #000 !important;
						}
						/* .table-bordered > thead > tr > th, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > tbody > tr > td, .table-bordered > tfoot > tr > td
						{

						} */

				.row .col-md-12 .title-item{float:left;width: 130px; font-weight:bold; text-align:right; padding-right: 20px;}
				.title-img{float:left; padding-left:30px;}
				img.logo{margin:0 auto;}
			</style>
    </head>
     <body class="receipt_spacing">
			 <div class="row">
				 <div class="col-xs-12">
						 <img src="<?php echo base_url().'assets/logo/'.$contacts['logo'];?>" alt="<?php echo $contacts['company_name'];?>" class="img-responsive logo"/>
					 </div>
			 </div>
			 <br>

			 <div class="row " >
				<div class="col-md-12 center-align">
					 <h3><strong>TENANTS STATEMENT</strong></h3>
				 </div>

				</div>

			 <div class="row ">
				 <div class="col-xs-6 invoice-col">
			     <address>
			       <b>TENANT NAME :</b> <?php echo strtoupper($tenant_name);?><br>
			       <b>TENANT CODE :</b> <?php echo $tenant_code;?><br>
			       <b>INCEPTION DATE :</b> <?php echo $lease_start_date;?> <br>
			     </address>
			   </div>


				 <div class="col-xs-6 ">
			     <address>
						 <b>LANDLORD NAME :</b> <?php echo strtoupper($property_name);?><br>
						<b>UNIT NO :</b> <?php echo strtoupper($rental_unit_name);?><br>
						<b>EXPIRY DATE :</b> <?php echo $expiry_date;?> <br>
			     </address>
			   </div>

			   <!-- /.col -->
			 </div>



        <!-- Table row -->
        <div class="row">
          <div class="col-md-12 table-responsive">
            <table class="table table-hover table-bordered">
        		<thead>
        			<tr>
								<th>#</th>
                <th>Date</th>
        				<th>Reference Details</th>
        				<th>Transaction</th>
        				<th>Debit</th>
        				<th>Credit</th>
        				<th>Balance</th>
        			</tr>
        		</thead>
        	  	<tbody>
        	  		<?php echo $tenant_response['result'];?>
        	  	</tbody>
        	</table>
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->

				<div class="row">
				  <!-- accepted payments column -->
				  <div class="col-xs-7">
				 </div>
				  <!-- /.col -->
				  <div class="col-xs-5">
				    <h4 >Payment Summary</h4>

				    <div class="table-responsive">
				      <table class="table no-border">
				        <tr>
				          <td>Total Amount Payable:</td>
				          <td><?php echo number_format($tenant_response['total_invoice'],2);?></td>
				        </tr>
				        <tr>
				          <td>Total Debit Notes:</td>
				          <td> (<?php echo number_format($tenant_response['total_debits_amount'],2);?>)</td>
				        </tr>
				        <tr>
				          <td>Total Credit Notes:</td>
				          <td> (<?php echo number_format($tenant_response['total_pardon_amount'],2);?>)</td>
				        </tr>
				        <tr>
				          <td>Total Amount Paid:</td>
				          <td><strong> (<?php echo number_format($tenant_response['total_payment_amount'],2);?>)</strong> </td>
				        </tr>

				        <tr>
				          <td>Total Amount Due:</td>
				          <td><strong> <?php echo number_format($tenant_response['total_arrears'],2);?></strong> </td>
				        </tr>
				      </table>
				    </div>
				  </div>
				  <!-- /.col -->
				</div>
				<!-- /.row -->

				<div class="col-xs-12">
					<!-- <p class="lead">Payment Methods:</p> -->
						<p style="margin-bottom:80px;"><b>Prepared By :</b> ..............................  <b>Checked By :</b> ..................................  <b>Approved By :</b> ............................................ </p>

						 <p><b>Signature :</b> ....................................  <b>Signature :</b> .......................................  <b>Signature :</b> ............................................ </p>
					</div>
				</div>


     </body>
</html>
