
<div class="box">
    <div class="box-header with-border">
      <h3 class="box-title">Search Transactions</h3>

      <div class="box-tools pull-right">

          <a href="<?php echo site_url();?>update-mpesa-payments" class="btn btn-sm btn-danger "  ><i class="fa fa-recycle"></i> Update payment list</a>
      </div>
    </div>
    <div class="box-body">
       <div class="padd">
           <?php
           echo form_open("search-mpesa-transactions", array("class" => "form-horizontal"));
           ?>
           <div class="row">
               <!-- <div class="col-md-3">
                   <div class="form-group">
                       <label class="col-md-4 control-label">Serial: </label>

                       <div class="col-md-8">
                           <input type="text" class="form-control" name="serial_number" placeholder="Serial"/>
                       </div>
                   </div>
               </div> -->
               <div class="col-md-3">
                   <div class="form-group">
                       <label class="col-md-4 control-label">Phone: </label>

                       <div class="col-md-8">
                           <input type="number" class="form-control" name="sender_phone" placeholder="254" autocomplete="off"/>
                       </div>
                   </div>
               </div>
               <div class="col-md-3">
                   <div class="form-group">
                       <label class="col-md-4 control-label">Date from: </label>

                       <div class="col-md-8">
                           	<input type="text" class="form-control" name="transaction_date_from" placeholder="Date"  id="datepicker" autocomplete="off">
                       </div>
                   </div>
               </div>
               <div class="col-md-3">
                   <div class="form-group">
                       <label class="col-md-4 control-label">Date to: </label>

                       <div class="col-md-8">
                           	<input type="text" class="form-control" name="transaction_date_to" placeholder="Date"  id="datepicker1" autocomplete="off">
                       </div>
                   </div>
               </div>


               <div class="col-md-3">
                   <div class="center-align">
                       <button type="submit" class="btn btn-info btn-sm">Search</button>
                   </div>
               </div>
           </div>


           <?php
           echo form_close();
           ?>
       </div>
   </div>
</div>
