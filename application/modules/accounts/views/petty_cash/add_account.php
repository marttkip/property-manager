 <section class="panel">
    <header class="panel-heading">
          <h4 class="pull-left"><i class="icon-reorder"></i><?php echo $title;?></h4>
          <div class="widget-icons pull-right">
                <a href="<?php echo base_url();?>accounts/account-balances" class="btn btn-primary pull-right btn-sm">Back to accounts</a>
          </div>
          <div class="clearfix"></div>
    </header>
    <div class="panel-body">
        <?php
        if(isset($error)){
            echo '<div class="alert alert-danger"> Oh snap! Change a few things up and try submitting again. </div>';
        }
        $validation_errors = validation_errors();
        
        if(!empty($validation_errors))
        {
    		$store_id = set_value('store_id');
    		$account_name = set_value('account_name');
    		$account_opening_balance = set_value('account_balance');
			$account_status = 0;
    		
            echo '<div class="alert alert-danger"> Oh snap! '.$validation_errors.' </div>';
        }
    	
        ?>
        
        <?php echo form_open_multipart($this->uri->uri_string(), array("class" => "form-horizontal", "role" => "form"));?>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="col-lg-4 control-label">Acount Name</label>
                    <div class="col-lg-4">
                    	<input type="text" class="form-control" name="account_name" placeholder="Account Name" value="<?php echo set_value('account_name');;?>" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-4 control-label">Opening Balance</label>
                    <div class="col-lg-4">
                    	<input type="text" class="form-control" name="account_balance" placeholder="Account Balance" value="<?php echo set_value('account_balance');?>" required>
                    </div>
                </div>
               
            </div>
            <div class="col-md-6">
                <!-- Activate checkbox -->
                <div class="form-group">
                    <label class="col-lg-4 control-label">Activate account?</label>
                    <div class="col-lg-4">
                        <div class="radio">
                            <label>
                            	<?php
                                if($account_status == 1){echo '<input id="optionsRadios1" type="radio" checked value="1" name="store_status">';}
            					else{echo '<input id="optionsRadios1" type="radio" value="1" name="account_status">';}
            					?>
                                Yes
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                            	<?php
                                if($account_status == 0){echo '<input id="optionsRadios1" type="radio" checked value="0" name="store_status">';}
            					else{echo '<input id="optionsRadios1" type="radio" value="0" name="account_status">';}
            					?>
                                No
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-actions center-align">
                    <button class="submit btn btn-primary btn-sm" type="submit">
                        Add account
                    </button>
                </div>
            </div>
        <?php echo form_close();?>
    </div>
    </section>