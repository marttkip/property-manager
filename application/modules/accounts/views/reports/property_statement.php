<?php

$result = '';
$total_invoice_balance =0;
$total_balance_bf = 0;
$total_rent_payable = 0;

$this_year_item = $this->session->userdata('year');
$this_month_item = $this->session->userdata('month');
$return_date = $this->session->userdata('return_date');
$total_invoice_balance_end =0;
$total_payable_end =0;
$total_arreas_end = 0;
$total_rent_end = 0;
$total_current_invoice = 0;

// var_dump($month);die();

$parent_invoice_date = date('Y-m-d', strtotime($year.'-'.$month.'-01'));
$previous_invoice_date = date('Y-m-d', strtotime($parent_invoice_date.'-1 month'));

$count = 0;
//if users exist display them
if ($query->num_rows() > 0)
{


	$result .=
	'
	<table class="table table-bordered table-striped table-condensed">
		<thead>
			<tr>
				<th>HOUSE NUMBER</th>
				<th>TENANT NAME</th>
				<th>MOBILE</th>
				<th>RENT ARREARS</th>
				<th>CURRENT RENT</th>
				<th>INVOICED</th>
				<th>RENT PAID</th>
				<th>BAL C/F</th>
			</tr>
		</thead>
		  <tbody>

	';


		foreach ($query->result() as $leases_row)
		{
			$rental_unit_name = $leases_row->rental_unit_name;
			$rental_unit_id = $leases_row->rental_unit_id;
			// $property_name = $leases_row->property_name;
			// $property_id = $leases_row->property_id;


			$todays_month = $month;//$this->session->userdata('month');
			$todays_year = $year;//$this->session->userdata('year');
			$return_date = '01';//$this->session->userdata('return_date');
			// var_dump($todays_year);die();

			if($todays_month < 9)
			{
				$todays_month = $todays_month;
			}
			$lease_id = $leases_row->lease_id;
			$lease_number = $leases_row->lease_number;
			$rental_unit_id = $leases_row->rental_unit_id;
			$tenant_name = $leases_row->tenant_name;
			$tenant_phone_number = $leases_row->tenant_phone_number;
			$rent_payable = $leases_row->billing_amount;

				$count++;

		if($lease_id > 0)
		{
			$total_pardon_amount = 0;//$tenants_response['total_pardon_amount'];


			$total_credit_note = $this->accounts_model->get_rent_credit_note_brought_forward($lease_id,$parent_invoice_date,1,$todays_month,$todays_year,$rental_unit_id);
			$total_debit_note =$this->accounts_model->get_rent_debit_note_brought_forward($lease_id,$parent_invoice_date,1,$todays_month,$todays_year,$rental_unit_id);


			$rent_payment_amount = $this->accounts_model->get_rent_payments_brought_forward($lease_id,$parent_invoice_date,1,$todays_month,$todays_year,$rental_unit_id);

			$rent_invoice_amount =$this->accounts_model->get_rent_invoices_brought_forward($lease_id,$parent_invoice_date,1,$todays_month,$todays_year,$rental_unit_id);

			// $brought_forward = $this->accounts_model->get_tenant_lease_brought_forward($lease_id,$parent_invoice_date,1,$todays_month,$todays_year,$rental_unit_id);

			$brought_forward = ($rent_invoice_amount+$total_debit_note) - ($rent_payment_amount+$total_credit_note);

			// var_dump($previous_invoice_date);die();
			$rent_current_invoice =  $this->accounts_model->get_current_month_invoice($lease_id,$todays_month,$todays_year,1,$previous_invoice_date,$parent_invoice_date);
			$rent_current_payment = $this->accounts_model->get_current_months_payments($lease_id,$todays_month,$todays_year,1,$previous_invoice_date,$parent_invoice_date);

			$total_current_variance = $rent_current_invoice - $rent_current_payment;

			$total_balance = $brought_forward + $total_current_variance;

			// if($lease_id == 1529)
			// {
			// 	var_dump($rent_invoice_amount);die();

			// }

			$result .=
						'	<tr>
								<td>'.$rental_unit_name.'</td>
								<td>'.$tenant_name.'</td>
								<td>'.$tenant_phone_number.'</td>
								<td>'.number_format($brought_forward,2).'</td>
								<td>'.number_format($rent_payable,2).'</td>
								<td>'.number_format($rent_current_invoice,2).'</td>
								<td>'.number_format($rent_current_payment,2).'</td>
								<td>'.number_format($total_balance,0).'</td>
							</tr>
						';
			// add all the balances


			$total_invoice_balance_end = $total_invoice_balance_end + $total_balance;
			$total_arreas_end = $total_arreas_end + $total_rent_brought_forward;
			$total_payable_end = $total_payable_end + $rent_current_payment;
			$total_current_invoice += $rent_current_invoice;

			$total_rent_end = $total_rent_end + $rent_payable;
		}
		else{

			if(empty($rent_payable))
			{
				$rent_payable = 0;
			}

			$result .=
					'
						<tr>
							<td>'.$rental_unit_name.'</td>
							<td>Vacant House</td>
							<td>-</td>
							<td>0.00</td>
							<td>'.number_format($rent_payable,2).'</td>
							<td>0.00</td>
							<td>0.00</td>
							<td>0.00</td>
						</tr>
					';
		}

}


	$result .='<tr>
					<td></td>
					<td></td>
					<td><strong>Totals</strong></td>
					<td style="border-bottom: #888888 medium solid; border-top: #888888 medium solid; margin-bottom:1px;"><strong>'.number_format($total_arreas_end,2).'</strong></td>
					<td style="border-bottom: #888888 medium solid; border-top: #888888 medium solid; margin-bottom:1px;"><strong>'.number_format($total_rent_end,2).'</strong></td>
					<td style="border-bottom: #888888 medium solid; border-top: #888888 medium solid; margin-bottom:1px;"><strong>'.number_format($total_current_invoice,2).'</strong></td>
					<td style="border-bottom: #888888 medium solid; border-top: #888888 medium solid; margin-bottom:1px;"><strong>'.number_format($total_payable_end,2).'</strong></td>
					<td style="border-bottom: #888888 medium solid; border-top: #888888 medium solid; margin-bottom:1px;"><strong>'.number_format($total_invoice_balance_end,2).'</strong></td>
				</tr>';

	$result .=
	'
				  </tbody>
				</table>
	';
}

else
{
	$result .= "There are no items for this month";
}



?>
<!DOCTYPE html>
<html lang="en">
      <head>
        <title><?php echo $contacts['company_name'];?> | Property Statement</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" media="all"/>
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css" media="all"/>
				<style type="text/css">
						body {
								font-family: "tohoma";
								font-size: 14px;
								line-height: 0;
								color: #333333;
								background-color: #ffffff;
						}
						.receipt_spacing {
								letter-spacing: 0px;
								font-size: 14px;
						}
						.center-align{margin:0 auto; text-align:center;}

						.receipt_bottom_border{border-bottom: #888888 medium solid;}
						.row .col-md-12 table {
							border:solid #000 !important;
							border-width:1px 0 0 1px !important;
							/* font-size:10px; */
						}
						.row .col-md-12 th, .row .col-md-12 td {
							border:1px solid #000 !important;
							border-width:0 1px 1px 0 !important;
						}
						.table thead > tr > th, .table tbody > tr > th, .table tfoot > tr > th, .table thead > tr > td, .table tbody > tr > td, .table tfoot > tr > td
						{
							 padding: 1px;
								 border:1px solid #000 !important;
						}
						/* .table-bordered > thead > tr > th, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > tbody > tr > td, .table-bordered > tfoot > tr > td
						{

						} */

				.row .col-md-12 .title-item{float:left;width: 130px; font-weight:bold; text-align:right; padding-right: 20px;}
				.title-img{float:left; padding-left:30px;}
				img.logo{margin:0 auto;}
				.table .table-no-border thead > tr > th
				{
					border:none !important;
				}
			</style>
    </head>
     <body class="receipt_spacing">
			 <div class="row">
 					<div class="col-xs-12">
 							<div class="receipt_bottom_border">
 									<table class="table table-no-border table-condensed">
 											<tr>
 													<th><h2><?php echo $contacts['company_name'];?> </h2></th>
 													<th class="align-right">
 															<img src="<?php echo base_url().'assets/logo/'.$contacts['logo'];?>" alt="<?php echo $contacts['company_name'];?>" class="img-responsive logo" style="float:right;"/>
 													</th>
 											</tr>
 									</table>
 							</div>
 					</div>
 			</div>
 			<div class="row">
 					<div class="col-xs-12">
 						<tr>
 									<th class="align-center"><h3><?php echo $title;?> </h3></th>
 							</tr>
 							<?php echo $result;?>
 					</div>
 			</div>

        <div class="row" >
       		<div class="col-xs-12 align-center" style="border-top:1px #000 solid;">
       			<span style="font-size:10px; " > <?php echo $contacts['company_name'];?> | <?php echo $contacts['location'];?>, <?php echo $contacts['building'];?>, <?php echo $contacts['floor'];?>
                     E-mail: <?php echo $contacts['email'];?>.<br/> Tel : <?php echo $contacts['phone'];?>
                     P.O. Box <?php echo $contacts['address'];?> <?php echo $contacts['post_code'];?>, <?php echo $contacts['city'];?></span>
            </div>
       	</div>
     </body>
    </html>
