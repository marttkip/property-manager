<?php
$row = $query->row();

$serial_number = $row->serial_number;
$sender_name = $row->sender_name;
$sender_phone = $row->sender_phone;
$sender_name = str_replace('%20', ' ', $sender_name);
$account_number = $row->account_number;
$amount = $mpesa_amount = $row->amount;
$created = $row->created;
$mpesa_id = $row->mpesa_id;

$date_paid = date('Y-m-d',strtotime($created));




$tenant_where = 'tenants.tenant_id = leases.tenant_id AND property.property_id = rental_unit.property_id  AND leases.rental_unit_id = rental_unit.rental_unit_id  AND leases.lease_status < 4' ;
$tenant_table = 'tenants,rental_unit,leases,property';
$tenant_order = 'tenants.tenant_name';
$select =  'tenants.tenant_number,leases.lease_id,rental_unit.rental_unit_name,property.property_name,tenants.tenant_name,leases.lease_number';

$tenant_query = $this->tenants_model->get_tenant_list($tenant_table, $tenant_where, $tenant_order,$select);
$rs8 = $tenant_query->result();
$tenants_list = '';
foreach ($rs8 as $tenant_rs) :
  $tenant_id = $tenant_rs->tenant_id;
  $tenant_name = $tenant_rs->tenant_name;

  // $tenant_national_id = $tenant_rs->tenant_national_id;
  $property_name = $tenant_rs->property_name;
  $rental_unit_name = $tenant_rs->rental_unit_name;
  $tenant_number = $tenant_rs->tenant_number;
  $lease_number = $tenant_rs->lease_number;
  $lease_id = $tenant_rs->lease_id;

    $tenants_list .="<option value='".$lease_id."'>".$tenant_number."  ".$tenant_name." A/C No: ".$lease_number." ".$property_name." ".$rental_unit_name."</option>";

endforeach;

$v_data['tenants_list'] = $tenants_list;

// get account details
// $lease_number = 25376;
$lease_number_searched = $this->session->userdata('lease_number_searched');
if($lease_number_searched)
{
	$this->db->where('lease_id = "'.$lease_number_searched.'" AND leases.tenant_id = tenants.tenant_id AND leases.rental_unit_id = rental_unit.rental_unit_id AND property.property_id = rental_unit.property_id');
}
else {
	$this->db->where('lease_number = "'.$account_number.'" AND leases.tenant_id = tenants.tenant_id AND leases.rental_unit_id = rental_unit.rental_unit_id AND property.property_id = rental_unit.property_id');
}

$query_two = $this->db->get('leases,tenants,rental_unit,property');

if($query_two->num_rows() > 0)
{
	$leases_row = $query_two->row();
	$lease_id = $leases_row->lease_id;
	$tenant_id = $leases_row->tenant_id;
	$tenant_name = $leases_row->tenant_name;
	$tenant_phone_number = $leases_row->tenant_phone_number;
	$tenant_email = $leases_row->tenant_email;
	$tenant_national_id = $leases_row->tenant_national_id;
	$lease_id = $leases_row->lease_id;
	$tenant_unit_id = $leases_row->tenant_unit_id;
	$rental_unit_name2 = $leases_row->rental_unit_name;
	$property_name2 = $leases_row->property_name;
	$rental_unit_id = $leases_row->rental_unit_id;

	// explode(delimiter, string)
	$phone_number = explode('/',$tenant_phone_number);
	// var_dump($phone_number); die();

	if(is_array($phone_number))
	{
		$tenant_phone_number = $phone_number[0];
	}
	else
	{
		$tenant_phone_number = $tenant_phone_number;
	}
	$tenant_phone_number = str_replace(' ', '', $tenant_phone_number);
	// var_dump($tenant_phone_number); die();

	$lease_checked  = TRUE;
}
else
{
	$lease_checked  = FALSE;
}


$invoice_type = $this->accounts_model->get_invoice_types();
$invoice_types = '<option value="0">Select a Type</option>';
foreach($invoice_type->result() as $res)
{
	$invoice_type_id = $res->invoice_type_id;
	$invoice_type_name = $res->invoice_type_name;

	$invoice_types .= '<option value="'.$invoice_type_id.'">'.$invoice_type_name.'</option>';

}

$month = $this->accounts_model->get_months();
$months_list = '<option value="0">Select a Type</option>';
foreach($month->result() as $res)
{
	$month_id = $res->month_id;
	$month_name = $res->month_name;
	if($month_id < 10)
	{
		$month_id = '0'.$month_id;
	}
	$month = date('M');

	if($month == $month_name)
	{
		$months_list .= '<option value="'.$month_id.'" selected>'.$month_name.'</option>';
	}
	else {
		$months_list .= '<option value="'.$month_id.'">'.$month_name.'</option>';
	}



}


$start = 2015;
$end_year = 2030;
$year_list = '<option value="0">Select a Type</option>';
for ($i=$start; $i < $end_year; $i++) {
	// code...
	$year= date('Y');

	if($year == $i)
	{
		$year_list .= '<option value="'.$i.'" selected>'.$i.'</option>';
	}
	else {
		$year_list .= '<option value="'.$i.'">'.$i.'</option>';
	}
}
// var_dump($year_list);die();

$list = '';
if($lease_checked)
{
	$item_list = $this->accounts_model->get_all_invoice_items($lease_id);
if($item_list->num_rows() > 0)
{
	foreach ($item_list->result() as $key => $value) {
		// code...
		$invoice_type_name = $value->invoice_type_name;
		$total_amount = $value->total_amount;

		$list .= '<tr><td><span>'.$invoice_type_name.' :</span></td><td>'.number_format($total_amount,2).' </td></tr>';
	}
}
}

$mpesa_balance = $this->accounts_model->get_unallocated_mpesa($mpesa_id);
$mpesa_balance = $mpesa_amount - $mpesa_balance;
?>

<div class="col-md-12">
	<a href="<?php echo site_url();?>cash-office/mpesa-received" class="btn btn-sm btn-warning pull-right"  ><i class="fa fa-arrow-left"></i> Back to unreconcilled list</a>
</div>
<div class="row">
	<!-- <div class="col-md-12"> -->
		<div class="col-md-3">
			<div class="box">
			    <div class="box-header with-border">
			      <h3 class="box-title">Transaction Detail</h3>
			      <div class="box-tools pull-right">
			      </div>
			    </div>
			    <div class="box-body">
					<table class="table table-bordered table-striped table-condensed">
						<thead>
							<tr>
								<th style="width: 40%;">Title</th>
								<th style="width: 60%;">Detail</th>
							</tr>
						</thead>
					  	<tbody>
					  		<tr><td><span>Sender Name :</span></td><td><?php echo $sender_name;?></td></tr>
					  		<tr><td><span>Sender Phone :</span></td><td><?php echo $sender_phone;?></td></tr>
					  		<tr><td><span>Serial Number :</span></td><td><?php echo $serial_number;?></td></tr>
					  		<tr><td><span>Account Number :</span></td><td><?php echo $account_number;?></td></tr>
					  		<tr><td><span>Amount :</span></td><td><?php echo $amount;?></td></tr>
					  	</tbody>
					 </table>
				</div>
			</div>
		</div>

		<div class="col-md-6">
			<div class="box">
			    <div class="box-header with-border">
			      <h3 class="box-title">Tenancy Info</h3>

			      <div class="box-tools pull-right">

			      </div>
			    </div>
			    <div class="box-body">
						<?php echo form_open("search-tenant-lease", array("class" => "form-horizontal", "role" => "form"));?>
								<div class="row">
									<div class="col-md-12">
												<div class="col-md-8">
															<div class="form-group center-align">
																<label class="col-md-4 control-label">Tenant Name: </label>

																<div class="col-md-8">
																	<select id='lease_id' name='lease_id' class='form-control select2'>
																			<!-- <select class="form-control custom-select " id='procedure_id' name='procedure_id'> -->
																				<option value=''>None - Please Select a Tenant</option>
																				<?php echo $tenants_list;?>
																			</select>
																</div>
														</div>
												</div>
												<input type="hidden" name="redirect_url" value="<?php echo $this->uri->uri_string()?>">
												<div class="col-md-4">
												<div class="form-actions center-align">
														<button class="submit btn btn-primary btn-sm" type="submit">
																Search tenant
														</button>
												</div>
											</div>

									</div>
								</div>
							<?php echo form_close();?>
					<?php
						if($lease_checked)
						{
							?>
							<table class="table table-bordered table-striped table-condensed">
								<thead>
									<tr>
										<th style="width: 40%;">Title</th>
										<th style="width: 60%;">Detail</th>
									</tr>
								</thead>
							  	<tbody>
							  		<tr><td><span>Tenant Name :</span></td><td><?php echo $tenant_name;?></td></tr>
							  		<tr><td><span>Tenant Phone :</span></td><td><?php echo $tenant_phone_number;?></td></tr>
							  		<tr><td><span>Tenant Email :</span></td><td><?php echo $tenant_email;?></td></tr>
							  		<tr><td><span>Contact ID :</span></td><td><?php echo $lease_number;?></td></tr>
										<tr><td><span>Unit :</span></td><td><?php echo $property_name2.' - '.$rental_unit_name2;?></td></tr>

							  	</tbody>
							 </table>
							<?php

						}

					?>
				</div>
			</div>
		</div>
		<div class="col-md-3">
			<div class="box">
			    <div class="box-header with-border">
			      <h3 class="box-title">Account </h3>

			      <div class="box-tools pull-right">

			      </div>
			    </div>
			    <div class="box-body">
					<?php
						if($lease_checked)
						{
							?>

						        <table class="table table-bordered table-striped table-condensed">
						          <thead>
						            <tr>
						              <th style="width: 40%;">Item</th>
						              <th style="width: 60%;">Amount</th>
						            </tr>
						          </thead>
						            <tbody>

						              <?php echo $list?>
						            </tbody>
						        </table>
							<?php
						}
						?>
				</div>
			</div>
		</div>
		<?php
		if($lease_checked)
		{
			?>

		<div class="col-md-12">
			<div class="row">
			  <div class="col-md-12">
			  <?php
			    $error = $this->session->userdata('error_message');
			    $success = $this->session->userdata('success_message');

			    if(!empty($error))
			    {
			      echo '<div class="alert alert-danger">'.$error.'</div>';
			      $this->session->unset_userdata('error_message');
			    }

			    if(!empty($success))
			    {
			      echo '<div class="alert alert-success">'.$success.'</div>';
			      $this->session->unset_userdata('success_message');
			    }
			   ?>
			  </div>
			</div>
		  <div class="box">
		      <div class="box-header with-border">
		        <h3 class="box-title">Add Payment</h3>

		        <div class="box-tools pull-right">

		        </div>
		      </div>
		      <div class="box-body">

		        <?php echo form_open("accounts/add_payment_item/".$tenant_unit_id."/".$lease_id, array("class" => "form-horizontal"));?>
		            <input type="hidden" name="type_of_account" value="1">
		            <input type="hidden" name="tenant_unit_id" id="tenant_unit_id" value="<?php echo $tenant_unit_id;?>">
		            <input type="hidden" name="lease_id" id="lease_id" value="<?php echo $lease_id?>">
								<input type="hidden" name="tenant_id" id="tenant_id" value="<?php echo $tenant_id?>">
								<input type="hidden" name="rental_unit_id" id="rental_unit_id" value="<?php echo $rental_unit_id?>">
								<input type="hidden" name="lease_number" id="lease_number" value="<?php echo $lease_number?>">
		            <input type="hidden" name="redirect_url" value="<?php echo $this->uri->uri_string()?>">
		            <input type="hidden" name="actual_balance" id="actual_balance" value="<?php echo $mpesa_amount?>">
								<input type="hidden" name="mpesa_id" id="mpesa_id" value="<?php echo $mpesa_id?>">
		            <div class="col-md-12">
		              <div class="col-md-3">
		                <div class="form-group" id="payment_method">
		                  <label class="col-md-3 control-label">Type: </label>
		                  <div class="col-md-9">
		                    <select class="form-control select2" name="invoice_type_id"   required>
		                      <?php echo $invoice_types;?>
		                    </select>
		                    </div>
		                </div>
		              </div>
		              <div class="col-md-2">
		                <div class="form-group">
		                  <label class="col-md-3 control-label">Amount: </label>
		                  <div class="col-md-9">
		                    <input type="number" class="form-control" name="amount" id="paying_amount" placeholder="<?php echo $mpesa_balance?>"  autocomplete="off"  required>
		                  </div>
		                </div>
		              </div>
		              <div class="col-md-3">
		                <div class="form-group" id="payment_method">
		                  <label class="col-md-4 control-label">Month: </label>

		                  <div class="col-md-7">
		                    <select class="form-control select2" name="payment_month"   required>
		                      <?php echo $months_list;?>
		                    </select>
		                    </div>
		                </div>
		              </div>
		              <div class="col-md-2">
		                <div class="form-group" id="payment_method">
		                  <label class="col-md-4 control-label">Year: </label>
		                  <div class="col-md-7">
		                    <select class="form-control select2" name="payment_year"   required>

		                      <?php echo $year_list;?>
		                    </select>
		                    </div>
		                </div>
		              </div>
		              <div class="col-md-2">
		                  <button class="btn btn-info btn-sm" type="submit">Add Item </button>
		              </div>
		            </div>
		          <?php echo form_close();?>

		      <div class="modal fade" id="modal-defaults">
					  	<div class="modal-dialog">
					         <div class="modal-content">
		      			      <div class="modal-header">
		      			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		      			          <span aria-hidden="true">&times;</span></button>
		      			        <h4 class="modal-title">Record New Payment</h4>
		      			      </div>
		      			      	<div class="modal-body">
		      			        	<div class="row">
		          							<div class="col-md-12">
		          								<div class="box box-primary">
		          						            <div class="box-body">
		          						            		<!-- <form id="add_payments" method="post" class="form-horizontal"> -->

		          									</div>
		          								</div>
		          							</div>
		          						</div>
		          					</div>

		        			      <div class="modal-footer">
		        			        <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
		        			        <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
		        			      </div>
		      			    </div>
					    <!-- /.modal-content -->
					     </div>
					  	<!-- /.modal-dialog -->
					</div>

		        <?php
		        $tenant_where = 'payment_item.invoice_type_id = invoice_type.invoice_type_id AND payment_item.lease_id = '.$lease_id.' AND payment_item.payment_item_status = 0 AND mpesa_id = '.$mpesa_id;
		    		$tenant_table = 'payment_item,invoice_type';
		    		$tenant_order = 'invoice_type_name';
		        $select = 'payment_item.*,invoice_type.*,payment_item.remarks as item_remarks';

		    		$tenant_query = $this->tenants_model->get_tenant_list($tenant_table, $tenant_where, $tenant_order,$select);

		        $result_payment ='<table class="table table-bordered table-striped table-condensed">
		                            <thead>
		                              <tr>
		                                <th >#</th>

		                                <th >Type</th>
		                                <th >Remarks</th>
		                                <th >Amount</th>
		                                <th colspan="2" >Action</th>
		                              </tr>
		                            </thead>
		                              <tbody>';
		        $total_amount = 0;
		        // var_dump($tenant_query->result()); die();
		        if($tenant_query->num_rows() > 0)
		        {
		          $x = 0;

		          foreach ($tenant_query->result() as $key => $value) {
		            // code...
		            $invoice_type_name = $value->invoice_type_name;
		            $amount_paid = $value->amount_paid;
		            $payment_item_id = $value->payment_item_id;
		            $remarks = $value->item_remarks;
		            $total_amount += $amount_paid;

		            $x++;
		            $result_payment .= form_open("accounts/update_payments_item/".$payment_item_id."/".$lease_id, array("class" => "form-horizontal"));
		            $result_payment .= '<tr>
		                                    <td>'.$x.'</td>
		                                    <td>'.$invoice_type_name.'</td>
		                                    <td>'.$remarks.'</td>
		                                    <td><input class="form-control" name="invoice_item_amount'.$payment_item_id.'" value="'.$amount_paid.'"></td>
		                                    <td><button type="submit" class="btn btn-sm btn-warning" ><i class="fa fa-pencil"></i></button></td>
		                                    <td><a href="'.site_url().'delete-payment-item/'.$payment_item_id.'" type="submit" class="btn btn-sm btn-danger" ><i class="fa fa-trash"></i></a></td>
		                                </tr>';
		            $result_payment .=form_close();
		          }

		          // display button

		          $display = TRUE;
		        }
		        else {
		          $display = FALSE;
		        }

		        $result_payment .='</tbody>
		                        </table>';
		        ?>

		        <?php echo $result_payment;?>
		        <br>
		        <?php
		        if($display)
		        {
		          ?>
		          <div class="row">
		            <div class="col-md-12">
		              <?php echo form_open("accounts/confirm_payment/".$tenant_unit_id."/".$lease_id, array("class" => "form-horizontal"));?>
		                <div class="col-md-6">
		                </div>
		                <div class="col-md-6">
		                    <!-- <h2 class="pull-right"> KES. <?php echo number_format($total_amount,2);?></h2> -->

		                    <input type="hidden" name="type_of_account" value="1">
		                    <input type="hidden" name="tenant_unit_id" id="tenant_unit_id" value="<?php echo $tenant_unit_id;?>">
		                    <input type="hidden" name="lease_id" id="lease_id" value="<?php echo $lease_id?>">
		                    <!-- <input type="hidden" name="redirect_url" value="cash-office/mpesa-received"> -->
                        <input type="hidden" name="redirect_url" value="<?php echo $this->uri->uri_string()?>">
		                    <input type="hidden" name="rental_unit_id" id="lease_id" value="<?php echo $rental_unit_id?>">
												<input type="hidden" name="mpesa_id" id="mpesa_id" value="<?php echo $mpesa_id?>">
		                  <div class="form-group">
		                    <label class="col-md-4 control-label">Payment Date: </label>

		                    <div class="col-md-7">
		                       <div class="input-group">
		                            <span class="input-group-addon">
		                                <i class="fa fa-calendar"></i>
		                            </span>
		                            <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="payment_date" placeholder="Payment Date" id="datepicker" value="<?php echo date('Y-m-d')?>" required>
		                        </div>
		                    </div>
		                  </div>
		                  <div class="form-group" id="payment_method">
		                    <label class="col-md-4 control-label">Payment Method: </label>

		                    <div class="col-md-7">
		                      <select class="form-control" name="payment_method" onchange="check_payment_type(this.value)" required>
		                        <!-- <option value="0">Select a payment method</option> -->
		                                              <?php
		                          $method_rs = $this->accounts_model->get_payment_methods();

		                          foreach($method_rs->result() as $res)
		                          {
		                            $payment_method_id = $res->payment_method_id;
		                            $payment_method = $res->payment_method;


																if($payment_method == 'Bank Slip')
																{
																	 echo '<option value="'.$payment_method_id.'">'.$payment_method.'</option>';
																}



		                          }

		                        ?>
		                      </select>
		                      </div>
		                  </div>
		                  <div id="mpesa_div" class="form-group" style="display:none;" >
		                    <label class="col-md-4 control-label"> Mpesa TX Code: </label>

		                    <div class="col-md-7">
		                      <input type="text" class="form-control" name="mpesa_code" placeholder="">
		                    </div>
		                  </div>
		                  <div id="cheque_div"   >
		                    <div class="form-group" >
		                      <label class="col-md-4 control-label"> Bank: </label>

		                      <div class="col-md-7">
		                        <select class="form-control " name="bank_id"  required>
		                          <!-- <option value="0">Select a bank</option> -->
		                                                <?php
		                            $bank_rs = $this->accounts_model->get_bank_accounts();

		                            foreach($bank_rs->result() as $res)
		                            {
		                              $id = $res->id;
		                              $name = $res->name;

																	if($name == 'Paybill')
																	{
		                              	echo '<option value="'.$id.'">'.$name.'</option>';
																	}

		                            }

		                          ?>
		                        </select>
		                      </div>
		                    </div>
		                    <div class="form-group" >
		                      <label class="col-md-4 control-label"> Cheque Number: </label>
		                      <div class="col-md-7">
		                        <input type="text" class="form-control" name="reference_number" placeholder="" value="<?php echo $serial_number?>" readonly>
		                      </div>
		                    </div>
		                  </div>
		                  <div class="form-group">
		                    <label class="col-md-4 control-label">Total Amount: </label>

		                    <div class="col-md-7">
		                      <input type="number" class="form-control" name="total_amount" placeholder=""  autocomplete="off" value="<?php echo $total_amount;?>" readonly>
		                    </div>
		                  </div>
		                  <div class="form-group">
		                    <label class="col-md-4 control-label">Paid in By: </label>

		                    <div class="col-md-7">
		                      <input type="text" class="form-control" name="paid_by" placeholder="<?php echo $tenant_name;?>" value="<?php echo $tenant_name;?>" autocomplete="off" >
		                    </div>
		                  </div>
		                  <div class="col-md-12">
		                      <div class="text-center">
		                        <button class="btn btn-info btn-sm " type="submit">Complete Payment </button>
		                      </div>
		                  </div>
		                </div>
		              <?php echo form_close();?>
		            </div>
		          </div>
		          <?php
		        }
		        ?>


		      </div>
		  </div>
		</div>





		<?php
			}
			?>
	<!-- </div> -->
<div class="col-md-12">
  <div class="row">
    <div class="col-md-12">
      <div class="box">
          <div class="box-header with-border">
            <h3 class="box-title">Latest Payments</h3>

            <div class="box-tools pull-right">

            </div>
          </div>
          <div class="box-body">
              <table class="table table-hover table-bordered col-md-12">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Payment Date</th>
                    <th>Receipt Number</th>
                    <th>Amount Paid</th>
                    <th>Paid By</th>
                    <th>Receipted Date</th>
                    <th>Receipted By</th>
                    <th>Reconcilled Status</th>
                    <th colspan="3">Actions</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  	$lease_payments = $this->accounts_model->get_lease_mpesa_payments($mpesa_id,10);
                    // var_dump($tenant_query);die();
                  if($lease_payments->num_rows() > 0)
                  {
                    $y = 0;
                    foreach ($lease_payments->result() as $key) {
                      # code...
                      $receipt_number = $key->receipt_number;
                      $amount_paid = $key->amount_paid;
                      $payment_id = $key->payment_id;
                      $paid_by = $key->paid_by;
                      $payment_date = $key->payment_date;
                      $document_number = $key->document_number;
                      $payment_created = $key->payment_created;
                      $payment_created_by = $key->payment_created_by;
                      $confirmation_status = $key->confirmation_status;

                      if($confirmation_status == 0)
                      {
                        $confirmation_status_text = "Reconciled";
                        $button = '<a class="btn btn-sm btn-danger" href="'.site_url().'accounts/deactivate_payments/'.$payment_id.'/'.$lease_id.'" onclick="return confirm(\'Do you want to Deactivate this payment Kes.'.$amount_paid.'?\');" title="Deactivate For Company Return'.$amount_paid.'"> Deactivate </a>';


                      }
                      else
                      {
                        $confirmation_status_text = "Not Reconciled";
                        $button = '<a class="btn btn-sm btn-info" href="'.site_url().'accounts/activate_payments/'.$payment_id.'/'.$lease_id.'" onclick="return confirm(\'Do you want to Activate this payment Kes.'.$amount_paid.'?\');" title="Activate For Company Return'.$amount_paid.'"> Activate</a>';


                      }
                      $payment_explode = explode('-', $payment_date);

                      $payment_date = date('jS M Y',strtotime($payment_date));
                      $payment_created = date('jS M Y',strtotime($payment_created));
                      $y++;
                      $message = $this->site_model->create_web_name('Your payment of Ksh. '.number_format($amount_paid, 0).' has been received for Hse No. '.$rental_unit_name);

                      ?>
                      <tr>
                        <td><?php echo $y?></td>
                        <td><?php echo $payment_date;?></td>
                        <td><?php echo $document_number?></td>
                        <td><?php echo number_format($amount_paid,2);?></td>
                        <td><?php echo $paid_by;?></td>
                        <td><?php echo $payment_date;?></td>
                        <td><?php echo $payment_created_by;?></td>
                        <td><?php echo $confirmation_status_text;?></td>
                        <td><a href="<?php echo site_url().'cash-office/print-receipt/'.$payment_id.'/'.$tenant_unit_id.'/'.$lease_id;?>" class="btn btn-sm btn-primary" target="_blank">Receipt</a></td>
                        <td><a href="<?php echo site_url().'send-tenants-receipt/'.$payment_id.'/'.$tenant_unit_id.'/'.$lease_id;?>" class="btn btn-sm btn-success">Send SMS</a></td>

                      </tr>
                      <?php

                    }
                  }
                  ?>

                </tbody>
              </table>

          </div>
        </div>
      </div>
    </div>
</div>
</div>


<script type="text/javascript">
	$(function() {
	    $("#invoice_type_id").customselect();
	    $("#billing_schedule_id").customselect();

	});


	function check_payment_type(payment_type_id){

	  var myTarget1 = document.getElementById("cheque_div");

	  var myTarget2 = document.getElementById("mpesa_div");

	  var myTarget3 = document.getElementById("insuarance_div");

	  if(payment_type_id == 1)
	  {
	    // this is a cash

	    myTarget1.style.display = 'none';
	    myTarget2.style.display = 'none';
	    myTarget3.style.display = 'none';
	  }
	  else if(payment_type_id == 2 || payment_type_id == 3 || payment_type_id == 5)
	  {
	    // cheque
	    myTarget1.style.display = 'block';
	    myTarget2.style.display = 'none';
	    myTarget3.style.display = 'none';
	  }

	  else
	  {
	    myTarget1.style.display = 'none';
	    myTarget2.style.display = 'none';
	    myTarget3.style.display = 'none';
	  }

	}
</script>
