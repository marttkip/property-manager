<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once "./application/modules/real_estate_administration/controllers/property.php";
// require_once "./application/modules/mpesa/controllers/cron.php";
// header('Content-type: application/json;');
// error_reporting(0);
date_default_timezone_set('Africa/Nairobi');
class Accounts extends property {
	var $csv_path;
	var $invoice_path;
	var $statement_path;
	function __construct()
	{
		parent:: __construct();
		$this->load->model('real_estate_administration/tenants_model');
		$this->load->model('real_estate_administration/rental_unit_model');
		$this->load->model('real_estate_administration/leases_model');
		$this->load->model('real_estate_administration/property_owners_model');
		$this->load->model('real_estate_administration/property_model');
		$this->load->model('accounts/accounts_model');
		$this->load->model('accounting/petty_cash_model');
		$this->load->model('admin/email_model');
		$this->load->model('payroll/payroll_model');
		$this->load->model('hr/personnel_model');
		$this->load->model('admin/branches_model');
		$this->load->model('administration/reports_model');
		// $this->load->controller('mpesa/cron');
		// $this->load->model('accounting/salary_advance_model');

		$this->csv_path = realpath(APPPATH . '../assets/csv');
		$this->invoice_path = realpath(APPPATH . '../');
		$this->statement_path = realpath(APPPATH . '../statements/');

		 $this->load->model('auth/auth_model');
		if(!$this->auth_model->check_login())
		{
			redirect('login');
		}
	}

	/*
	*
	*	Default action is to show all the tenants
	*
	*/

	public function index()
	{
		$pager = NULL;
		$rental_unit_id = null;

		$where = 'tenant_id > 0';
		$table = 'tenants';

		$tenants_search = $this->session->userdata('all_accounts_search');

		if(!empty($tenants_search))
		{
			$where .= $tenants_search;

		}
		//pagination
		// var_dump($where);
		$segment = 3;

		$this->load->library('pagination');
		$config['base_url'] = base_url().'accounts/tenants-accounts';
		$config['total_rows'] = $this->tenants_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;

		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';

		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';

		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';

		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';

		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active">';
		$config['cur_tag_close'] = '</li>';

		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);

		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
				$v_data["links"] = $this->pagination->create_links();
		$query = $this->tenants_model->get_all_tenants($table, $where, $config["per_page"], $page, $order=NULL, $order_method=NULL);

		$tenant_order = 'tenants.tenant_name';
		$tenant_table = 'tenants';
		$tenant_where = 'tenants.tenant_status = 1';

		$tenant_query = $this->tenants_model->get_tenant_list($tenant_table, $tenant_where, $tenant_order);
		$rs8 = $tenant_query->result();
		$tenants_list = '';
		foreach ($rs8 as $tenant_rs) :
			$tenant_id = $tenant_rs->tenant_id;
			$tenant_name = $tenant_rs->tenant_name;
			$tenant_number = $tenant_rs->tenant_number;

			$tenant_national_id = $tenant_rs->tenant_national_id;
			$tenant_phone_number = $tenant_rs->tenant_phone_number;

				$tenants_list .="<option value='".$tenant_id."'>".$tenant_number." ".$tenant_name." Phone: ".$tenant_phone_number."</option>";

		endforeach;

		$v_data['tenants_list'] = $tenants_list;
		$v_data['pager'] = $pager;

		if($rental_unit_id != NULL AND $rental_unit_id != 0)
		{
			$rental_unit_name = $this->rental_unit_model->get_rental_unit_name($rental_unit_id);
			$data['title'] = $rental_unit_name.'\'s Tenants';
			$v_data['title'] = $data['title'];
		}
		else
		{
			$data['title'] = 'All Tenants';
			$v_data['title'] = $data['title'];
		}

		$v_data['rental_unit_id'] = $rental_unit_id;
		$v_data['tenants'] = $query;
		$v_data['page'] = $page;

		$data['content'] = $this->load->view('tenants/tenants_list', $v_data, true);

		$this->load->view('admin/templates/general_page', $data);
	}
	



	public function index_old()
	{

		$where = 'tenants.tenant_id > 0 AND tenants.tenant_id = tenant_unit.tenant_id AND tenant_unit.rental_unit_id = rental_unit.rental_unit_id  AND tenant_unit.tenant_unit_id = leases.tenant_unit_id AND leases.lease_status = 1 AND property.property_id = rental_unit.property_id';
		$table = 'tenants,tenant_unit,rental_unit,leases,property';


		$accounts_search = $this->session->userdata('all_accounts_search');

		if(!empty($accounts_search))
		{
			$where .= $accounts_search;

		}


		$properties = $this->session->userdata('properties');

		if(isset($properties) AND !empty($properties))
		{
			$where .= $properties;
		}
		// var_dump($properties); die();
		$segment = 3;
		//pagination

		$this->load->library('pagination');
		$config['base_url'] = base_url().'accounts/tenants-accounts';
		$config['total_rows'] = $this->tenants_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;

		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';

		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';

		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';

		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';

		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active">';
		$config['cur_tag_close'] = '</li>';

		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);

		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
    $v_data["links"] = $this->pagination->create_links();
		$query = $this->accounts_model->get_all_tenants($table, $where, $config["per_page"], $page, $order='rental_unit.rental_unit_name', $order_method='ASC');

		$properties = $this->property_model->get_active_property();

		// $this->accounts_model->update_invoices();


		$rs8 = $properties->result();
		$property_list = '';
		foreach ($rs8 as $property_rs) :
			$property_id = $property_rs->property_id;
			$property_name = $property_rs->property_name;
			$property_location = $property_rs->property_location;

		    $property_list .="<option value='".$property_id."'>".$property_name." Location: ".$property_location."</option>";

		endforeach;
		$v_data['property_list'] = $property_list;


		$data['title'] = 'All Tenants Accounts';
		$v_data['title'] = $data['title'];
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		$data['content'] = $this->load->view('cash_office/tenants_list', $v_data, true);

		$this->load->view('admin/templates/general_page', $data);
	}

	public function water_tenants()
	{

		$where = 'tenants.tenant_id > 0 AND tenants.tenant_id = tenant_unit.tenant_id AND tenant_unit.rental_unit_id = rental_unit.rental_unit_id  AND tenant_unit.tenant_unit_id = leases.tenant_unit_id AND leases.lease_status = 1 AND property.property_id = rental_unit.property_id';
		$table = 'tenants,tenant_unit,rental_unit,leases,property';


		$accounts_search = $this->session->userdata('all_accounts_search');

		if(!empty($accounts_search))
		{
			$where .= $accounts_search;

		}
		$segment = 3;
		//pagination

		$this->load->library('pagination');
		$config['base_url'] = base_url().'accounts/tenants-accounts';
		$config['total_rows'] = $this->tenants_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;

		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';

		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';

		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';

		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';

		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active">';
		$config['cur_tag_close'] = '</li>';

		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);

		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->accounts_model->get_all_tenants($table, $where, $config["per_page"], $page, $order='rental_unit.rental_unit_id,rental_unit.rental_unit_name,property.property_name', $order_method='ASC');

		$properties = $this->property_model->get_active_property();
		$rs8 = $properties->result();
		$property_list = '';
		foreach ($rs8 as $property_rs) :
			$property_id = $property_rs->property_id;
			$property_name = $property_rs->property_name;
			$property_location = $property_rs->property_location;

		    $property_list .="<option value='".$property_id."'>".$property_name." Location: ".$property_location."</option>";

		endforeach;
		$v_data['property_list'] = $property_list;
		$data['title'] = 'All Tenants Accounts';
		$v_data['title'] = $data['title'];
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		$data['content'] = $this->load->view('cash_office/water_tenants', $v_data, true);

		$this->load->view('admin/templates/general_page', $data);
	}
	public function update_invoices()
	{

		$this->accounts_model->update_invoices();

		redirect('cash-office/tenants-leases');
	}
	public function update_home_owner_invoices()
	{

		$this->accounts_model->update_home_owner_invoices();

		redirect('cash-office/owners');
	}
	public function search_accounts()
	{
		// $property_id = $this->input->post('property_id');
		// $tenant_name = $this->input->post('tenant_name');
		// $contract_no = $this->input->post('contract_no');
		// $rental_unit_name = $this->input->post('rental_unit_name');
		//
		// $search_title = '';
		//
		// if(!empty($property_id))
		// {
		// 	$this->db->where('property_id', $property_id);
		// 	$query = $this->db->get('property');
		//
		// 	if($query->num_rows() > 0)
		// 	{
		// 		$row = $query->row();
		// 		$search_title .= $row->property_name.' ';
		// 	}
		//
		//
		// 	$property_id = ' AND rental_unit.property_id = '.$property_id.' ';
		//
		//
		// }
		//
		// if(!empty($tenant_name))
		// {
		// 	$search_title .= $tenant_name.' ';
		// 	$tenant_name = ' AND tenants.tenant_name LIKE \'%'.$tenant_name.'%\'';
		//
		//
		// }
		// else
		// {
		// 	$tenant_name = '';
		// 	$search_title .= '';
		// }
		//
		// if(!empty($contract_no))
		// {
		// 	$search_title .= $contract_no.' ';
		// 	$contract_no = ' AND leases.lease_number = '.$contract_no.'';
		//
		//
		// }
		// else
		// {
		// 	$contract_no = '';
		// 	$search_title .= '';
		// }
		//
		// if(!empty($rental_unit_name))
		// {
		// 	$search_title .= $rental_unit_name.' ';
		// 	$rental_unit_name = ' AND rental_unit.rental_unit_name LIKE \'%'.$rental_unit_name.'%\'';
		//
		// }
		// else
		// {
		// 	$rental_unit_name = '';
		// 	$search_title .= '';
		// }
		//
		//
		// $search = $property_id.$tenant_name.$rental_unit_name.$contract_no;
		// $property_search = $property_id;
		// // var_dump($search); die();
		//
		// $this->session->set_userdata('property_search', $property_search);
		// $this->session->set_userdata('all_accounts_search', $search);
		// $this->session->set_userdata('accounts_search_title', $search_title);



		$tenant_name = $this->input->post('tenant_name');

		$tenant_id = $this->input->post('tenant_id');
		$tenant_phone_number = $this->input->post('tenant_phone_number');
		$tenant_national_id = $this->input->post('tenant_national_id');

		$search_title = 'Showing reports for: ';


		if(!empty($tenant_name))
		{
			$tenant_name = ' AND tenants.tenant_name LIKE \'%'.$tenant_name.'%\'';
		}
		else
		{
			$tenant_name = '';
		}

		if(!empty($tenant_id))
		{
			$tenant_id = ' AND tenants.tenant_id LIKE \'%'.$tenant_id.'%\'';
		}
		else
		{
			$tenant_id = '';
		}

		if(!empty($tenant_phone_number))
		{
			$tenant_phone_number = ' AND tenants.tenant_phone_number LIKE \'%'.$tenant_phone_number.'%\'';
		}
		else
		{
			$tenant_phone_number = '';
		}

		if(!empty($tenant_national_id))
		{
			$tenant_national_id = ' AND tenants.tenant_national_id LIKE \'%'.$tenant_national_id.'%\'';
		}
		else
		{
			$tenant_national_id = '';
		}

		$search = $tenant_id;

		// $tenants_search = $this->session->userdata('all_tenants_search');


		$this->session->set_userdata('all_accounts_search', $search);

		$this->index();
	}
	public function search_home_owners()
	{
		$property_id = $this->input->post('property_id');
		$home_owner_name = $this->input->post('home_owner_name');
		$rental_unit_name = $this->input->post('rental_unit_name');

		$search_title = '';

		if(!empty($property_id))
		{
			$this->db->where('property_id', $property_id);
			$query = $this->db->get('property');

			if($query->num_rows() > 0)
			{
				$row = $query->row();
				$search_title .= $row->property_name.' ';
			}


			$property_id = ' AND rental_unit.property_id = '.$property_id.' ';


		}

		if(!empty($home_owner_name))
		{
			$search_title .= $home_owner_name.' ';
			$home_owner_name = ' AND home_owners.home_owner_name LIKE \'%'.$home_owner_name.'%\'';
		}
		else
		{
			$home_owner_name = '';
			$search_title .= ' ';
		}

		if(!empty($rental_unit_name))
		{
			$search_title .= $home_owner_name.' ';
			$rental_unit_name = ' AND rental_unit.rental_unit_name LIKE \'%'.$rental_unit_name.'%\'';
		}
		else
		{
			$rental_unit_name = '';
			$search_title .= ' ';
		}


		$search = $property_id.$home_owner_name.$rental_unit_name;
		$property_search = $property_id;
		// var_dump($search); die();

		// $this->session->set_userdata('home_owner_property_search', $property_search);
		$this->session->set_userdata('home_owner_property_search', $search);
		$this->session->set_userdata('owners_search_title', $search_title);

		$this->owners();
	}
	public function close_accounts_search()
	{
		$this->session->unset_userdata('all_accounts_search');
		$this->session->unset_userdata('search_title');


		redirect('accounts/tenants-accounts');
	}
	public function close_home_owners_search()
	{
		$this->session->unset_userdata('home_owner_property_search');
		$this->session->unset_userdata('search_title');


		$this->owners();
	}
	public function make_payments($tenant_unit_id , $lease_id, $close_page = NULL)
	{
		$this->form_validation->set_rules('payment_method', 'Payment Method', 'trim|required|xss_clean');
		$this->form_validation->set_rules('amount_paid', 'Amount', 'trim|required|xss_clean');
		$this->form_validation->set_rules('payment_date', 'Date Receipted', 'trim|required|xss_clean');
		$this->form_validation->set_rules('water_amount', 'Water amount', 'trim|xss_clean');
		$this->form_validation->set_rules('service_charge_amount', 'Service charge amount', 'trim|xss_clean');
		$this->form_validation->set_rules('penalty_fee', 'Service charge amount', 'trim|xss_clean');
		$this->form_validation->set_rules('rent_amount', 'Rent amount', 'trim|xss_clean');



		$payment_method = $this->input->post('payment_method');
		$water_amount = $this->input->post('water_amount');
		$rent_amount = $this->input->post('rent_amount');
		$service_charge_amount = $this->input->post('service_charge_amount');
		$penalty_fee = $this->input->post('penalty_fee');
		$amount_paid = $this->input->post('amount_paid');
		$fixed_charge = $this->input->post('fixed_charge');
		$deposit_charge = $this->input->post('deposit_charge');

		$insurance = $this->input->post('insurance');
		$sinking_funds = $this->input->post('sinking_funds');
		$bought_water = $this->input->post('bought_water');
		$painting_charge = $this->input->post('painting_charge');
		$legal_fees = $this->input->post('legal_fees');


		if(empty($water_amount))
		{
			$water_amount = 0;
		}
		if(empty($service_charge_amount))
		{
			$service_charge_amount = 0;
		}
		if(empty($rent_amount))
		{
			$rent_amount = 0;
		}
		if(empty($penalty_fee))
		{
			$penalty_fee = 0;
		}
		if(empty($fixed_charge))
		{
			$fixed_charge = 0;
		}

		if(empty($deposit_charge))
		{
			$deposit_charge = 0;
		}
		if(empty($insurance))
		{
			$insurance = 0;
		}
		if(empty($sinking_funds))
		{
			$sinking_funds = 0;
		}
		if(empty($bought_water))
		{
			$bought_water = 0;
		}
		if(empty($painting_charge))
		{
			$painting_charge = 0;
		}
		if(empty($legal_fees))
		{
			$legal_fees = 0;
		}
		//  add all this items
		$total = $service_charge_amount + $rent_amount + $water_amount + $penalty_fee + $fixed_charge + $deposit_charge + $insurance + $sinking_funds + $bought_water + $painting_charge+ $legal_fees;

		// var_dump($total); die();
		// Normal

		if(!empty($payment_method))
		{
			if($payment_method == 1)
			{
				// check for cheque number if inserted
				$this->form_validation->set_rules('bank_name', 'Bank Name', 'trim|required|xss_clean');

			}
			else if($payment_method == 5)
			{
				//  check for mpesa code if inserted
				$this->form_validation->set_rules('mpesa_code', 'Amount', 'is_unique[payments.transaction_code]|trim|required|xss_clean');
			}
			$response['status'] = 'fail';
			$response['message'] = 'Please select a payment method';
		}

		//if form conatins invalid data
		if ($this->form_validation->run())
		{

			if($amount_paid == $total)
			{
				$this->accounts_model->receipt_payment($lease_id);

				$this->session->set_userdata("success_message", 'Payment successfully added');
				$response['status'] = 'success';
				$response['message'] = 'Payment successfully added';
			}
			else
			{
				$this->session->set_userdata("error_message", 'The amounts of payment do not add up to the total amount paid');
				$response['status'] = 'fail';
				$response['message'] = 'The amounts of payment do not add up to the total amount paid';
			}


		}
		else
		{
			$this->session->set_userdata("error_message", validation_errors());
			$response['status'] = 'fail';
			$response['message'] = validation_errors();

		}

		echo json_encode($response);

		// $redirect_url = $this->input->post('redirect_url');
		// redirect($redirect_url);
	}
	public function create_pardon($tenant_unit_id , $lease_id, $close_page = NULL)
	{
		$this->form_validation->set_rules('pardon_amount', 'Amount', 'numeric|trim|required|xss_clean');
		$this->form_validation->set_rules('pardon_date', 'Date ', 'trim|required|xss_clean');
		$this->form_validation->set_rules('pardon_reason', 'Reason', 'required|trim|xss_clean');
		//if form conatins invalid data
		if ($this->form_validation->run())
		{

			if($this->accounts_model->receipt_pardon($lease_id))
			{
				$this->session->set_userdata("success_message", 'Pardon created successfully added');
			}
			else
			{
				$this->session->set_userdata("error_message", 'Sorry. Please try again');
			}


		}
		else
		{
			$this->session->set_userdata("error_message", validation_errors());

		}

		// $redirect_url = $this->input->post('redirect_url');
		// redirect($redirect_url);
		$response['message'] = 'success';
		echo json_decode($response);

	}
	public function create_owner_pardon($home_owner_unit_id , $rental_unit_id, $close_page = NULL)
	{
		$this->form_validation->set_rules('pardon_amount', 'Amount', 'numeric|trim|required|xss_clean');
		$this->form_validation->set_rules('pardon_date', 'Date ', 'trim|required|xss_clean');
		$this->form_validation->set_rules('pardon_reason', 'Reason', 'required|trim|xss_clean');
		//if form conatins invalid data
		if ($this->form_validation->run())
		{

			if($this->accounts_model->receipt_owner_pardon($home_owner_unit_id, $rental_unit_id))
			{
				$this->session->set_userdata("success_message", 'Pardon created successfully added');
			}
			else
			{
				$this->session->set_userdata("error_message", 'Sorry. Please try again');
			}


		}
		else
		{
			$this->session->set_userdata("error_message", validation_errors());

		}

		$redirect_url = $this->input->post('redirect_url');
		redirect($redirect_url);

	}
    public function payments($tenant_unit_id,$lease_id)
	{
		$v_data = array('tenant_unit_id'=>$tenant_unit_id,'lease_id'=>$lease_id);
		$v_data['title'] = 'Tenant payments';
		$v_data['cancel_actions'] = $this->accounts_model->get_cancel_actions();
		// get lease payments for this year


		$data['content'] = $this->load->view('cash_office/payments', $v_data, true);

		$data['title'] = 'Tenant payments';

		$this->load->view('admin/templates/general_page', $data);
	}

	/*
	*
	*	Add a new tenant page
	*
	*/
	public function add_tenant($rental_unit_id = NULL)
	{
		//form validation rules
		$this->form_validation->set_rules('tenant_email', 'Email', 'xss_clean|is_unique[tenants.tenant_email]|valid_email');
		$this->form_validation->set_rules('tenant_name', 'Tenant name', 'required|xss_clean');
		$this->form_validation->set_rules('tenant_phone_number', 'Tenant Phone Number', 'required|xss_clean|is_unique[tenants.tenant_phone_number]');
		$this->form_validation->set_rules('tenant_national_id', 'National Id', 'xss_clean');

		//if form has been submitted
		if ($this->form_validation->run())
		{
			//check if tenant has valid login credentials
			if($this->tenants_model->add_tenant())
			{
				$this->session->unset_userdata('tenants_error_message');
				$this->session->set_userdata('success_message', 'Tenant has been successfully added');

				if($rental_unit_id == NULL OR $rental_unit_id == 0)
				{
					redirect('rental-management/tenants');
				}
				else
				{
					redirect('rents/tenants/'.$rental_unit_id);
				}


			}

			else
			{
				$this->session->set_userdata('error_message', 'Sorry something went wrong. Please try again');
				if($rental_unit_id == NULL OR $rental_unit_id == 0)
				{
					redirect('rental-management/tenants');
				}
				else
				{
					redirect('rents/tenants/'.$rental_unit_id);
				}

			}
		}

		//open the add new tenant page
	}
	public function allocate_tenant_to_unit($rental_unit_id)
	{
		$this->form_validation->set_rules('tenant_id', 'Tenant id', 'required|trim|xss_clean');

		if ($this->form_validation->run())
		{
			if($this->tenants_model->add_tenant_to_unit($rental_unit_id))
			{
				$this->session->unset_userdata('lease_error_message');
				$this->session->set_userdata('success_message', 'Tenant has been added successfully');
			}
			else
			{
				$this->session->set_userdata('error_message', 'Sorry please check for errors has been added successfully');
			}
		}
		else
		{
			$this->session->set_userdata('error_message', 'Please fill in all the fields');
		}
		redirect('tenants/'.$rental_unit_id);
	}
	public function allocate_tenant_to_unit_other($rental_unit_id)
	{
		//form validation rules
		$this->form_validation->set_rules('tenant_id', 'Tenant name', 'required|xss_clean');
		$this->form_validation->set_rules('rental_unit_id', 'Rental Unit', 'required|xss_clean');

		//if form has been submitted
		if ($this->form_validation->run())
		{
			// check if another tenant account is active
			$rental_unit_id = $this->input->post('rental_unit_id');
			$checker = $this->tenants_model->check_for_account($rental_unit_id);

			if($checker = TRUE)
			{
				$this->session->set_userdata('error_message', 'Seems Like another lease is still active, Please close the active lease before proceeding');
				redirect('rents/tenants/'.$rental_unit_id);
			}
			else
			{
				//check if tenant has valid login credentials
				if($this->tenants_model->allocate_tenant_to_rental_unit())
				{
					$this->session->unset_userdata('tenants_error_message');
					$this->session->set_userdata('success_message', 'Tenant has been successfully added');
					redirect('rents/tenants/'.$rental_unit_id);
				}

				else
				{
					$this->session->set_userdata('error_message', 'Sorry something went wrong. Please try again');
					redirect('rents/tenants/'.$rental_unit_id);

				}
			}

		}

		//open the add new tenant page
	}

	/*
	*
	*	Edit an existing tenant page
	*	@param int $tenant_id
	*
	*/
	public function edit_tenant($tenant_id)
	{
		//form validation rules
		$this->form_validation->set_rules('tenant_email', 'Email', 'xss_clean|exists[tenants.tenant_email]|valid_email');
		$this->form_validation->set_rules('tenant_name', 'Tenant name', 'required|xss_clean');
		$this->form_validation->set_rules('tenant_phone_number', 'Tenant Phone Number', 'required|xss_clean|exists[tenants.tenant_phone_number]');
		$this->form_validation->set_rules('tenant_national_id', 'National Id', 'xss_clean');

		//if form has been submitted
		if ($this->form_validation->run())
		{
			//check if tenant has valid login credentials
			if($this->tenants_model->edit_tenant($tenant_id))
			{
				$this->session->set_userdata('success_message', 'tenant edited successfully');
				$pwd_update = $this->input->post('admin_tenant');
				if(!empty($pwd_update))
				{
					redirect('admin-profile/'.$tenant_id);
				}

				else
				{
					redirect('admin/administrators');
				}
			}

			else
			{
				$data['error'] = 'Unable to add tenant. Please try again';
			}
		}

		//open the add new tenant page
		$data['title'] = 'Edit administrator';
		$v_data['title'] = $data['title'];

		//select the tenant from the database
		$query = $this->tenants_model->get_tenant($tenant_id);
		if ($query->num_rows() > 0)
		{
			$v_data['tenants'] = $query->result();
			$data['content'] = $this->load->view('tenants/edit_tenant', $v_data, true);
		}

		else
		{
			$data['content'] = 'tenant does not exist';
		}

		$this->load->view('admin/templates/general_page', $data);
	}

	/*
	*
	*	Delete an existing tenant page
	*	@param int $tenant_id
	*
	*/
	public function delete_tenant($tenant_id)
	{
		if($this->tenants_model->delete_tenant($tenant_id))
		{
			$this->session->set_userdata('success_message', 'Administrator has been deleted');
		}

		else
		{
			$this->session->set_userdata('error_message', 'Administrator could not be deleted');
		}

		redirect('admin/administrators');
	}

	/*
	*
	*	Activate an existing tenant page
	*	@param int $tenant_id
	*
	*/
	public function activate_tenant($tenant_id)
	{
		if($this->tenants_model->activate_tenant($tenant_id))
		{
			$this->session->set_userdata('success_message', 'Administrator has been activated');
		}

		else
		{
			$this->session->set_userdata('error_message', 'Administrator could not be activated');
		}

		redirect('admin/administrators');
	}

	/*
	*
	*	Deactivate an existing tenant page
	*	@param int $tenant_id
	*
	*/
	public function deactivate_tenant($tenant_id)
	{
		if($this->tenants_model->deactivate_tenant($tenant_id))
		{
			$this->session->set_userdata('success_message', 'Administrator has been disabled');
		}

		else
		{
			$this->session->set_userdata('error_message', 'Administrator could not be disabled');
		}

		redirect('admin/administrators');
	}

	/*
	*
	*	Reset a tenant's password
	*	@param int $tenant_id
	*
	*/
	public function reset_password($tenant_id)
	{
		$new_password = $this->login_model->reset_password($tenant_id);
		$this->session->set_userdata('success_message', 'New password is <br/><strong>'.$new_password.'</strong>');

		redirect('admin/administrators');
	}

	/*
	*
	*	Show an administrator's profile
	*	@param int $tenant_id
	*
	*/
	public function admin_profile($tenant_id)
	{
		//open the add new tenant page
		$data['title'] = 'Edit tenant';

		//select the tenant from the database
		$query = $this->tenants_model->get_tenant($tenant_id);
		if ($query->num_rows() > 0)
		{
			$v_data['tenants'] = $query->result();
			$v_data['admin_tenant'] = 1;
			$tab_content[0] = $this->load->view('tenants/edit_tenant', $v_data, true);
		}

		else
		{
			$data['tab_content'][0] = 'tenant does not exist';
		}
		$tab_name[1] = 'Overview';
		$tab_name[0] = 'Edit Account';
		$tab_content[1] = 'Coming soon';//$this->load->view('account_overview', $v_data, true);
		$data['total_tabs'] = 2;
		$data['content'] = $tab_content;
		$data['tab_name'] = $tab_name;

		$this->load->view('templates/tabs', $data);
	}
	public function change_password()
	{
		$this->form_validation->set_rules('current_password', 'Current Password', 'required|xss_clean');
		$this->form_validation->set_rules('new_password', 'New Password', 'required|xss_clean');
		$this->form_validation->set_rules('confirm_new_password', 'Confirm New Password', 'required|xss_clean');

		//if form has been submitted
		if ($this->form_validation->run())
		{
			if($this->input->post('new_password') == $this->input->post('confirm_new_password'))
			{
				if($this->tenants_model->change_password())
				{
					$this->session->set_userdata('success_message', 'Your password has been changed successfully');
				}
				else
				{
					$this->session->set_userdata('error_message', 'Something went wrong, please try again');
				}
			}
			else
			{
				$this->session->set_userdata('error_message', 'Ensure that the new password match with the confirm password');
			}
		}
		else
		{
			$this->session->set_userdata('error_message', 'Please check that all the fields have values');
		}
		redirect('dashboard');
	}

	public function import_payments()
	{

		$where = 'import_payment_upload_deleted = 0';
		$table = 'import_payment_upload';


		$import_payment_search = $this->session->userdata('all_import_payment_search');

		if(!empty($import_payment_search))
		{
			$where .= $import_payment_search;

		}
		$segment = 3;
		//pagination

		$this->load->library('pagination');
		$config['base_url'] = base_url().'import/payments';
		$config['total_rows'] = $this->tenants_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;

		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';

		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';

		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';

		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';

		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active">';
		$config['cur_tag_close'] = '</li>';

		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);

		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->accounts_model->get_all_failed($table, $where, $config["per_page"], $page, $order='property_id', $order_method='ASC');



		$v_data['title'] = $data['title'] = 'Import Tenant payments';

		$v_data['page'] = $page;
		$property_order = 'property_id';
		$property_table = 'property';
		$property_where = 'property_id > 0';

		$property_query = $this->property_model->get_active_list($property_table, $property_where, $property_order);
		$rs8 = $property_query->result();
		$property_list = '';
		foreach ($rs8 as $property_rs) :
			$property_id = $property_rs->property_id;
			$property_name = $property_rs->property_name;

		    $property_list .="<option value='".$property_id."'>".$property_name."</option>";

		endforeach;

		$v_data['property_list'] = $property_list;


		$v_data['query'] = $query;
		$data['content'] = $this->load->view('import/import_payments', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}

	public function payments_import_template()
	{
		$this->accounts_model->payments_import_template();
	}

	public function payments_import()
	{
		if(isset($_FILES['import_csv']))
		{
			if(is_uploaded_file($_FILES['import_csv']['tmp_name']))
			{
				//import products from excel
				$response = $this->accounts_model->import_csv_payroll($this->csv_path);

				if($response == FALSE)
				{
					$v_data['import_response_error'] = 'Something went wrong. Please try again.';
				}

				else
				{
					if($response['check'])
					{
						$v_data['import_response'] = $response['response'];
					}

					else
					{
						$v_data['import_response_error'] = $response['response'];
					}
				}
			}

			else
			{
				$v_data['import_response_error'] = 'Please select a file to import.';
			}
		}

		else
		{
			$v_data['import_response_error'] = 'Please select a file to import.';
		}

		$v_data['title'] = $data['title'] = 'Import Tenant payments';

		redirect('import/payments');
		// $data['content'] = $this->load->view('import/import_payments', $v_data, true);
		// $this->load->view('admin/templates/general_page', $data);
	}
	public function send_arrears($lease_id)
	{

		$month = date('m');
		$year = date('Y');

		$this->send_invoices($lease_id,$month,$year);

		$this->session->set_userdata('success_message', 'Message has been send successfully');

		redirect('accounts/tenants-accounts');
	}
	public function print_receipt($payment_id, $tenant_unit_id, $lease_id)
	{
		$data = array('payment_id' => $payment_id, 'tenant_unit_id' => $tenant_unit_id, 'lease_id' => $lease_id);
		$data['contacts'] = $this->site_model->get_contacts();
		$data['lease_payments'] = $this->accounts_model->get_lease_payments($lease_id);

		$data['lease_payments'] = $this->accounts_model->get_payment_details($payment_id);
		$data['payment_idd'] = $payment_id;

		$all_leases = $this->leases_model->get_lease_detail($lease_id);
		foreach ($all_leases->result() as $leases_row)
		{
			$lease_id = $leases_row->lease_id;
			$tenant_unit_id = $leases_row->tenant_unit_id;
			$property_name = $leases_row->property_name;
			$rental_unit_name = $leases_row->rental_unit_name;
			$tenant_name = $leases_row->tenant_name;
			$tenant_email = $leases_row->tenant_email;
			$lease_start_date = $leases_row->lease_start_date;
			$lease_duration = $leases_row->lease_duration;
			$rent_amount = $leases_row->rent_amount;
			$lease_number = $leases_row->lease_number;
			$arrears_bf = $leases_row->arrears_bf;
			$rent_calculation = $leases_row->rent_calculation;
			$deposit = $leases_row->deposit;
			$deposit_ext = $leases_row->deposit_ext;
			$tenant_phone_number = $leases_row->tenant_phone_number;
			$tenant_national_id = $leases_row->tenant_national_id;
			$lease_status = $leases_row->lease_status;
			$tenant_status = $leases_row->tenant_status;
			$created = $leases_row->created;

			$lease_start_date = date('jS M Y',strtotime($lease_start_date));

			// $expiry_date  = date('jS M Y',strtotime($lease_start_date, mktime()) . " + 365 day");
			$expiry_date  = date('jS M Y', strtotime(''.$lease_start_date.'+1 years'));

		}

		$contacts = $data['contacts'];

		// $this->load->view('receipts/tenants_receipt', $data);
	 	echo $this->load->view('receipts/tenants_receipt', $data,true);

		// $this->load->view('admin/templates/invoice_template', $v_data);
	}

	public function send_tenants_receipt($payment_id, $tenant_unit_id, $lease_id)
	{
		$data = array('payment_id' => $payment_id, 'tenant_unit_id' => $tenant_unit_id, 'lease_id' => $lease_id);
		$data['contacts'] = $this->site_model->get_contacts();
		$data['lease_payments'] = $lease_payments = $this->accounts_model->get_lease_payments($lease_id);

		$data['payment_details'] = $payment_details = $this->accounts_model->get_payment_details($payment_id);
		$data['payment_idd'] = $payment_id;

		$all_leases = $this->leases_model->get_lease_detail($lease_id);
		foreach ($all_leases->result() as $leases_row)
		{
			$lease_id = $leases_row->lease_id;
			$tenant_unit_id = $leases_row->tenant_unit_id;
			$property_name = $leases_row->property_name;
			$rental_unit_name = $leases_row->rental_unit_name;
			$tenant_name = $leases_row->tenant_name;
			$tenant_email = $leases_row->tenant_email;
			$lease_start_date = $leases_row->lease_start_date;
			$lease_duration = $leases_row->lease_duration;
			$rent_amount = $leases_row->rent_amount;
			$lease_number = $leases_row->lease_number;
			$arrears_bf = $leases_row->arrears_bf;
			$rent_calculation = $leases_row->rent_calculation;
			$deposit = $leases_row->deposit;
			$deposit_ext = $leases_row->deposit_ext;
			$tenant_phone_number = $leases_row->tenant_phone_number;
			$tenant_national_id = $leases_row->tenant_national_id;
			$lease_status = $leases_row->lease_status;
			$lease_number = $leases_row->lease_number;
			$tenant_status = $leases_row->tenant_status;
			$created = $leases_row->created;

			$lease_start_date = date('jS M Y',strtotime($lease_start_date));

			$expiry_date  = date('jS M Y', strtotime(''.$lease_start_date.'+1 years'));

		}

		$contacts = $data['contacts'];


		// $html = $this->load->view('receipts/tenants_receipt', $data, TRUE);
		$v_data['content'] = $this->load->view('receipts/tenants_receipt', $data, true);

		$html = $this->load->view('admin/templates/invoice_template', $v_data,true);
		// var_dump($html);die();


		if($payment_details->num_rows() > 0)
		{
			$y = 0;
			foreach ($payment_details->result() as $key)
			{
				$payment_id = $key->payment_id;
				$document_number = $key->document_number;
				$amount_paid = $key->amount_paid;
				$paid_by = $key->paid_by;
				$payment_date = $date_of_payment = $key->payment_date;
				$payment_created = $key->payment_created;
				$payment_created_by = $key->payment_created_by;

				$payment_date = date('jS M Y',strtotime($payment_date));
				$payment_created = date('jS M Y',strtotime($payment_created));
				$y++;
			}
		}
		// var_dump($tenant_phone_number);die();
		// $tenant_email = 'marttkip@gmail.com';
		if(!empty($tenant_email))
		{
			var_dump($html);die();
			$this->load->library('mpdf');

			// $document_number = date("ymdhis");
			$receipt_month_date = date('Y-m-d');
			$receipt_month = date('M',strtotime($receipt_month_date));
			$receipt_year = date('Y',strtotime($receipt_month_date));
			$title = date('ymdhis').$document_number.'-REC.pdf';
			$invoice = $title;


			$mpdf=new mPDF();
			$mpdf->WriteHTML($html);
			$mpdf->Output($title, 'F');

			while(!file_exists($title))
			{

			}
			$message['subject'] =  $document_number.' Receipt';
			$message['text'] = ' <p>Dear '.$tenant_name.',<br>
														<p>Receipt '.$document_number.' of KES '.$amount_paid.' has been raised on your account '.$lease_number.' on '.$payment_date.' .</p>
														<p>Kindly find attached document. </p>
														<p>Please do not hesitate to contact us for any clarification you may need.</p>
														<p>'.$contacts['company_name'].'</p>
														<p>P O Box 8936 - 30100 Eldoret, Kenya |   0700-633199, 0702 777717, 0738 777718 | '.$contacts['email'].'</p>
														';
			$sender_email = $contacts['email'];
			$shopping = "";
			$from = $contacts['email'];

			$button = '';
			$sender['email'] = $contacts['email'];
			$sender['name'] = $contacts['company_name'];
			$receiver['email'] = $tenant_email;
			$receiver['name'] = $tenant_name;
			$payslip = $title;
			// var_dump($tenant_email);die();

			// $this->email_model->send_sendgrid_mail($receiver, $sender, $message, $payslip);

			// save the invoice sent out on the database
			$insert_array = array('email'=>$receiver['email'],'client_name'=>$tenant_name,'email_body'=>$message['text'],'type_of_account'=>1,'email_attachment'=>$title,'date_created'=>date('Y-m-d'),'email_type'=>2);
			$this->db->insert('email',$insert_array);
		}
		// $tenant_phone_number = 704808007;
		// var_dump($tenant_phone_number);die();
		if(!empty($tenant_phone_number))
		{
			$date = date('jS M Y',strtotime(date('Y-m-d')));
			$explode = explode(' ',$tenant_name);
			$first_name = $explode[0];
			$message = 'Dear '.$first_name.', This is to acknowledge receipt of KES. '.number_format($amount_paid,2).' sent to  '.$contacts['company_name'].' for account '.$lease_number.' on '.$payment_date.'. Thank you, For help please call 0702777717. Allan & Bradley Co. LTD';
			// $this->accounts_model->sms($tenant_phone_number,$message,$tenant_name);


		}

		redirect('cash-office/payments/'.$tenant_unit_id.'/'.$lease_id.'');
	}

	public function print_owners_receipt($payment_id, $home_owner_unit_id, $rental_unit_id)
	{
		$data = array('payment_id' => $payment_id, 'home_owner_unit_id' => $home_owner_unit_id, 'rental_unit_id' => $rental_unit_id);
		$data['contacts'] = $this->site_model->get_contacts();
		// $data['lease_payments'] = $this->accounts_model->get_lease_payments_owners($rental_unit_id);

		$data['lease_payments'] = $this->accounts_model->get_owners_payment_details($payment_id);
		$data['payment_idd'] = $payment_id;

		$all_leases = $this->leases_model->get_owner_unit_detail($rental_unit_id);


		$contacts = $data['contacts'];

		$this->load->view('receipts/owners_receipt', $data);
	}


	public function send_owners_receipt($payment_id, $home_owner_unit_id, $rental_unit_id)
	{
		$data = array('payment_id' => $payment_id, 'home_owner_unit_id' => $home_owner_unit_id, 'rental_unit_id' => $rental_unit_id);
		$data['contacts'] = $this->site_model->get_contacts();
		$data['lease_payments'] = $this->accounts_model->get_lease_payments_owners($rental_unit_id);

		$data['payment_details'] = $this->accounts_model->get_owners_payment_details($payment_id);
		$data['payment_idd'] = $payment_id;

		$all_leases = $this->leases_model->get_owner_unit_detail($rental_unit_id);


		$contacts = $data['contacts'];

		$html = $this->load->view('receipts/owners_receipt', $data,TRUE);


		$this->load->library('mpdf');
		// var_dump($html);die();
		$document_number = date("ymdhis");
		$invoice_month_date = date('Y-m-d');
		$invoice_month = date('M',strtotime($invoice_month_date));
		$invoice_year = date('Y',strtotime($invoice_month_date));
		$title = $document_number.'-INV.pdf';
		$invoice_path = $this->invoice_path;
		$invoice = $invoice_path.'/'.$title;
		// echo $invoice;die();

		$mpdf=new mPDF();
		$mpdf->WriteHTML($html);
		$mpdf->Output($title, 'F');

		while(!file_exists($title))
		{

		}

		$all_leases = $this->leases_model->get_lease_detail_owners_receipt($home_owner_unit_id);
		// var_dump($all_leases); die();
		foreach ($all_leases->result() as $leases_row)
		{
			$rental_unit_id = $leases_row->rental_unit_id;
			$home_owner_id = $leases_row->home_owner_id;
			$home_owner_name = $leases_row->home_owner_name;
			$home_owner_email = $leases_row->home_owner_email;
			$home_owner_phone_number = $leases_row->home_owner_phone_number;
			$rental_unit_name = $leases_row->rental_unit_name;
			$property_name = $leases_row->property_name;
			$message_prefix = $leases_row->message_prefix;

		}

		$lease_payments = $this->accounts_model->get_lease_payments_owners($rental_unit_id);
		if($lease_payments->num_rows() > 0)
		{
			$y = 0;
			foreach ($lease_payments->result() as $key)
			{
				$payment_idd = $key->payment_id;
				$receipt_number = $key->receipt_number;
				$amount_paid = $key->amount_paid;
				$paid_by = $key->paid_by;
				$payment_date = $key->payment_date;
				$payment_created = $key->payment_created;
				$payment_created_by = $key->payment_created_by;
				$transaction_code = $key->transaction_code;
				$invoice_month_number = $key->month;

				$payment_date = date('jS M Y',strtotime($payment_date));
				$payment_created = date('jS M Y',strtotime($payment_created));
				$y++;
			}
		}

		// $home_owner_email = 'marttkip@gmail.com';
		if(!empty($home_owner_email))
		{
			$message['subject'] =  $document_number.' Receipt';
			$message['text'] = ' <p>Dear '.$home_owner_name.',<br>
									Please find attached receipt
									</p>
								';
			$sender_email = $contacts['email'];



			$shopping = "";
			$from = $contacts['email'];
			$home_owner_email = str_replace(';', '/', $home_owner_email);
			$home_owner_email = str_replace(' ', '', $home_owner_email);
			$email_array = explode('/', $home_owner_email);
			$total_rows_email = count($email_array);

			for($x = 0; $x < $total_rows_email; $x++)
			{
				$receiver['email'] = $home_owner_email = $email_array[$x];

				$button = '';
				$sender['email'] = $contacts['email'];
				$sender['name'] = $contacts['company_name'];
				// $receiver['email'] = $home_owner_email;
				$receiver['name'] = $home_owner_name;
				$payslip = $title;


				// $this->email_model->send_sendgrid_mail($receiver, $sender, $message, $payslip);

				// save the invoice sent out on the database

				$insert_array = array('email'=>$receiver['email'],'client_name'=>$home_owner_name,'email_body'=>$message['text'],'type_of_account'=>0,'email_attachment'=>$title,'date_created'=>date('Y-m-d'),'email_type'=>2);
				$this->db->insert('email',$insert_array);
			}

			//  save the item set out
		}
		// var_dump($tenant_phone_number);die();
		// send SMS to the person
		// $home_owner_phone_number = 71654;
		if(!empty($home_owner_phone_number))
		{
			$date = date('jS M Y',strtotime(date('Y-m-d')));
			// $tenant_array = explode(' ', $tenant_name);
			// $tenant_name = $tenant_array[0];
			$phone_number_array = explode('/', $home_owner_phone_number);
			$total_rows_phone_number = count($phone_number_array);

			for($x = 0; $x < $total_rows_phone_number; $x++)
			{
			   $home_owner_phone_number = $phone_number_array[$x];

				$message = 'Dear '.$home_owner_name.', Your payment of  KES. '.$amount_paid.' for '.$rental_unit_name.' has been successfully received '.$message_prefix;
				// $this->accounts_model->sms($home_owner_phone_number,$message,$home_owner_name);
			}

			// save the message sent out
			// $insert_array = array('phone_number'=>$home_owner_phone_number,'client_name'=>$home_owner_name,'type_of_account'=>1,'message'=>$message,'date_created'=>date('Y-m-d'),'sms_type'=>2);
			// $this->db->insert('sms',$insert_array);
			// save the message sent out

		}

			redirect('owners-payments/'.$home_owner_id.'/'.$rental_unit_id.'');
	}

	public function print_lease_statement($lease_id)
	{
		$data = array('lease_id' => $lease_id);
		$data['contacts'] = $this->site_model->get_contacts();
		$data['lease_payments'] = $this->accounts_model->get_lease_payments($lease_id);


		$this->load->view('cash_office/statement', $data);
	}
	public function print_invoice($lease_id,$invoice_month,$invoice_year,$lease_invoice_id=null)
	{
		$data = array('lease_id' => $lease_id,'invoice_month' => $invoice_month,'invoice_year' => $invoice_year,'lease_invoice_id'=>$lease_invoice_id);
		$data['contacts'] = $this->site_model->get_contacts();

		$all_leases = $this->leases_model->get_lease_detail($lease_id);
		foreach ($all_leases->result() as $leases_row)
		{
			$lease_id = $leases_row->lease_id;
			$tenant_unit_id = $leases_row->tenant_unit_id;
			$property_name = $leases_row->property_name;
			$property_id = $leases_row->property_id;
			// $units_name = $leases_row->units_name;
			$rental_unit_name = $leases_row->rental_unit_name;
			$tenant_name = $leases_row->tenant_name;
			$tenant_email = $leases_row->tenant_email;
			$lease_start_date = $leases_row->lease_start_date;
			$lease_duration = $leases_row->lease_duration;
			$rent_amount = $leases_row->rent_amount;
			$lease_number = $leases_row->lease_number;
			$arrears_bf = $leases_row->arrears_bf;
			$rent_calculation = $leases_row->rent_calculation;
			$deposit = $leases_row->deposit;
			$deposit_ext = $leases_row->deposit_ext;
			$tenant_phone_number = $leases_row->tenant_phone_number;
			$tenant_national_id = $leases_row->tenant_national_id;
			$lease_status = $leases_row->lease_status;
			$tenant_status = $leases_row->tenant_status;
			$created = $leases_row->created;

			$lease_start_date = date('jS M Y',strtotime($lease_start_date));

			$expiry_date  = date('jS M Y', strtotime(''.$lease_start_date.'+1 years'));

		}

		$contacts = $data['contacts'];


		// $invoice_template_name = $this->accounts_model->get_invoice_template(1,$property_id);

		echo $this->load->view('cash_office/tenants_statement', $data, true);

		// $this->load->view('admin/templates/invoice_template', $v_data);


	}
	public function edit_invoice($lease_id,$invoice_month,$invoice_year)
	{
		$data = array('lease_id' => $lease_id,'invoice_month' => $invoice_month,'invoice_year' => $invoice_year);
		$data['contacts'] = $this->site_model->get_contacts();
		$data['lease_invoice'] = $this->accounts_model->get_invoices_month($lease_id,$invoice_month,$invoice_year);

		$data['lease_payments'] = $this->accounts_model->get_lease_payments($lease_id);

		$all_leases = $this->leases_model->get_lease_detail($lease_id);
		foreach ($all_leases->result() as $leases_row)
		{
			$lease_id = $leases_row->lease_id;
			$tenant_unit_id = $leases_row->tenant_unit_id;
			$property_name = $leases_row->property_name;
			$property_id = $leases_row->property_id;
			// $units_name = $leases_row->units_name;
			$rental_unit_name = $leases_row->rental_unit_name;
			$tenant_name = $leases_row->tenant_name;
			$tenant_email = $leases_row->tenant_email;
			$lease_start_date = $leases_row->lease_start_date;
			$lease_duration = $leases_row->lease_duration;
			$rent_amount = $leases_row->rent_amount;
			$lease_number = $leases_row->lease_number;
			$arrears_bf = $leases_row->arrears_bf;
			$rent_calculation = $leases_row->rent_calculation;
			$deposit = $leases_row->deposit;
			$deposit_ext = $leases_row->deposit_ext;
			$tenant_phone_number = $leases_row->tenant_phone_number;
			$tenant_national_id = $leases_row->tenant_national_id;
			$lease_status = $leases_row->lease_status;
			$tenant_status = $leases_row->tenant_status;
			$created = $leases_row->created;

			$lease_start_date = date('jS M Y',strtotime($lease_start_date));

			// $expiry_date  = date('jS M Y',strtotime($lease_start_date, mktime()) . " + 365 day");
			$expiry_date  = date('jS M Y', strtotime(''.$lease_start_date.'+1 years'));

		}

		$contacts = $data['contacts'];

		$invoice_template_name = $this->accounts_model->get_invoice_template(1,$property_id);
		// $this->load->view('cash_office/water_charge_invoice', $data);
		$this->load->view('invoices/'.$invoice_template_name.'', $data);

	}
	public function print_owners_invoice($home_owner_id,$rental_unit_id,$invoice_month,$invoice_year)
	{
		$data = array('home_owner_id' => $home_owner_id,'rental_unit_id'=>$rental_unit_id,'invoice_month_parent' => $invoice_month,'invoice_year_parent' => $invoice_year);
		$data['contacts'] = $this->site_model->get_contacts();
		// $data['lease_invoice'] = $this->accounts_model->get_invoices_month_home_owners($home_owner_id,$invoice_month,$invoice_year);

		// $data['lease_payments'] = $this->accounts_model->get_lease_payments_owners($home_owner_id);

		$all_leases = $this->leases_model->get_lease_detail_owners($home_owner_id);


		foreach ($all_leases->result() as $leases_row)
		{
			$rental_unit_id = $leases_row->rental_unit_id;
			$home_owner_name = $leases_row->home_owner_name;
			$home_owner_email = $leases_row->home_owner_email;
			$home_owner_phone_number = $leases_row->home_owner_phone_number;
			$rental_unit_name = $leases_row->rental_unit_name;
			$property_name = $leases_row->property_name;
			$property_id = $leases_row->property_id;

		}

		$contacts = $data['contacts'];

		// $html = $this->load->view('cash_office/owner_invoice', $data,TRUE);

		$invoice_template_name = $this->accounts_model->get_invoice_template(0,$property_id);
		// var_dump($invoice_template_name); die();
		// $this->load->view('cash_office/water_charge_invoice', $data);
		$this->load->view('cash_office/'.$invoice_template_name.'', $data);

	}

	public function send_sms($payment_id, $tenant_unit_id, $lease_id, $phone = NULL, $message = NULL)
	{
		// $this->load->model('messaging/messaging_model');
		$message = $this->site_model->decode_web_name($message);
		$contacts = $this->site_model->get_contacts();
		$where = 'leases.lease_id = '.$lease_id.' AND leases.tenant_unit_id = tenant_unit.tenant_unit_id AND tenant_unit.tenant_id = tenants.tenant_id AND tenant_unit.rental_unit_id = rental_unit.rental_unit_id';
		$this->db->from('leases,tenant_unit,tenants,rental_unit');
		$this->db->select('tenant_name,rental_unit_name,tenant_phone_number,tenant_email');
		$this->db->where($where);
		$query = $this->db->get();

		$data = array('lease_id' => $lease_id);
		$data['contacts'] = $this->site_model->get_contacts();
		$data['lease_payments'] = $this->accounts_model->get_lease_payments($lease_id);


		$html = $this->load->view('cash_office/statement', $data);

		$this->load->library('mpdf');
		$title = $lease_id.'-'.$rental_unit_name.'-Statement.pdf';
		$statement_path = $this->statement_path;
		$statement = $statement_path.'/'.$title;

		$mpdf=new mPDF();
		$mpdf->WriteHTML($html);
		$mpdf->Output($title, 'F');

		if($query->num_rows > 0)
		{
			$date_today = date('Y-m-d');
			$date_exploded = explode('-', $date_today);
			$_year = $date_exploded[0];
			$_month = $date_exploded[1];
			$rows = $query->row();

			$tenant_name = $rows->tenant_name;
			$rental_unit_name = $rows->rental_unit_name;
			$tenant_phone_number = $rows->tenant_phone_number;
			$tenant_email  =$rows->tenant_email;

			$tenant_explode = explode(' ', $tenant_name);
			$name = $tenant_explode[0];


			if(!empty($phone))
			{
				//sms for that month,year n unit has been sent
				if($this->check_statement_sms_sent($name,$_year,$_month)==FALSE)
				{
					// $status = $this->accounts_model->sms($tenant_phone_number, $message,$name,$title);
					if($status == 'Success')
					{

						$this->session->set_userdata('success_message', 'Message sent successfully');
					}

					else
					{
						$this->session->set_userdata('error_message', 'Unable to send message'.$status);
					}
				}
			}

			else
			{
				$this->session->set_userdata('error_message', 'Please enter tenant\'s phone number to send message');
			}
			if(!empty($tenant_email))
			{
				$message['subject'] =  $document_number.' Statement';
				$message['text'] = ' <p>Dear '.$tenant_name.',<br>
										Please find attached your statement
										</p>
									';
				$sender_email = $contacts['email'];
				$shopping = "";
				$from = $contacts['email'];

				$button = '';
				$sender['email'] = $contacts['email'];
				$sender['name'] = $contacts['company_name'];
				$receiver['email'] = $tenant_email;
				$receiver['name'] = $tenant_name;
				$statement_file = $title;

				//check if statement was sent for that month,year for that unit
				if($this->statement_email_sent($_year,$_month,$tenant_unit_id,1)==FALSE)
				{
					$this->email_model->send_sendgrid_mail($receiver, $sender, $message, $statement_file);

					$statement_insert_array = array('email'=>$receiver['email'],'client_name'=>$tenant_name,'email_body'=>$message['text'],'email_attachment'=>$title,'date_created'=>date('Y-m-d'), 'tenant_unit_id'=>$tenant_unit_id, 'statement_year'=>$_year, 'statement_month'=>$_month, 'statement_email_sent'=>1);
					$this->db->insert('statement_email',$statement_insert_array);
				}
			}

		}
		else
		{
			$this->session->set_userdata('error_message', 'Sorry something went wrong please try again');
		}


		redirect('cash-office/payments/'.$tenant_unit_id.'/'.$lease_id);
	}
	public function send_notifications()
	{

		//  get all accounts with invoices for the month ...
		$month = date('m');
		$year = date('Y');

		$leases = $this->accounts_model->get_all_invoices($month,$year);
		// var_dump($leases); die();
		if($leases->num_rows() > 0)
		{
			foreach ($leases->result() as $key) {
				# code...
				$lease_id = $key->lease_id;
				$document_number = $key->document_number;
				$this->send_invoices($lease_id,$month,$year,$document_number);
			}
			$this->session->set_userdata('success_message', 'Notifications have been successfully sent');
		}
		else
		{
			$this->session->set_userdata('error_message', 'No notifications to be sent out');
		}
		redirect('accounts/tenants-accounts');
	}

	public function send_invoices($lease_id,$invoice_month,$invoice_year,$document_number=null,$lease_invoice_id)
	{
		$data = array('lease_id' => $lease_id,'invoice_month' => $invoice_month,'invoice_year' => $invoice_year,'document_number'=>$document_number,'lease_invoice_id'=>$lease_invoice_id);
		$data['contacts'] = $this->site_model->get_contacts();

		$all_leases = $this->leases_model->get_lease_detail($lease_id);

		foreach ($all_leases->result() as $leases_row)
		{
			$lease_id = $leases_row->lease_id;
			$tenant_unit_id = $leases_row->tenant_unit_id;
			$property_name = $leases_row->property_name;
			$property_id = $leases_row->property_id;
			// $units_name = $leases_row->units_name;
			$rental_unit_name = $leases_row->rental_unit_name;
			$rental_unit_id = $leases_row->rental_unit_id;
			$tenant_name = $leases_row->tenant_name;
			$tenant_email = $leases_row->tenant_email;
			$lease_start_date = $leases_row->lease_start_date;
			$lease_duration = $leases_row->lease_duration;
			$rent_amount = $leases_row->rent_amount;
			$lease_number = $leases_row->lease_number;
			$arrears_bf = $leases_row->arrears_bf;
			$rent_calculation = $leases_row->rent_calculation;
			$deposit = $leases_row->deposit;
			$deposit_ext = $leases_row->deposit_ext;
			$tenant_phone_number = $leases_row->tenant_phone_number;
			$tenant_national_id = $leases_row->tenant_national_id;
			$lease_status = $leases_row->lease_status;
			$tenant_status = $leases_row->tenant_status;
			$created = $leases_row->created;
			$message_prefix = $leases_row->message_prefix;
			$lease_end_date = $leases_row->lease_end_date;
			$lease_start_date = date('jS M Y',strtotime($lease_start_date));
			$expiry_date  = date('jS M Y', strtotime($lease_end_date));

		}

		$contacts = $data['contacts'];

		$data['rental_unit_id'] = $lease_id;

		$v_data['content'] = $this->load->view('cash_office/invoice_template', $data,true);
		$html = $this->load->view('admin/templates/invoice_template', $v_data,true);


		$this->load->library('mpdf');

		$invoice_month_date = date('Y-m-d');
		$invoice_month = date('M',strtotime($invoice_month_date));
		$invoice_year = date('Y',strtotime($invoice_month_date));
		$title = $document_number.'.pdf';
		$invoice_path = $this->invoice_path;
		$invoice = $invoice_path.'/'.$title;


		$mpdf=new mPDF();
		$mpdf->WriteHTML($html);
		$mpdf->Output($title, 'F');

		while(!file_exists($title))
		{

		}

		$todays_date = date('Y-m-d');
		$todays_month = date('m');
		$todays_year = date('Y');



		$tenants_response = $this->accounts_model->get_tenants_billings($lease_id);
		$total_arrears = $tenants_response['total_arrears'];
		$invoice_date = $tenants_response['invoice_date'];



		$current_invoice_amount = $this->accounts_model->get_latest_invoice_amount($lease_id,date('Y'),date('m'));
		$total_current_invoice = $current_invoice_amount + $current_balance - $total_paid_amount;


		$date_today = date('Y-m-d');
		$date_exploded = explode('-', $date_today);
		$_year = $date_exploded[0];
		$_month = $date_exploded[1];


		$tenant_name = ucfirst($tenant_name);



		if(!empty($tenant_email))
		{

			$message['subject'] =  $document_number.' Invoice';

			$message['text'] = ' <p>Dear '.$tenant_name.',<br>
									Please find attached invoice.
									</p>
								';
			$sender_email = $contacts['email'];
			$shopping = "";
			$from = $contacts['email'];

			$button = '';
			$sender['email']= $contacts['email'];
			$sender['name'] = $contacts['company_name'];
			$receiver['name'] = $tenant_name;
			$payslip = $title;

			$sender_email = 'marttkip@gmail.com';
			$tenant_email .= '/'.$sender_email.'';
			$email_array = explode('/', $tenant_email);
			$total_rows_email = count($email_array);

			for($x = 0; $x < $total_rows_email; $x++)
			{
				$receiver['email'] = $email_tenant = $email_array[$x];
				// $this->email_model->send_sendgrid_mail($receiver, $sender, $message, $payslip);

			}
		}

		if(!empty($tenant_phone_number))
		{
			$date = date('jS M Y',strtotime(date('Y-m-d')));
			$phone_array = explode('/', $tenant_phone_number);
			$total_rows = count($phone_array);

			for($r = 0; $r < $total_rows; $r++)
			{
				$tenant_phone_number = $phone_array[$r];

				$message = 'Dear '.$tenant_name.', Your current dues upto '.$invoice_month.' '.$invoice_year.'  is KES. '.number_format($total_arrears).' '.$message_prefix.' ';

				// $this->accounts_model->sms($tenant_phone_number,$message,$tenant_name);

			}
			// save the message sent out
		}

		$update_array['sent_status'] = 1;
		$this->db->where('document_number =  "'.$document_number.'" AND lease_id = '.$lease_id);
		$this->db->update('invoice',$update_array);

	}


	public function send_notifications_home_owners()
	{

		// $leases = $this->accounts_model->get_all_invoices_home_owners($month,$year);
		$leases = $this->accounts_model->get_all_owners_lists();
		// var_dump($leases); die();
		if($leases->num_rows() > 0)
		{
			foreach ($leases->result() as $key) {
				# code...
				$rental_unit_id = $key->rental_unit_id;
				$home_owner_id = $key->home_owner_id;
				$home_owner_unit_id = $key->home_owner_unit_id;

				$latest_month = $this->accounts_model->get_client_latest_invoice($home_owner_id,$rental_unit_id);
				if(!empty($latest_month))
				{
				   $latest_month_explode = explode('-', $latest_month);
				   $month = $latest_month_explode[1];
				   $year = $latest_month_explode[0];
				   // var_dump($month); die();
				   $this->send_invoices_home_owners($home_owner_id,$rental_unit_id,$month,$year,$latest_month);

				}
			}
			$this->session->set_userdata('success_message', 'Notifications have been successfully sent');
		}
		else
		{
			$this->session->set_userdata('error_message', 'No notifications to be sent out');
		}
		redirect('cash-office/owners');
	}
	public function send_invoices_home_owners($home_owner_id,$rental_unit_id,$invoice_month,$invoice_year,$invoice_date_parent=NULL)
	{


		$data = array('home_owner_id' => $home_owner_id,'rental_unit_id' => $rental_unit_id,'invoice_month' => $invoice_month,'invoice_year' => $invoice_year,'invoice_month_parent'=>$invoice_month,'invoice_year_parent'=>$invoice_year);

		$data['contacts'] = $this->site_model->get_contacts();


		$all_leases = $this->leases_model->get_lease_detail_owners($home_owner_id);

		$total_bill = 0;
		$rental = '';
		$x = 0;
		foreach ($all_leases->result() as $leases_row)
		{
			$rental_unit_id = $leases_row->rental_unit_id;
			$home_owner_id = $leases_row->home_owner_id;
			$home_owner_name = $leases_row->home_owner_name;
			$home_owner_email = $leases_row->home_owner_email;
			$home_owner_phone_number = $leases_row->home_owner_phone_number;
			$rental_unit_name = $leases_row->rental_unit_name;
			$property_name = $leases_row->property_name;
			$property_id = $leases_row->property_id;



			$rental .= $rental_unit_name;
			$x++;
			if($x < $all_leases->num_rows())
			{
				$rental .= ',';
			}
			$todays_date = date('Y-m-d');
			$todays_month = date('m');
			$todays_year = date('Y');


			$owners_response = $this->accounts_model->get_owners_billings($rental_unit_id,$home_owner_id);
			$total_bills = $owners_response['total_arrears'];
			$invoice_date = $owners_response['invoice_date'];

			$total_bill = $total_bill + $total_bills;
		}

		$contacts = $data['contacts'];



		$invoice_template_name = $this->accounts_model->get_invoice_template(0,$property_id);
		// $this->load->view('cash_office/water_charge_invoice', $data);
		$html = $this->load->view('cash_office/'.$invoice_template_name.'', $data, TRUE);

		$this->load->library('mpdf');
		// var_dump($html);die();
		// $document_number = date("ymdhis");
		$document_number = 'U'.$rental_unit_id.'-'.$invoice_month.'-'.$invoice_year;
		if(!empty($invoice_date_parent))
		{
			$invoice_month_date = $invoice_date_parent;
		}
		else
		{
			$invoice_month_date = date('Y-m-d');
		}

		// var_dump($invoice_month_date);die();
		$invoice_month = date('M',strtotime($invoice_month_date));
		$invoice_year = date('Y',strtotime($invoice_month_date));
		$title = $document_number.'-INV.pdf';
		$invoice_path = $this->invoice_path;
		$invoice = $invoice_path.'/'.$title;
		// echo $invoice;die();

		$mpdf=new mPDF();
		$mpdf->WriteHTML($html);

		$mpdf->Output($title, 'F');

		while(!file_exists($title))
		{

		}
		// echo $title; die();

		$date_today = date('Y-m-d');
		$date_exploded = explode('-', $date_today);
		$_year = $date_exploded[0];
		$_month = $date_exploded[1];

		// $home_owner_email = "marttkip@gmail.com/accounts@homesandabroad.co.ke";
		// var_dump($home_owner_email); die();
		$home_owner_name = ucfirst($home_owner_name);
		if(!empty($home_owner_email))
		{
			if($property_id == 2)
			{

				$message['subject'] =  $invoice_month.' Service Charge Invoice';
			}
			else
			{
				$message['subject'] =  $document_number.' Invoice';
			}
			$message['subject'] =  $document_number.' Invoice';
			$message['text'] = ' <p>Dear '.$home_owner_name.',<br>
									Please find attached invoice
									</p>
								';
			$sender_email = $contacts['email'];
			$shopping = "";
			$from = $contacts['email'];;
			$button = '';
			$sender['email'] = $contacts['email'];
			$sender['name'] = $contacts['company_name'];

			$receiver['name'] = $home_owner_name;
			$payslip = $title;

			$sender_email = 'homesandabroad@gmail.com';
			$home_owner_email .= '/'.$sender_email.'';


			$email_array = explode('/', $home_owner_email);
			$total_rows_email = count($email_array);

			for($x = 0; $x < $total_rows_email; $x++)
			{
				$receiver['email'] = $owner_email = $email_array[$x];

				// if($this->accounts_model->check_if_email_sent($rental_unit_id,$owner_email,$todays_year,$todays_month,0))
				// {

				// }
				// else
				// {
					// $this->email_model->send_sendgrid_mail($receiver, $sender, $message, $payslip);

					$insert_array = array('email'=>$receiver['email'],'client_name'=>$home_owner_name,'email_body'=>$message['text'],'type_of_account'=>0,'email_attachment'=>$title,'date_created'=>date('Y-m-d'),'rental_unit_id'=>$rental_unit_id, 'invoice_year'=>$_year, 'invoice_month'=>$_month, 'email_sent'=>1);
					$this->db->insert('email',$insert_array);
				// }

			}

		}

		// send SMS to the person
		// $home_owner_phone_number = 704808007;//724330022;
		if(!empty($home_owner_phone_number))
		{
			$owner_array = explode(' ', $home_owner_email);
			$first = $owner_array[0];
			$second = $owner_array[0];

			$name = $first.' '.$second;
			$name = ucfirst($name);
			$date = date('jS M Y',strtotime(date('Y-m-d')));
			// $home_owner_phone_number = 716548104;//724330022;


			$phone_array = explode('/', $home_owner_phone_number);
			$total_rows = count($phone_array);

			for($r = 0; $r < $total_rows; $r++)
			{
				$home_owner_phone = $phone_array[$r];

				// var_dump($tenant_phone_number); die();
				// if($this->accounts_model->check_if_sms_sent($rental_unit_id,$home_owner_phone,$todays_year,$todays_month,0))
				// {
				// }
				// else
				// {
					$message = 'Dear '.$home_owner_name.', Your current service charge dues for '.$rental.' as at '.$date.' is KES. '.number_format($total_bill).'';
					// $message = 'Dear '.$home_owner_name.', Contribution towards debt repayment for '.$rental.'  is KES. '.number_format(50000).'';
					// $this->accounts_model->sms($home_owner_phone,$message,$home_owner_name);

					// save the message sent out
					$insert_array = array('phone_number'=>$home_owner_phone,'client_name'=>$home_owner_name,'type_of_account'=>0,'message'=>$message,'date_created'=>date('Y-m-d'),'rental_unit_id'=>$rental_unit_id, 'invoice_year'=>$invoice_year, 'invoice_month'=>$invoice_month, 'sms_sent'=>1);
					// $this->db->insert('sms',$insert_array);
				// }

			 }
			//
			// save the message sent out

		}

	}


	/*
	*
	*	Default action is to show all the tenants
	*
	*/
	public function owners()
	{
		$where = 'property.property_id = rental_unit.property_id AND home_owners.home_owner_id = home_owner_unit.home_owner_id AND home_owner_unit.rental_unit_id = rental_unit.rental_unit_id AND  home_owner_unit.home_owner_unit_status = 1';
		$home_owner_property_search = $this->session->userdata('home_owner_property_search');
		// var_dump($home_owner_property_search); die();
		if(!empty($home_owner_property_search))
		{
			$where .= $home_owner_property_search;

		}
		$table = 'rental_unit,property,home_owners,home_owner_unit';

		$segment = 3;
		//pagination

		$this->load->library('pagination');
		$config['base_url'] = base_url().'cash-office/owners';
		$config['total_rows'] = $this->tenants_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;

		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';

		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';

		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';

		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';

		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active">';
		$config['cur_tag_close'] = '</li>';

		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);

		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->accounts_model->get_all_unit_owners($table, $where, $config["per_page"], $page, $order='rental_unit.rental_unit_id,rental_unit.rental_unit_name,property.property_name', $order_method='ASC');

		$properties = $this->property_model->get_active_property();

		// $this->accounts_model->update_invoices();


		$rs8 = $properties->result();
		$property_list = '';
		foreach ($rs8 as $property_rs) :
			$property_id = $property_rs->property_id;
			$property_name = $property_rs->property_name;
			$property_location = $property_rs->property_location;

		    $property_list .="<option value='".$property_id."'>".$property_name." Location: ".$property_location."</option>";

		endforeach;
		$v_data['property_list'] = $property_list;
		$data['title'] = 'All Unit Owners';
		$v_data['title'] = $data['title'];
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		$data['content'] = $this->load->view('cash_office/unit_owners', $v_data, true);

		$this->load->view('admin/templates/general_page', $data);
	}

	public function owners_old()
	{
		$where = 'property.property_id = rental_unit.property_id AND rental_unit_id > 0';
		$rental_unit_search = $this->session->userdata('all_rental_unit_search');

		if(!empty($rental_unit_search))
		{
			$where .= $rental_unit_search;

		}
		$table = 'rental_unit,property';

		$segment = 3;
		//pagination

		$this->load->library('pagination');
		$config['base_url'] = base_url().'cash-office/owners';
		$config['total_rows'] = $this->tenants_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;

		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';

		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';

		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';

		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';

		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active">';
		$config['cur_tag_close'] = '</li>';

		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);

		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->accounts_model->get_all_unit_owners($table, $where, $config["per_page"], $page, $order='rental_unit.rental_unit_id,rental_unit.rental_unit_name,property.property_name', $order_method='ASC');

		$properties = $this->property_model->get_active_property();

		// $this->accounts_model->update_invoices();


		$rs8 = $properties->result();
		$property_list = '';
		foreach ($rs8 as $property_rs) :
			$property_id = $property_rs->property_id;
			$property_name = $property_rs->property_name;
			$property_location = $property_rs->property_location;

		    $property_list .="<option value='".$property_id."'>".$property_name." Location: ".$property_location."</option>";

		endforeach;
		$v_data['property_list'] = $property_list;
		$data['title'] = 'All Unit Owners';
		$v_data['title'] = $data['title'];
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		$data['content'] = $this->load->view('cash_office/unit_owners', $v_data, true);

		$this->load->view('admin/templates/general_page', $data);
	}

	public function send_owners($home_owner_id,$rental_unit_id)
	{
		$latest_month = $this->accounts_model->get_client_latest_invoice($home_owner_id,$rental_unit_id);
		if(!empty($latest_month))
		{
		   $latest_month_explode = explode('-', $latest_month);
		   $month = $latest_month_explode[1];
		   $year = $latest_month_explode[0];

			$this->send_invoices_home_owners($home_owner_id,$rental_unit_id,$month,$year,$latest_month);

			$this->session->set_userdata('success_message', 'Notification have been successfully sent');
		}
		else
		{
			$this->session->set_userdata('error_message', 'Nothing to send as notification');
		}

		redirect('cash-office/owners');
	}
	public function owners_payments($home_owner_id,$rental_unit_id)
	{


		$v_data = array('rental_unit_id'=>$rental_unit_id,'home_owner_id'=>$home_owner_id);
		$v_data['title'] = 'Owner payments';
		$v_data['cancel_actions'] = $this->accounts_model->get_cancel_actions();
		// get lease payments for this year
		$v_data['rental_payments'] = $this->accounts_model->get_lease_payments_owners($rental_unit_id);
		$v_data['lease_pardons'] = $this->accounts_model->get_lease_pardons_owner($rental_unit_id);

		$data['content'] = $this->load->view('cash_office/owner_payments', $v_data, true);

		$data['title'] = 'Tenant payments';

		$this->load->view('admin/templates/general_page', $data);
	}
	public function update_changes()
	{
		$month = date('m');
		$year = date('Y');

		$this->db->select("*");
		$this->db->where('invoice_month = "'.$month.'" AND invoice_year= "'.$year.'" AND invoice_type = 2');
		$query = $this->db->get('invoice');
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key) {
				# code...
				$invoice_id = $key->invoice_id;
				$lease_id = $key->lease_id;
				$invoice_amount = $key->invoice_amount;
				$invoice_month = $key->invoice_month;
				$invoice_year = $key->invoice_year;

				$current_invoice_amount = 1100;

				$array = array('invoice_amount' => $current_invoice_amount);

				$this->db->where('invoice_id',$invoice_id);
				$this->db->update('invoice',$array);
			}
		}

		redirect('accounts/tenants-accounts');
	}

	public function print_other_invoices()
	{
		$this->db->select('*');
		// $this->db->where('property.property_id = rental_unit.property_id AND rental_unit.rental_unit_id AND rental_unit.rental_unit_id > 2016 AND rental_unit.rental_unit_id < 2026 ');
		$this->db->where('property.property_id = rental_unit.property_id AND rental_unit.rental_unit_id AND rental_unit.rental_unit_id = 2047');
		$query = $this->db->get('rental_unit,property');
		$data['query'] = $query;
		$data['contacts'] = $this->site_model->get_contacts();

		foreach ($query->result() as $leases_row)
		{
			$rental_unit_id = $leases_row->rental_unit_id;
			$rental_unit_name = $leases_row->rental_unit_name;
			$property_name = $leases_row->property_name;


			$becam_email = 'preet.sen@hotmail.com';
			$becam_phone = 715770467;

		}

		$contacts = $data['contacts'];

		// $this->load->view('cash_office/other_water_charge_invoice', $data);
		$this->load->view('cash_office/service_charge_invoice', $data);
	}
	public function send_other_invoices()
	{
		$this->db->select('*');
		// $this->db->where('property.property_id = rental_unit.property_id AND rental_unit.rental_unit_id AND rental_unit.rental_unit_id > 2016 AND rental_unit.rental_unit_id < 2026 ');
		// $this->db->where('property.property_id = rental_unit.property_id AND rental_unit.rental_unit_id AND rental_unit.rental_unit_id = 2038 ');
		// $this->db->where('property.property_id = rental_unit.property_id AND rental_unit.rental_unit_id AND rental_unit.rental_unit_id > 2040 AND rental_unit.rental_unit_id < 2043');
		$this->db->where('property.property_id = rental_unit.property_id AND rental_unit.rental_unit_id AND rental_unit.rental_unit_id = 2047');
		// AND  rental_unit.rental_unit_id < 2039
		$query = $this->db->get('rental_unit,property');
		$data['query'] = $query;
		$data['contacts'] = $this->site_model->get_contacts();
		$total_bill = 0;
		foreach ($query->result() as $leases_row)
		{
			$rental_unit_id = $leases_row->rental_unit_id;
			$rental_unit_name = $leases_row->rental_unit_name;
			$property_name = $leases_row->property_name;
			$message_prefix = $leases_row->message_prefix;
			$home_owner_name = $leases_row->home_owner_name;
			$home_owner_phone_number = $leases_row->home_owner_phone_number;
			$home_owner_email = $leases_row->home_owner_email;
			$invoice_month = date('m');
			$invoice_year = date('Y');
			$lease_invoice = $this->accounts_model->get_invoices_month_home_owners($rental_unit_id,$invoice_month,$invoice_year);
			if($lease_invoice->num_rows() > 0)
			{

				$service_charge = 0;
				$service_bf = 0;
				$penalty_charge =0;
				$penalty_bf =0;

				foreach ($lease_invoice->result() as $key_invoice) {
					# code...
					$invoice_date = $key_invoice->invoice_date;
					$invoice_id = $key_invoice->invoice_id;
					$invoice_type = $key_invoice->invoice_type;


					$invoice_date_date = date('jS F Y',strtotime($invoice_date));


					if($invoice_type == 4)
					{
						// service charge
						$service_charge = $key_invoice->invoice_amount;
						$service_bf = $key_invoice->arrears_bf;
					}


					if($invoice_type == 5)
					{
						// penalty

						$penalty_charge = $key_invoice->invoice_amount;
						$penalty_bf = $key_invoice->arrears_bf;
					}
				}
			}


			$total_bill = $total_bill + $service_charge + $penalty_charge;

			// $becam_email = 'marttkip@gmail.com';//'oyugierick3@gmail.com';


		}

		$contacts = $data['contacts'];

		$html = $this->load->view('cash_office/service_charge_invoice', $data, TRUE);

		$this->load->library('mpdf');
		// var_dump($html);die();
		$invoice_month_date = date('Y-m-d');
		$invoice_month = date('M',strtotime($invoice_month_date));
		$invoice_year = date('Y',strtotime($invoice_month_date));
		$title = 'Invoice-for-'.$invoice_month.'-'.$invoice_year.'.pdf';
		$invoice_path = $this->invoice_path;
		$invoice = $invoice_path.'/'.$title;
		// echo $invoice;die();

		$mpdf=new mPDF();
		$mpdf->WriteHTML($html);
		$mpdf->Output($title, 'F');

		while(!file_exists($title))
		{

		}
		if(!empty($home_owner_email))
		{
			$message['subject'] = $invoice_month.' '.$invoice_year.' Invoice';
			$message['text'] = ' <p>Dear '.$home_owner_name.',<br>
									Please find attached invoice for Q4 2016
									</p>
								';
			$sender_email = $contacts['email'];
			$shopping = "";
			$from = $contacts['email'];

			$button = '';
			$sender['email'] = $contacts['email'];
			$sender['name'] = $contacts['company_name'];
			$receiver['email'] = $home_owner_email;
			$receiver['name'] = $home_owner_name;
			$payslip = $title;

			// $this->email_model->send_sendgrid_mail($receiver, $sender, $message, $payslip);
		}

		// send SMS to the person
		if(!empty($home_owner_phone_number))
		{
			$date = date('jS M Y',strtotime(date('Y-m-d')));
			$message = 'Dear '.$home_owner_name.', Your current service charge dues for Q4 '.$invoice_year.' is KES. '.number_format($total_bill).'';
			// $this->accounts_model->sms($home_owner_phone_number,$message,$home_owner_name);

		}
	}

	public function update_payment_item($import_payment_id)
	{

		if($this->accounts_model->update_payment_item($import_payment_id))
		{
			$this->session->set_userdata('success_message', 'Successfully corrected the payment detail');
		}
		else
		{
			$this->session->set_userdata('error_message', 'Please check the details and try again ');
		}
		redirect('import/payments');
	}
	public function delete_payment_item($import_payment_id)
	{
		$update_array = array('import_payment_upload_deleted'=>1);
		$this->db->where('import_payment_id',$import_payment_id);
		if($this->db->update('import_payment_upload',$update_array))
		{
			$this->session->set_userdata('success_message', 'Successfully corrected the record');
		}
		else
		{
			$this->session->set_userdata('error_message', 'Please check the details and try again ');
		}

		redirect('import/payments');
	}




	public function tenants_payments()
	{
			// $v_data['property_list'] = $property_list;
			$data['title'] = 'Tenants Payments ';
			$v_data['title'] = $data['title'];
			$v_data['query'] = $query;
			$v_data['page'] = $page;
			$data['content'] = $this->load->view('receipts/tenants_payments', $v_data, true);

			$this->load->view('admin/templates/general_page', $data);
	}
	public function search_tenants_payments($lease_id = NULL)
	{
		// var_dump($leas);die();
		if(empty($lease_id))
		{
			$lease_id = $lease_id_searched = $this->input->post('lease_id');
		}
		
		$lease_id_searched = $lease_id;
		$search_title = '';

		if(!empty($lease_id))
		{
			$this->db->where('lease_id', $lease_id);


			$lease_id = ' AND leases.lease_id = '.$lease_id.' ';


		}

		$search = $lease_id;

		// var_dump($search);die();

		$this->session->set_userdata('lease_id_searched', $lease_id_searched);
		$this->session->set_userdata('search_tenants_payments', $search);
		redirect('cash-office/tenants-payments');
	}

	public function close_searched_payment_lease()
	{
		$this->session->unset_userdata('lease_id_searched');
		$this->session->unset_userdata('search_tenants_payments');
		redirect('cash-office/tenants-leases');
	}

	public function update_payments_receipt()
	{
		$this->db->where('payment_id > 0');
		$query = $this->db->get('payments');

		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key) {
				# code...
				$payment_id = $key->payment_id;

				$receipt_number = $this->accounts_model->create_receipt_number();
				// var_dump($receipt_number);
				$array = array('receipt_number'=>$receipt_number);
				$this->db->where('payment_id',$payment_id);
				$this->db->update('payments',$array);
			}
		}
	}

	public function update_owners_payments_receipt()
	{
		$this->db->where('payment_id > 0');
		$query = $this->db->get('home_owners_payments');

		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key) {
				# code...
				$payment_id = $key->payment_id;

				$receipt_number = $this->accounts_model->create_owners_receipt_number();

				$array = array('receipt_number'=>$receipt_number);
				$this->db->where('payment_id',$payment_id);
				$this->db->update('home_owners_payments',$array);
			}
		}
	}
	public function lease_invoices($lease_id)
	{



		$where = 'invoice_status = 1 AND lease_id = '.$lease_id ;
		$table = 'invoice';


		$segment = 3;
		//pagination

		$this->load->library('pagination');
		$config['base_url'] = base_url().'lease-invoices/'.$lease_id;
		$config['total_rows'] = $this->tenants_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;

		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';

		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';

		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';

		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';

		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active">';
		$config['cur_tag_close'] = '</li>';

		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);

		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->accounts_model->get_all_lease_invoices($table, $where, $config["per_page"], $page, $order='invoice.invoice_month', $order_method='DESC');
		// var_dump($where); die();
		$properties = $this->property_model->get_active_property();

		$rs8 = $properties->result();
		$property_list = '';
		foreach ($rs8 as $property_rs) :
			$property_id = $property_rs->property_id;
			$property_name = $property_rs->property_name;
			$property_location = $property_rs->property_location;

		    $property_list .="<option value='".$property_id."'>".$property_name." Location: ".$property_location."</option>";

		endforeach;
		$v_data['property_list'] = $property_list;
		$data['title'] = 'Lease Invoices ';
		$v_data['title'] = $data['title'];
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		$v_data['lease_id'] = $lease_id;
		$data['content'] = $this->load->view('edits/lease_invoices', $v_data, true);

		$this->load->view('admin/templates/general_page', $data);
	}

	public function lease_payments($lease_id)
	{
		$where = 'payments.payment_id = payment_item.payment_id AND payment_status = 1 AND lease_id = '.$lease_id ;
		$table = 'payments,payment_item';


		$segment = 3;
		//pagination

		$this->load->library('pagination');
		$config['base_url'] = base_url().'lease-payments/'.$lease_id;
		$config['total_rows'] = $this->tenants_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;

		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';

		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';

		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';

		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';

		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active">';
		$config['cur_tag_close'] = '</li>';

		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);

		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->accounts_model->get_all_lease_payments($table, $where, $config["per_page"], $page, $order='payments.month,payments.year', $order_method='DESC');
		// var_dump($where); die();
		$properties = $this->property_model->get_active_property();

		$rs8 = $properties->result();

		$property_list = '';
		foreach ($rs8 as $property_rs) :
			$property_id = $property_rs->property_id;
			$property_name = $property_rs->property_name;
			$property_location = $property_rs->property_location;

		    $property_list .="<option value='".$property_id."'>".$property_name." Location: ".$property_location."</option>";

		endforeach;


		$v_data['property_list'] = $property_list;
		$data['title'] = 'Lease Payments ';
		$v_data['title'] = $data['title'];
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		$v_data['lease_id'] = $lease_id;
		$data['content'] = $this->load->view('edits/lease_payments', $v_data, true);

		$this->load->view('admin/templates/general_page', $data);
	}
	public function update_account_invoice($lease_id,$invoice_type_id,$invoice_month,$invoice_year,$invoice_id)
	{
		if($invoice_type_id == 2)
		{
			// water charge
			$current_reading = $this->input->post('water_curr_reading');
			$previous_reading = $this->input->post('water_prev_reading');

			// get the water invoice id
			$record_id = $this->accounts_model->get_water_record_id($invoice_id);

			//  update the balance on water management

			if(!empty($record_id))
			{
				$rate = 110;
				if($this->accounts_model->update_water_management($record_id,$rate))
				{

					// update the invoice

					$consumption = $current_reading - $previous_reading;

					$invoice_amount = $consumption * $rate;

					$update_array = array('invoice_amount'=>$invoice_amount);
					$this->db->where('invoice_id',$invoice_id);
					if($this->db->update('invoice',$update_array))
					{
						$this->session->set_userdata('success_message', 'Successfully updated water charge');
					}
					else
					{
						$this->session->set_userdata('error_message', 'Please try again to update the record');
					}
				}
				else
				{
					$this->session->set_userdata('error_message', 'Please try again to update the record');
				}
			}


		}
		if($invoice_type_id == 4 || $invoice_type_id == 8 || $invoice_type_id == 7 || $invoice_type_id == 9 || $invoice_type_id == 5)
		{
			// service charge

			$invoice_amount = $this->input->post('amount');
			$update_array = array('invoice_amount'=>$invoice_amount);
			$this->db->where('invoice_id',$invoice_id);
			if($this->db->update('invoice',$update_array))
			{
				$this->session->set_userdata('success_message', 'Successfully updated service charge');
			}
			else
			{
				$this->session->set_userdata('error_message', 'Please try again to update the record');
			}
		}
		if($invoice_type_id == 10)
		{
			$bought_rate = 0.5;
			$invoice_amount = $this->input->post('amount');

			$invoice_amount = $invoice_amount * 0.5;
			// var_dump($invoice_amount); die();
			$update_array = array('invoice_amount'=>$invoice_amount);
			$this->db->where('invoice_id',$invoice_id);
			if($this->db->update('invoice',$update_array))
			{
				$this->session->set_userdata('success_message', 'Successfully updated service charge');
			}
			else
			{
				$this->session->set_userdata('error_message', 'Please try again to update the record');
			}
		}

		redirect('lease-invoices/'.$lease_id);
	}

	public function update_account_payment($lease_id,$payment_idd,$property_id)
	{

		$total_amount = $this->input->post('amount_paid_total');
		$payment_date_from = $this->input->post('payment_date_from');


		$property_invoices = $this->accounts_model->get_property_invoice_types($property_id,1);

		if($property_invoices->num_rows() > 0)
		{
			// var_dump($property_invoices->result()); die();
			foreach ($property_invoices->result() as $key_types) {
				# code...
				$property_invoice_type_id = $key_types->invoice_type_id;

				// update payment item
				$payments_list = $this->accounts_model->get_payments_month_payment($payment_idd,$property_invoice_type_id);
				if($payments_list->num_rows() > 0)
				{

					foreach ($payments_list->result() as $key_invoice) {
						$payment_item_id = $key_invoice->payment_item_id;

						$amount_paid = $this->input->post('amount_paid'.$payment_item_id);


						// update value
						$update_array = array('amount_paid'=>$amount_paid);
						$this->db->where('payment_item_id',$payment_item_id);
						$this->db->update('payment_item',$update_array);

					}

				}
				else
				{

					// insert for the other item
					$amount_paid = $this->input->post('insert_amount'.$property_invoice_type_id);


					$insert_array = array('amount_paid'=>$amount_paid,'payment_id'=>$payment_idd,'invoice_type_id'=>$property_invoice_type_id,'lease_id'=>$lease_id,'payment_item_created'=>date('Y-m-d'));

					$this->db->insert('payment_item',$insert_array);

				}
			}
			$this->session->set_userdata('success_message', 'Successfully updated items');

		}
		else
		{
			$this->session->set_userdata('error_message', 'Could not update item');
		}

		redirect('lease-payments/'.$lease_id);
	}


	public function update_owner_account_payment($rental_unit_id,$payment_idd,$property_id)
	{

		$total_amount = $this->input->post('amount_paid_total');
		$payment_date_from = $this->input->post('payment_date_from');


		$property_invoices = $this->accounts_model->get_property_invoice_types($property_id,0);

		if($property_invoices->num_rows() > 0)
		{
			// var_dump($property_invoices->result()); die();
			foreach ($property_invoices->result() as $key_types) {
				# code...
				$property_invoice_type_id = $key_types->invoice_type_id;

				// update payment item
				$payments_list = $this->accounts_model->get_owner_payments_month_payment($payment_idd,$property_invoice_type_id);
				if($payments_list->num_rows() > 0)
				{

					foreach ($payments_list->result() as $key_invoice) {
						$payment_item_id = $key_invoice->payment_item_id;

						$amount_paid = $this->input->post('amount_paid'.$payment_item_id);


						// update value
						$update_array = array('amount_paid'=>$amount_paid);
						$this->db->where('payment_item_id',$payment_item_id);
						$this->db->update('home_owner_payment_item',$update_array);

					}

				}
				else
				{

					// insert for the other item
					$amount_paid = $this->input->post('insert_amount'.$property_invoice_type_id);


					$insert_array = array('amount_paid'=>$amount_paid,'payment_id'=>$payment_idd,'invoice_type_id'=>$property_invoice_type_id,'rental_unit_id'=>$rental_unit_id,'payment_item_created'=>date('Y-m-d'));

					$this->db->insert('home_owner_payment_item',$insert_array);

				}
			}
			$this->session->set_userdata('success_message', 'Successfully updated items');

		}
		else
		{
			$this->session->set_userdata('error_message', 'Could not update item');
		}

		redirect('rental-payments/'.$rental_unit_id);
	}

	public function statement_email_sent($_year,$_month,$tenant_unit_id,$statement_sent_status)
	{
		$this->db->select('*');
		$this->db->where('tenant_unit_id = '.$tenant_unit_id. ' AND statement_year = '.$_year.' AND statement_month = "'.$_month.'" AND statement_email_sent = '.$statement_sent_status);
		$statement_email_query_sent = $this->db->get('statement_email');
		if($statement_email_query_sent->num_rows() > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	public function check_statement_sms_sent($name,$_year,$_month)
	{
		$this->db->select('*');
		$this->db->where('statement_year = '.$_year.' AND statement_month = "'.$_month.'" AND messaging_tenant_name = "'.$name.'"');
		$sms_statement_query_sent = $this->db->get('messaging');
		if($sms_statement_query_sent->num_rows() > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	public function update_payment_detail($lease_id,$payment_id)
	{

		$amount_paid = $this->input->post('amount_paid_total'.$payment_id);
		$date_of_payment = $this->input->post('payment_date'.$payment_id);
		// var_dump($amount_paid); die();
		$array = array('amount_paid'=>$amount_paid,'payment_date'=>$date_of_payment);
		$this->db->where('payment_id',$payment_id);
		if($this->db->update('payments',$array))
		{
			$this->session->set_userdata('success_message', 'Successfully updated items');
		}
		else
		{
			$this->session->set_userdata('error_message', 'Please try again');
		}
		redirect('lease-payments/'.$lease_id);
	}
public function update_owner_payment_detail($rental_unit_id,$payment_id)
	{

		$amount_paid = $this->input->post('amount_paid_total'.$payment_id);
		$date_of_payment = $this->input->post('payment_date'.$payment_id);
		// var_dump($amount_paid); die();
		$array = array('amount_paid'=>$amount_paid,'payment_date'=>$date_of_payment);
		$this->db->where('payment_id',$payment_id);
		if($this->db->update('home_owners_payments',$array))
		{
			$this->session->set_userdata('success_message', 'Successfully updated items');
		}
		else
		{
			$this->session->set_userdata('error_message', 'Please try again');
		}
		redirect('rental-payments/'.$rental_unit_id);
	}

	public function cancel_payment($payment_id)
	{
		$this->form_validation->set_rules('cancel_description', 'Description', 'trim|required|xss_clean');
		$this->form_validation->set_rules('cancel_action_id', 'Action', 'trim|required|xss_clean');
		$redirect_url = $this->input->post('redirect_url');
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			// end of checker function
			if($this->accounts_model->cancel_payment($payment_id))
			{
				$this->session->set_userdata("success_message", "Payment action saved successfully");
			}
			else
			{
				$this->session->set_userdata("error_message", "Oops something went wrong. Please try again");
			}
		}
		else
		{
			$this->session->set_userdata("error_message", validation_errors());

		}
		redirect($redirect_url);
	}
	public function cancel_owner_payment($payment_id)
	{
		$this->form_validation->set_rules('cancel_description', 'Description', 'trim|required|xss_clean');
		$this->form_validation->set_rules('cancel_action_id', 'Action', 'trim|required|xss_clean');
		$redirect_url = $this->input->post('redirect_url');
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			// end of checker function
			if($this->accounts_model->cancel_owner_payment($payment_id))
			{
				$this->session->set_userdata("success_message", "Payment action saved successfully");
			}
			else
			{
				$this->session->set_userdata("error_message", "Oops something went wrong. Please try again");
			}
		}
		else
		{
			$this->session->set_userdata("error_message", validation_errors());

		}
		redirect($redirect_url);
	}

	public function update_payments_to_sept()
	{
		$this->accounts_model->update_payments_to_sept();
	}
	public function rental_payments($rental_unit_id)
	{
		$where = 'payment_status = 1 AND rental_unit_id = '.$rental_unit_id;
		$table = 'home_owners_payments';


		$segment = 3;
		//pagination

		$this->load->library('pagination');
		$config['base_url'] = base_url().'rental-payments/'.$rental_unit_id;
		$config['total_rows'] = $this->tenants_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;

		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';

		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';

		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';

		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';

		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active">';
		$config['cur_tag_close'] = '</li>';

		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);

		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->accounts_model->get_all_owner_payments($table, $where, $config["per_page"], $page, $order='home_owners_payments.month,home_owners_payments.year', $order_method='DESC');

		$properties = $this->property_model->get_active_property();

		$rs8 = $properties->result();

		$property_list = '';
		foreach ($rs8 as $property_rs) :
			$property_id = $property_rs->property_id;
			$property_name = $property_rs->property_name;
			$property_location = $property_rs->property_location;

		    $property_list .="<option value='".$property_id."'>".$property_name." Location: ".$property_location."</option>";

		endforeach;


		$v_data['property_list'] = $property_list;
		$data['title'] = 'Home Owner Payments ';
		$v_data['title'] = $data['title'];
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		$v_data['rental_unit_id'] = $rental_unit_id;
		$data['content'] = $this->load->view('edits/lease_owners_payment', $v_data, true);

		$this->load->view('admin/templates/general_page', $data);
	}

	public function cancel_pardon($pardon_id)
	{
		$this->form_validation->set_rules('cancel_description', 'Description', 'trim|required|xss_clean');
		$this->form_validation->set_rules('cancel_action_id', 'Action', 'trim|required|xss_clean');
		$redirect_url = $this->input->post('redirect_url');
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			// end of checker function
			if($this->accounts_model->cancel_pardon($pardon_id))
			{
				$this->session->set_userdata("success_message", "Pardon action saved successfully");
			}
			else
			{
				$this->session->set_userdata("error_message", "Oops something went wrong. Please try again");
			}
		}
		else
		{
			$this->session->set_userdata("error_message", validation_errors());

		}
		redirect($redirect_url);
	}
	public function cancel_owner_pardon($pardon_id)
	{
		$this->form_validation->set_rules('cancel_action_id', 'Action', 'trim|required|xss_clean');
		$redirect_url = $this->input->post('redirect_url');
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			// end of checker function
			if($this->accounts_model->cancel_owners_pardon($pardon_id))
			{
				$this->session->set_userdata("success_message", "Pardon action saved successfully");
			}
			else
			{
				$this->session->set_userdata("error_message", "Oops something went wrong. Please try again");
			}
		}
		else
		{
			$this->session->set_userdata("error_message", validation_errors());

		}
		redirect($redirect_url);
	}
	public function update_invoice($tenant_unit_id,$lease_id,$rental_unit_id)
	{
		$this->form_validation->set_rules('current_reading'.$lease_id, 'current reading', 'trim|required|xss_clean');
		$this->form_validation->set_rules('previous_reading'.$lease_id, 'previous reading', 'trim|required|xss_clean');
		$this->form_validation->set_rules('water_charges'.$lease_id, 'Water Charge', 'trim|required|xss_clean');
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			$current_reading = $this->input->post('current_reading'.$lease_id);
			$previous_reading = $this->input->post('previous_reading'.$lease_id);
			$water_charges = $this->input->post('water_charges'.$lease_id);

			if($current_reading < $previous_reading)
			{
				$this->session->set_userdata("error_message", "Sorry you the current reading is not accurate");
			}

			else
			{
				if($this->accounts_model->update_water_invoice($lease_id,$rental_unit_id))
				{

					$this->session->set_userdata("success_message", "You have successfully billed for water");
				}

				else
				{
					$this->session->set_userdata("error_message", "Oops something went wrong. Please try again");

				}
			}

		}
		else
		{
			$this->session->set_userdata("error_message", validation_errors());

		}
		redirect('accounts/tenants-accounts');
	}

	// Confirmed Payments
	public function activate_payment($payment_id, $lease_id)
	{
		if($this->accounts_model->activate_payment($payment_id))
		{
			$this->session->set_userdata('success_message', 'Payment has been Confirmed');
		}

		else
		{
			$this->session->set_userdata('error_message', 'Payment could not be Confirmed');
		}

		redirect('accounts/payments/'.$lease_id.'/'.$lease_id);
	}

	/*
	*
	*	Deactivate an existing payment page
	*	@param int $payment_id
	*
	*/
	public function deactivate_payment($payment_id, $lease_id)
	{
		if($this->accounts_model->deactivate_payment($payment_id))
		{
			$this->session->set_userdata('success_message', 'Payment has been Unconfirmed');
		}

		else
		{
			$this->session->set_userdata('error_message', 'Payment could not be Unconfirmed');
		}

		redirect('accounts/payments/'.$lease_id.'/'.$lease_id);
	}

	function display_lease_payments($lease_id=null,$tenant_id,$tenant_unit_id)
	{
		$v_data['lease_id'] = $lease_id;
		$v_data['tenant_id'] = $tenant_id;
		$v_data['tenant_unit_id'] = $tenant_unit_id;
		$v_data['lease_payments'] = $this->accounts_model->get_lease_payments($lease_id);
		$v_data['cancel_actions'] = $this->accounts_model->get_cancel_actions();

		$this->load->view('accounts/cash_office/accounts/payments_list',$v_data);
	}

	function display_lease_pardons($lease_id=null,$tenant_id,$tenant_unit_id)
	{
		$v_data['lease_id'] = $lease_id;
		$v_data['tenant_id'] = $tenant_id;
		$v_data['tenant_unit_id'] = $tenant_unit_id;
		$v_data['cancel_actions'] = $this->accounts_model->get_cancel_actions();
		$v_data['lease_pardons'] = $this->accounts_model->get_lease_pardons($lease_id);

		$this->load->view('accounts/cash_office/accounts/pardons_list',$v_data);
	}


	function display_lease_invoices($lease_id=null,$tenant_id,$tenant_unit_id)
	{
		$v_data['lease_id'] = $lease_id;
		$v_data['tenant_id'] = $tenant_id;
		$v_data['tenant_unit_id'] = $tenant_unit_id;
		$v_data = array('tenant_unit_id'=>$tenant_unit_id,'lease_id'=>$lease_id);
		$v_data['title'] = 'Tenant payments';
		$v_data['cancel_actions'] = $this->accounts_model->get_cancel_actions();

		$v_data['lease_invoices'] = $this->accounts_model->get_tenant_lease_invoices($lease_id);

		$this->load->view('accounts/cash_office/accounts/invoices_list',$v_data);
	}


	function display_tenant_summary($lease_id=null,$tenant_id,$tenant_unit_id)
	{
		$v_data['lease_id'] = $lease_id;
		$v_data['tenant_id'] = $tenant_id;
		$v_data['tenant_unit_id'] = $tenant_unit_id;
		$v_data = array('tenant_unit_id'=>$tenant_unit_id,'lease_id'=>$lease_id);
		$v_data['title'] = 'Tenant payments';


		// $v_data['tenant_response'] = $this->accounts_model->get_rent_and_service_charge($lease_id);
		// $v_data['lease_pardons'] = $this->accounts_model->get_lease_pardons($lease_id);
		$unit_billing_where = 'billing_schedule.billing_schedule_id = property_billing.billing_schedule_id AND property_billing.invoice_type_id = invoice_type.invoice_type_id AND property_billing.property_billing_deleted = 0 AND property_billing.lease_id = '.$lease_id;
		$unit_billing_table = 'billing_schedule,property_billing,invoice_type';
		$unit_billing_order = 'property_billing.property_billing_id';

		$unit_billing_query = $this->tenants_model->get_tenant_list($unit_billing_table, $unit_billing_where, $unit_billing_order);

		$v_data['query_invoice'] = $unit_billing_query;

		$this->load->view('accounts/cash_office/accounts/tenant_summary',$v_data);
	}

	function display_tenant_statement($lease_id=null,$tenant_id,$tenant_unit_id)
	{
		$v_data['lease_id'] = $lease_id;
		$v_data['tenant_id'] = $tenant_id;
		$v_data['tenant_unit_id'] = $tenant_unit_id;
		$v_data['tenant_response'] = $this->accounts_model->get_rent_and_service_charge($lease_id);
		// $v_data['lease_pardons'] = $this->accounts_model->get_lease_pardons($lease_id);

		$this->load->view('accounts/cash_office/accounts/tenant_statement',$v_data);
	}
	function display_lease_detail($lease_id=null,$tenant_id,$tenant_unit_id)
	{
		$v_data['lease_id'] = $lease_id;
		$v_data['tenant_id'] = $tenant_id;
		$v_data['tenant_unit_id'] = $tenant_unit_id;

		$this->load->view('accounts/cash_office/accounts/lease_detail',$v_data);
	}

	function print_tenant_statement($lease_number,$lease_id)
	{
		$v_data['lease_number'] = $lease_number;
		$v_data['lease_id'] = $lease_id;
		$v_data['tenant_response'] = $this->accounts_model->print_lease_statement($lease_number,$lease_id);
		// $v_data['lease_pardons'] = $this->accounts_model->get_lease_pardons($lease_id);
		$v_data['contacts'] = $this->site_model->get_contacts();

	echo  $this->load->view('leases_statement', $v_data,true);

		// $this->load->view('admin/templates/invoice_template', $data);
	}


	public function send_single_invoices($lease_id,$invoice_month,$invoice_year,$invoice_id)
	{
		$data = array('lease_id' => $lease_id,'invoice_month' => $invoice_month,'invoice_year' => $invoice_year,'invoice_id'=>$invoice_id);
		$data['contacts'] = $this->site_model->get_contacts();

		$all_leases = $this->leases_model->get_lease_detail($lease_id);
		foreach ($all_leases->result() as $leases_row)
		{
			$lease_id = $leases_row->lease_id;
			$tenant_unit_id = $leases_row->tenant_unit_id;
			$property_name = $leases_row->property_name;
			$property_id = $leases_row->property_id;
			// $units_name = $leases_row->units_name;
			$rental_unit_name = $leases_row->rental_unit_name;
			$rental_unit_id = $leases_row->rental_unit_id;
			$tenant_name = $leases_row->tenant_name;
			$tenant_email = $leases_row->tenant_email;
			$lease_start_date = $leases_row->lease_start_date;
			$lease_duration = $leases_row->lease_duration;
			$rent_amount = $leases_row->rent_amount;
			$lease_number = $leases_row->lease_number;
			$arrears_bf = $leases_row->arrears_bf;
			$rent_calculation = $leases_row->rent_calculation;
			$deposit = $leases_row->deposit;
			$deposit_ext = $leases_row->deposit_ext;
			$tenant_phone_number = $leases_row->tenant_phone_number;
			$tenant_national_id = $leases_row->tenant_national_id;
			$lease_status = $leases_row->lease_status;
			$tenant_status = $leases_row->tenant_status;
			$created = $leases_row->created;
			$message_prefix = $leases_row->message_prefix;
			$lease_start_date = date('jS M Y',strtotime($lease_start_date));

			$expiry_date  = date('jS M Y', strtotime(''.$lease_start_date.'+1 years'));

		}

		$contacts = $data['contacts'];

		$data['rental_unit_id'] = $lease_id;


		$v_data['content'] = $this->load->view('cash_office/invoice_template', $data, true);
		$html = $this->load->view('admin/templates/invoice_template', $v_data,true);


		// $document_number = 'L'.$lease_id.'-'.$invoice_month.'-'.$invoice_year;




		$todays_date = date('Y-m-d');
		$todays_month = date('m');
		$todays_year = date('Y');

		$tenants_response = $this->accounts_model->get_lease_balance($lease_id);
		$total_arrears = $tenants_response['balance'];


		$date_today = date('Y-m-d');
		$date_exploded = explode('-', $date_today);
		$_year = $date_exploded[0];
		$_month = $date_exploded[1];

		// $tenant_email = 'marttkip@gmail.com';

		$tenant_name = ucfirst($tenant_name);

		$invoice_details = $this->accounts_model->get_invoice_items($invoice_id);
		if($invoice_details->num_rows() > 0)
		{
			foreach ($invoice_details->result() as $key => $value) {
				// code...
				$total_amount = $value->total_amount;
				$invoice_date = $value->invoice_date;
				$document_number = $value->document_number;

			}
		}



		if(!empty($tenant_email))
		{

			$this->load->library('mpdf');
			$invoice_month_date = date('Y-m-d');
			$invoice_month = date('M',strtotime($invoice_month_date));
			$invoice_year = date('Y',strtotime($invoice_month_date));
			$title = date('ymdhis').$document_number.'.pdf';
			$invoice_path = $this->invoice_path;
			$invoice = $invoice_path.'/'.$title;
			// echo $invoice;die();

			$mpdf=new mPDF();
			$mpdf->WriteHTML($html);
			$mpdf->Output($title, 'F');

			while(!file_exists($title))
			{

			}

			// var_dump($html); die();
			$message['subject'] =  $contacts['company_name'].' '.$document_number.' Invoice';

			$message['text'] = ' <p>Dear '.$tenant_name.',<br>
														<p>Invoice '.$document_number.' of KES '.$total_amount.' has been raised on your account '.$lease_number.' dated '.$invoice_date.' .</p>
														<p>Kindly find attached document. </p>
														<p>Please do not hesitate to contact us for any clarification you may need.</p>
														<p>'.$contacts['company_name'].'</p>
														<p>P O Box 8936 - 30100 Eldoret, Kenya |   0700-633199, 0702 777717, 0738 777718 | '.$contacts['email'].'</p>
														';
			$sender_email = $contacts['email'];
			$shopping = "";
			$from = $contacts['email'];

			$button = '';
			$sender['email']= $contacts['email'];
			$sender['name'] = $contacts['company_name'];
			$receiver['name'] = $tenant_name;
			$payslip = $title;

			$sender_email = $contacts['email'];
			// $tenant_email .= '/'.$sender_email.'';
			// var_dump($tenant_email); die();
			$email_array = explode('/', $tenant_email);
			$total_rows_email = count($email_array);

			for($x = 0; $x < $total_rows_email; $x++)
			{
				$receiver['email'] = $email_tenant = $email_array[$x];
				// $this->email_model->send_sendgrid_mail($receiver, $sender, $message, $payslip);
			}

		}

		// $tenant_phone_number = 704808007;
		if(!empty($tenant_phone_number))
		{
			$date = date('jS M Y',strtotime(date('Y-m-d')));
			$phone_array = explode('/', $tenant_phone_number);
			$total_rows = count($phone_array);

			for($r = 0; $r < $total_rows; $r++)
			{
				$tenant_phone_number = $phone_array[$r];
				$explode = explode(' ',$tenant_name);
				$first_name = $explode[0];
				$message = 'Dear '.$first_name.'. This is to let you know that invoice '.$document_number.' of KES '.$total_amount.' has been raised on your account '.$lease_number.' dated '.$invoice_date.'. For help please call 0702777717. Allan & Bradley Co. LTD';

				$this->accounts_model->sms($tenant_phone_number,$message,$tenant_name);

			}

		}

		$update_array['sent_status'] = 1;
		$this->db->where('lease_invoice_id =  '.$invoice_id.' AND lease_id = '.$lease_id);
		$this->db->update('lease_invoice',$update_array);

		redirect('accounts/payments/'.$lease_id.'/'.$lease_id);
	}

	public function delete_invoice($lease_id,$invoice_id)
	{

		$update_array['invoice_deleted'] = 1;
		$this->db->where('lease_invoice_id =  '.$invoice_id.' AND lease_id = '.$lease_id);
		$this->db->update('lease_invoice',$update_array);



		$response['message'] = 'success';

		echo json_encode($response);

	}

	function display_chats($lease_id=null,$tenant_id,$tenant_unit_id)
	{
		$v_data['lease_id'] = $lease_id;
		$v_data['tenant_id'] = $tenant_id;
		$v_data['tenant_unit_id'] = $tenant_unit_id;

		$this->load->view('accounts/cash_office/accounts/chat',$v_data);
	}

	public function search_property_report()
	{
		$properties = $this->property_model->get_active_property();
		$months = $this->property_model->get_months();
		$rs8 = $properties->result();

		$property_list = '';
		foreach ($rs8 as $property_rs) :
			$property_id = $property_rs->property_id;
			$property_name = $property_rs->property_name;
			$property_location = $property_rs->property_location;

		    $property_list .="<option value='".$property_id."'>".$property_name." Location: ".$property_location."</option>";

		endforeach;

		$month_list = '';
		if($months->num_rows() > 0)
		{
			foreach ($months->result() as $month_rs) :
				$month_id = $month_rs->month_id;
				$month_name = $month_rs->month_name;
				$month_list .="<option value='".$month_id."_".$month_name."'>".$month_name."</option>";
			endforeach;
		}
		$v_data['months'] = $month_list;
		$v_data['property_list'] = $property_list;
		$v_data['title'] = $data['title'] = 'Search Property';
		$data['content'] = $this->load->view('reports/search_property', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}

	public function property_statement($property_id=NULL,$month_id=NULL,$year=NULL)
	{
		$property_id = $this->input->post('property_id');
		$month = $this->input->post('month_id');
		$year = $this->input->post('year');

		$table = 'rental_unit,property';

		// var_dump($property_id);die();
		$this->db->where('property_id',$property_id);
		$query_result = $this->db->get('property');
		$return_date = '01';
		if($query_result->num_rows() > 0)
		{
			foreach ($query_result->result() as $key => $value) {
				# code...
				$return_date = $value->return_date;
				$property_name = $value->property_name;
			}
		}
		if(empty($return_date))
		{
			$return_date = '01';
		}

		$this->session->set_userdata('month',$month);
		$this->session->set_userdata('year',$year);
		$this->session->set_userdata('return_date',$return_date);
		$this->session->set_userdata('property_id',$property_id);

		// $where = 'tenants.tenant_id > 0 AND tenants.tenant_id = tenant_unit.tenant_id AND tenant_unit.rental_unit_id = rental_unit.rental_unit_id  AND tenant_unit.tenant_unit_id = leases.tenant_unit_id  AND property.property_id = rental_unit.property_id AND property.property_id = '.$property_id;
		$where = 'property.property_id = rental_unit.property_id AND rental_unit.property_id = '.$property_id;

		$title = $property_name.' Report for '.$month.' '.$year;
		$segment = 3;

		$query = $this->accounts_model->get_all_leases_rental_units($table, $where, null,null, $order='rental_unit.rental_unit_number,rental_unit.rental_unit_name,rental_unit.rental_unit_id', $order_method='ASC',$month,$year);

		$data['contacts'] = $this->site_model->get_contacts();
		$data['title'] = $v_data['title'] = $title;
		$v_data['query'] = $data['query'] =  $query;
		$v_data['month'] = $data['month'] = $month;
		$v_data['year'] = $data['year'] = $year;
			echo $this->load->view('accounts/reports/property_statement', $data, true);

	}


	public function create_lease_invoice($lease_id)
	{
		$this->form_validation->set_rules('invoice_amount', 'Amount', 'trim|required|xss_clean');
		$this->form_validation->set_rules('invoice_type_id', 'Invoice type', 'trim|required|xss_clean');
		$this->form_validation->set_rules('invoice_date', 'Invoice Date', 'trim|required|xss_clean');
		$this->form_validation->set_rules('remarks', 'Remarks', 'trim|required|xss_clean');
		//if form conatins invalid data
		// var_dump($_POST); die();
		if ($this->form_validation->run())
		{
				if($this->accounts_model->create_lease_bill($lease_id))
				{

					$this->session->set_userdata("success_message", "You have successfully charged");
				}

				else
				{
					$this->session->set_userdata("error_message", "Oops something went wrong. Please try again");

				}
		}
		else
		{
			$this->session->set_userdata("error_message", validation_errors());

		}
		$redirect_url = $this->input->post('redirect_url');
		redirect($redirect_url);
	}

	public function batch_lease_invoice($lease_id)
	{
		$this->db->where('lease_invoice_id = 0 and lease_id = '.$lease_id);
		$this->db->select('SUM(invoice_amount) AS total_amount,created');
		$query = $this->db->get('invoice');

		$value = $query->row();

		$amount = $value->total_amount;
		$created = $value->created;
		$remarks= $value->remarks;

		if($amount > 0)
		{

			// create a lease invoice
			$date = explode('-', $created);
			$month = $date[1];
			$year = $date[0];
			$document_number = $this->accounts_model->create_invoice_number();
			// create the invoice
			$insertarray['invoice_date'] = $invoice_date;
			$insertarray['invoice_year'] = $year;
			$insertarray['invoice_month'] = $month;
			$insertarray['lease_id'] = $lease_id;
			$insertarray['document_number'] = $document_number;
			$insertarray['total_amount'] = $amount;
			$insertarray['rental_unit_id'] = $rental_unit_id;
			$insertarray['created_by'] = $this->session->userdata('personnel_id');
			$insertarray['created'] = date('Y-m-d');
			$insertarray['prefix'] = '';
			$insertarray['suffix'] = '';
			$insertarray['invoice_cat'] = 1;
			$insertarray['lease_id'] = $lease_id;

			$this->db->insert('lease_invoice',$insertarray);
			$lease_invoice_id = $this->db->insert_id();

			$service = array(
								'lease_invoice_id'=>$lease_invoice_id,
								'created' =>$invoice_date,
								'invoice_item_status'=>1,
								'year'=>$year,
								'month'=>$month,
							);
			$this->db->where('invoice_item_status = 0 AND lease_id = '.$lease_id.' AND lease_invoice_id = 0');
			if($this->db->update('invoice',$array_update))
			{
				$this->session->set_userdata("success_message", "You have successfully charged");
			}
			else {
				$this->session->set_userdata("error_message", "You have billed tenants");
			}

		}
		redirect('lease-detail/'.$lease_id);
	}

	public function generate_token()
	{
		$url = 'https://api.safaricom.co.ke/oauth/v1/generate?grant_type=client_credentials';

		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		$credentials = base64_encode('QGw5xR2twwA8apaEvjBxGBWeMAZpEsQB:zG7CqzfYaWLfiqcC');
		curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Basic '.$credentials)); //setting a custom header
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_HEADER, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

		// echo "<br>Here";
		$curl_response = curl_exec($curl);

		// echo "<br>Here 2: $curl_response";
		//echo "<br>As json:". json_decode($curl_response);

		$token_pos = strpos($curl_response, "access_token");
        if ($token_pos === FALSE) {
            // $this->addPublicError("Token not found");
            // echo "<br>Token position not found.";
            $access_token = "";
        } else {
        	// echo "<br>Position: $token_pos";
            $curl_response = str_replace('"', '', $curl_response);
            $curl_response = str_replace(" ", "", $curl_response);
            $token_pos = strpos($curl_response, "access_token");
            $start = strpos($curl_response, ':', $token_pos) + 1;
            $end = strpos($curl_response, ',', $start);
            $length = $end - $start;
            $access_token = substr($curl_response, $start, $length);

        }
        echo $access_token;
       // return $access_token;
	}

	public function register_url()
	{
		$url = 'https://api.safaricom.co.ke/mpesa/c2b/v1/registerurl';

		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json','Authorization:Bearer '.$this->generate_token())); //setting custom header

		// echo site_url().'mpesa-receive/0';

		$curl_post_data = array(
		  //Fill in the request parameters with valid values
		  'ShortCode' => '601364',
		  'ResponseType' => 'Completed',
		  'ConfirmationURL' => site_url().'mpesa-receive/0',
		  'ValidationURL' => site_url().'mpesa-receive/1'
		);

		$data_string = json_encode($curl_post_data);

		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);

		$curl_response = curl_exec($curl);
		// print_r($curl_response);

		// echo $curl_response;
	}
	public function mpesa_receive_transaction($type)
	{
		$json = file_get_contents('php://input');

		$insert_array['test'] = $json;
		$this->db->insert('mpesa_test',$insert_array);
		$decoded = json_decode($json);
		$serial_number = $decoded->TransID;
		if(!empty($serial_number))
		{
			$TransAmount = $decoded->TransAmount;
			$TransTime = $decoded->TransTime;
			$TransID = $decoded->TransID;
			$BusinessShortCode = $decoded->BusinessShortCode;
			$BillRefNumber = $decoded->BillRefNumber;
			$MSISDN = $decoded->MSISDN;
			$FirstName = $decoded->FirstName;
			$MiddleName = $decoded->MiddleName;
			$LastName = $decoded->LastName;
			$OrgAccountBalance = $decoded->OrgAccountBalance;
			$sender_name = $FirstName.' '.$MiddleName.' '.$LastName;
	    $sender_name = str_replace('%20', ' ', $sender_name);
			$insert_array2['serial_number'] = $TransID;
			$insert_array2['account_number'] = $BillRefNumber;
			$insert_array2['mpesa_status'] = 0;
			$insert_array2['sender_name'] = $sender_name;
			$insert_array2['sender_phone'] = $MSISDN;
			$insert_array2['amount'] = $TransAmount;
			$insert_array2['created'] = date('Y-m-d H:i:s');
			if($this->db->insert('mpesa_transactions',$insert_array2))
			{

				$response['ResultCode'] = 0;
				$response['ResultDesc'] = 'The service was accepted successfully';
				$response['ThirdPartyTransID'] = date('YmdHis');

				$tenant_phone_number = '+'.$MSISDN;

			}
			else
			{
				$response['ResultCode'] = 0;
				$response['ResultDesc'] = 'Could not process payment';
				$response['ThirdPartyTransID'] = date('YmdHis');
			}
		}
		else
		{
			$response['ResultCode'] = 1;
			$response['ResultDesc'] = 'Could not process payment';
			$response['ThirdPartyTransID'] = date('YmdHis');
		}
		echo json_encode($response);
	}
	public function mpesa_transaction($serial_number,$account_number,$sender_name,$sender_phone,$amount,$time_stamp,$mpesa_status=null)
	{

		// check if this transaction exisits

		$sender_name = str_replace('%20', ' ', $sender_name);
		$insert_array['serial_number'] = $serial_number;
		$insert_array['account_number'] = $account_number;
		$insert_array['mpesa_status'] = 0;
		$insert_array['sender_name'] = $sender_name;
		$insert_array['sender_phone'] = $sender_phone;
		$insert_array['amount'] = $amount;
		// $insert_array['lease_id'] = $lease_id;
		$insert_array['created'] = date('Y-m-d H:i:s');
		$this->db->insert('mpesa_transactions',$insert_array);
		{
			$date = date('jS M Y',strtotime(date('Y-m-d')));
			// $tenant_array = explode(' ', $tenant_name);

		$tenant_phone_number = '+'.$sender_phone;
		if(!empty($tenant_phone_number))
			// $tenant_name = $tenant_array[0];

			$message = 'We have received Ksh. '.$amount.' for account '.$account_number.' has been successfully received. A receipt will be send shortly. Thank you';
			// $this->accounts_model->sms($tenant_phone_number,$message,$tenant_name);

			// save the message sent out
			$insert_array = array('phone_number'=>$tenant_phone_number,'client_name'=>$tenant_name,'type_of_account'=>1,'message'=>$message,'date_created'=>date('Y-m-d'),'sms_type'=>2);
			$this->db->insert('sms',$insert_array);
			// save the message sent out

		}

		$response['status'] = 1;


		echo json_encode($response);
	}

	public function unreconcilled_payments()
	{
		$where = 'mpesa_status = 0 AND lease_id is null';
		$table = 'mpesa_transactions';

		$search = $this->session->userdata('search_mpesa_received');
		if(!empty($search))
		{
			$where .= $search;
		}
				$this->session->unset_userdata('lease_number_searched');
		$segment = 3;
		//pagination

		$this->load->library('pagination');
		$config['base_url'] = base_url().'cash-office/mpesa-received';
		$config['total_rows'] = $this->tenants_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;

		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';

		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';

		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';

		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';

		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active">';
		$config['cur_tag_close'] = '</li>';

		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);

		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->accounts_model->get_unreconcilled_payments($table, $where, $config["per_page"], $page, $order='created', $order_method='DESC');

		$v_data['cancel_actions'] = $this->accounts_model->get_cancel_actions();

		$data['title'] = 'All Unreconcilled Payments';
		$v_data['title'] = $data['title'];
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		$data['content'] = $this->load->view('unreconcilled_payments', $v_data, true);

		$this->load->view('admin/templates/general_page', $data);
	}
	public function reconcile_payment($mpesa_id)
	{

		$where = 'mpesa_status = 0 AND mpesa_id = '.$mpesa_id;
		$table = 'mpesa_transactions';

		$this->db->where($where);
		$query = $this->db->get('mpesa_transactions');
		$data['title'] = 'All Unreconcilled Payments';
		$v_data['title'] = $data['title'];
		$v_data['query'] = $query;
		$data['content'] = $this->load->view('reconcile_payment', $v_data, true);

		$this->load->view('admin/templates/general_page', $data);
	}

	public function reconcile_payment_item($tenant_unit_id , $lease_id, $mpesa_id)
	{
		$this->form_validation->set_rules('payment_method', 'Payment Method', 'trim|required|xss_clean');
		$this->form_validation->set_rules('amount_paid', 'Amount', 'trim|required|xss_clean');
		$this->form_validation->set_rules('payment_date', 'Date Receipted', 'trim|required|xss_clean');
		$this->form_validation->set_rules('water_amount', 'Water amount', 'trim|xss_clean');
		$this->form_validation->set_rules('service_charge_amount', 'Service charge amount', 'trim|xss_clean');
		$this->form_validation->set_rules('penalty_fee', 'Service charge amount', 'trim|xss_clean');
		$this->form_validation->set_rules('rent_amount', 'Rent amount', 'trim|xss_clean');



		$payment_method = $this->input->post('payment_method');
		$water_amount = $this->input->post('water_amount');
		$rent_amount = $this->input->post('rent_amount');
		$service_charge_amount = $this->input->post('service_charge_amount');
		$penalty_fee = $this->input->post('penalty_fee');
		$amount_paid = $this->input->post('amount_paid');
		$fixed_charge = $this->input->post('fixed_charge');
		$deposit_charge = $this->input->post('deposit_charge');

		$insurance = $this->input->post('insurance');
		$sinking_funds = $this->input->post('sinking_funds');
		$bought_water = $this->input->post('bought_water');
		$painting_charge = $this->input->post('painting_charge');
		$legal_fees = $this->input->post('legal_fees');


		if(empty($water_amount))
		{
			$water_amount = 0;
		}
		if(empty($service_charge_amount))
		{
			$service_charge_amount = 0;
		}
		if(empty($rent_amount))
		{
			$rent_amount = 0;
		}
		if(empty($penalty_fee))
		{
			$penalty_fee = 0;
		}
		if(empty($fixed_charge))
		{
			$fixed_charge = 0;
		}

		if(empty($deposit_charge))
		{
			$deposit_charge = 0;
		}
		if(empty($insurance))
		{
			$insurance = 0;
		}
		if(empty($sinking_funds))
		{
			$sinking_funds = 0;
		}
		if(empty($bought_water))
		{
			$bought_water = 0;
		}
		if(empty($painting_charge))
		{
			$painting_charge = 0;
		}
		if(empty($legal_fees))
		{
			$legal_fees = 0;
		}
		//  add all this items
		$total = $service_charge_amount + $rent_amount + $water_amount + $penalty_fee + $fixed_charge + $deposit_charge + $insurance + $sinking_funds + $bought_water + $painting_charge+ $legal_fees;

		// var_dump($total); die();
		// Normal


		if($payment_method == 1)
		{
			// check for cheque number if inserted
			$this->form_validation->set_rules('bank_name', 'Bank Name', 'trim|required|xss_clean');

		}
		else if($payment_method == 5)
		{
			//  check for mpesa code if inserted
			$this->form_validation->set_rules('mpesa_code', 'Amount', 'is_unique[payments.transaction_code]|trim|required|xss_clean');
		}

		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			if($amount_paid == $total)
			{
				$this->accounts_model->receipt_payment($lease_id);

				$this->session->set_userdata("success_message", 'Payment successfully added');
				$response['status'] = 'success';
				$response['message'] = 'Payment successfully added';

				// update payments
				$update_array['lease_id'] = $lease_id;
				$update_array['mpesa_status'] = 1;
				$this->db->where('mpesa_id',$mpesa_id);
				$this->db->update('mpesa_transactions',$update_array);

				$sender_phone = $this->input->post('sender_phone');
				$account_number = $this->input->post('account_number');
				// $tenant_phone_number = '+'.$sender_phone;
				$tenant_phone_number = '+254734808007';
				if(!empty($tenant_phone_number))
				{
					$message = 'We have received Ksh. '.$amount_paid.' for account '.$account_number.'. Thank you';
					// $this->accounts_model->sms($tenant_phone_number,$message,$tenant_name);

					// save the message sent out
					// $insert_array = array('phone_number'=>$tenant_phone_number,'client_name'=>$tenant_name,'type_of_account'=>1,'message'=>$message,'date_created'=>date('Y-m-d'),'sms_type'=>2);
					// $this->db->insert('sms',$insert_array);
					// save the message sent out

				}

				redirect('accounts/payments/'.$tenant_unit_id.'/'.$lease_id);
			}
			else
			{
				$this->session->set_userdata("error_message", 'The amounts of payment do not add up to the total amount paid');
				$response['status'] = 'fail';
				$response['message'] = 'The amounts of payment do not add up to the total amount paid';
				$redirect_url = $this->input->post('redirect_url');
				redirect($redirect_url);
			}
		}
		else
		{
			$this->session->set_userdata("error_message", validation_errors());
			$response['status'] = 'fail';
			$response['message'] = validation_errors();
			$redirect_url = $this->input->post('redirect_url');
			redirect($redirect_url);

		}

		// echo json_encode($response);


	}

	public function add_payment_item($tenant_unit_id , $lease_id, $module = NULL)
	{
		// var_dump($lease_id); die();
		$this->form_validation->set_rules('invoice_type_id', 'Invoice Item', 'trim|required|xss_clean');
		$this->form_validation->set_rules('amount', 'Amount', 'trim|required|xss_clean');
		$this->form_validation->set_rules('payment_month', 'Month', 'trim|required|xss_clean');
		$this->form_validation->set_rules('payment_year', 'Year', 'trim|required|xss_clean');

		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			$mpesa_id = $this->input->post('mpesa_id');

			if($mpesa_id > 0)
			{
				// check if the value is  greater that the one placed
					$actual_balance = $this->input->post('actual_balance');
					$amount = $this->input->post('amount');

					if($amount > $actual_balance)
					{
						$response['status'] = 'error';
						$response['message'] = 'Payment successfully added';
						$this->session->set_userdata('error_message', 'Sorry the amount placed cannot be more that the amount of money paid by the customer');
					}
					else {
						$this->accounts_model->payment_item_add($lease_id);
						$this->session->set_userdata("success_message", 'Payment successfully added');
						$response['status'] = 'success';
						$response['message'] = 'Payment successfully added';
						$this->session->set_userdata('success_message', 'You have added the payment item successfully');
					}

			}
			else {

				// var_dump($_POST);die();
				$this->accounts_model->payment_item_add($lease_id);
				$this->session->set_userdata("success_message", 'Payment successfully added');
				$response['status'] = 'success';
				$response['message'] = 'Payment successfully added';
				$this->session->set_userdata('success_message', 'You have added the payment item successfully');
			}

		}
		else
		{
			$this->session->set_userdata("error_message", validation_errors());
			$response['status'] = 'fail';
			$response['message'] = validation_errors();
			$this->session->set_userdata('error_message', validation_errors());
		}
		$redirect_url = $this->input->post('redirect_url');
		redirect($redirect_url);
	}

	public function delete_payment_items_id($payment_item_id,$lease_id = null,$tenant_id = null,$payment_id=NULL)
	{

		$this->db->where('payment_item_id',$payment_item_id);
		if($this->db->delete('payment_item'))
		{
			$this->session->set_userdata('success_message', 'you have successfully removed the payment item');
		}
		else
		{
			$this->session->set_userdata('error_message', 'Please check the details and try again ');
		}
		if(!empty($lease_id) OR !empty($tenant_id))
		{
			if(!empty($payment_id))
			{
				redirect('edit-payment/'.$lease_id.'/'.$payment_id);
			}
			else
			{
				redirect('view-tenant-payments/'.$lease_id.'/'.$tenant_id);
			}
			
		}
		else
		{
			redirect('cash-office/tenants-payments');
		}
	}


	public function confirm_payment($tenant_unit_id , $lease_id, $close_page = NULL)
	{
		$this->form_validation->set_rules('payment_method', 'Payment Method', 'trim|required|xss_clean');
		$this->form_validation->set_rules('total_amount', 'Amount', 'trim|required|xss_clean');
		$this->form_validation->set_rules('payment_date', 'Date Receipted', 'trim|required|xss_clean');
		$payment_method = $this->input->post('payment_method');

		// var_dump($_POST);die();
		if(!empty($payment_method))
		{
			if($payment_method == 2 || $payment_method == 3)
			{
				// check for cheque number if inserted
				$this->form_validation->set_rules('bank_id', 'Bank Id', 'trim|required|xss_clean');
				$this->form_validation->set_rules('reference_number', 'Reference Number', 'trim|required|xss_clean');
			}

			$response['status'] = 'fail';
			$response['message'] = 'Please select a payment method';
		}

		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			$mpesa_id = $this->input->post('mpesa_id');
			$this->accounts_model->confirm_payment($lease_id);

			$this->session->set_userdata("success_message", 'Payment successfully added');
			$response['status'] = 'success';
			$response['message'] = 'Payment successfully added';

			$redirect_url = $this->input->post('redirect_url');
			$payment_id = $this->input->post('payment_id');
			if(!empty($payment_id))
			{
				$lease_number = $this->input->post('lease_number');
				redirect('view-tenant-payments/'.$lease_number.'/'.$lease_id);
			}
			else
			{
				redirect($redirect_url);
			}
		}
		else
		{
			$this->session->set_userdata("error_message", validation_errors());
			$response['status'] = 'fail';
			$response['message'] = validation_errors();

			$redirect_url = $this->input->post('redirect_url');
			redirect($redirect_url);

		}

		// echo json_encode($response);


		
	}

	public function update_payments_item($payment_item_id,$lease_id,$tenant_id = null,$module=null)
	{
		$this->form_validation->set_rules('invoice_item_amount'.$payment_item_id, 'Payment Method', 'trim|required|xss_clean');

		//if form conatins invalid data
		if ($this->form_validation->run())
		{
				$array['amount_paid'] = $this->input->post('invoice_item_amount'.$payment_item_id);
				$this->db->where('payment_item_id',$payment_item_id);
				$this->db->update('payment_item',$array);

				$this->session->set_userdata("success_message", 'Successfully removed the payment item');
				$response['status'] = 'success';
				$response['message'] = 'Payment successfully added';
		}
		else
		{
			$this->session->set_userdata("error_message", validation_errors());
		}
		if($module == 1)
		{
			redirect('view-tenant-payments/'.$lease_id.'/'.$tenant_id);
		}
		else if($module == 3)
		{
			$payment_id = $this->input->post('payment_id');
			redirect('edit-payment/'.$lease_id.'/'.$payment_id);
		}
		else
		{
			redirect('cash-office/tenants-payments');
		}
	}



	// credit notes


	public function tenants_credit_notes()
	{
			// $v_data['property_list'] = $property_list;
			$data['title'] = 'Tenants Credit Notes ';
			$v_data['title'] = $data['title'];
			$v_data['query'] = $query;
			$v_data['page'] = $page;
			$data['content'] = $this->load->view('receipts/tenants_credit_notes', $v_data, true);

			$this->load->view('admin/templates/general_page', $data);
	}
	public function search_tenants_credit_notes()
	{
		// var_dump($_POST);die();
		$lease_id = $lease_id_searched = $this->input->post('lease_id');

		$search_title = '';

		if(!empty($lease_id))
		{
			$this->db->where('lease_id', $lease_id);


			$lease_id = ' AND leases.lease_id = '.$lease_id.' ';


		}

		$search = $lease_id;

		// var_dump($search);die();

		$this->session->set_userdata('credit_lease_id_searched', $lease_id_searched);
		$this->session->set_userdata('search_tenants_credit_notes', $search);
		redirect('cash-office/tenants-credit-notes');
	}

	public function close_searched_credit_lease()
	{
		$this->session->unset_userdata('credit_lease_id_searched');
		$this->session->unset_userdata('search_tenants_credit_notes');
		redirect('cash-office/tenants-credit-notes');
	}


	public function add_credit_note_item($tenant_unit_id , $lease_id, $close_page = NULL)
	{
		$this->form_validation->set_rules('invoice_type_id', 'Invoice Item', 'trim|required|xss_clean');
		$this->form_validation->set_rules('amount', 'Amount', 'trim|required|xss_clean');
		$this->form_validation->set_rules('tenant_id', 'Tenant_id', 'trim|required|xss_clean');

		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			$this->accounts_model->add_credit_note_item($lease_id);
			$this->session->set_userdata("success_message", 'Credit Note successfully added');
			$response['status'] = 'success';
			$response['message'] = 'Payment successfully added';
		}
		else
		{
			$this->session->set_userdata("error_message", validation_errors());
			$response['status'] = 'fail';
			$response['message'] = validation_errors();

		}
		$redirect_url = $this->input->post('redirect_url');
		redirect($redirect_url);
	}

	public function delete_credit_note_items_id($credit_note_item_id)
	{

		$this->db->where('credit_note_item_id',$credit_note_item_id);
		if($this->db->delete('credit_note_item'))
		{
			$this->session->set_userdata('success_message', 'you have successfully removed the credit note item');
		}
		else
		{
			$this->session->set_userdata('error_message', 'Please check the details and try again ');
		}

		redirect('cash-office/tenants-credit-notes');
	}


	public function update_credit_notes_item($credit_note_item_id,$lease_id)
	{
		$this->form_validation->set_rules('invoice_item_amount'.$credit_note_item_id, 'Payment Method', 'trim|required|xss_clean');

		//if form conatins invalid data
		if ($this->form_validation->run())
		{
				$array['credit_note_amount'] = $this->input->post('invoice_item_amount'.$credit_note_item_id);
				$this->db->where('credit_note_item_id',$credit_note_item_id);
				$this->db->update('credit_note_item',$array);

				$this->session->set_userdata("success_message", 'Successfully updated the payment item');
				$response['status'] = 'success';
				$response['message'] = 'Payment successfully added';
		}
		else
		{
			$this->session->set_userdata("error_message", validation_errors());
		}

		redirect('cash-office/tenants-credit-notes');
	}

	public function delete_credit_note_id($credit_note_item_id)
	{

		$this->db->where('credit_note_item_id',$credit_note_item_id);
		if($this->db->delete('credit_note_item'))
		{
			$this->session->set_userdata('success_message', 'you have successfully removed the  item');
		}
		else
		{
			$this->session->set_userdata('error_message', 'Please check the details and try again ');
		}
		redirect('cash-office/tenants-credit-notes');
	}

	public function confirm_credit_note($tenant_unit_id , $lease_id, $close_page = NULL)
	{

		// var_dump($_POST);die();
		$this->form_validation->set_rules('reason', 'Reason', 'trim|required|xss_clean');
		$this->form_validation->set_rules('total_amount', 'Amount', 'trim|required|xss_clean');
		$this->form_validation->set_rules('credit_note_date', 'Date Receipted', 'trim|required|xss_clean');
		if ($this->form_validation->run())
		{
				$this->accounts_model->confirm_credit_note($lease_id);

				$this->session->set_userdata("success_message", 'Credit Note successfully added');
				$response['status'] = 'success';
				$response['message'] = 'Payment successfully added';
		}
		else
		{
			$this->session->set_userdata("error_message", validation_errors());
			$response['status'] = 'fail';
			$response['message'] = validation_errors();

		}

		$redirect_url = $this->input->post('redirect_url');
		redirect($redirect_url);
	}




	// invoices notes




	public function tenants_debit_notes()
	{
			// $v_data['property_list'] = $property_list;
			$data['title'] = 'Tenants Debit Notes ';
			$v_data['title'] = $data['title'];
			$v_data['query'] = $query;
			$v_data['page'] = $page;
			$data['content'] = $this->load->view('receipts/tenants_debit_notes', $v_data, true);

			$this->load->view('admin/templates/general_page', $data);
	}
	public function search_tenants_debit_notes()
	{
		// var_dump($_POST);die();
		$lease_id = $lease_id_searched = $this->input->post('lease_id');

		$search_title = '';

		if(!empty($lease_id))
		{
			$this->db->where('lease_id', $lease_id);


			$lease_id = ' AND leases.lease_id = '.$lease_id.' ';


		}

		$search = $lease_id;

		// var_dump($search);die();

		$this->session->set_userdata('debit_lease_id_searched', $lease_id_searched);
		$this->session->set_userdata('search_tenants_debit_notes', $search);
		redirect('cash-office/tenants-debit-notes');
	}

	public function close_searched_debit_lease()
	{
		$this->session->unset_userdata('debit_lease_id_searched');
		$this->session->unset_userdata('search_tenants_debit_notes');
		redirect('cash-office/tenants-debit-notes');
	}


	public function add_debit_note_item($tenant_unit_id , $lease_id, $close_page = NULL)
	{
		$this->form_validation->set_rules('invoice_type_id', 'Invoice Item', 'trim|required|xss_clean');
		$this->form_validation->set_rules('amount', 'Amount', 'trim|required|xss_clean');

		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			$this->accounts_model->add_debit_note_item($lease_id);
			$this->session->set_userdata("success_message", 'Credit Note successfully added');
			$response['status'] = 'success';
			$response['message'] = 'Payment successfully added';
		}
		else
		{
			$this->session->set_userdata("error_message", validation_errors());
			$response['status'] = 'fail';
			$response['message'] = validation_errors();

		}
		$redirect_url = $this->input->post('redirect_url');
		redirect($redirect_url);
	}

	public function delete_debit_note_items_id($debit_note_item_id)
	{

		$this->db->where('debit_note_item_id',$debit_note_item_id);
		if($this->db->delete('debit_note_item'))
		{
			$this->session->set_userdata('success_message', 'you have successfully removed the debit note item');
		}
		else
		{
			$this->session->set_userdata('error_message', 'Please check the details and try again ');
		}

		redirect('cash-office/tenants-debit-notes');
	}


	public function update_debit_notes_item($debit_note_item_id,$lease_id)
	{
		$this->form_validation->set_rules('invoice_item_amount'.$debit_note_item_id, 'Payment Method', 'trim|required|xss_clean');

		//if form conatins invalid data
		if ($this->form_validation->run())
		{
				$array['amount_paid'] = $this->input->post('invoice_item_amount'.$debit_note_item_id);
				$this->db->where('debit_note_item_id',$debit_note_item_id);
				$this->db->update('debit_note_item',$array);

				$this->session->set_userdata("success_message", 'Successfully updated the payment item');
				$response['status'] = 'success';
				$response['message'] = 'Payment successfully added';
		}
		else
		{
			$this->session->set_userdata("error_message", validation_errors());
		}

		redirect('cash-office/tenants-debit-notes');
	}

	public function delete_debit_note_id($debit_note_item_id)
	{

		$this->db->where('debit_note_item_id',$debit_note_item_id);
		if($this->db->delete('debit_note_item'))
		{
			$this->session->set_userdata('success_message', 'you have successfully removed the  item');
		}
		else
		{
			$this->session->set_userdata('error_message', 'Please check the details and try again ');
		}
		redirect('cash-office/tenants-debit-notes');
	}

	public function confirm_debit_note($tenant_unit_id , $lease_id, $close_page = NULL)
	{
		$this->form_validation->set_rules('total_amount', 'Amount', 'trim|required|xss_clean');
		$this->form_validation->set_rules('debit_note_date', 'Date Receipted', 'trim|required|xss_clean');
		if ($this->form_validation->run())
		{
				$this->accounts_model->confirm_debit_note($lease_id);

				$this->session->set_userdata("success_message", 'Credit Note successfully added');
				$response['status'] = 'success';
				$response['message'] = 'Payment successfully added';
		}
		else
		{
			$this->session->set_userdata("error_message", validation_errors());
			$response['status'] = 'fail';
			$response['message'] = validation_errors();

		}

		$redirect_url = $this->input->post('redirect_url');
		redirect($redirect_url);
	}







	// invoice items



		public function tenants_invoices()
		{
				// $v_data['property_list'] = $property_list;
				$data['title'] = 'Tenants Invoice ';
				$v_data['title'] = $data['title'];
				$data['content'] = $this->load->view('receipts/tenants_invoices', $v_data, true);

				$this->load->view('admin/templates/general_page', $data);
		}
		public function search_tenants_invoice($lease_id = NULL)
		{
			// var_dump($_POST);die();
			if(empty($lease_id))
			{
				$lease_id = $lease_id_searched = $this->input->post('lease_id');
			}

			$lease_id_searched = $lease_id;
			

			$search_title = '';

			if(!empty($lease_id))
			{
				$this->db->where('lease_id', $lease_id);


				$lease_id = ' AND leases.lease_id = '.$lease_id.' ';


			}

			$search = $lease_id;

			// var_dump($search);die();

			$this->session->set_userdata('invoice_lease_id_searched', $lease_id_searched);
			$this->session->set_userdata('search_tenants_invoice', $search);
			redirect('cash-office/tenants-invoices');
		}

		public function close_searched_invoices_lease()
		{
			$this->session->unset_userdata('invoice_lease_id_searched');
			$this->session->unset_userdata('search_tenants_invoice');
			redirect('cash-office/tenants-leases');
		}


		public function add_invoice($tenant_unit_id , $lease_id, $close_page = NULL)
		{
			$this->form_validation->set_rules('invoice_type_id', 'Invoice Item', 'trim|required|xss_clean');
			$this->form_validation->set_rules('amount', 'Amount', 'trim|required|xss_clean');

			//if form conatins invalid data
			if ($this->form_validation->run())
			{
				$this->accounts_model->add_invoice_item($lease_id);
				$this->session->set_userdata("success_message", 'Invoice Item successfully added');
				$response['status'] = 'success';
				$response['message'] = 'Payment successfully added';
			}
			else
			{
				$this->session->set_userdata("error_message", validation_errors());
				$response['status'] = 'fail';
				$response['message'] = validation_errors();

			}
			$redirect_url = $this->input->post('redirect_url');
			redirect($redirect_url);
		}

		public function delete_invoice_items_id($invoice_id,$lease_number=null,$lease_id=null,$lease_invoice_id=null)
		{
			
			$this->db->where('invoice_id',$invoice_id);
			if($this->db->delete('invoice'))
			{
				$this->session->set_userdata('success_message', 'you have successfully removed the invoices item');
			}
			else
			{
				$this->session->set_userdata('error_message', 'Please check the details and try again ');
			}

			if(!empty($lease_number) OR !empty($lease_id))
			{
				if(!empty($lease_invoice_id))
				{
					redirect('edit-invoice/'.$lease_id.'/'.$lease_invoice_id);
				}
				else
				{
					redirect('view-tenant-invoices/'.$lease_number.'/'.$lease_id);
				}
				
			}
			else
			{
				redirect('cash-office/tenants-leases');
			}
		}


		public function delete_lease_invoice($lease_id,$lease_invoice_id,$tenant_id = null, $module =null )
		{
			$lease_invoice_array['invoice_deleted'] = 1;
			$lease_invoice_array['deleted_by'] = $this->session->userdata('personnel_id');
			$this->db->where('lease_invoice_id',$lease_invoice_id);
			if($this->db->update('lease_invoice',$lease_invoice_array))
			{
				$this->session->set_userdata('success_message', 'You have successfully removed the invoice');
			}
			else
			{
				$this->session->set_userdata('error_message', 'Please check the details and try again ');
			}

			if($module == 1)
			{
				redirect('view-tenant-invoices/'.$lease_id.'/'.$tenant_id);
			}
			else
			{
				redirect('cash-office/tenants-invoices');
			}

			
		}


		public function update_invoice_item($invoice_id,$lease_id)
		{
			$this->form_validation->set_rules('invoice_item_amount'.$invoice_id, 'Payment Method', 'trim|required|xss_clean');

			//if form conatins invalid data
			if ($this->form_validation->run())
			{
					$array['invoice_amount'] = $this->input->post('invoice_item_amount'.$invoice_id);
					$this->db->where('invoice_id',$invoice_id);
					$this->db->update('invoice',$array);
					$this->session->set_userdata("success_message", 'Successfully updated the invoice item');
					$response['status'] = 'success';
					$response['message'] = 'Payment successfully added';
			}
			else
			{
				$this->session->set_userdata("error_message", validation_errors());
			}
			$redirect_url = $this->input->post('redirect_url');
			if(!empty($redirect_url))
			{
				redirect($redirect_url);
			}
			else {

				redirect('cash-office/tenants-accounts');
			}
		}

		public function confirm_invoice_note($tenant_unit_id , $lease_id, $close_page = NULL)
		{
			$this->form_validation->set_rules('total_amount', 'Amount', 'trim|required|xss_clean');
			$this->form_validation->set_rules('invoice_date', 'Date Invoiced', 'trim|required|xss_clean');
			$this->form_validation->set_rules('invoice_month', 'Invoice Month', 'trim|required|xss_clean');
			$this->form_validation->set_rules('invoice_year', 'Invoice Year', 'trim|required|xss_clean');
			if ($this->form_validation->run())
			{
					$this->accounts_model->confirm_invoices($lease_id);

					$this->session->set_userdata("success_message", 'Invoice successfully added');
					$response['status'] = 'success';
					$response['message'] = 'Payment successfully added';
					$redirect_url = $this->input->post('redirect_url');
					$lease_invoice_id = $this->input->post('lease_invoice_id');
					if(!empty($lease_invoice_id))
					{
						$lease_number = $this->input->post('lease_number');
						redirect('view-tenant-invoices/'.$lease_number.'/'.$lease_id);
					}
					else
					{
						redirect($redirect_url);
					}
			}
			else
			{
				$this->session->set_userdata("error_message", validation_errors());
				$response['status'] = 'fail';
				$response['message'] = validation_errors();

				$redirect_url = $this->input->post('redirect_url');
				redirect($redirect_url);

			}

			
		}
		public function get_lease_invoice_items($lease_invoice_id)
		{

			$invoice_type = $this->accounts_model->get_lease_invoice_items($lease_invoice_id);
			echo '<option value="0">Select a Type</option>';
			foreach($invoice_type->result() as $res)
			{
			  $invoice_type_id = $res->invoice_type_id;
			  $invoice_type_name = $res->invoice_type_name;

				echo '<option value="'.$invoice_type_id.'">'.$invoice_type_name.'</option>';

			}

		}

		public function get_lease_invoice_balance($lease_invoice_id,$invoice_type_id)
		{
			$invoice_amount = $this->accounts_model->get_lease_invoice_amount($lease_invoice_id,$invoice_type_id);
			$amount_paid = $this->accounts_model->get_lease_paid_amount($lease_invoice_id,$invoice_type_id);
			$credit_amount = $this->accounts_model->get_lease_credit_notes_amount($lease_invoice_id,$invoice_type_id);

			if(empty($amount_paid))
			{
				$amount_paid = 0;
			}

			if(empty($credit_amount))
			{
				$credit_amount = 0;
			}

			$response['balance'] = $invoice_amount - $amount_paid - $credit_amount;
			echo  json_encode($response);

		}

		public function get_tenants_invoices($lease_id)
		{

			$search_title = '';
			$lease_id = $lease_id_searched = $lease_id;
			$lease_id = ' AND leases.lease_id = '.$lease_id.' ';
			$search = $lease_id;

			// var_dump($search);die();

			$this->session->set_userdata('invoice_lease_id_searched', $lease_id_searched);
			$this->session->set_userdata('search_tenants_invoice', $search);
			redirect('cash-office/tenants-invoices');
		}

		public function get_tenants_credit_notes($lease_id)
		{

			$search_title = '';
			$lease_id = $lease_id_searched = $lease_id;
			$lease_id = ' AND leases.lease_id = '.$lease_id.' ';
			$search = $lease_id;

			// var_dump($search);die();

			$this->session->set_userdata('credit_lease_id_searched', $lease_id_searched);
			$this->session->set_userdata('search_tenants_credit', $search);
			redirect('cash-office/tenants-credit-notes');
		}


		public function search_mpesa_transactions()
		{
			$serial_number = $this->input->post('serial_number');
			$sender_phone = $this->input->post('sender_phone');
			$transaction_date_from = $this->input->post('transaction_date_from');
			$transaction_date_to = $this->input->post('transaction_date_to');
			// var_dump($_POST);die();
			$search_title = '';

			if(!empty($serial_number))
			{
				$serial_number = ' AND mpesa_transactions.serial_number = "'.$serial_number.'"';
			}
			if(!empty($sender_phone))
			{
				$search_title .= $sender_phone.' ';
				$sender_phone = ' AND mpesa_transactions.sender_phone LIKE \'%'.$sender_phone.'%\'';


			}
			if(!empty($transaction_date_from) && !empty($transaction_date_to))
			{
				$transaction_date = ' AND mpesa_transactions.created BETWEEN \''.$transaction_date_from.'\' AND \''.$transaction_date_to.'\'';
				$search_title .= 'payment date from '.date('jS M Y', strtotime($transaction_date_from)).' to '.date('jS M Y', strtotime($transaction_date_to)).' ';
			}

			else if(!empty($transaction_date_from))
			{
				$transaction_date = ' AND mpesa_transactions.created = \''.$transaction_date_from.'\'';
				$search_title .= 'payment date of '.date('jS M Y', strtotime($transaction_date_from)).' ';
			}

			else if(!empty($transaction_date_to))
			{
				$transaction_date = ' AND mpesa_transactions.created = \''.$transaction_date_to.'\'';
				$search_title .= 'payment date of '.date('jS M Y', strtotime($transaction_date_to)).' ';
			}

			else
			{
				$transaction_date = '';
			}


			$search = $serial_number.$sender_phone.$transaction_date;

			$this->session->set_userdata('search_mpesa_received', $search);
			redirect('cash-office/mpesa-received');
		}
		public function close_mpesa_search()
		{
			$this->session->unset_userdata('search_mpesa_received');
			redirect('cash-office/mpesa-received');
		}

		public function search_tenant_lease()
		{
			// var_dump($_POST);die();
			$lease_id = $lease_id_searched = $this->input->post('lease_id');
			$search_title = '';
			if(!empty($lease_id))
			{
				$this->db->where('lease_id', $lease_id);

				$lease_id = ' AND leases.lease_id = '.$lease_id.' ';


			}

			$search = $lease_id;

			// var_dump($search);die();

			$this->session->set_userdata('lease_number_searched', $lease_id_searched);
			$redirect_url = $this->input->post('redirect_url');
			redirect($redirect_url);
		}
		public function update_mpesa_list()
		{
			$test_url = site_url().'mpesa/cron/pull_transactions';

			$patient_details['id'] = 1;
			$data_string = json_encode($patient_details);

			try{

				$ch = curl_init($test_url);
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
				curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($ch, CURLOPT_HTTPHEADER, array(
					'Content-Type: application/json',
					'Content-Length: ' . strlen($data_string))
				);
				$result = curl_exec($ch);
				curl_close($ch);
			}
			catch(Exception $e)
			{
				$response = "something went wrong";
				echo json_encode($response.' '.$e);
			}
			redirect('cash-office/mpesa-received');
		}
		public function reverse_payment_detail($payment_id,$tenant_id,$lease_id,$module=null)
		{
			$this->db->where('lease_id = '.$lease_id.' AND payment_id ='.$payment_id);
			$query = $this->db->get('payments');

			if($query->num_rows() > 0)
			{
				$value = $query->row();
				$array = json_decode(json_encode($value), true);
				$amount_paid = $value->amount_paid;
				unset($array['payment_id']);
				unset($array['amount_paid']);
				$array['amount_paid'] = -$amount_paid;
				// var_dump($payment_id);die();
				$this->db->insert('payments',$array);
				$payment_idd = $this->db->insert_id();

				$this->db->where('payment_id ='.$payment_id);
				$query_two = $this->db->get('payment_item');

				if($query_two->num_rows() > 0)
				{
					foreach ($query_two->result() as $key => $value_two) {
						// code...
						$array_two = json_decode(json_encode($value_two), true);
						$amount_paid_two = $value_two->amount_paid;
						unset($array_two['payment_item_id']);
						unset($array_two['payment_id']);
						unset($array_two['remarks']);
						unset($array_two['amount_paid']);
						$array_two['amount_paid'] = -$amount_paid_two;
						$array_two['remarks'] = 'Payment Reversal';
						$array_two['payment_id'] = $payment_idd;
						$this->db->insert('payment_item',$array_two);
					}
				}
			}
			if($module == 1)
			{
				redirect('view-tenant-payments/'.$lease_id.'/'.$tenant_id);
			}
			else
			{
				redirect('cash-office/tenants-payments');
			}
			
		}
		public function print_mpesa_transactions()
		{

			$where = 'mpesa_status = 0 AND lease_id is null';
			$table = 'mpesa_transactions';

			$search = $this->session->userdata('search_mpesa_received');
			if(!empty($search))
			{
				$where .= $search;
			}

			$data['contacts'] = $this->site_model->get_contacts();
			$data['query'] = $this->accounts_model->get_unreconcilled_payments($table, $where,null,null,'mpesa_transactions.created');

			$this->load->view('print_mpesa_transactions', $data);
		}

		public function cancel_mpesa_payment($mpesa_id)
		{
			$this->form_validation->set_rules('cancel_description', 'Description', 'trim|required|xss_clean');
			$this->form_validation->set_rules('cancel_action_id', 'Action', 'trim|required|xss_clean');
			$redirect_url = $this->input->post('redirect_url');
			//if form conatins invalid data
			if ($this->form_validation->run())
			{
				// end of checker function
				if($this->accounts_model->cancel_mpesa_payment($mpesa_id))
				{
					$this->session->set_userdata("success_message", "Transaction successfully cancelled");
				}
				else
				{
					$this->session->set_userdata("error_message", "Oops something went wrong. Please try again");
				}
			}
			else
			{
				$this->session->set_userdata("error_message", validation_errors());

			}
			redirect($redirect_url);
		}

		public function update_all_invoices()
		{
			$this->db->where('invoice.created IS NULL AND lease_invoice.lease_invoice_id = invoice.lease_invoice_id AND invoice_type.invoice_type_id = invoice.invoice_type');
			$query = $this->db->get('invoice,invoice_type,lease_invoice');

			if($query->num_rows() > 0)
			{
				foreach ($query->result() as $key => $value) {
					// code...
					$invoice_year = $value->invoice_year;
					$invoice_id = $value->invoice_id;
					$invoice_month = $value->invoice_month;
					$invoice_date = $value->invoice_date;
					$lease_invoice_id = $value->lease_invoice_id;
					$invoice_type_name = $value->invoice_type_name;

					$concate = $invoice_year.'-'.$invoice_month;
					$date = date('F Y',strtotime($concate));

					$remarks = $invoice_type_name.' for '.$date;
					$update_query['remarks'] = $remarks;
					$update_query['created'] = $invoice_date;

					// var_dump($update_query);die();
					$this->db->where('invoice_id',$invoice_id);
					$this->db->update('invoice',$update_query);
				}
			}
		}

		public function download_pdf()
		{

						$html = '
									// You can put your HTML code here
									< h1 > Lorem Ipsum... </ h1 >
									< h2 > Lorem Ipsum... </ h2 >
									< h3 > Lorem Ipsum... </ h3 >
									< h4 > Lorem Ipsum... </ h4 >
									';

						$this->load->library('mpdf');

						$contacts = $data['contacts'];

						// $document_number = date("ymdhis");
						$receipt_month_date = date('Y-m-d');
						$receipt_month = date('M',strtotime($receipt_month_date));
						$receipt_year = date('Y',strtotime($receipt_month_date));
						$title = date('ymdhis').'-REP.pdf';
						$invoice = $title;


						$mpdf=new mPDF();
						$mpdf->WriteHTML($html);
						$mpdf->Output($title, 'F');

						while(!file_exists($title))
						{

						}
						$message['subject'] = ' Receipt';
						$message['text'] = ' changes
																	';
						$sender_email = $contacts['email'];
						$shopping = "";
						$from = $contacts['email'];

						$button = '';
						$sender['email'] = 'marttkip@gmail.com';// $contacts['email'];
						$sender['name'] = $contacts['company_name'];
						$receiver['email'] = 'marttkip@gmail.com';
						$receiver['name'] = 'Martin';
						$payslip = $title;
						// var_dump($payslip);die();

						// var_dump($this->invoice_path );die();
						$this->email_model->send_sendgrid_mail($receiver, $sender, $message, $payslip);
						unlink($title);
						// save the invoice sent out on the database
						// $insert_array = array('email'=>$receiver['email'],'client_name'=>$tenant_name,'email_body'=>$message['text'],'type_of_account'=>1,'email_attachment'=>$title,'date_created'=>date('Y-m-d'),'email_type'=>2);
						// $this->db->insert('email',$insert_array);
		}

	public function tenant_leases()
	{

		$where = 'tenants.tenant_id > 0 AND tenants.tenant_id = tenant_unit.tenant_id AND tenant_unit.rental_unit_id = rental_unit.rental_unit_id  AND tenant_unit.tenant_unit_id = leases.tenant_unit_id  AND property.property_id = rental_unit.property_id AND property.property_deleted = 0';
		$table = 'tenants,tenant_unit,rental_unit,leases,property';


		$accounts_search = $this->session->userdata('all_tenants_account_search');

		if(!empty($accounts_search))
		{
			$where .= $accounts_search;

		}
		else
		{
			$where .=  ' AND leases.lease_status = 1';
		}


		$properties = $this->session->userdata('properties');

		if(isset($properties) AND !empty($properties))
		{
			$where .= $properties;
		}
		// var_dump($properties); die();
		$segment = 3;
		//pagination

		$this->load->library('pagination');
		$config['base_url'] = base_url().'cash-office/tenants-leases';
		$config['total_rows'] = $this->tenants_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;

		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';

		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';

		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';

		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';

		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active">';
		$config['cur_tag_close'] = '</li>';

		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);

		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
    	$v_data["links"] = $this->pagination->create_links();
		$query = $this->accounts_model->get_all_tenants($table, $where, $config["per_page"], $page, $order='property.property_name,rental_unit.rental_unit_number,rental_unit.rental_unit_name,leases.lease_status', $order_method='ASC');

		$properties = $this->property_model->get_active_property();

		// $this->accounts_model->update_invoices();


		$rs8 = $properties->result();
		$property_list = '';
		foreach ($rs8 as $property_rs) :
			$property_id = $property_rs->property_id;
			$property_name = $property_rs->property_name;
			$property_location = $property_rs->property_location;

		    $property_list .="<option value='".$property_id."'>".$property_name." Location: ".$property_location."</option>";

		endforeach;
		$v_data['property_list'] = $property_list;


		$data['title'] = 'All Tenants Accounts';
		$v_data['title'] = $data['title'];
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		$data['content'] = $this->load->view('cash_office/tenants_list', $v_data, true);

		$this->load->view('admin/templates/general_page', $data);
	}

	public function search_tenant_accounts()
	{
		$property_id = $this->input->post('property_id');
		$tenant_name = $this->input->post('tenant_name');
		$contract_no = $this->input->post('contract_no');
		$rental_unit_name = $this->input->post('rental_unit_name');
		$lease_status = $this->input->post('lease_status');
		
		$search_title = '';
		
		if(!empty($property_id))
		{
			$this->db->where('property_id', $property_id);
			$query = $this->db->get('property');
		
			if($query->num_rows() > 0)
			{
				$row = $query->row();
				$search_title .= $row->property_name.' ';
			}
		
		
			$property_id = ' AND rental_unit.property_id = '.$property_id.' ';
		
		
		}
		
		if(!empty($tenant_name))
		{
			$search_title .= $tenant_name;

			$counter = explode(' ', $tenant_name);

			$total = count($counter);
			// var_dump($count);die();
			$count = 1;
			$tenant_name = ' AND (';
			for($r = 0; $r < $total; $r++)
			{
				if($count == $total)
				{
					$tenant_name .= ' tenants.tenant_name LIKE \'%'.$counter[$r].'%\'';
				}

				else
				{
					$tenant_name .= ' tenants.tenant_name LIKE \'%'.$counter[$r].'%\' AND ';
				}
				$count++;
			}
			$tenant_name .= ') ';
			
			// $tenant_name = ' AND tenants.tenant_name LIKE \'%'.$tenant_name.'%\'';
		
		
		}
		else
		{
			$tenant_name = '';
			$search_title .= '';
		}
		
		if(!empty($contract_no))
		{
			$search_title .= $contract_no.' ';
			$contract_no = ' AND leases.lease_number = '.$contract_no.'';
		
		
		}
		else
		{
			$contract_no = '';
			$search_title .= '';
		}

		if(!empty($lease_status))
		{
			$search_title .= $lease_status.' ';
			$lease_status = ' AND leases.lease_status = '.$lease_status.'';
		
		
		}
		else
		{
			$lease_status = '';
			$search_title .= '';
		}
		
		if(!empty($rental_unit_name))
		{
			$search_title .= $rental_unit_name.' ';
			$rental_unit_name = ' AND rental_unit.rental_unit_name LIKE \'%'.$rental_unit_name.'%\'';
		
		}
		else
		{
			$rental_unit_name = '';
			$search_title .= '';
		}
		
		
		$search = $property_id.$tenant_name.$rental_unit_name.$contract_no.$lease_status;
		$property_search = $property_id;
		// var_dump($search); die();
		
		$this->session->set_userdata('property_search', $property_search);
		$this->session->set_userdata('all_tenants_account_search', $search);
		$this->session->set_userdata('accounts_search_title', $search_title);

		redirect('cash-office/tenants-leases');
	}

	public function close_leases_search()
	{
		$this->session->unset_userdata('all_tenants_account_search');
		$this->session->unset_userdata('accounts_search_title');


		redirect('cash-office/tenants-leases');
	}


	public function tenant_lease_invoices($lease_number,$lease_id)
	{

		$where = 'lease_invoice.lease_invoice_id = invoice.lease_invoice_id AND lease_invoice.invoice_deleted = 0 AND lease_invoice.lease_id = '.$lease_id;
		$table = 'invoice,lease_invoice';

		$segment = 4;
		//pagination

		$this->load->library('pagination');
		$config['base_url'] = base_url().'view-tenant-invoices/'.$lease_number.'/'.$lease_id;
		$config['total_rows'] = $this->tenants_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;

		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';

		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';

		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';

		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';

		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active">';
		$config['cur_tag_close'] = '</li>';

		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);

		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
    	$v_data["links"] = $this->pagination->create_links();
		$query = $this->accounts_model->get_all_invoices_transactions($table, $where, $config["per_page"], $page, $order='lease_invoice.invoice_date', $order_method='DESC');



		$data['title'] = 'Lease Invoices';
		$v_data['title'] = $data['title'];
		$v_data['lease_invoices'] = $query;
		$v_data['page'] = $page;
		$v_data['lease_id'] = $lease_id;

		$data['content'] = $this->load->view('leases/lease_invoices', $v_data, true);

		$this->load->view('admin/templates/general_page', $data);
	}






	public function tenant_lease_payments($lease_number,$lease_id)
	{

			
		$where = 'leases.lease_id = payments.lease_id AND payments.payment_id = payment_item.payment_id AND payments.account_id = '.$lease_number.' AND payments.lease_id = '.$lease_id.'  AND payments.cancel = 0';
		$table = 'payments,leases,payment_item';


		// $accounts_search = $this->session->userdata('all_tenants_account_search');

		// if(!empty($accounts_search))
		// {
		// 	$where .= $accounts_search;

		// }


		// $properties = $this->session->userdata('properties');

		// if(isset($properties) AND !empty($properties))
		// {
		// 	$where .= $properties;
		// }
		// var_dump($properties); die();
		$segment = 4;
		//pagination

		$this->load->library('pagination');
		$config['base_url'] = base_url().'view-tenant-payments/'.$lease_number.'/'.$lease_id;
		$config['total_rows'] = $this->tenants_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;

		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';

		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';

		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';

		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';

		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active">';
		$config['cur_tag_close'] = '</li>';

		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);

		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
    	$v_data["links"] = $this->pagination->create_links();
		$query = $this->accounts_model->get_all_payment_transactions($table, $where, $config["per_page"], $page, $order='payments.payment_date', $order_method='DESC');



		$data['title'] = 'Lease Payments';
		$v_data['title'] = $data['title'];
		$v_data['lease_payments'] = $query;
		$v_data['page'] = $page;
		$v_data['lease_id'] = $lease_id;

		$data['content'] = $this->load->view('leases/lease_payments', $v_data, true);

		$this->load->view('admin/templates/general_page', $data);
	}

	public function tenant_lease_credits($lease_number,$lease_id)
	{

		$where = 'leases.lease_id = credit_notes.lease_id AND credit_notes.credit_note_id = credit_note_item.credit_note_id AND credit_notes.account_id = '.$lease_number;
		$table = 'credit_notes,leases,credit_note_item';


		// $accounts_search = $this->session->userdata('all_tenants_account_search');

		// if(!empty($accounts_search))
		// {
		// 	$where .= $accounts_search;

		// }


		// $properties = $this->session->userdata('properties');

		// if(isset($properties) AND !empty($properties))
		// {
		// 	$where .= $properties;
		// }
		// var_dump($properties); die();
		$segment = 4;
		//pagination

		$this->load->library('pagination');
		$config['base_url'] = base_url().'view-tenant-credits/'.$lease_number.'/'.$lease_id;
		$config['total_rows'] = $this->tenants_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;

		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';

		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';

		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';

		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';

		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active">';
		$config['cur_tag_close'] = '</li>';

		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);

		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
    	$v_data["links"] = $this->pagination->create_links();
		$query = $this->accounts_model->get_all_transactions($table, $where, $config["per_page"], $page, $order='credit_notes.credit_note_date', $order_method='DESC');



		$data['title'] = 'Lease Credit Notes';
		$v_data['title'] = $data['title'];
		$v_data['lease_credits'] = $query;
		$v_data['page'] = $page;
		$v_data['lease_id'] = $lease_id;

		$data['content'] = $this->load->view('leases/lease_credits', $v_data, true);

		$this->load->view('admin/templates/general_page', $data);

	}


	public function tenant_lease_debits($lease_number,$lease_id)
	{

		$where = 'leases.lease_id = debit_notes.lease_id AND debit_notes.debit_note_id = debit_note_item.debit_note_id AND debit_notes.account_id = '.$lease_number;
		$table = 'debit_notes,leases,debit_note_item';


		// $accounts_search = $this->session->userdata('all_tenants_account_search');

		// if(!empty($accounts_search))
		// {
		// 	$where .= $accounts_search;

		// }


		// $properties = $this->session->userdata('properties');

		// if(isset($properties) AND !empty($properties))
		// {
		// 	$where .= $properties;
		// }
		// var_dump($properties); die();
		$segment = 4;
		//pagination

		$this->load->library('pagination');
		$config['base_url'] = base_url().'view-tenant-debits/'.$lease_number.'/'.$lease_id;
		$config['total_rows'] = $this->tenants_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;

		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';

		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';

		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';

		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';

		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active">';
		$config['cur_tag_close'] = '</li>';

		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);

		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
    	$v_data["links"] = $this->pagination->create_links();
		$query = $this->accounts_model->get_all_transactions($table, $where, $config["per_page"], $page, $order='debit_notes.debit_note_date', $order_method='DESC');



		$data['title'] = 'Lease Debit Notes';
		$v_data['title'] = $data['title'];
		$v_data['lease_debits'] = $query;
		$v_data['page'] = $page;
		$v_data['lease_id'] = $lease_id;

		$data['content'] = $this->load->view('leases/lease_debits', $v_data, true);

		$this->load->view('admin/templates/general_page', $data);

	}


	public function edit_tenant_payment($lease_id,$payment_id,$module=null)
	{
		$data = array('lease_id' => $lease_id,'payment_id' => $payment_id,'module' => $module);
		$data['contacts'] = $this->site_model->get_contacts();
		
		$data['title'] = 'Edit Payment Detail';
		$v_data['title'] = $data['title'];
		$v_data['lease_debits'] = $query;
		$v_data['page'] = $page;
		$v_data['lease_id'] = $lease_id;
		$v_data['payment_id'] = $payment_id;

		$data['content'] = $this->load->view('leases/edit_payment_detail', $v_data, true);

		$this->load->view('admin/templates/general_page', $data);


	}


	public function edit_tenant_invoice($lease_id,$lease_invoice_id,$module=null)
	{
		$data = array('lease_id' => $lease_id,'lease_invoice_id' => $lease_invoice_id,'module' => $module);
		$data['contacts'] = $this->site_model->get_contacts();
		
		$data['title'] = 'Edit Invoice Detail';
		$v_data['title'] = $data['title'];
		$v_data['lease_debits'] = $query;
		$v_data['page'] = $page;
		$v_data['lease_id'] = $lease_id;
		$v_data['lease_invoice_id'] = $lease_invoice_id;
		// var_dump($v_data);die();
		$data['content'] = $this->load->view('leases/edit_invoice_detail', $v_data, true);

		$this->load->view('admin/templates/general_page', $data);


	}

	public function send_tenants_invoices()
	{

		$this->db->where('sms_type = 10 AND type_of_account = 1 AND sms_sent = 0 AND phone_number IS NOT NULL');
		$query = $this->db->get('sms');

		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$message = $value->message;
				$phone_number = $value->phone_number;
				$client_name = $value->client_name;
				$lease_invoice_id = $value->lease_invoice_id;
				$lease_id = $value->lease_id;
				$sms_id = $value->sms_id;

				if(!empty($phone_number))
				{
					$date = date('jS M Y',strtotime(date('Y-m-d')));
					$phone_array = explode('/', $phone_number);
					$total_rows = count($phone_array);

					for($r = 0; $r < $total_rows; $r++)
					{
						$tenant_phone_number = $phone_array[$r];

						// remove any spaces
						$tenant_phone_number = str_replace(' ', '', $tenant_phone_number);
						// $tenant_phone_number = '0727418460';
										

						$this->accounts_model->sms($tenant_phone_number,$message,$client_name);
						// var_dump($tenant_phone_number);die();	

					}

				}

				$update_array['sent_status'] = 1;
				$this->db->where('lease_invoice_id =  '.$lease_invoice_id.' AND lease_id = '.$lease_id);
				$this->db->update('lease_invoice',$update_array);

				$update_array2['sms_sent'] = 1;
				$this->db->where('sms_id =  '.$sms_id);
				$this->db->update('sms',$update_array2);
			}
		}
		redirect('cash-office/tenants-leases');

	}
}

?>
