<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');
header('Content-type: application/json;');
class Mpesa  extends MX_Controller
{

  public function mpesa_receive_transaction($type)
	{
		$json = file_get_contents('php://input');

		$insert_array['test'] = $json;
		$this->db->insert('mpesa_test',$insert_array);
		$decoded = json_decode($json);
	    $serial_number = null;
	    if(isset($decoded))
	    {
	      $serial_number = $decoded->TransID;
	    }

		if(!empty($serial_number))
		{
			$TransAmount = $decoded->TransAmount;
			$TransTime = $decoded->TransTime;
			$TransID = $decoded->TransID;
			$BusinessShortCode = $decoded->BusinessShortCode;
			$BillRefNumber = $decoded->BillRefNumber;
			$MSISDN = $decoded->MSISDN;
			$FirstName = $decoded->FirstName;
			$MiddleName = $decoded->MiddleName;
			$LastName = $decoded->LastName;
			$OrgAccountBalance = $decoded->OrgAccountBalance;
			$sender_name = $FirstName.' '.$MiddleName.' '.$LastName;
	    $sender_name = str_replace('%20', ' ', $sender_name);
			$insert_array2['serial_number'] = $TransID;
			$insert_array2['account_number'] = $BillRefNumber;
			$insert_array2['mpesa_status'] = $type;
			$insert_array2['sender_name'] = $sender_name;
			$insert_array2['sender_phone'] = $MSISDN;
			$insert_array2['amount'] = $TransAmount;
			$insert_array2['created'] = date('Y-m-d H:i:s');
			if($this->db->insert('mpesa_transactions',$insert_array2))
			{

				$response['ResultCode'] = 0;
				$response['ResultDesc'] = 'The service was accepted successfully';
				$response['ThirdPartyTransID'] = date('YmdHis');

				$tenant_phone_number = '+'.$MSISDN;

			}
			else
			{
				$response['ResultCode'] = 0;
				$response['ResultDesc'] = 'Could not process payment';
				$response['ThirdPartyTransID'] = date('YmdHis');
			}
		}
		else
		{
			$response['ResultCode'] = 1;
			$response['ResultDesc'] = 'Could not process payment';
			$response['ThirdPartyTransID'] = date('YmdHis');
		}
		echo json_encode($response);
	}

}
?>
