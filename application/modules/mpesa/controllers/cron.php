<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// header('Content-type: application/json;');
error_reporting(E_ALL);
ini_set('display_errors','1');
class Cron  extends MX_Controller
{
  function __construct()
	{
		parent:: __construct();
		// $this->load->model('cloud_model');
	}
  public function pull_transactions()
	{

			$test_url = 'https://simplex-financials.com/allan-bradley/mpesa-pull';

      $patient_details['id'] = 1;
			$data_string = json_encode($patient_details);
       // var_dump($data_string);die();
			try{

				$ch = curl_init($test_url);
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
				curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($ch, CURLOPT_HTTPHEADER, array(
					'Content-Type: application/json',
					'Content-Length: ' . strlen($data_string))
				);
				$result = curl_exec($ch);
				curl_close($ch);

				$this->sync_down_response($result);

				 // $response = $result;
				 // echo json_encode($response);
			}
			catch(Exception $e)
			{
				$response = "something went wrong";
				echo json_encode($response.' '.$e); die();
			}


  }

  public function get_mpesa_transactions()
  {
    $where = 'mpesa_transactions.sync_status = 0 AND mpesa_status = 1';
    // var_dump($where); die();
    $this->db->where($where);
    $this->db->select('mpesa_transactions.*');
    $query_transactions = $this->db->get('mpesa_transactions');

    $patients['transactions'] = array();
    // var_dump($where); die();
    if($query_transactions->num_rows() > 0)
    {
      foreach ($query_transactions->result() as $value) {
        # code...
        array_push($patients['transactions'], $value);
      }
    }

    echo json_encode($patients);
  }


  public function sync_down_response($result)
  {
    $sync_response = json_decode($result);


    $response_data = $sync_response->transactions;
    //var_dump($response_data);die();



    $total_rows = count($response_data);

    $online_parse['transactions'] = array();
    for($r = 0; $r < $total_rows; $r++)
    {
      $array_passed = array();
      $patient_data = $response_data[$r];
      $online_id = $patient_data->mpesa_id;
      $serial_number = $patient_data->serial_number;
      $created = $patient_data->created;
      $account_number = $patient_data->account_number;
      $sender_name = $patient_data->sender_name;
      $sender_phone = $patient_data->sender_phone;
      $amount = $patient_data->amount;
      $time_received = $patient_data->time_received;


      $insert_array['remote_id'] = $online_id;
      $insert_array['serial_number'] = $serial_number;
      $insert_array['created'] = $created;
      $insert_array['transaction_date'] = $created;
      $insert_array['account_number'] = $account_number;
      $insert_array['sender_name'] = $sender_name;
      $insert_array['sender_phone'] = $sender_phone;
      $insert_array['amount'] = $amount;
      $insert_array['time_received'] = $time_received;
      // check if the serial number pcntl_wexitstatus

      $this->db->where('remote_id',$online_id);
      $query = $this->db->get('mpesa_transactions');
      if($query->num_rows() == 0)
      {
        // do the insert
        $this->db->insert('mpesa_transactions',$insert_array);
        $local_id = $this->db->insert_id();
        // parse the data up
        $array_passed['local_id'] = $local_id;
        $array_passed['mpesa_id'] = $online_id;
        $array_passed['sync_status'] = 1;


      }
      else {
        // parse and array for updating the the online values
        $row = $query->row();
        $local_id = $row->mpesa_id;
        $array_passed['local_id'] = $local_id;
        $array_passed['mpesa_id'] = $online_id;
        $array_passed['sync_status'] = 1;
      }

      array_push($online_parse['transactions'], $array_passed);

    }
    if($total_rows > 0)
    {

      $this->sync_up_response($online_parse);
    }

  }
  public function delete_multiple()
  {
   $query =  $this->db->query('SELECT serial_number, COUNT(*)
FROM mpesa_transactions
GROUP BY remote_id
HAVING COUNT(*) > 1');

   var_dump($query->num_rows());die();
  }
  public function sync_up_response($array)
  {

    $test_url = 'https://simplex-financials.com/allan-bradley/mpesa-push';

    $data_string = json_encode($array);
    // var_dump($data_string);die();
    try{

      $ch = curl_init($test_url);
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
      curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Content-Length: ' . strlen($data_string))
      );
      $result = curl_exec($ch);
      curl_close($ch);

    }
    catch(Exception $e)
    {
      $response = "something went wrong";
      echo json_encode($response.' '.$e);
    }



  }

  public function push_mpesa_transactions()
  {
    $json = file_get_contents('php://input');

    $decoded = json_decode($json);

    $response_data = $decoded->transactions;
    $total_rows = count($response_data);


    if($total_rows > 0)
    {
      for($r = 0; $r < $total_rows; $r++)
      {
        $patient_data = $response_data[$r];
        $mpesa_id = $patient_data->mpesa_id;
        $local_id = $patient_data->local_id;
        $sync_status = $patient_data->sync_status;

        $update_array['local_id'] = $local_id;
        $update_array['sync_status'] = $sync_status;

        // var_dump($update_array);die();
        $this->db->where('mpesa_id',$mpesa_id);

        $this->db->update('mpesa_transactions',$update_array);

      }
    }


  }
}
