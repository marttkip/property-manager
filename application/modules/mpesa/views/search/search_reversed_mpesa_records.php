
<div class="box box-danger">
    <div class="box-header with-border">
      <h3 class="box-title">Search Transactions</h3>

      <div class="box-tools pull-right">

          <!-- <a href="<?php echo site_url();?>update-mpesa-payments" class="btn btn-sm btn-danger "  ><i class="fa fa-recycle"></i> Update payment list</a> -->
      </div>
    </div>
    <div class="box-body">
       <div class="padd">
           <?php
           echo form_open("search-reversed-mpesa-transactions", array("class" => "form-horizontal"));
           ?>
           <div class="row">

               <div class="col-md-3">
                   <div class="form-group">
                       <label class="col-md-4 control-label">Name: </label>

                       <div class="col-md-8">
                           <input type="text" class="form-control" name="sender_name" placeholder="Name" autocomplete="off"/>
                       </div>
                   </div>
               </div>
               <div class="col-md-3">
                   <div class="form-group">
                       <label class="col-md-4 control-label">Serial: </label>

                       <div class="col-md-8">
                           <input type="text" class="form-control" name="serial_number" placeholder="Serial" autocomplete="off"/>
                       </div>
                   </div>
               </div>
               <div class="col-md-2">
                   <div class="form-group">
                       <label class="col-md-2 control-label">FROM: </label>

                       <div class="col-md-10">
                            <input type="text" class="form-control" name="transaction_date_from" placeholder="Date"  id="datepicker" autocomplete="off">
                       </div>
                   </div>
               </div>
               <div class="col-md-2">
                   <div class="form-group">
                       <label class="col-md-2 control-label">TO : </label>

                       <div class="col-md-10">
                            <input type="text" class="form-control" name="transaction_date_to" placeholder="Date"  id="datepicker1" autocomplete="off">
                       </div>
                   </div>
               </div>


               <div class="col-md-2">
                   <div class="center-align">
                       <button type="submit" class="btn btn-info btn-sm">Search</button>
                   </div>
               </div>
           </div>


           <?php
           echo form_close();
           ?>
       </div>
   </div>
</div>
