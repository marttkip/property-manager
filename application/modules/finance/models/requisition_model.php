<?php

class Requisition_model extends CI_Model
{

  public function count_items($table, $where, $limit = NULL)
  {
    if($limit != NULL)
    {
      $this->db->limit($limit);
    }
    $this->db->from($table);
    $this->db->where($where);
    return $this->db->count_all_results();
  }

  /*
  * Retrieve all creditor
  * @param string $table
  *   @param string $where
  *
  */
  public function get_all_requisition($table, $where, $per_page, $page, $order = 'requisition_id', $order_method = 'ASC')
  {
    //retrieve all users
    $this->db->from($table);
    $this->db->select('*,property_owners.property_owner_name,tenants.tenant_name,personnel.personnel_fname,personnel.personnel_onames,requisition.created AS date_added');
    $this->db->where($where);
    $this->db->order_by($order, $order_method);
    $this->db->join('leases', 'leases.lease_id = requisition.lease_id','LEFT');
    $this->db->join('tenants', 'tenants.tenant_id = leases.tenant_id','LEFT');
    $this->db->join('property_owners', 'property_owners.property_owner_id = requisition.landlord_id','LEFT');
    $this->db->join('personnel', 'personnel.personnel_id = requisition.created_by','LEFT');
    $query = $this->db->get('', $per_page, $page);

    return $query;
  }

  public function add_requisition()
  {
  	 $requisition_type = $this->input->post('requisition_type');
  	  $requisition_description = $this->input->post('requisition_description');

      $landlord_id = $this->input->post('landlord_id');
      $lease_id = $this->input->post('lease_id');

      if($requisition_type == 0)
      {
        $prefix = 'RR';
      }
      else if($requisition_type == 1)
      {
        $prefix = 'RF';
      }
      else if($requisition_type == 2)
      {
        $prefix = 'UP';
      }

      $suffix = $suffix_number = $this->create_requisition_number($requisition_type);


      if($suffix < 1000)
      {
        if($suffix < 100)
        {
          if($suffix < 10)
          {
            $suffix = '00'.$suffix; 
          }
          else
          {
             $suffix = '0'.$suffix; 
          }
        }
        else
        {
          $suffix = $suffix; 
        }
      }

      $requisition_number = $prefix.$suffix;



      if(empty($landlord_id))
      {
        $landlord_id = 0;
      }

      if(empty($lease_id))
      {
        $lease_id = 0;
      }

  	  $array['created'] = date('Y-m-d H:i:s');
  	  $array['created_by'] = $this->session->userdata('created_by');
  	  $array['requisition_description'] = $requisition_description;
  	  $array['requisition_type'] = $requisition_type;
      $array['lease_id'] = $lease_id;
      $array['landlord_id'] = $landlord_id;
      $array['requisition_number'] = $requisition_number;
      $array['prefix'] = $prefix;
      $array['suffix'] = $suffix_number;

  	 if($this->db->insert('requisition',$array))
  	 {
  	 	return TRUE;
  	 }
  	 else
  	 {
  	 	return FALSE;
  	 }

  }

  public function create_requisition_number($requisition_type)
  {
    //select product code
    $this->db->where('requisition_id > 0 AND requisition_type = '.$requisition_type.'');
    $this->db->from('requisition');
    $this->db->select('MAX(suffix) AS number');
    $query = $this->db->get();
    if($query->num_rows() > 0)
    {
      $result = $query->result();
      $number =  $result[0]->number;

      $number++;//go to the next number
      $number = $number;
    }
    else{//start generating receipt numbers
      // $current_year = str_replace('20', '', $current_year);
      $number = 1;
    }
    // var_dump($number); die();
    return $number;
  }

  public function get_notes($notes_type_id, $requisition_id)
  {
    $created_by = $this->session->userdata('personnel_id');
    $this->db->select('notes.*, notes_type.notes_type_name, personnel.personnel_fname');
    $this->db->where('notes.requisition_id = '.$requisition_id.' AND notes.notes_type_id = '.$notes_type_id.' AND notes.notes_status = 1 AND notes.notes_type_id = notes_type.notes_type_id AND notes.created_by ='.$created_by);
    $this->db->join('personnel', 'personnel.personnel_id = notes.created_by', 'left');
    $this->db->order_by('notes_date', 'ASC');
    $this->db->order_by('notes_time', 'ASC');
    $query = $this->db->get('notes, notes_type');
    
    return $query;
  }

  function check_patient_notes($requisition_id,$notes_type_id){
    $table = "notes";
    $where = "requisition_id = ".$requisition_id." AND notes_type_id = ".$notes_type_id;
    $items = "*";

    $this->db->where($where);
    $query = $this->db->get($table);

    if($query->num_rows() > 0)
    {
      foreach ($query->result() as $key => $value) {
        # code...
        $notes_id = $value->notes_id;
      }
      return $notes_id;
    }
    else
    {
      return FALSE;
    }
    
    
  }

  public function update_notes($notes_id, $signature_name, $personnel_id)
  {
    $notes=$this->input->post('notes');
    $date=$this->input->post('date');
    $time=$this->input->post('time');

    //  enter into the nurse notes trail 
    $trail_data = array(
            "notes_name" => $notes,
            "notes_time" => date('H:i:s'),
            "notes_date" => date('Y-m-d'),
            'modified_by'=>$personnel_id
        );
    $this->db->where('notes_id', $notes_id);
    if($this->db->update('notes', $trail_data))
    {
      return TRUE;
    }
    
    else
    {
      return FALSE;
    }
  }

  public function add_notes($requisition_id, $notes_type_id, $signature_name, $personnel_id)
  {
    $notes=$this->input->post('notes');
    $date=$this->input->post('date');
    $time=$this->input->post('time');

    //  enter into the nurse notes trail 
    $trail_data = array(
            "notes_type_id" => $notes_type_id,
            "requisition_id" => $requisition_id,
            "notes_name" => $notes,
            "notes_time" => date('H:i:s'),
            "notes_date" => date('Y-m-d'),
            "notes_signature" => $signature_name,
            'created'=>date('Y-m-d H:i:s'),
            'created_by'=>$personnel_id,
            'modified_by'=>$personnel_id
        );

    if($this->db->insert('notes', $trail_data))
    {
      return $this->db->insert_id();
    }
    
    else
    {
      return FALSE;
    }
  }

  public function get_requisition_detail($requisition_id)
  {
      $this->db->from('requisition');
      $this->db->select('*,property_owners.property_owner_name,tenants.tenant_name,personnel.personnel_fname,personnel.personnel_onames,requisition.created AS date_added');
      $this->db->where('requisition_id > 0');
      $this->db->join('leases', 'leases.lease_id = requisition.lease_id','LEFT');
      $this->db->join('tenants', 'tenants.tenant_id = leases.tenant_id','LEFT');
      $this->db->join('property_owners', 'property_owners.property_owner_id = requisition.landlord_id','LEFT');
      $this->db->join('personnel', 'personnel.personnel_id = requisition.created_by','LEFT');
      $query = $this->db->get('');

      return $query;
  }

  public function close_requisition($requisition_id,$document)
  {
      $array['document'] = $document;
      $array['requisition_status'] = 2;
      $array['closed_by'] = $this->session->userdata('personnel_id');
      $array['closed_date'] = date('Y-m-d');

      $this->db->where('requisition_id',$requisition_id);
      if($this->db->update('requisition',$array))
      {
        return TRUE;
      } 
      else
      {
        return FALSE;
      }
  }

}
?>