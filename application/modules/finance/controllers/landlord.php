<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once "./application/modules/admin/controllers/admin.php";

class Landlord extends admin
{
	function __construct()
	{
		parent:: __construct();

    $this->load->model('finance/landlord_model');
		$this->load->model('real_estate_administration/tenants_model');
    $this->load->model('accounts/accounts_model');
    $this->load->model('finance/purchases_model');
    $this->load->model('real_estate_administration/property_model');
	}

	public function landlord_payments()
	{
		//form validation
		// $this->form_validation->set_rules('account_from_id', 'From','required|xss_clean');
		// $this->form_validation->set_rules('account_to_id', 'Account To','required|xss_clean');
		// $this->form_validation->set_rules('transacted_amount', 'Amount','required|xss_clean');
  //   $this->form_validation->set_rules('reference_number', 'Reference Number','required|xss_clean');
		// $this->form_validation->set_rules('description', 'Description','required|xss_clean');
		// // $this->form_validation->set_rules('account_to_type', 'Account To Type','required|xss_clean');
		// // $this->form_validation->set_rules('payment_date', 'Payment Date','required|xss_clean');

		// if ($this->form_validation->run())
		// {
		// 	//update order
		// 	if($this->landlord_model->add_payment_amount())
		// 	{
		// 		$this->session->set_userdata('success_message', 'Cheque successfully writted to account');


		// 		redirect('accounting/landlord-payments');
		// 	}

		// 	else
		// 	{
		// 		$this->session->set_userdata('error_message', 'Could not write cheque. Please try again');
		// 	}
		// }
		// else
		// {
		// 	$this->session->set_userdata('error_message', validation_errors());
		// }



		//open the add new order
		$v_data['accounts'] = $this->purchases_model->get_child_accounts("Bank");
    $v_data['creditors'] = $this->purchases_model->get_creditor();
    $v_data['expense_accounts']= $this->purchases_model->get_child_accounts("Income Accounts");

		$where = 'landlord_transaction_id > 0  AND landlord_transaction_deleted < 2';

    $search_payments = $this->session->userdata('search_landlord_payments');
    if($search_payments)
    {
      $where .= $search_payments;
    }
		$table = 'landlord_transactions';

		$segment = 3;
		$this->load->library('pagination');
		$config['base_url'] = site_url().'accounting/landlord-payments';
		$config['total_rows'] = $this->landlord_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;

		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';

		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';

		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';

		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';

		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';

		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);

		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->landlord_model->get_account_payments_transactions($table, $where, $config["per_page"], $page, $order='landlord_transactions.document_number', $order_method='DESC');
		// var_dump($query); die();

		$data['title'] = 'Landlord Payments';
		$v_data['title'] = $data['title'];

		$v_data['query_purchases'] = $query;
		$v_data['page'] = $page;

		$data['title'] = $v_data['title']= ' Landlord Payments';

		$data['content'] = $this->load->view('landlord/landlord_transactions', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}

  public function record_landlord_transaction()
  {
    //form validation
		$this->form_validation->set_rules('account_to_id', 'Expense Account','xss_clean');
		$this->form_validation->set_rules('transacted_amount', 'Amount','required|xss_clean');
    $this->form_validation->set_rules('transaction_number', 'Reference Number','required|xss_clean');
		$this->form_validation->set_rules('description', 'Description','required|xss_clean');
    $this->form_validation->set_rules('property_id', 'Property','required|xss_clean');
    $this->form_validation->set_rules('transaction_type_id', 'Type','required|xss_clean');
    $this->form_validation->set_rules('bank_id', 'Type','xss_clean');
		$this->form_validation->set_rules('month', 'Type','required|xss_clean');
		$this->form_validation->set_rules('year', 'Type','required|xss_clean');
    // var_dump($_POST);die();
		$transaction_type_id = $this->input->post('transaction_type_id');
		if($transaction_type_id == 4)
		{
			$this->form_validation->set_rules('property_to_id', 'Property To','required|xss_clean');
		}
		if ($this->form_validation->run())
		{
			//update order

			if($this->landlord_model->add_payment_amount())
			{
				$this->session->set_userdata('success_message', 'Amount has been successfully added');
			}

			else
			{
				$this->session->set_userdata('error_message', 'Could not write cheque. Please try again');
			}
		}
		else
		{
			$this->session->set_userdata('error_message', validation_errors());
		}

    redirect('accounting/landlord-payments');
  }

	public function landlord_receipts()
	{
		//form validation
		// $this->form_validation->set_rules('account_from_id', 'From','required|xss_clean');
		// $this->form_validation->set_rules('account_to_id', 'Account To','required|xss_clean');
		// $this->form_validation->set_rules('transacted_amount', 'Amount','required|xss_clean');
  //   	$this->form_validation->set_rules('reference_number', 'Reference Number','required|xss_clean');
		// $this->form_validation->set_rules('description', 'Description','required|xss_clean');
		// // $this->form_validation->set_rules('account_to_type', 'Account To Type','required|xss_clean');
		// // $this->form_validation->set_rules('payment_date', 'Payment Date','required|xss_clean');

		// if ($this->form_validation->run())
		// {
		// 	//update order
		// 	if($this->landlord_model->add_payment_amount())
		// 	{
		// 		$this->session->set_userdata('success_message', 'Cheque successfully writted to account');


		// 		redirect('accounting/landlord-payments');
		// 	}

		// 	else
		// 	{
		// 		$this->session->set_userdata('error_message', 'Could not write cheque. Please try again');
		// 	}
		// }
		// else
		// {
		// 	$this->session->set_userdata('error_message', validation_errors());
		// }



		//open the add new order
		$v_data['accounts'] = $this->purchases_model->get_child_accounts("Bank");
    $v_data['creditors'] = $this->purchases_model->get_creditor();
    $v_data['expense_accounts']= $this->purchases_model->get_child_accounts("Income Accounts");

		$where = 'landlord_receipt_id > 0 AND landlord_receipt_deleted < 2';

    $search_purchases = $this->session->userdata('search_purchases');
    if($search_purchases)
    {
      $where .= $search_purchases;
    }
		$table = 'landlord_receipts';

		$segment = 3;
		$this->load->library('pagination');
		$config['base_url'] = site_url().'accounting/landlord-receipts';
		$config['total_rows'] = $this->landlord_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;

		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';

		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';

		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';

		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';

		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';

		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);

		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->landlord_model->get_account_payments_receipt($table, $where, $config["per_page"], $page, $order='landlord_receipts.document_number', $order_method='DESC');
		// var_dump($query); die();

		$data['title'] = 'Landlord Receipts';
		$v_data['title'] = $data['title'];

		$v_data['query_purchases'] = $query;
		$v_data['page'] = $page;

		$data['title'] = $v_data['title']= ' Landlord Receipts';

		$data['content'] = $this->load->view('landlord/landlord_receipts', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}

	public function search_landlord_payments()
	{
		$visit_date_from = $this->input->post('date_from');
		$transaction_number = $this->input->post('transaction_number');
		$visit_date_to = $this->input->post('date_to');

		$search_title = '';

		if(!empty($transaction_number))
		{
			$search_title .= $tenant_name.' ';
			$transaction_number = ' AND landlord_transactions.document_number LIKE \'%'.$transaction_number.'%\'';


		}
		else
		{
			$transaction_number = '';
			$search_title .= '';
		}

     if(!empty($visit_date_from) && !empty($visit_date_to))
     {
       $visit_date = ' AND landlord_transactions.transaction_date BETWEEN \''.$visit_date_from.'\' AND \''.$visit_date_to.'\'';
       $search_title .= 'Payments from '.date('jS M Y', strtotime($visit_date_from)).' to '.date('jS M Y', strtotime($visit_date_to)).' ';
     }

     else if(!empty($visit_date_from))
     {
       $visit_date = ' AND landlord_transactions.transaction_date = \''.$visit_date_from.'\'';
       $search_title .= 'Payments of '.date('jS M Y', strtotime($visit_date_from)).' ';
     }

     else if(!empty($visit_date_to))
     {
       $visit_date = ' AND landlord_transactions.transaction_date = \''.$visit_date_to.'\'';
       $search_title .= 'Payments of '.date('jS M Y', strtotime($visit_date_to)).' ';
     }

     else
     {
       $visit_date = '';
     }


		$search = $visit_date.$transaction_number;

		$this->session->set_userdata('search_landlord_payments', $search);

    redirect('accounting/landlord-payments');
  }
	public function close_landlord_payments_search()
	{
		$this->session->unset_userdata('search_landlord_payments');
		redirect('accounting/landlord-payments');
	}

	public function record_landlord_receipt()
  {
    //form validation
		$this->form_validation->set_rules('account_to_id', 'Expense Account','xss_clean');
		$this->form_validation->set_rules('transacted_amount', 'Amount','required|xss_clean');
    	$this->form_validation->set_rules('transaction_number', 'Reference Number','required|xss_clean');
		$this->form_validation->set_rules('description', 'Description','required|xss_clean');
    	$this->form_validation->set_rules('property_id', 'Property','required|xss_clean');
    	$this->form_validation->set_rules('transaction_type_id', 'Type','required|xss_clean');
    	$this->form_validation->set_rules('bank_id', 'Type','xss_clean');
		$this->form_validation->set_rules('month', 'Type','required|xss_clean');
		$this->form_validation->set_rules('year', 'Type','required|xss_clean');
    // var_dump($_POST);die();
		$transaction_type_id = $this->input->post('transaction_type_id');
		if($transaction_type_id == 4)
		{
			$this->form_validation->set_rules('property_to_id', 'Property To','required|xss_clean');
		}
		if ($this->form_validation->run())
		{
			//update order

			if($this->landlord_model->add_receipt_amount())
			{
				$this->session->set_userdata('success_message', 'Amount has been successfully added');
			}

			else
			{
				$this->session->set_userdata('error_message', 'Could not write cheque. Please try again');
			}
		}
		else
		{
			$this->session->set_userdata('error_message', validation_errors());
		}

    redirect('accounting/landlord-receipts');
  }

  public function print_voucher($landlord_transaction_id)
  {

  	$v_data['contacts'] = $this->site_model->get_contacts();

	$where =  'landlord_transaction_id = '.$landlord_transaction_id;
	$table = 'landlord_transactions';
	$v_data['search_title'] = 'LANDLORD VOUCHER';
	$v_data['query_purchases'] = $this->landlord_model->get_landlord_transactions($where, $table);
	$v_data['title'] = 'LANDLORD VOUCHER';
	$this->load->view('landlord/print_voucher', $v_data);
  }

  public function edit_voucher_payment($landlord_transaction_id)
  {
  	$v_data['landlord_transaction_id'] = $landlord_transaction_id;
	$v_data['accounts'] = $this->purchases_model->get_child_accounts("Bank");
    $v_data['creditors'] = $this->purchases_model->get_creditor();
    $v_data['expense_accounts']= $this->purchases_model->get_child_accounts("Income Accounts");
	$data['title'] = $v_data['title']= 'Edit Landlord Payments';


	$data['content'] = $this->load->view('landlord/edit_payment', $v_data, true);
	$this->load->view('admin/templates/general_page', $data);
  }

  public function update_landlord_transaction($landlord_transaction_id)
  {

  		// var_dump($_POST);die();
  	 	//form validation
		$this->form_validation->set_rules('account_to_id', 'Expense Account','xss_clean');
		$this->form_validation->set_rules('transacted_amount', 'Amount','required|xss_clean');
    	$this->form_validation->set_rules('transaction_number', 'Reference Number','required|xss_clean');
		$this->form_validation->set_rules('description', 'Description','required|xss_clean');
    	$this->form_validation->set_rules('property_id', 'Property','required|xss_clean');
    	$this->form_validation->set_rules('transaction_type_id', 'Type','required|xss_clean');
    	$this->form_validation->set_rules('bank_id', 'Type','xss_clean');
		$this->form_validation->set_rules('month', 'Type','required|xss_clean');
		$this->form_validation->set_rules('year', 'Type','required|xss_clean');
    	// var_dump($_POST);die();
		$transaction_type_id = $this->input->post('transaction_type_id');
		if($transaction_type_id == 4)
		{
			$this->form_validation->set_rules('property_to_id', 'Property To','required|xss_clean');
		}
		if ($this->form_validation->run())
		{
			//update order

			if($this->landlord_model->update_payment_amount($landlord_transaction_id))
			{
				$this->session->set_userdata('success_message', 'Payment has been successfully updated');
				redirect('accounting/landlord-payments');
			}

			else
			{
				$this->session->set_userdata('error_message', 'Could not update payment. Please try again');
				redirect('edit-payment/'.$landlord_transaction_id);
			}
		}
		else
		{
			$this->session->set_userdata('error_message', validation_errors());
			redirect('edit-payment/'.$landlord_transaction_id);
		}

    

  }


  	public function delete_landlord_transaction($landlord_transaction_id)
	{	
		$personnel_id = $this->session->userdata('personnel_id');
		$this->db->where('landlord_transaction_id = '.$landlord_transaction_id);
		$query = $this->db->get('landlord_transactions');
		$item = $query->row();

		$deleted_by = $item->deleted_by;
		$landlord_transaction_deleted = $item->landlord_transaction_deleted;
		// var_dump($item);die();
		if($deleted_by == $personnel_id AND $landlord_transaction_deleted > 0)
		{
			// $this->session->set_userdata('error_message', 'Sorry could not delete the entry, please ask the other person to approve this');
			// var_dump($deleted_by);die();

			if($landlord_transaction_deleted == 0)
			{
				$deleted_status = 0;
			}
			else
			{
				$deleted_status = $landlord_transaction_deleted-1;
			}
			

			if($deleted_status == 1)
			{
				$status = ' reverted deletion';
			}
			else
			{
				$status = ' confirmed revertion';
			}


			$update_array['deleted_by'] = NULL;
			$update_array['date_deleted'] = NULL;
			$update_array['landlord_transaction_deleted'] = $deleted_status;
			$update_array['deleted_remarks'] = NULL;
			$this->db->where('landlord_transaction_id = '.$landlord_transaction_id);
			if($this->db->update('landlord_transactions',$update_array))
			{
				$this->session->set_userdata('success_message', 'You have reversed the transaction status successfully ');
			}
			else
			{
				$this->session->set_userdata('error_message', 'Sorry could not perform the action. Please try again');
			}
		}
		else
		{

			$deleted_status = $landlord_transaction_deleted +1;

			if($deleted_status == 1)
			{
				$status = ' request to delete';
			}
			else
			{
				$status = ' confirmed delete';
			}


			$update_array['deleted_by'] = $personnel_id;
			$update_array['date_deleted'] = date('Y-m-d');
			$update_array['landlord_transaction_deleted'] = $deleted_status;
			$update_array['deleted_remarks'] = $deleted_status;
			$this->db->where('landlord_transaction_id = '.$landlord_transaction_id);
			if($this->db->update('landlord_transactions',$update_array))
			{
				$this->session->set_userdata('success_message', 'You have successfully '.$status);
			}
			else
			{
				$this->session->set_userdata('error_message', 'Sorry could not perform the action. Please try again');
			}

		}
		redirect('accounting/landlord-payments');
	}


	public function delete_landlord_receipt($landlord_receipt_id)
	{	
		$personnel_id = $this->session->userdata('personnel_id');
		$this->db->where('landlord_receipt_id = '.$landlord_receipt_id);
		$query = $this->db->get('landlord_receipts');
		$item = $query->row();

		$deleted_by = $item->deleted_by;
		$landlord_receipt_deleted = $item->landlord_receipt_deleted;
		// var_dump($item);die();
		if($deleted_by == $personnel_id AND $landlord_receipt_deleted > 0)
		{
			// $this->session->set_userdata('error_message', 'Sorry could not delete the entry, please ask the other person to approve this');
			// var_dump($deleted_by);die();

			if($landlord_receipt_deleted == 0)
			{
				$deleted_status = 0;
			}
			else
			{
				$deleted_status = $landlord_receipt_deleted-1;
			}
			

			if($deleted_status == 1)
			{
				$status = ' reverted deletion';
			}
			else
			{
				$status = ' confirmed revertion';
			}


			$update_array['deleted_by'] = NULL;
			$update_array['date_deleted'] = NULL;
			$update_array['landlord_receipt_deleted'] = $deleted_status;
			$update_array['deleted_remarks'] = NULL;
			$this->db->where('(landlord_receipt_id = '.$landlord_receipt_id.' OR parent_transaction ='.$landlord_receipt_id.')');
			if($this->db->update('landlord_receipts',$update_array))
			{
				$this->session->set_userdata('success_message', 'You have reversed the transaction status successfully ');
			}
			else
			{
				$this->session->set_userdata('error_message', 'Sorry could not perform the action. Please try again');
			}
		}
		else
		{

			$deleted_status = $landlord_receipt_deleted +1;

			if($deleted_status == 1)
			{
				$status = ' request to delete';
			}
			else
			{
				$status = ' confirmed delete';
			}


			$update_array['deleted_by'] = $personnel_id;
			$update_array['date_deleted'] = date('Y-m-d');
			$update_array['landlord_receipt_deleted'] = $deleted_status;
			$update_array['deleted_remarks'] = $deleted_status;
			$this->db->where('(landlord_receipt_id = '.$landlord_receipt_id.' OR parent_transaction ='.$landlord_receipt_id.')');
			if($this->db->update('landlord_receipts',$update_array))
			{
				$this->session->set_userdata('success_message', 'You have successfully '.$status);
			}
			else
			{
				$this->session->set_userdata('error_message', 'Sorry could not perform the action. Please try again');
			}

		}
		redirect('accounting/landlord-receipts');
	}

}
?>
