<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once "./application/modules/admin/controllers/admin.php";
// error_reporting(E_ALL);
class Requisition extends admin
{
	var $leases_path;
	var $leases_location;
	var $csv_path;
	function __construct()
	{
		parent:: __construct();

    	$this->load->model('finance/requisition_model');
    	$this->load->model('real_estate_administration/property_model');
    	$this->load->model('real_estate_administration/tenants_model');
    	$this->load->model('real_estate_administration/leases_model');
		$this->load->model('accounts/accounts_model');
		$this->load->model('auth/auth_model');
		$this->load->library('image_lib');

		$this->leases_path = realpath(APPPATH . '../assets/requisition');
		$this->leases_location = base_url().'assets/requisition/';
		$this->csv_path = realpath(APPPATH . '../assets/csv');
		if(!$this->auth_model->check_login())
		{
			redirect('login');
		}
	}

	public function index()
	{
	

		$where = 'requisition_deleted = 0';

	    $search_purchases = $this->session->userdata('search_requisition');
	    if($search_purchases)
	    {
	      $where .= $search_purchases;
	    }

	    // var_dump($where);die();
		$table = 'requisition';


		$segment = 3;
		$this->load->library('pagination');
		$config['base_url'] = site_url().'accounting/requisitions';
		$config['total_rows'] = $this->requisition_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;

		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';

		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';

		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';

		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';

		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';

		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);

		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->requisition_model->get_all_requisition($table, $where, $config["per_page"], $page, $order='requisition.created', $order_method='DESC');
		// var_dump($query); die();

		// $v_data['accounts'] = $this->purchases_model->get_child_accounts("Bank");
	 //    $v_data['creditors'] = $this->purchases_model->get_creditor();

	 //    $v_data['expense_accounts']= $this->purchases_model->get_child_accounts("Expense Accounts");

		$data['title'] = 'Accounts';
		$v_data['title'] = $data['title'];

		$v_data['query'] = $query;
		$v_data['page'] = $page;

		$v_data['location_path'] = $this->leases_location;

		$data['title'] = $v_data['title']= 'Requisition List';

		$data['content'] = $this->load->view('requisition/all_requisition', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}
	

	public function add_requisition()
	{
		$data = array('visit_id'=>1);

		$page = $this->load->view('requisition/sidebar/add_requisition',$data);

		echo $page;
	}

	public function add_new_requision()
	{
		$this->form_validation->set_rules('requisition_type', 'Lease Number', 'required|xss_clean|required');
		$this->form_validation->set_rules('requisition_description', 'Ticket Description', 'required|xss_clean');
		// var_dump($_POST);die();
		//if form has been submitted

		$requisition_type = $this->input->post('requisition_type');
		if($requisition_type <= 1)
		{
			$this->form_validation->set_rules('lease_id', 'Lease', 'required|xss_clean|required');
		}
		else if($requisition_type == 2)
		{
			$this->form_validation->set_rules('landlord_id', 'Lease', 'required|xss_clean|required');
		}
		if ($this->form_validation->run())
		{
			//check if tenant has valid login credentials
			if($this->requisition_model->add_requisition())
			{


				#redirect('accounting/requisitions');
				$response['message'] = 'success';
				$response['reponse'] = 'Sorry could not add the requsition';
			}

			else
			{
				// $this->session->set_userdata('error_message', 'Sorry something went wrong. Please try again');

				$response['message'] = 'fail';
				$response['reponse'] = 'Sorry could not add the requsition';

			}
		}
		else
		{
			$response['message'] = 'fail';
			$response['reponse'] = 'Sorry could not add the requsition';
		}

		echo json_encode($response);

	}

	public function requisition_detail($requisition_id)
	{


		$data['title'] = 'Accounts';
		$v_data['title'] = $data['title'];

	
		$v_data['requisition_id'] = $requisition_id;

		$data['title'] = $v_data['title']= 'Requisition List';



		$data['content'] = $this->load->view('requisition/requisition_detail', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);




	}
	public function display_details($requisition_id,$type,$module=null)
	{
		$data['requisition_id'] = $requisition_id;
		$data['type'] = $type;
		$data['module'] = $module;


		$this->load->view('requisition/all_notes', $data);
	}


	public function notes_written($requisition_id,$type,$module=null)
	{
		$v_data['requisition_id'] = $requisition_id;
		$v_data['type'] = $type;
		$v_data['module'] = $module;

		$v_data['signature_location'] = base_url().'assets/signatures/';
		$v_data['query'] =  $query_data = $this->requisition_model->get_notes($type, $requisition_id);

		if($query_data->num_rows() > 0)
		{
			foreach ($query_data->result() as $key => $value_two) {
				# code...
				$summary = $value_two->notes_name;
			}
			
		}
		else
		{
			$summary = '';
		}
		$v_data['module'] = $module;


		$notes = $this->load->view('finance/requisition/notes_written', $v_data);

		// var_dump($requisition_id);die();
		// $this->load->view('requisition/notes_written', $data);
	}

	
	public function save_soap_notes()
	{

		$notes=$this->input->post('notes');
		$requisition_id=$this->input->post('requisition_id');
		$type=$this->input->post('type');
		$personnel_id = $this->session->userdata('personnel_id');

        $check = $this->requisition_model->check_patient_notes($requisition_id,$type);
        $return['check'] = $check;
        if($check > 0)
        {
        	if($this->requisition_model->update_notes($check, $signature_name = '', $personnel_id))
			{
				// $v_data['signature_location'] = $this->signature_location;
				$v_data['mobile_personnel_id'] = '';
				$v_data['query'] = $this->requisition_model->get_notes($type, $requisition_id);
				$return['result'] = 'success';
				$return['message'] = '';
				$return['requisition_id'] = $requisition_id;
				$return['type'] = $type;
				echo json_encode($return);
			}
			
			else
			{
				$return['result'] = 'false';
				$return['requisition_id'] = $requisition_id;
				$return['type'] = $type;
				echo json_encode($return);
			}
        }
       	else
       	{
       		if($this->requisition_model->add_notes($requisition_id, $type, $signature_name = '', $personnel_id))
			{
				// $v_data['signature_location'] = $this->signature_location;
				$v_data['mobile_personnel_id'] = '';
				$v_data['query'] = $this->requisition_model->get_notes($type, $requisition_id);
				$return['result'] = 'success';
				$return['message'] = '';
				$return['requisition_id'] = $requisition_id;
				$return['type'] = $type;
				echo json_encode($return);
			}			
			else
			{
				$return['result'] = 'false';
				$return['requisition_id'] = $requisition_id;
				$return['type'] = $type;
				echo json_encode($return);
			}
       	}

	}

	public function print_requisition_note($requisition_id)
	{
		$v_data['requisition_id'] = $requisition_id;
		// var_dump($v_data['requisition_id']);die();
	    $v_data['contacts'] = $this->site_model->get_contacts();
	    $v_data['title'] = $data['title'] = 'Requisition Note';
	    $this->load->view('requisition/print_requisition', $v_data);
	}

	public function close_requisition_detail($requisition_id)
	{
		$image_error = '';
		$this->session->unset_userdata('upload_error_message');
		$document_name = 'document_scan';

		// var_dump($_FILES['document_scan']);die();

		//upload image if it has been selected
		$response = $this->leases_model->upload_any_file($this->leases_path, $this->leases_location, $document_name, 'document_scan');
		// var_dump($response); die();
		if($response)
		{
			$document_upload_location = $this->leases_location.$this->session->userdata($document_name);
		}

		//case of upload error
		else
		{
			$image_error = $this->session->userdata('upload_error_message');
			$this->session->unset_userdata('upload_error_message');
		}

		$document = $this->session->userdata($document_name);
// var_dump($document); die();

		if(!empty($document))
		{
			if($this->requisition_model->close_requisition($requisition_id, $document))
			{
				$this->session->set_userdata('success_message', 'You have successfully closed the requisition');
				$this->session->unset_userdata($document_name);
			}

			else
			{
				$this->session->set_userdata('error_message', 'Could not upload document. Please try again');
			}
			
			redirect('accounting/requisitions');

		}
		else
		{
			$redirect = $this->input->post('redirect_url');
			redirect($redirect);

		}
		

	}

	public function search_requisition()
	{
		$visit_date_from = $this->input->post('date_from');
		$requisition_number = $this->input->post('requisition_number');
		$requisition_type = $this->input->post('requisition_type');
		$visit_date_to = $this->input->post('date_to');

		$search_title = '';

		if(!empty($requisition_type))
		{
			$search_title .= 'Property id ';
			$property = ' AND requisition.requisition_type LIKE \'%'.$requisition_type.'%\'';
		}
		else
		{
			$property = '';
			$search_title .= '';
		}

		if(!empty($requisition_number))
		{
			$search_title .= 'Document ';
			$document = ' AND requisition.requisition_number = "'.$requisition_number.'"';
		}
		else
		{
			$document = '';
			$search_title .= '';
		}

	     if(!empty($visit_date_from) && !empty($visit_date_to))
	     {
	       $visit_date = ' AND requisition.created BETWEEN \''.$visit_date_from.'\' AND \''.$visit_date_to.'\'';
	       $search_title .= 'Payments from '.date('jS M Y', strtotime($visit_date_from)).' to '.date('jS M Y', strtotime($visit_date_to)).' ';
	     }

	     else if(!empty($visit_date_from))
	     {
	       $visit_date = ' AND requisition.created = \''.$visit_date_from.'\'';
	       $search_title .= 'Payments of '.date('jS M Y', strtotime($visit_date_from)).' ';
	     }

	     else if(!empty($visit_date_to))
	     {
	       $visit_date = ' AND requisition.created = \''.$visit_date_to.'\'';
	       $search_title .= 'Payments of '.date('jS M Y', strtotime($visit_date_to)).' ';
	     }

	     else
	     {
	       $visit_date = '';
	     }


		$search = $visit_date.$property.$document;


		

		$this->session->set_userdata('search_requisition', $search);

		// $session = $this->session->userdata('search_purchases_items');

		// var_dump($session);die();

	    redirect('accounting/requisitions');
	}

	public function close_requisition_search()
	{
		$this->session->unset_userdata('search_requisition');
	    redirect('accounting/requisitions');

	}

}
?>
