<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once "./application/modules/admin/controllers/admin.php";

class Purchases extends admin
{
	function __construct()
	{
		parent:: __construct();

    	$this->load->model('finance/purchases_model');
    	$this->load->model('real_estate_administration/property_model');
		$this->load->model('accounts/accounts_model');
		$this->load->model('auth/auth_model');
		if(!$this->auth_model->check_login())
		{
			redirect('login');
		}
	}

	public function all_purchases()
	{
		//form validation
		// $this->form_validation->set_rules('account_from_id', 'From','required|xss_clean');
		// $this->form_validation->set_rules('account_to_id', 'Account To','required|xss_clean');
		// $this->form_validation->set_rules('transacted_amount', 'Amount','required|xss_clean');
  //       $this->form_validation->set_rules('reference_number', 'Reference Number','required|xss_clean');
		// $this->form_validation->set_rules('description', 'Description','required|xss_clean');
		// // $this->form_validation->set_rules('account_to_type', 'Account To Type','required|xss_clean');
		// // $this->form_validation->set_rules('payment_date', 'Payment Date','required|xss_clean');

		// if ($this->form_validation->run())
		// {
		// 	//update order
		// 	if($this->purchases_model->add_payment_amount())
		// 	{
		// 		$this->session->set_userdata('success_message', 'Cheque successfully writted to account');


		// 		redirect('accounting/landlord-expenses');
		// 	}

		// 	else
		// 	{
		// 		$this->session->set_userdata('error_message', 'Could not write cheque. Please try again');
		// 	}
		// }
		// else
		// {
		// 	$this->session->set_userdata('error_message', validation_errors());
		// }



		//open the add new order
	

		$where = 'finance_purchase.finance_purchase_id = finance_purchase_payment.finance_purchase_id AND finance_purchase_payment.type = 1 AND finance_purchase.finance_purchase_deleted < 2';

	    $search_purchases = $this->session->userdata('search_purchases_items');
	    if($search_purchases)
	    {
	      $where .= $search_purchases;
	    }

	    // var_dump($where);die();
		$table = 'finance_purchase,finance_purchase_payment';


		$segment = 3;
		$this->load->library('pagination');
		$config['base_url'] = site_url().'accounting/landlord-expenses';
		$config['total_rows'] = $this->purchases_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;

		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';

		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';

		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';

		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';

		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';

		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);

		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->purchases_model->get_account_payments_transactions($table, $where, $config["per_page"], $page, $order='finance_purchase.created', $order_method='DESC');
		// var_dump($query); die();

		$v_data['accounts'] = $this->purchases_model->get_child_accounts("Bank");
	    $v_data['creditors'] = $this->purchases_model->get_creditor();

	    $v_data['expense_accounts']= $this->purchases_model->get_child_accounts("Expense Accounts");

		$data['title'] = 'Accounts';
		$v_data['title'] = $data['title'];

		$v_data['query_purchases'] = $query;
		$v_data['page'] = $page;

		$data['title'] = $v_data['title']= 'Landlord Expenses';

		$data['content'] = $this->load->view('purchases/purchases', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}

	public function edit_voucher_expense($finance_purchase_payment_id)
	{


		$v_data['finance_purchase_payment_id'] = $finance_purchase_payment_id;
		$v_data['accounts'] = $this->purchases_model->get_child_accounts("Bank");
    	$v_data['creditors'] = $this->purchases_model->get_creditor();

    	$v_data['expense_accounts']= $this->purchases_model->get_child_accounts("Expense Accounts");
		$data['title'] = $v_data['title']= 'Edit Finance Purchase';


		$data['content'] = $this->load->view('purchases/edit_purchases', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}

	public function record_landlord_expenses()
	{
		//form validation
		$this->form_validation->set_rules('department_id', 'Department','xss_clean');
		$this->form_validation->set_rules('account_to_id', 'Expense Account','required|xss_clean');
		$this->form_validation->set_rules('account_from_id', 'Paying Account','required|xss_clean');
		$this->form_validation->set_rules('transacted_amount', 'Amount','required|xss_clean');
		$this->form_validation->set_rules('transaction_number', 'Reference Number','required|xss_clean');
		$this->form_validation->set_rules('description', 'Description','required|xss_clean');
		// $this->form_validation->set_rules('account_to_type', 'Account To Type','required|xss_clean');
		// $this->form_validation->set_rules('payment_date', 'Payment Date','required|xss_clean');
		// var_dump($_POST);die();
		if ($this->form_validation->run())
		{
			//update order
			// check balance

			// $account_balance = $this->purchases_model->get_account_balance('Petty Cash');
			// $transacted_amount = $this->input->post('transacted_amount');

			// if($transacted_amount > $account_balance)
			// {
			// 	// var_dump($account_balance);die();
			// 	$this->session->set_userdata('error_message', 'Sorry the amount you want to transact is more than what you have in your account.');
			// }
			// else {
				if($this->purchases_model->add_payment_amount())
				{
					$this->session->set_userdata('success_message', 'Expense has been successfully added');
				}

				else
				{
					$this->session->set_userdata('error_message', 'Could not write cheque. Please try again');
				}
			// }


		}
		else
		{
			$this->session->set_userdata('error_message', validation_errors());
		}

		redirect('accounting/landlord-expenses');
	}


	public function update_landlord_expenses($finance_purchase_id,$finance_purchase_payment_id)
	{
		//form validation
		$this->form_validation->set_rules('department_id', 'Department','xss_clean');
		$this->form_validation->set_rules('account_to_id', 'Expense Account','required|xss_clean');
		$this->form_validation->set_rules('account_from_id', 'Paying Account','required|xss_clean');
		$this->form_validation->set_rules('transacted_amount', 'Amount','required|xss_clean');
		$this->form_validation->set_rules('transaction_number', 'Reference Number','required|xss_clean');
		$this->form_validation->set_rules('description', 'Description','required|xss_clean');

		// var_dump($_POST);die();
		
		if ($this->form_validation->run())
		{
			
				if($this->purchases_model->update_payment_amount($finance_purchase_id,$finance_purchase_payment_id))
				{
					$this->session->set_userdata('success_message', 'Expense has been successfully updated');
					redirect('accounting/landlord-expenses');
				}

				else
				{
					$this->session->set_userdata('error_message', 'Could not update expense. Please try again');
					redirect('edit-voucher/'.$finance_purchase_payment_id);
				}
			


		}
		else
		{
			$this->session->set_userdata('error_message', validation_errors());
			redirect('edit-voucher/'.$finance_purchase_payment_id);
		}

		
	}
  public function purchase_payments()
  {
    //form validation
		// $this->form_validation->set_rules('account_from_id', 'From','required|xss_clean');
		$this->form_validation->set_rules('account_to_id', 'Account To','required|xss_clean');
		$this->form_validation->set_rules('transacted_amount', 'Amount','required|xss_clean');
    $this->form_validation->set_rules('reference_number', 'Reference Number','required|xss_clean');
		$this->form_validation->set_rules('description', 'Description','required|xss_clean');
		// $this->form_validation->set_rules('account_to_type', 'Account To Type','required|xss_clean');
		// $this->form_validation->set_rules('payment_date', 'Payment Date','required|xss_clean');

		if ($this->form_validation->run())
		{
			//update order
			if($this->purchases_model->add_payment_amount())
			{
				$this->session->set_userdata('success_message', 'Cheque successfully writted to account');


				redirect('accounting/landlord-expenses');
			}

			else
			{
				$this->session->set_userdata('error_message', 'Could not write cheque. Please try again');
			}
		}
		else
		{
			$this->session->set_userdata('error_message', validation_errors());
		}



		//open the add new order
		$v_data['accounts'] = $this->purchases_model->get_child_accounts("Bank");
    $v_data['purchase_items'] = $this->purchases_model->get_all_purchase_invoices();

		$where = 'account_payment_deleted = 0 ';
		$table = 'account_payments';


		$segment = 3;
		$this->load->library('pagination');
		$config['base_url'] = site_url().'accounting/write-cheque';
		$config['total_rows'] = $this->purchases_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;

		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';

		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';

		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';

		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';

		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';

		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);

		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->purchases_model->get_account_payments_transactions($table, $where, $config["per_page"], $page, $order='account_payments.payment_date', $order_method='ASC');
		// var_dump($query); die();

		$data['title'] = 'Accounts';
		$v_data['title'] = $data['title'];

		$v_data['query'] = $query;
		$v_data['page'] = $page;

		$data['title'] = $v_data['title']= 'Purchase Payment';

		$data['content'] = $this->load->view('purchases/purchase_payments', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
  }
  public function record_petty_cash_old()
  {
    //form validation
		// $this->form_validation->set_rules('account_from_id', 'From','required|xss_clean');
		$this->form_validation->set_rules('account_to_id', 'Expense Account','required|xss_clean');
		$this->form_validation->set_rules('transacted_amount', 'Amount','required|xss_clean');
    $this->form_validation->set_rules('transaction_number', 'Reference Number','required|xss_clean');
		$this->form_validation->set_rules('description', 'Description','required|xss_clean');
		// $this->form_validation->set_rules('account_to_type', 'Account To Type','required|xss_clean');
		// $this->form_validation->set_rules('payment_date', 'Payment Date','required|xss_clean');
    // var_dump($_POST);die();
		if ($this->form_validation->run())
		{
			//update order
			if($this->purchases_model->add_payment_amount())
			{
				$this->session->set_userdata('success_message', 'Expense has been successfully added');



			}

			else
			{
				$this->session->set_userdata('error_message', 'Could not write cheque. Please try again');
			}
		}
		else
		{
			$this->session->set_userdata('error_message', validation_errors());
		}

    redirect('accounting/landlord-expenses');
  }


  public function make_payment($finance_purchase_id)
  {
    //form validation
    // $this->form_validation->set_rules('account_from_id', 'From','required|xss_clean');
    $this->form_validation->set_rules('account_from_id', 'Expense Account','required|xss_clean');
    $this->form_validation->set_rules('amount_paid', 'Amount','required|xss_clean');
    $this->form_validation->set_rules('reference_number', 'Reference Number','required|xss_clean');
    $this->form_validation->set_rules('payment_date', 'Description','required|xss_clean');
    // $this->form_validation->set_rules('account_to_type', 'Account To Type','required|xss_clean');
    // $this->form_validation->set_rules('payment_date', 'Payment Date','required|xss_clean');
    // var_dump($_POST);die();
    if ($this->form_validation->run())
    {
      //update order
      $balance = $this->input->post('balance');
      $amount_paid = $this->input->post('amount_paid');
      if($amount_paid > $balance)
      {
        $this->session->set_userdata('error_message', 'Sorry you are not allowed to make an overpayment to this account ');
      }
      else {
        if($this->purchases_model->payaninvoice($finance_purchase_id))
        {
          $this->session->set_userdata('success_message', 'You have successfully added the payment');
        }

        else
        {
          $this->session->set_userdata('error_message', 'Could not write cheque. Please try again');
        }
      }

    }
    else
    {
      $this->session->set_userdata('error_message', validation_errors());
    }

    redirect('accounting/landlord-expenses');
  }

  public function search_purchases()
  {
    $visit_date_from = $this->input->post('date_from');
		$document_number = $this->input->post('document_number');
		$property_id = $this->input->post('property_id');
		$visit_date_to = $this->input->post('date_to');

		$search_title = '';

		if(!empty($property_id))
		{
			$search_title .= 'Property id ';
			$property = ' AND finance_purchase.property_id LIKE \'%'.$property_id.'%\'';
		}
		else
		{
			$property = '';
			$search_title .= '';
		}

	if(!empty($document_number))
	{
		$search_title .= 'Document ';
		$document = ' AND finance_purchase.document_number = "'.$document_number.'"';
	}
	else
	{
		$document = '';
		$search_title .= '';
	}

     if(!empty($visit_date_from) && !empty($visit_date_to))
     {
       $visit_date = ' AND finance_purchase.transaction_date BETWEEN \''.$visit_date_from.'\' AND \''.$visit_date_to.'\'';
       $search_title .= 'Payments from '.date('jS M Y', strtotime($visit_date_from)).' to '.date('jS M Y', strtotime($visit_date_to)).' ';
     }

     else if(!empty($visit_date_from))
     {
       $visit_date = ' AND finance_purchase.transaction_date = \''.$visit_date_from.'\'';
       $search_title .= 'Payments of '.date('jS M Y', strtotime($visit_date_from)).' ';
     }

     else if(!empty($visit_date_to))
     {
       $visit_date = ' AND finance_purchase.transaction_date = \''.$visit_date_to.'\'';
       $search_title .= 'Payments of '.date('jS M Y', strtotime($visit_date_to)).' ';
     }

     else
     {
       $visit_date = '';
     }


	$search = $visit_date.$property.$document;


	

	$this->session->set_userdata('search_purchases_items', $search);

	// $session = $this->session->userdata('search_purchases_items');

	// var_dump($session);die();

    redirect('accounting/landlord-expenses');
  }
	public function close_search()
	{
		$this->session->unset_userdata('search_purchases_items');
		redirect('accounting/landlord-expenses');
	}


	public function petty_cash()
	{
		// $this->form_validation->set_rules('account_to_id', 'Account To','required|xss_clean');
		// $this->form_validation->set_rules('transacted_amount', 'Amount','required|xss_clean');
		// $this->form_validation->set_rules('reference_number', 'Reference Number','required|xss_clean');
		// $this->form_validation->set_rules('description', 'Description','required|xss_clean');

		// if ($this->form_validation->run())
		// {
		// 	//update order
		// 	if($this->purchases_model->add_payment_amount())
		// 	{
		// 		$this->session->set_userdata('success_message', 'Cheque successfully writted to account');


		// 		redirect('accounting/landlord-expenses');
		// 	}

		// 	else
		// 	{
		// 		$this->session->set_userdata('error_message', 'Could not write cheque. Please try again');
		// 	}
		// }
		// else
		// {
		// 	$this->session->set_userdata('error_message', validation_errors());
		// }



		//open the add new order
		$v_data['accounts'] = $this->purchases_model->get_child_accounts("Bank");
		$v_data['creditors'] = $this->purchases_model->get_creditor();

		$v_data['expense_accounts']= $this->purchases_model->get_child_accounts("Expense Accounts");
		$account_from_id = $this->purchases_model->get_account_id('Petty Cash');
		$where =  '((v_account_ledger_by_date.transactionClassification = "Purchase Payment"
									OR v_account_ledger_by_date.transactionCategory = "Transfer"
									OR v_account_ledger_by_date.transactionCategory = "Income"
									OR v_account_ledger_by_date.transactionCategory = "Landlord Payment")
									 AND v_account_ledger_by_date.accountName = "Petty Cash" ) ';

		$search_purchases = $this->session->userdata('search_petty_cash');
		if($search_purchases)
		{
			$where .= $search_purchases;
			$search_title = $this->session->userdata('search_petty_cash_title');
		}
		else {

			$add7days = date('Y-m-d');
			$where .= ' AND v_account_ledger_by_date.transactionDate BETWEEN \''.$add7days.'\' AND \''.date('Y-m-d').'\'';
			$transaction_date = date('jS M Y');

			$todays_date = date('jS M Y',strtotime(date('Y-m-d')));
			$search_title = 'Transactions for period '.$transaction_date.' to  '.$todays_date.' ';
		}
		$table = 'v_account_ledger_by_date';

		// var_dump($search_title);die();
		$v_data['search_title'] = $search_title;
		$v_data['query_purchases'] = $this->purchases_model->get_petty_cash($where, $table);
		// var_dump($table); die();
			$v_data['departments'] = $this->purchases_model->get_all_departments();
		$v_data['account_from_id'] = $this->purchases_model->get_account_id('Petty Cash');
		$data['title'] = 'Accounts';
		$v_data['title'] = $data['title'];

		// $v_data['query_purchases'] = $query;
			$v_data['search_title'] = $search_title;
		// $v_data['page'] = $page;

		$data['title'] = $v_data['title']= 'Petty Cash';

		$data['content'] = $this->load->view('purchases/petty_cash', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}

	public function tenant_expenses()
	{
		// $this->form_validation->set_rules('account_to_id', 'Account To','required|xss_clean');
		// $this->form_validation->set_rules('transacted_amount', 'Amount','required|xss_clean');
		// $this->form_validation->set_rules('reference_number', 'Reference Number','required|xss_clean');
		// $this->form_validation->set_rules('description', 'Description','required|xss_clean');

		// if ($this->form_validation->run())
		// {
		// 	//update order
		// 	if($this->purchases_model->add_payment_amount())
		// 	{
		// 		$this->session->set_userdata('success_message', 'Cheque successfully writted to account');


		// 		redirect('accounting/landlord-expenses');
		// 	}

		// 	else
		// 	{
		// 		$this->session->set_userdata('error_message', 'Could not write cheque. Please try again');
		// 	}
		// }
		// else
		// {
		// 	$this->session->set_userdata('error_message', validation_errors());
		// }



		//open the add new order
		$v_data['accounts'] = $this->purchases_model->get_child_accounts("Bank");
		$v_data['creditors'] = $this->purchases_model->get_creditor();

		$v_data['expense_accounts']= $this->purchases_model->get_child_accounts("Expense Accounts");
		$account_from_id = $this->purchases_model->get_account_id('Petty Cash');
		$where =  '((v_account_ledger_by_date.transactionClassification = "Purchase Payment"
									OR v_account_ledger_by_date.transactionCategory = "Transfer"
									OR v_account_ledger_by_date.transactionCategory = "Income"
									OR v_account_ledger_by_date.transactionCategory = "Landlord Payment")
									 AND v_account_ledger_by_date.accountName = "Petty Cash" ) ';

		$search_purchases = $this->session->userdata('search_petty_cash');
		if($search_purchases)
		{
			$where .= $search_purchases;
			$search_title = $this->session->userdata('search_petty_cash_title');
		}
		else {

			$add7days = date('Y-m-d');
			$where .= ' AND v_account_ledger_by_date.transactionDate BETWEEN \''.$add7days.'\' AND \''.date('Y-m-d').'\'';
			$transaction_date = date('jS M Y');

			$todays_date = date('jS M Y',strtotime(date('Y-m-d')));
			$search_title = 'Transactions for period '.$transaction_date.' to  '.$todays_date.' ';
		}
		$table = 'v_account_ledger_by_date';

		// var_dump($search_title);die();
		$v_data['search_title'] = $search_title;
		$v_data['query_purchases'] = $this->purchases_model->get_petty_cash($where, $table);
		// var_dump($table); die();
			$v_data['departments'] = $this->purchases_model->get_all_departments();
		$v_data['account_from_id'] = $this->purchases_model->get_account_id('Petty Cash');
		$data['title'] = 'Accounts';
		$v_data['title'] = $data['title'];

		// $v_data['query_purchases'] = $query;
			$v_data['search_title'] = $search_title;
		// $v_data['page'] = $page;

		$data['title'] = $v_data['title']= 'Petty Cash';

		$data['content'] = $this->load->view('purchases/tenant_expenses', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}


	public function record_petty_cash()
	{
		//form validation
		$this->form_validation->set_rules('department_id', 'Department','xss_clean');
		$this->form_validation->set_rules('account_to_id', 'Expense Account','required|xss_clean');
		$this->form_validation->set_rules('account_from_id', 'Paying Account','required|xss_clean');
		$this->form_validation->set_rules('transacted_amount', 'Amount','required|xss_clean');
		$this->form_validation->set_rules('transaction_number', 'Reference Number','required|xss_clean');
		$this->form_validation->set_rules('description', 'Description','required|xss_clean');
		// $this->form_validation->set_rules('account_to_type', 'Account To Type','required|xss_clean');
		// $this->form_validation->set_rules('payment_date', 'Payment Date','required|xss_clean');
		// var_dump($_POST);die();
		if ($this->form_validation->run())
		{
			//update order
			// check balance

			// $account_balance = $this->purchases_model->get_account_balance('Petty Cash');
			// $transacted_amount = $this->input->post('transacted_amount');

			// if($transacted_amount > $account_balance)
			// {
			// 	// var_dump($account_balance);die();
			// 	$this->session->set_userdata('error_message', 'Sorry the amount you want to transact is more than what you have in your account.');
			// }
			// else {
				if($this->purchases_model->record_petty_cash_transaction())
				{
					$this->session->set_userdata('success_message', 'Expense has been successfully added');
				}

				else
				{
					$this->session->set_userdata('error_message', 'Could not write cheque. Please try again');
				}
			// }


		}
		else
		{
			$this->session->set_userdata('error_message', validation_errors());
		}

		redirect('accounting/petty-cash');
	}

	public function search_petty_cash()
	{
		$visit_date_from = $this->input->post('date_from');
		// $transaction_number = $this->input->post('transaction_number');
		$visit_date_to = $this->input->post('date_to');

		$search_title = '';



		 if(!empty($visit_date_from) && !empty($visit_date_to))
		 {
			 $visit_date = ' AND v_account_ledger_by_date.transactionDate BETWEEN \''.$visit_date_from.'\' AND \''.$visit_date_to.'\'';
			 $search_title .= 'Transaction from '.date('jS M Y', strtotime($visit_date_from)).' to '.date('jS M Y', strtotime($visit_date_to)).' ';
		 }

		 else if(!empty($visit_date_from))
		 {
			 // $visit_date_from = $visit_date_from;
			 $visit_date = ' AND v_account_ledger_by_date.transactionDate = \''.$visit_date_from.'\'';
			 $search_title .= 'Transactions of '.date('jS M Y', strtotime($visit_date_from)).' ';
		 }

		 else if(!empty($visit_date_to))
		 {
			 $visit_date = ' AND v_account_ledger_by_date.transactionDate = \''.$visit_date_to.'\'';
			 $search_title .= 'Transactions of '.date('jS M Y', strtotime($visit_date_to)).' ';
		 }

		 else
		 {
			 $visit_date = '';
		 }


		$search = $visit_date;

		$this->session->set_userdata('search_petty_cash', $search);
		$this->session->set_userdata('petty_cash_visit_date_from', $visit_date_from);

		redirect('accounting/petty-cash');
	}

	public function close_petty_cash_search()
	{
		$this->session->unset_userdata('search_petty_cash');
		$this->session->unset_userdata('petty_cash_visit_date_from');



		redirect('accounting/petty-cash');
	}

	public function print_petty_cash()
	{
		// var_dump($account); die();
		$v_data['contacts'] = $this->site_model->get_contacts();


		$where =  '((v_account_ledger_by_date.transactionClassification = "Purchase Payment"
									OR v_account_ledger_by_date.transactionCategory = "Transfer"
									OR v_account_ledger_by_date.transactionCategory = "Income"
									OR v_account_ledger_by_date.transactionCategory = "Landlord Payment")
									 AND v_account_ledger_by_date.accountName = "Petty Cash" ) ';


		$search_purchases = $this->session->userdata('search_petty_cash');
		if($search_purchases)
		{
			$where .= $search_purchases;
			$search_title = $this->session->userdata('search_petty_cash_title');
		}
		else {

			$add7days = date('Y-m-d');
			$where .= ' AND v_account_ledger_by_date.transactionDate BETWEEN \''.$add7days.'\' AND \''.date('Y-m-d').'\'';
			$transaction_date = date('jS M Y');

			$todays_date = date('jS M Y',strtotime(date('Y-m-d')));
			$search_title = 'Transactions for period '.$transaction_date.' to  '.$todays_date.' ';
		}
		$table = 'v_account_ledger_by_date';

		// var_dump($search_title);die();
		$v_data['search_title'] = $search_title;
		$v_data['query_purchases'] = $this->purchases_model->get_petty_cash($where, $table);
		$v_data['title'] = $search_title;
		$this->load->view('purchases/print_petty_cash', $v_data);
	}
	public function print_voucher($finance_purchase_payment_id)
	{
			$v_data['contacts'] = $this->site_model->get_contacts();

			$where =  'finance_purchase_payment.finance_purchase_id = finance_purchase.finance_purchase_id AND  finance_purchase_payment.finance_purchase_payment_id = '.$finance_purchase_payment_id;
			$table = 'finance_purchase_payment,finance_purchase';
			$v_data['search_title'] = 'EXPENSE VOUCHER';
			$v_data['query_purchases'] = $this->purchases_model->get_purchase_details($where, $table);
			$v_data['title'] = 'EXPENSE VOUCHER';
			$this->load->view('purchases/print_voucher', $v_data);
	}


	public function delete_purchases_item($finance_purchase_id)
	{	
		$personnel_id = $this->session->userdata('personnel_id');
		$this->db->where('finance_purchase_id = '.$finance_purchase_id);
		$query = $this->db->get('finance_purchase');
		$item = $query->row();

		$deleted_by = $item->deleted_by;
		$finance_purchase_deleted = $item->finance_purchase_deleted;

		if($deleted_by == $personnel_id AND $finance_purchase_deleted > 0)
		{
			
				if($finance_purchase_deleted == 0)
				{
					$deleted_status = 0;
				}
				else
				{
					$deleted_status = $finance_purchase_deleted-1;
				}			

				if($deleted_status == 1)
				{
					$status = ' reverted deletion';
				}
				else
				{
					$status = ' confirmed revertion';
				}


				$update_array['deleted_by'] = NULL;
				$update_array['date_deleted'] = NULL;
				$update_array['finance_purchase_deleted'] = $deleted_status;
				$update_array['remarks'] = NULL;
				$this->db->where('finance_purchase_id = '.$finance_purchase_id);
				if($this->db->update('finance_purchase',$update_array))
				{
					$this->session->set_userdata('success_message', 'You have reversed the transaction status successfully ');
				}
				else
				{
					$this->session->set_userdata('error_message', 'Sorry could not perform the action. Please try again');
				}
		}
		else
		{

			$deleted_status = $finance_purchase_deleted +1;

			if($deleted_status == 1)
			{
				$status = ' request to delete';
			}
			else
			{
				$status = ' confirmed delete';
			}
			$update_array['deleted_by'] = $personnel_id;
			$update_array['date_deleted'] = date('Y-m-d');
			$update_array['finance_purchase_deleted'] = $deleted_status;
			$this->db->where('finance_purchase_id = '.$finance_purchase_id);
			if($this->db->update('finance_purchase',$update_array))
			{
				$this->session->set_userdata('success_message', 'You have successfully '.$status);
			}
			else
			{
				$this->session->set_userdata('error_message', 'Sorry could not perform the action. Please try again');
			}

		}
		redirect('accounting/landlord-expenses');
	}

	public function delete_petty_cash_expense($finance_purchase_payment_id)
	{
  		$personnel_id = $this->session->userdata('personnel_id');


  		$this->db->where('finance_purchase_payment_id = '.$finance_purchase_payment_id);
		$query_payment = $this->db->get('finance_purchase_payment');
		$item_payment = $query_payment->row();

		$finance_purchase_id = $item_payment->finance_purchase_id;


		if($finance_purchase_id > 0)
		{
			$this->db->where('finance_purchase_id = '.$finance_purchase_id);
			$query = $this->db->get('finance_purchase');
			$item = $query->row();

			$deleted_by = $item->deleted_by;
			$finance_purchase_deleted = $item->finance_purchase_deleted;
			// var_dump($item);die();
			if($deleted_by == $personnel_id AND $finance_purchase_deleted > 0)
			{
				if($finance_purchase_deleted == 0)
				{
					$deleted_status = 0;
				}
				else
				{
					$deleted_status = $finance_purchase_deleted-1;
				}			

				if($deleted_status == 1)
				{
					$status = ' reverted deletion';
				}
				else
				{
					$status = ' confirmed revertion';
				}


				$update_array['deleted_by'] = NULL;
				$update_array['date_deleted'] = NULL;
				$update_array['finance_purchase_deleted'] = $deleted_status;
				$update_array['remarks'] = NULL;
				$this->db->where('finance_purchase_id = '.$finance_purchase_id);
				if($this->db->update('finance_purchase',$update_array))
				{
					$this->session->set_userdata('success_message', 'You have reversed the transaction status successfully ');
				}
				else
				{
					$this->session->set_userdata('error_message', 'Sorry could not perform the action. Please try again');
				}
			}
			else
			{

				$deleted_status = $finance_purchase_deleted +1;

				if($deleted_status == 1)
				{
					$status = ' request to delete';
				}
				else
				{
					$status = ' confirmed delete';
				}


				$update_array['deleted_by'] = $personnel_id;
				$update_array['date_deleted'] = date('Y-m-d');
				$update_array['finance_purchase_deleted'] = $deleted_status;
				$update_array['remarks'] = $deleted_status;
				$this->db->where('finance_purchase_id = '.$finance_purchase_id);
				if($this->db->update('finance_purchase',$update_array))
				{
					$this->session->set_userdata('success_message', 'You have successfully '.$status);
				}
				else
				{
					$this->session->set_userdata('error_message', 'Sorry could not perform the action. Please try again');
				}

			}

		}
		else
		{
			$this->session->set_userdata('error_message', 'Sorry could not perform the action. Please try again');
		}

		
		redirect('accounting/petty-cash');
  	
	}
}
?>
