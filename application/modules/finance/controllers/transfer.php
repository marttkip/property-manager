<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once "./application/modules/admin/controllers/admin.php";

class Transfer extends admin
{
	function __construct()
	{
		parent:: __construct();

	    $this->load->model('finance/purchases_model');
	    $this->load->model('finance/transfer_model');
	    $this->load->model('real_estate_administration/property_model');
	    
    	if(!$this->auth_model->check_login())
		{
			redirect('login');
		}
	}



  public function write_cheque()
	{
		//form validation
		$this->form_validation->set_rules('account_from_id', 'From','required|xss_clean');
		$this->form_validation->set_rules('account_to_id', 'Account To','required|xss_clean');
		$this->form_validation->set_rules('amount', 'Amount','required|xss_clean');
		$this->form_validation->set_rules('description', 'Description','required|xss_clean');
		$this->form_validation->set_rules('reference_number', 'Reference Number','required|xss_clean');
		$this->form_validation->set_rules('transfer_date', 'Transfer Date','required|xss_clean');

		if ($this->form_validation->run())
		{
			//update order
			if($this->transfer_model->transfer_funds())
			{
				$this->session->set_userdata('success_message', 'Cheque successfully writted to account');


				redirect('accounting/accounts-transfer');
			}

			else
			{
				$this->session->set_userdata('error_message', 'Could not write cheque. Please try again');
			}
		}
		else
		{
			$this->session->set_userdata('error_message', validation_errors());
		}



		//open the add new order
		$v_data['accounts'] = $this->purchases_model->get_child_accounts("Bank");

		$where = 'finance_transfer_status = 1 AND finance_transfer_deleted < 2';


	    $search_transfers = $this->session->userdata('search_transfers');
	    if(!empty($search_transfers))
	    {
	      $where .= $search_transfers;
	    }

		$table = 'finance_transfer';


		$segment = 3;
		$this->load->library('pagination');
		$config['base_url'] = site_url().'accounting/accounts-transfer';
		$config['total_rows'] = $this->transfer_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;

		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';

		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';

		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';

		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';

		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';

		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);

		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->transfer_model->get_account_transfer_transactions($table, $where, $config["per_page"], $page, $order='finance_transfer.transaction_date', $order_method='DESC');
		// var_dump($query); die();

		$data['title'] = 'Accounts';
		$v_data['title'] = $data['title'];

		$v_data['query'] = $query;
		$v_data['page'] = $page;

		$data['title'] = $v_data['title']= 'Transfer Cheque';

		$data['content'] = $this->load->view('finance/transfer/write_cheques', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}

  public function get_list_type($type)
  {
        $query = $this->purchases_model->get_child_accounts("Bank",$type);
        echo '<option value="0">--Select an option --</option>';
        $options = $query;
        foreach($options-> result() AS $key_old) {
            if($key_old->account_id != $type)
            {
                echo '<option value="'.$key_old->account_id.'">'.$key_old->account_name.'</option>';
            }

        }
    }
    public function search_transfers()
    {
      $visit_date_from = $this->input->post('date_from');
      $reference_number = $this->input->post('transaction_number');
      $visit_date_to = $this->input->post('date_to');

      $search_title = '';

      if(!empty($reference_number))
      {
        $search_title .= $tenant_name.' ';
        $transaction_number = ' AND finance_transfer.reference_number LIKE \'%'.$reference_number.'%\'';


      }
      else
      {
        $transaction_number = '';
        $search_title .= '';
      }

       if(!empty($visit_date_from) && !empty($visit_date_to))
       {
         $visit_date = ' AND finance_transfer.transaction_date BETWEEN \''.$visit_date_from.'\' AND \''.$visit_date_to.'\'';
         $search_title .= 'Payments from '.date('jS M Y', strtotime($visit_date_from)).' to '.date('jS M Y', strtotime($visit_date_to)).' ';
       }

       else if(!empty($visit_date_from))
       {
         $visit_date = ' AND finance_transfer.transaction_date = \''.$visit_date_from.'\'';
         $search_title .= 'Payments of '.date('jS M Y', strtotime($visit_date_from)).' ';
       }

       else if(!empty($visit_date_to))
       {
         $visit_date = ' AND finance_transfer.transaction_date = \''.$visit_date_to.'\'';
         $search_title .= 'Payments of '.date('jS M Y', strtotime($visit_date_to)).' ';
       }

       else
       {
         $visit_date = '';
       }


      $search = $visit_date.$transaction_number;

      $this->session->set_userdata('search_transfers', $search);

      redirect('accounting/accounts-transfer');
    }

    public function close_search()
  	{
  		$this->session->unset_userdata('search_transfers');
  		redirect('accounting/accounts-transfer');
  	}
  	public function delete_transfer_payment($finance_transfer_id)
  	{
  		$personnel_id = $this->session->userdata('personnel_id');
		$this->db->where('finance_transfer_id = '.$finance_transfer_id);
		$query = $this->db->get('finance_transfer');
		$item = $query->row();

		$deleted_by = $item->deleted_by;
		$finance_transfer_deleted = $item->finance_transfer_deleted;
		// var_dump($item);die();
		if($deleted_by == $personnel_id AND $finance_transfer_deleted > 0)
		{
			if($finance_transfer_deleted == 0)
			{
				$deleted_status = 0;
			}
			else
			{
				$deleted_status = $finance_transfer_deleted-1;
			}			

			if($deleted_status == 1)
			{
				$status = ' reverted deletion';
			}
			else
			{
				$status = ' confirmed revertion';
			}


			$update_array['deleted_by'] = NULL;
			$update_array['date_deleted'] = NULL;
			$update_array['finance_transfer_deleted'] = $deleted_status;
			$update_array['deleted_remarks'] = NULL;
			$this->db->where('finance_transfer_id = '.$finance_transfer_id);
			if($this->db->update('finance_transfer',$update_array))
			{
				$this->session->set_userdata('success_message', 'You have reversed the transaction status successfully ');
			}
			else
			{
				$this->session->set_userdata('error_message', 'Sorry could not perform the action. Please try again');
			}
		}
		else
		{

			$deleted_status = $finance_transfer_deleted +1;

			if($deleted_status == 1)
			{
				$status = ' request to delete';
			}
			else
			{
				$status = ' confirmed delete';
			}


			$update_array['deleted_by'] = $personnel_id;
			$update_array['date_deleted'] = date('Y-m-d');
			$update_array['finance_transfer_deleted'] = $deleted_status;
			$update_array['deleted_remarks'] = $deleted_status;
			$this->db->where('finance_transfer_id = '.$finance_transfer_id);
			if($this->db->update('finance_transfer',$update_array))
			{
				$this->session->set_userdata('success_message', 'You have successfully '.$status);
			}
			else
			{
				$this->session->set_userdata('error_message', 'Sorry could not perform the action. Please try again');
			}

		}
		redirect('accounting/accounts-transfer');
  	}
}
?>
