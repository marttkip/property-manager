<?php
    
    $result = '';
    // var_dump($query);die();
    //if users exist display them
    if ($query->num_rows() > 0)
    {
      $count = $page;
      
      $result .= 
      '
      <table class="table table-bordered table-striped table-condensed">
        <thead>
          <tr>
            <th>#</th>
            <th>Requisition Number</th>
            <th>Requisition type</th>
            <th>Tenant</th>
            <th>Landlord</th>
            <th>Requisition description</th>
            <th>Created by</th>
            <th>created date Date</th>

            <th>Status</th>
             <th>Document</th>
            <th colspan="3">Actions</th>
          </tr>
        </thead>
          <tbody>
          
      ';
      
     
      
      foreach ($query->result() as $row)
      {
        $requisition_id = $row->requisition_id;
        $requisition_type = $row->requisition_type;
        $requisition_description = $row->requisition_description;
        // $created_by = $row->created_by;
        $requisition_status = $row->requisition_status;
        $created = $row->date_added;
        $requisition_number = $row->requisition_number;
        $property_owner_name = $row->property_owner_name;
        $tenant_name = $row->tenant_name;
        $personnel_fname = $row->personnel_fname;
        $personnel_onames = $row->personnel_onames;
        $document = $row->document;

        $created_by = $personnel_fname.' '.$personnel_onames;
        
        //status
        if($requisition_status == 1)
        {
          $status = 'Active';
        }
        else
        {
          $status = 'Closed';
        }
        //status
       
        
        //create deactivated status display
        // if($requisition_status == 0)
        // {
        //   $status = '<span class="label label-default">Deactivated</span>';
        //   $button = '<a class="btn btn-info" href="'.site_url().'customers/activate-customer/'.$requisition_id.'" onclick="return confirm(\'Do you want to activate '.$requisition_number.'?\');" title="Activate '.$requisition_number.'"><i class="fa fa-thumbs-up"></i>Activate</a>';
        // }
        // //create activated status display
        // else if($requisition_status == 1)
        // {
        //   $status = '<span class="label label-success">Active</span>';
        //   $button = '<a class="btn btn-default" href="'.site_url().'customers/deactivate-customer/'.$requisition_id.'" onclick="return confirm(\'Do you want to deactivate '.$requisition_number.'?\');" title="Deactivate '.$requisition_number.'"><i class="fa fa-thumbs-down"></i>Deactivate</a>';
        // }



         if($requisition_type == 2)
        {
          $requisition_type = 'Refunds';
        }
        else
          
        {
          $requisition_type = 'Repairs';
        }
       

        
        //create deactivated status display
      
        //status
       
        
        //create deactivated status display
        if($requisition_type == 2)
        {
          $requisition_type = 'Utility payments';
        }
        //create activated status display
        else if($requisition_status == 0)
        {
          $requisition_type = 'Refunds';
        }
        
        if(!empty($document))
        {
          $link = '<a href="'.$location_path.$document.'" target="_blank">Document</a>';
        }
        else
        {
          $link = '-';
        }

        
        
        $count++;
        $result .= 
        '
          <tr>
            <td>'.$count.'</td>
            <td>'.$requisition_number.'</td>
            <td>'.$requisition_type.'</td>
            <td>'.$tenant_name.'</td>
            <td>'.$property_owner_name.'</td>
            <td>'.$requisition_description.'</td>
            <td>'.$created_by.'</td>
            <td>'.date('jS M Y',strtotime($row->date_added)).'</td>
            <td>'.$status.'</td>
            <td>'.$link.'</td>
           
            <td>
                 <a href="'.base_url().'requisition-detail/'.$requisition_id.'" class="btn btn-sm btn-success">Requisition details</a>
            </td>
            <td>
                 <a href="'.base_url().'print-requsition-note/'.$requisition_id.'" target="_blank" class="btn btn-sm btn-warning"><i class="fa fa-print"></i> Print</a>
            </td>
            
          </tr> 
        ';
      }
      
      $result .= 
      '
              </tbody>
            </table>
      ';
    }
    
    else
    {
      $result .= "There are no Requisitions";
    }
?>






<div class="row">
    <div class="col-md-12">
      <section class="panel panel-info">
          <header class="panel-heading">
              <h3 class="panel-title">Search Requisition</h3>
          </header>
          <div class="panel-body">
               <div class="pull-right">
                  
               </div>
               <div class="row">
                 <div class="col-md-12">
             <?php echo form_open("finance/requisition/search_requisition", array("class" => "form-horizontal"));?>



                  <div class="col-md-3">
                       <div class="form-group">
                           <label class="col-md-4 control-label">RQ Number: </label>

                           <div class="col-md-8">
                                <input  type="text"  class="form-control" name="requisition_number" placeholder="Requisition Number" value="" autocomplete="off" >
                           </div>
                       </div>




                 </div>
                 <div class="col-md-3">
                        <div class="form-group">
                           <label class="col-md-4 control-label">Date From: </label>

                           <div class="col-md-8">
                               <div class="input-group">
                                   <span class="input-group-addon">
                                       <i class="fa fa-calendar"></i>
                                   </span>
                                   <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="date_from" placeholder="Transaction date" value="" id="datepicker"  autocomplete="off">
                               </div>
                           </div>
                       </div>
                         <div class="form-group">
                             <label class="col-md-4 control-label">Date To: </label>

                             <div class="col-md-8">
                                 <div class="input-group">
                                     <span class="input-group-addon">
                                         <i class="fa fa-calendar"></i>
                                     </span>
                                     <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="date_to" placeholder="Transaction date" value="" id="datepicker1" autocomplete="off">
                                 </div>
                             </div>
                         </div>


                 </div>
                 <div class="col-md-5">
                   <div class="form-group">
                      <label class="col-lg-3 control-label"> Type</label>

                      <div class="col-lg-8">
                        <div class="col-lg-4">
                              <div class="radio">
                                  <label>
                                      <input id="optionsRadios2" type="radio" name="requisition_type" value="0" onclick="check_type(0)">
                                      Repairs
                                  </label>
                              </div>
                          </div>
                          <div class="col-lg-4">
                              <div class="radio">
                                  <label>
                                      <input id="optionsRadios2" type="radio" name="requisition_type" value="1" onclick="check_type(1)" >
                                      Refunds
                                  </label>
                              </div>
                          </div>
                          <div class="col-lg-4">
                              <div class="radio">
                                  <label>
                                      <input id="optionsRadios2" type="radio" name="requisition_type" value="2" onclick="check_type(2)">
                                      Utility Payments
                                  </label>
                              </div>
                          </div>
                      </div>
                </div>
                 </div>
                 <div class="col-md-1">
                   <div class="form-group">
                     <div class="text-center">
                         <button type="submit" class="btn btn-sm btn-info">Search record</button>
                     </div>
                   </div>
                 </div>

               <?php echo form_close();?>
               
             </div>


           </div>

            </div>
        </section>
    </div>
    <div class="col-md-12">
      <section class="panel panel-info">
          <header class="panel-heading">
              <h3 class="panel-title">All requisitions</h3>
          </header>
          <div class="panel-body">
               <div class="pull-right">
                 <a href="#" class="btn btn-sm btn-success" data-toggle="control-sidebar" onclick="add_requisition()"><i class="fa fa-plus"></i> Add a requisition</a>
               </div>
               <br>

               <?php
                $search = $this->session->userdata('search_requisition');
                if(!empty($search))
                {
                  ?>
                          <a href="<?php echo base_url().'finance/requisition/close_requisition_search';?>" class="btn btn-sm btn-success"><i class="fa fa-print"></i> Close Search</a>
                          <?php
                }
                $error = $this->session->userdata('error_message');
                $success = $this->session->userdata('success_message');

                if(!empty($error))
                {
                  echo '<div class="alert alert-danger">'.$error.'</div>';
                  $this->session->unset_userdata('error_message');
                }

                if(!empty($success))
                {
                  echo '<div class="alert alert-success">'.$success.'</div>';
                  $this->session->unset_userdata('success_message');
                }
                ?>
                <?php echo $result;?>
            </div>

      </section>
    </div>
</div>

<script type="text/javascript">
  

$(document).on("submit","form#add-requisition",function(e)
{
      e.preventDefault();
      // myApp.showIndicator();
      
      var form_data = new FormData(this);

      // alert(form_data);

      var config_url = $('#config_url').val();


     var url = config_url+"finance/requisition/add_new_requision";
     $.ajax({
     type:'POST',
     url: url,
     data:form_data,
     dataType: 'text',
     processData: false,
     contentType: false,
     success:function(data){
         // alert("sdasldkal;skd");
        var data = jQuery.parseJSON(data);
     // alert(data.message);
        if(data.message == "success")
        {
              window.location.href = config_url+"accounting/requisitions";
        }
        else
        {
          alert('Please ensure you have added included all the items');
        }
         
     },
     error: function(xhr, status, error) {
     alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
     
     }
     });
   
  
   
  
});
  function add_requisition()
  {
    

    var config_url = $('#config_url').val();
    var data_url = config_url+"finance/requisition/add_requisition";
    //window.alert(data_url);
    $.ajax({
    type:'POST',
    url: data_url,
    data:{visit_id: 1},
    dataType: 'text',
    success:function(data){
    //window.alert("You have successfully updated the symptoms");
    //obj.innerHTML = XMLHttpRequestObject.responseText;
     $("#sidebar-div").html(data);
     CKEDITOR.replace('editor1');
     $('.select2').select2();
        //bootstrap WYSIHTML5 - text editor
     // $('.textarea').wysihtml5();
     // get_radiology_sidebar(visit_id);
      // alert(data);
    },
    error: function(xhr, status, error) {
    //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
    alert(error);
    }

    });
  }


  function check_type(type)
  {
    if(type == 0)
    {
        $('#tenant_div').css('display', 'block');
        $('#landlord_div').css('display', 'none');
    }
    else if(type == 1) 
    {
      $('#tenant_div').css('display', 'block');
      $('#landlord_div').css('display', 'none');
    }
    else if(type == 2)
    {
       $('#tenant_div').css('display', 'none');
       $('#landlord_div').css('display', 'block');
    }

  }



</script>