<?php
$rs = $this->requisition_model->get_requisition_detail($requisition_id);


if($rs->num_rows() > 0)
{
  foreach ($rs->result() as $key => $row) {
    # code...

    $requisition_id = $row->requisition_id;
    $requisition_type = $row->requisition_type;
    $requisition_description = $row->requisition_description;
    // $created_by = $row->created_by;
    $requisition_status = $row->requisition_status;
    $created = $row->created;
    $requisition_number = $row->requisition_number;
    $property_owner_name = $row->property_owner_name;
    $tenant_name = $row->tenant_name;
    $personnel_fname = $row->personnel_fname;
    $personnel_onames = $row->personnel_onames;

    $created_by = $personnel_fname.' '.$personnel_onames;

     if($requisition_type == 2)
      {
        $requisition_type = 'Refunds';
      }
      else
        
      {
        $requisition_type = 'Repairs';
      }
     

      
      //create deactivated status display
    
      //status
     
      
      //create deactivated status display
      if($requisition_type == 2)
      {
        $requisition_type = 'Utility payments';
      }
      //create activated status display
      else if($requisition_status == 0)
      {
        $requisition_type = 'Refunds';
        }

        if($requisition_type <= 1)
        {
          $name = $tenant_name;
        }
        else
        {
          $name = $property_owner_name;
        }
        
  }
}

?>
<div class="box">
    <div class="box-header with-border">
      <h3 class="box-title">Requisition # <?php echo $requisition_number;?> : <?php echo $requisition_type;?> - <?php echo $name;?></h3>

      <div class="box-tools pull-right">
         <a href="<?php echo site_url();?>print-requsition-note/<?php echo $requisition_id?>" target="_blank" class="btn btn-warning "> Print Requisition Note</a>
         <a href="<?php echo site_url();?>accounting/requisitions"  class="btn btn-info"><i class="fa fa-arrow-left"></i> Back to requisition</a>

         
      </div>
    </div>
    <div class="box-body">

          <div class="padd">
            <div class="col-md-12">                        
                <h3 class="center-align">1. Initiaton of the Request</h3>
                <br>
                <div id="request-initiation"></div>                        
            </div>
           

             <div class="col-md-12">                        
                <h3 class="center-align">2. Inspection</h3>
                <br>
                <div id="inspection"></div>                        
            </div>
            <div class="col-md-12">                        
                <h3 class="center-align">3. Inspection Confirmation</h3>
                <br>
                <div id="inspection-confirmation"></div>                        
            </div>
            <div class="col-md-12">                        
                <h3 class="center-align">4. Authorization of payments</h3>
                <br>
                <div id="authorization-of-payments"></div>                        
            </div>
            <div class="col-md-12">                        
                <h3 class="center-align">5. Confirmation of Repairs/Utilities Payments</h3>
                <br>
                <div id="confirmation-of-repairs"></div>                        
            </div>           
      </div>
    </div>
  </div>
  <div class="box-body pad">
    <div class="col-md-12 center-align">

      <?php
          echo form_open_multipart("finance/requisition/close_requisition_detail/".$requisition_id, array("class" => "form-horizontal", "role" => "form"));
          ?>
          <input type="hidden" name="redirect_url" value="<?php echo $this->uri->uri_string();?>">
         <div class="col-md-12">
            <div class="form-group text-center">
                    <label class="col-lg-4 control-label">Requisition Form</label>

                    <div class="col-lg-8">
                       <input type="file"  class="form-control" name="document_scan"  >
                                 
                    </div>
              </div>
          </div>
          <br/>
          <div class="col-md-12">
                <div class="form-actions text-center">

                    <button class="submit btn btn-primary" type="submit" onclick="return confirm('Are you sure you want to close this requisition ? ')">
                        Close Requisition
                    </button>
                </div>
          </div>
     

         
     <?php echo form_close();?>
    </div>
  </div>



<script type="text/javascript">

  
  $(document).ready(function(){
      
      display_notes(<?php echo $requisition_id?>,1);
      display_notes(<?php echo $requisition_id?>,2);
      display_notes(<?php echo $requisition_id?>,3);
      display_notes(<?php echo $requisition_id?>,4);
      display_notes(<?php echo $requisition_id?>,5);
      
  });


  function display_notes(requisition_id,type)
  {
   
     var XMLHttpRequestObject = false;
         
     if (window.XMLHttpRequest) {
     
         XMLHttpRequestObject = new XMLHttpRequest();
     } 
         
     else if (window.ActiveXObject) {
         XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
     }


     
     var config_url = document.getElementById("config_url").value;
     var url = config_url+"finance/requisition/display_details/"+requisition_id+"/"+type;
     // alert(url);
     if(XMLHttpRequestObject) {
                 
         XMLHttpRequestObject.open("GET", url);
                 
         XMLHttpRequestObject.onreadystatechange = function(){
             
             if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
                if(type == 1)
                {

                 document.getElementById("request-initiation").innerHTML=XMLHttpRequestObject.responseText;
                 CKEDITOR.replace('editor1'+requisition_id);
                   view_notes(requisition_id,type);
                }
                else if(type == 2)
                {
                  document.getElementById("inspection").innerHTML=XMLHttpRequestObject.responseText;
                  CKEDITOR.replace('editor2'+requisition_id);
                    view_notes(requisition_id,type);
                }
                else if(type == 3)
                {
                  document.getElementById("inspection-confirmation").innerHTML=XMLHttpRequestObject.responseText;
                  CKEDITOR.replace('editor3'+requisition_id);
                    view_notes(requisition_id,type);

                }
                 else if(type == 4)
                {
                  document.getElementById("authorization-of-payments").innerHTML=XMLHttpRequestObject.responseText;
                  CKEDITOR.replace('editor4'+requisition_id);
                    view_notes(requisition_id,type);
                }
                else if(type == 5)
                {
                  document.getElementById("confirmation-of-repairs").innerHTML=XMLHttpRequestObject.responseText;
                  CKEDITOR.replace('editor5'+requisition_id);
                    view_notes(requisition_id,type);
                }
               
                 
             }
         }
                 
         XMLHttpRequestObject.send(null);
     }
    
   }

  $(document).on("submit","form#add-note",function(e)
  {
      e.preventDefault();
      // myApp.showIndicator();
      
      var form_data = new FormData(this);

      // alert(form_data);

       var config_url = $('#config_url').val();
      var url = "<?php echo site_url();?>finance/requisition/save_soap_notes";


       $.ajax({
       type:'POST',
       url: url,
       data:form_data,
       dataType: 'text',
       processData: false,
       contentType: false,
        success:function(data){
           // alert(data);
           var data = jQuery.parseJSON(data);

          
          if(data.result == 'success')
          {
            // $('#history_notes').html(data.message);
            // tinymce.get('visit_presenting_complaint').setContent('');
           
            alert("You have successfully saved the note");
            var requisition_id = data.requisition_id;
            var type = data.type;
            // display_notes(requisition_id,type);
            // alert(type);
            view_notes(requisition_id,type);
          }
          else
          {
            alert("Unable to add the action plan");
          }
        //obj.innerHTML = XMLHttpRequestObject.responseText;
        },
        error: function(xhr, status, error) {
        //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
        alert(error);
        }
      });

  });


  function view_notes(requisition_id,type)
  {
   
     var XMLHttpRequestObject = false;
         
     if (window.XMLHttpRequest) {
     
         XMLHttpRequestObject = new XMLHttpRequest();
     } 
         
     else if (window.ActiveXObject) {
         XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
     }


     
     var config_url = document.getElementById("config_url").value;
     var url = config_url+"finance/requisition/notes_written/"+requisition_id+"/"+type;
     // alert(requisition_id);

     if(XMLHttpRequestObject) {
                 
         XMLHttpRequestObject.open("GET", url);
                 
         XMLHttpRequestObject.onreadystatechange = function(){
             
             if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
                if(type == 1)
                {

                 document.getElementById("section-note1").innerHTML=XMLHttpRequestObject.responseText;
                 // CKEDITOR.replace('editor1'+requisition_id);
                }
                else if(type == 2)
                {
                  document.getElementById("section-note2").innerHTML=XMLHttpRequestObject.responseText;
                  // CKEDITOR.replace('editor2'+requisition_id);
                }
                else if(type == 3)
                {
                  document.getElementById("section-note3").innerHTML=XMLHttpRequestObject.responseText;
                  // CKEDITOR.replace('editor3'+requisition_id);

                }
                 else if(type == 4)
                {
                  document.getElementById("section-note4").innerHTML=XMLHttpRequestObject.responseText;
                  // CKEDITOR.replace('editor4'+requisition_id);
                }
                else if(type == 5)
                {
                  document.getElementById("section-note5").innerHTML=XMLHttpRequestObject.responseText;
                  // CKEDITOR.replace('editor5'+requisition_id);
                }

                 
             }
         }
                 
         XMLHttpRequestObject.send(null);
     }
   }



</script>