<?php

// $rs2 = $this->nurse_model->get_visit_symptoms($requisition_id);
// $num_rows2 = count($rs2);

$v_data['signature_location'] = base_url().'assets/signatures/';
$v_data['query'] =  $query_data = $this->requisition_model->get_notes($type, $requisition_id);

if($query_data->num_rows() > 0)
{
	foreach ($query_data->result() as $key => $value_two) {
		# code...
		$summary = $value_two->notes_name;
	}
	
}
else
{
	$summary = '';
}
if(!isset($mobile_personnel_id))
{
	$mobile_personnel_id = NULL;
}

if($type == 1)
{
	$checked = 'editor1';
	$section = 'section-note1';


}else if($type == 2)
{
	$checked = 'editor2';
	$section = 'section-note2';
}
else if($type == 3)
{
	$checked = 'editor3';
	$section = 'section-note3';

	// var_dump($checked);die();
}
else if($type == 4)
{
	$checked = 'editor4';
	$section = 'section-note4';
}
else if($type == 5)
{
	$checked = 'editor5';
	$section = 'section-note5';
}
// var_dump($summary); die();

if($module <> 1)
{


?>
<?php
echo form_open($this->uri->uri_string(), array("class" => "form-horizontal", "role" => "form", "id"=> "add-note"));
?>
<div class='col-md-6'>
	<div class="row">
    	<div class='col-md-12'>
        	<input type="hidden" name="date" value="<?php echo date('Y-m-d');?>" />
        	<input type="hidden" name="time" value="<?php echo date('H:i');?>" />
        	<input type="hidden" name="type" id="type" value="<?php echo $type;?>" />
        	<input type="hidden" name="requisition_id" id="requisition_id" value="<?php echo $requisition_id?>" />
            <textarea class='form-control' name="notes" id='<?php echo $checked.$requisition_id?>' rows="5" placeholder="Describe" ><?php echo $summary?></textarea>
        </div>
    </div>
    <br>
    <div class="row" style="margin-top:10px;">
	    <div class="col-md-12">
	          <div class="form-actions text-center">
	              <button class="submit btn btn-primary" type="submit">
	                  Update request note
	              </button>
	          </div>
	      </div>
	  </div>
    <br>
</div>

 <?php echo form_close();?>
<div class='col-md-6'>
	<div id="<?php echo $section?>"></div>
</div>
<?php

}
else
{
	if(empty($summary))
	{
		echo "<p></p>
		   	  <p></p>
		   	  <p></p>
		   	  <p></p>";
	
	}
	else
	{
		 echo $summary;
	}


}
?>
