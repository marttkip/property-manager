<?php
$rs = $this->requisition_model->get_requisition_detail($requisition_id);


if($rs->num_rows() > 0)
{
  foreach ($rs->result() as $key => $row) {
    # code...

    $requisition_id = $row->requisition_id;
    $requisition_type = $row->requisition_type;
    $requisition_description = $row->requisition_description;
    // $created_by = $row->created_by;
    $requisition_status = $row->requisition_status;
    $created = $row->date_added;
    $requisition_number = $row->requisition_number;
    $property_owner_name = $row->property_owner_name;
    $tenant_name = $row->tenant_name;
    $personnel_fname = $row->personnel_fname;
    $personnel_onames = $row->personnel_onames;

    $created_by = $personnel_fname.' '.$personnel_onames;

     if($requisition_type == 2)
      {
        $requisition_type = 'Refunds';
      }
      else
        
      {
        $requisition_type = 'Repairs';
      }
     
      if($requisition_type == 2)
      {
        $requisition_type = 'Utility payments';
      }
      //create activated status display
      else if($requisition_status == 0)
      {
        $requisition_type = 'Refunds';
        }

        if($requisition_type <= 1)
        {
          $name = $tenant_name;
        }
        else
        {
          $name = $property_owner_name;
        }
        
  }
}
?>
<!DOCTYPE html>
<html lang="en">
      <head>
        <title><?php echo $contacts['company_name'];?> | Property Statement</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" media="all"/>
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css" media="all"/>
         <script src="<?php echo base_url()."assets/themes/admin/";?>bower_components/jquery/dist/jquery.min.js"></script>
        <style type="text/css">
			.receipt_spacing{letter-spacing:0px;}
			.center-align{margin:0 auto; text-align:center;}

			.receipt_bottom_border{border-bottom: #888888 medium solid; margin-bottom:1px;}
			.title-img{float:left; padding-left:30px;}
			img.logo{max-height:70px; margin:0 auto;}
			.align-right{margin:0 auto; text-align: right !important;}
			.align-center
			{
				/*padding: 50px;*/
    			text-align: center;
    			font-weight: bolder;
			}
			.table {
			  font-size: 11px !important;
			  margin-bottom: 15px;
			  max-width: 100%;
			  width: 100%;

			}
			.row {
  				margin-left: 0px !important;
    			margin-right: 0px !important;
			}
			.table-condensed > thead > tr > th, .table-condensed > tbody > tr > th, .table-condensed > tfoot > tr > th, .table-condensed > thead > tr > td, .table-condensed > tbody > tr > td, .table-condensed > tfoot > tr > td {
			  padding: 2px;
			}
			h3, .h3 {
			  font-size: 15px !important;
			}
			.col-print-1 {width:8%;  float:left;}
			.col-print-2 {width:16%; float:left;}
			.col-print-3 {width:25%; float:left;}
			.col-print-4 {width:33%; float:left;}
			.col-print-5 {width:42%; float:left;}
			.col-print-6 {width:50%; float:left;}
			.col-print-7 {width:58%; float:left;}
			.col-print-8 {width:66%; float:left;}
			.col-print-9 {width:75%; float:left;}
			.col-print-10{width:83%; float:left;}
			.col-print-11{width:92%; float:left;}
			.col-print-12{width:100%; float:left;}
			.items-list
			{

			}
			.items-list p
			{
				margin-bottom: 5px;
				border-bottom: 1px dotted #000;
				text-decoration: none;
				font-style: 10px;
			}
			body
			{
				font-style: 10px !important;
			}
			body p
			{
				font-style: 10px !important;
				margin: 0 0 0px !important;
			}
		</style>
    </head>
     <body class="receipt_spacing">
       
        <div class="row">
            <div class="col-xs-12">
            	 
		<input type="hidden" id="base_url" value="<?php echo site_url();?>">
    	<input type="hidden" id="config_url" value="<?php echo site_url();?>">
				<form class="mt-15" id="bookingForm" method="post" novalidate>
				   <h3>REQUISITION FORM</h3>
				   <div class="row receipt_bottom_border">
				      <div class="col-print-6">
				         <p>
				         <ul>
				            <u>For official use only</u>
				            <li>repairs</li>
				            <li> refunds </li>
				            <li>utility payments</li>
				         </ul>
				      </div>
				      <div class="col-print-6">
				      	<!-- <div class="row">
				      		<img src="<?php echo base_url().'assets/logo/'.$contacts['logo'];?>" alt="<?php echo $contacts['company_name'];?>" class="img-responsive logo"/>
				      	<br>
				      	</div> -->
				      	
				      	<div class="items-list">
					   		<p ><strong>DATE:</strong> <?php echo date('jS M Y',strtotime($created));?></p>
					   	</div>
				      </div>
				      </p>
				   </div>
				   <div class="row">
				      <div class="col-print-6">
				         <label for="mumber">Requisition form number:</label>  <?php echo $requisition_number;?>
				      </div>
				      
				   </div>
				   <div class="row">
				      <div class="col-print-6">
				         <label for="name">Name of landlord/landlady:</label> <?php echo $property_owner_name;?>
				      </div>
				    
				   </div>
				   <div class="row">
				      <div class="col-print-6">
				         <label for="name">Name of tenant:</label> <?php echo $tenant_name;?>
				      </div>
				    
				   </div>
				   
				   <h3>1) Initiation of request</h3>
				   <p>The following is needed for repairs,refunds or utility payments</p>
				   <p>a) Nature of the request(repairs, refunds or utility payments) and Details</p>
				  
				   	<div class="items-list">
				   		<span id="request-initiation"></span>
				   	</div>
				    
				  
				   <div class="row" style="margin-top:30px;">
				      <div class="col-print-3">
				         <div class="input-group">
				            <span>
				            <i class="fa fa-user"></i> Name:
				            </span>
				           	......................
				         </div>
				      </div>
				      <div class="col-print-3">
				         <div class="input-group">
				            <span>
				            <i class="fa fa-calendar"></i>Date:
				            </span>
				           	....................
				         </div>
				      </div>
				      <div class="col-print-3">
				         <div class="input-group">
				            <span>
				            <i class="fa fa-envelope"></i> Signature:
				            </span>
				           	.....................
				         </div>
				      </div>
				      <div class="col-print-3">
				         <div class="input-group">
				            <span>
				            <i class="fa fa-phone"></i> Tel
				            </span>
				            ......................
				         </div>
				      </div>
				   </div>
				   <h3>2) Inspection</h3>
				   <p>Quotation/refund/utility/payment is hereby under:</p>
				   <div class="items-list">
				   		<span id="inspection"></span>
				   	</div>
				   
				  <div class="row" style="margin-top:30px;">
				      <div class="col-print-3">
				         <div class="input-group">
				            <span>
				            <i class="fa fa-user"></i> Name:
				            </span>
				           	......................
				         </div>
				      </div>
				      <div class="col-print-3">
				         <div class="input-group">
				            <span>
				            <i class="fa fa-calendar"></i>Date:
				            </span>
				           	....................
				         </div>
				      </div>
				      <div class="col-print-3">
				         <div class="input-group">
				            <span>
				            <i class="fa fa-envelope"></i> Signature:
				            </span>
				           	.....................
				         </div>
				      </div>
				      <div class="col-print-3">
				         <div class="input-group">
				            <span>
				            <i class="fa fa-phone"></i> Tel
				            </span>
				            ......................
				         </div>
				      </div>
				   </div>
				   <h3><u>3) Confirmation of inspection and quotation</u></h3>
				   <p><b>(To liase direcy with the tenant/landlord/lady)</b></p>
				   <p>Qoutation/refund/utility/payment is hereby under:</p>
				   <div class="items-list">
				   		<span id="inspection-confirmation"></span>
				   	</div>
				  
				   <h3><b>
				      (Responsibility(tenant/landlord/landlady)-delete innapropriate choice)</b>
				   </h3>
				   <p>I, the undersigned confirm that i verified the inspection and quotation report above as true and as a reflection of the status in the premise. I also confirm that i have satisfactory explained to the landlord/tenant who is responsible for the repairs/refund/utility payment.</p>
				  <div class="row">
				      <div class="col-print-3">
				         <div class="input-group">
				            <span>
				            <i class="fa fa-user"></i> Name:
				            </span>
				           	......................
				         </div>
				      </div>
				      <div class="col-print-3">
				         <div class="input-group">
				            <span>
				            <i class="fa fa-calendar"></i>Date:
				            </span>
				           	....................
				         </div>
				      </div>
				      <div class="col-print-3">
				         <div class="input-group">
				            <span>
				            <i class="fa fa-envelope"></i> Signature:
				            </span>
				           	.....................
				         </div>
				      </div>
				      <div class="col-print-3">
				         <div class="input-group">
				            <span>
				            <i class="fa fa-phone"></i> Tel
				            </span>
				            ......................
				         </div>
				      </div>
				  </div>
				   <ol type="a">
				      <li>
				         The landlord/tenant:<br>
				         <ol type="i">
				            <li>
				               <b>Has consented and authorised</b> the purchase of all/some items in the attached list (landlord to sign on the list upon marking of the approved items), OR
				            </li>
				            <li>Will buy the items himself or herself and deliver them to the caretaker/watchman/cleaner</li>
				            <li>Hss not approved any of the lsited items hence left pending</li>
				         </ol>
				      </li>
				      <li>
				         <p>the landlord is not easily reachable kindly buy all the required items/or items no </p>
				         <div class="row row-xs-space mt-1">
				         <div class="input-group">
				            <span>
				            <i class="icon-birthday"></i>
				            </span>
				           ...................................
				         </div>
				         only beacuse of the urgency required then we will inform the landlord when reachable.</p>
				   </div>
				
					</li>
				</ol>
				<p style="page-break-after: always;">&nbsp;</p>
				 
			     <h3><u>4) Authorization of payments</u></h3>
				  <p>I, the undersigned confirm the release of the sum of Kshs. .................... being cost of Repairs/Utility Payment for the following items against which prices are indicated.</p>
				  <div class="items-list">
				   		<span id="authorization-of-payments"></span>
				   	</div>

				   	<div class="row" style="margin-top: 30px;">
				      <div class="col-print-3">
				         <div class="input-group">
				            <span>
				            <i class="fa fa-user"></i> Name:
				            </span>
				           	......................
				         </div>
				      </div>
				      <div class="col-print-3">
				         <div class="input-group">
				            <span>
				            <i class="fa fa-calendar"></i>Date:
				            </span>
				           	....................
				         </div>
				      </div>
				      <div class="col-print-3">
				         <div class="input-group">
				            <span>
				            <i class="fa fa-envelope"></i> Signature:
				            </span>
				           	.....................
				         </div>
				      </div>
				      <div class="col-print-3">
				         <div class="input-group">
				            <span>
				            <i class="fa fa-phone"></i> Tel
				            </span>
				            ......................
				         </div>
				      </div>
				  	</div>

				  <h3><u>5) Confirmation of Repairs/Utility Payments</u></h3>
				  <p>I, the undersigned confirm the following Repairs/Utility Payments have been undertaken.</p>
				  <div class="items-list">
				   		<span id="confirmation-of-repairs"></span>
				   	</div>

				   	<div class="row" style="margin-top: 30px;">
				      <div class="col-print-3">
				         <div class="input-group">
				            <span>
				            <i class="fa fa-user"></i> Name:
				            </span>
				           	......................
				         </div>
				      </div>
				      <div class="col-print-3">
				         <div class="input-group">
				            <span>
				            <i class="fa fa-calendar"></i>Date:
				            </span>
				           	....................
				         </div>
				      </div>
				      <div class="col-print-3">
				         <div class="input-group">
				            <span>
				            <i class="fa fa-envelope"></i> Signature:
				            </span>
				           	.....................
				         </div>
				      </div>
				      <div class="col-print-3">
				         <div class="input-group">
				            <span>
				            <i class="fa fa-phone"></i> Tel
				            </span>
				            ......................
				         </div>
				      </div>
				  	</div>
				 <h3><u>6) Confirmation of Refunds </u></h3>
				  <p>I, the undersigned confirm that refunds in the sum of Kshs .................. has been made to the following tenant;</p>


				   <p><strong> Name of Tenant :</strong>	..................................</p>
				   <br>
				    <p><strong> Amount Paid :</strong>	..................................</p>
				    <br>
				     <p><strong> Signature of Tenant :</strong>	..................................</p>

				   	<div class="row" style="margin-top: 50px">
				      <div class="col-print-3">
				         <div class="input-group">
				            <span>
				            <i class="fa fa-user"></i> Name:
				            </span>
				           	......................
				         </div>
				      </div>
				      <div class="col-print-3">
				         <div class="input-group">
				            <span>
				            <i class="fa fa-calendar"></i>Date:
				            </span>
				           	....................
				         </div>
				      </div>
				      <div class="col-print-3">
				         <div class="input-group">
				            <span>
				            <i class="fa fa-envelope"></i> Signature:
				            </span>
				           	.....................
				         </div>
				      </div>
				      <div class="col-print-3">
				         <div class="input-group">
				            <span>
				            <i class="fa fa-phone"></i> Tel
				            </span>
				            ......................
				         </div>
				      </div>
				  	</div>

				   <h3><u>7) Audit</u></h3>
				  <p>I, the undersigned confirm that the process of Repairs/Refunds/Utility Payments has been completed and the accounts are now duly settled and audited.</p>

				   	<div class="row" style="margin-top: 30px;">
				      <div class="col-print-3">
				         <div class="input-group">
				            <span>
				            <i class="fa fa-user"></i> Name:
				            </span>
				           	......................
				         </div>
				      </div>
				      <div class="col-print-3">
				         <div class="input-group">
				            <span>
				            <i class="fa fa-calendar"></i>Date:
				            </span>
				           	....................
				         </div>
				      </div>
				      <div class="col-print-3">
				         <div class="input-group">
				            <span>
				            <i class="fa fa-envelope"></i> Signature:
				            </span>
				           	.....................
				         </div>
				      </div>
				      <div class="col-print-3">
				         <div class="input-group">
				            <span>
				            <i class="fa fa-phone"></i> Tel
				            </span>
				            ......................
				         </div>
				      </div>
				  	</div>
					  
				</form>
				</li>
				</ol>
				 <h3><i> NB: The <u>actual</u> and <u>ETR</u> receipts of the purchases should be availed to accounts office as proof of purchase ad accountability.</i></h3>

            </div>
        </div>
       
        <div class="row" >
       		<div class="col-xs-12 align-center" style="border-top:1px #000 solid;">
       			<span style="font-size:10px; " > <?php echo $contacts['company_name'];?> | <?php echo $contacts['location'];?>, <?php echo $contacts['building'];?>, <?php echo $contacts['floor'];?>
                     E-mail: <?php echo $contacts['email'];?>.<br/> Tel : <?php echo $contacts['phone'];?>
                     P.O. Box <?php echo $contacts['address'];?> <?php echo $contacts['post_code'];?>, <?php echo $contacts['city'];?></span>
            </div>
       	</div>
       	<script type="text/javascript">

			  $(document).ready(function(){
			     
			      display_notes(<?php echo $requisition_id?>,1);
			      display_notes(<?php echo $requisition_id?>,2);
			      display_notes(<?php echo $requisition_id?>,3);
			      display_notes(<?php echo $requisition_id?>,4);
			      display_notes(<?php echo $requisition_id?>,5);
			      
			  });

			  function display_notes(requisition_id,type)
			  {
			   

			     var XMLHttpRequestObject = false;
			         
			     if (window.XMLHttpRequest) {
			     
			         XMLHttpRequestObject = new XMLHttpRequest();
			     } 
			         
			     else if (window.ActiveXObject) {
			         XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
			     }


			     
			     var config_url = document.getElementById("config_url").value;
			     var url = config_url+"finance/requisition/display_details/"+requisition_id+"/"+type+"/1";
			    

			     if(XMLHttpRequestObject) {
			                 
			         XMLHttpRequestObject.open("GET", url);
			                 
			         XMLHttpRequestObject.onreadystatechange = function(){
			             
			             if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
			                if(type == 1)
			                {

			                 document.getElementById("request-initiation").innerHTML=XMLHttpRequestObject.responseText;
			                 CKEDITOR.replace('editor1'+requisition_id);
			                   view_notes(requisition_id,type);
			                }
			                else if(type == 2)
			                {
			                  document.getElementById("inspection").innerHTML=XMLHttpRequestObject.responseText;
			                  CKEDITOR.replace('editor2'+requisition_id);
			                    view_notes(requisition_id,type);
			                }
			                else if(type == 3)
			                {
			                  document.getElementById("inspection-confirmation").innerHTML=XMLHttpRequestObject.responseText;
			                  CKEDITOR.replace('editor3'+requisition_id);
			                    view_notes(requisition_id,type);

			                }
			                 else if(type == 4)
			                {
			                  document.getElementById("authorization-of-payments").innerHTML=XMLHttpRequestObject.responseText;
			                  CKEDITOR.replace('editor4'+requisition_id);
			                    view_notes(requisition_id,type);
			                }
			                else if(type == 5)
			                {
			                  document.getElementById("confirmation-of-repairs").innerHTML=XMLHttpRequestObject.responseText;
			                  CKEDITOR.replace('editor5'+requisition_id);
			                    view_notes(requisition_id,type);
			                }
			               
			                 
			             }
			         }
			                 
			         XMLHttpRequestObject.send(null);
			     }
			    
			   }
       	</script>
     </body>
</html>