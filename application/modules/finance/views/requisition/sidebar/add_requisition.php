<?php

$tenant_where = 'tenants.tenant_id = tenant_unit.tenant_id AND tenant_unit.rental_unit_id = rental_unit.rental_unit_id  AND tenant_unit.tenant_unit_id = leases.tenant_unit_id AND leases.lease_status < 4 AND tenant_unit.tenant_unit_id = leases.tenant_unit_id' ;
$tenant_table = 'tenants,tenant_unit,rental_unit,leases';
$tenant_order = 'tenants.tenant_name';

$tenant_query = $this->tenants_model->get_tenant_list($tenant_table, $tenant_where, $tenant_order);
$rs8 = $tenant_query->result();
$tenants_list = '';
foreach ($rs8 as $tenant_rs) :
  $tenant_id = $tenant_rs->tenant_id;
  $tenant_name = $tenant_rs->tenant_name;

  $tenant_national_id = $tenant_rs->tenant_national_id;
  $tenant_phone_number = $tenant_rs->tenant_phone_number;
  $rental_unit_name = $tenant_rs->rental_unit_name;
  $tenant_number = $tenant_rs->tenant_number;
  $lease_number = $tenant_rs->lease_number;
  $lease_id = $tenant_rs->lease_id;

    $tenants_list .="<option value='".$lease_id."'>".$tenant_number."  ".$tenant_name." A/C No: ".$lease_number." ".$rental_unit_name."</option>";

endforeach;




$owner_where = 'property_owners.property_owner_id > 0' ;
$owner_table = 'property_owners';
$owner_order = 'property_owner_id';

$owner_query = $this->tenants_model->get_tenant_list($owner_table, $owner_where, $owner_order);
$rs9 = $owner_query->result();
$property_owner_list = '';
foreach ($rs9 as $owner_rs) :

  $property_owner_name = $owner_rs->property_owner_name;
  $property_owner_id = $owner_rs->property_owner_id;

    $property_owner_list .="<option value='".$property_owner_id."'>".$property_owner_name."</option>";

endforeach;

?>
<div class="box" style="color:black !important;">
	<div class="box-body pad">
		<div class="col-md-12">

    	<?php
        	echo form_open($this->uri->uri_string(), array("class" => "form-horizontal", "role" => "form", "id"=> "add-requisition"));
          ?>
    	   <div class="col-md-12">
        		<div class="form-group">
    	              <label class="col-lg-3 control-label">Requisition Type</label>

    	              <div class="col-lg-8">
    	                <div class="col-lg-4">
                            <div class="radio">
                                <label>
                                    <input id="optionsRadios2" type="radio" name="requisition_type" value="0" onclick="check_type(0)">
                                    Repairs
                                </label>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="radio">
                                <label>
                                    <input id="optionsRadios2" type="radio" name="requisition_type" value="1" onclick="check_type(1)" >
                                    Refunds
                                </label>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="radio">
                                <label>
                                    <input id="optionsRadios2" type="radio" name="requisition_type" value="2" onclick="check_type(2)">
                                    Utility Payments
                                </label>
                            </div>
                        </div>
    	              </div>
	            </div>
	        </div>

            <div class="col-md-12">
                <div class="col-md-12" style="display: none" id="tenant_div">
                    <div class="form-group">
                        <label class="col-lg-3 control-label">Tenant</label>

                        <div class="col-lg-8">
                            <select name="lease_id" id="lease_id" class="form-control col-md-12 select2">
                                <option value=''>None - Please Select a Tenant</option>
                                <?php echo $tenants_list?>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="col-md-12" style="display: none" id="landlord_div">
                    <div class="form-group">
                        <label class="col-lg-3 control-label">Landloard</label>

                        <div class="col-lg-8">
                            <select name="landlord_id" id="landlord_id" class="form-control col-md-12 select2">
                                <option value=''>None - Please Select a landlord</option>
                                <?php echo $property_owner_list;?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
	    	<div class="col-md-12">
	    		<textarea id="editor1" name="requisition_description" rows="10" cols="80" placeholder="Requisiton Description" required="required">
	                 
	          	</textarea>
	    	</div>
	    	<br/>
	        <div class="col-md-12">
	              <div class="form-actions text-center">

	                  <button class="submit btn btn-primary" type="submit">
	                      Add Requisiton
	                  </button>
	              </div>
	        </div>
	   

         
     <?php echo form_close();?>
	 	</div>
	</div>
</div>
 	

 <!-- Create the tabs -->
			   