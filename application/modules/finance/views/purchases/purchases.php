<!-- search -->
<?php //echo $this->load->view('search/search_petty_cash', '', TRUE);

    $properties = $this->property_model->get_active_property();
    $rs8 = $properties->result();
    $property_list = '';
    foreach ($rs8 as $property_rs) :
        $property_id = $property_rs->property_id;
        $property_name = $property_rs->property_name;
        $property_location = $property_rs->property_location;

        $property_list .="<option value='".$property_id."'>".$property_name." Location: ".$property_location."</option>";

    endforeach;
$document_invoice = $this->purchases_model->create_purchases_number();
$document_receipt = $this->purchases_model->create_purchases_payment();

$month = $this->accounts_model->get_months();
$months_list = '<option value="0">Select a Type</option>';
foreach($month->result() as $res)
{
  $month_id = $res->month_id;
  $month_name = $res->month_name;
  if($month_id < 10)
  {
    $month_id = '0'.$month_id;
  }
  $month = date('M');

  if($month == $month_name)
  {
    $months_list .= '<option value="'.$month_id.'" selected>'.$month_name.'</option>';
  }
  else {
    $months_list .= '<option value="'.$month_id.'">'.$month_name.'</option>';
  }



}


$start = 2015;
$end_year = 2030;
$year_list = '<option value="0">Select a Type</option>';
for ($i=$start; $i < $end_year; $i++) {
  // code...
  $year= date('Y');

  if($year == $i)
  {
    $year_list .= '<option value="'.$i.'" selected>'.$i.'</option>';
  }
  else {
    $year_list .= '<option value="'.$i.'">'.$i.'</option>';
  }
}

 
?>
<!-- end search -->
<!--begin the reports section-->
<?php
//unset the sessions set\
?>
<!--end reports -->
<div class="row">
    <div class="col-md-12">
      <div class="box">
               <div class="box-header with-border">
                 <h3 class="box-title">Search List</h3>

                 <div class="box-tools pull-right">
                 </div>
               </div>
               <div class="box-body">
               <div class="pull-right">
                 <!-- <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#record_petty_cash"><i class="fa fa-plus"></i> Record</button>
                 <a href="<?php echo base_url().'accounts/petty_cash/print_petty_cash/';?>" class="btn btn-sm btn-success" target="_blank"><i class="fa fa-print"></i> Print</a>
                 <a href="<?php echo base_url().'administration/sync_app_petty_cash';?>" class="btn btn-sm btn-info"><i class="fa fa-sign-out"></i> Sync</a> -->
               </div>

             <?php echo form_open("finance/purchases/search_purchases", array("class" => "form-horizontal"));?>
               <div class="row">
                 <div class="col-md-12">
                  <div class="col-md-2">
                       <div class="form-group">
                           <label class="col-md-4 control-label">Document#: </label>

                           <div class="col-md-8">
                               <div class="input-group">
                                   <input type="text"  class="form-control" name="document_number" placeholder="Document No" value=""  autocomplete="off" >
                               </div>
                           </div>
                       </div>
                  </div>
                 <div class="col-md-3">
                       <div class="form-group">
                           <label class="col-md-4 control-label">Date From: </label>

                           <div class="col-md-8">
                               <div class="input-group">
                                  
                                   <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="date_from" placeholder="Transaction date" value="" id="datepicker" autocomplete="off" >
                               </div>
                           </div>
                       </div>
                  </div>
                   <div class="col-md-3">
                       <div class="form-group">
                           <label class="col-md-4 control-label">Date To: </label>

                           <div class="col-md-8">
                               <div class="input-group">
                                   <span class="input-group-addon">
                                       <i class="fa fa-calendar"></i>
                                   </span>
                                   <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="date_to" placeholder="Transaction date" value="" id="datepicker1" autocomplete="off" >
                               </div>
                           </div>
                       </div>
                  </div>
                  <div class="col-md-3">
                       <div class="form-group">
                           <label class="col-lg-4 control-label">Property</label>

                           <div class="col-lg-8">
                              <select  name='property_id' class='form-control select2'>
                                 <option value=''>None - Please Select a property</option>
                                 <?php echo $property_list;?>
                               </select>
                           </div>
                       </div>
                 </div>
                
                <div class="col-md-1">
                         <div class="form-group">
                           <div class="text-center">
                               <button type="submit" class="btn btn-sm btn-primary">Search record</button>
                           </div>
                         </div>

                 </div>
                 </div>


               </div>
               <?php echo form_close();?>

            </div>
        </div>
    </div>
    <div class="col-md-12">
      <div class="box">
               <div class="box-header with-border">
                 <h3 class="box-title">Add <?php echo $title;?></h3>

                 <div class="box-tools pull-right">
                 </div>
               </div>
               <div class="box-body">
               <div class="pull-right">
                 <!-- <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#record_petty_cash"><i class="fa fa-plus"></i> Record</button>
                 <a href="<?php echo base_url().'accounts/petty_cash/print_petty_cash/';?>" class="btn btn-sm btn-success" target="_blank"><i class="fa fa-print"></i> Print</a>
                 <a href="<?php echo base_url().'administration/sync_app_petty_cash';?>" class="btn btn-sm btn-info"><i class="fa fa-sign-out"></i> Sync</a> -->
               </div>

             <?php echo form_open("finance/purchases/record_landlord_expenses", array("class" => "form-horizontal"));?>
               <div class="row">
                 <div class="col-md-12">
                 <div class="col-md-6">
                       <div class="form-group">
                           <label class="col-md-4 control-label">Transaction date: </label>

                           <div class="col-md-8">
                               <div class="input-group">
                                   <span class="input-group-addon">
                                       <i class="fa fa-calendar"></i>
                                   </span>
                                   <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="transaction_date" placeholder="Transaction date" value="<?php echo date('Y-m-d');?>" id="datepicker2" required>
                               </div>
                           </div>
                       </div>
                       <div class="form-group">
                           <label class="col-md-4 control-label">Transaction Number *</label>

                           <div class="col-md-8">
                               <input type="text" class="form-control" name="transaction_number" placeholder="Transaction Number" value="<?php echo $document_invoice;?>" required="required" readonly/>
                           </div>
                       </div>
                       <div class="form-group">
                           <label class="col-md-4 control-label">Type *</label>

                           <div class="col-md-8">
                               <div class="radio">
                                   <label>
                                       <input  type="radio" checked value="0" name="transaction_type_id" id="account_to_type" onclick="get_transaction_type_list(this.value)">
                                       None
                                   </label>
                                   <label>
                                       <input  type="radio"  value="1" name="transaction_type_id" id="account_to_type" onclick="get_transaction_type_list(this.value)">
                                       Creditor
                                   </label>
                               </div>
                           </div>
                       </div>
                       <div class="form-group" style="display:none;" id="creditor_div">
                           <label class="col-md-4 control-label">Creditor*</label>

                           <div class="col-md-8">
                               <select class="form-control" name="creditor_id" id="creditor_id">
                                 <option value="">--- select a creditor - ---</option>
                                 <?php
                                 if($creditors->num_rows() > 0)
                                 {
                                   foreach ($creditors->result() as $key => $value) {
                                     // code...
                                     $creditor_id = $value->creditor_id;
                                     $creditor_name = $value->creditor_name;
                                     echo '<option value="'.$creditor_id.'"> '.$creditor_name.'</option>';
                                   }
                                 }
                                 ?>
                               </select>
                           </div>
                       </div>

                       <div class="form-group" >
                           <label class="col-md-4 control-label">Expense Account *</label>

                           <div class="col-md-8">
                               <select class="form-control select2" name="account_to_id" id="account_to_id" required>
                                 <option value="">--- select an expense account - ---</option>
                                 <?php
                                 if($expense_accounts->num_rows() > 0)
                                 {
                                   foreach ($expense_accounts->result() as $key => $value) {
                                     // code...
                                     $account_id = $value->account_id;
                                     $account_name = $value->account_name;
                                     echo '<option value="'.$account_id.'"> '.$account_name.'</option>';
                                   }
                                 }
                                 ?>
                               </select>
                           </div>
                       </div>
                       <div class="form-group">
                           <label class="col-md-4 control-label">Description *</label>

                           <div class="col-md-8">
                               <textarea class="form-control" name="description" required></textarea>
                           </div>
                       </div>

                 </div>
                 <div class="col-md-6">
                          <div class="form-group">
                             <label class="col-lg-4 control-label">Property</label>

                             <div class="col-lg-8">
                                <select  name='property_id' class='form-control select2' required="required">
                                   <option value=''>None - Please Select a property</option>
                                   <?php echo $property_list;?>
                                 </select>
                             </div>
                         </div>
                           
                            <div class="form-group" >
                                <label class="col-md-4 control-label">Payment Account*</label>

                                <div class="col-md-8">
                                    <select class="form-control" name="account_from_id" required>
                                      <option value=""> ---- Select paying account ----</option>
                                      <?php
                                      if($accounts->num_rows() > 0)
                                      {
                                        foreach ($accounts->result() as $key => $value) {
                                          // code...
                                          $account_id = $value->account_id;
                                          $account_name = $value->account_name;
                                          echo '<option value="'.$account_id.'"> '.$account_name.'</option>';
                                        }
                                      }
                                     ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Payment Reference No *</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="reference_number" placeholder="Ref No." />
                                </div>
                            </div>
                           <input type="hidden" class="form-control" name="type" value="1" required/>
                           

                           <div class="form-group">
                               <label class="col-md-4 control-label">Amount *</label>

                               <div class="col-md-8">
                                   <input type="text" class="form-control" name="transacted_amount" placeholder="Amount" required/>
                               </div>
                           </div>
                              <div class="form-group" id="payment_method">
                                <label class="col-md-4 control-label">Month: </label>

                                <div class="col-md-8">
                                  <select class="form-control select2" name="purchase_month"   required>
                                    <?php echo $months_list;?>
                                  </select>
                                  </div>
                              </div>
                              <div class="form-group" id="payment_method">
                                <label class="col-md-4 control-label">Year: </label>
                                <div class="col-md-8">
                                  <select class="form-control select2" name="purchase_year"   required>

                                    <?php echo $year_list;?>
                                  </select>
                                  </div>
                              </div>
                        </div>
                     </div>
                 <br>
                 <div class="row">
                       <div class="col-md-12">
                           <div class="text-center">
                              <button type="submit" class="btn btn-sm btn-primary" onclick="return confirm('Are you sure you want to add this record ? ')">Save record</button>
                           </div>
                       </div>
                   </div>

               </div>
               <?php echo form_close();?>


            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">


			<?php
      $search = $this->session->userdata('search_purchases_items');
			if(!empty($search))
			{
				?>
                <a href="<?php echo base_url().'finance/purchases/close_search';?>" class="btn btn-sm btn-success"><i class="fa fa-print"></i> Close Search</a>
                <?php
			}
			$error = $this->session->userdata('error_message');
			$success = $this->session->userdata('success_message');

      // var_dump($error);die();

			if(!empty($error))
			{
				echo '<div class="alert alert-danger">'.$error.'</div>';
				$this->session->unset_userdata('error_message');
			}

			if(!empty($success))
			{
				echo '<div class="alert alert-success">'.$success.'</div>';
				$this->session->unset_userdata('success_message');
			}
      ?>
    </div>
  <div class="col-md-12">
    <?php
			$result =  '';

			// echo $result;

      $result = '';
$count = 0;
  if($query_purchases->num_rows() > 0)
  {
    foreach ($query_purchases->result() as $key => $value) {
      // code...
      $document_number = $value->document_number;
      $transaction_number = $value->transaction_number;
      $transaction_date = $value->transaction_date;
      $creditor_name = $value->creditor_name;
      $creditor_id = $value->creditor_id;
      $property_name = $value->property_name;
      $property_id = $value->property_id;
      $account_name = $value->account_name;
      $finance_purchase_id = $value->finance_purchase_id;
      $finance_purchase_description = $value->finance_purchase_description;
      $finance_purchase_amount = $value->finance_purchase_amount;
      $finance_purchase_payment_id = $value->finance_purchase_payment_id;
      $finance_purchase_deleted = $value->finance_purchase_deleted;
      
      $amount_paid = $this->purchases_model->get_amount_paid($finance_purchase_id);
      $checkbox_data = array(
                'name'        => 'visit[]',
                'id'          => 'checkbox'.$finance_purchase_id,
                'class'          => 'css-checkbox lrg',
                'value'       => $finance_purchase_id
              );

      if($amount_paid == $finance_purchase_amount)
      {
        $status = '<td class="success">Fully Paid</td>';
      }
      else if($amount_paid >0) {
        $status = '<td class="warning">Partially paid</td>';
      }
      else {
        $status = '<td class="primary">Not paid</td>';
      }

      if($finance_purchase_deleted == 0)
      {
        $delete_status = '<span class="label label-xs label-default"> Active</span>';
      }
      else if($finance_purchase_deleted == 1)
      {
        $delete_status = '<span class="label label-xs label-warning">Pending Delete Approval</span>';
      }
      else if($finance_purchase_deleted == 2)
      {
        $delete_status = '<span class="label label-xs label-danger">Deleted</span>';
      }

      $balance = $finance_purchase_amount - $amount_paid;

      $checked = '<td><a href="'.base_url().'edit-voucher/'.$finance_purchase_payment_id.'" class="btn btn-xs btn-success"><i class="fa fa-pencil"></i> Edit</a></td>
                  <td><a href="'.base_url().'print-vourcher/'.$finance_purchase_payment_id.'" target="_blank" class="btn btn-xs btn-warning"><i class="fa fa-print"></i> Voucher</a></td>
                  <td><a href="'.base_url().'delete-purchases/'.$finance_purchase_id.'" class="btn btn-xs btn-danger" onclick="return confirm(\'Are you sure you want to delete this record ? \')" ><i class="fa fa-trash"></i></a></td>
                ';
      $count++;
      $result .='
                <tr>
                  <td>'.$count.'</td>
                 
                  <td>'.$transaction_date.'</td>
                  <td>'.$document_number.'</td>
                  <td>'.$finance_purchase_description.'</td>
                  <td>'.number_format($finance_purchase_amount,2).'</td>
                  <td>'.number_format($amount_paid,2).'</td>
                  <td>'.$property_name.'</td>
                  <td>'.$delete_status.'</td>
                  '.$checked.'
                </tr>
                ';
    }
  }
?>
<div class="box">
 <div class="box-header with-border">
   <h3 class="box-title">All Invoices</h3>

   <div class="box-tools pull-right">
   </div>
 </div>
 <div class="box-body">
      <table class="table table-hover table-bordered ">
			 	<thead>
					<tr>
            <th>#</th>
					  <th>Date</th>
					  <th>Ref Number</th>
					  <th>Description</th>
					  <th>Invoice Amount</th>
            <th>Amount Paid</th>
            <th>Property</th>
            <th colspan="4">Action</th>
					</tr>
				 </thead>
			  	<tbody>
            <?php echo $result;?>
			  	</tbody>
			</table>

    </div>
</div>
</div>
</div>

 <script type="text/javascript">

        function get_transaction_type_list(type)
        {
            // var type = getRadioCheckedValue(radio_name);
            // $("#charge_to_id").customselect()="";
            // alert(radio_name);
            var myTarget1 = document.getElementById("creditor_div");

            if(type == 1)
            {


                myTarget1.style.display = 'block';
                $('#creditor_id').addClass('select2');
            }
            else {
              myTarget1.style.display = 'none';
            }
            var url = "<?php echo site_url();?>accounting/petty_cash/get_list_type_petty_cash/1";
            // alert(url);
            //get department services
            $.get( url, function( data )
            {
                $( "#charge_to_id" ).html( data );
                // $(".custom-select").customselect();
            });

        }

        function getRadioCheckedValue(radio_name)
        {
           var oRadio = document.forms[0].elements[radio_name];

           for(var i = 0; i < oRadio.length; i++)
           {
              if(oRadio[i].checked)
              {
                 return oRadio[i].value;
              }
           }

           return '';
        }

        function display_payment_model(modal_id)
      	{
      		$('#modal-defaults'+modal_id).modal('show');
      		$('#datepicker1'+modal_id).datepicker({
    	      autoclose: true,
    	      format: 'yyyy-mm-dd',
    	    })
      	}


        function check_payment_type(payment_type_id){

          var myTarget1 = document.getElementById("cheque_div");

          var myTarget2 = document.getElementById("mpesa_div");

          var myTarget3 = document.getElementById("insuarance_div");

          if(payment_type_id == 1)
          {
            // this is a check

            myTarget1.style.display = 'block';
            myTarget2.style.display = 'none';
            myTarget3.style.display = 'none';
          }
          else if(payment_type_id == 2)
          {
            myTarget1.style.display = 'none';
            myTarget2.style.display = 'none';
            myTarget3.style.display = 'none';
          }
          else if(payment_type_id == 3)
          {
            myTarget1.style.display = 'none';
            myTarget2.style.display = 'none';
            myTarget3.style.display = 'block';
          }
          else if(payment_type_id == 4)
          {
            myTarget1.style.display = 'none';
            myTarget2.style.display = 'none';
            myTarget3.style.display = 'none';
          }
          else if(payment_type_id == 5)
          {
            myTarget1.style.display = 'none';
            myTarget2.style.display = 'block';
            myTarget3.style.display = 'none';
          }
          else
          {
            myTarget2.style.display = 'none';
            myTarget3.style.display = 'block';
          }

        }

    </script>
