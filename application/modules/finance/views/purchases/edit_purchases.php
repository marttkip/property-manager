<?php //echo $this->load->view('search/search_petty_cash', '', TRUE);


$purchase_payment_rs = $this->purchases_model->get_purchase_payment_detail($finance_purchase_payment_id);

if($purchase_payment_rs->num_rows() > 0)
{
	foreach ($purchase_payment_rs->result() as $key => $value_checked) {
		# code...
		$finance_purchase_id = $value_checked->finance_purchase_id;
		$finance_purchase_amount = $value_checked->finance_purchase_amount;
		$finance_purchase_description = $value_checked->finance_purchase_description;
		$property_id = $value_checked->property_id;
		$account_to_id = $value_checked->account_to_id;
		$quantity = $value_checked->quantity;
		$finance_purchase_status = $value_checked->finance_purchase_status;
		$transaction_number = $value_checked->transaction_number;
		$document_number = $value_checked->document_number;
		$transaction_date = $value_checked->transaction_date;
		$creditor_id = $value_checked->creditor_id;
		$purchase_month = $value_checked->purchase_month;
		$purchase_year = $value_checked->purchase_year;
		$account_from_id = $value_checked->account_from_id;
		$type = $value_checked->type;
		$purchase_document_number = $value_checked->purchase_document_number;

	}
}


if(!empty($creditor_id))
{
	$creditor = 'checked';
	$non_creditor = '';
}
else
{
	$creditor = '';
	$non_creditor = 'checked';
}

// var_dump($creditor_id);die();

$properties = $this->property_model->get_active_property();
$rs8 = $properties->result();
$property_list = '';
foreach ($rs8 as $property_rs) :
    $property_idd = $property_rs->property_id;
    $property_name = $property_rs->property_name;
    $property_location = $property_rs->property_location;
    if($property_id == $property_idd)
    {
    	$property_list .="<option value='".$property_idd."' selected>".$property_name." Location: ".$property_location."</option>";
    }
    else
    {
    	$property_list .="<option value='".$property_idd."'>".$property_name." Location: ".$property_location."</option>";
    }
    

endforeach;
$document_invoice = $document_number;
$document_receipt = $document_number;

$month = $this->accounts_model->get_months();
$months_list = '<option value="0">Select a Type</option>';
// var_dump($purchase_month);die();
foreach($month->result() as $res)
{
  $month_id = $res->month_id;
  $month_name = $res->month_name;
  if($month_id < 10)
  {
    $month_id = '0'.$month_id;
  }
  $month = date('M');

  if($purchase_month == $month_id)
  {
    $months_list .= '<option value="'.$month_id.'" selected>'.$month_name.'</option>';
  }
  else {
    $months_list .= '<option value="'.$month_id.'">'.$month_name.'</option>';
  }



}


$start = 2015;
$end_year = 2030;
$year_list = '<option value="0">Select a Type</option>';
for ($i=$start; $i < $end_year; $i++) {
  // code...

  if($purchase_year == $i)
  {
    $year_list .= '<option value="'.$i.'" selected>'.$i.'</option>';
  }
  else {
    $year_list .= '<option value="'.$i.'">'.$i.'</option>';
  }
}

 
?>
<div class="row">
    
    <div class="col-md-12">
      <div class="box">
               <div class="box-header with-border">
                 <h3 class="box-title"><?php echo $title;?></h3>

                 <div class="box-tools pull-right">
                 	<a href="<?php echo base_url().'accounting/landlord-expenses';?>" class="btn btn-sm btn-warning"><i class="fa fa-arrow-left"></i> Back to expenses</a> 
                 </div>
               </div>
               <div class="box-body">
               

             <?php echo form_open("finance/purchases/update_landlord_expenses/".$finance_purchase_id."/".$finance_purchase_payment_id, array("class" => "form-horizontal"));?>
               <div class="row">
                 <div class="col-md-12">
                 <div class="col-md-6">
                       <div class="form-group">
                           <label class="col-md-4 control-label">Transaction date: </label>

                           <div class="col-md-8">
                               <div class="input-group">
                                   <span class="input-group-addon">
                                       <i class="fa fa-calendar"></i>
                                   </span>
                                   <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="transaction_date" placeholder="Transaction date" value="<?php echo $transaction_date;?>" id="datepicker2" required>
                               </div>
                           </div>
                       </div>
                       <div class="form-group">
                           <label class="col-md-4 control-label">Transaction Number *</label>

                           <div class="col-md-8">
                               <input type="text" class="form-control" name="transaction_number" placeholder="Transaction Number" value="<?php echo $document_invoice;?>" required="required" readonly/>
                           </div>
                       </div>
                       <div class="form-group">
                           <label class="col-md-4 control-label">Type *</label>

                           <div class="col-md-8">
                               <div class="radio">
                                   <label>
                                       <input  type="radio" <?php echo $non_creditor;?> value="0" name="transaction_type_id" id="account_to_type" onclick="get_transaction_type_list(this.value)">
                                       None
                                   </label>
                                   <label>
                                       <input  type="radio" <?php echo $creditor;?>  value="1" name="transaction_type_id" id="account_to_type" onclick="get_transaction_type_list(this.value)">
                                       Creditor
                                   </label>
                               </div>
                           </div>
                       </div>
                       <div class="form-group" style="display:none;" id="creditor_div">
                           <label class="col-md-4 control-label">Creditor*</label>

                           <div class="col-md-8">
                               <select class="form-control" name="creditor_id" id="creditor_id">
                                 <option value="">--- select a creditor - ---</option>
                                 <?php
                                 if($creditors->num_rows() > 0)
                                 {
                                   foreach ($creditors->result() as $key => $value) {
                                     // code...
                                     $creditor_idd = $value->creditor_id;
                                     $creditor_name = $value->creditor_name;

                                     if($creditor_id == $creditor_idd)
                                     {
                                     	echo '<option value="'.$creditor_idd.'" selected> '.$creditor_name.'</option>';
                                     }
                                     else
                                     {
                                     	echo '<option value="'.$creditor_idd.'"> '.$creditor_name.'</option>';
                                     }
                                     
                                   }
                                 }
                                 ?>
                               </select>
                           </div>
                       </div>

                       <div class="form-group" >
                           <label class="col-md-4 control-label">Expense Account *</label>

                           <div class="col-md-8">
                               <select class="form-control select2" name="account_to_id" id="account_to_id" required>
                                 <option value="">--- select an expense account - ---</option>
                                 <?php
                                 if($expense_accounts->num_rows() > 0)
                                 {
                                   foreach ($expense_accounts->result() as $key => $value) {
                                     // code...
                                     $account_id = $value->account_id;
                                     $account_name = $value->account_name;

                                     if($account_to_id == $account_id)
                                     {
                                     	echo '<option value="'.$account_id.'" selected> '.$account_name.'</option>';
                                     }
                                     else
                                     {
                                     	echo '<option value="'.$account_id.'"> '.$account_name.'</option>';
                                     }
                                     
                                   }
                                 }
                                 ?>
                               </select>
                           </div>
                       </div>
                       <div class="form-group">
                           <label class="col-md-4 control-label">Description *</label>

                           <div class="col-md-8">
                               <textarea class="form-control" name="description" required><?php echo $finance_purchase_description;?></textarea>
                           </div>
                       </div>

                 </div>
                 <div class="col-md-6">
                          <div class="form-group">
                             <label class="col-lg-4 control-label">Property</label>

                             <div class="col-lg-8">
                                <select  name='property_id' class='form-control select2' required="required">
                                   <option value=''>None - Please Select a property</option>
                                   <?php echo $property_list;?>
                                 </select>
                             </div>
                         </div>
                           
                            <div class="form-group" >
                                <label class="col-md-4 control-label">Payment Account*</label>

                                <div class="col-md-8">
                                    <select class="form-control" name="account_from_id" required>
                                      <option value=""> ---- Select paying account ----</option>
                                      <?php
                                      if($accounts->num_rows() > 0)
                                      {
                                        foreach ($accounts->result() as $key => $value) {
                                          // code...
                                          $account_id = $value->account_id;
                                          $account_name = $value->account_name;
                                          if($account_from_id == $account_id)
                                          {
                                          	echo '<option value="'.$account_id.'" selected> '.$account_name.'</option>';
                                          }
                                          else
                                          {
                                          	echo '<option value="'.$account_id.'"> '.$account_name.'</option>';
                                          }
                                          
                                        }
                                      }
                                     ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Payment Reference No *</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="reference_number" placeholder="Ref No." value="<?php echo $transaction_number?>" />
                                </div>
                            </div>
                           <input type="hidden" class="form-control" name="type" value="<?php echo $type;?>" required/>
                           <input type="hidden" class="form-control" name="purchase_document_number" value="<?php echo $purchase_document_number;?>" required/>
                           

                           <div class="form-group">
                               <label class="col-md-4 control-label">Amount *</label>

                               <div class="col-md-8">
                                   <input type="text" class="form-control" name="transacted_amount" placeholder="Amount" value="<?php echo $finance_purchase_amount?>" required/>
                               </div>
                           </div>
                              <div class="form-group" id="payment_method">
                                <label class="col-md-4 control-label">Month: </label>

                                <div class="col-md-8">
                                  <select class="form-control select2" name="purchase_month"   required>
                                    <?php echo $months_list;?>
                                  </select>
                                  </div>
                              </div>
                              <div class="form-group" id="payment_method">
                                <label class="col-md-4 control-label">Year: </label>
                                <div class="col-md-8">
                                  <select class="form-control select2" name="purchase_year"   required>

                                    <?php echo $year_list;?>
                                  </select>
                                  </div>
                              </div>
                        </div>
                     </div>
                 <br>
                 <div class="row">
                       <div class="col-md-12">
                           <div class="text-center">
                              <button type="submit" class="btn btn-sm btn-primary" onclick="return confirm('Are you sure you want to update this record ? ')">Update record</button>
                           </div>
                       </div>
                   </div>

               </div>
               <?php echo form_close();?>


            </div>
        </div>
    </div>
</div>
 <script type="text/javascript">
 		$( document ).ready(function() {
		    var type = <?php echo $creditor_id?>;
		    if(type > 0)
		    {
		    	type = 1;
		    }
		    else
		    {
		    	type = null;
		    }
		    get_transaction_type_list(type);
		});
        function get_transaction_type_list(type)
        {
            // var type = getRadioCheckedValue(radio_name);
            // $("#charge_to_id").customselect()="";
            // alert(type);
            
            var myTarget1 = document.getElementById("creditor_div");

            if(type == 1)
            {


                myTarget1.style.display = 'block';
                $('#creditor_id').addClass('select2');
            }
            else {
              myTarget1.style.display = 'none';
            }
            var url = "<?php echo site_url();?>accounting/petty_cash/get_list_type_petty_cash/1";
            // alert(url);
            //get department services
            $.get( url, function( data )
            {
                $( "#charge_to_id" ).html( data );
                // $(".custom-select").customselect();
            });

        }

        function getRadioCheckedValue(radio_name)
        {
           var oRadio = document.forms[0].elements[radio_name];

           for(var i = 0; i < oRadio.length; i++)
           {
              if(oRadio[i].checked)
              {
                 return oRadio[i].value;
              }
           }

           return '';
        }

        function display_payment_model(modal_id)
      	{
      		$('#modal-defaults'+modal_id).modal('show');
      		$('#datepicker1'+modal_id).datepicker({
    	      autoclose: true,
    	      format: 'yyyy-mm-dd',
    	    })
      	}


        function check_payment_type(payment_type_id){

          var myTarget1 = document.getElementById("cheque_div");

          var myTarget2 = document.getElementById("mpesa_div");

          var myTarget3 = document.getElementById("insuarance_div");

          if(payment_type_id == 1)
          {
            // this is a check

            myTarget1.style.display = 'block';
            myTarget2.style.display = 'none';
            myTarget3.style.display = 'none';
          }
          else if(payment_type_id == 2)
          {
            myTarget1.style.display = 'none';
            myTarget2.style.display = 'none';
            myTarget3.style.display = 'none';
          }
          else if(payment_type_id == 3)
          {
            myTarget1.style.display = 'none';
            myTarget2.style.display = 'none';
            myTarget3.style.display = 'block';
          }
          else if(payment_type_id == 4)
          {
            myTarget1.style.display = 'none';
            myTarget2.style.display = 'none';
            myTarget3.style.display = 'none';
          }
          else if(payment_type_id == 5)
          {
            myTarget1.style.display = 'none';
            myTarget2.style.display = 'block';
            myTarget3.style.display = 'none';
          }
          else
          {
            myTarget2.style.display = 'none';
            myTarget3.style.display = 'block';
          }

        }

    </script>
