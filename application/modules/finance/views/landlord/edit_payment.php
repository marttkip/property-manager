<!-- search -->
<?php //echo $this->load->view('search/search_petty_cash', '', TRUE);


	$transaction_rs = $this->landlord_model->get_transaction_details($landlord_transaction_id);

	if($transaction_rs->num_rows() > 0)
	{
	    foreach ($transaction_rs->result() as $key => $value) {
	      // code...
	      $document_number = $value->document_number;
	      $transaction_number = $value->transaction_number;
	      $transaction_date = $value->transaction_date;
	      $property_id = $value->property_id;
	      $landlord_transaction_id = $value->landlord_transaction_id;
	      $landlord_transaction_description = $value->remarks;
	      $landlord_transaction_amount = $value->landlord_transaction_amount;
	      $payment_method_id = $value->payment_method_id;
	       $bank_id = $value->bank_id;
	      $paymenttermid = $value->paymenttermid;
	      $payment_month = $value->month;

	      if($payment_month < 10)
	      {
	      	$payment_month = '0'.$payment_month;
	      }
	      $payment_year = $value->year;



	     }

	}
	// var_dump($payment_month);die();

    $properties = $this->property_model->get_active_property();
    $rs8 = $properties->result();
    $property_list = '';
    foreach ($rs8 as $property_rs) :
        $property_idd = $property_rs->property_id;
        $property_name = $property_rs->property_name;
        $property_location = $property_rs->property_location;
        if($property_id == $property_idd)
        {
        	$property_list .="<option value='".$property_idd."' selected>".$property_name." Location: ".$property_location."</option>";
        }
        else
        {
        	$property_list .="<option value='".$property_idd."'>".$property_name." Location: ".$property_location."</option>";
        }
        

    endforeach;


    $invoice_type_order = 'invoice_type.invoice_type_id';
    $invoice_type_table = 'invoice_type';
    $invoice_type_where = 'invoice_type.invoice_type_status = 1 AND generaljournalaccountid = 6';

    $invoice_type_query = $this->tenants_model->get_tenant_list($invoice_type_table, $invoice_type_where, $invoice_type_order);
    $rs8 = $invoice_type_query->result();
    $invoice_type_list = '';
    foreach ($rs8 as $invoice_rs) :
      $invoice_type_id = $invoice_rs->invoice_type_id;
      $invoice_type_name = $invoice_rs->invoice_type_name;
      	if($paymenttermid == $invoice_type_id)
      	{
      		 $invoice_type_list .="<option value='".$invoice_type_id."' selected>".$invoice_type_name."</option>";
      	}
      	else
      	{
      		 $invoice_type_list .="<option value='".$invoice_type_id."'>".$invoice_type_name."</option>";
      	}

       

    endforeach;

    $v_data['invoice_type_list'] = $invoice_type_list;

?>
<!-- end search -->
<!--begin the reports section-->
<?php
//unset the sessions set\

$month = $this->accounts_model->get_months();
$months_list = '<option value="0">Select a Type</option>';
foreach($month->result() as $res)
{
  $month_id = $res->month_id;
  $month_name = $res->month_name;
  if($month_id < 10)
  {
    $month_id = '0'.$month_id;
  }
  $month = date('M');

  if($payment_month == $month_id)
  {
    $months_list .= '<option value="'.$month_id.'" selected>'.$month_name.'</option>';
  }
  else {
    $months_list .= '<option value="'.$month_id.'">'.$month_name.'</option>';
  }



}


$start = 2015;
$end_year = 2030;
$year_list = '<option value="0">Select a Type</option>';
for ($i=$start; $i < $end_year; $i++) {
  // code...
  $year= date('Y');

  if($payment_year == $i)
  {
    $year_list .= '<option value="'.$i.'" selected>'.$i.'</option>';
  }
  else {
    $year_list .= '<option value="'.$i.'">'.$i.'</option>';
  }
}
?>
<!--end reports -->
<div class="row">
    
    <div class="col-md-12">
      <div class="box box-success">
               <div class="box-header with-border">
                 <h3 class="box-title"><?php echo $title;?></h3>

                 <div class="box-tools pull-right">
                 	<a href="<?php echo base_url().'accounting/landlord-payments';?>" class="btn btn-sm btn-warning"><i class="fa fa-arrow-left"></i> Back to landlord payments </a>
                 </div>
               </div>
               <div class="box-body">
            

             <?php echo form_open("finance/landlord/update_landlord_transaction/".$landlord_transaction_id, array("class" => "form-horizontal"));?>
               <div class="row">
                 <div class="col-md-12">
                 <div class="col-md-6">
                       <div class="form-group">
                           <label class="col-lg-4 control-label">Property </label>

                           <div class="col-lg-8">
                              <select  name='property_id' class='form-control select2' required="required">
                                 <option value=''>None - Please Select a property</option>
                                 <?php echo $property_list;?>
                               </select>
                           </div>
                       </div>



                       <div class="form-group" style="display:none">
                           <label class="col-md-4 control-label">Type *</label>

                           <div class="col-md-8">
                               <div class="radio">
                                   <label>
                                       <input  type="radio"  value="3" name="transaction_type_id" id="account_to_type" onclick="get_transaction_type_list(this.value)" checked="checked">
                                       Payments
                                   </label>
                               </div>
                           </div>
                       </div>


                       <div class="form-group" id="property_div" style="display:none;">
                           <label class="col-lg-4 control-label">Property To</label>

                           <div class="col-lg-8">
                              <select  name='property_to_id' class='form-control select2' >
                                 <option value=''>None - Please Select a property</option>
                                 <?php echo $property_list;?>
                               </select>
                           </div>
                       </div>

                       <div class="form-group" >
                           <label class="col-md-4 control-label">Type Account *</label>

                           <div class="col-md-8">
                                <select id='invoice_type_id' name='invoice_type_id' class='form-control select2 ' required="required">
                                 <option value=''>None - Please Select an invoice type</option>
                                 <?php echo $invoice_type_list;?>
                               </select>
                           </div>
                       </div>
                       <div class="form-group">
                           <label class="col-md-4 control-label">Description *</label>

                           <div class="col-md-8">
                               <textarea class="form-control" name="description" required><?php echo $landlord_transaction_description;?></textarea>
                           </div>
                       </div>

                       <div class="form-group" id="payment_method">
                         <label class="col-md-4 control-label">Payment Method: </label>

                         <div class="col-md-7">
                           <select class="form-control" name="payment_method" onchange="check_payment_type(this.value)" required>
                             <option value="0">Select a payment method</option>
                                                   <?php
                               $method_rs = $this->accounts_model->get_payment_methods();

                               foreach($method_rs->result() as $res)
                               {
                                 $payment_method_idd = $res->payment_method_id;
                                 $payment_method = $res->payment_method;
                                 if($payment_method_id == $payment_method_idd)
                                 {
                                 	echo '<option value="'.$payment_method_id.'" selected>'.$payment_method.'</option>';
                                 }
                                 else
                                 {
                                 	echo '<option value="'.$payment_method_id.'">'.$payment_method.'</option>';
                                 }

                                 

                               }

                             ?>
                           </select>
                           </div>
                       </div>
                       <div id="cheque_div" class="form-group" style="display:none;" >
                         <div class="form-group" >
                           <label class="col-md-4 control-label"> Bank: </label>

                           <div class="col-md-7">
                             <select class="form-control " name="bank_id"  required>
                               <option value="0">Select a bank</option>
                                                     <?php
                                 $bank_rs = $this->accounts_model->get_bank_accounts();

                                 foreach($bank_rs->result() as $res)
                                 {
                                   $id = $res->id;
                                   $name = $res->name;

                                   if($bank_id == $id)
                                   {
                                   	 echo '<option value="'.$id.'" selected>'.$name.'</option>';
                                   }
                                   else
                                   {
                                   	echo '<option value="'.$id.'">'.$name.'</option>';
                                   }

                                   

                                 }

                               ?>
                             </select>
                           </div>
                         </div>
                       </div>





                 </div>
                 <div class="col-md-6">
                   <div class="form-group">
                       <label class="col-md-4 control-label">Transaction No / Cheque *</label>

                       <div class="col-md-8">
                           <input type="text" class="form-control" name="transaction_number" placeholder="Transaction Number" value="<?php echo $transaction_number?>" required/>
                       </div>
                   </div>
                    <input type="hidden" class="form-control" name="document_number" placeholder="Transaction Number" value="<?php echo $document_number?>" required/>
                   <div class="form-group">
                       <label class="col-md-4 control-label">Amount *</label>

                       <div class="col-md-8">
                           <input type="text" class="form-control" name="transacted_amount" value="<?php echo $landlord_transaction_amount?>" placeholder="Amount" required/>
                       </div>
                   </div>

                   <div class="form-group" id="payment_method">
                     <label class="col-md-4 control-label">Month: </label>

                     <div class="col-md-7">
                       <select class="form-control select2" name="month"   required>
                         <?php echo $months_list;?>
                       </select>
                       </div>
                   </div>
                   <div class="form-group" id="payment_method">
                     <label class="col-md-4 control-label">Year: </label>
                     <div class="col-md-7">
                       <select class="form-control select2" name="year"   required>

                         <?php echo $year_list;?>
                       </select>
                       </div>
                   </div>
                   <div class="form-group">
                       <label class="col-md-4 control-label">Transaction date: </label>

                       <div class="col-md-8">
                           <div class="input-group">
                               <span class="input-group-addon">
                                   <i class="fa fa-calendar"></i>
                               </span>
                               <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="transaction_date" placeholder="Transaction date" value="<?php echo $transaction_date?>" id="datepicker2" required>
                           </div>
                       </div>
                   </div>
                 </div>
                 </div>
                 <br>
                 <div class="row">
                       <div class="col-md-12">
                           <div class="text-center">
                               <button type="submit" class="btn btn-sm btn-primary" onclick="return confirm('Are you sure you want to update this record ?')">Update record</button>
                           </div>
                       </div>
                   </div>

               </div>
               <?php echo form_close();?>


            </div>
        </div>
    </div>
</div>


 <script type="text/javascript">

 		$( document ).ready(function() {
		    var type = <?php echo $payment_method_id?>;
		   // alert(type);
		    check_payment_type(type);
		});

        function get_transaction_type_list(type)
        {
            var myTarget1 = document.getElementById("property_div");

            if(type == 4)
            {
                myTarget1.style.display = 'block';
                $('#property_id_to').addClass('select2');
            }

            var url = "<?php echo site_url();?>accounting/petty_cash/get_list_type_petty_cash/"+type;
            // alert(url);
            //get department services
            $.get( url, function( data )
            {
                $( "#account_to_id" ).html( data );
                // $(".custom-select").customselect();
            });

        }

        function getRadioCheckedValue(radio_name)
        {
           var oRadio = document.forms[0].elements[radio_name];

           for(var i = 0; i < oRadio.length; i++)
           {
              if(oRadio[i].checked)
              {
                 return oRadio[i].value;
              }
           }

           return '';
        }

        function display_payment_model(modal_id)
      	{
      		$('#modal-defaults'+modal_id).modal('show');
      		$('#datepicker1'+modal_id).datepicker({
    	      autoclose: true,
    	      format: 'yyyy-mm-dd',
    	    })
      	}

        function check_payment_type(payment_type_id){

          var myTarget1 = document.getElementById("cheque_div");

          var myTarget2 = document.getElementById("mpesa_div");

          var myTarget3 = document.getElementById("insuarance_div");
          // alert(payment_type_id);
          if(payment_type_id == 1)
          {
            // this is a cash

            myTarget1.style.display = 'none';
            myTarget2.style.display = 'none';
            myTarget3.style.display = 'none';
          }
          else if(payment_type_id == 2 || payment_type_id == 3 || payment_type_id == 5)
          {
            // cheque
            myTarget1.style.display = 'block';
            myTarget2.style.display = 'none';
            myTarget3.style.display = 'none';
          }

          else
          {
            myTarget1.style.display = 'none';
            myTarget2.style.display = 'none';
            myTarget3.style.display = 'none';
          }

        }


    </script>
