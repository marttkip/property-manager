<?php

$company_name = $contacts['company_name'];
$payment_result ='';
$finance_purchase_amount_total = 0;
if($query_purchases->num_rows() > 0)
{
  $x=0;
  foreach ($query_purchases->result() as $key => $value) {
    // code...
    $x++;
    $document_number = $value->document_number;
	$transaction_number = $value->transaction_number;
	$transaction_date = $value->transaction_date;
	$property_name = $value->property_owner_name;
	$property_id = $value->property_id;
	$account_name = $value->account_name;
	$landlord_transaction_id = $value->landlord_transaction_id;
	$landlord_transaction_description = $value->remarks;
	$landlord_transaction_amount = $value->landlord_transaction_amount;
	$payment_method = $value->payment_method;
	$name = $value->name;

    $finance_purchase_amount_total += $landlord_transaction_amount;

    $payment_result .= '<tr>
                            <td>'.$x.'</td>
                            <td>'.$property_name.'</td>
                            <td>'.$landlord_transaction_description.'</td>
                            <td>'.number_format($landlord_transaction_amount,2).'</td>
                        </tr>';
  }
  $payment_result .= '<tr>
                          <th colspan="4">PAYMENT DONE: '.$payment_method.' '.$name.' '.$transaction_number.'</th>
                      </tr>';
  $payment_result .= '<tr>
                          <th></th>
                          <th></th>
                          <th>TOTAL</th>
                          <th>'.number_format($finance_purchase_amount_total,2).'</th>
                      </tr>';
}
$served_by = $this->accounts_model->get_personnel($this->session->userdata('personnel_id'));
?>

<!DOCTYPE html>
<html lang="en">
      <head>
        <title><?php echo $contacts['company_name'];?> | Landlord Payment</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" media="all"/>
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css" media="all"/>
        <style type="text/css">
			.receipt_spacing{letter-spacing:0px;}
			.center-align{margin:0 auto; text-align:center;}

			.receipt_bottom_border{border-bottom: #000 medium solid; margin-bottom:1px;}
			.title-img{float:left; padding-left:30px;}
			img.logo{max-height:70px; margin:0 auto;}
			.align-right{margin:0 auto; text-align: right !important;}
			.align-center
			{
				/*padding: 50px;*/
    			text-align: center;
    			font-weight: bolder;
			}
			.table {
			  font-size: 15px !important;
			  margin-bottom: 0px !important;
			  max-width: 100%;
			  width: 100%;

			}
			.row {
  				margin-left: 0px !important;
    			margin-right: 0px !important;
			}
			.table-condensed > thead > tr > th, .table-condensed > tbody > tr > th, .table-condensed > tfoot > tr > th, .table-condensed > thead > tr > td, .table-condensed > tbody > tr > td, .table-condensed > tfoot > tr > td {
			  padding: 1px !important;

			}
			table tr, th, td
			{
				/* border: #000 solid 2px !important; */
				padding: 2px !important;
			}
			h3, .h3 {
			  font-size: 13px !important;
			}
      table
      {
        border: #000 solid 2px !important;
      }
      @media print {
          html, body {
              /*width: 5.5in; /* was 8.5in */
              height: 8.5in; /* was 5.5in */
              display: block;
              font-family: "Calibri";
              /*font-size: auto; NOT A VALID PROPERTY */
          }

          @page {
             /* size: 8.5in 8.5in*/ /* . Random dot? */;
          }

          

      }
		</style>
    </head>
     <body class="receipt_spacing">

		 <div class="row " >

				 <div class="col-md-12 center-align">
						 <?php echo $company_name;?><br>
              <?php echo $contacts['location'];?>,<br/>
               Address : P.O. Box <?php echo $contacts['address'];?> <?php echo $contacts['post_code'];?>, <?php echo $contacts['city'];?> <br/>
               Phone:  <?php echo $contacts['phone'];?>
					 </div>
			 </div>


        <!-- Table row -->
        <div class="row" style="margin-top:10px;">
          <div class="col-md-12 table-responsive">
						<table class="table table-hover table-bordered">
							<thead>
				      <tr>
				        <td><strong> Date : </strong><?php echo $transaction_date?><br> <strong> Voucher No : </strong><?php echo strtoupper($document_number);?><br><strong> Served By: </strong> <?php echo $served_by;?></td>
				      </tr>
				      </thead>
        	  	<tbody>
							</tbody>
						</table>
            <table class="table table-hover table-bordered">
							<thead>
				      <tr>
				        <th>#</th>
						<th>Property</th>
		                <th>Memo</th>
		                <th>Amount</th>
				      </tr>
				      </thead>
        	  	<tbody>
                <?php echo $payment_result;?>

        	  	</tbody>
        	</table>
          </div>
          <!-- /.col -->
        </div>
        <div class="row" style="margin-top:25px;">
          <div class="col-md-12 ">

            Sign ..................................................................   Date ..................................................
          </div>
        </div>
        <!-- /.row -->
     </body>
</html>
