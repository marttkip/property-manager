<section class="panel panel-featured panel-featured-info">
    <header class="panel-heading">
      <h2 class="panel-title pull-right">Active branch: <?php echo $branch_name;?></h2>
      <h2 class="panel-title">Search</h2>
    </header>

  <!-- Widget content -->
        <div class="panel-body">
<?php
    echo form_open("search-transactions", array("class" => "form-horizontal"));
    ?>
    <div class="row">
        <div class="col-md-4">
          <div class="form-group">
              <label class="col-lg-4 control-label">Payment Date From: </label>

              <div class="col-lg-8">
                <div class="input-group">
                      <span class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                      </span>
                      <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker id="datepicker1" class="form-control" name="payment_date_from" placeholder="Report Date From">
                  </div>
              </div>
          </div>

        </div>

        <div class="col-md-4">



            <div class="form-group">
                <label class="col-lg-4 control-label">Payment Date To: </label>

                <div class="col-lg-8">
                  <div class="input-group">
                        <span class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </span>
                        <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker id="datepicker" class="form-control" name="payment_date_to" placeholder="Report Date To">
                    </div>
                </div>
            </div>

        </div>

        <div class="col-md-4">
            <div class="form-group">
                <div class="col-lg-8 col-lg-offset-4">
                  <div class="center-align">
                      <button type="submit" class="btn btn-info">Search</button>
            </div>
                </div>
            </div>
        </div>
    </div>


    <?php
    echo form_close();
    ?>
  </div>
</section>
