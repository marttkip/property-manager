<?php
$property_owner_where = 'property_owner_id > 0' ;
$property_owner_table = 'property_owners';
$property_owner_order = 'property_owner_id';

$property_owner_query = $this->reports_model->get_list_items($property_owner_table, $property_owner_where, $property_owner_order);
$rs8 = $property_owner_query->result();
$property_owner_list = '';
foreach ($rs8 as $property_owner_rs) :
  $property_owner_id = $property_owner_rs->property_owner_id;
  $property_owner_name = $property_owner_rs->property_owner_name;

    $property_owner_list .="<option value='".$property_owner_id."'>".$property_owner_name."</option>";

endforeach;

?>
<div class="col-md-12">
    <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"><?php echo $title;?></h3>

          <div class="box-tools pull-right">

          </div>
        </div>
        <div class="box-body">
            <!-- select a tenant  -->
            <div class="row" style="margin-bottom:20px;">
              <?php echo form_open("search-landlord-report", array("class" => "form-horizontal", "role" => "form"));?>
                  <div class="row">
                    <div class="col-md-12">
                          <div class="col-md-8">
                                <div class="form-group center-align">
                                  <label class="col-md-4 control-label">Landlord Name: </label>

                                  <div class="col-md-8">
                                    <select id='landlord_id' name='landlord_id' class='form-control select2'>
                                        <!-- <select class="form-control custom-select " id='procedure_id' name='procedure_id'> -->
                                          <option value=''>None - Please Select a landlord</option>
                                          <?php echo $property_owner_list;?>
                                        </select>
                                  </div>
                              </div>
                          </div>

                          <div class="col-md-4">
                          <div class="form-actions center-align">
                              <button class="submit btn btn-primary btn-sm" type="submit">
                                  Search tenant
                              </button>
                          </div>
                        </div>

                    </div>
                  </div>
                <?php echo form_close();?>
            </div>
        </div>
    </div>
  </div>
