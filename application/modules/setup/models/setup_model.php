<?php

class Setup_model extends CI_Model 
{	
	
	/*
	*	Retrieve all customers
	*	@param string $table
	* 	@param string $where
	*
	*/
	public function get_all_properties($table, $where, $per_page, $page, $order = 'property_name', $order_method = 'ASC')
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}
	

	/*
	*	Retrieve all uploads
	*	@param string $table
	* 	@param string $where
	*
	*/
	public function get_all_uploads($table, $where, $per_page, $page, $order = 'upload_id', $order_method = 'ASC')
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}
	


/*
	*	Retrieve all uploads
	*	@param string $table
	* 	@param string $where
	*
	*/
	public function get_all_complaints($table, $where, $per_page, $page, $order = 'complaint_id', $order_method = 'ASC')
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}
		public function add_property()
	{
		
		$data = array(

				'property_name'=>$this->input->post('property_name'),
				'property_location'=>$this->input->post('property_location'),
				'total_units'=>$this->input->post('total_units'),	
				'property_status'=>1


				#'created_by'=>$this->session->userdata('personnel_id'),
				#'modified_by'=>$this->session->userdata('personnel_id')
			);
			
		if($this->db->insert('property', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}	
	
	/*
	*	Update an existing customer
	*	@param string $image_name
	*	@param int $customer_id
	*
	*/
	public function update_property($property_id)
	{
		$data = array(

				
				'property_name'=>$this->input->post('property_name'),
				'property_location'=>$this->input->post('property_location'),
				'total_units'=>$this->input->post('total_units')	
				
			);
			
		$this->db->where('property_id', $property_id);
		if($this->db->update('property', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	/*
	*	get a single customer's details
	*	@param int $customer_id
	*
	*/
	public function get_property($property_id)
	{
		//retrieve all users
		$this->db->from('property');
		$this->db->select('*');
		$this->db->where('property_id = '.$property_id);
		$query = $this->db->get();
		
		return $query;
	}
	

	public function activate_property($property_id)
	{
		$data = array(
				'property_status' => 1
			);
		$this->db->where('property_id', $property_id);
		
		if($this->db->update('property', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	/*
	*	Deactivate an activated customer
	*	@param int $customer_id
	*
	*/
	public function deactivate_property($property_id)
	{
		$data = array(
				'property_status' => 0
			);
		$this->db->where('property_id', $property_id);
		
		if($this->db->update('property', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	public function get_all_units($table, $where, $per_page, $page, $order = 'rental_unit.rental_unit_id', $order_method = 'ASC')
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		//$this->db->order_by($order, $order_method);
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}
	public function add_unit($property_id)
	{
		
		$data = array(

				'rental_unit_name'=>$this->input->post('unit_name'),
				'rental_unit_size'=>$this->input->post('unit_size'),
				'rental_unit_status'=>1,
				'property_id'=>$property_id


				#'created_by'=>$this->session->userdata('personnel_id'),
				#'modified_by'=>$this->session->userdata('personnel_id')
			);
			
		if($this->db->insert('rental_unit', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}	


public function get_all_water_meters($table, $where, $per_page, $page, $order = 'rental_unit.rental_unit_id', $order_method = 'ASC')
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		//$this->db->order_by($order, $order_method);
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}
	public function deactivate_meter($rental_unit_id)
	{
		$data = array(
				'meter_status' => 0
			);
		$this->db->where('rental_unit_id', $rental_unit_id);
		
		if($this->db->update('water_meter', $data))
		{
			return 1;
		}
		else{
			return 0;
		}
	}

	public function add_meter($rental_unit_id)
	{
		if($this->deactivate_meter($rental_unit_id) == 1){
			$data = array(

				'meter_number'=>$this->input->post('meter_number'),
				'meter_status'=>1,
				'rental_unit_id'=>$rental_unit_id


				#'created_by'=>$this->session->userdata('personnel_id'),
				#'modified_by'=>$this->session->userdata('personnel_id')
			);
			
		if($this->db->insert('water_meter', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}

		}

		
		
	}
	public function get_all_tenants($table, $where, $per_page, $page, $order = 'tenants.tenant_name', $order_method = 'ASC')
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		//$this->db->order_by($order, $order_method);
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}	
	public function add_tenant()
	{
		$tenant_surname = $this->input->post('customer_surname');
		$tenant_firstname = $this->input->post('customer_first_name');
		$tenant_name = $tenant_firstname.' '.$tenant_surname;

		
		$data = array(

				'tenant_name'=>$tenant_name,
				
				'tenant_phone_number'=>$this->input->post('customer_phone'),
				'tenant_email'=>$this->input->post('customer_email'),
				'tenant_postal_code'=>$this->input->post('customer_post_code'),
				'tenant_national_id'=>$this->input->post('customer_address'),
				'created'=>date('Y-m-d H:i:s'),
				'tenant_status'=>1


				#'created_by'=>$this->session->userdata('personnel_id'),
				#'modified_by'=>$this->session->userdata('personnel_id')
			);
			
		if($this->db->insert('tenants', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}

	public function add_tenant_uploads($file_name,$tenant_id)
	{
		$document_name = $this->input->post('document_name');
		$arrayName = array(
							'document_name'=>$document_name,
							'upload_document_name' => $file_name,
							'tenant_id' =>$tenant_id,
							'created_by' => $this->session->userdata('personnel_id'),
							'created' => date('Y-m-d'),

							);
		if($this->db->insert('tenant_uploads',$arrayName))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}


	}

	public function add_tenant_complaint($tenant_id)
	{
		$complaint = $this->input->post('complaint');
		$arrayName = array(
							'complaint'=>$complaint,
							'tenant_id' =>$tenant_id,
							'created_by' => $this->session->userdata('personnel_id'),
							'created' => date('Y-m-d'),

							);
		if($this->db->insert('tenant_complaints',$arrayName))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}


	}
	public function get_personnel_name($created_by)
	{
		$this->db->where('personnel_id = '.$created_by);
		$query = $this->db->get('personnel');
		$personnel_fname = '-';
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key) {
				# code...
				$personnel_fname = $key->personnel_fname;
			}
		}
		return $personnel_fname;
	}
	public function get_all_unit_tenant($rental_unit_id){
		$tenant_id=0;
		$this->db->select('tenant_id');
		$this->db->where('lease_status = 1 and rental_unit_id = '.$rental_unit_id);
		$query = $this->db->get('leases');
		$tenant_name = FALSE;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key) {
				# code...
				$tenant_id = $key->tenant_id;
					
				$tenant_name = $this->get_tenant_name($tenant_id);
						
			}
		}
		
	
		return $tenant_name;

	}
	public function get_tenant_name($tenant_id)
	{
	 $tenant_name="";
		$this->db->select('tenant_name');
		$this->db->where('tenant_id = '.$tenant_id);
		$query = $this->db->get('tenants');
		$total = $query->row();
		$tenant_name = $total->tenant_name;
		return $tenant_name;
	}
	public function all_tenants()
	{
		$this->db->where('tenant_status > 0');
		$query = $this->db->get('tenants');
		
		return $query;
	}
	public function add_lease($rental_unit_id)
	{

		$lease_start_date = $this->input->post('lease_start_date');
		$lease_end_date = $this->input->post('lease_end_date');
		$tenant_id = $this->input->post('tenant_id');

	 $rent_amount = $this->input->post('rent_amount');
		$rent_amount_period = $this->input->post('rent_amount_period');
		$rent_deposit = $this->input->post('rent_deposit');
		$water_deposit = $this->input->post('water_deposit');
		$electricity_deposit = $this->input->post('electricity_deposit');
		
		$water_charges = $this->input->post('water_charges');
		$water_charges_period = $this->input->post('water_charges_period');
		$security_charges = $this->input->post('security_charges');
		$security_charges_period = $this->input->post('security_charges_period');
		$garbage_charges = $this->input->post('garbage_charges');
		$garbage_charges_period = $this->input->post('garbage_charges_period');

		$electricity_arrears = $this->input->post('electricity_arrears');
		$water_arrears = $this->input->post('water_arrears');
		$security_arrears = $this->input->post('security_arrears');
		$garbage_arrears = $this->input->post('garbage_arrears');
		$rent_arrears = $this->input->post('rent_arrears');


		
		$data = array(

				'tenant_id'=>$tenant_id,
				'lease_start_date'=>$this->input->post('lease_start_date'),
				'lease_end_date'=>$this->input->post('lease_end_date'),

				'rent_amount'=>$this->input->post('rent_amount'),
				'rent_amount_period'=>$this->input->post('rent_amount_period'),
				'rent_deposit'=>$this->input->post('rent_deposit'),
				'water_deposit'=>$this->input->post('water_deposit'),
				'electricity_deposit'=>$this->input->post('electricity_deposit'),

				'water_charges'=>$this->input->post('water_charges'),
				'water_charges_period'=>$this->input->post('water_charges_period'),
				'security_charges'=>$this->input->post('security_charges'),
				'security_charges_period'=>$this->input->post('security_charges_period'),
				'garbage_charges'=>$this->input->post('garbage_charges'),
				'garbage_charges_period'=>$this->input->post('garbage_charges_period'),

				'electricity_arrears'=>$this->input->post('electricity_arrears'),
				'water_arrears'=>$this->input->post('water_arrears'),
				'security_arrears'=>$this->input->post('security_arrears'),
				'garbage_arrears'=>$this->input->post('garbage_arrears'),
				'rent_arrears'=>$this->input->post('rent_arrears'),
		
				
				'created'=>date('Y-m-d H:i:s'),
				'rental_unit_id'=>$rental_unit_id,
				'lease_status'=>1


				#'created_by'=>$this->session->userdata('personnel_id'),
				#'modified_by'=>$this->session->userdata('personnel_id')
			);
			
		if($this->db->insert('leases', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}

	public function get_invoice_types()
	{
		//retrieve all users
		$this->db->from('invoice_type');
		$this->db->select('*');
		$this->db->where('invoice_type_status > 0');
		$query = $this->db->get('');
		
		return $query;
	}
	public function add_invoice_type()
	{

		$invoice_type_name = $this->input->post('invoice_type_name');
		$invoice_type_code = $this->input->post('invoice_type_code');
		

		
		$data = array(

				'invoice_type_name'=>$invoice_type_name,
				'invoice_type_code'=>$this->input->post('invoice_type_code'),
				'invoice_type_status'=>1


				#'created_by'=>$this->session->userdata('personnel_id'),
				#'modified_by'=>$this->session->userdata('personnel_id')
			);
			
		if($this->db->insert('invoice_type', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	public function get_billing_schedule()
	{
		//retrieve all users
		$this->db->from('billing_schedule');
		$this->db->select('*');
		$this->db->where('billing_schedule_status > 0');
		$query = $this->db->get('');
		
		return $query;
	}
	public function add_billing_schedule()
	{

		$billing_schedule_name = $this->input->post('billing_schedule_name');
		
		
		$data = array(

				'billing_schedule_name'=>$billing_schedule_name,
				'billing_schedule_status'=>1


				#'created_by'=>$this->session->userdata('personnel_id'),
				#'modified_by'=>$this->session->userdata('personnel_id')
			);
			
		if($this->db->insert('billing_schedule', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}

	
}

?>