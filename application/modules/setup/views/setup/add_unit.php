<?php
//personnel data
$unit_name = set_value('unit_name');
$unit_size = set_value('unit_size');


?>   
          <section class="panel">
                <header class="panel-heading">
                    <h2 class="panel-title"><?php echo $title;?></h2>
                </header>
                <div class="panel-body">
                    <div class="row" style="margin-bottom:20px;">
                        <div class="col-lg-12">
                            <a href="<?php echo site_url();?>setup/property" class="btn btn-info pull-right">Back to Property</a>
                        </div>
                    </div>
                        
                    <!-- Adding Errors -->
                    <?php
                        $success = $this->session->userdata('success_message');
                        $error = $this->session->userdata('error_message');
                        
                        if(!empty($success))
                        {
                            echo '
                                <div class="alert alert-success">'.$success.'</div>
                            ';
                            
                            $this->session->unset_userdata('success_message');
                        }
                        
                        if(!empty($error))
                        {
                            echo '
                                <div class="alert alert-danger">'.$error.'</div>
                            ';
                            
                            $this->session->unset_userdata('error_message');
                        }
                        $validation_errors = validation_errors();
                        
                        if(!empty($validation_errors))
                        {
                            echo '<div class="alert alert-danger"> Oh snap! '.$validation_errors.' </div>';
                        }
                    ?>
                    
                    <?php echo form_open($this->uri->uri_string(), array("class" => "form-horizontal", "role" => "form"));?>
<div class="row">
    <div class="col-md-6">
          <div class="form-group">
            <label class="col-lg-5 control-label">Unit Name: </label>
            
            <div class="col-lg-7">
                <input type="text" class="form-control" name="unit_name" placeholder="Property Name" value="<?php echo $unit_name;?>">
            </div>
        </div>
        
        
     
         
        
    </div>
    
    <div class="col-md-6">
        
       
        
        <div class="form-group">
            <label class="col-lg-5 control-label">Unit Size: </label>
            
            <div class="col-lg-7">
                <input type="text" class="form-control" name="unit_size" placeholder="Total Units" value="<?php echo $unit_size;?>">
            </div>
        </div>
        
       
    

    </div>
</div>
<div class="row" style="margin-top:10px;">
    <div class="col-md-12">
        <div class="form-actions center-align">
            <button class="submit btn btn-primary" type="submit">
                Add Unit
            </button>
        </div>
    </div>
</div>
                    <?php echo form_close();?>
                </div>
            </section>