<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// require_once "./application/modules/auth/controllers/auth.php";
class Sms  extends MX_Controller
{	
	function __construct()
	{
		parent:: __construct();
		$this->load->model('sms_model');
	}
	
	public function send_sms()

	{
		//if form has been submitted
		if ($this->form_validation->run())
		{
			$api_key = 1000;

			if($this->sms_model->validateApi_Key($api_key)){
			$sender_id = 1;
			$message = $this->input->post("message");
			$phone_number = $this->input->post("phone_number");
             /// This means go to SMS model find function Sms
			$result = $this->sms_model->sms($message, $api_key,$phone_number,$sender_id);

			}else{
                $response['result'] = FALSE;
				$response['message'] = "API Key Not Found...";
			}
			
	

		}
		
		else
		{
		 
		        // Validation Error gives errors based on validation rules
			    $response['result'] = FALSE;
				$response['message'] = validation_errors();
		}
		
	}
	
}
?>