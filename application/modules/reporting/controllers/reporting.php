<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');
error_reporting(E_ALL);
// require_once "./application/modules/accounts/controllers/accounts.php";

class Reporting  extends MX_Controller
{
	var $attachments_path;
	function __construct()
	{
		parent:: __construct();
		$this->load->model('reporting/reporting_model');
    $this->load->model('admin/dashboard_model');
    $this->load->model('administration/reports_model');
    $this->load->model('site/site_model');
    $this->load->model('admin/email_model');
    $this->load->model('finance/purchases_model');
		$this->attachments_path = realpath(APPPATH . '../assets/attachments');
	}

	function daily_report()
	{


		// var_dump($account); die();
		$v_data['contacts'] = $this->site_model->get_contacts();


		$where =  '((v_account_ledger_by_date.transactionClassification = "Purchase Payment"
									OR v_account_ledger_by_date.transactionCategory = "Transfer"
									OR v_account_ledger_by_date.transactionCategory = "Income"
									OR v_account_ledger_by_date.transactionCategory = "Landlord Payment")
									 AND v_account_ledger_by_date.accountName = "Petty Cash" ) ';	

		$add7days = date('Y-m-d');
		$where .= ' AND v_account_ledger_by_date.transactionDate BETWEEN \''.$add7days.'\' AND \''.date('Y-m-d').'\'';
		$transaction_date = date('jS M Y');

		$todays_date = date('jS M Y',strtotime(date('Y-m-d')));
		$search_title = 'Expenses for '.$transaction_date.'';
	
		$table = 'v_account_ledger_by_date';

		$v_data['search_title'] = $search_title;
		$v_data['query_purchases'] = $this->purchases_model->get_petty_cash($where, $table);
		$v_data['title'] = $search_title;
		$html = $this->load->view('finance/purchases/print_petty_cash', $v_data,true);

		$this->load->library('mpdf');
		$title = date("ymdhis").'Expenses.pdf';

		$mpdf=new mPDF();
		$mpdf->WriteHTML($html);
		$mpdf->Output($title, 'F');

		while(!file_exists($title))
		{

		}

		$payslip = $title;
		


		$date_tomorrow = date('Y-m-d');
		$visit_date = date('jS M Y',strtotime($date_tomorrow));
		$branch = $this->config->item('branch_name');
		$message['subject'] = $subject =  $visit_date.' REPORT';
		// var_dump($date_tomorrow); die();
    	$v_data['transaction_date'] = $date_tomorrow;
		$text =  $this->load->view('transactions_report', $v_data,true);
		echo $text; die();
		$message['text'] =$text;
		$contacts = $this->site_model->get_contacts();
		$sender_email =$this->config->item('sender_email');//$contacts['email'];
		$shopping = "";
		$from = $sender_email;

		$button = '';
		$sender['email']= $sender_email;
		$sender['name'] = $contacts['company_name'];
		$receiver['name'] = $subject;
		// $payslip = $title;

		$sender_email = $sender_email;
		$tenant_email = $this->config->item('recepients_email');

		$email_array = explode('/', $tenant_email);
		$total_rows_email = count($email_array);
    	// echo $tenant_email; die();
		for($x = 0; $x < $total_rows_email; $x++)
		{
			$receiver['email'] = $email_tenant = $email_array[$x];
			$this->email_model->send_sendgrid_mail($receiver, $sender, $message, $payslip);
			unlink($payslip);
		}

    echo "<script>window.close();</script>";

 }

 	function daily_report_new()
	{


		// var_dump($account); die();
		$v_data['contacts'] = $this->site_model->get_contacts();


		$where =  '((v_account_ledger_by_date.transactionClassification = "Purchase Payment"
									OR v_account_ledger_by_date.transactionCategory = "Transfer"
									OR v_account_ledger_by_date.transactionCategory = "Income"
									OR v_account_ledger_by_date.transactionCategory = "Landlord Payment")
									 AND v_account_ledger_by_date.accountName = "Petty Cash" ) ';	

		$add7days = date('Y-m-d');
		$where .= ' AND v_account_ledger_by_date.transactionDate BETWEEN \''.$add7days.'\' AND \''.date('Y-m-d').'\'';
		$transaction_date = date('jS M Y');

		$todays_date = date('jS M Y',strtotime(date('Y-m-d')));
		$search_title = 'Expenses for '.$transaction_date.'';
	
		$table = 'v_account_ledger_by_date';

		$v_data['search_title'] = $search_title;
		$v_data['query_purchases'] = $this->purchases_model->get_petty_cash($where, $table);
		$v_data['title'] = $search_title;
		$html = $this->load->view('finance/purchases/print_petty_cash', $v_data,true);

		$this->load->library('mpdf');
		$title = date("ymdhis").'Expenses.pdf';

		$mpdf=new mPDF();
		$mpdf->WriteHTML($html);
		$mpdf->Output($title, 'F');

		while(!file_exists($title))
		{

		}

		$payslip = $title;
		


		$date_tomorrow = date('Y-m-d');
		$visit_date = date('jS M Y',strtotime($date_tomorrow));
		$branch = $this->config->item('branch_name');
		$message['subject'] = $subject =  $visit_date.' REPORT';
		// var_dump($date_tomorrow); die();
    	$v_data['transaction_date'] = $date_tomorrow;
		$text =  $this->load->view('transactions_report_new', $v_data,true);
		echo $text; die();
		$message['text'] =$text;
		$contacts = $this->site_model->get_contacts();
		$sender_email =$this->config->item('sender_email');//$contacts['email'];
		$shopping = "";
		$from = $sender_email;

		$button = '';
		$sender['email']= $sender_email;
		$sender['name'] = $contacts['company_name'];
		$receiver['name'] = $subject;
		// $payslip = $title;

		$sender_email = $sender_email;
		$tenant_email = $this->config->item('recepients_email');

		$email_array = explode('/', $tenant_email);
		$total_rows_email = count($email_array);
    	// echo $tenant_email; die();
		for($x = 0; $x < $total_rows_email; $x++)
		{
			$receiver['email'] = $email_tenant = $email_array[$x];
			$this->email_model->send_sendgrid_mail($receiver, $sender, $message, $payslip);
			unlink($payslip);
		}

    echo "<script>window.close();</script>";

 }
}

?>
