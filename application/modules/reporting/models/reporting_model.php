<?php
class Reporting_model extends CI_Model
{

  	/*
  	*	Retrieve total revenue
  	*
  	*/
  	public function get_total_cash_collection($where, $table, $page = NULL)
  	{
  		//payments
  		$table_search = $this->session->userdata('all_transactions_tables');

  		if($page != 'cash')
  		{
  			$where .= ' AND payments.cancel = 0';
  		}
  		if((!empty($table_search)) || ($page == 'cash'))
  		{
  			$this->db->from($table);
  		}

  		else
  		{
  			$this->db->from($table);
  		}
  		$this->db->select('SUM(payments.amount_paid) AS total_paid');
  		$this->db->where($where);
  		$query = $this->db->get();

  		$cash = $query->row();
  		$total_paid = $cash->total_paid;
  		if($total_paid > 0)
  		{
  		}

  		else
  		{
  			$total_paid = 0;
  		}

  		return $total_paid;
  	}

  	/*
  	*	Retrieve total revenue
  	*
  	*/
  	public function get_normal_payments($where, $table, $page = NULL)
  	{
  		if($page != 'cash')
  		{
  			$where .= ' AND payments.cancel = 0';
  		}
  		//payments
  		$table_search = $this->session->userdata('all_transactions_tables');
  		if((!empty($table_search)) || ($page == 'cash'))
  		{
  			$this->db->from($table);
  		}

  		else
  		{
  			$this->db->from($table);
  		}
  		$this->db->select('*');
  		$this->db->where($where);
  		$query = $this->db->get();

  		return $query;
  	}
  	public function get_payment_methods()
  	{
  		$this->db->select('*');
  		$query = $this->db->get('payment_method');

  		return $query;
  	}

  // public function get_content($table, $where,$select,$group_by=NULL,$limit=NULL)
  // {
  //   $this->db->from($table);
  //   $this->db->select($select);
  //   $this->db->where($where);
  //   if($group_by != NULL)
  //   {
  //     $this->db->group_by($group_by);
  //   }
  //   if($limit != NULL)
  //   {
  //     $this->db->limit($limit);
  //   }
  //   $query = $this->db->get('');
    
  //   return $query;
  // }

    public function get_purchases_details($finance_purchase_id)
  {

    $this->db->where('finance_purchase_id = '.$finance_purchase_id);
    $query = $this->db->get('finance_purchase');


    return $query;
  }

      public function get_landlord_details($landlord_transaction_id)
  {

    $this->db->where('landlord_transaction_id = '.$landlord_transaction_id);
    $query = $this->db->get('landlord_transactions');


    return $query;
  }



       public function get_content($table, $where,$select,$group_by=NULL,$limit=NULL,$order_by=NULL)
  {
    $this->db->from($table);
    $this->db->select($select);
    $this->db->where($where);
    if($group_by != NULL)
    {
      $this->db->group_by($group_by);
    }

    if($order_by != NULL)
    {
      $this->db->order_by($order_by);
    }
    $query = $this->db->get('');
    
    return $query;
  }


    public function get_expense_records($transaction_date,$type)
    {

      if($type == 1)
      {
        $type_check = ' AND projectId > 0';
      }
      else {
        $type_check = ' AND projectId = 0';
      }
      $this->db->select('SUM(dr_amount) AS total_amount');
      $this->db->where('transactionCategory = "Expense" AND transactionDate = "'.$transaction_date.'" '.$type_check);
  		$query = $this->db->get('v_general_ledger');
      $row = $query->row();

      $total_amount = $row->total_amount;

  		return $total_amount;

    }
    public function get_amount_collected_invoice_type($invoice_type_id,$month_id,$year,$property_owner_id=null)
  	{
          $prop = $this->session->userdata('search_property');
  		if(!empty($prop))
  		{
  			$add = $this->session->userdata('search_property');
  		}
  		else
  		{
  			$add = '';
  		}


  		if(!empty($property_owner_id))
  		{
  			$add .= ' AND property.property_owner_id ='.$property_owner_id;
  		}
  		else
  		{
  			$add .= '';
  		}

  		if($invoice_type_id == NULL)
  		{
  			$where = 'payments.payment_status = 1 AND payments.cancel = 0 AND payment_item.payment_id = payments.payment_id  AND MONTH(payments.payment_date) = \''.$month_id.'\' AND YEAR(payments.payment_date) =  '.$year.'  AND payments.lease_id = leases.lease_id AND leases.rental_unit_id = rental_unit.rental_unit_id AND rental_unit.property_id = property.property_id AND payments.cancel = 0  '.$add;

  		}
  		else
  		{
  			$where = 'payments.payment_status = 1 AND payments.cancel = 0 AND payment_item.payment_id = payments.payment_id  AND MONTH(payments.payment_date) = \''.$month_id.'\' AND YEAR(payments.payment_date) =  '.$year.'  AND payments.lease_id = leases.lease_id AND leases.rental_unit_id = rental_unit.rental_unit_id AND rental_unit.property_id = property.property_id AND payments.cancel = 0  AND payment_item.invoice_type_id = '.$invoice_type_id.' '.$add;
  		}

  		// var_dump($where); die();
  		$this->db->select('SUM(payment_item.amount_paid) AS total_amount');
  		$this->db->where($where);
  		$query = $this->db->get('payments,payment_item,leases,rental_unit,property');
  		$total_amount = 0;
  		if($query->num_rows() > 0)
  		{
  			foreach ($query->result() as $key) {
  				# code...
  				$total_amount = $key->total_amount;
  			}
  		}

  		return $total_amount;
  	}

    public function get_all_properties()
    {


      // var_dump($where); die();
      $this->db->select('*');
      $this->db->where('property_deleted = 0 OR closing_date IS NULL');
       $this->db->order_by('property_name','ASC');
      $query = $this->db->get('property');      

      return $query;
    }



    public function get_current_month_invoice($property_id, $invoice_month,$invoice_year,$invoice_type_id = NULL,$start_date=null,$end_date=null)
  {
    if($invoice_type_id != NULL)
    {
      $add = 'AND invoice_type = '.$invoice_type_id;
    }
    else
    {
      $add = '';
    }

    $invoice_date_checked = 'AND month = "'.$invoice_month.'" AND year = "'.$invoice_year.'" ';
    $where = 'lease_invoice.lease_invoice_id = invoice.lease_invoice_id AND leases.lease_id = lease_invoice.lease_id AND rental_unit.rental_unit_id = leases.rental_unit_id and rental_unit.property_id = '.$property_id.' AND lease_invoice.invoice_deleted = 0 '.$invoice_date_checked.' '.$add;
    $this->db->from('invoice,lease_invoice,leases,rental_unit');
    $this->db->select('SUM(invoice_amount) AS total_invoice');
    $this->db->where($where);
    $query = $this->db->get();
    $total_invoice = 0;
    if($query->num_rows() >0)
    {
      $row=$query->row();
      $total_invoice = $row->total_invoice;
    }


    if($invoice_type_id != NULL)
    {
      $added = 'AND invoice_type_id = '.$invoice_type_id;
    }
    else
    {
      $added = '';
    }

    $credit_note_date_checked = 'AND credit_note_month = "'.$invoice_month.'" AND credit_note_year = "'.$invoice_year.'" ';
    $where = 'credit_notes.credit_note_id = credit_note_item.credit_note_id AND leases.lease_id = credit_notes.lease_id AND rental_unit.rental_unit_id = leases.rental_unit_id and rental_unit.property_id = '.$property_id.'  '.$credit_note_date_checked.' '.$added;
    $this->db->from('credit_note_item,credit_notes,leases,rental_unit');
    $this->db->select('SUM(credit_note_item.credit_note_amount) AS total_invoice');
    $this->db->where($where);
    $query = $this->db->get();
    $total_credit_note = 0;
    if($query->num_rows() >0)
    {
      $row=$query->row();
      $total_credit_note = $row->total_invoice;
    }





    return $total_invoice - $total_credit_note;
  }


  public function get_current_months_payments($property_id,$invoice_month,$invoice_year,$invoice_type_id = NULL,$start_date = null,$end_date=null)
  {

    $add = 'AND payment_item.invoice_type_id = '.$invoice_type_id;
    $invoice_date_checked = ' AND payment_year = "'.$invoice_year.'" AND payment_month = "'.$invoice_month.'" ';
    $where = 'payments.payment_id = payment_item.payment_id AND leases.lease_id = payments.lease_id AND rental_unit.rental_unit_id = leases.rental_unit_id and rental_unit.property_id = '.$property_id.' '.$invoice_date_checked.' '.$add;
    $this->db->from('payments,payment_item,leases,rental_unit');
    $this->db->select('SUM(payment_item.amount_paid) AS total_paid');
    $this->db->where($where);
    $query = $this->db->get();
    $total_paid = 0;
    if($query->num_rows() >0)
    {
      $row=$query->row();
      $total_paid = $row->total_paid;
    }
    return $total_paid;
  }


  public function get_outstanding_brought_forward($property_id,$month,$year)
  {


    $this->db->where('property_id = '.$property_id);
    $property_query = $this->db->get('property');
    $rows = $property_query->row();

    $opening_balance = $rows->opening_balance;
    $start_date = $rows->start_date;

    $exploded = explode('-', $start_date);
    $explode_year = $exploded[0];
    $explode_month = $exploded[1];




    $parent_invoice_date = date('Y-m-d', strtotime($year.'-'.$month.'-01'));
    $previous_invoice_date = date('Y-m-d', strtotime($parent_invoice_date.'-1 month'));

    $exploded_previous = explode('-', $previous_invoice_date);
    $previous_year = $exploded_previous[0];
    $previous_month = $exploded_previous[1];


    $invoice_date_checked = ' AND year = '.$year.' AND month = "'.$month.'"';
    $where = 'lease_invoice.lease_invoice_id = invoice.lease_invoice_id AND property.property_id = rental_unit.property_id AND leases.lease_id = lease_invoice.lease_id AND rental_unit.rental_unit_id = leases.rental_unit_id AND leases.lease_id = v_tenant_leases.lease_id and rental_unit.property_id = '.$property_id.' AND lease_invoice.invoice_deleted = 0 AND (lease_invoice.invoice_date <= v_tenant_leases.notice_date OR v_tenant_leases.notice_date IS NULL) AND (lease_invoice.invoice_date <= property.closing_date OR property.closing_date IS NULL OR property.closing_date = "0000-00-00") AND (v_tenant_leases.notice_date >= "'.$start_date.'" OR v_tenant_leases.notice_date IS NULL) '.$invoice_date_checked;
    $this->db->from('invoice,lease_invoice,leases,rental_unit,v_tenant_leases,property');
    $this->db->select('SUM(invoice_amount) AS total_invoice');
    $this->db->where($where);
    $query = $this->db->get();

    $month_invoice = 0;
    if($query->num_rows() >0)
    {
      $row=$query->row();
      $month_invoice = $row->total_invoice;
    }



    

    $invoice_date_checked = ' AND (year >  "'.$explode_year.'" OR (year = '.$explode_year.' AND month >= "'.$explode_month.'") )
       AND (year <  "'.$year.'" OR (year = '.$year.' AND month <= "'.$month.'") )';
    // $invoice_date_checked = '';
    $where = 'lease_invoice.lease_invoice_id = invoice.lease_invoice_id AND property.property_id = rental_unit.property_id AND leases.lease_id = lease_invoice.lease_id AND rental_unit.rental_unit_id = leases.rental_unit_id AND leases.lease_id = v_tenant_leases.lease_id and rental_unit.property_id = '.$property_id.' AND lease_invoice.invoice_deleted = 0 AND (lease_invoice.invoice_date <= v_tenant_leases.notice_date OR v_tenant_leases.notice_date IS NULL) AND (lease_invoice.invoice_date <= property.closing_date OR property.closing_date IS NULL OR property.closing_date = "0000-00-00") AND (v_tenant_leases.notice_date >= "'.$start_date.'" OR v_tenant_leases.notice_date IS NULL) '.$invoice_date_checked;
    $this->db->from('invoice,lease_invoice,leases,rental_unit,v_tenant_leases,property');
    $this->db->select('SUM(invoice_amount) AS total_invoice');
    $this->db->where($where);
    $query = $this->db->get();

    $total_invoice = 0;
    if($query->num_rows() >0)
    {
      $row=$query->row();
      $total_invoice = $row->total_invoice;
    }

    $previous_invoice = $total_invoice - $month_invoice;

    // months credit note


    $credit_note_date_checked = ' AND credit_note_year = '.$year.' AND credit_note_month = "'.$month.'"';
    $where = 'credit_notes.credit_note_id = credit_note_item.credit_note_id AND leases.lease_id = credit_notes.lease_id AND rental_unit.rental_unit_id = leases.rental_unit_id and rental_unit.property_id = '.$property_id.'   '.$credit_note_date_checked;
    $this->db->from('credit_note_item,credit_notes,leases,rental_unit');
    $this->db->select('SUM(credit_note_item.credit_note_amount) AS total_invoice');
    $this->db->where($where);
    $query = $this->db->get();

    $month_credit_note = 0;
    if($query->num_rows() >0)
    {
      $row=$query->row();
      $month_credit_note = $row->total_invoice;
    }



    $credit_note_date_checked = ' AND (credit_note_year >  "'.$explode_year.'" OR (credit_note_year = '.$explode_year.' AND credit_note_month >= "'.$explode_month.'") ) AND (credit_note_year <  "'.$year.'" OR (credit_note_year = '.$year.' AND credit_note_month <= "'.$month.'") ) ';
    // $credit_note_date_checked = '';
    $where = 'credit_notes.credit_note_id = credit_note_item.credit_note_id AND leases.lease_id = credit_notes.lease_id AND rental_unit.rental_unit_id = leases.rental_unit_id and rental_unit.property_id = '.$property_id.'   '.$credit_note_date_checked;
    $this->db->from('credit_note_item,credit_notes,leases,rental_unit');
    $this->db->select('SUM(credit_note_item.credit_note_amount) AS total_invoice');
    $this->db->where($where);
    $query = $this->db->get();

    $total_credit_note = 0;
    if($query->num_rows() >0)
    {
      $row=$query->row();
      $total_credit_note = $row->total_invoice;
    }

    $previous_credit_note = $total_credit_note - $month_credit_note;

    // $total_bill = $total_invoice - $total_credit_note;


    // months payments

     $add = '';
    $invoice_date_checked = ' AND payment_item.payment_year = '.$year.' AND payment_item.payment_month = "'.$month.'"';
    $where = 'payments.payment_id = payment_item.payment_id AND payments.cancel = 0  AND v_tenant_leases.property_id = property.property_id AND v_tenant_leases.property_id = '.$property_id.' AND payments.lease_id = v_tenant_leases.lease_id AND payments.lease_id IN (SELECT lease_invoice.lease_id FROM lease_invoice,invoice WHERE lease_invoice.lease_invoice_id = invoice.lease_invoice_id AND lease_invoice.invoice_deleted = 0 AND (lease_invoice.invoice_date <= v_tenant_leases.notice_date OR v_tenant_leases.notice_date IS NULL) AND (lease_invoice.invoice_date <= property.closing_date OR property.closing_date IS NULL OR property.closing_date = "0000-00-00")) AND (v_tenant_leases.notice_date >= "'.$start_date.'" OR v_tenant_leases.notice_date IS NULL)  '.$invoice_date_checked.' '.$add;
    $this->db->from('payments,payment_item,v_tenant_leases,property');
    $this->db->select('SUM(payment_item.amount_paid) AS total_paid');
    $this->db->where($where);
    $query = $this->db->get();
    $months_payments = 0;
    if($query->num_rows() >0)
    {
      $row=$query->row();
      $months_payments = $row->total_paid;
    }






    $add = '';
    $invoice_date_checked = ' AND (payment_item.payment_year >  "'.$explode_year.'" OR (payment_item.payment_year = '.$explode_year.' AND payment_item.payment_month >= "'.$explode_month.'") ) ';
    // $invoice_date_checked = '';
    $where = 'payments.payment_id = payment_item.payment_id AND v_tenant_leases.property_id = property.property_id AND v_tenant_leases.property_id = '.$property_id.' AND payments.lease_id = v_tenant_leases.lease_id AND payments.lease_id IN (SELECT lease_invoice.lease_id FROM lease_invoice,invoice WHERE lease_invoice.lease_invoice_id = invoice.lease_invoice_id AND lease_invoice.invoice_deleted = 0 AND (lease_invoice.invoice_date <= v_tenant_leases.notice_date OR v_tenant_leases.notice_date IS NULL) AND (lease_invoice.invoice_date <= property.closing_date OR property.closing_date IS NULL OR property.closing_date = "0000-00-00")) AND (v_tenant_leases.notice_date >= "'.$start_date.'" OR v_tenant_leases.notice_date IS NULL)  '.$invoice_date_checked.' '.$add;
    $this->db->from('payments,payment_item,v_tenant_leases,property');
    $this->db->select('SUM(payment_item.amount_paid) AS total_paid');
    $this->db->where($where);
    $query = $this->db->get();
    $total_paid = 0;
    if($query->num_rows() >0)
    {
      $row=$query->row();
      $total_paid = $row->total_paid;
    }

    $previous_payments = $total_paid - $months_payments;


      
    $balance_bf = $previous_invoice - ($previous_credit_note + $previous_payments);

    $current_bill = $month_invoice - ($month_credit_note);



    $total_outstanding = ($balance_bf + $current_bill) - $months_payments;


   
    $reviews['balance_bf'] = $balance_bf;
    $reviews['current_bill'] = $current_bill;
    $reviews['months_payments'] = $months_payments;
    $reviews['total_outstanding'] = $total_outstanding;

  
    return $reviews;

  }


  public function get_property_month_landlord_outflows($property_id,$this_month,$this_year)
  {
    // (int)$this_month;
    $this->db->select('SUM(landlord_transaction_amount) AS total_amount');
    $this->db->where('month ="'.$this_month.'" AND year = "'.$this_year.'" AND property_id = '.$property_id.'  AND landlord_transactions.transaction_type_id = 3 AND landlord_transaction_deleted < 2');
    $this->db->join('account','landlord_transactions.account_to_id = account.account_id','left');
    $landlord_outflows = $this->db->get('landlord_transactions');

    $invoice_amount = 0;
    if($landlord_outflows->num_rows() > 0)
    {


      foreach ($landlord_outflows->result() as $key) 
      {
        # code...
          $invoice_amount = $key->total_amount;
      }

    }else
    {
      $invoice_amount =0;
    }

    return $invoice_amount;

  }

  public function get_property_month_outflows($property_id,$this_month,$this_year)
  {

    $this->db->select('SUM(finance_purchase_amount) AS total_amount');
    $this->db->where('month(transaction_date) = "'.$this_month.'" AND year(transaction_date) = "'.$this_year.'" AND property_id = '.$property_id.'  AND finance_purchase.account_to_id = account.account_id AND finance_purchase.finance_purchase_deleted < 2');
    $query = $this->db->get('finance_purchase,account');

    $invoice_amount = 0;
    if($query->num_rows() > 0)
    {
      foreach ($query->result() as $key) 
      {
        # code...
          $invoice_amount = $key->total_amount;
      }

    }else
    {
      $invoice_amount =0;
    }

    return $invoice_amount;

  }
  public function get_property_month_new_leases($property_id,$this_month,$this_year)
  {

    $this->db->select('SUM(payment_item.amount_paid) AS deposit_amount');
    $this->db->where('leases.tenant_unit_id = tenant_unit.tenant_unit_id AND rental_unit.rental_unit_id = tenant_unit.rental_unit_id AND rental_unit.property_id = '.$property_id.'  AND tenant_unit.tenant_id = tenants.tenant_id AND payments.payment_id = payment_item.payment_id AND payment_item.lease_id = leases.lease_id AND payment_item.payment_month = "'.$this_month.'" AND payment_item.payment_year = "'.$this_year.'" AND payment_item.invoice_type_id = 2');
    $query = $this->db->get('leases,tenant_unit,rental_unit,tenants,payments,payment_item');

     $invoice_amount = 0;
    if($query->num_rows() > 0)
    {
      foreach ($query->result() as $key) 
      {
        # code...
          $invoice_amount = $key->deposit_amount;
      }

    }else
    {
      $invoice_amount =0;
    }

    return $invoice_amount;

  }

  public function get_other_payments_within_month($property_id,$month,$year)
  {

    $this->db->where('property_id = '.$property_id);
    $property_query = $this->db->get('property');
    $rows = $property_query->row();

    $opening_balance = $rows->opening_balance;
    $start_date = $rows->start_date;

    $exploded = explode('-', $start_date);
    $explode_year = $exploded[0];
    $explode_month = $exploded[1];
    // $checking_date = 

    $sql='SELECT SUM(payment_item.amount_paid) AS total_amount FROM (`payments`, `payment_item`, `v_tenant_leases`, `property`) 
      WHERE `payments`.`payment_id` = payment_item.payment_id 
      AND v_tenant_leases.property_id = property.property_id 
      AND v_tenant_leases.property_id = '.$property_id.' AND payments.lease_id = v_tenant_leases.lease_id AND payments.lease_id 
      NOT IN (SELECT lease_invoice.lease_id FROM lease_invoice,invoice WHERE invoice_month = "'.$month.'" AND invoice_year = "'.$year.'" AND lease_invoice.lease_invoice_id = invoice.lease_invoice_id AND lease_invoice.invoice_deleted = 0
       AND (lease_invoice.invoice_date <= v_tenant_leases.notice_date OR v_tenant_leases.notice_date IS NULL) 
      AND (lease_invoice.invoice_date <= property.closing_date OR property.closing_date IS NULL OR property.closing_date = "0000-00-00")) 
      AND (v_tenant_leases.notice_date >= "'.$start_date.'" OR v_tenant_leases.notice_date IS NULL) 
      AND (payment_item.payment_year = '.$year.' AND payment_item.payment_month ="'.$month.'") 
      AND payment_item.invoice_type_id = 1';
    $query = $this->db->query($sql);

     $invoice_amount = 0;
    if($query->num_rows() > 0)
    {
      foreach ($query->result() as $key) 
      {
        # code...
          $invoice_amount = $key->total_amount;
      }

    }else
    {
      $invoice_amount =0;
    }

    return $invoice_amount;
  }

  public function get_property_month_income($property_id,$this_month,$this_year)
  {

    $this->db->select('SUM(landlord_transaction_amount) AS total_amount');
    $this->db->where('month(transaction_date) = "'.$this_month.'" AND year(transaction_date) = "'.$this_year.'" AND property_id = '.$property_id.'  AND landlord_transactions.account_to_id = account.account_id AND landlord_transactions.transaction_type_id = 2 AND landlord_transaction_deleted < 2');
    $query = $this->db->get('landlord_transactions,account');

     $invoice_amount = 0;
    if($query->num_rows() > 0)
    {


      foreach ($query->result() as $key) 
      {
        # code...
          $invoice_amount = $key->total_amount;
      }

    }else
    {
      $invoice_amount =0;
    }

    return $invoice_amount;

  }

    public function get_month_landlord_receipts($property_id,$this_month,$this_year)
  {

    $this->db->select('SUM(landlord_receipt_amount) AS total_amount');
    $this->db->where('month = "'.$this_month.'" AND year = "'.$this_year.'" AND property_id = '.$property_id.'');
    $query = $this->db->get('landlord_receipts');

    $invoice_amount = 0;
    if($query->num_rows() > 0)
    {


      foreach ($query->result() as $key) 
      {
        # code...
          $invoice_amount = $key->total_amount;
      }

    }else
    {
      $invoice_amount =0;
    }

    return $invoice_amount;

  }

  public function get_agent_list_properties()
  {
    $this->db->select('*');
    $this->db->where('personnel_id IN (SELECT personnel_id FROM personnel_properties)');
    $this->db->order_by('personnel_id','ASC');
    $query = $this->db->get('personnel');      

    return $query;
  }

  public function get_agent_properties($personnel_id)
  {

    $this->db->select('*');
    $this->db->where('(property.property_deleted = 0 OR property.closing_date IS NULL ) AND property.property_id = personnel_properties.property_id AND personnel_properties.personnel_id = '.$personnel_id);
     $this->db->order_by('property_name','ASC');
    $query = $this->db->get('property,personnel_properties');      

    return $query;
  }

    
}
?>
