<?php

class Property_model extends CI_Model
{
	public function upload_property_image($property_path, $edit = NULL)
	{
		//upload product's gallery images
		$resize['width'] = 500;
		$resize['height'] = 500;

		if(!empty($_FILES['property_image']['tmp_name']))
		{
			$image = $this->session->userdata('property_file_name');

			if((!empty($image)) || ($edit != NULL))
			{
				if($edit != NULL)
				{
					$image = $edit;
				}
				//delete any other uploaded image
				$this->file_model->delete_file($property_path."\\".$image, $property_path);

				//delete any other uploaded thumbnail
				$this->file_model->delete_file($property_path."\\thumbnail_".$image, $property_path);
			}
			//Upload image
			$response = $this->file_model->upload_file($property_path, 'property_image', $resize, 'height');
			if($response['check'])
			{
				$file_name = $response['file_name'];
				$thumb_name = $response['thumb_name'];

				//crop file to 1920 by 1010
				$response_crop = $this->file_model->crop_file($property_path."\\".$file_name, $resize['width'], $resize['height']);

				if(!$response_crop)
				{
					$this->session->set_userdata('property_error_message', $response_crop);

					return FALSE;
				}

				else
				{
					//Set sessions for the image details
					$this->session->set_userdata('property_file_name', $file_name);
					$this->session->set_userdata('property_thumb_name', $thumb_name);

					return TRUE;
				}
			}

			else
			{
				$this->session->set_userdata('property_error_message', $response['error']);

				return FALSE;
			}
		}

		else
		{
			$this->session->set_userdata('property_error_message', '');
			return FALSE;
		}
	}

	public function get_all_properties($table, $where, $per_page, $page)
	{
		//retrieve all property
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by('property.property_name');
		// $this->db->join('property_owners','property_owners.property_owner_id = property.property_owner_id','left');
		$this->db->join('property_type','property_type.property_type_id = property.property_type','left');

		$query = $this->db->get('', $per_page, $page);

		return $query;
	}

	/*
	*	Delete an existing property
	*	@param int $property_id
	*
	*/
	public function delete_property($property_id)
	{
		$array['property_status'] = 3;
		$array['property_deleted'] = 1;
		$this->db->where('property_id',$property_id);
		if($this->db->update('property',$array))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	public function get_property($property_id)
	{
		$this->db->select('*');
		$this->db->where('property_id', $property_id);
		$query = $this->db->get('property');
		return $query;
	}
	public function get_active_properties()
	{
		$this->db->select('*');
		$this->db->where('property_id > 0 AND property_deleted = 0');
		$query = $this->db->get('property');
		return $query;
	}
	/*
	*	Activate a deactivated property
	*	@param int $property_id
	*
	*/
	public function activate_property($property_id)
	{
		$data = array(
				'property_status' => 1,
				'closing_date' => null,
				'closing_remarks' => null,
				'property_deleted' => 0
			);
		$this->db->where('property_id', $property_id);

		if($this->db->update('property', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}

	/*
	*	Deactivate an activated property
	*	@param int $property_id
	*
	*/
	public function deactivate_property($property_id)
	{
		$data = array(
				'property_status' => 0
			);
		$this->db->where('property_id', $property_id);

		if($this->db->update('property', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}

	public function get_active_property()
	{
  		$table = "property";

		$where = "property_status = 1 AND  property.property_deleted = 0";
		$properties = $this->session->userdata('properties');
		if(isset($properties) AND !empty($properties))
		{
			$where .= $properties;
		}

		$this->db->where($where);
		$query = $this->db->get($table);

		return $query;
	}
	public function get_property_name($property_id)
	{

		$table = "property";
		$where = "property_id = ".$property_id;

		$this->db->where($where);
		$query = $this->db->get($table);
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key) {
				# code...
				$property_name =$key->property_name;
			}
			return $property_name;
		}
		else
		{
			return FALSE;
		}
	}
	public function get_property_billings($table, $where, $per_page, $page)
	{
		//retrieve all property
		$this->db->from($table);
		$this->db->select('invoice_type.invoice_type_name,billing_schedule.billing_schedule_name, property.property_name,property_billing.*');
		$this->db->where($where);
		$this->db->order_by('property_billing.property_billing_id');
		$query = $this->db->get('', $per_page, $page);

		return $query;
	}
	/*
	*	Retrieve all community_group
	*	@param string $table
	* 	@param string $where
	*
	*/
	public function get_active_list($table, $where, $order, $order_method = 'ASC')
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		$query = $this->db->get('');

		return $query;
	}

	public function add_billing($property_id)
	{
		// check if there is data already added
		$charge_to = $this->input->post('charge_to');
		$invoice_type_id = $this->input->post('invoice_type_id');
		$billing_schedule_id = $this->input->post('billing_schedule_id');
		$amount = $this->input->post('amount');
		$where_array = array(
								'charge_to'=>$charge_to,
								'invoice_type_id' => $invoice_type_id,
								'billing_schedule_id' => $billing_schedule_id,
								'property_id' => $property_id,
							);
		$this->db->where($where_array);
		$query = $this->db->get('property_billing');

		if($query->num_rows() > 0)
		{
			$this->db->where($where_array);
			$where_array['billing_amount'] = $amount;
			if($this->db->update('property_billing',$where_array))
			{
				return TRUE;
			}
			else
			{
				return FALSE;
			}
		}
		else
		{
			//  do an insert

			$where_array['billing_amount'] = $amount;
			if($this->db->insert('property_billing',$where_array))
			{
				return TRUE;
			}
			else
			{
				return FALSE;
			}

		}


	}
	public function get_property_invoicing($table, $where,$per_page ,$page, $order='invoice_structure.invoice_structure_name', $order_method = 'ASC')
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		$query = $this->db->get('');

		return $query;
	}
	public function add_property_invoicing($property_id)
	{
		// check if there is data already added
		$charge_to = $this->input->post('charge_to');
		$invoice_structure_id = $this->input->post('invoice_structure_id');
		$where_array = array(
								'charge_to'=>$charge_to,
								'invoice_structure_id' => $invoice_structure_id,
								'property_id' => $property_id,
							);
		$this->db->where($where_array);
		$query = $this->db->get('property_invoice_structure');

		if($query->num_rows() > 0)
		{
			$this->db->where($where_array);
			if($this->db->update('property_invoice_structure',$where_array))
			{
				return TRUE;
			}
			else
			{
				return FALSE;
			}
		}
		else
		{
			//  do an insert

			if($this->db->insert('property_invoice_structure',$where_array))
			{
				return TRUE;
			}
			else
			{
				return FALSE;
			}

		}


	}

	public function get_months()
	{
		return $this->db->get('month');
	}
	public function export_properties()
	{
		$this->load->library('excel');

		//get all transactions
		$where = 'property.property_owner_id = property_owners.property_owner_id';
		

		if(isset($properties) AND !empty($properties))
		{
			$where .= $properties;
		}
		else
		{
			$where .= ' AND property_status = 1';
		}

		// var_dump($where); die();
		$table = 'property,property_owners';

		

		$this->db->where($where);
		$this->db->order_by('property.property_name', 'ASC');
		$this->db->join('property_type','property.property_type = property_type.property_type_id', 'LEFT');
		$this->db->select('*');
		$transactions_query = $this->db->get($table);

		$title = 'Property Export';

		if($transactions_query->num_rows() > 0)
		{
			$count = 0;
			/*
				-----------------------------------------------------------------------------------------
				Document Header
				-----------------------------------------------------------------------------------------
			*/

			
			$row_count = 0;
			$report[$row_count][0] = '#';
			$report[$row_count][1] = 'Code';
			$report[$row_count][2] = 'Property Name.';
			$report[$row_count][3] = 'Property Landlord';
			$report[$row_count][4] = 'Type';
			$report[$row_count][5] = 'Total Units';
			$report[$row_count][6] = 'Location';
			$report[$row_count][7] = 'status';
			//get & display all services

			//display all patient data in the leftmost columns
			foreach($transactions_query->result() as $row)
			{
				$property_id = $row->property_id;
				$property_name = $row->property_name;
				$property_location = $row->property_location;
				$property_owner_name = $row->property_owner_name;
				$property_type_name = $row->property_type_name;
				$total_units = $row->total_units;
				$property_code = $row->property_code;
				$created = $row->created;
				$property_status = $row->property_status;

				if($property_status == 1)
				{
					$status = 'Active';
				}
				else if($property_status == 2)
				{
					$status = 'Closed';
				}
				else if($property_status == 3)
				{
					$status = 'Deleted';
				}



				$row_count++;
				$count++;
				//display the patient data
				$report[$row_count][0] = $count;
				$report[$row_count][1] = $property_code;
				$report[$row_count][2] = $property_name;
				$report[$row_count][3] = $property_owner_name;
				$report[$row_count][4] = $property_type_name;
				$report[$row_count][5] = $total_units;
				$report[$row_count][6] = $property_location;
				$report[$row_count][7] = $status;

				
			}
		}

		//create the excel document
		$this->excel->addArray ( $report );
		$this->excel->generateXML ($title);
	}
}
