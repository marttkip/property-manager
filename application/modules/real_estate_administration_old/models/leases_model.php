<?php

class Leases_model extends CI_Model
{
	/*
	*	Count all items from a table
	*	@param string $table
	* 	@param string $where
	*
	*/
	public function count_items($table, $where, $limit = NULL)
	{
		if($limit != NULL)
		{
			$this->db->limit($limit);
		}
		$this->db->from($table);
		$this->db->where($where);
		return $this->db->count_all_results();
	}

	/*
	*	Retrieve all leases
	*	@param string $table
	* 	@param string $where
	*
	*/
	public function get_all_leases($table, $where, $per_page, $page, $order = 'leases.lease_status', $order_method = 'DESC')
	{
		//retrieve all leases
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		$query = $this->db->get('', $per_page, $page);

		return $query;
	}

	public function add_lease($tenant_id,$rental_unit_id)
	{
		// check if the tenant has been allocated a unit
		$checker = $this->check_tenant_unit_account($tenant_id,$rental_unit_id);
		$deposit= $this->input->post('deposit_amount');

		if($checker > 0)
		{
			$update_array = array('lease_status'=>0);
			$this->db->where('rental_unit_id = '.$rental_unit_id);
			$this->db->update('leases',$update_array);
			// means that the item has been successfully inserted
			// $prefix = 'LN';
			// $suffix = $this->create_form_lease_number($prefix);
			// // var_dump($suffix); die();

			// // $prefix = $prefix;
			// if($suffix < 100)
			// {
			// 	if($suffix < 10)
			// 	{
			// 		$lease_number = $prefix.'-'.date('Y').'-00'.$suffix;
			// 	}
			// 	else
			// 	{
			// 		$lease_number = $prefix.'-'.date('Y').'-0'.$suffix;
			// 	}
			// }
			// else
			// {
			// 	$lease_number = $prefix.'-'.date('Y').'-'.$suffix;
			// }
			$lease_number = $this->create_lease_number();
			$prefix = '';
			$suffix = '';

			$data = array(
				'lease_start_date'=>$this->input->post('lease_start_date'),
				'lease_duration'=>$this->input->post('lease_duration'),
				'lease_number'=>$lease_number,
				'rent_amount'=>$this->input->post('rent_amount'),
				'arrears_bf'=>$this->input->post('arrears_bf'),
				'deposit'=>$this->input->post('deposit_amount'),
				'deposit_ext'=>$this->input->post('deposit_ext'),
				'tenant_unit_id'=>$checker,
				'created'=>date('Y-m-d H:i:s'),
				'lease_status'=>1,
				'prefix'=>$prefix,
				'suffix'=>$suffix,
				'rental_unit_id'=>$rental_unit_id,
				'created_by'=>$this->session->userdata('personnel_id'),
				'branch_code'=>$this->session->userdata('branch_code')
			);
			$lease = $this->input->post('lease_start_date');

			$lease_explode = explode('-', $lease_start_date);

			$year = $lease_explode[0];
			$month = $lease_explode[1];
			if($this->db->insert('leases', $data))
			{
				$lease_id = $this->db->insert_id();

				if($deposit > 0 && is_numeric($deposit))
				{
					$insert_array = array(
										'lease_id' => $lease_id,
										'invoice_date' => $lease,
										'invoice_month' => $month,
										'invoice_year' => $year,
										'invoice_amount' => $deposit,
										'arrears_bf' => 0,
										'invoice_type' => 13
								 	 );

					$this->db->insert('invoice',$insert_array);
				}
				return TRUE;
			}
			else{
				return FALSE;
			}
		}
		else
		{
			return FALSE;
		}

	}
	public function check_tenant_unit_account($tenant_id,$rental_unit_id)
	{
		$this->db->where('tenant_id = '.$tenant_id.' AND rental_unit_id = '.$rental_unit_id.' AND tenant_unit_status = 1');
		$this->db->from('tenant_unit');
		$this->db->select('*');
		$query = $this->db->get();

		if($query->num_rows() == 1)
		{
			foreach ($query->result() as $key) {
				# code...
				$tenant_unit_id = $key->tenant_unit_id;
				$tenant_unit_status = $key->tenant_unit_status;

			}

			return $tenant_unit_id;
		}
		else if($query->num_rows() == 0)
		{
			// create the tenant unit number
			$insert_array = array(
							'tenant_id'=>$tenant_id,
							'rental_unit_id'=>$rental_unit_id,
							'created'=>date('Y-m-d'),
							'created_by'=>$this->session->userdata('personnel_id'),
							'tenant_unit_status'=>1,
							);
			$this->db->insert('tenant_unit',$insert_array);
			$tenant_unit_id = $this->db->insert_id();

			return $tenant_unit_id;
		}
	}

	public function create_lease_number($rental_unit_id=NULL)
	{
		//select product code
		$this->db->where('lease_number > 0');
		$this->db->from('leases');
		$this->db->select('MAX(lease_number) AS number');
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			$result = $query->result();
			$number =  $result[0]->number;
			$number++;//go to the next number


		}
		else{//start generating receipt numbers
			$number = 1;
		}
		return $number;
	}

	/*
	*	Add a new front end lease to the database
	*
	*/
	public function add_frontend_lease()
	{
		$data = array(
				'lease_name'=>ucwords(strtolower($this->input->post('lease_name'))),
				'lease_email'=>$this->input->post('lease_email'),
				'lease_national_id'=>$this->input->post('lease_national_id'),
				'lease_password'=>md5(123456),
				'lease_phone_number'=>$this->input->post('lease_phone_number'),
				'created'=>date('Y-m-d H:i:s'),
				'lease_status'=>1,
				'created_by'=>$this->session->userdata('personnel_id'),
			);

		if($this->db->insert('leases', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}

	/*
	*	Edit an existing lease
	*	@param int $lease_id
	*
	*/
	public function edit_lease($lease_id)
	{
		$data = array(
				'lease_name'=>ucwords(strtolower($this->input->post('lease_name'))),
				'lease_email'=>$this->input->post('lease_email'),
				'lease_national_id'=>$this->input->post('lease_national_id'),
				'lease_phone_number'=>$this->input->post('lease_phone_number'),
				'lease_status'=>1,
				'modified_by'=>$this->session->userdata('personnel_id'),
			);

		//check if lease wants to update their password
		$pwd_update = $this->input->post('admin_lease');
		if(!empty($pwd_update))
		{
			if($this->input->post('old_password') == md5($this->input->post('current_password')))
			{
				$data['password'] = md5($this->input->post('new_password'));
			}

			else
			{
				$this->session->set_userdata('error_message', 'The current password entered does not match your password. Please try again');
			}
		}

		$this->db->where('lease_id', $lease_id);

		if($this->db->update('leases', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}

	/*
	*	Edit an existing lease
	*	@param int $lease_id
	*
	*/
	public function edit_frontend_lease($lease_id)
	{
		$data = array(
				'lease_name'=>ucwords(strtolower($this->input->post('lease_name'))),
				'other_names'=>ucwords(strtolower($this->input->post('last_name'))),
				'phone'=>$this->input->post('phone')
			);

		//check if lease wants to update their password
		$pwd_update = $this->input->post('admin_lease');
		if(!empty($pwd_update))
		{
			if($this->input->post('old_password') == md5($this->input->post('current_password')))
			{
				$data['password'] = md5($this->input->post('new_password'));
			}

			else
			{
				$this->session->set_userdata('error_message', 'The current password entered does not match your password. Please try again');
			}
		}

		$this->db->where('lease_id', $lease_id);

		if($this->db->update('leases', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}

	/*
	*	Edit an existing lease's password
	*	@param int $lease_id
	*
	*/
	public function edit_password($lease_id)
	{
		if($this->input->post('slug') == md5($this->input->post('current_password')))
		{
			if($this->input->post('new_password') == $this->input->post('confirm_password'))
			{
				$data['password'] = md5($this->input->post('new_password'));

				$this->db->where('lease_id', $lease_id);

				if($this->db->update('leases', $data))
				{
					$return['result'] = TRUE;
				}
				else{
					$return['result'] = FALSE;
					$return['message'] = 'Oops something went wrong and your password could not be updated. Please try again';
				}
			}
			else{
					$return['result'] = FALSE;
					$return['message'] = 'New Password and Confirm Password don\'t match';
			}
		}

		else
		{
			$return['result'] = FALSE;
			$return['message'] = 'You current password is not correct. Please try again';
		}

		return $return;
	}

	/*
	*	Retrieve a single lease
	*	@param int $lease_id
	*
	*/
	public function get_lease($lease_id)
	{
		//retrieve all leases
		$this->db->from('leases');
		$this->db->select('*');
		$this->db->where('lease_id = '.$lease_id);
		$query = $this->db->get();

		return $query;
	}



/*
	*	Retrieve a single lease
	*	@param int $lease_id
	*
	*/
	public function get_lease_detail($lease_id)
	{
		//retrieve all leases
		$this->db->from('leases,rental_unit,tenant_unit,tenants,property');
		$this->db->select('*');
		$this->db->where('leases.tenant_unit_id = tenant_unit.tenant_unit_id AND tenant_unit.tenant_id = tenants.tenant_id AND tenant_unit.rental_unit_id = rental_unit.rental_unit_id AND rental_unit.property_id = property.property_id AND lease_id = '.$lease_id);
		$query = $this->db->get();

		return $query;
	}

	/*
	*	Retrieve a single lease
	*	@param int $lease_id
	*
	*/
	public function get_owner_unit_detail($rental_unit_id)
	{
		//retrieve all leases
		$where = 'home_owners.home_owner_id = home_owner_unit.home_owner_id AND home_owner_unit.rental_unit_id = rental_unit.rental_unit_id AND rental_unit.property_id = property.property_id AND home_owner_unit.rental_unit_id = '.$rental_unit_id;

		//retrieve all leases
		$this->db->from('rental_unit,property,home_owners,home_owner_unit');
		$this->db->select('home_owners.*,home_owner_unit.home_owner_unit_id,rental_unit.rental_unit_name,rental_unit.rental_unit_name,rental_unit.rental_unit_id,property.property_id,property.property_name,property.message_prefix');
		$this->db->where($where);
		$query = $this->db->get();

		return $query;
	}

	public function get_lease_detail_owners($home_owner_id)
	{
		$where = 'home_owners.home_owner_id = home_owner_unit.home_owner_id AND home_owner_unit.rental_unit_id = rental_unit.rental_unit_id AND home_owner_unit.home_owner_unit_status = 1 AND rental_unit.property_id = property.property_id AND home_owner_unit.home_owner_id = '.$home_owner_id;
		// var_dump($where); die();
		//retrieve all leases
		$this->db->from('rental_unit,property,home_owners,home_owner_unit');
		$this->db->select('home_owners.*,rental_unit.rental_unit_name,rental_unit.rental_unit_id,property.property_id,property.property_name,property.message_prefix');
		$this->db->where($where);
		$query = $this->db->get();

		return $query;
	}

	public function get_lease_detail_owners_receipt($home_owner_unit)
	{
		$where = 'home_owners.home_owner_id = home_owner_unit.home_owner_id AND home_owner_unit.rental_unit_id = rental_unit.rental_unit_id AND rental_unit.property_id = property.property_id AND home_owner_unit.home_owner_unit_id = '.$home_owner_unit;
		// var_dump($where); die();
		//retrieve all leases
		$this->db->from('rental_unit,property,home_owners,home_owner_unit');
		$this->db->select('home_owners.*,rental_unit.rental_unit_name,rental_unit.rental_unit_id,property.property_id,property.property_name,property.message_prefix');
		$this->db->where($where);
		$query = $this->db->get();

		return $query;
	}

	/*
	*	Retrieve a single lease by their email
	*	@param int $email
	*
	*/
	public function get_lease_by_email($email)
	{
		//retrieve all leases
		$this->db->from('leases');
		$this->db->select('*');
		$this->db->where('email = \''.$email.'\'');
		$query = $this->db->get();

		return $query;
	}

	/*
	*	Delete an existing lease
	*	@param int $lease_id
	*
	*/
	public function delete_lease($lease_id)
	{
		if($this->db->delete('leases', array('lease_id' => $lease_id)))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}

	/*
	*	Activate a deactivated lease
	*	@param int $lease_id
	*
	*/
	public function activate_lease($lease_id)
	{
		$data = array(
				'activated' => 1
			);
		$this->db->where('lease_id', $lease_id);

		if($this->db->update('leases', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}

	/*
	*	Deactivate an activated lease
	*	@param int $lease_id
	*
	*/
	public function deactivate_lease($lease_id)
	{
		$data = array(
				'activated' => 0
			);
		$this->db->where('lease_id', $lease_id);

		if($this->db->update('leases', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}



	public function create_web_name($field_name)
	{
		$web_name = str_replace(" ", "-", strtolower($field_name));

		return $web_name;
	}

	public function get_tenant_unit_leases($tenant_id,$rental_unit_id)
	{
		$this->db->from('leases,tenant_unit');
		$this->db->select('*');
		$this->db->where('leases.tenant_unit_id = tenant_unit.tenant_unit_id AND tenant_unit.tenant_id = '.$tenant_id.' AND tenant_unit.rental_unit_id ='.$rental_unit_id);
		$this->db->order_by('leases.lease_id','DESC');
		$query = $this->db->get();

		return $query;
	}

	public function check_for_account($rental_unit_id)
	{

		$this->db->from('lease_unit');
		$this->db->select('*');
		$this->db->where('lease_unit_status = 1 AND rental_unit_id ='.$rental_unit_id);
		$query = $this->db->get();

		if($query->num_rows() > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}



	public function get_lease_list($table, $where, $order)
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by($order,'asc');
		$query = $this->db->get('');

		return $query;
	}
	public function tenants_units_import_template()
	{
		$this->load->library('Excel');

		$title = 'Tenants Units Import Template';
		$count=1;
		$row_count=0;
		$report[$row_count][0] = 'Rental Unit Name';
		$report[$row_count][1] = 'Rent Amount';
		$report[$row_count][2] = 'Lease Start Date';
		$report[$row_count][3] = 'Lease End Date';
		$report[$row_count][4] = 'Tenant Name';
		$report[$row_count][5] = 'National ID';
		$report[$row_count][6] = 'Phone Number';
		$report[$row_count][7] = 'Email Address';
		$report[$row_count][8] = 'Opening Balance';
		$report[$row_count][9] = 'Deposit';
		$report[$row_count][10] = 'Deposit Cover (i.e. 2 months deposit)';
		$report[$row_count][11] = 'Property Code';

		$row_count++;

		//create the excel document
		$this->excel->addArray ( $report );
		$this->excel->generateXML ($title);
	}


	public function import_csv_payroll($upload_path)
	{
		//load the file model
		$this->load->model('admin/file_model');
		/*
			-----------------------------------------------------------------------------------------
			Upload csv
			-----------------------------------------------------------------------------------------
		*/
		$response = $this->file_model->upload_csv($upload_path, 'import_csv');

		if($response['check'])
		{
			$file_name = $response['file_name'];

			$array = $this->file_model->get_array_from_csv($upload_path.'/'.$file_name);
			// var_dump($array); die();
			$response2 = $this->sort_payroll_data($array);

			if($this->file_model->delete_file($upload_path."\\".$file_name, $upload_path))
			{
			}

			return $response2;
		}
		else
		{
			$this->session->set_userdata('error_message', $response['error']);
			return FALSE;
		}
	}
	public function sort_payroll_data($array)
	{
		$total_rows = count($array);
		$total_columns = count($array[0]);
		// var_dump($total_columns); die();
		//if products exist in array
		if(($total_rows > 0) && ($total_columns == 12))
		{
			//retrieve the data from array
			for($r = 1; $r < $total_rows; $r++)
			{
				$suffix = $this->tenants_model->create_tenant_number();
				$prefix = 'T';
				if($suffix < 100)
				{
					if($suffix < 10)
					{
						$tenant_number = $prefix.'00'.$suffix;
					}
					else
					{
						$tenant_number = $prefix.'0'.$suffix;
					}
				}
				else
				{
					$tenant_number = $prefix.$suffix;
				}

				$rental_unit_name = $array[$r][0];
				$rental_amount = $array[$r][1];
				$rental_start_date = $array[$r][2];
				$rental_end_date = $array[$r][3];
				$newDate = date("Y-m-d", strtotime($rental_start_date));				
				$endDate = $lease_end_date  = date("Y-m-d",strtotime(date("Y-m-d", strtotime($newDate)) . '+36 month'));
				$tenant_name =$items['tenant_name'] = $array[$r][4];
				$items['tenant_national_id'] = $array[$r][5];
				$tenant_phone_number = $items['tenant_phone_number'] = $array[$r][6];
				$tenant_phone_number = $array[$r][6];
				$items['tenant_email']=$array[$r][7];
				$opening_balance =$array[$r][8];
				$deposit = $array[$r][9];
				$deposit_times =$array[$r][10];
				$property_code = $array[$r][11];
				$items['tenant_status'] = 1;
				$items['created'] = date('Y-m-d H-i-s');
				$items['tenant_number'] = $tenant_number;

				if(empty($opening_balance))
				{
					$opening_balance = 0;
				}
				// var_dump($endDate); die();

				//  get the rental Unit ID using the unit name
				$property_id = $this->get_property_id_by_code($property_code);
				// var_dump($property_id); die();
				if(!empty($property_id) AND !empty($rental_unit_name))
				{
					$rental_unit_id = $this->get_rental_unit_id($rental_unit_name,$property_id);
				}
				else
				{
					$rental_unit_id = NULL;
				}
				

				// var_dump($rental_unit_id);die();
				$comment ='';

				if(!empty($rental_unit_id))
				{
					
					$this->db->where('rental_unit_id = '.$rental_unit_id.' AND status = 1');
					$query_units  = $this->db->get('rental_unit_prices');
					$units_add['rental_unit_id'] = $rental_unit_id;
					$units_add['current'] = $rental_amount;
					if($query_units->num_rows() > 0)
					{
						$units_update['status'] = 0;
						$this->db->where('rental_unit_id = '.$rental_unit_id.' AND status = 1');
						$this->db->update('rental_unit_prices',$units_update);
					}
					else
					{
						$this->db->insert('rental_unit_prices',$units_add);
					}
					//check if the project name already exists
					if($this->tenants_model->check_tenant_exist($tenant_name,$tenant_phone_number))
					{

					}
					else
					{
						$this->db->insert('tenants', $items);

						$tenant_id = $this->db->insert_id();
						// give tenant the unit  (tenant unit id )
						$this->db->where('tenant_unit_status = 1 AND rental_unit_id = '.$rental_unit_id.'');
						$this->db->from('tenant_unit');
						$this->db->select('*');
						$query_items = $this->db->get();

						if($query_items->num_rows() > 0)
						{
							foreach ($query_items->result() as $key_items) {
								# code...
								$tenant_unit_id = $key_items->tenant_unit_id;
								$tenant_unit_status = $key_items->tenant_unit_status;
									// update the details the status to 1
								$update_array = array('tenant_unit_status'=>0);
								$this->db->where('tenant_unit_id = '.$tenant_unit_id);
								$this->db->update('tenant_unit',$update_array);
							}
						}
						$insert_array = array(
											'tenant_id'=>$tenant_id,
											'rental_unit_id'=>$rental_unit_id,
											'created'=>date('Y-m-d'),
											'created_by'=>$this->session->userdata('personnel_id'),
											'tenant_unit_status'=>1,
											);

						$this->db->insert('tenant_unit',$insert_array);
						$tenant_unit_id = $this->db->insert_id();

						$this->db->where('rental_unit_id = '.$rental_unit_id.'');
						$this->db->from('leases');
						$this->db->select('*');
						$rental_items = $this->db->get();

						if($rental_items->num_rows() > 0)
						{
							foreach ($rental_items->result() as $rental_items) {
								$lease_id = $rental_items->lease_id;
								$lease_status = $rental_items->lease_status;
									// update the details the status to 1
								$update_array = array('lease_status'=>3);
								$this->db->where('lease_id = '.$lease_id);
								$this->db->update('leases',$update_array);
							}
						}

						
						$lease_number = $this->create_lease_number();
						$prefix = '';
						$suffix = '';
						$items_row['lease_start_date'] = $newDate;
						$items_row['lease_end_date'] = $endDate;
						$items_row['lease_duration'] = 36;
						$items_row['lease_number'] = $lease_number;
						$items_row['rent_amount'] = $rental_amount;
						$items_row['deposit'] = $deposit;
						$items_row['deposit_ext'] = $deposit_times;
						$items_row['arrears_bf'] = 0;
						$items_row['deposit'] = $rental_amount;
						$items_row['deposit_ext'] = 1;
						$items_row['tenant_unit_id'] = $tenant_unit_id;
						$items_row['rental_unit_id'] = $rental_unit_id;
						$items_row['tenant_id'] = $tenant_id;
						$items_row['created'] = date('Y-m-d H:i:s');
						$items_row['lease_status'] = 1;
						$items_row['arrears_bf'] = $opening_balance;
						$items_row['created_by'] = $this->session->userdata('personnel_id');
						$items_row['branch_code'] = $this->session->userdata('branch_code');

						$this->db->insert('leases', $items_row);
						$lease_id = $this->db->insert_id();

						$date = explode('-', $newDate);
						$month = $date[1];
						$year = $date[0];
						$invoice_amount = $rental_amount;


						// create property billings
						$charge_to = 1;
						$invoice_type_id = 1;
						$billing_schedule_id = 1;
						$start_date = $newDate;
						$arrears = 0;
						$initial_reading = 0;
						$amount = $rental_amount;
						$where_array = array(
												'charge_to'=>1,
												'invoice_type_id' => $invoice_type_id,
												'billing_schedule_id' => $billing_schedule_id,
												'lease_id' => $lease_id,
												'start_date' => $start_date,
												'initial_reading' => $initial_reading,
												'arrears_bf' => $opening_balance,
												'lease_number' => $lease_number,
											);
						$this->db->where($where_array);
						$query = $this->db->get('property_billing');

						if($query->num_rows() > 0)
						{
							$this->db->where($where_array);
							$where_array['billing_amount'] = $amount;
							$this->db->update('property_billing',$where_array);

						}
						else
						{
							//  do an insert

							$where_array['billing_amount'] = $amount;
							$this->db->insert('property_billing',$where_array);

						}

						$invoice_year = date('Y');
						$invoice_month = date('m');

						if($opening_balance > 0)
						{
							$document_number = $this->accounts_model->create_invoice_number();
							// create the invoice
							$insertarray['invoice_date'] = $newDate;
							$insertarray['invoice_year'] = $invoice_year;
							$insertarray['invoice_month'] = $invoice_month;
							$insertarray['lease_id'] = $lease_id;
							$insertarray['rental_unit_id'] = $rental_unit_id;
							$insertarray['tenant_id'] = $tenant_id;
							$insertarray['document_number'] = $document_number;
							$insertarray['total_amount'] = $opening_balance;
							$insertarray['created_by'] = $this->session->userdata('personnel_id');
							$insertarray['created'] = date('Y-m-d');

							$this->db->insert('lease_invoice',$insertarray);
							$lease_invoice_id = $this->db->insert_id();
							

							$next_date = $invoice_year.'-'.$invoice_month.'-'.'01';
							$next_date = strtotime($newDate);
							$next_quarter = ceil(date('m', $next_date) / 3);
							$next_month = ($next_quarter * 3) - 2;
							$next_year = date('Y', $next_date);
							$next_quarter = 'AC'.$next_quarter.'-'.$next_year;

							$concate = $invoice_year.'-'.$invoice_month;
							$date = date('F Y',strtotime($concate));

							$remarks = 'Opening Balance from '.$date;

							

							$insert_array = array(
											'lease_id' => $lease_id,
											'year' => $invoice_year,
											'month' => $invoice_month,
											'lease_invoice_id' => $lease_invoice_id,
											'invoice_amount' => $opening_balance,
											'invoice_type' => 34,
											'billing_schedule_quarter' => $next_quarter,
											'invoice_item_status'=>1,
											'tax_amount'=>0,
											'created'=>date('Y-m-d'),
											'personnel_id'=>$this->session->userdata('personnel_id'),
											'created_by'=>$this->session->userdata('personnel_id'),
											'tenant_id'=>$tenant_id,
											'rental_unit_id'=>$rental_unit_id,
											'lease_number'=>$lease_number,
											'remarks'=>$remarks

									 	 );
							$this->db->insert('invoice',$insert_array);
						}

					}
					
				}

			}



			$return['response'] = 'success';
			$return['check'] = TRUE;
		}

		//if no products exist
		else
		{
			$return['response'] = 'Member data not found ';
			$return['check'] = FALSE;
		}

		return $return;
	}

	public function get_property_id_by_code($property_code)
	{
		$this->db->where('property_prefix',$property_code);
		$query = $this->db->get('property');

		$property_id = 0;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key) {
				# code...
				$property_id = $key->property_id;
			}
		}
		return $property_id;
	}
	public function get_rental_unit_id($rental_unit_name,$property_id = NULL)
	{
		if(!empty($property_id))
		{
			$add = ' AND property_id = '.$property_id;
		}
		else
		{
			$add = '';
		}
		$this->db->from('rental_unit');
		$this->db->select('*');
		$this->db->where('rental_unit_name = "'.$rental_unit_name.'"'.$add);

		$query = $this->db->get();

		$rental_unit_id = 0;

		if($query->num_rows() > 0 )
		{
			$result = $query->result();
			$rental_unit_id = $result[0]->rental_unit_id;
		}
		else
		{
			if(!empty($property_id))
			{
				$array['property_id'] = $property_id;
				$array['rental_unit_name'] = $rental_unit_name;
				$array['created'] = date('Y-m-d');
				$array['created_by'] = $this->session->userdata('personnel_id');
				$this->db->insert('rental_unit',$array);
				$rental_unit_id = $this->db->insert_id('rental_unit_id');
			}

		}


		return $rental_unit_id;

	}
	public function home_owners_import_template()
	{
		$this->load->library('Excel');

		$title = 'Home Owners Import Template';
		$count=1;
		$row_count=0;
		$report[$row_count][0] = 'Rental Unit Name';
		$report[$row_count][1] = 'Home Owner Name';
		$report[$row_count][2] = 'National ID';
		$report[$row_count][3] = 'Phone Number';
		$report[$row_count][4] = 'Email Address';
		$report[$row_count][5] = 'Opening Balance';


		$row_count++;

		//create the excel document
		$this->excel->addArray ( $report );
		$this->excel->generateXML ($title);
	}


	public function import_csv_home_owners($upload_path)
	{
		//load the file model
		$this->load->model('admin/file_model');
		/*
			-----------------------------------------------------------------------------------------
			Upload csv
			-----------------------------------------------------------------------------------------
		*/
		$response = $this->file_model->upload_csv($upload_path, 'import_csv');

		if($response['check'])
		{
			$file_name = $response['file_name'];

			$array = $this->file_model->get_array_from_csv($upload_path.'/'.$file_name);
			// var_dump($array); die();
			$response2 = $this->sort_home_owners_data($array);

			if($this->file_model->delete_file($upload_path."\\".$file_name, $upload_path))
			{
			}

			return $response2;
		}
		else
		{
			$this->session->set_userdata('error_message', $response['error']);
			return FALSE;
		}
	}
	public function sort_home_owners_data($array)
	{
		$total_rows = count($array);
		$total_columns = count($array[0]);
		// var_dump($total_columns); die();
		//if products exist in array
		if(($total_rows > 0) && ($total_columns == 6))
		{
			//retrieve the data from array
			for($r = 1; $r < $total_rows; $r++)
			{

				$rental_unit_name = $array[$r][0];
				$home_owner_name  = $array[$r][1];
				$home_owner_phone_number  = $array[$r][3];
				$items['home_owner_name']  = $array[$r][1];
				$items['home_owner_national_id'] = $array[$r][2];
				$items['home_owner_phone_number'] = $array[$r][3];
				$items['home_owner_email'] =$array[$r][4];
				$opening_balance =$array[$r][5];

				$rental_unit_id = $this->get_rental_unit_id($rental_unit_name);
				$comment ='';

				if(!empty($rental_unit_id))
				{
					//check if the project name already exists
					$home_owner_id = $this->home_owners_model->check_home_owner_exist($home_owner_name,$home_owner_phone_number);

					if(!empty($home_owner_id))
					{

						// var_dump($home_owner_id.'here');die();
						// give tenant the unit  (tenant unit id )
						$this->db->where('home_owner_id = '.$home_owner_id.' AND rental_unit_id = '.$rental_unit_id.'');
						$this->db->from('home_owner_unit');
						$this->db->select('*');
						$query_items = $this->db->get();

						if($query_items->num_rows() > 0)
						{
							foreach ($query_items->result() as $key_items) {
								# code...
								$home_owner_unit_id = $key_items->home_owner_unit_id;
									// update the details the status to 1
								$update_array = array('home_owner_id'=>$home_owner_id);
								$this->db->where('home_owner_unit_id = '.$home_owner_unit_id);
								$this->db->update('home_owner_unit',$update_array);
							}
						}
						else
						{
							$insert_array = array(
											'home_owner_id'=>$home_owner_id,
											'rental_unit_id'=>$rental_unit_id,
											'created'=>date('Y-m-d'),
											'created_by'=>$this->session->userdata('personnel_id'),
											'home_owner_unit_status'=>1,
											'lease_start_date' => '2016-01-01'
											);

							$this->db->insert('home_owner_unit',$insert_array);
							$home_owner_unit_id = $this->db->insert_id();

						}


							//  invoice the
						$newDate='2016-01-01';
						$date = explode('-', $newDate);
						$month = $date[1];
						$year = $date[0];
						$invoice_amount = $opening_balance;

						$insert_array = array(
											'rental_unit_id' => $rental_unit_id,
											'invoice_date' => $newDate,
											'invoice_month' => $month,
											'invoice_year' => $year,
											'invoice_amount' => $invoice_amount,
											'arrears_bf' => $opening_balance,
											'invoice_type' => 4,
											'balance_bf'=>1
									 	 );

						$this->db->insert('home_owners_invoice',$insert_array);
					}
					else
					{
						// var_dump($items);die();
						$this->db->insert('home_owners', $items);

						$home_owner_id = $this->db->insert_id();


						// give tenant the unit  (tenant unit id )
						$this->db->where('home_owner_id = '.$home_owner_id.' AND rental_unit_id = '.$rental_unit_id.'');
						$this->db->from('home_owner_unit');
						$this->db->select('*');
						$query_items = $this->db->get();

						if($query_items->num_rows() > 0)
						{
							foreach ($query_items->result() as $key_items) {
								# code...
								$home_owner_unit_id = $key_items->home_owner_unit_id;
									// update the details the status to 1
								$update_array = array('home_owner_id'=>$home_owner_id);
								$this->db->where('home_owner_unit_id = '.$home_owner_unit_id);
								$this->db->update('home_owner_unit',$update_array);
							}
						}
						$insert_array = array(
											'home_owner_id'=>$home_owner_id,
											'rental_unit_id'=>$rental_unit_id,
											'created'=>date('Y-m-d'),
											'created_by'=>$this->session->userdata('personnel_id'),
											'home_owner_unit_status'=>1,
											);

						$this->db->insert('home_owner_unit',$insert_array);
						$home_owner_id = $this->db->insert_id();

							//  invoice the
						$newDate='2016-01-01';
						$date = explode('-', $newDate);
						$month = $date[1];
						$year = $date[0];
						$invoice_amount = $opening_balance;
						$insert_array = array(
											'rental_unit_id' => $rental_unit_id,
											'invoice_date' => $newDate,
											'invoice_month' => $month,
											'invoice_year' => $year,
											'invoice_amount' => $invoice_amount,
											'arrears_bf' => $opening_balance,
											'invoice_type' => 1
									 	 );

						$this->db->insert('home_owners_invoice',$insert_array);

					}




				}

			}



			$return['response'] = 'success';
			$return['check'] = TRUE;
		}

		//if no products exist
		else
		{
			$return['response'] = 'Member data not found ';
			$return['check'] = FALSE;
		}

		return $return;
	}
	public function add_billing($lease_id)
	{
		// check if there is data already added
		$charge_to = $this->input->post('charge_to');
		$invoice_type_id = $this->input->post('invoice_type_id');
		$billing_schedule_id = $this->input->post('billing_schedule_id');
		$start_date = $this->input->post('start_date');
		$arrears = $this->input->post('arrears');
		$initial_reading = $this->input->post('initial_reading');
		$lease_number = $this->input->post('lease_number');
		$amount = $this->input->post('amount');
		$where_array = $insert_array = array(
								'charge_to'=>1,
								'invoice_type_id' => $invoice_type_id,
								'billing_schedule_id' => $billing_schedule_id,
								'lease_id' => $lease_id
							);
		$this->db->where($where_array);
		$query = $this->db->get('property_billing');

		if($query->num_rows() > 0)
		{
			$this->db->where($where_array);
			$where_array['billing_amount'] = $amount;
			$where_array['initial_reading'] = $initial_reading;
			$where_array['start_date'] = $start_date;
			$where_array['arrears_bf'] = $arrears_bf;
			$where_array['property_billing_deleted'] = 1;
			// var_dump($where_array);die();
			if($this->db->update('property_billing',$where_array))
			{
				$insert_array['billing_amount'] = $amount;
				$insert_array['property_billing_deleted'] = 0;
				$insert_array['lease_number'] = $lease_number;
				$insert_array['initial_reading'] = $initial_reading;
				$insert_array['start_date'] = $start_date;
				$insert_array['arrears_bf'] = $arrears_bf;
				if($this->db->insert('property_billing',$insert_array))
				{
					return TRUE;
				}
				else
				{
					return FALSE;
				}
			}
			else
			{
				return FALSE;
			}
		}
		else
		{
			//  do an insert

			$insert_array['billing_amount'] = $amount;
			$insert_array['initial_reading'] = $initial_reading;
			$insert_array['start_date'] = $start_date;
			$insert_array['arrears_bf'] = $arrears_bf;
			$insert_array['property_billing_deleted'] = 0;
			$insert_array['lease_number'] = $lease_number;

			// var_dump($insertarray);die();
			if($this->db->insert('property_billing',$insert_array))
			{
				return TRUE;
			}
			else
			{
				return FALSE;
			}

		}


	}

	public function update_billing($lease_id)
	{

		$table = 'tenants,tenant_unit,rental_unit,leases,property';
		$where = 'tenants.tenant_id = tenant_unit.tenant_id AND tenant_unit.rental_unit_id = rental_unit.rental_unit_id  AND tenant_unit.tenant_unit_id = leases.tenant_unit_id AND leases.lease_status = 1  AND rental_unit.property_id = property.property_id AND leases.lease_id = '.$lease_id;


		$this->db->from($table);
		$this->db->select('leases.*,property.property_id');
		$this->db->where($where);
		$query = $this->db->get();
		// var_dump($query); die();
		if($query->num_rows() > 0)
		{
			// get the lease information

			foreach ($query->result() as $key) {
				# code...
				$lease_id = $key->lease_id;

				$lease_start_date = $key->lease_start_date;
				$lease_end_date = $key->lease_end_date;
				$rent_amount = $key->rent_amount;
				$inital_rent_amount = $key->rent_amount;
				$property_id = $key->property_id;
				$rental_unit_id = $key->rental_unit_id;

				// get the year and the month
				$date = explode('-', $lease_start_date);
				$month = $date[1];
				$year = $date[0];
				$x=0;
				$todays_month = date('m');



				$datestring=''.$lease_start_date.' first day of last month';
				$dt=date_create($datestring);
				$previous = $dt->format('Y-m-d');
				$previous_date = explode('-', $previous);
				$previous_year = $previous_date[0];
				$previous_month = $previous_date[1];
				$previous_date_item = $previous;


				$start_date = strtotime('3 months ago');
				$start_quarter = ceil(date('m', $start_date) / 3);
				$start_month = ($start_quarter * 3) - 2;
				$start_year = date('Y', $start_date);


				$prev_quarter = 'AC'.$start_quarter.'-'.$start_year;

				// current items

				$current_date = strtotime(date('Y-m-d'));
				$current_quarter = ceil(date('m', $current_date) / 3);
				$current_month = ($current_quarter * 3) - 2;
				$current_year = date('Y', $current_date);

				$invoice_date_item = date('Y-m-d');

				$curr_quarter = 'AC'.$current_quarter.'-'.$current_year;


				// var_dump($todays_month); die();



				// $month = str_replace('0', '', $month);
				// $todays_month = str_replace('0', '', $todays_month);
				$todays_year = date('Y');
				$explode_end = explode('-', $lease_end_date);
				$end_year = $explode_end[0];
				$end_month = $explode_end[1];
				$end_month = (int)$end_month;

				if($end_year == $todays_year)
				{
					$todays_month =  $end_month;
				}
				else
				{
					$todays_month = (int)$todays_month;
				}

				$this->db->from('invoice_type,property_billing');
					$this->db->select('*');
					$this->db->where('invoice_type.invoice_type_id = property_billing.invoice_type_id AND property_billing.charge_to = 1 AND billing_schedule_id <> 4 AND property_billing.property_billing_status = 1  AND property_billing.property_billing_deleted = 0 AND property_billing.lease_id = '.$lease_id);

					$invoice_type_query = $this->db->get();

					if($invoice_type_query->num_rows() > 0)
					{

						foreach ($invoice_type_query->result() as $invoice_key) {
							$invoice_type_id = $invoice_key->invoice_type_id;
							$billing_amount = $invoice_key->billing_amount;
							$arrears_bf = $invoice_key->arrears_bf;

							if($arrears_bf > 0)
							{

								$invoice_amount = $arrears_bf;

									// $todaym2 = str_replace('00', '0', $todaym);
									$next_quarter = $curr_quarter;

									if($invoice_amount > 0)
									{
										$insert_array = array(
														'lease_id' => $lease_id,
														'invoice_date' => $previous_date_item,
														'invoice_month' => $previous_month,
														'invoice_year' => $previous_year,
														'invoice_amount' => $invoice_amount,
														'invoice_type' => $invoice_type_id,
														'invoice_status' => 1,
														'billing_schedule_quarter' => $next_quarter
												 	 );
										$this->db->insert('invoice',$insert_array);


									}
							}
						}
					}
					// arrears end

					$this->db->from('invoice_type,property_billing');
					$this->db->select('*');
					$this->db->where('invoice_type.invoice_type_id = property_billing.invoice_type_id AND property_billing.charge_to = 1 AND billing_schedule_id = 3 AND property_billing.property_billing_status = 1  AND property_billing.property_billing_deleted = 0 AND property_billing.lease_id = '.$lease_id);

					$invoice_type_query = $this->db->get();
					// var_dump($invoice_type_query); die();
					if($invoice_type_query->num_rows() > 0)
					{

						foreach ($invoice_type_query->result() as $invoice_key) {
							$invoice_type_id = $invoice_key->invoice_type_id;
							$billing_amount = $invoice_key->billing_amount;
							$arrears_bf = $invoice_key->arrears_bf;

							if($billing_amount > 0)
							{

								$invoice_amount = $billing_amount;

									// $todaym2 = str_replace('00', '0', $todaym);
									$next_quarter = $curr_quarter;

									if($invoice_amount > 0)
									{
										$insert_array = array(
														'lease_id' => $lease_id,
														'invoice_date' => $lease_start_date,
														'invoice_month' => $month,
														'invoice_year' => $year,
														'invoice_amount' => $billing_amount,
														'invoice_type' => $invoice_type_id,
														'invoice_status' => 1,
														'billing_schedule_quarter' => $next_quarter
												 	 );
										$this->db->insert('invoice',$insert_array);


									}
							}
						}
					}

				$month = (int)$month;
				$month_today = (int)date('m');
				// var_dump($month_today); die();
				// for ($m=$month; $m<=$month_today; $m++) {
				$todays_date = date('Y-m-d');
				$start = $month = strtotime($lease_start_date);
				$end = strtotime($todays_date);
				while (strtotime($lease_start_date) <= strtotime($todays_date))
				{
	                // echo "$lease_start_date\n";


	                $lease_start_date = date ("Y-m-d", strtotime($lease_start_date));

					$days = explode('-', $lease_start_date);

					$lease_month = $days[1];
					$lease_year = $days[0];

					$this->db->from('invoice_type,property_billing');
					$this->db->select('*');
					$this->db->where('invoice_type.invoice_type_id = property_billing.invoice_type_id AND property_billing.charge_to = 1 AND billing_schedule_id = 1 AND property_billing.property_billing_status = 1 AND property_billing.property_billing_deleted = 0 AND property_billing.lease_id = '.$lease_id);

					$invoice_type_query = $this->db->get();

					if($invoice_type_query->num_rows() > 0)
					{

						foreach ($invoice_type_query->result() as $invoice_key) {
							$invoice_type_id = $invoice_key->invoice_type_id;
							$billing_amount = $invoice_key->billing_amount;
							$arrears_bf = $invoice_key->arrears_bf;

							$billing_schedule_id = $invoice_key->billing_schedule_id;

							if($billing_schedule_id == 2)
							{


								$datestring=''.$invoice_date_item.' first day of next month';
								$dt=date_create($datestring);
								$next = $dt->format('Y-m-d');
								$next_date = explode('-', $next);
								$next_year = $next_date[0];
								$next_month = $next_date[1];
								$next_date = $next_year.'-'.$next_month.'-'.'01';
								$next_date = strtotime($next_date);
								$next_quarter = ceil(date('m', $next_date) / 3);
								$next_month = ($next_quarter * 3) - 2;
								$next_year = date('Y', $next_date);

								$next_quarter = 'AC'.$next_quarter.'-'.$next_year;

								$where = 'invoice_type = '.$invoice_type_id.'  AND lease_id = '.$lease_id.' AND billing_schedule_quarter = "'.$next_quarter.'"';

								$this->db->from('invoice');
								$this->db->select('*');
								$this->db->where($where);
							}

							else
							{
								$next_quarter = $curr_quarter;
								$this->db->from('invoice');
								$this->db->select('*');
								$this->db->where('invoice_type = '.$invoice_type_id.' AND invoice_month = "'.$lease_month.'" AND invoice_year = '.$lease_year.' AND lease_id = '.$lease_id.'');
							}

							$service_charge_query = $this->db->get();
							// var_dump($service_charge_query->num_rows()); die();
							if($service_charge_query->num_rows() == 0 )
							{

									$invoice_amount = $billing_amount;


									if($invoice_amount > 0)
									{
										$invoice_date_item = $lease_year.'-'.$lease_month.'-01';
										$insert_array = array(
														'lease_id' => $lease_id,
														'invoice_date' => $invoice_date_item,
														'invoice_month' => $lease_month,
														'invoice_year' => $lease_year,
														'invoice_amount' => $invoice_amount,
														'invoice_type' => $invoice_type_id,
														'invoice_status' => 1,
														'billing_schedule_quarter' => $next_quarter
												 	 );
										$this->db->insert('invoice',$insert_array);


									}


							}
						}

					}

	                $lease_start_date = date ("Y-m-d", strtotime("+1 month", strtotime($lease_start_date)));
				}




			}


		}
		else
		{

		}

	}


	/// BOB Functions



	public function lease_details_import_template()
	{
		$this->load->library('Excel');

		$title = 'Lease Details Import Template';
		$count=1;
		$row_count=0;
		$report[$row_count][0] = 'Rental Unit Name';
		$report[$row_count][1] = 'Lease Start Date';
		$report[$row_count][2] = 'Tenant Name';
		$report[$row_count][3] = 'National ID';
		$report[$row_count][4] = 'Phone Number';
		$report[$row_count][5] = 'Email Address';


		$row_count++;

		//create the excel document
		$this->excel->addArray ( $report );
		$this->excel->generateXML ($title);
	}


	public function import_csv_lease_details($upload_path)
	{
		//load the file model
		$this->load->model('admin/file_model');
		/*
			-----------------------------------------------------------------------------------------
			Upload csv
			-----------------------------------------------------------------------------------------
		*/
		$response = $this->file_model->upload_csv($upload_path, 'import_csv');

		if($response['check'])
		{
			$file_name = $response['file_name'];

			$array = $this->file_model->get_array_from_csv($upload_path.'/'.$file_name);
			// var_dump($array); die();
			$response2 = $this->sort_lease_details($array);

			if($this->file_model->delete_file($upload_path."\\".$file_name, $upload_path))
			{
			}

			return $response2;
		}
		else
		{
			$this->session->set_userdata('error_message', $response['error']);
			return FALSE;
		}
	}
	public function sort_lease_details($array)
	{
		$total_rows = count($array);
		$total_columns = count($array[0]);
		// var_dump($total_columns); die();
		//if products exist in array
		if(($total_rows > 0) && ($total_columns == 6))
		{
			//retrieve the data from array
			for($r = 1; $r < $total_rows; $r++)
			{
				$tenant_number = $this->tenants_model->create_tenant_number();
				$prefix = 'T';
				if($suffix < 100)
				{
					if($suffix < 10)
					{
						$tenant_number = $prefix.'00'.$suffix;
					}
					else
					{
						$tenant_number = $prefix.'0'.$suffix;
					}
				}
				else
				{
					$tenant_number = $prefix.$suffix;
				}

				$rental_unit_name = $array[$r][0];
				$rental_start_date = $array[$r][1];
				$newDate = date("Y-m-d", strtotime($rental_start_date));
				$tenant_name = $items['tenant_name'] = $array[$r][2];
				$items['tenant_national_id'] = $array[$r][3];
				$tenant_phone_number = $items['tenant_phone_number'] = $array[$r][4];
				$tenant_phone_number = $array[$r][4];
				$items['tenant_email']=$array[$r][5];
				$items['tenant_status'] = 1;
				$items['created'] = date('Y-m-d H-i-s');
				$items['tenant_number'] = $tenant_number;
				// var_dump($newDate); die();

				//  get the rental Unit ID using the unit name

				$rental_unit_id = $this->get_rental_unit_id($rental_unit_name);

				// var_dump($rental_unit_id);die();
				$comment ='';

				if(!empty($rental_unit_id))
				{
					//check if the project name already exists
					if($this->tenants_model->check_tenant_exist($tenant_name,$tenant_phone_number))
					{

					}
					else
					{
						$this->db->insert('tenants', $items);

						$tenant_id = $this->db->insert_id();


						// give tenant the unit  (tenant unit id )
						$this->db->where('tenant_unit_status = 1 AND rental_unit_id = '.$rental_unit_id.'');
						$this->db->from('tenant_unit');
						$this->db->select('*');
						$query_items = $this->db->get();

						if($query_items->num_rows() > 0)
						{
							foreach ($query_items->result() as $key_items) {
								# code...
								$tenant_unit_id = $key_items->tenant_unit_id;
								$tenant_unit_status = $key_items->tenant_unit_status;
									// update the details the status to 1
								$update_array = array('tenant_unit_status'=>0);
								$this->db->where('tenant_unit_id = '.$tenant_unit_id);
								$this->db->update('tenant_unit',$update_array);
							}
						}
						$insert_array = array(
											'tenant_id'=>$tenant_id,
											'rental_unit_id'=>$rental_unit_id,
											'created'=>date('Y-m-d'),
											'created_by'=>$this->session->userdata('personnel_id'),
											'tenant_unit_status'=>1,
											);

						$this->db->insert('tenant_unit',$insert_array);
						$tenant_unit_id = $this->db->insert_id();

						$this->db->where('rental_unit_id = '.$rental_unit_id.'');
						$this->db->from('leases');
						$this->db->select('*');
						$rental_items = $this->db->get();

						if($rental_items->num_rows() > 0)
						{
							foreach ($rental_items->result() as $rental_items) {
								$lease_id = $rental_items->lease_id;
								$lease_status = $rental_items->lease_status;
									// update the details the status to 1
								$update_array = array('lease_status'=>0);
								$this->db->where('lease_id = '.$lease_id);
								$this->db->update('leases',$update_array);
							}
						}

						$items_row['lease_start_date'] = $newDate;
						$items_row['lease_duration'] = 24;
						$items_row['lease_number'] = $this->create_lease_number($rental_unit_id);
						$items_row['tenant_unit_id'] = $tenant_unit_id;
						$items_row['created'] = date('Y-m-d H:i:s');
						$items_row['lease_status'] = 1;
						$items_row['rental_unit_id'] = $rental_unit_id;
						$items_row['created_by'] = $this->session->userdata('personnel_id');
						$items_row['branch_code'] = $this->session->userdata('branch_code');

						$this->db->insert('leases', $items_row);
						$lease_id = $this->db->insert_id();

						$date = explode('-', $newDate);
						$month = $date[1];
						$year = $date[0];

					}
				}

			}



			$return['response'] = 'success';
			$return['check'] = TRUE;
		}

		//if no products exist
		else
		{
			$return['response'] = 'Member data not found ';
			$return['check'] = FALSE;
		}

		return $return;
	}


	public function invoice_details_import_template()
	{
		$this->load->library('Excel');

		$title = 'Invoice Details Import Template';
		$count=1;
		$row_count=0;
		$report[$row_count][0] = 'Lease Number';
		$report[$row_count][1] = 'Invoice Number';
		$report[$row_count][2] = 'Invoice Type Code';
		// $report[$row_count][3] = 'Schedule i.e Monthly 1, Quarterly 2, Once 3, Upon Billing 3';
		$report[$row_count][3] = 'Amount';
		$report[$row_count][4] = 'Invoice date';



		$row_count++;

		//create the excel document
		$this->excel->addArray ( $report );
		$this->excel->generateXML ($title);
	}


	public function import_csv_invoice_details($upload_path)
	{
		//load the file model
		$this->load->model('admin/file_model');
		/*
			-----------------------------------------------------------------------------------------
			Upload csv
			-----------------------------------------------------------------------------------------
		*/
		$response = $this->file_model->upload_csv($upload_path, 'import_csv');

		if($response['check'])
		{
			$file_name = $response['file_name'];

			$array = $this->file_model->get_array_from_csv($upload_path.'/'.$file_name);
			// var_dump($array); die();
			$response2 = $this->sort_invoice_details($array);

			if($this->file_model->delete_file($upload_path."\\".$file_name, $upload_path))
			{
			}

			return $response2;
		}
		else
		{
			$this->session->set_userdata('error_message', $response['error']);
			return FALSE;
		}
	}
	public function sort_invoice_details($array)
	{
		$total_rows = count($array);
		$total_columns = count($array[0]);
		// var_dump($total_columns); die();
		//if products exist in array
		if(($total_rows > 0) && ($total_columns == 5))
		{
			//retrieve the data from array
			for($r = 1; $r < $total_rows; $r++)
			{
				$lease_number = $array[$r][0];
				$lease_query = $this->get_lease_details($lease_number);

				if($lease_query->num_rows() > 0)
				{
					foreach ($lease_query->result() as $key => $value) {
						# code...
						$lease_id = $value->lease_id;
						$rental_unit_id = $value->rental_unit_id;
						$property_id = $value->property_id;
					}
				}
				// var_dump($lease_number); die();
				$invoice_type = $array[$r][2];
				$invoice_type_id = $this->get_invoice_type_id($invoice_type);
				$invoice_number = $array[$r][1];
				$invoice_date = $array[$r][4];
				$invoice_amount = $array[$r][3];
				$newDate = date("Y-m-d", strtotime($invoice_date));

				$explode = explode('-', $newDate);
				$invoice_year = $explode[0];
				$invoice_month = $explode[1];
				if(!empty($invoice_number))
				{
					$document_number = $invoice_number;
					$prefix = '';
					$suffix = '';
				}
				else
				{
					$prefix = 'INV-'.$invoice_year.'-'.$property_id;
					$suffix = $this->accounts_model->create_form_document_number($prefix);

					// $prefix = $prefix;
					if($suffix < 100)
					{
						if($suffix < 10)
						{
							$document_number = $prefix.'-00'.$suffix;
						}
						else
						{
							$document_number = $prefix.'-0'.$suffix;
						}
					}
					else
					{
						$document_number = $prefix.'-'.$suffix;
					}
					// create invoice number
					$document_number  = $document_number;
				}
				// var_dump($document_number); die();

				// check if the invoice nuber exisits
				$this->db->where('document_number',$document_number);
				$query = $this->db->get('lease_invoice');

				if($query->num_rows() == 1)
				{
					foreach ($query->result() as $key => $value) {
						# code...
						$lease_invoice_id = $value->lease_invoice_id;
						$invoice_year = $value->invoice_year;
						$invoice_month = $value->invoice_month;
					}
				}
				else
				{

					// create the invoice
					$insertarray['invoice_date'] = $newDate;
					$insertarray['invoice_year'] = $invoice_year;
					$insertarray['invoice_month'] = $invoice_month;
					$insertarray['lease_id'] = $lease_id;
					$insertarray['document_number'] = $document_number;
					$insertarray['created_by'] = $this->session->userdata('personnel_id');
					$insertarray['created'] = date('Y-m-d');
					$insertarray['prefix'] = $prefix;
					$insertarray['suffix'] = $suffix;

					$this->db->insert('lease_invoice',$insertarray);
					$lease_invoice_id = $this->db->insert_id();
				}

				$next_date = $invoice_year.'-'.$invoice_month.'-'.'01';
				$next_date = strtotime($next_date);
				$next_quarter = ceil(date('m', $next_date) / 3);
				$next_month = ($next_quarter * 3) - 2;
				$next_year = date('Y', $next_date);
				$next_quarter = 'AC'.$next_quarter.'-'.$next_year;

				$insert_array = array(
										'lease_id' => $lease_id,
										'lease_invoice_id' => $lease_invoice_id,
										'invoice_amount' => $invoice_amount,
										'created' => date('Y-m-d'),
										'invoice_type' => $invoice_type_id,
										'billing_schedule_quarter' => $next_quarter,
										'personnel_id'=>$this->session->userdata('personnel_id'),
										'tenant_id'=>$tenant_id
								 	 );
				$this->db->insert('invoice',$insert_array);

			}
			$return['response'] = 'success';
			$return['check'] = TRUE;
		}
		else
		{
			$return['response'] = 'Member data not found ';
			$return['check'] = FALSE;
		}

		return $return;
	}
	public function get_invoice_type_id($invoice_type_code)
	{
		$this->db->where('invoice_type.invoice_type_code = "'.$invoice_type_code.'"');
		$this->db->select('invoice_type_id');
		$query = $this->db->get('invoice_type');
		if($query->num_rows() > 0)
		{
			$item = $query->row();
			$invoice_type_id = $item->invoice_type_id;
			return $invoice_type_id;
		}
		else
		{
			return FALSE;
		}
	}

	public function other_data()
	{

			// 	$amount = $items['billing_amount'] = $array[$r][3];

			// 	$newDate = date("Y-m-d", strtotime($rental_start_date));
			// 	$lease_id = $items['lease_id'] = $array[$r][0];

			// 	$items['start_date'] = $newDate;
			// 	$items['property_billing_status'] = 1;
			// 	$items['charge_to'] = 1;
			// 	$items['arrears_bf'] = 0;
			// 	$items['initial_reading'] = 0;
			// 	$items['created'] = date('Y-m-d H-i-s');

			// 	$comment ='';

			// 	if(!empty($lease_id))

			// 	{

			// 		//check if the property billing already exists
			// 		if($this->check_property_billing_schedule_exists($lease_id,$invoice_type_id))
			// 		{
			// 			$return['response'] = 'Property Billing Already Exists';
			//             $return['check'] = FALSE;

			// 		}
			// 		else
			// 		{
			// 			if($this->check_schedule_exists($billing_schedule_id))
			// 			{
			// 				if($this->check_invoice_type_exists($invoice_type_id))
			// 			    {
			// 			    	$this->db->insert('property_billing', $items);
			// 			        $property_billing_id = $this->db->insert_id();

			// 			    }
			// 			    else
			// 			    {
			// 			    	$return['response'] = 'Billing Invoice Type Does not Exist';
			//                     $return['check'] = FALSE;

			// 			    }




			// 			}
			// 			else
			// 			{

			// 				$return['response'] = 'Billing Schedule Period Does not Exist';
			//                 $return['check'] = FALSE;

			// 			}



			// 		}
			// 	}

			// }
	}
	public function get_lease_start_date($lease_id)
	{
		$this->db->from('leases');
		$this->db->select('*');
		$this->db->where('lease_id = "'.$lease_id.'"');

		$query = $this->db->get();

		$lease_start_date = " ";

		if($query->num_rows() > 0 )
		{
			$result = $query->result();
			$lease_start_date = $result[0]->lease_start_date;
		}


		return $lease_start_date;

	}

	public function get_lease_details($lease_number)
	{
		$this->db->from('leases,rental_unit');
		$this->db->select('leases.lease_id,rental_unit.property_id,leases.lease_start_date,rental_unit.rental_unit_id');
		$this->db->where('leases.lease_number = "'.$lease_number.'" AND leases.rental_unit_id = rental_unit.rental_unit_id');

		$query = $this->db->get();

		return $query;

	}
	public function check_property_billing_schedule_exists($lease_id,$invoice_type_id)
	{
		$this->db->where(array ('lease_id'=> $lease_id,'invoice_type_id'=>$invoice_type_id));

		$query = $this->db->get('property_billing');

		if($query->num_rows() > 0)
		{
			return TRUE;
		}

		else
		{
			return FALSE;
		}
	}
	public function check_schedule_exists($billing_schedule_id)
	{
		$this->db->where(array ('billing_schedule_id'=> $billing_schedule_id));

		$query = $this->db->get('billing_schedule');

		if($query->num_rows() > 0)
		{
			return TRUE;
		}

		else
		{
			return FALSE;
		}
	}
	public function check_invoice_type_exists($invoice_type_id)
	{
		$this->db->where(array ('invoice_type_id'=> $invoice_type_id));

		$query = $this->db->get('invoice_type');

		if($query->num_rows() > 0)
		{
			return TRUE;
		}

		else
		{
			return FALSE;
		}
	}


	public function payment_details_import_template()
	{
		$this->load->library('Excel');

		$title = 'Payment Details Import Template';
		$count=1;
		$row_count=0;
		$report[$row_count][0] = 'Lease Number';
		$report[$row_count][1] = 'Amount';
		$report[$row_count][2] = 'Payment Date';
		$report[$row_count][3] = 'Invoice Type';
		$report[$row_count][4] = 'Paid By';




		$row_count++;

		//create the excel document
		$this->excel->addArray ( $report );
		$this->excel->generateXML ($title);
	}


	public function import_csv_payment_details($upload_path)
	{
		//load the file model
		$this->load->model('admin/file_model');
		/*
			-----------------------------------------------------------------------------------------
			Upload csv
			-----------------------------------------------------------------------------------------
		*/
		$response = $this->file_model->upload_csv($upload_path, 'import_csv');

		if($response['check'])
		{
			$file_name = $response['file_name'];

			$array = $this->file_model->get_array_from_csv($upload_path.'/'.$file_name);
			// var_dump($array); die();
			$response2 = $this->sort_payment_details($array);

			if($this->file_model->delete_file($upload_path."\\".$file_name, $upload_path))
			{
			}

			return $response2;
		}
		else
		{
			$this->session->set_userdata('error_message', $response['error']);
			return FALSE;
		}
	}
	public function sort_payment_details($array)
	{
		$total_rows = count($array);
		$total_columns = count($array[0]);
		// var_dump($total_columns); die();
		//if products exist in array
		if(($total_rows > 0) && ($total_columns == 5 ))
		{
			//retrieve the data from array
			for($r = 1; $r < $total_rows; $r++)
			{
				$lease_id = $items['lease_id'] = $array[$r][0];
				$amount = $items['amount_paid'] = $array[$r][1];
				$date = $items['payment_date'] = $array[$r][2];
				$payment_date = date("Y-m-d", strtotime($date));
				$expleded = explode('-', $payment_date);
							$month = $expleded[1];
							$year = $expleded[0];
				$invoice_type_code =  $array[$r][3];
				$invoice_type_id = $this->get_payment_code_id($invoice_type_code);
				$receipt_number = $this->create_receipt_number();
				$paid_by = $items['paid_by'] = $array[$r][4];
				$items['payment_method_id'] = 2;
				$items['payment_status'] = 1;
				$items['month'] = $month;
				$items['year'] = $year;
				$items['receipt_number'] = $receipt_number;
				$items['confirm_number'] = $receipt_number;
				$items['payment_created'] = date('Y-m-d H-i-s');
				$items['date_approved'] = date('Y-m-d');

				$comment ='';

				if(!empty($lease_id))

				{
					if($this->check_month_payment_exists($lease_id,$month,$year))
					{


					}
					else
					{
						$this->db->insert('payments', $items);
						$payment_id = $this->db->insert_id();
						$service = array(
									'lease_id'=>$lease_id,
									'payment_id'=>$payment_id,
									'amount_paid'=> $amount,
									'invoice_type_id' => $invoice_type_id,
									'payment_item_status' => 1,
									'payment_item_created' => date('Y-m-d')
								);
						$this->db->insert('payment_item',$service);


					}

				}

			}



			$return['response'] = 'success';
			$return['check'] = TRUE;
		}

		//if no products exist
		else
		{
			$return['response'] = 'Member data not found ';
			$return['check'] = FALSE;
		}

		return $return;
	}
	public function get_payment_code_id($invoice_type_code)
	{
		$this->db->from('invoice_type');
		$this->db->select('*');
		$this->db->where('invoice_type_code = "'.$invoice_type_code.'"');

		$query = $this->db->get();

		$result = $query->result();

		$invoice_type_id = 0;

		if($query->num_rows() > 0 )
		{
			$result = $query->result();
			$invoice_type_id = $result[0]->invoice_type_id;
		}

		return $invoice_type_id;

	}
	function create_receipt_number()
	{
		//select product code
		$preffix = "HA-RT-";
		$this->db->from('payments');
		$this->db->where("receipt_number LIKE '".$preffix."%' AND payment_status = 1");
		$this->db->select('MAX(receipt_number) AS number');
		$query = $this->db->get();//echo $query->num_rows();

		if($query->num_rows() > 0)
		{
			$result = $query->result();
			$number =  $result[0]->number;
			$real_number = str_replace($preffix, "", $number);
			$real_number++;//go to the next number
			$number = $preffix.sprintf('%03d', $real_number);
		}
		else{//start generating receipt numbers
			$number = $preffix.sprintf('%03d', 1);
		}

		return $number;
	}
	public function check_month_payment_exists($lease_id,$month,$year)
	{
		$this->db->where(array ('lease_id'=> $lease_id,'month'=>$month,'year'=>$year));

		$query = $this->db->get('payments');

		if($query->num_rows() > 0)
		{
			return TRUE;
		}

		else
		{
			return FALSE;
		}
	}
	public function create_form_lease_number($prefix)
	{
		//select product code
		$this->db->where('prefix LIKE  \'%'.$prefix.'%\'');
		$this->db->from('leases');
		$this->db->select('MAX(suffix) AS number');
		$this->db->order_by('lease_id','DESC');
		// $this->db->limit(1);
		$query = $this->db->get();
		// var_dump($query); die();
		if($query->num_rows() > 0)
		{
			$result = $query->result();
			$number =  $result[0]->number;
			$number++;

		}
		else{
			$number = 1;
		}
		// var_dump($number);die();
		return $number;
	}
	public function update_lease_billing($lease_id)
	{
		$table = 'tenants,tenant_unit,rental_unit,leases,property';
		$where = 'tenants.tenant_id = tenant_unit.tenant_id AND tenant_unit.rental_unit_id = rental_unit.rental_unit_id  AND tenant_unit.tenant_unit_id = leases.tenant_unit_id AND leases.lease_status = 1 AND leases.lease_deleted = 0 AND rental_unit.property_id = property.property_id AND leases.lease_id = '.$lease_id;

		$this->db->from($table);
		$this->db->select('leases.*,property.property_id');
		$this->db->where($where);
		$query = $this->db->get();

		if($query->num_rows() > 0)
		{
			// get the lease information
			foreach ($query->result() as $key) {
				# code...
				$lease_id = $key->lease_id;
				$lease_start_date = $key->lease_start_date;
				$rent_amount = $key->rent_amount;
				$inital_rent_amount = $key->rent_amount;
				$property_id = $key->property_id;
				$arreas_bf = $key->arrears_bf;
				$rental_unit_id = $key->rental_unit_id;
				$tenant_id = $key->tenant_id;
				$lease_number = $key->lease_number;

				// get the year and the month
				$date = explode('-', $lease_start_date);
				$month = $date[1];
				$year = $date[0];
				$x=0;
				$todays_month = date('m');

				// create a document number

				// $prefix = 'INV-'.date('Y').'-'.$property_id;
				$document_number = $this->accounts_model->create_invoice_number();


				$this->db->from('lease_invoice');
				$this->db->select('lease_invoice.*');
				$this->db->where('lease_id',$lease_id);
				$lease_items = $this->db->get();
				$checked = $lease_items->num_rows();

				if($checked == 0)
				{
					$insertarray['invoice_date'] = $date_invoiced = $lease_start_date;
					$insertarray['invoice_year'] = $year_invoiced = $year;
					$insertarray['invoice_month'] = $month_invoiced = $month;
				}
				else
				{
					$insertarray['invoice_date'] = $date_invoiced = date('Y-m-d');
					$insertarray['invoice_year'] = $year_invoiced = date('Y');
					$insertarray['invoice_month'] = $month_invoiced = date('m');
				}

				$insertarray['lease_id'] = $lease_id;
				$insertarray['rental_unit_id'] = $rental_unit_id;
				$insertarray['tenant_id'] = $tenant_id;
				$insertarray['account_id'] = $lease_number;
				$insertarray['document_number'] = $document_number;
				$insertarray['created_by'] = $this->session->userdata('personnel_id');
				$insertarray['created'] = date('Y-m-d');


				$datestring=''.$lease_start_date.' first day of last month';
				$dt=date_create($datestring);
				$previous = $dt->format('Y-m');
				$previous_date = explode('-', $previous);
				$previous_year = $previous_date[0];
				$previous_month = $previous_date[1];

				$todaym2 = date('m');
				$todaym = str_replace('00', '0', $todaym);
					// for the service charge

				$this->db->from('invoice_type,property_billing');
				$this->db->select('*');
				$this->db->where('invoice_type.invoice_type_id = property_billing.invoice_type_id AND property_billing.charge_to = 1 AND billing_schedule_id < 4  AND property_billing.property_billing_status = 1  AND property_billing.property_billing_deleted = 0 AND property_billing.lease_id = '.$lease_id);

				$invoice_type_query = $this->db->get();


				$start_date = strtotime('3 months ago');
				$start_quarter = ceil(date('m', $start_date) / 3);
				$start_month = ($start_quarter * 3) - 2;
				$start_year = date('Y', $start_date);


				$prev_quarter = 'AC'.$start_quarter.'-'.$start_year;

				// current items

				$current_date = strtotime(date('Y-m-d'));
				$current_quarter = ceil(date('m', $current_date) / 3);
				$current_month = ($current_quarter * 3) - 2;
				$current_year = date('Y', $current_date);

				$invoice_date_item = date('Y-m-d');

				$curr_quarter = 'AC'.$current_quarter.'-'.$current_year;

				// end of current querter
				if($invoice_type_query->num_rows() > 0)
				{
					$this->db->insert('lease_invoice',$insertarray);
					$lease_invoice_id = $this->db->insert_id();
						$total_amount_invoiced = 0;
					// var_dump($lease_invoice_id); die();
					foreach ($invoice_type_query->result() as $invoice_key){

						$invoice_type_id = $invoice_key->invoice_type_id;
						$billing_amount = $invoice_key->billing_amount;
						$tax_amount = $invoice_key->tax_amount;
						$billing_schedule_id = $invoice_key->billing_schedule_id;
						$invoice_type_name = $invoice_key->invoice_type_name;

						if($billing_schedule_id == 2)
						{

							$datestring=''.$invoice_date_item.' first day of next month';
							$dt=date_create($datestring);
							$next = $dt->format('Y-m-d');
							$next_date = explode('-', $next);
							$next_year = $next_date[0];
							$next_month = $next_date[1];
							$next_date = $next_year.'-'.$next_month.'-'.'01';
							$next_date = strtotime($next_date);
							$next_quarter = ceil(date('m', $next_date) / 3);
							$next_month = ($next_quarter * 3) - 2;
							$next_year = date('Y', $next_date);

							$next_quarter = 'AC'.$next_quarter.'-'.$next_year;

							$where = 'invoice.invoice_type = '.$invoice_type_id.' AND lease_invoice.invoice_deleted = 0  AND lease_invoice.lease_invoice_id = invoice.lease_invoice_id AND lease_id = '.$lease_id.' AND invoice.billing_schedule_quarter = "'.$next_quarter.'"';

							$this->db->from('invoice,lease_invoice');
							$this->db->select('*');
							$this->db->where($where);

						}
						else if($billing_schedule_id == 3)
						{
							$next_quarter = $curr_quarter;
							$this->db->from('invoice,lease_invoice');
							$this->db->select('*');
							$this->db->where('invoice.invoice_type = '.$invoice_type_id.' AND lease_invoice.invoice_deleted = 0 AND lease_invoice.lease_invoice_id = invoice.lease_invoice_id AND lease_invoice.lease_id = '.$lease_id.'');
						}
						else
						{
							$next_quarter = $curr_quarter;
							$this->db->from('invoice,lease_invoice');
							$this->db->select('*');
							$this->db->where('invoice.invoice_type = '.$invoice_type_id.' AND lease_invoice.invoice_deleted = 0  AND lease_invoice.lease_invoice_id = invoice.lease_invoice_id AND lease_invoice.invoice_month = "'.$month.'" AND lease_invoice.invoice_year = '.$year.' AND lease_invoice.lease_id = '.$lease_id.'');
						}

						$service_charge_query = $this->db->get();

						if($service_charge_query->num_rows() == 0 )
						{

							$invoice_amount = $billing_amount;

							$todaym2 = str_replace('00', '0', $todaym2);


							$concate = $year.'-'.$month;
							$date = date('F Y',strtotime($concate));

							$remarks = $invoice_type_name.' for '.$date;

							if($invoice_amount > 0)
							{

								$insert_array = array(
												'lease_id' => $lease_id,
												'year' => $year,
												'month' => $month,
												'lease_invoice_id' => $lease_invoice_id,
												'invoice_amount' => $invoice_amount,
												'invoice_type' => $invoice_type_id,
												'billing_schedule_quarter' => $next_quarter,
												'invoice_item_status'=>1,
												'tax_amount'=>$tax_amount,
												'created'=>$date_invoiced,
												'personnel_id'=>$this->session->userdata('personnel_id'),
												'created_by'=>$this->session->userdata('personnel_id'),
												'tenant_id'=>$tenant_id,
												'rental_unit_id'=>$rental_unit_id,
												'lease_number'=>$lease_number,
												'remarks'=>$remarks

										 	 );
								$this->db->insert('invoice',$insert_array);
								$total_amount_invoiced += $invoice_amount;

							}


						}

					}
					$update_array_lease['invoice_date'] = $lease_start_date;
					$update_array_lease['invoice_month'] = $month;
					$update_array_lease['invoice_year'] = $year;
					$update_array_lease['total_amount'] = $total_amount_invoiced;
					$this->db->where('lease_invoice_id',$lease_invoice_id);
					$this->db->update('lease_invoice',$update_array_lease);
				}
				//  add an in


			}


		}
		else
		{

		}
	}

	public function upload_any_file($path, $location, $name, $upload, $edit = NULL)
	{
		if(!empty($_FILES[$upload]['tmp_name']))
		{
			$image = $this->session->userdata($name);

			if((!empty($image)) || ($edit != NULL))
			{
				if($edit != NULL)
				{
					$image = $edit;
				}

				//delete any other uploaded image
				if($this->file_model->delete_file($path."\\".$image, $location))
				{
					//delete any other uploaded thumbnail
					$this->file_model->delete_file($path."\\thumbnail_".$image, $location);
				}

				else
				{
					$this->file_model->delete_file($path."/".$image, $location);
					$this->file_model->delete_file($path."/thumbnail_".$image, $location);
				}
			}
			//Upload image
			$response = $this->file_model->upload_any_file($path, $upload);

			// var_dump($response); die();/
			if($response['check'])
			{
				$file_name = $response['file_name'];

				//Set sessions for the image details
				$this->session->set_userdata($name, $file_name);

				return TRUE;
			}

			else
			{
				$this->session->set_userdata('upload_error_message', $response['error']);

				return FALSE;
			}
		}

		else
		{
			$this->session->set_userdata('upload_error_message', '');
			return FALSE;
		}
	}

	function upload_lease_documents($lease_id, $document)
	{
		$data = array(
			'document_type_id'=> $this->input->post('document_type_id'),
			'remarks'=> $this->input->post('remarks'),
			'document'=> $document,
			'created_by'=> $this->session->userdata('personnel_id'),
			'modified_by'=> $this->session->userdata('personnel_id'),
			'created'=> date('Y-m-d H:i:s'),
			'lease_id'=>$lease_id
		);

		if($this->db->insert('lease_document_uploads', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}

	public function all_document_types()
	{
		$this->db->order_by('document_type_name');
		$query = $this->db->get('document_type');

		return $query;
	}

	function get_document_uploads($lease_id)
	{
		$this->db->from('lease_document_uploads, document_type');
		$this->db->select('*');
		$this->db->where('lease_document_uploads.document_type_id = document_type.document_type_id AND lease_id = '.$lease_id);
		$query = $this->db->get();

		return $query;
	}

	function get_lease_closing_invoices($lease_id)
	{
		$this->db->from('invoice');
		$this->db->select('*,invoice.remarks as invoice_remarks');
		$this->db->where('lease_invoice_id = 0 AND lease_id = '.$lease_id);
		$this->db->join('invoice_type','invoice.invoice_type = invoice_type.invoice_type_id','left');
		$query = $this->db->get();

		return $query;
	}

	public function get_lease_statuses($lease_number,$rental_unit_id)
	{

		$this->db->from('rental_unit,leases,property,tenant_unit');
		$this->db->select('rental_unit_name,lease_id,property_name');
		$this->db->where('property.property_id = rental_unit.property_id AND leases.lease_status = 1 AND tenant_unit.tenant_unit_id = leases.tenant_unit_id  AND rental_unit.rental_unit_id = leases.rental_unit_id AND leases.lease_number = '.$lease_number.'');
		$query = $this->db->get();

		return $query;

	}


	public function get_other_houses_on_property($rental_unit_id,$property_id)
	{

		$this->db->from('rental_unit,property');
		$this->db->select('rental_unit_name,property_name,rental_unit_id');
		$this->db->where('property.property_id = rental_unit.property_id AND rental_unit.property_id = '.$property_id.' AND rental_unit.rental_unit_id <> '.$rental_unit_id);		
		// $this->db->join('leases','leases.rental_unit_id = rental_unit.rental_unit_id','left');
		$this->db->order_by('rental_unit.rental_unit_number,rental_unit.rental_unit_name','ASC');

		$query = $this->db->get();

		return $query;

	}
}
?>
