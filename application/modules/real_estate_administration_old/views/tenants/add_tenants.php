<div class="box">
    <div class="box-header with-border">
      <h3 class="box-title"><?php echo $title;?></h3>

      <div class="box-tools pull-right">
			     <a href="<?php echo site_url().'lease-manager/tenants'?>" class="btn btn-sm btn-warning " ><i class="fa fa arrow-left"></i> Back to Tenants</a>

      </div>
    </div>
    <div class="box-body">
      <div class="row" style="margin-bottom:20px;">
        <div class="col-lg-12 col-sm-12 col-md-12">
          <div class="row">
          <?php
              echo form_open("add-tenant", array("class" => "form-horizontal", "role" => "form"));
          ?>
              <div class="col-md-12">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                    <label class="col-lg-5 control-label">Tenant Name: </label>

                    <div class="col-lg-7">
                      <input type="text" class="form-control" name="tenant_name" placeholder="Name" value="" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-5 control-label">National id: </label>

                    <div class="col-lg-7">
                      <input type="text" class="form-control" name="tenant_national_id" placeholder="National ID" value="" >
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-5 control-label">Occupation: </label>

                    <div class="col-lg-7">
                      <select class="form-control select2" name="occupation_id" >
                          <option value="">-- Select Occupation --</option>
                          <?php
                          if($occupations->num_rows() > 0)
                          {
                              $occupation = $occupations->result();

                              foreach($occupation as $res)
                              {
                                  $occupation_id = $res->occupation_id;
                                  $occupation_name = $res->occupation_name;
                                  echo '<option value="'.$occupation_id.'">'.$occupation_name.'</option>';
                              }
                          }
                      ?>
                      </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-5 control-label">Place of Work: </label>

                    <div class="col-lg-7">
                      <input type="text" class="form-control" name="place_of_work" placeholder="Place of Work" value="" required>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                    <label class="col-lg-5 control-label">Phone number: </label>

                    <div class="col-lg-7">
                      <input type="text" class="form-control" name="tenant_phone_number" placeholder="Phone" value="" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-5 control-label">Email address: </label>

                    <div class="col-lg-7">
                      <input type="text" class="form-control" name="tenant_email" placeholder="Email address" value="">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-5 control-label">Next of Kin: </label>

                    <div class="col-lg-7">
                      <input type="text" class="form-control" name="next_of_kin_name" placeholder="Next of Kin Name" value="">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-5 control-label">Next of Kin Phone: </label>

                    <div class="col-lg-7">
                      <input type="text" class="form-control" name="next_of_kin_phone" placeholder="next of kin phone" value="">
                    </div>
                </div>
            </div>
        </div>
          <div class="row" style="margin-top:10px;">
          <div class="col-md-12">
                <div class="form-actions text-center">
                    <button class="submit btn btn-primary" type="submit">
                        Add tenant
                    </button>
                </div>
            </div>
        </div>
              </div>
              <?php echo form_close();?>
              <!-- end of form -->
            </div>


        </div>

      </div>
</div>
  </div>
</div>
