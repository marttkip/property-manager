<?php
foreach ($query->result() as $key) {
	$tenant_id = $key->tenant_id;
	$tenant_national_id = $key->tenant_national_id;
	$tenant_name = $key->tenant_name;
	$tenant_phone_number = $key->tenant_phone_number;
	$tenant_email = $key->tenant_email;
	$tenant_next_of_kin = $key->tenant_next_of_kin;
	$tenant_next_of_kin_no = $key->tenant_next_of_kin_no;
	$occupation_id = $key->occupation_id;
	$place_of_work = $key->place_of_work;
}
// var_dump($occupation_id); die();
?>

<div class="box">
    <div class="box-header with-border">
      <h3 class="box-title">Edit <?php echo $tenant_name;?></h3>

      <div class="box-tools pull-right">
      	 <a href="<?php echo site_url();?>lease-manager/tenants" class="btn btn-info pull-right">Back to tenants</a>
      </div>
    </div>
    <div class="box-body">

		<div class="row" >
			<div class="col-lg-12 col-sm-12 col-md-12">
				<div class="row">
				<?php echo form_open("edit-tenant/".$tenant_id, array("class" => "form-horizontal", "role" => "form"));?>
    				<div class="col-md-12">
    					<div class="row">
        					<div class="col-md-6">
            					<div class="form-group">
						            <label class="col-lg-5 control-label">Tenant Name: </label>

						            <div class="col-lg-7">
						            	<input type="text" class="form-control" name="tenant_name" placeholder="Name" value="<?php echo $tenant_name;?>">
						            </div>
						        </div>
						        <div class="form-group">
						            <label class="col-lg-5 control-label">National id: </label>

						            <div class="col-lg-7">
						            	<input type="text" class="form-control" name="tenant_national_id" placeholder="National ID" value="<?php echo $tenant_national_id;?>">
						            </div>
						        </div>
										<div class="form-group">
		                    <label class="col-lg-5 control-label">Occupation: </label>

		                    <div class="col-lg-7">
		                      <select class="form-control select2" name="occupation_id" >
		                          <option value="">-- Select Occupation --</option>
		                          <?php
		                          if($occupations->num_rows() > 0)
		                          {
		                              $occupation = $occupations->result();

		                              foreach($occupation as $res)
		                              {
		                                  $dboccupation_id = $res->occupation_id;
		                                  $occupation_name = $res->occupation_name;
																			if($dboccupation_id == $occupation_id)
																			{
																				echo '<option value="'.$occupation_id.'" selected="selected">'.$occupation_name.'</option>';
																			}
																			else {
																				echo '<option value="'.$occupation_id.'">'.$occupation_name.'</option>';
																			}

		                              }
		                          }
		                      ?>
		                      </select>
		                    </div>
		                </div>
		                <div class="form-group">
		                    <label class="col-lg-5 control-label">Place of Work: </label>

		                    <div class="col-lg-7">
		                      <input type="text" class="form-control" name="place_of_work" placeholder="Place of Work" value="<?php echo $place_of_work?>" >
		                    </div>
		                </div>
						    </div>
						    <div class="col-md-6">
						    	<div class="form-group">
						            <label class="col-lg-5 control-label">Phone number: </label>

						            <div class="col-lg-7">
						            	<input type="text" class="form-control" name="tenant_phone_number" placeholder="Phone" value="<?php echo $tenant_phone_number;?>">
						            </div>
						        </div>
						        <div class="form-group">
						            <label class="col-lg-5 control-label">Email address: </label>

						            <div class="col-lg-7">
						            	<input type="text" class="form-control" name="tenant_email" placeholder="Email address" value="<?php echo $tenant_email;?>">
						            </div>
						        </div>
										<div class="form-group">
		                    <label class="col-lg-5 control-label">Next of Kin: </label>

		                    <div class="col-lg-7">
		                      <input type="text" class="form-control" name="next_of_kin_name" placeholder="Next of Kin Name" value="<?php echo $tenant_next_of_kin?>">
		                    </div>
		                </div>
		                <div class="form-group">
		                    <label class="col-lg-5 control-label">Next of Kin Phone: </label>

		                    <div class="col-lg-7">
		                      <input type="text" class="form-control" name="next_of_kin_phone" placeholder="next of kin phone" value="<?php echo $tenant_next_of_kin_no?>">
		                    </div>
		                </div>
						    </div>
						</div>
					    <div class="row" style="margin-top:10px;">
							<div class="col-md-12">
						        <div class="form-actions text-center">
						            <button class="submit btn btn-primary" type="submit">
						                Edit tenant
						            </button>
						        </div>
						    </div>
						</div>
    				</div>
    				<?php echo form_close();?>
    				<!-- end of form -->
    			</div>


			</div>

		</div>
	</div>
</div>
