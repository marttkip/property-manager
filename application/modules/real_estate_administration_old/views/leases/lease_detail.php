<?php

$all_leases = $this->leases_model->get_lease_detail($lease_id);
// var_dump($all_leases->result()); die();
	foreach ($all_leases->result() as $leases_row)
	{
		$lease_id = $leases_row->lease_id;
		$tenant_id = $leases_row->tenant_id;
		$tenant_unit_id = $leases_row->tenant_unit_id;
		$property_name = $leases_row->property_name;
		$rental_unit_name = $leases_row->rental_unit_name;
		$rental_unit_price = $leases_row->rental_unit_price;
		$tenant_name = $leases_row->tenant_name;
		$lease_start_date =$leases_row->lease_start_date;
		$lease_duration = $leases_row->lease_duration;
		$rent_amount = $leases_row->rent_amount;
		$lease_number = $leases_row->lease_number;
		$arreas_bf = $leases_row->arrears_bf;
		$rent_calculation = $leases_row->rent_calculation;
		$deposit = $leases_row->deposit;
		$deposit_ext = $leases_row->deposit_ext;
		$tenant_phone_number = $leases_row->tenant_phone_number;
		$tenant_email = $leases_row->tenant_email;
		$tenant_national_id = $leases_row->tenant_national_id;
		$lease_status = $leases_row->lease_status;
		$lease_number = $leases_row->lease_number;
		$created = $leases_row->created;
		$expense_amount = $leases_row->expense_amount;
		$closing_end_date = $leases_row->closing_end_date;
		$remarks = $leases_row->remarks;
		$closing_water_reading = $leases_row->closing_water_reading;
		$lease_end_date = $leases_row->lease_end_date;
		$vacated_on = $leases_row->vacated_on;
		$notice_date = $leases_row->notice_date;
		$property_id = $leases_row->property_id;
		$tenant_number = $leases_row->tenant_number;
		$transfer_id = $leases_row->transfer_id;

		$rental_unit_id = $leases_row->rental_unit_id;


		// $lease_start_date = date('jS M Y',strtotime($lease_start_date));

		// $expiry_date  = date('jS M Y',strtotime($lease_start_date, mktime()) . " + 365 day");
		// $lease_end_date  = date('jS M Y', strtotime($lease_end_date));

		$total_due = $rent_amount*12;

		$total_paid = 6000;
		$lease_start = $leases_row->lease_start_date;
		// var_dump($leases_row); die();


		//create deactivated status display
		if($lease_status == 0)
		{
			$status = '<span class="label label-default"> Inactive Lease</span>';

			$button = '';
			$delete_button = '';
		}
		//create activated status display
		else if($lease_status == 1)
		{
			$status = '<span class="label label-success">Active Lease</span>';
			$button = '<td><a class="btn btn-default" href="'.site_url().'deactivate-rental-unit/'.$lease_id.'" onclick="return confirm(\'Do you want to deactivate '.$lease_number.'?\');" title="Deactivate '.$lease_number.'"><i class="fa fa-thumbs-down"></i></a></td>';
			$delete_button = '<td><a href="'.site_url().'deactivate-rental-unit/'.$lease_id.'" class="btn btn-sm btn-danger" onclick="return confirm(\'Do you really want to delete '.$lease_number.'?\');" title="Delete '.$lease_number.'"><i class="fa fa-trash"></i></a></td>';

		}
		else if($lease_status == 2)
		{
			$status = '<span class="label label-warning">New Lease</span>';
		}
		else if($lease_status == 3)
		{
				$status = '<span class="label label-warning">Lease on Notice</span>';
		}
		else if($lease_status == 4)
		{
				$status = '<span class="label label-danger">Lease has been closed</span>';
		}
		else
		{
				$status = '<span class="label label-danger"></span>';
		}


	}

	$amount_to_return = $this->accounts_model->get_deposits_paid($lease_id);
	if(empty($amount_to_return))
	{
		$amount_to_return = 0;
	}

	$expenses_payable = $this->accounts_model->get_expenses_payable($lease_id);
	if(empty($expenses_payable))
	{
		$expenses_payable = 0;
	}


	$tenants_response = $this->accounts_model->get_lease_balance($lease_id);
	$total_arrears = $tenants_response['balance'];
	$invoiced = $tenants_response['invoiced'];
	$waived = $tenants_response['waived'];
	$paid = $tenants_response['paid'];

	if($lease_end_date == '0000-00-00')
	{
		$lease_end_date = '';
	}

	$success = $this->session->userdata('success_message');

if(!empty($success))
{
echo '<div class="alert alert-success"> <strong>Success!</strong> '.$success.' </div>';
$this->session->unset_userdata('success_message');
}

$error = $this->session->userdata('error_message');

if(!empty($error))
{
echo '<div class="alert alert-danger"> <strong>Oh snap!</strong> '.$error.' </div>';
$this->session->unset_userdata('error_message');
}


if($lease_status >= 1 AND $lease_status <= 3)
{

?>
<!-- <div class="row"> -->
	<div class="col-md-12">
		<a href="<?php echo site_url();?>lease-manager/leases" class="btn btn-sm btn-warning pull-right"  ><i class="fa fa-arrow-left"></i> Back to leases</a>
		<a href="<?php echo site_url();?>print-tenant-statement/<?php echo $lease_number;?>/<?php echo $lease_id;?>" target="_blank" class="btn btn-sm btn-success" > <i class="fa fa-print"></i> Tenant's Statement</a>
		
		<a href="<?php echo site_url();?>view-tenant-invoices/<?php echo $lease_number.'/'.$lease_id;?>"  class="btn btn-sm btn-primary" > <i class="fa fa-print"></i> Tenant Invoices</a>
		<a href="<?php echo site_url();?>view-tenant-credits/<?php echo $lease_number.'/'.$lease_id;?>"  class="btn btn-sm btn-info" > <i class="fa fa-print"></i> Tenant Credit Notes</a>
		<a href="<?php echo site_url();?>view-tenant-debits/<?php echo $lease_number.'/'.$lease_id;?>"  class="btn btn-sm btn-warning" > <i class="fa fa-print"></i> Tenant Debit Notes</a>

	</div>
<!-- </div> -->
<br/>
<?php

}
else if($lease_status == 4)
{

	?>
	<!-- <div class="row"> -->
		<div class="col-md-12">
			<a href="<?php echo site_url();?>lease-manager/tenants" class="btn btn-sm btn-warning pull-right"  ><i class="fa fa-arrow-left"></i> Back to tenants lists</a>
			<a href="<?php echo site_url();?>print-tenant-statement/<?php echo $lease_number;?>/<?php echo date('Y');?>" target="_blank" class="btn btn-sm btn-success" > <i class="fa fa-print"></i> Tenant's Statement</a>
		</div>
	<!-- </div> -->

	<?php
}
?>



<br/>
<div class="row" >
	<div class="col-md-12">
		<div class="col-md-4">
			<div class="box box-info">
			    <div class="box-header with-border">
			      <h3 class="box-title">Lease Detail</h3>

			      <div class="box-tools pull-right">

			      </div>
			    </div>
			    <div class="box-body">
					<div class="table-responsive">
						<?php echo form_open("update-lease-details/".$lease_id, array("class" => "form-horizontal", "role" => "form"));?>
						<table class="table table-bordered table-striped table-condensed">
							<thead>
								<tr>
									<th style="width: 40%;">Title</th>
									<th style="width: 60%;">Detail</th>
								</tr>
							</thead>
						  	<tbody>
						  		<tr><td><span>Lease Number :</span></td><td><?php echo $lease_number;?></td></tr>
						  		<tr><td><span>Lease Status :</span></td><td><?php echo $status;?></td></tr>
						  		<tr><td><span>Property Name :</span></td><td><?php echo $property_name;?> - <?php echo $rental_unit_name;?></td></tr>
						  		<tr><td><span>Rental Unit :</span></td><td><?php echo $rental_unit_name;?></td></tr>
						  		<tr><td><span>Lease Start date :</span></td><td>
			                        <div class="input-group">
			                            <span class="input-group-addon">
			                                <i class="fa fa-calendar"></i>
			                            </span>
			                            <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="lease_start_date" value="<?php echo $lease_start;?>" placeholder="Report Date From" id="datepicker">
			                        </div>
						  			</td></tr>
						  		<tr><td><span>Lease Duration :</span></td>
						  			<td>
						  				<div class="input-group">
				                            <input type="text" class="form-control" name="lease_duration" value="<?php echo $lease_duration;?>"></td>
				                        </div>
						  			</td>
						  		</tr>




						  	</tbody>
						</table>

						  <input type="hidden" class="form-control" name="redirect_url" placeholder="" autocomplete="off" value="<?php echo $this->uri->uri_string()?>">
						  <?php
						  if($lease_status <= 2)
						  {
						  ?>
							<div class="col-md-12">
								<div class="form-actions text-center">
						            <button class="submit btn btn-primary btn-sm" type="submit">
						                UPDATE DETAILS
						            </button>
						        </div>
							</div>
							<?php
						}
							?>

            			<?php echo form_close();?>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="box box-info">
			    <div class="box-header with-border">
			      <h3 class="box-title">Tenancy Info</h3>

			      <div class="box-tools pull-right">

			      </div>
			    </div>
			    <div class="box-body">
					<div class="table-responsive">
						<?php echo form_open("update-tenant-details/".$tenant_id, array("class" => "form-horizontal", "role" => "form"));?>
						<table class="table table-bordered table-striped table-condensed">
							<thead>
								<tr>
									<th style="width: 40%;">Title</th>
									<th style="width: 60%;">Detail</th>
								</tr>
							</thead>
						  	<tbody>
									<tr><td><span>Tenant Code :</span></td><td>
										<input type="text" class="form-control" name="tenant_numner" value="<?php echo $tenant_number;?>" readonly></td></tr>
						  		<tr><td><span>Tenant Name :</span></td><td>
						  			<input type="text" class="form-control" name="tenant_name" value="<?php echo $tenant_name;?>"></td></tr>
						  		<tr><td><span>Tenant Phone :</span></td><td>
						  			<input type="text" class="form-control" name="tenant_phone_number" value="<?php echo $tenant_phone_number;?>"></td></tr>
						  		<tr>
						  			<td><span>Tenant National Id :</span></td><td>
						  			<input type="text" class="form-control" name="tenant_national_id" value="<?php echo $tenant_national_id;?>">
						  			</td>
						  		</tr>
						  		<tr>
						  			<td><span>Tenant Email :</span></td><td>
						  			<input type="text" class="form-control" name="tenant_email" value="<?php echo $tenant_email;?>">
						  			</td>
						  		</tr>

						  	</tbody>
						  </table>
						  <input type="hidden" class="form-control" name="redirect_url" placeholder="" autocomplete="off" value="<?php echo $this->uri->uri_string()?>">
						  <?php
						  if($lease_status <= 2)
						  {
						  ?>
						  <div class="col-md-12">
								<div class="form-actions text-center">
						            <button class="submit btn btn-primary btn-sm" type="submit">
						                UPDATE DETAILS
						            </button>
						        </div>
							</div>
							<?php
						}
							?>

            			<?php echo form_close();?>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="box box-info">
			    <div class="box-header with-border">
			      <h3 class="box-title">Account </h3>

			      <div class="box-tools pull-right">

			      </div>
			    </div>
			    <div class="box-body">
					<div class="table-responsive">
						<table class="table table-bordered table-striped table-condensed">
							<thead>
								<tr>
									<th>Title</th>
									<th>Detail</th>
								</tr>
							</thead>
						  	<tbody>
						  		<tr><td><span>Deposits Paid :</span></td><td><strong>KES. <?php echo number_format(($amount_to_return),2);?></strong> </td></tr>
						  		<tr><td><span>Total Invoices :</span></td><td><strong>KES. <?php echo number_format($invoiced,2);?></strong> </td></tr>
						  		<tr><td><span>Total Debit Notes :</span></td><td>KES <?php echo number_format($waived,2);?></td></tr>
						  		<tr><td><span>Total Credit Notes :</span></td><td><strong>KES. (<?php echo number_format(($waived),2);?>)</strong> </td></tr>
						  		<tr><td><span>Total Payments :</span></td><td><strong>KES. (<?php echo number_format(($paid),2);?>)</strong> </td></tr>
						  		<tr><td><span>Total Balance :</span></td><td><strong>KES. <?php echo number_format($total_arrears,2);?></strong> </td></tr>


						  	</tbody>
						  </table>
						  <?php
						  if($lease_status <= 2)
						  {
						  ?>
						  <div class="col-md-12">
								<div class="form-actions text-center">
						            <a href="<?php echo site_url()?>update-billing/<?php echo $lease_id?>" class="btn btn-primary btn-sm" onclick="return confirm('Do you really want to update billing?');">
						                UPDATE ACCOUNT
						            </a>
						        </div>
							</div>
						<?php
						}
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<div class="row">
	<div class="col-md-12">
		<div class="col-md-12">
			<div class="box box-info">
			    <div class="box-header with-border">
			      <h3 class="box-title">Invoicing Detail</h3>

			      <div class="box-tools pull-right">

			      </div>
			    </div>
			    <div class="box-body">
					<div class="row" style="margin-bottom:20px;">
						<?php
						  if($lease_status <= 2)
						  {
						  ?>
						<?php echo form_open("add-lease-charge/".$lease_id, array("class" => "form-horizontal", "role" => "form"));?>
            			<div class="col-lg-12 col-sm-12 col-md-12">
            				<div class="row">
                				<div class="col-md-12">

                					<div class="row">
                    					<div class="col-md-4">
                        					<div class="form-group ">
									            <label class="col-lg-3 control-label">Invoice type: </label>

									            <div class="col-lg-9">
									            	<select id='invoice_type_id' name='invoice_type_id' class='form-control select2 ' required="required">
								                    <!-- <select class="form-control custom-select " id='procedure_id' name='procedure_id'> -->
								                      <option value=''>None - Please Select an invoice type</option>
								                      <?php echo $invoice_type_list;?>
								                    </select>
									            </div>
									        </div>
									    </div>
									    <div class="col-md-4">
									        <div class="form-group ">
									            <label class="col-lg-3 control-label">Billing Schedule: </label>

									            <div class="col-lg-9">
									            	<select id='billing_schedule_id' name='billing_schedule_id' class='form-control select2 ' required="required">
								                    <!-- <select class="form-control custom-select " id='procedure_id' name='procedure_id'> -->
								                      <option value=''>None - Please Select a billing schedule</option>
								                      <?php echo $billing_schedule_list;?>
								                    </select>
									            </div>
									        </div>
									    </div>
									    <div class="col-md-4">
									        <div class="form-group ">
									            <label class="col-lg-3 control-label">Amount/Rate : </label>

									            <div class="col-lg-9">
									            	<input type="text" class="form-control" name="amount" value="" required="required">

									            </div>
									        </div>
									    </div>
									    <div class="col-md-4">
									        <div class="form-group ">
									            <label class="col-lg-3 control-label">Arrears : </label>

									            <div class="col-lg-9">
									            	<input type="text" class="form-control" name="arrears" value="">
									            </div>
									        </div>
									    </div>
									    <div class="col-md-4">
									        <div class="form-group ">
									            <label class="col-lg-3 control-label">Initial Reading : </label>

									            <div class="col-lg-9">
									            	<input type="text" class="form-control" name="initial_reading" value="">
									            </div>
									        </div>
									    </div>
									    <div class="col-md-4">
									        <div class="form-group ">
									            <label class="col-lg-3 control-label">Start Date : </label>

									            <div class="col-lg-9">
									            	<div class="input-group">
							                            <span class="input-group-addon">
							                                <i class="fa fa-calendar"></i>
							                            </span>
							                            <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="start_date" value="<?php echo $lease_start;?>" placeholder="Start Date From" required="required">
							                        </div>
									            </div>
									        </div>
									    </div>
									    </div>
									</div>
									<input type="hidden" name="lease_number" value="<?php echo $lease_number?>">
								    <div class="row" style="margin-top:10px;">
										<div class="col-md-12">
									        <div class="form-actions text-center">
									            <button class="submit btn btn-primary btn-sm" type="submit" onclick="return confirm('Are you sure you want to proceed with this action ?')">
									                Add Billing Detail
									            </button>
									        </div>
									    </div>
									</div>
                				</div>
                			</div>

            			</div>
            			<?php echo form_close();?>
                				<!-- end of form -->
                		<?php
                		}
                		?>

            		</div>
            		<div class="row">
        				<div class="col-md-12">
        					<table class="table table-bordered table-striped table-condensed">
								<thead>
									<tr>
										<th style="width: 5%;">#</th>
										<th style="width: 20%;">Charge</th>
										<th style="width: 20%;">Schedule</th>
										<th style="width: 10%;">Arrears</th>
										<th style="width: 10%;">Amount/Rate</th>
										<th style="width: 10%;">Initial reading</th>
										<th style="width: 10%;">Date</th>
										<th style="width: 15%;">Action</th>
									</tr>
								</thead>
							  	<tbody>
							  		<?php
							  		if($query_invoice->num_rows() > 0)
							  		{
							  			$x = 0;
							  			foreach ($query_invoice->result() as $key => $value) {
								  			# code...
							  				$billing_schedule_id = $value->billing_schedule_id;
							  				$property_billing_id = $value->property_billing_id;
							  				$invoice_type_id = $value->invoice_type_id;
							  				$invoice_type_name = $value->invoice_type_name;
							  				$billing_schedule_name = $value->billing_schedule_name;
							  				$billing_start_date = $value->start_date;
							  				$arrears = $value->arrears_bf;
							  				$billing_amount = $value->billing_amount;
							  				$initial_water_meter_reading = $value->initial_reading;

										  if($lease_status < 2)
										  {

										  	$buttons = '<button type="button" class="btn btn-sm btn-info" data-toggle="modal" data-target="#refund_payment'.$property_billing_id.'"><i class="fa fa-pencil"></i></button>
														<a href="'.site_url().'delete-billing/'.$property_billing_id.'/'.$lease_id.'" class="btn btn-sm btn-danger" onclick="return confirm(\'Do you really want to delete?\');" title="Delete "><i class="fa fa-trash"></i></a>';
										  }
										  else
										  {
										  	$buttons ='';
										  }
							  				$x++;
							  				echo '	<tr>
											  			<td>'.$x.'</td>
											  			<td>'.$invoice_type_name.'</td>
											  			<td>'.$billing_schedule_name.'</td>
											  			<td>'.$arrears.'</td>
											  			<td>'.$billing_amount.'</td>
											  			<td>'.$initial_water_meter_reading.'</td>
											  			<td>'.$billing_start_date.'</td>
											  			<td>

											  				'.$buttons.'
															<!-- Modal -->
															<div class="modal fade" id="refund_payment'.$property_billing_id.'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
															    <div class="modal-dialog" role="document">
															        <div class="modal-content">
															            <div class="modal-header">
															            	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
															            	<h4 class="modal-title" id="myModalLabel">Edit '.$invoice_type_name.' Billing</h4>
															            </div>
															            <div class="modal-body">
															            	'.form_open("update-billing-item/".$property_billing_id."/".$invoice_type_id, array("class" => "form-horizontal","id"=>"payments-paid-form")).'
															                <div class="row">
										                    					<div class="col-md-12">

																			        <div class="form-group ">
																			            <label class="col-lg-3 control-label">Billing Schedule: </label>

																			            <div class="col-lg-9">
																			            	<select id="billing_schedule_id" name="billing_schedule_id" class="form-control custom-select " required>
																		                    <!-- <select class="form-control custom-select " id="procedure_id" name="procedure_id"> -->
																		                      <option value="">None - Please Select a billing schedule</option>
																		                      '.$billing_schedule_list.'
																		                    </select>
																			            </div>
																			        </div>
																			        <div class="form-group ">
																			            <label class="col-lg-3 control-label">Amount/Rate : </label>

																			            <div class="col-lg-9">
																			            	<input type="text" class="form-control" name="amount" value="'.$billing_amount.'">

																			            </div>
																			        </div>
																			        <div class="form-group ">
																			            <label class="col-lg-3 control-label">Arrears : </label>

																			            <div class="col-lg-9">
																			            	<input type="text" class="form-control" name="arrears" value="'.$arrears.'">
																			            </div>
																			        </div>
																			        <div class="form-group ">
																			            <label class="col-lg-3 control-label">Initial Reading : </label>

																			            <div class="col-lg-9">
																			            	<input type="text" class="form-control" name="initial_reading" value="'.$initial_water_meter_reading.'">
																			            </div>
																			        </div>
																			        <div class="form-group ">
																			            <label class="col-lg-3 control-label">Start Date : </label>

																			            <div class="col-lg-9">
																			            	<div class="input-group">
																	                            <span class="input-group-addon">
																	                                <i class="fa fa-calendar"></i>
																	                            </span>
																	                            <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="start_date" value="'.$billing_start_date.'" placeholder="Start Date From" >
																	                        </div>
																			            </div>
																			        </div>

																			    </div>
																			</div>
																			 <input type="hidden" class="form-control" name="redirect_url" placeholder="" autocomplete="off" value="'.$this->uri->uri_string().'">

															                <div class="row">
															                	<div class="col-md-8 col-md-offset-4">
															                    	<div class="text-center">
															                        	<button type="submit" class="btn btn-primary">Save action</button>
															                        </div>
															                    </div>
															                </div>
															                '.form_close().'
															            </div>
															            <div class="modal-footer">
															                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
															            </div>
															        </div>
															    </div>
															</div>
											  			</td>
											  		</tr>';
								  		}

							  		}

							  		?>
							  	</tbody>
							</table>
        				</div>
        			</div>
				</div>
			</div>
		</div>
		<div class="col-md-12">
				<div class="col-md-4">
					<div class="box box-success">
					    <div class="box-header with-border">
					      <h3 class="box-title">Lease Documents</h3>

					      <div class="box-tools pull-right">

					      </div>
					    </div>
					    <div class="box-body">
							<div class="row" style="margin-bottom:20px;">
								<?php
								if($lease_status < 4)
								{
								?>
								<?php echo form_open_multipart("real_estate_administration/leases/upload_documents/".$lease_id, array("class" => "form-horizontal", "role" => "form"));?>
		            			<div class="col-lg-12 col-sm-12 col-md-12">
		            				<div class="row">
		                				<div class="col-md-12">
		                					<div class="row">
												<div class="col-md-12">
			    									<div class="col-md-12">
												        <input type="hidden" class="form-control" name="redirect_url" placeholder="" autocomplete="off" value="<?php echo $this->uri->uri_string()?>">
												        <div class="form-group ">
												            <label class="col-lg-3 control-label">Type: </label>

												            <div class="col-lg-9">
																<select id='document_type_id' name='document_type_id' class='form-control select2 '>
																	<option value=''>None - </option>
																	<?php
									                                    if($document_types->num_rows() > 0)
									                                    {
									                                        foreach($document_types->result() as $res)
									                                        {
									                                            $document_type_id2 = $res->document_type_id;
									                                            $document_type_name = $res->document_type_name;

									                                            echo '<option value="'.$document_type_id2.'">'.$document_type_name.'</option>';
									                                        }
									                                    }
									                                ?>

																</select>
												            </div>

												        </div>
													</div>
													<div class="col-md-12">
												        <input type="hidden" class="form-control" name="redirect_url" placeholder="" autocomplete="off" value="<?php echo $this->uri->uri_string()?>">
												        <div class="form-group ">
												            <label class="col-lg-3 control-label">Upload: </label>

												            <div class="col-lg-9">
												            	<div class="input-group">
										                            <input type="file"  class="form-control" name="document_scan"  >
										                        </div>
												            </div>
												        </div>
													</div>
												</div>
												<div class="col-md-12">
											        <div class="form-group ">
											            <label class="col-lg-3 control-label">Remarks : </label>
											            <div class="col-lg-9">
											            	<textarea class="form-control" placeholder="remarks" name="remarks"></textarea>

											            </div>
											        </div>
												</div>
											</div>
										    <div class="row" style="margin-top:10px;">
												<div class="col-md-12">
											        <div class="form-actions text-center">
											            <button class="submit btn btn-primary btn-sm" type="submit">
											                Add Lease Agreement Uploads
											            </button>
											        </div>
											    </div>
											</div>
		                				</div>
		                			</div>

		            			</div>
		            			<?php echo form_close();?>
		            			<?php
		            			}
		            			?>
		                				<!-- end of form -->
		                		<!-- <div class="row"> -->
			                		<div class="col-md-12">
			                			<?php
													       if($lease_other_documents->num_rows() > 0)
													        {
													            $count = 0;

													            $identification_result =
													            '
													            <table class="table table-bordered table-striped table-condensed">
													                <thead>
													                    <tr>
													                        <th>#</th>
													                        <th>Document Type</th>
													                         <th>Description</th>
													                        <th>Download Link</th>
													                    </tr>
													                </thead>
													                  <tbody>

													            ';

													            foreach ($lease_other_documents->result() as $row)
													            {
													                $document_type_name = $row->document_type_name;
													                // $document_upload_id = $row->document_upload_id;
													                // $document_name = $row->document_name;
													                $document_upload_name = $row->document;
													                $description_item = $row->remarks;
													                // $document_status = $row->document_status;



													                $count++;
													                $identification_result .=
													                '
													                    <tr>
													                        <td>'.$count.'</td>
													                        <td>'.$document_type_name.'</td>
													                        <td>'.$description_item.'</td>
													                        <td><a href="'.$leases_location.''.$document_upload_name.'" target="_blank" >Download Here</a></td>
													                    </tr>
													                ';
													            }

													            $identification_result .=
													            '
													                          </tbody>
													                        </table>
													            ';
													        }

													        else
													        {
													            $identification_result = "<p>No plans have been added</p>";
													        }
													        echo $identification_result;
													       ?>
			                		</div>
			                	<!-- </div> -->

		            		</div>
						</div>
					</div>
				</div>
				<?php
				if($lease_status == 1)
				{
					$rental_leases = $this->leases_model->get_other_houses_on_property($rental_unit_id,$property_id);
					// if($rental_leases->num_rows() > 1)
					// {


					?>
					<div class="col-md-4">
							<div class="box box-warning">
								<div class="box-header with-border">
									<h3 class="box-title">Transfer Account</h3>
									<div class="box-tools pull-right">
									</div>
								</div>
								<div class="box-body">
								<div class="row" style="margin-bottom:20px;">
									<?php echo form_open("transfer-lease/".$lease_id.'/'.$tenant_unit_id, array("class" => "form-horizontal", "role" => "form",'id'=>'ConfirmForm'));?>
												<!-- <div class="col-lg-12 col-sm-12 col-md-12">
													<div class="row">
															<div class="col-md-12">
																<div class="row">
																		<div class="col-md-12">
																			<input type="hidden" class="form-control" name="redirect_url" placeholder="" autocomplete="off" value="<?php echo $this->uri->uri_string()?>">
																			<input type="hidden" class="form-control" name="lease_number" placeholder="" autocomplete="off" value="<?php echo $lease_number?>">
																			<input type="hidden" class="form-control" name="rental_unit_name" placeholder="" autocomplete="off" value="<?php echo $rental_unit_name?>">
																			<input type="hidden" class="form-control" name="rental_unit_price" placeholder="" autocomplete="off" value="<?php echo $rental_unit_price?>">
																			<input type="hidden" class="form-control" name="rental_unit_id_old" placeholder="" autocomplete="off" value="<?php echo $rental_unit_id?>">
																			<input type="hidden" class="form-control" name="tenant_id" placeholder="" autocomplete="off" value="<?php echo $tenant_id?>">
																			<div class="form-group ">
																	            <label class="col-lg-3 control-label">Transfer Date : </label>
																	            <div class="col-lg-9">
																	            	<div class="input-group">
															                            <span class="input-group-addon">
															                                <i class="fa fa-calendar"></i>
															                            </span>
															                            <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="transfer_date" value="" placeholder="Transfer date" id="datepicker6" >
															                        </div>
																	            </div>
																	        </div>
																			<div class="form-group ">
																					<label class="col-lg-3 control-label">Transfer to : </label>

																					<div class="col-lg-9">
																						<select id='rental_unit_id' name='rental_unit_id' class='form-control  select2' required="required">
																							<option value=''>None - </option>
																							<?php
																								if($rental_leases->num_rows() > 0)
																								{
																										foreach($rental_leases->result() as $res)
																										{
																												// $lease_ids = $res->lease_id;
																												$rental_unit_name = $res->rental_unit_name;
																												$property_name = $res->property_name;
																												$rental_unit_idd = $res->rental_unit_id;
																												$tenancy_query = $this->rental_unit_model->get_tenancy_details($rental_unit_idd);
																												// var_dump($tenancy_query->num_rows());die();
																												if($tenancy_query->num_rows() == 1)
																												{
																													$value = $tenancy_query->row();
																													$tenant_name = $value->tenant_name;
																													// $lease_id = $value->lease_id;
																													// $tenant_name = $this->rental_unit_model->get_tenant_name($rental_unit_id);
																													$tenancy_status = $tenant_name;

																												

																												}
																												else
																												{
																													$tenancy_status = 'VACANT';

																												}
																												 echo '<option value="'.$rental_unit_idd.'">'.$rental_unit_name.' - '.$tenancy_status.'</option>';
																												
																										}
																								}
																							?>

																						</select>
																					</div>
																			</div>
																			<div class="form-group ">
																	            <label class="col-lg-3 control-label">Remarks : </label>

																	            <div class="col-lg-9">
																	            	<textarea class="form-control" placeholder="Remarks" name="transfer_remarks"></textarea>
																	            </div>
																	        </div>
																			<div class="col-md-12">
																				<div class="alert alert-danger">
																					Note: The account balances on this rental account will be the opening balance on the other unit you are choosing. Please be keen
																				</div>
																			</div>
																	</div>
															</div>
															<div class="row" style="margin-top:10px;">
																<div class="col-md-12">
																			<div class="form-actions text-center">
																					<button class="submit btn btn-primary btn-sm" type="submit" id="confirmBTN" onclick="return confirm('Are you sure you want to proceed with transfering this tenant to the rental unit you have chosen ? ')">
																							Transfer Lease
																					</button>
																			</div>
																	</div>
															</div>
															</div>
														</div>

												</div> -->
												<?php echo form_close();?>
															<!-- end of form -->


											</div>
							</div>
						</div>
					</div>
					<?php
					
				}
				
				if($lease_status < 3 )
				{
					?>
					<div class="col-md-4">
						<div class="box box-danger">
						    <div class="box-header with-border">
						      <h3 class="box-title">House Vacation Notices</h3>

						      <div class="box-tools pull-right">

						      </div>
						    </div>
						    <div class="box-body">
								<div class="row" style="margin-bottom:20px;">
									<?php echo form_open("vacation-lease/".$lease_id.'/'.$tenant_unit_id, array("class" => "form-horizontal", "role" => "form"));?>
			            			<div class="col-lg-12 col-sm-12 col-md-12">
			            				<div class="row">
			                				<div class="col-md-12">
			                					<div class="row">
			                    					<div class="col-md-12">
															        <input type="hidden" class="form-control" name="redirect_url" placeholder="" autocomplete="off" value="<?php echo $this->uri->uri_string()?>">
															        <div class="form-group ">
															            <label class="col-lg-3 control-label">Notice Date : </label>

															            <div class="col-lg-9">
															            	<div class="input-group">
													                            <span class="input-group-addon">
													                                <i class="fa fa-calendar"></i>
													                            </span>
													                            <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="notice_date" value="" placeholder="Notice date" id="datepicker3" >
													                        </div>
															            </div>
															        </div>
															        <div class="form-group ">
															            <label class="col-lg-3 control-label">Remarks : </label>

															            <div class="col-lg-9">
															            	<textarea class="form-control" placeholder="Remarks" name="notice_remarks"><?php echo $remarks;?></textarea>
															            </div>
															        </div>
															    </div>
															</div>
											    <div class="row" style="margin-top:10px;">
													<div class="col-md-12">
												        <div class="form-actions text-center">
												            <button class="submit btn btn-primary btn-sm" type="submit">
												                Close Lease
												            </button>
												        </div>
												    </div>
												</div>
			                				</div>
			                			</div>

			            			</div>
			            			<?php echo form_close();?>
			                				<!-- end of form -->


			            		</div>
							</div>
						</div>
					</div>
			<?php
				}
			?>
			</div>
		</div>
	</div>
</div>

<?php
if($lease_status == 3 OR $lease_status == 4)
{
	?>
	<div class="row">
		<div class="col-md-6">
			<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">Lease Closing Expense</h3>

						<div class="box-tools pull-right">

						</div>
					</div>
					<div class="box-body">
					<div class="row" style="margin-bottom:20px;">
						<?php
						if($lease_status <=3)
						{


						?>
						<?php echo form_open("create-lease-invoice/".$lease_id, array("class" => "form-horizontal", "role" => "form"));?>
									<div class="col-lg-12 col-sm-12 col-md-12">
										<div class="row">
												<div class="col-md-12">
													<div class="row">
															<div class="col-md-12">
																	<div class="form-group ">
															<label class="col-lg-3 control-label"> Amount : </label>

															<div class="col-lg-9">
																<input type="text" class="form-control" name="invoice_amount" value="">
															</div>
													</div>
													<div class="form-group ">
															<label class="col-lg-3 control-label">Invoice type: </label>

															<div class="col-lg-9">
																<select id='invoice_type_id' name='invoice_type_id' class='form-control select2 '>
																			<option value=''>None - Please Select an invoice type</option>
																			<?php echo $invoice_type_list;?>
																		</select>
															</div>
													</div>
													<input type="hidden" class="form-control" name="redirect_url" placeholder="" autocomplete="off" value="<?php echo $this->uri->uri_string()?>">
													<div class="form-group ">
															<label class="col-lg-3 control-label">Date : </label>

															<div class="col-lg-9">
																<div class="input-group">
																					<span class="input-group-addon">
																							<i class="fa fa-calendar"></i>
																					</span>
																					<input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="invoice_date" value="<?php echo $notice_date?>" placeholder="Date" id="datepicker2">
																			</div>
															</div>
													</div>
													<div class="form-group ">
															<label class="col-lg-3 control-label">Remarks : </label>

															<div class="col-lg-9">
																<textarea class="form-control" placeholder="remarks" name="remarks"><?php echo $remarks;?></textarea>

															</div>
													</div>

											</div>
									</div>
										<div class="row" style="margin-top:10px;">
												<div class="col-md-12">
															<div class="form-actions text-center">
																	<button class="submit btn btn-primary btn-sm" type="submit">
																			Add Expense Amount
																	</button>
															</div>
													</div>
											</div>
										</div>
									</div>

									</div>
									<?php echo form_close();?>
												<!-- end of form -->
								<?php
								}
								?>


								</div>
								<div class="col-md-12">

									<?php
									$tenant_where = 'invoice.invoice_type = invoice_type.invoice_type_id AND lease_id = '.$lease_id.' AND invoice_item_status = 0 AND personnel_id ='.$this->session->userdata('personnel_id');
									$tenant_table = 'invoice,invoice_type';
									$tenant_order = 'invoice_type_name';

									$tenant_query = $this->tenants_model->get_tenant_list($tenant_table, $tenant_where, $tenant_order);

									$result_payment ='<table class="table table-bordered table-striped table-condensed">
																			<thead>
																				<tr>
																					<th >#</th>
																					<th >Type</th>
																					<th >Amount</th>
																					<th colspan="2" >Action</th>
																				</tr>
																			</thead>
																				<tbody>';
									$total_amount = 0;
									if($tenant_query->num_rows() > 0)
									{
										$x = 0;

										foreach ($tenant_query->result() as $key => $value) {
											// code...
											$invoice_type_name = $value->invoice_type_name;
											$invoice_note_amount = $value->invoice_amount;
											$invoice_id = $value->invoice_id;
											$total_amount += $invoice_note_amount;

											$x++;
											$result_payment .= form_open("accounts/update_invoice_item/".$invoice_id."/".$lease_id, array("class" => "form-horizontal"));
											$result_payment .= '<tr>
																							<td>'.$x.'</td>
																							<input type="hidden" name="redirect_url" value="'.$this->uri->uri_string().'">
																							<td>'.$invoice_type_name.'</td>
																							<td><input class="form-control" name="invoice_item_amount'.$invoice_id.'" value="'.$invoice_note_amount.'"></td>
																							<td><button type="submit" class="btn btn-sm btn-warning" ><i class="fa fa-pencil"></i></button></td>
																							<td><a href="'.site_url().'delete-invoice-item/'.$invoice_id.'" type="submit" class="btn btn-sm btn-danger" ><i class="fa fa-trash"></i></a></td>
																					</tr>';
											$result_payment .=form_close();
										}

										// display button

										$display = TRUE;
									}
									else {
										$display = FALSE;
									}

									$result_payment .='</tbody>
																	</table>';
									?>

									<?php echo $result_payment;?>

									<?php
					        if($display)
					        {
					          ?>
					          <div class="row">
					            <div class="col-md-12">
					              <?php echo form_open("accounts/confirm_invoice_note/".$tenant_unit_id."/".$lease_id, array("class" => "form-horizontal"));?>
					                <div class="col-md-6">
					                </div>
					                <div class="col-md-6">
					                    <!-- <h2 class="pull-right"> KES. <?php echo number_format($total_amount,2);?></h2> -->

					                    <input type="hidden" name="type_of_account" value="1">
					                    <input type="hidden" name="tenant_unit_id" id="tenant_unit_id" value="<?php echo $tenant_unit_id;?>">
					                    <input type="hidden" name="lease_id" id="lease_id" value="<?php echo $lease_id?>">
															<input type="hidden" name="invoice_cat"  value="1">
					                    <input type="hidden" name="rental_unit_id" id="lease_id" value="<?php echo $rental_unit_id?>">
					                    <input type="hidden" name="redirect_url" value="<?php echo $this->uri->uri_string()?>">
					                  <div class="form-group">
					                    <label class="col-md-4 control-label">Date: </label>

					                    <div class="col-md-7">
					                       <div class="input-group">
					                            <span class="input-group-addon">
					                                <i class="fa fa-calendar"></i>
					                            </span>
					                            <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="invoice_date" placeholder="Payment Date" id="datepicker5" value="<?php echo $notice_date?>" required>
					                        </div>
					                    </div>
					                  </div>

					                  <div class="form-group">
					                    <label class="col-md-4 control-label">Total Amount: </label>

					                    <div class="col-md-7">
					                      <input type="number" class="form-control" name="total_amount" placeholder=""  autocomplete="off" value="<?php echo $total_amount;?>" readonly>
					                    </div>
					                  </div>

					                  <div class="col-md-12">
					                      <div class="text-center">
					                        <button class="btn btn-info btn-sm " type="submit">Complete Invoice </button>
					                      </div>
					                  </div>
					                </div>
					              <?php echo form_close();?>
					            </div>
					          </div>
					          <?php
					        }
					        ?>
								</div>

								<div class="row">
								  <div class="col-md-12">
								    <div class="box">
								        <div class="box-header with-border">
								          <h3 class="box-title">Invoices</h3>

								          <div class="box-tools pull-right">

								          </div>
								        </div>
								        <div class="box-body">
								            <table class="table table-hover table-bordered col-md-12">
								              <thead>
								                <tr>
								                  <th>#</th>
								                  <th>Debit Note Date</th>
								                  <th>Document Number</th>
								                  <th>Amount </th>
								                  <th>Captured Date</th>
								                  <th>Receipted By</th>
								                  <th colspan="1">Actions</th>
								                </tr>
								              </thead>
								              <tbody>
								                <?php
								                	$lease_payments = $this->accounts_model->get_lease_invoice($lease_id,null,1);
								                  // var_dump($tenant_query);die();
								                if($lease_payments->num_rows() > 0)
								                {
								                  $y = 0;
								                  foreach ($lease_payments->result() as $key) {
								                    # code...
								                    $total_amount = $key->total_amount;
								                    $lease_invoice_id = $key->lease_invoice_id;
								                    $invoice_date = $key->invoice_date;
																		 $invoice_month = $key->invoice_month;
																		  $invoice_year = $key->invoice_year;
								                    $document_number = $key->document_number;
								                    $created = $key->created;
								                    $created_by = $key->created_by;



								                    $payment_explode = explode('-', $invoice_date);

								                    $invoice_note_date = date('jS M Y',strtotime($invoice_date));
								                    $created = date('jS M Y',strtotime($created));
								                    $y++;

								                    ?>
								                    <tr>
								                      <td><?php echo $y?></td>
								                      <td><?php echo $invoice_note_date;?></td>
								                      <td><?php echo $document_number?></td>
								                      <td><?php echo number_format($total_amount,2);?></td>
								                      <td><?php echo $invoice_note_date;?></td>
								                      <td><?php echo $created_by;?></td>
								                      <td><a href="<?php echo site_url().'invoice/'.$lease_id.'/'.$invoice_month.'/'.$invoice_year.'/'.$lease_invoice_id.'';?>" class="btn btn-sm btn-primary" target="_blank">Invoice</a></td>

								                    </tr>
								                    <?php

								                  }
								                }
								                ?>

								              </tbody>
								            </table>

								        </div>
								      </div>
								    </div>
								  </div>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">Cash Back to Tenants</h3>

						<div class="box-tools pull-right">

						</div>
					</div>
					<div class="box-body">

						<table class="table table-bordered table-striped table-condensed">
							<thead>
								<tr>
									<th>Title</th>
									<th>Detail</th>
								</tr>
							</thead>
								<tbody>
									<?php $amount_payable = $amount_to_return - $total_arrears;?>
									<tr><td><span>Deposits Paid :</span></td><td><strong>KES. <?php echo number_format(($amount_to_return),2);?></strong> </td></tr>
									<tr><td><span>Arrears :</span></td><td><strong>KES. (<?php echo number_format($total_arrears-$expenses_payable,2);?>)</strong> </td></tr>
									<tr><td><span>Expenses :</span></td><td><strong>KES. (<?php echo number_format($expenses_payable,2);?>)</strong> </td></tr>
									<tr><td><span>Amount Payable :</span></td><td><strong>KES. <?php echo number_format($amount_payable,2);?></strong> </td></tr>

								</tbody>
							</table>
							<br>
							<?php
							if($lease_status < 4)
							{


								if($amount_payable >= 0)
								{
									$remaining_arrears = $total_arrears-$expenses_payable;
								?>
									<div class="row" style="margin-bottom:20px;">

										<?php echo form_open("close-lease/".$lease_id.'/'.$tenant_unit_id, array("class" => "form-horizontal", "role" => "form"));?>
										<div class="col-lg-12 col-sm-12 col-md-12">
											<div class="row">
													<input type="hidden" class="form-control" name="arrears" value="<?php echo $remaining_arrears?>" readonly>
													<div class="col-md-12">
														<div class="row">
														<div class="col-md-12">
														<div class="form-group ">
															<label class="col-lg-3 control-label">Refund Amount : </label>
															<div class="col-lg-9">
																<input type="text" class="form-control" name="payable_amount" value="<?php echo $amount_payable?>" readonly>
															</div>
														</div>

														<div class="form-group ">
																<label class="col-lg-3 control-label">Method : </label>
																<div class="col-lg-9">
																		<select class="form-control select2" name="payment_method" required>
																			<option value="0">Select a payment method</option>
							                                            	<?php
																			  $method_rs = $this->accounts_model->get_payment_methods();

																				foreach($method_rs->result() as $res)
																				{
																				  $payment_method_id = $res->payment_method_id;
																				  $payment_method = $res->payment_method;

																					echo '<option value="'.$payment_method_id.'">'.$payment_method.'</option>';

																				}

																		  ?>
																		</select>
																</div>
														</div>
														 <input type="hidden" class="form-control" name="redirect_url" placeholder="" autocomplete="off" value="<?php echo $this->uri->uri_string()?>">
														 <div class="form-group ">
												            <label class="col-lg-3 control-label">Refund Type: </label>

												            <div class="col-lg-9">
												            	<select id='invoice_type_id' name='invoice_type_id' class='form-control select2 '>
											                    <!-- <select class="form-control custom-select " id='procedure_id' name='procedure_id'> -->
											                      <option value=''>None - Please Select an invoice type</option>
											                      <?php echo $invoice_type_list;?>
											                    </select>
												            </div>
												        </div>
														<div class="form-group ">
																<label class="col-lg-3 control-label">Date paid : </label>

																<div class="col-lg-9">
																	<div class="input-group">
																						<span class="input-group-addon">
																								<i class="fa fa-calendar"></i>
																						</span>
																						<input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="closing_end_date" value="<?php echo $closing_end_date;?>" placeholder="Report Date From" id="datepicker1">
																				</div>
																</div>
														</div>
														<div class="form-group ">
																<label class="col-lg-3 control-label">General Remarks : </label>

																<div class="col-lg-9">
																	<textarea class="form-control" name="remarks" placeholder="remarks"><?php echo $remarks;?></textarea>

																</div>
														</div>

												</div>
										</div>
											<div class="row" style="margin-top:10px;">
											<div class="col-md-12">
														<div class="form-actions text-center">
																<button class="submit btn btn-primary btn-sm" type="submit">
																		 Lease
																</button>
														</div>
												</div>
										</div>
													</div>
												</div>

										</div>
										<?php echo form_close();?>
													<!-- end of form -->


									</div>
									<div class="col-md-12">
										<div class="alert alert-warning">
											Note that if there is any pending invoices the amount shall be deducted from the deposits
										</div>
									</div>
								<?php
								}
								else
								{
									?>
									<div class="col-md-12">
										<div class="alert alert-warning">
											Sorry ensure the tenant has made payment of the amount before closing the lease
										</div>
									</div>
									<?php
								}
							}
							?>
				</div>
			</div>
		</div>
	</div>
	<?php
}
?>

<script type="text/javascript">


	 $(document).ready(function () {

	    $("#ConfirmForm").submit(function (e) {

	        //stop submitting the form to see the disabled button effect
	        // e.preventDefault();

	        //disable the submit button
	        $("#confirmBTN").attr("disabled", true);

	        //disable a normal button
	        // $("#btnTest").attr("disabled", true);

	        return true;

	    });
	});
	$(function() {
	    $("#invoice_type_id").customselect();
	    $("#billing_schedule_id").customselect();

	});
</script>
