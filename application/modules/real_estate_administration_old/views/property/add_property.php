<?php
//personnel data
$property_name = set_value('property_name');
$property_location = set_value('property_location');
$lr_no = set_value('lr_no');
$estate = set_value('estate');
$road = set_value('road');
$message_prefix = set_value('message_prefix');
$property_code = set_value('property_code');
$property_owner_id = set_value('property_owner_id');
$property_type_id = set_value('property_type_id');
$manager_percent = set_value('manager_percent');
$commission_type = set_value('commission_type');
$return_date = set_value('return_date');
$total_units = set_value('total_units');
$start_date = set_value('start_date');
$opening_balance = set_value('opening_balance');
?>          <div class="box">
                <div class="box-header with-border">
                  <h3 class="box-title"><?php echo $title;?></h3>

                  <div class="box-tools pull-right">
                     <a href="<?php echo site_url();?>property-manager/properties" class="btn btn-sm btn-info">Back to properties</a>
                  </div>
                </div>
                <div class="box-body">


                    <!-- Adding Errors -->
                    <?php
						$success = $this->session->userdata('success_message');
						$error = $this->session->userdata('error_message');

						if(!empty($success))
						{
							echo '
								<div class="alert alert-success">'.$success.'</div>
							';

							$this->session->unset_userdata('success_message');
						}

						if(!empty($error))
						{
							echo '
								<div class="alert alert-danger">'.$error.'</div>
							';

							$this->session->unset_userdata('error_message');
						}

						$validation_errors = validation_errors();

						if(!empty($validation_errors))
						{
							echo '<div class="alert alert-danger"> Oh snap! '.$validation_errors.' </div>';
						}

						$validation_errors = validation_errors();

						if(!empty($validation_errors))
						{
							echo '<div class="alert alert-danger"> Oh snap! '.$validation_errors.' </div>';
						}
                    ?>

                    <?php echo form_open($this->uri->uri_string(), array("class" => "form-horizontal", "role" => "form"));?>
            						<div class="row">
              						   <div class="col-md-6">
              						        <div class="form-group">
              						            <label class="col-lg-5 control-label">Property Name: </label>

              						            <div class="col-lg-7">
              						            	<input type="text" class="form-control" name="property_name" placeholder="Names" value="<?php echo $property_name;?>" required>
              						            </div>
              						        </div>
                                  <div class="form-group">
                                      <label class="col-lg-5 control-label">Property Code: </label>

                                      <div class="col-lg-7">
                                        <input type="text" class="form-control" name="property_code" placeholder="Code" value="<?php echo $property_code;?>">
                                      </div>
                                  </div>
                                  <div class="form-group">
                                       <label class="col-md-5 control-label">Start Date: </label>

                                       <div class="col-md-7">
                                           <div class="input-group">
                                               <span class="input-group-addon">
                                                   <i class="fa fa-calendar"></i>
                                               </span>
                                               <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="start_date" placeholder="Start date"  id="datepicker1" autocomplete="off" value="<?php echo $start_date;?>" >
                                           </div>
                                       </div>
                                   </div>
                                    <div class="form-group">
                                        <label class="col-lg-5 control-label">Opening Balance: </label>

                                        <div class="col-lg-7">
                                          <input type="text" class="form-control" name="opening_balance" placeholder="Code" value="<?php echo $opening_balance;?>">
                                        </div>
                                    </div>
                                  <div class="form-group">
                                      <label class="col-md-5 control-label">Property Owner</label>

                                      <div class="col-md-7">
                                          <select class="form-control select2" name="property_owner_id">
                                              <option value="">-- Select property Owner --</option>
                                              <?php
                                              if($property_owners->num_rows() > 0)
                                              {
                                                  $property_owner = $property_owners->result();

                                                  foreach($property_owner as $res)
                                                  {
                                                      $db_property_owner_id = $res->property_owner_id;
                                                      $property_owner_name = $res->property_owner_name;

                                                      if($db_property_owner_id == $property_owner_id)
                                                      {
                                                          echo '<option value="'.$db_property_owner_id.'" selected>'.$property_owner_name.'</option>';
                                                      }

                                                      else
                                                      {
                                                          echo '<option value="'.$db_property_owner_id.'">'.$property_owner_name.'</option>';
                                                      }
                                                  }
                                              }
                                          ?>
                                          </select>
                                      </div>
                                  </div>
              						        <div class="form-group">
              						            <label class="col-lg-5 control-label">Property Location: </label>

              						            <div class="col-lg-7">
              						            	<input type="text" class="form-control" name="property_location" placeholder="Property Location" value="<?php echo $property_location;?>" required>
              						            </div>
              						        </div>
                                  <div class="form-group">
                                      <label class="col-lg-5 control-label">Return Date: </label>

                                      <div class="col-lg-7">
                                          <input type="text" class="form-control" name="return_date" placeholder="i.e" value="<?php echo $return_date;?>">
                                      </div>
                                  </div>
                                  
  							             </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                      <label class="col-lg-5 control-label">Manager Percent: </label>

                                      <div class="col-lg-7">
                                          <input type="text" class="form-control" name="manager_percent" placeholder="i.e" value="<?php echo $manager_percent;?>" required>
                                      </div>
                                  </div>
                                  <div class="form-group">
                                      <label class="col-md-5 control-label">Charge VAT ? *</label>

                                      <div class="col-md-7">
                                          <div class="radio">
                                              <label>
                                                  <input  type="radio" checked value="No" name="vatable" id="account_to_type" >
                                                  No
                                              </label>
                                              <label>
                                                  <input  type="radio"  value="Yes" name="vatable" id="account_to_type" >
                                                  Yes
                                              </label>
                                          </div>

                                      </div>
                                  </div>
                                <div class="form-group">
                                    <label class="col-md-5 control-label">Property Type</label>

                                    <div class="col-md-7">
                                        <select class="form-control select2 required" name="property_type_id">
                                            <option value="">-- Select Type --</option>
                                            <?php
                                            if($property_types->num_rows() > 0)
                                            {
                                                $types = $property_types->result();

                                                foreach($types as $res)
                                                {
                                                    $db_property_type_id = $res->property_type_id;
                                                    $property_type_name = $res->property_type_name;

                                                    if($db_property_type_id == $property_type_id)
                                                    {
                                                        echo '<option value="'.$db_property_type_id.'" selected>'.$property_type_name.'</option>';
                                                    }

                                                    else
                                                    {
                                                        echo '<option value="'.$db_property_type_id.'">'.$property_type_name.'</option>';
                                                    }
                                                }
                                            }
                                        ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-5 control-label">Total Units: </label>

                                    <div class="col-lg-7">
                                        <input type="text" class="form-control" name="total_units" placeholder="Total Units" value="<?php echo $total_units;?>" required>
                                    </div>
                                </div>
    						                <div class="form-group">
                                    <label class="col-lg-5 control-label">LR/NO: </label>

                                    <div class="col-lg-7">
                                        <input type="text" class="form-control" name="lr_no" placeholder="LR/NO" value="<?php echo $lr_no;?>" >
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-5 control-label">Estate: </label>

                                    <div class="col-lg-7">
                                        <input type="text" class="form-control" name="estate" placeholder="Estate" value="<?php echo $estate;?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-5 control-label">Road: </label>

                                    <div class="col-lg-7">
                                        <input type="text" class="form-control" name="road" placeholder="Road" value="<?php echo $road;?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-5 control-label">Message prefix: </label>

                                    <div class="col-lg-7">
                                        <textarea class="form-control" name="message_prefix"><?php echo $message_prefix;?></textarea>
                                    </div>
                                </div>
            						    </div>
            						</div>
            						<div class="row" style="margin-top:10px;">
            							<div class="col-md-12">
            						        <div class="form-actions text-center">
            						            <button class="submit btn btn-sm btn-primary" type="submit">
            						                Add property
            						            </button>
            						        </div>
            						    </div>
            						</div>
                    <?php echo form_close();?>
                </div>
            </div>
