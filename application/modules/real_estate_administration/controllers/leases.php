<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once "./application/modules/admin/controllers/admin.php";

class leases extends admin {
	var $leases_path;
	var $leases_location;
	var $csv_path;
	function __construct()
	{
		parent:: __construct();
		$this->load->model('admin/users_model');
		$this->load->model('property_model');
		$this->load->model('admin/admin_model');
		$this->load->model('leases_model');
		$this->load->model('tenants_model');
		$this->load->model('home_owners_model');
		$this->load->model('rental_unit_model');
		$this->load->model('accounts/accounts_model');
		$this->load->model('admin/file_model');
		$this->load->library('image_lib');


		//path to image directory

		$this->leases_path = realpath(APPPATH . '../assets/uploads');
		$this->leases_location = base_url().'assets/uploads/';
		$this->csv_path = realpath(APPPATH . '../assets/csv');
		
	}

	/*
	*
	*	Default action is to show all the registered lease
	*
	*/
	public function index()
	{

		$where = 'leases.tenant_unit_id = tenant_unit.tenant_unit_id AND tenant_unit.tenant_id = tenants.tenant_id AND tenant_unit.rental_unit_id = rental_unit.rental_unit_id AND rental_unit.property_id = property.property_id AND  leases.lease_status <=2 AND property.property_deleted = 0';

		$properties = $this->session->userdata('properties');

		if(isset($properties) AND !empty($properties))
		{
			$where .= $properties;
		}

		$all_leases_search = $this->session->userdata('all_leases_search');

		if(isset($all_leases_search) AND !empty($all_leases_search))
		{
			$where .= $all_leases_search;
		}
		// var_dump($where); die();
		$table = 'leases,rental_unit,tenant_unit,tenants,property';
		$segment = 3;
		//pagination
		$this->load->library('pagination');
		$config['base_url'] = base_url().'lease-management/leases';
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;

		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';

		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';

		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';

		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';

		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';

		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);

		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->leases_model->get_all_leases($table, $where, $config["per_page"], $page,'property.property_id,rental_unit.rental_unit_number,rental_unit.rental_unit_name', $order_method = 'ASC');

		$v_data['query'] = $query;
		$v_data['page'] = $page;
		$v_data['title'] ='Leases';


		$tenant_order = 'tenants.tenant_name';
		$tenant_table = 'tenants';
		$tenant_where = 'tenants.tenant_status = 1';

		$tenant_query = $this->tenants_model->get_tenant_list($tenant_table, $tenant_where, $tenant_order);
		$rs8 = $tenant_query->result();
		$tenants_list = '';
		foreach ($rs8 as $tenant_rs) :
			$tenant_id = $tenant_rs->tenant_id;
			$tenant_name = $tenant_rs->tenant_name;
			$tenant_number = $tenant_rs->tenant_number;
			$tenant_national_id = $tenant_rs->tenant_national_id;
			$tenant_phone_number = $tenant_rs->tenant_phone_number;

		    $tenants_list .="<option value='".$tenant_id."'>".$tenant_name." Phone: ".$tenant_phone_number." Phone: ".$tenant_number."</option>";

		endforeach;

		$v_data['tenants_list'] = $tenants_list;


		$rental_unit_order = 'rental_unit.property_id = property.property_id';
		$rental_unit_table = 'rental_unit,property';
		$rental_unit_where = 'rental_unit.rental_unit_status = 1 and rental_unit.property_id = property.property_id';

		$rental_unit_query = $this->tenants_model->get_tenant_list($rental_unit_table, $rental_unit_where, $rental_unit_order);
		$rs8 = $rental_unit_query->result();
		$rental_unit_list = '';
		foreach ($rs8 as $rental_unit) :
			$rental_unit_id = $rental_unit->rental_unit_id;
			$rental_unit_name = $rental_unit->rental_unit_name;

			$property_name = $rental_unit->property_name;

		    $rental_unit_list .="<option value='".$rental_unit_id."'>".$rental_unit_name." Property: ".$property_name."</option>";

		endforeach;

		$v_data['rental_unit_list'] = $rental_unit_list;

		$properties = $this->property_model->get_active_property();

		// $this->accounts_model->update_invoices();


		$rs8 = $properties->result();
		$property_list = '';
		foreach ($rs8 as $property_rs) :
			$property_id = $property_rs->property_id;
			$property_name = $property_rs->property_name;
			$property_location = $property_rs->property_location;

		    $property_list .="<option value='".$property_id."'>".$property_name." Location: ".$property_location."</option>";

		endforeach;
		$v_data['property_list'] = $property_list;

		$data['content'] = $this->load->view('leases/all_leases', $v_data, true);

		$data['title'] = 'All rental unit';

		$this->load->view('admin/templates/general_page', $data);
	}

	function add_lease($tenant_id,$rental_unit_id)
	{

		$lease_error = $this->session->userdata('lease_error_message');

		$this->form_validation->set_rules('lease_start_date', 'lease name', 'required|trim|xss_clean');
		$this->form_validation->set_rules('lease_duration', 'lease duration', 'required|trim|xss_clean');
		$this->form_validation->set_rules('rent_amount', 'Rent amount', 'required|trim|xss_clean');
		$this->form_validation->set_rules('deposit_amount', 'lease location', 'required|trim|xss_clean');

		if ($this->form_validation->run())
		{
			if(empty($lease_error))
			{
				if($this->leases_model->add_lease($tenant_id,$rental_unit_id))
				{
					$this->session->unset_userdata('lease_error_message');
					$this->session->set_userdata('success_message', 'Lease has been added successfully');
				}
				else
				{
					$this->session->set_userdata('error_message', 'Sorry please check for errors has been added successfully');
				}


			}
			else
			{
				$this->session->set_userdata('error_message', 'Sorry please check for errors has been added successfully');

			}
		}
		else
		{
			$this->session->set_userdata('error_message', 'Please fill in all the fields');
		}
		redirect('tenants/'.$rental_unit_id);
	}

	function edit_lease($lease_id, $page = NULL)
	{
		//get lease data
		$table = "rental_unit, rental_unit_owners";
		$where = "rental_unit_owners.rental_unit_owner_id = rental_unit.rental_unit_owner_id AND rental_unit.rental_unit_id = ".$rental_unit_id;

		$this->db->where($where);
		$rental_unit_query = $this->db->get($table);
		$rental_unit_row = $rental_unit_query->row();
		$v_data['rental_unit_row'] = $rental_unit_row;
		$v_data['rental_unit_owners'] = $this->rental_unit_owners_model->get_all_front_end_rental_unit_owners();

		$this->form_validation->set_rules('rental_unit_name', 'rental_unit name', 'trim|xss_clean');
		$this->form_validation->set_rules('rental_unit_location', 'rental_unit location', 'trim|xss_clean');

		if ($this->form_validation->run())
		{
			if(empty($rental_unit_error))
			{

				$data2 = array(
					'rental_unit_name'=>$this->input->post("rental_unit_name"),
					'rental_unit_location'=>$this->input->post("rental_unit_location"),
					'rental_unit_status'=>1,
					'rental_unit_owner_id'=>$this->input->post("rental_unit_owner_id")
				);

				$table = "rental_unit";
				$this->db->where('rental_unit_id', $rental_unit_id);
				$this->db->update($table, $data2);
				$this->session->set_userdata('success_message', 'rental_unit has been edited');

				redirect('real-estate-administration/rental-units/'.$page);
			}
		}

		$rental_unit = $this->session->userdata('rental_unit_file_name');



		$data['content'] = $this->load->view("rental_unit/edit_rental_unit", $v_data, TRUE);
		$data['title'] = 'Edit rental_unit';

		$this->load->view('admin/templates/general_page', $data);
	}

    public function update_lease_detail($lease_id)
    {
	    	$this->form_validation->set_rules('lease_start_date', 'Lease Start Date', 'trim|required|xss_clean');
				$this->form_validation->set_rules('lease_duration', 'Lease Duration', 'trim|required|xss_clean');

				if ($this->form_validation->run())
				{

					$this->db->where('tenant_unit.tenant_unit_id = leases.tenant_unit_id AND rental_unit.rental_unit_id = leases.rental_unit_id AND leases.lease_status = 2 AND (leases.lease_number IS NULL OR leases.lease_number = "") AND leases.lease_id ='.$lease_id);
					$this->db->select('tenant_unit.tenant_id,leases.lease_id,rental_unit.rental_unit_price,tenant_unit.rental_unit_id');
					$query= $this->db->get('leases,tenant_unit,rental_unit');
					// var_dump($query);die();
					if($query->num_rows() > 0)
					{
						$value = $query->row();
						$tenant_id = $value->tenant_id;
						$rental_unit_price = $value->rental_unit_price;
						$rental_unit_id = $value->rental_unit_id;
						$lease_number = $this->leases_model->create_lease_number();
						$lease_duration = $this->input->post('lease_duration');
						$date = $this->input->post("lease_start_date");
						// $lease_end_date  = strtotime(date("Y-m-d", strtotime($date)) . '+$lease_duration month');
						$lease_end_date  = date("Y-m-d",strtotime(date("Y-m-d", strtotime($date)) . '+'.$lease_duration.' month'));

						// var_dump($lease_end_date);die();
						if(!empty($rental_unit_price))
						{
							$data2 = array(
															'lease_start_date'=>$this->input->post("lease_start_date"),
															'lease_duration'=>$this->input->post("lease_duration"),
															'lease_end_date'=>$lease_end_date,
															'lease_status'=>1,
															// 'tenant_id'=>$tenant_id,
															'lease_number'=>$lease_number,
														);
							$table = "leases";
							$this->db->where('lease_id', $lease_id);
							$this->db->update($table, $data2);


							$this->db->where('rental_unit_id', $rental_unit_id);
							$this->db->limit(1);
							$this->db->order_by('rental_unit_price_id','DESC');
							$query_cog = $this->db->get('rental_unit_prices');
							$row_cog = $query_cog->row();

							$tax_amount = $row_cog->tax_amount;

							// var_dump($tax_amount); die();
							// put the default rent amount
							$charge_to = 1;
							$invoice_type_id = 1;
							$billing_schedule_id = 1;
							$start_date = date('Y-m-d');
							$arrears = 0;
							$initial_reading = 0;
							$amount = $rental_unit_price;
							$where_array = array(
													'charge_to'=>1,
													'invoice_type_id' => $invoice_type_id,
													'billing_schedule_id' => $billing_schedule_id,
													'lease_id' => $lease_id,
													'start_date' => $this->input->post("lease_start_date"),
													'initial_reading' => $initial_reading,
													'arrears_bf' => $arrears,
													'billing_amount'=>$amount,
													'tax_amount'=>$tax_amount,
													'lease_number'=>$lease_number
												);
							$query = $this->db->insert('property_billing',$where_array);
							$this->session->set_userdata('success_message', 'lease has been created');
						}
						else {
							// code...
							$this->session->set_userdata('success_message', 'Sorry the rental unit does not have a price to it. Please update it and try again ');
						}

					}
					else
					{
						$date = $this->input->post("lease_start_date");
						$lease_duration = $this->input->post('lease_duration');
						// $lease_end_date  = strtotime(date("Y-m-d", strtotime($date)) . '+$lease_duration month');
						$lease_end_date  = date("Y-m-d",strtotime(date("Y-m-d", strtotime($date)) . '+'.$lease_duration.' month'));
						$data2 = array(
							'lease_end_date'=>$lease_end_date
						);

						$table = "leases";
						$this->db->where('lease_id', $lease_id);
						$this->db->update($table, $data2);

						$lease_detail = $this->leases_model->get_lease_detail($lease_id);
						$lease_row = $lease_detail->row();

						$tenant_phone_number = $lease_row->tenant_phone_number;
						$tenant_name = $lease_row->tenant_name;
						$account_number = $lease_row->lease_number;
						$rental_unit_name = $lease_row->rental_unit_name;
						$property_name = $lease_row->property_name;

						if(!empty($tenant_phone_number))
						{
							 $explode = explode(' ',$tenant_name);
							 $first_name = $explode[0];
							$message = 'Dear '.$tenant_name.', Your lease has been successfully activated for house '.$rental_unit_name.' . Your lease if for a period of '.$lease_duration.' starting '.$date.'. Payments shall be made through MPESA PAYBILL 550134 ACCOUNT NO. '.$lease_number.'. Enjoy your stay . For help please call 0702777717. Allan & Bradley Co. LTD. ';
							// $this->accounts_model->sms($tenant_phone_number,$message,$tenant_name);
						}
						$this->session->set_userdata('success_message', 'lease has been edited');
					}


				}

			$redirect_url = $this->input->post('redirect_url');
			redirect($redirect_url);
    }


	/*
	*
	*	Delete an existing rental_unit
	*	@param int $rental_unit_id
	*
	*/
	function delete_rental_unit($rental_unit_id, $page)
	{
		//get rental_unit data
		$table = "rental_unit";
		$where = "rental_unit_id = ".$rental_unit_id;

		$this->db->where($where);
		$rental_unit_query = $this->db->get($table);
		$rental_unit_row = $rental_unit_query->row();
		$rental_unit_path = $this->rental_unit_path;

		$image_name = $rental_unit_row->rental_unit_image_name;

		//delete any other uploaded image
		$this->file_model->delete_file($rental_unit_path."\\".$image_name);

		//delete any other uploaded thumbnail
		$this->file_model->delete_file($rental_unit_path."\\thumbnail_".$image_name);

		if($this->rental_unit_model->delete_rental_unit($rental_unit_id))
		{
			$this->session->set_userdata('success_message', 'rental_unit has been deleted');
		}

		else
		{
			$this->session->set_userdata('error_message', 'rental_unit could not be deleted');
		}
		redirect('real-estate-administration/rental-units/'.$page);
	}

	/*
	*
	*	Activate an existing rental_unit
	*	@param int $rental_unit_id
	*
	*/
	public function activate_rental_unit($rental_unit_id, $page = NULL)
	{
		if($this->rental_unit_model->activate_rental_unit($rental_unit_id))
		{
			$this->session->set_userdata('success_message', 'rental_unit has been activated');
		}

		else
		{
			$this->session->set_userdata('error_message', 'rental_unit could not be activated');
		}
		redirect('rental-units/'.$page);
	}

	/*
	*
	*	Deactivate an existing rental_unit
	*	@param int $rental_unit_id
	*
	*/
	public function deactivate_rental_unit($rental_unit_id, $page = NULL)
	{
		if($this->rental_unit_model->deactivate_rental_unit($rental_unit_id))
		{
			$this->session->set_userdata('success_message', 'rental_unit has been disabled');
		}

		else
		{
			$this->session->set_userdata('error_message', 'rental_unit could not be disabled');
		}
		redirect('rental-units/'.$page);
	}
	public function import_tenant_units()
	{
		$v_data['title'] = $data['title'] = 'Import Tenant Units';

		$data['content'] = $this->load->view('import/import_tenant_units', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}
	public function tenant_units_import_template()
	{
			//export products template in excel
		$this->leases_model->tenants_units_import_template();
	}


	public function tenant_units_import()
	{
		if(isset($_FILES['import_csv']))
		{
			if(is_uploaded_file($_FILES['import_csv']['tmp_name']))
			{
				//import products from excel
				$response = $this->leases_model->import_csv_payroll($this->csv_path);

				if($response == FALSE)
				{
					$v_data['import_response_error'] = 'Something went wrong. Please try again.';
				}

				else
				{
					if($response['check'])
					{
						$v_data['import_response'] = $response['response'];
					}

					else
					{
						$v_data['import_response_error'] = $response['response'];
					}
				}
			}

			else
			{
				$v_data['import_response_error'] = 'Please select a file to import.';
			}
		}

		else
		{
			$v_data['import_response_error'] = 'Please select a file to import.';
		}

		$v_data['title'] = $data['title'] = 'Import Tenant Units';

		$data['content'] = $this->load->view('import/import_tenant_units', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}












	public function import_home_owners()
	{
		$v_data['title'] = $data['title'] = 'Import Home Owners';

		$data['content'] = $this->load->view('import/import_home_owners', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}
	public function home_owners_import_template()
	{
			//export products template in excel
		$this->leases_model->home_owners_import_template();
	}


	public function home_owners_import()
	{
		if(isset($_FILES['import_csv']))
		{
			if(is_uploaded_file($_FILES['import_csv']['tmp_name']))
			{
				//import products from excel
				$response = $this->leases_model->import_csv_home_owners($this->csv_path);

				if($response == FALSE)
				{
					$v_data['import_response_error'] = 'Something went wrong. Please try again.';
				}

				else
				{
					if($response['check'])
					{
						$v_data['import_response'] = $response['response'];
					}

					else
					{
						$v_data['import_response_error'] = $response['response'];
					}
				}
			}

			else
			{
				$v_data['import_response_error'] = 'Please select a file to import.';
			}
		}

		else
		{
			$v_data['import_response_error'] = 'Please select a file to import.';
		}

		$v_data['title'] = $data['title'] = 'Import Tenant Units';

		$data['content'] = $this->load->view('import/import_home_owners', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}

	public function lease_detail($lease_id,$lease_number=NULL)
	{
		if(!empty($lease_number))
		{
			$add = ' and leases.lease_number ='.$lease_number;
		}
		else
		{
			$add = ' and leases.lease_id ='.$lease_id;
		}

		//get lease data
		$where = 'leases.lease_id > 0 AND leases.tenant_unit_id = tenant_unit.tenant_unit_id AND tenant_unit.tenant_id = tenants.tenant_id AND tenant_unit.rental_unit_id = rental_unit.rental_unit_id AND rental_unit.property_id = property.property_id  '.$add;
		$table = 'leases,rental_unit,tenant_unit,tenants,property';

		$this->db->where($where);
		$rental_unit_query = $this->db->get($table);
		$rental_unit_row = $rental_unit_query->row();
		$v_data['rental_unit_row'] = $rental_unit_row;


		$rental_unit = $this->session->userdata('rental_unit_file_name');

		$v_data['lease_id'] = $lease_id;

		$invoice_type_order = 'invoice_type.invoice_type_id';
		$invoice_type_table = 'invoice_type';
		$invoice_type_where = 'invoice_type.invoice_type_status = 1 AND generaljournalaccountid = 6';

		$invoice_type_query = $this->tenants_model->get_tenant_list($invoice_type_table, $invoice_type_where, $invoice_type_order);
		$rs8 = $invoice_type_query->result();
		$invoice_type_list = '';
		foreach ($rs8 as $invoice_rs) :
			$invoice_type_id = $invoice_rs->invoice_type_id;
			$invoice_type_name = $invoice_rs->invoice_type_name;


		    $invoice_type_list .="<option value='".$invoice_type_id."'>".$invoice_type_name."</option>";

		endforeach;

		$v_data['invoice_type_list'] = $invoice_type_list;



		$billing_schedule_order = 'billing_schedule.billing_schedule_id';
		$billing_schedule_table = 'billing_schedule';
		$billing_schedule_where = 'billing_schedule.billing_schedule_status = 1';

		$billing_schedule_query = $this->tenants_model->get_tenant_list($billing_schedule_table, $billing_schedule_where, $billing_schedule_order);
		$rs8 = $billing_schedule_query->result();
		$billing_schedule_list = '';
		foreach ($rs8 as $billing_rs) :
			$billing_schedule_id = $billing_rs->billing_schedule_id;
			$billing_schedule_name = $billing_rs->billing_schedule_name;


		    $billing_schedule_list .="<option value='".$billing_schedule_id."'>".$billing_schedule_name."</option>";

		endforeach;

		$v_data['billing_schedule_list'] = $billing_schedule_list;


		// if(!empty($lease_number))
		// {
		// 	$add_property = ' AND property_billing.lease_id ='.$lease_id.' AND property_billing.lease_number ='.$lease_number;
		// }
		// else
		// {
			$add_property = ' AND property_billing.lease_id ='.$lease_id;
		// }

		$unit_billing_where = 'billing_schedule.billing_schedule_id = property_billing.billing_schedule_id AND property_billing.invoice_type_id = invoice_type.invoice_type_id AND property_billing.property_billing_deleted = 0 '.$add_property;
		$unit_billing_table = 'billing_schedule,property_billing,invoice_type';
		$unit_billing_order = 'property_billing.property_billing_id';

		$unit_billing_query = $this->tenants_model->get_tenant_list($unit_billing_table, $unit_billing_where, $unit_billing_order);

		$v_data['query_invoice'] = $unit_billing_query;

		$v_data['document_types'] = $this->leases_model->all_document_types();
		$v_data['lease_other_documents'] = $this->leases_model->get_document_uploads($lease_id);
		$v_data['closing_lease_expenses'] = $this->leases_model->get_lease_closing_invoices($lease_id);

		$v_data['lease_path'] = $this->leases_path;
		$v_data['leases_location'] = $this->leases_location;
		$data['content'] = $this->load->view("leases/lease_detail", $v_data, TRUE);
		$data['title'] = 'Lease Detail';

		$this->load->view('admin/templates/general_page', $data);

	}

	public function add_billing($lease_id)
	{
		$this->form_validation->set_rules('charge_to', 'Charge To', 'trim|xss_clean');
		$this->form_validation->set_rules('billing_schedule_id', 'Billing', 'trim|xss_clean');
		$this->form_validation->set_rules('invoice_type_id', 'Invoice Type', 'trim|xss_clean');
		$this->form_validation->set_rules('arrears', 'Arrears', 'trim|xss_clean');
		$this->form_validation->set_rules('amount', 'Amount', 'trim|xss_clean');
		$this->form_validation->set_rules('start_date', 'Start date', 'trim|xss_clean');
		if ($this->form_validation->run())
		{

			if($this->leases_model->add_billing($lease_id))
			{
				$this->session->set_userdata('success_message', 'property has been edited');
			}
			else
			{
				$this->session->set_userdata('error_message', 'Sorry seems like you have another billing');
			}



		}
		$lease_number = $this->input->post('lease_number');

		if(!empty($lease_number))
		{
			redirect('lease-detail/'.$lease_id.'/'.$lease_number.'');
		}
		else
		{
			redirect('lease-detail/'.$lease_id.'');
		}

		
	}
	public function update_property_billing($property_billing_id,$invoice_type_id)
	{

		$this->form_validation->set_rules('billing_schedule_id', 'Billing', 'trim|xss_clean');
		$this->form_validation->set_rules('arrears', 'Arrears', 'trim|xss_clean');
		$this->form_validation->set_rules('amount', 'Amount', 'required|trim|xss_clean');
		$this->form_validation->set_rules('start_date', 'Start date', 'required|trim|xss_clean');
		if ($this->form_validation->run())
		{

			$data2 = array(
											'billing_schedule_id'=>$this->input->post("billing_schedule_id"),
											'arrears_bf'=>$this->input->post("arrears"),
											'initial_reading'=>$this->input->post("initial_reading"),
											'start_date'=>$this->input->post("start_date"),
											'billing_amount'=>$this->input->post("amount"),
											'property_billing_deleted' => 0
										);
			// var_dump($data2);die();
			$table = "property_billing";
			$this->db->where('property_billing_id', $property_billing_id);


			if($this->db->update($table, $data2))
			{
				$this->session->set_userdata('success_message', 'property billing has been edited successfully');
			}
			else
			{
				$this->session->set_userdata('error_message', 'property billing has not been edited');
			}



		}
		$redirect_url = $this->input->post('redirect_url');
		redirect($redirect_url);

	}

	public function delete_property_billing($property_billing_id,$lease_id)
	{
		$data2 = array(
			'property_billing_deleted'=>1,
		);

		$table = "property_billing";
		$this->db->where('property_billing_id', $property_billing_id);


		if($this->db->update($table, $data2))
		{
			$this->session->set_userdata('success_message', 'property billing has been deleted successfully');
		}
		else
		{
			$this->session->set_userdata('error_message', 'property billing has not been deleted');
		}

		redirect('lease-detail/'.$lease_id);

	}
	public function update_billing($lease_id)
	{
		$this->leases_model->update_lease_billing($lease_id);
		redirect('lease-detail/'.$lease_id);
	}


	public function close_lease($lease_id,$tenant_unit_id)
	{
		$this->form_validation->set_rules('payable_amount', 'Payable Amount', 'trim|xss_clean');
		$this->form_validation->set_rules('closing_end_date', 'Closing Date', 'trim|required|xss_clean');
		if ($this->form_validation->run())
		{

			// check if there is arrears on account
			$arrears = $this->input->post('arrears');

			$lease_detail = $this->leases_model->get_lease($lease_id);
			$lease_row = $lease_detail->row();

			$tenant_id = $lease_row->tenant_id;
			$rental_unit_id = $lease_row->rental_unit_id;

			$date_check = explode('-', $this->input->post("closing_end_date"));
			$month = $date_check[1];
			$year = $date_check[0];

			if($arrears > 0)
			{

				$receipt_number = $this->accounts_model->create_receipt_number();


				$data = array(
								'payment_method_id'=>$this->input->post("payment_method_id"),
								'bank_id'=>$this->input->post("bank_id"),
								'amount_paid'=>$arrears,
								'personnel_id'=>$this->session->userdata("personnel_id"),
								'transaction_code'=>$receipt_number,
								'payment_date'=>$this->input->post('closing_end_date'),
								'receipt_number'=>$receipt_number,
								'document_number'=>$receipt_number,
								'paid_by'=>$this->input->post('paid_by'),
								'payment_created'=>date("Y-m-d"),
								'year'=>$year,
								'month'=>$month,
								'confirm_number'=> $receipt_number,
								'payment_created_by'=>$this->session->userdata("personnel_id"),
								'approved_by'=>$personnel_id,'date_approved'=>date('Y-m-d')
							);

				if($this->db->insert('payments', $data))
				{
					$payment_id = $this->db->insert_id();

					$remarks = 'Payment for arrears on account ';

					$service = array(
										'payment_id'=>$payment_id,
										'amount_paid'=> $this->input->post("payable_amount"),
										'invoice_type_id' => 4,
										'payment_item_status' => 1,
										'lease_id' => $lease_id,
										'invoice_id' => 0,
										'personnel_id' => $this->session->userdata('personnel_id'),
										'payment_item_created' => date('Y-m-d'),
										'remarks'=>$remarks,
										'year'=>$year,
										'month'=>$month
									);

					$this->db->insert('payment_item',$service);
				}


			}

			$payable_amount = $this->input->post('payable_amount');
			if($payable_amount > 0)
			{
				// offset the payable to tenants refunds on account with a payment


				$data2 = array(
								'remarks'=>'Deposit Refund',
								'transaction_date'=>$this->input->post("closing_end_date"),
								'tenant_id'=>$tenant_id,
								'rental_unit_id'=>$rental_unit_id,
								'payment_method_id'=>$this->input->post("payment_method_id"),
								'amount'=>$this->input->post("payable_amount"),
								'refunded_on'=>date('Y-m-d'),
								'bank_id'=>$this->input->post("bank_id"),
								'cheque_no'=>$this->input->post("cheque_no"),
								'month'=>$month,
								'year'=>$year,
								'created_by'=>$this->session->userdata("personnel_id"),
								'lease_id'=>$lease_id,
							);
				$this->db->insert('tenant_refund', $data2);

			}

			$data2 = array(
				'remarks'=>$this->input->post("remarks"),
				'closing_end_date'=>$this->input->post("closing_end_date"),
				'lease_status'=>4,
			);
			$table = "leases";
			$this->db->where('lease_id', $lease_id);

			if($this->db->update($table, $data2))
			{
				$data23 = array(
					'tenant_unit_status'=>0,
				);
				// var_dump($data23); die();
				$table2 = "tenant_unit";
				$this->db->where('tenant_unit_id', $tenant_unit_id);
				$this->db->update($table2, $data23);

				$this->session->set_userdata('success_message', 'Lease successfully terminated');
			}
			else
			{
				$this->session->set_userdata('error_message', 'Leasse has not been terminisated');
			}



		}
		$redirect_url = $this->input->post('redirect_url');
		redirect($redirect_url);
	}





	//// BOBS FUNCTIONS
	public function import_lease_details()
	{
		$v_data['title'] = $data['title'] = 'Import Lease Details';

		$data['content'] = $this->load->view('import/import_lease_details', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}
	public function lease_details_import_template()
	{
			//export products template in excel
		$this->leases_model->lease_details_import_template();
	}


	public function lease_details_import()
	{
		if(isset($_FILES['import_csv']))
		{
			if(is_uploaded_file($_FILES['import_csv']['tmp_name']))
			{
				//import products from excel
				$response = $this->leases_model->import_csv_lease_details($this->csv_path);

				if($response == FALSE)
				{
					$v_data['import_response_error'] = 'Something went wrong. Please try again.';
				}

				else
				{
					if($response['check'])
					{
						$v_data['import_response'] = $response['response'];
					}

					else
					{
						$v_data['import_response_error'] = $response['response'];
					}
				}
			}

			else
			{
				$v_data['import_response_error'] = 'Please select a file to import.';
			}
		}

		else
		{
			$v_data['import_response_error'] = 'Please select a file to import.';
		}

		$v_data['title'] = $data['title'] = 'Import Lease Details';

		$data['content'] = $this->load->view('import/import_lease_details', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}





	public function import_invoice_details()
	{
		$v_data['title'] = $data['title'] = 'Import Invoice Details';

		$data['content'] = $this->load->view('import/import_invoice_details', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}
	public function invoice_details_import_template()
	{
			//export products template in excel
		$this->leases_model->invoice_details_import_template();
	}


	public function invoice_details_import()
	{
		if(isset($_FILES['import_csv']))
		{
			if(is_uploaded_file($_FILES['import_csv']['tmp_name']))
			{
				//import products from excel
				$response = $this->leases_model->import_csv_invoice_details($this->csv_path);

				if($response == FALSE)
				{
					$v_data['import_response_error'] = 'Something went wrong. Please try again.';
				}

				else
				{
					if($response['check'])
					{
						$v_data['import_response'] = $response['response'];
					}

					else
					{
						$v_data['import_response_error'] = $response['response'];
					}
				}
			}

			else
			{
				$v_data['import_response_error'] = 'Please select a file to import.';
			}
		}

		else
		{
			$v_data['import_response_error'] = 'Please select a file to import.';
		}

		$v_data['title'] = $data['title'] = 'Import Invoice Details';

		$data['content'] = $this->load->view('import/import_invoice_details', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}


	public function import_payment_details()
	{
		$v_data['title'] = $data['title'] = 'Import Payment Details';

		$data['content'] = $this->load->view('import/import_payment_details', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}
	public function payment_details_import_template()
	{
			//export products template in excel
		$this->leases_model->payment_details_import_template();
	}


	public function payment_details_import()
	{
		if(isset($_FILES['import_csv']))
		{
			if(is_uploaded_file($_FILES['import_csv']['tmp_name']))
			{
				//import products from excel
				$response = $this->leases_model->import_csv_payment_details($this->csv_path);

				if($response == FALSE)
				{
					$v_data['import_response_error'] = 'Something went wrong. Please try again.';
				}

				else
				{
					if($response['check'])
					{
						$v_data['import_response'] = $response['response'];
					}

					else
					{
						$v_data['import_response_error'] = $response['response'];
					}
				}
			}

			else
			{
				$v_data['import_response_error'] = 'Please select a file to import.';
			}
		}

		else
		{
			$v_data['import_response_error'] = 'Please select a file to import.';
		}

		$v_data['title'] = $data['title'] = 'Import Payment Details';

		$data['content'] = $this->load->view('import/import_payment_details', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}

	public function search_leases()
	{
		$property_id = $this->input->post('property_id');
		$tenant_name = $this->input->post('tenant_name');
		$contract_no = $this->input->post('contract_no');
		$tenant_number = $this->input->post('tenant_number');
		$rental_unit_name = $this->input->post('rental_unit_name');

		$search_title = '';

		if(!empty($property_id))
		{
			$this->db->where('property_id', $property_id);
			$query = $this->db->get('property');

			if($query->num_rows() > 0)
			{
				$row = $query->row();
				$search_title .= $row->property_name.' ';
			}


			$property_id = ' AND rental_unit.property_id = '.$property_id.' ';


		}

		if(!empty($tenant_name))
		{
			$search_title .= $tenant_name;

			$counter = explode(' ', $tenant_name);

			$total = count($counter);
			// var_dump($count);die();
			$count = 1;
			$tenant_name = ' AND (';
			for($r = 0; $r < $total; $r++)
			{
				if($count == $total)
				{
					$tenant_name .= ' tenants.tenant_name LIKE \'%'.$counter[$r].'%\'';
				}

				else
				{
					$tenant_name .= ' tenants.tenant_name LIKE \'%'.$counter[$r].'%\' AND ';
				}
				$count++;
			}
			$tenant_name .= ') ';
			
			// $tenant_name = ' AND tenants.tenant_name LIKE \'%'.$tenant_name.'%\'';
		
		
		}
		else
		{
			$tenant_name = '';
			$search_title .= '';
		}

		if(!empty($tenant_number))
		{
			$search_title .= $tenant_number.' ';
			$tenant_number = ' AND tenants.tenant_number LIKE \'%'.$tenant_number.'%\'';


		}
		else
		{
			$tenant_number = '';
			$search_title .= '';
		}

		if(!empty($contract_no))
		{
			$search_title .= $contract_no.' ';
			$contract_no = ' AND leases.lease_number = '.$contract_no.'';


		}
		else
		{
			$contract_no = '';
			$search_title .= '';
		}

		if(!empty($rental_unit_name))
		{
			$search_title .= $rental_unit_name.' ';
			$rental_unit_name = ' AND rental_unit.rental_unit_name LIKE \'%'.$rental_unit_name.'%\'';

		}
		else
		{
			$rental_unit_name = '';
			$search_title .= '';
		}


		$search = $property_id.$tenant_name.$rental_unit_name.$contract_no.$tenant_number;
		$property_search = $property_id;
		// var_dump($search); die();

		// $this->session->set_userdata('property_', $property_search);
		$this->session->set_userdata('all_leases_search', $search);
		$this->session->set_userdata('lease_search_title', $search_title);

		redirect('lease-manager/leases');
	}


	public function close_lease_search()
	{
		$this->session->unset_userdata('all_leases_search');
		$this->session->unset_userdata('lease_search_title');
		redirect('lease-manager/leases');
	}





	/*
	*
	*	Delete an existing rental_unit
	*	@param int $rental_unit_id
	*
	*/
	function delete_lease($lease_id)
	{
		$update_array['lease_deleted'] = 1;
		//get rental_unit data
		$this->db->where('lease_id',$lease_id);


		if($this->db->update('leases',$update_array))
		{
			$this->session->set_userdata('success_message', 'Lease was removed');
		}

		else
		{
			$this->session->set_userdata('error_message', 'Sorry could not remove the lease');
		}
		redirect('lease-manager/leases');
	}


	/*
	*
	*	Default action is to show all the registered lease
	*
	*/
	public function due_leases()
	{

		$where = 'leases.tenant_unit_id = tenant_unit.tenant_unit_id AND tenant_unit.tenant_id = tenants.tenant_id AND tenant_unit.rental_unit_id = rental_unit.rental_unit_id AND rental_unit.property_id = property.property_id AND leases.lease_deleted = 0 AND MONTH(leases.lease_end_date) ="'.date('m').'" AND YEAR(leases.lease_end_date) ="'.date('Y').'"  ';

		$properties = $this->session->userdata('properties');

		if(isset($properties) AND !empty($properties))
		{
			$where .= $properties;
		}

		$all_leases_search = $this->session->userdata('all_due_leases_search');

		if(isset($all_leases_search) AND !empty($all_leases_search))
		{
			$where .= $all_leases_search;
		}
		// var_dump($where); die();
		$table = 'leases,rental_unit,tenant_unit,tenants,property';
		$segment = 3;
		//pagination
		$this->load->library('pagination');
		$config['base_url'] = base_url().'lease-management/leases';
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		// var_dump($config); die();
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;

		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';

		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';

		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';

		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';

		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';

		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);

		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->leases_model->get_all_leases($table, $where, $config["per_page"], $page);
		// var_dump($query); die();
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		$v_data['title'] ='Due Leases';


		$tenant_order = 'tenants.tenant_name';
		$tenant_table = 'tenants';
		$tenant_where = 'tenants.tenant_status = 1';

		$tenant_query = $this->tenants_model->get_tenant_list($tenant_table, $tenant_where, $tenant_order);
		$rs8 = $tenant_query->result();
		$tenants_list = '';
		foreach ($rs8 as $tenant_rs) :
			$tenant_id = $tenant_rs->tenant_id;
			$tenant_name = $tenant_rs->tenant_name;

			$tenant_national_id = $tenant_rs->tenant_national_id;
			$tenant_phone_number = $tenant_rs->tenant_phone_number;

		    $tenants_list .="<option value='".$tenant_id."'>".$tenant_name." Phone: ".$tenant_phone_number."</option>";

		endforeach;

		$v_data['tenants_list'] = $tenants_list;


		$rental_unit_order = 'rental_unit.property_id = property.property_id';
		$rental_unit_table = 'rental_unit,property';
		$rental_unit_where = 'rental_unit.rental_unit_status = 1 and rental_unit.property_id = property.property_id';

		$rental_unit_query = $this->tenants_model->get_tenant_list($rental_unit_table, $rental_unit_where, $rental_unit_order);
		$rs8 = $rental_unit_query->result();
		$rental_unit_list = '';
		foreach ($rs8 as $rental_unit) :
			$rental_unit_id = $rental_unit->rental_unit_id;
			$rental_unit_name = $rental_unit->rental_unit_name;

			$property_name = $rental_unit->property_name;

		    $rental_unit_list .="<option value='".$rental_unit_id."'>".$rental_unit_name." Property: ".$property_name."</option>";

		endforeach;

		$v_data['rental_unit_list'] = $rental_unit_list;

		$properties = $this->property_model->get_active_property();

		// $this->accounts_model->update_invoices();


		$rs8 = $properties->result();
		$property_list = '';
		foreach ($rs8 as $property_rs) :
			$property_id = $property_rs->property_id;
			$property_name = $property_rs->property_name;
			$property_location = $property_rs->property_location;

		    $property_list .="<option value='".$property_id."'>".$property_name." Location: ".$property_location."</option>";

		endforeach;
		$v_data['property_list'] = $property_list;

		$data['content'] = $this->load->view('leases/due_leases', $v_data, true);

		$data['title'] = 'Due Leases';

		$this->load->view('admin/templates/general_page', $data);
	}

	public function search_due_leases()
	{
		$property_id = $this->input->post('property_id');
		$tenant_name = $this->input->post('tenant_name');
		$rental_unit_name = $this->input->post('rental_unit_name');

		$search_title = '';

		if(!empty($property_id))
		{
			$this->db->where('property_id', $property_id);
			$query = $this->db->get('property');

			if($query->num_rows() > 0)
			{
				$row = $query->row();
				$search_title .= $row->property_name.' ';
			}


			$property_id = ' AND rental_unit.property_id = '.$property_id.' ';


		}

		if(!empty($tenant_name))
		{
			$search_title .= $tenant_name;

			$counter = explode(' ', $tenant_name);

			$total = count($counter);
			// var_dump($count);die();
			$count = 1;
			$tenant_name = ' AND (';
			for($r = 0; $r < $total; $r++)
			{
				if($count == $total)
				{
					$tenant_name .= ' tenants.tenant_name LIKE \'%'.$counter[$r].'%\'';
				}

				else
				{
					$tenant_name .= ' tenants.tenant_name LIKE \'%'.$counter[$r].'%\' AND ';
				}
				$count++;
			}
			$tenant_name .= ') ';
			
			// $tenant_name = ' AND tenants.tenant_name LIKE \'%'.$tenant_name.'%\'';
		
		
		}
		else
		{
			$tenant_name = '';
			$search_title .= '';
		}
		if(!empty($rental_unit_name))
		{
			$search_title .= $rental_unit_name.' ';
			$rental_unit_name = ' AND rental_unit.rental_unit_name LIKE \'%'.$rental_unit_name.'%\'';

		}
		else
		{
			$rental_unit_name = '';
			$search_title .= '';
		}


		$search = $property_id.$tenant_name.$rental_unit_name;
		$property_search = $property_id;
		// var_dump($search); die();

		// $this->session->set_userdata('property_', $property_search);
		$this->session->set_userdata('all_due_leases_search', $search);
		$this->session->set_userdata('due_lease_search_title', $search_title);

		redirect('lease-manager/due-leases');
	}

	public function close_due_lease_search()
	{
		$this->session->unset_userdata('all_due_leases_search');
		$this->session->unset_userdata('due_lease_search_title');
		redirect('lease-manager/due-leases');
	}
	public function upload_documents($lease_id)
	{
		$image_error = '';
		$this->session->unset_userdata('upload_error_message');
		$document_name = 'document_scan';

		//upload image if it has been selected
		$response = $this->leases_model->upload_any_file($this->leases_path, $this->leases_location, $document_name, 'document_scan');
		// var_dump($response); die();
		if($response)
		{
			$document_upload_location = $this->leases_location.$this->session->userdata($document_name);
		}

		//case of upload error
		else
		{
			$image_error = $this->session->userdata('upload_error_message');
			$this->session->unset_userdata('upload_error_message');
		}

		$document = $this->session->userdata($document_name);

		$this->form_validation->set_rules('document_type_id', 'Document Type', 'required|xss_clean');
		$this->form_validation->set_rules('remarks', 'Description', 'required|xss_clean');
		//if form has been submitted
		if ($this->form_validation->run())
		{

			if($this->leases_model->upload_lease_documents($lease_id, $document))
			{
				$this->session->set_userdata('success_message', 'Document uploaded successfully');
				$this->session->unset_userdata($document_name);
			}

			else
			{
				$this->session->set_userdata('error_message', 'Could not upload document. Please try again');
			}
		}
		else
		{
			$this->session->set_userdata('error_message', 'Could not upload document. Please try again');
		}
		$redirect = $this->input->post('redirect_url');

		redirect($redirect);
	}

	public function vacation_notice_lease($lease_id,$tenant_unit_id)
	{
		// var_dump($_POST); die();
		$this->form_validation->set_rules('notice_date', 'Date', 'trim|required|xss_clean');
		$this->form_validation->set_rules('notice_remarks', 'Notice Remarks', 'trim|required|xss_clean');
		if ($this->form_validation->run())
		{

			$data2 = array(
							'notice_date'=>$this->input->post("notice_date"),
							'notice_remarks'=>$this->input->post("notice_remarks"),
							'lease_status'=>3,
							);
			// var_dump($data2); die();
			$table = "leases";
			$this->db->where('lease_id', $lease_id);


			if($this->db->update($table, $data2))
			{
				$data23 = array(
					'tenant_unit_status'=>0,
				);
				// var_dump($data23); die();
				$table2 = "tenant_unit";
				$this->db->where('tenant_unit_id', $tenant_unit_id);
				$this->db->update($table2, $data23);
				$this->session->set_userdata('success_message', 'Lease successfully put on hold');
			}
			else
			{
				$this->session->set_userdata('error_message', 'Leasse has not been put on notice');
			}



		}
		$redirect_url = $this->input->post('redirect_url');
		redirect($redirect_url);
	}




	/*
	*
	*	Default action is to show all the registered lease
	*
	*/
	public function leases_on_hold()
	{

		$where = 'leases.tenant_unit_id = tenant_unit.tenant_unit_id AND tenant_unit.tenant_id = tenants.tenant_id AND tenant_unit.rental_unit_id = rental_unit.rental_unit_id AND rental_unit.property_id = property.property_id AND leases.lease_deleted = 0 AND leases.lease_status = 3 ';

		$properties = $this->session->userdata('properties');

		if(isset($properties) AND !empty($properties))
		{
			$where .= $properties;
		}

		$all_leases_search = $this->session->userdata('all_held_leases_search');

		if(isset($all_leases_search) AND !empty($all_leases_search))
		{
			$where .= $all_leases_search;
		}
		// var_dump($where); die();
		$table = 'leases,rental_unit,tenant_unit,tenants,property';
		$segment = 3;
		//pagination
		$this->load->library('pagination');
		$config['base_url'] = base_url().'lease-manager/leases-on-hold';
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		// var_dump($config); die();
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;

		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';

		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';

		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';

		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';

		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';

		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);

		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->leases_model->get_all_leases($table, $where, $config["per_page"], $page);
		// var_dump($query); die();
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		$v_data['title'] ='Due Leases';


		$tenant_order = 'tenants.tenant_name';
		$tenant_table = 'tenants';
		$tenant_where = 'tenants.tenant_status = 1';

		$tenant_query = $this->tenants_model->get_tenant_list($tenant_table, $tenant_where, $tenant_order);
		$rs8 = $tenant_query->result();
		$tenants_list = '';
		foreach ($rs8 as $tenant_rs) :
			$tenant_id = $tenant_rs->tenant_id;
			$tenant_name = $tenant_rs->tenant_name;

			$tenant_national_id = $tenant_rs->tenant_national_id;
			$tenant_phone_number = $tenant_rs->tenant_phone_number;

		    $tenants_list .="<option value='".$tenant_id."'>".$tenant_name." Phone: ".$tenant_phone_number."</option>";

		endforeach;

		$v_data['tenants_list'] = $tenants_list;


		$rental_unit_order = 'rental_unit.property_id = property.property_id';
		$rental_unit_table = 'rental_unit,property';
		$rental_unit_where = 'rental_unit.rental_unit_status = 1 and rental_unit.property_id = property.property_id';

		$rental_unit_query = $this->tenants_model->get_tenant_list($rental_unit_table, $rental_unit_where, $rental_unit_order);
		$rs8 = $rental_unit_query->result();
		$rental_unit_list = '';
		foreach ($rs8 as $rental_unit) :
			$rental_unit_id = $rental_unit->rental_unit_id;
			$rental_unit_name = $rental_unit->rental_unit_name;

			$property_name = $rental_unit->property_name;

		    $rental_unit_list .="<option value='".$rental_unit_id."'>".$rental_unit_name." Property: ".$property_name."</option>";

		endforeach;

		$v_data['rental_unit_list'] = $rental_unit_list;

		$properties = $this->property_model->get_active_property();

		// $this->accounts_model->update_invoices();


		$rs8 = $properties->result();
		$property_list = '';
		foreach ($rs8 as $property_rs) :
			$property_id = $property_rs->property_id;
			$property_name = $property_rs->property_name;
			$property_location = $property_rs->property_location;

		    $property_list .="<option value='".$property_id."'>".$property_name." Location: ".$property_location."</option>";

		endforeach;
		$v_data['property_list'] = $property_list;

		$data['content'] = $this->load->view('leases/lease_notices', $v_data, true);

		$data['title'] = 'Due Leases';

		$this->load->view('admin/templates/general_page', $data);
	}
	public function search_held_leases()
	{
		$property_id = $this->input->post('property_id');
		$tenant_name = $this->input->post('tenant_name');
		$contract_no = $this->input->post('contract_no');
		$rental_unit_name = $this->input->post('rental_unit_name');

		$search_title = '';

		if(!empty($property_id))
		{
			$this->db->where('property_id', $property_id);
			$query = $this->db->get('property');

			if($query->num_rows() > 0)
			{
				$row = $query->row();
				$search_title .= $row->property_name.' ';
			}


			$property_id = ' AND rental_unit.property_id = '.$property_id.' ';


		}

		if(!empty($tenant_name))
		{
			$search_title .= $tenant_name;

			$counter = explode(' ', $tenant_name);

			$total = count($counter);
			// var_dump($count);die();
			$count = 1;
			$tenant_name = ' AND (';
			for($r = 0; $r < $total; $r++)
			{
				if($count == $total)
				{
					$tenant_name .= ' tenants.tenant_name LIKE \'%'.$counter[$r].'%\'';
				}

				else
				{
					$tenant_name .= ' tenants.tenant_name LIKE \'%'.$counter[$r].'%\' AND ';
				}
				$count++;
			}
			$tenant_name .= ') ';
			
			// $tenant_name = ' AND tenants.tenant_name LIKE \'%'.$tenant_name.'%\'';
		
		
		}
		else
		{
			$tenant_name = '';
			$search_title .= '';
		}

		if(!empty($contract_no))
		{
			$search_title .= $contract_no.' ';
			$contract_no = ' AND leases.lease_number = '.$contract_no.'';


		}
		else
		{
			$contract_no = '';
			$search_title .= '';
		}

		if(!empty($rental_unit_name))
		{
			$search_title .= $rental_unit_name.' ';
			$rental_unit_name = ' AND rental_unit.rental_unit_name LIKE \'%'.$rental_unit_name.'%\'';

		}
		else
		{
			$rental_unit_name = '';
			$search_title .= '';
		}


		$search = $property_id.$tenant_name.$rental_unit_name.$contract_no;
		$property_search = $property_id;
		// var_dump($search); die();

		// $this->session->set_userdata('property_', $property_search);
		$this->session->set_userdata('all_held_leases_search', $search);
		$this->session->set_userdata('held_lease_search_title', $search_title);

		redirect('lease-manager/leases-on-hold');
	}


	public function close_held_lease_search()
	{
		$this->session->unset_userdata('all_held_leases_search');
		$this->session->unset_userdata('held_lease_search_title');
		redirect('lease-manager/leases-on-hold');
	}
	public function terminated_leases()
	{

		$where = 'leases.tenant_unit_id = tenant_unit.tenant_unit_id AND tenant_unit.tenant_id = tenants.tenant_id AND tenant_unit.rental_unit_id = rental_unit.rental_unit_id AND rental_unit.property_id = property.property_id AND leases.lease_deleted = 0 AND leases.lease_status > 2 ';

		$properties = $this->session->userdata('properties');

		if(isset($properties) AND !empty($properties))
		{
			$where .= $properties;
		}

		$all_leases_search = $this->session->userdata('all_terminated_leases_search');

		if(isset($all_leases_search) AND !empty($all_leases_search))
		{
			$where .= $all_leases_search;
		}
		// var_dump($where); die();
		$table = 'leases,rental_unit,tenant_unit,tenants,property';
		$segment = 3;
		//pagination
		$this->load->library('pagination');
		$config['base_url'] = base_url().'lease-manager/terminated-leases';
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		// var_dump($config); die();
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;

		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';

		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';

		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';

		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';

		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';

		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);

		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
				$v_data["links"] = $this->pagination->create_links();
		$query = $this->leases_model->get_all_leases($table, $where, $config["per_page"], $page);
		// var_dump($query); die();
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		$v_data['title'] ='Inactive Leases';


		$tenant_order = 'tenants.tenant_name';
		$tenant_table = 'tenants';
		$tenant_where = 'tenants.tenant_status = 1';

		$tenant_query = $this->tenants_model->get_tenant_list($tenant_table, $tenant_where, $tenant_order);
		$rs8 = $tenant_query->result();
		$tenants_list = '';
		foreach ($rs8 as $tenant_rs) :
			$tenant_id = $tenant_rs->tenant_id;
			$tenant_name = $tenant_rs->tenant_name;

			$tenant_national_id = $tenant_rs->tenant_national_id;
			$tenant_phone_number = $tenant_rs->tenant_phone_number;

				$tenants_list .="<option value='".$tenant_id."'>".$tenant_name." Phone: ".$tenant_phone_number."</option>";

		endforeach;

		$v_data['tenants_list'] = $tenants_list;


		$rental_unit_order = 'rental_unit.property_id = property.property_id';
		$rental_unit_table = 'rental_unit,property';
		$rental_unit_where = 'rental_unit.rental_unit_status = 1 and rental_unit.property_id = property.property_id';

		$rental_unit_query = $this->tenants_model->get_tenant_list($rental_unit_table, $rental_unit_where, $rental_unit_order);
		$rs8 = $rental_unit_query->result();
		$rental_unit_list = '';
		foreach ($rs8 as $rental_unit) :
			$rental_unit_id = $rental_unit->rental_unit_id;
			$rental_unit_name = $rental_unit->rental_unit_name;

			$property_name = $rental_unit->property_name;

				$rental_unit_list .="<option value='".$rental_unit_id."'>".$rental_unit_name." Property: ".$property_name."</option>";

		endforeach;

		$v_data['rental_unit_list'] = $rental_unit_list;

		$properties = $this->property_model->get_active_property();

		// $this->accounts_model->update_invoices();


		$rs8 = $properties->result();
		$property_list = '';
		foreach ($rs8 as $property_rs) :
			$property_id = $property_rs->property_id;
			$property_name = $property_rs->property_name;
			$property_location = $property_rs->property_location;

				$property_list .="<option value='".$property_id."'>".$property_name." Location: ".$property_location."</option>";

		endforeach;
		$v_data['property_list'] = $property_list;

		$data['content'] = $this->load->view('leases/terminated_leases', $v_data, true);

		$data['title'] = 'Due Leases';

		$this->load->view('admin/templates/general_page', $data);
	}
	public function search_terminated_leases()
	{
		$property_id = $this->input->post('property_id');
		$tenant_name = $this->input->post('tenant_name');
		$contract_no = $this->input->post('contract_no');
		$rental_unit_name = $this->input->post('rental_unit_name');

		$search_title = '';

		if(!empty($property_id))
		{
			$this->db->where('property_id', $property_id);
			$query = $this->db->get('property');

			if($query->num_rows() > 0)
			{
				$row = $query->row();
				$search_title .= $row->property_name.' ';
			}


			$property_id = ' AND rental_unit.property_id = '.$property_id.' ';


		}

		if(!empty($tenant_name))
		{
			$search_title .= $tenant_name;

			$counter = explode(' ', $tenant_name);

			$total = count($counter);
			// var_dump($count);die();
			$count = 1;
			$tenant_name = ' AND (';
			for($r = 0; $r < $total; $r++)
			{
				if($count == $total)
				{
					$tenant_name .= ' tenants.tenant_name LIKE \'%'.$counter[$r].'%\'';
				}

				else
				{
					$tenant_name .= ' tenants.tenant_name LIKE \'%'.$counter[$r].'%\' AND ';
				}
				$count++;
			}
			$tenant_name .= ') ';
			
			// $tenant_name = ' AND tenants.tenant_name LIKE \'%'.$tenant_name.'%\'';
		
		
		}
		else
		{
			$tenant_name = '';
			$search_title .= '';
		}

		if(!empty($contract_no))
		{
			$search_title .= $contract_no.' ';
			$contract_no = ' AND leases.lease_number = '.$contract_no.'';


		}
		else
		{
			$contract_no = '';
			$search_title .= '';
		}

		if(!empty($rental_unit_name))
		{
			$search_title .= $rental_unit_name.' ';
			$rental_unit_name = ' AND rental_unit.rental_unit_name LIKE \'%'.$rental_unit_name.'%\'';

		}
		else
		{
			$rental_unit_name = '';
			$search_title .= '';
		}


		$search = $property_id.$tenant_name.$rental_unit_name.$contract_no;
		$property_search = $property_id;
		// var_dump($search); die();

		// $this->session->set_userdata('property_', $property_search);
		$this->session->set_userdata('all_terminated_leases_search', $search);
		$this->session->set_userdata('terminated_lease_search_title', $search_title);

		redirect('lease-manager/terminated-leases');
	}


	public function close_terminated_lease_search()
	{
		$this->session->unset_userdata('all_terminated_leases_search');
		$this->session->unset_userdata('terminated_lease_search_title');
		redirect('lease-manager/terminated-leases');
	}

	public function update_system_dates()
	{

		$this->db->where('lease_number > 0');
		$query_check = $this->db->get('leases');

		if($query_check->num_rows() > 0)
		{
			foreach ($query_check->result() as $key => $value) {
				// code...
				$lease_start_date = $value->lease_start_date;
				$lease_duration = $value->lease_duration;

				$lease_id = $value->lease_id;
				$lease_end_date  = date("Y-m-d",strtotime(date("Y-m-d", strtotime($lease_start_date)) . '+'.$lease_duration.' month'));
				$array['lease_end_date'] = $lease_end_date;

				$this->db->where('lease_id',$lease_id);
				$this->db->update('leases',$array);
				#
			}
		}
		// $date = '2019-03-01';//$this->input->post("lease_start_date");
		// $lease_duration = 36;
		// $lease_end_date  = date("Y-m-d",strtotime(date("Y-m-d", strtotime($date)) . '+'.$lease_duration.' month'));
		// var_dump($lease_end_date);
	}

	public function tenant_lease_detail($rental_unit_id)
	{
		$tenancy_query = $this->rental_unit_model->get_tenancy_details($rental_unit_id);
		// var_dump($tenancy_query->num_rows());die();
		if($tenancy_query->num_rows() == 1)
		{
		  $value = $tenancy_query->row();
		  $tenant_name = $value->tenant_name;
		  $lease_id = $value->lease_id;
		  $lease_id = $lease_id;
		}
		else
		{
		  $lease_id = null;

		}
		if($lease_id > 0)
		{
			//get lease data
			$where = 'leases.lease_id > 0 AND leases.tenant_unit_id = tenant_unit.tenant_unit_id AND tenant_unit.tenant_id = tenants.tenant_id AND tenant_unit.rental_unit_id = rental_unit.rental_unit_id AND rental_unit.property_id = property.property_id  and leases.lease_id ='.$lease_id;
			$table = 'leases,rental_unit,tenant_unit,tenants,property';

			$this->db->where($where);
			$rental_unit_query = $this->db->get($table);
			$rental_unit_row = $rental_unit_query->row();
			$v_data['rental_unit_row'] = $rental_unit_row;



			$rental_unit = $this->session->userdata('rental_unit_file_name');

			$v_data['lease_id'] = $lease_id;

			$invoice_type_order = 'invoice_type.invoice_type_id';
			$invoice_type_table = 'invoice_type';
			$invoice_type_where = 'invoice_type.invoice_type_status = 1 AND generaljournalaccountid = 6';

			$invoice_type_query = $this->tenants_model->get_tenant_list($invoice_type_table, $invoice_type_where, $invoice_type_order);
			$rs8 = $invoice_type_query->result();
			$invoice_type_list = '';
			foreach ($rs8 as $invoice_rs) :
				$invoice_type_id = $invoice_rs->invoice_type_id;
				$invoice_type_name = $invoice_rs->invoice_type_name;


					$invoice_type_list .="<option value='".$invoice_type_id."'>".$invoice_type_name."</option>";

			endforeach;

			$v_data['invoice_type_list'] = $invoice_type_list;



			$billing_schedule_order = 'billing_schedule.billing_schedule_id';
			$billing_schedule_table = 'billing_schedule';
			$billing_schedule_where = 'billing_schedule.billing_schedule_status = 1';

			$billing_schedule_query = $this->tenants_model->get_tenant_list($billing_schedule_table, $billing_schedule_where, $billing_schedule_order);
			$rs8 = $billing_schedule_query->result();
			$billing_schedule_list = '';
			foreach ($rs8 as $billing_rs) :
				$billing_schedule_id = $billing_rs->billing_schedule_id;
				$billing_schedule_name = $billing_rs->billing_schedule_name;


					$billing_schedule_list .="<option value='".$billing_schedule_id."'>".$billing_schedule_name."</option>";

			endforeach;

			$v_data['billing_schedule_list'] = $billing_schedule_list;


			$unit_billing_where = 'billing_schedule.billing_schedule_id = property_billing.billing_schedule_id AND property_billing.invoice_type_id = invoice_type.invoice_type_id AND property_billing.property_billing_deleted = 0 AND property_billing.lease_id = '.$lease_id;
			$unit_billing_table = 'billing_schedule,property_billing,invoice_type';
			$unit_billing_order = 'property_billing.property_billing_id';

			$unit_billing_query = $this->tenants_model->get_tenant_list($unit_billing_table, $unit_billing_where, $unit_billing_order);

			$v_data['lease_other_documents'] = $this->leases_model->get_document_uploads($lease_id);
			$v_data['closing_lease_expenses'] = $this->leases_model->get_lease_closing_invoices($lease_id);
			$v_data['query_invoice'] = $unit_billing_query;
		}


		// var_dump($lease_id);die();

		$v_data['document_types'] = $this->leases_model->all_document_types();
		$v_data['lease_path'] = $this->leases_path;
		$v_data['lease_id'] = $lease_id;
		$v_data['rental_unit_id'] = $rental_unit_id;
		$v_data['leases_location'] = $this->leases_location;
		$data['content'] = $this->load->view("leases/tenant_lease_detail", $v_data, TRUE);
		$data['title'] = 'Lease Detail';

		$this->load->view('admin/templates/general_page', $data);
	}

	public function transfer_lease($lease_id_old,$tenant_unit_id)
	{
		$lease_number = $this->input->post('lease_number');
		$rental_unit_id = $this->input->post('rental_unit_id');


		$lease_error = $this->session->userdata('lease_error_message');

		$this->form_validation->set_rules('lease_number', 'Lease Number', 'required|trim|xss_clean');
		$this->form_validation->set_rules('rental_unit_id', 'Rental Unit id', 'required|trim|xss_clean');
		$this->form_validation->set_rules('transfer_remarks', 'Remarks', 'required|trim|xss_clean');
		$this->form_validation->set_rules('transfer_date', 'Date of transfer', 'required|trim|xss_clean');
		$this->form_validation->set_rules('tenant_id', ' Tenant', 'required|trim|xss_clean');
		$this->form_validation->set_rules('rental_unit_name', ' Tenant', 'required|trim|xss_clean');
		$this->form_validation->set_rules('rental_unit_price', ' Rental unit Price', 'required|trim|xss_clean');

		if ($this->form_validation->run())
		{
			
			//  start transfer process
			// get create a new lease

			$tenant_id = $this->input->post('tenant_id');
			$rental_unit_id = $this->input->post('rental_unit_id');
			$rental_unit_id_old = $this->input->post('rental_unit_id_old');
			$rental_unit_name = $this->input->post('rental_unit_name');
			$lease_number = $this->input->post('lease_number');
			$rental_unit_price = $this->input->post('rental_unit_price');
			$transfer_date = $this->input->post('transfer_date');

			$lease_duration = 36;
			$date = $this->input->post("transfer_date");
			$lease_end_date  = date("Y-m-d",strtotime(date("Y-m-d", strtotime($date)) . '+'.$lease_duration.' month'));


			$this->db->where('rental_unit.rental_unit_id ='.$this->input->post('rental_unit_id'));
			$this->db->select('rental_unit.rental_unit_price,rental_unit_name');
			$query= $this->db->get('rental_unit');
			// var_dump($query);die();
		
			$value = $query->row();
			$rental_unit_price_new = $value->rental_unit_price;		
			$rental_unit_name_to = $value->rental_unit_name;		
				
			$this->db->where('rental_unit_id', $rental_unit_id);
			$this->db->limit(1);
			$this->db->order_by('rental_unit_price_id','DESC');
			$query_cog = $this->db->get('rental_unit_prices');
			$row_cog = $query_cog->row();
			$current = 0;
			if($query_cog->num_rows() > 0)
			{
				$tax_amount = $row_cog->tax_amount;
				$current = $row_cog->current;
			}

			if(empty($current))
			{
				if(empty($rental_unit_price_new))
				{
					$current = 0;
				}
				else
				{
					$current = $rental_unit_price_new;
				}
				
			}
			else
			{
				if(empty($rental_unit_price_new))
				{
					$current = 0;
				}
				else
				{
					$current = $rental_unit_price_new;
				}
			}
			
			// var_dump($current);die();
			if($rental_unit_price == $current)
			{
				$insert_array = array(
								'tenant_id'=>$this->input->post('tenant_id'),
								'rental_unit_id'=>$this->input->post('rental_unit_id'),
								'created'=>date('Y-m-d'),
								'created_by'=>$this->session->userdata('personnel_id'),
								'tenant_unit_status'=>1,
								);
				$this->db->insert('tenant_unit',$insert_array);

				$tenant_unit_id = $this->db->insert_id();

				$insert_array = array(
								'tenant_unit_id'=>$tenant_unit_id,
								'lease_start_date'=>$this->input->post("transfer_date"),
								'lease_duration'=>36,
								'lease_end_date'=>$lease_end_date,
								'rental_unit_id'=>$rental_unit_id,
								'lease_number'=>$this->input->post('lease_number'),
								'lease_status'=>5,
								'tenant_id'=>$this->input->post('tenant_id')
								);
				$this->db->insert('leases',$insert_array);
				$lease_id = $this->db->insert_id();

			
				
				// var_dump($tax_amount); die();
				// put the default rent amount
				
				if($current > 0)
				{
					$charge_to = 1;
					$invoice_type_id = 1;
					$billing_schedule_id = 1;
					$start_date = date('Y-m-d');
					$arrears = 0;
					$initial_reading = 0;
					$amount = $rental_unit_price;
					// $where_array = array(
					// 						'charge_to'=>1,
					// 						'invoice_type_id' => $invoice_type_id,
					// 						'billing_schedule_id' => $billing_schedule_id,
					// 						'lease_id' => $lease_id,
					// 						'start_date' => $this->input->post("transfer_date"),
					// 						'initial_reading' => $initial_reading,
					// 						'arrears_bf' => $arrears,
					// 						'billing_amount'=>$current,
					// 						'tax_amount'=>$tax_amount
					// 					);
					// $query = $this->db->insert('property_billing',$where_array);

					$update_array = array(
											'rental_unit_price'=>$current
										 );
					$rental_unit_id = $this->input->post('rental_unit_id');
					$this->db->where('rental_unit_id',$rental_unit_id);
					$this->db->update('rental_unit',$update_array);
				}	


				// $item_list = $this->accounts_model->get_all_invoice_items($lease_id_old);
			  
			 //    if($item_list->num_rows() > 0)
			 //    {
			 //      foreach ($item_list->result() as $key => $value) {
			 //        // code...
			 //        $invoice_type_name = $value->invoice_type_name;
			 //        $invoice_type_id = $value->accountId;
			 //        $total_amount = $value->total_amount;

			 //        $start_date_explode = explode('-', $transfer_date);
			 //        $invoice_year = $start_date_explode[0];
				// 	$invoice_month = $start_date_explode[1];

				// 	$next_date = $invoice_year.'-'.$invoice_month.'-'.'01';
				// 	$next_date = strtotime($transfer_date);
				// 	$next_quarter = ceil(date('m', $next_date) / 3);
				// 	$next_month = ($next_quarter * 3) - 2;
				// 	$next_year = date('Y', $next_date);
				// 	$next_quarter = 'AC'.$next_quarter.'-'.$next_year;

				// 	$concate = $invoice_year.'-'.$invoice_month;
				// 	$date = date('F Y',strtotime($concate));

				// 	if($total_amount > 0)
				// 	{
				// 		$document_number = $this->accounts_model->create_invoice_number();
				// 		// create the invoice
				// 		$insertarray['invoice_date'] = $date;
				// 		$insertarray['invoice_year'] = $invoice_year;
				// 		$insertarray['invoice_month'] = $invoice_month;
				// 		$insertarray['lease_id'] = $lease_id;
				// 		$insertarray['rental_unit_id'] = $rental_unit_id;
				// 		$insertarray['tenant_id'] = $tenant_id;
				// 		$insertarray['document_number'] = $document_number;
				// 		$insertarray['total_amount'] = $total_amount;
				// 		$insertarray['account_id'] = $lease_number;
				// 		$insertarray['created_by'] = $this->session->userdata('personnel_id');
				// 		$insertarray['created'] = date('Y-m-d');

				// 		$this->db->insert('lease_invoice',$insertarray);
				// 		$lease_invoice_id = $this->db->insert_id();
						

				// 		$remarks =  $invoice_type_name.' Balance B/F from unit '.$rental_unit_name.' as from '.$date;					

				// 		$insert_array = array(
				// 						'lease_id' => $lease_id,
				// 						'year' => $invoice_year,
				// 						'month' => $invoice_month,
				// 						'lease_invoice_id' => $lease_invoice_id,
				// 						'invoice_amount' => $total_amount,
				// 						'invoice_type' => $invoice_type_id,
				// 						'billing_schedule_quarter' => $next_quarter,
				// 						'invoice_item_status'=>1,
				// 						'tax_amount'=>0,
				// 						'created'=>date('Y-m-d'),
				// 						'personnel_id'=>$this->session->userdata('personnel_id'),
				// 						'created_by'=>$this->session->userdata('personnel_id'),
				// 						'tenant_id'=>$tenant_id,
				// 						'rental_unit_id'=>$rental_unit_id,
				// 						'lease_number'=>$lease_number,
				// 						'remarks'=>$remarks

				// 				 	 );
				// 		$this->db->insert('invoice',$insert_array);
				// 		// create a credit note on the previous account
				// 		$receipt_number = $this->accounts_model->create_credit_note_number();

				// 		$reason = $invoice_type_name.' Balance C/F from '.$rental_unit_name.' to '.$rental_unit_name_to.' as from '.$date;
				// 		$data = array(
				// 			'credit_note_amount'=>$total_amount,
				// 			'personnel_id'=>$this->session->userdata("personnel_id"),
				// 			'credit_note_date'=>$this->input->post('transfer_date'),
				// 			'document_number'=>$receipt_number,
				// 			'credit_note_created'=>date("Y-m-d"),
				// 			'year'=>$invoice_year,
				// 			'month'=>$invoice_month,
				// 			'credit_note_created_by'=>$this->session->userdata("personnel_id"),
				// 			'approved_by'=>$this->session->userdata("personnel_id"),
				// 			'rental_unit_id'=>$rental_unit_id_old,
				// 			'tenant_id'=>$tenant_id,
				// 			'account_id'=>$lease_number,
				// 			'date_approved'=>date('Y-m-d'),
				// 			'remarks'=>$reason,
				// 			'bank_id'=>6
				// 		);
				// 		$data['lease_id'] = $lease_id_old;

				// 		if($this->db->insert('credit_notes', $data))
				// 		{
				// 			$credit_note_id = $this->db->insert_id();
				// 			$reason = $invoice_type_name.' Balance C/F from '.$rental_unit_name.' to '.$rental_unit_name_to.' as from '.$date;
				// 			$service_credit = array(
				// 					'credit_note_id'=>$credit_note_id,
				// 					'credit_note_amount'=> $total_amount,
				// 					'invoice_type_id' => $invoice_type_id,
				// 					'credit_note_item_status' => 1,
				// 					'personnel_id' => $this->session->userdata('personnel_id'),
				// 					'credit_note_item_created' => date('Y-m-d'),
				// 					'invoice_id'=>$credit_note_id,
				// 					'lease_id'=>$lease_id_old,
				// 					'rental_unit_id'=>$rental_unit_id_old,
				// 					'tenant_id'=>$tenant_id,
				// 					'lease_number'=>$lease_number,
				// 					'remarks'=>$reason
				// 				);
				// 			$this->db->insert('credit_note_item',$service_credit);
				// 		}
				// 	}
				// 	else
				// 	{
				// 		// change the value to a positive figure 
				// 		$amount_paid = -$total_amount;
				// 		// create a prepayment on that account 

				// 		$receipt_number = $this->accounts_model->create_receipt_number();

				// 		$data = array(
				// 			'payment_method_id'=>6,
				// 			'bank_id'=>20,
				// 			'amount_paid'=>$amount_paid,
				// 			'personnel_id'=>$this->session->userdata("personnel_id"),
				// 			'transaction_code'=>$receipt_number,
				// 			'payment_date'=>$this->input->post('transfer_date'),
				// 			'receipt_number'=>$receipt_number,
				// 			'paid_by'=>'',
				// 			'payment_created'=>date("Y-m-d"),
				// 			'rental_unit_id'=>$rental_unit_id,
				// 			'year'=>$invoice_year,
				// 			'month'=>$invoice_month,
				// 			'payment_created_by'=>$this->session->userdata("personnel_id"),
				// 			'approved_by'=>$this->session->userdata("personnel_id"),'date_approved'=>date('Y-m-d')
				// 		);
						
				// 		$data['confirm_number'] = $receipt_number;
				// 		$data['document_number'] = $receipt_number;
				// 		$data['lease_id'] = $lease_id;
				// 		$data['tenant_id'] = $tenant_id;
				// 		$data['account_id'] = $lease_number;
						
				// 		if($this->db->insert('payments', $data))
				// 		{
				// 			$payment_id = $this->db->insert_id();
				// 			$remarks =  $invoice_type_name.' Balance B/F from unit '.$rental_unit_name.' as from '.$date;	
				// 			$service = array(
				// 					'payment_id'=>$payment_id,
				// 					'amount_paid'=> $amount_paid,
				// 					'payment_month'=> $invoice_month,
				// 					'rental_unit_id'=> $rental_unit_id,
				// 					'tenant_id'=> $tenant_id,
				// 					'lease_number'=> $lease_number,
				// 					'payment_year'=> $invoice_year,
				// 					'invoice_type_id' => $invoice_type_id,
				// 					'payment_item_status' => 1,
				// 					'lease_id' => $lease_id,
				// 					'mpesa_id' => '',
				// 					'personnel_id' => $this->session->userdata('personnel_id'),
				// 					'payment_item_created' => date('Y-m-d'),
				// 					'remarks'=>$remarks
				// 				);					

				// 			$this->db->insert('payment_item',$service);
				// 		}
				// 		// create a debit note on the previous account

				// 		$debit_receipt_number = $this->accounts_model->create_debit_note_number();

				// 		$data_debit = array(
				// 			'debit_note_amount'=>$amount_paid,
				// 			'personnel_id'=>$this->session->userdata("personnel_id"),
				// 			'debit_note_date'=>$this->input->post('transfer_date'),
				// 			'document_number'=>$debit_receipt_number,
				// 			'debit_note_created'=>date("Y-m-d"),
				// 			'year'=>$invoice_year,
				// 			'month'=>$invoice_month,
				// 			'debit_note_created_by'=>$this->session->userdata("personnel_id"),
				// 			'approved_by'=>$this->session->userdata("personnel_id"),
				// 			'rental_unit_id'=>$rental_unit_id_old,
				// 			'account_id'=>$lease_number,
				// 			'tenant_id'=>$tenant_id,
				// 			'date_approved'=>date('Y-m-d')
				// 		);
				// 		$data_debit['lease_id'] = $lease_id_old;

				// 		if($this->db->insert('debit_notes', $data_debit))
				// 		{
				// 			$debit_note_id = $this->db->insert_id();

				// 			$service_debit = array(
				// 								'debit_note_id'=>$debit_note_id,
				// 								'debit_note_amount'=> $amount_paid,
				// 								'invoice_type_id' => $invoice_type_id,
				// 								'debit_note_item_status' => 1,
				// 								'lease_id' => $lease_id_old,
				// 								'rental_unit_id' => $rental_unit_id_old,
				// 								'tenant_id'=>$tenant_id,
				// 								'lease_number'=>$lease_number,
				// 								'personnel_id' => $this->session->userdata('personnel_id'),
				// 								'debit_note_item_created' => date('Y-m-d')
				// 							);
				// 			$this->db->insert('debit_note_item',$service_debit);
				// 		}
				// 	}
			 //      }
			 //    }

			    // now update the lease details 

			    // completed the process 
		   		$update_lease = array(
									'lease_status'=>4,
									'vacated_on'=>$transfer_date,
									'notice_date'=>$transfer_date,
									'transfer_id'=>$lease_id,
									'notice_remarks'=>$this->input->post('transfer_remarks'),
								  );
			    $this->db->where('lease_id',$lease_id_old);
				if($this->db->update('leases',$update_lease))
				{
				    $update_lease_new = array(
												'lease_status'=>1,
											  );
				    $this->db->where('lease_id',$lease_id);
					if($this->db->update('leases',$update_lease_new))
					{

					}
					$this->session->set_userdata('success_message', 'lease has been created');

					redirect('lease-detail/'.$lease_id.'/'.$lease_number);
				}
				else
				{
					$this->session->set_userdata('success_message', 'Sorry something went wrong please check out this account');
					$redirect_url = $this->input->post('redirect_url');
					redirect($redirect_url);
				}
			}
			else
			{
				$this->session->set_userdata('error_message', 'Sorry the rent unit prices do not much. Please refer to the note at the bottom of transfering a rental unit');
				$redirect_url = $this->input->post('redirect_url');
				redirect($redirect_url);
			}
			// update opening balances

		}
		else
		{
			$this->session->set_userdata("error_message", validation_errors());
			$response['status'] = 'fail';
			$response['message'] = validation_errors();
			$redirect_url = $this->input->post('redirect_url');
			redirect($redirect_url);
		}
		



		// $array_update['lease_id'] = $lease_changed;
		// $this->db->where('lease_id',$lease_id);
		// $this->db->update('invoice',$array_update);

		// $this->db->where('lease_id',$lease_id);
		// $this->db->update('lease_invoice',$array_update);


		// $this->db->where('lease_id',$lease_id);
		// $this->db->update('payments',$array_update);

		// $this->db->where('lease_id',$lease_id);
		// $this->db->update('payment_item',$array_update);


		// $this->db->where('lease_id',$lease_id);
		// $this->db->update('credit_notes',$array_update);

		// $this->db->where('lease_id',$lease_id);
		// $this->db->update('credit_note_item',$array_update);

		// $this->db->where('lease_id',$lease_id);
		// $this->db->update('debit_notes',$array_update);

		// $this->db->where('lease_id',$lease_id);
		// $this->db->update('debit_note_item',$array_update);

		// $array_update2['lease_status'] = 3;
		// $this->db->where('lease_id',$lease_id);
		// $this->db->update('leases',$array_update2);

		// $array_update2['lease_status'] = 3;
		// $this->db->where('lease_id',$lease_id);
		// $this->db->update('rental_unit_id',$array_update2);

		// redirect('lease-detail/'.$lease_changed);
	}



}
?>
