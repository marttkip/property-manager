<div class="box">
    <div class="box-header with-border">
      <h3 class="box-title">Search</h3>

      <div class="box-tools pull-right">
         
      </div>
    </div>
    <div class="box-body">
	<?php
    echo form_open("search-tenants", array("class" => "form-horizontal"));
    ?>
    <div class="row">
        <div class="col-md-6">
            
            <div class="form-group">
                <label class="col-lg-4 control-label">Tenant Name: </label>
               							            
	            <div class="col-lg-8">
	            	<input type="text" class="form-control" name="tenant_name" placeholder="Tenant Name" value="">
	            </div>
            </div>
            
            <div class="form-group">
                <label class="col-lg-4 control-label">Phone Number: </label>
                
                <div class="col-lg-8">
                	<input type="text" class="form-control" name="tenant_phone_number" placeholder="Tenant Phone Number" value="">
                </div>
            </div>
            
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label class="col-lg-4 control-label">National Id: </label>
                
                <div class="col-lg-8">
                	<input type="text" class="form-control" name="tenant_national_id" placeholder="Tenant national Id" value="">
                </div>
            </div>
             <div class="form-group">
                <div class="col-lg-8 col-lg-offset-4">
                	<div class="center-align">
                   		<button type="submit" class="btn btn-info">Search</button>
    				</div>
                </div>
            </div>
        </div>
    </div>
    
    
    <?php
    echo form_close();
    ?>
  </div>
</div>