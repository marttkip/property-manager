<?php echo $this->load->view('search/search_properties','', true); ?>

<?php

		$result = '';

		//if users exist display them
		if ($query->num_rows() > 0)
		{
			$count = $page;

			$result .=
			'
			<table class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
						<th>#</th>
						<th><a >Code</a></th>
						<th><a>Name</a></th>
						<th><a >Landlord</a></th>
						<th><a >Type</a></th>
						<th><a >Units</a></th>
						<th><a >Location</a></th>
						<th><a >Status</a></th>
						<th colspan="4">Actions</th>
					</tr>
				</thead>
				  <tbody>

			';


			foreach ($query->result() as $row)
			{
				$property_id = $row->property_id;
				$property_name = $row->property_name;
				$property_location = $row->property_location;
				$property_owner_name = $row->property_owner_name;
				$property_type_name = $row->property_type_name;
				$total_units = $row->total_units;
				$property_code = $row->property_code;
				$created = $row->created;
				$property_status = $row->property_status;


				if($property_status == 1)
				 {
						$status ='Active';
						$color ='danger';
				 }
				 else
				 {
						$status ='Disabled';
						$color = 'success';
			     }

				//status
				// if($property_status == 1)
				// {
				// 	$status = 'Active';
				// }
				// else
				// {
				// 	$status = 'Disabled';
				// }

				//create deactivated status display
				if($property_status == 0)
				{
					$status = '<span class="label label-default"> Deactivated</span>';
					$button = '<a class="btn btn-info" href="'.site_url().'property-manager/properties/activate-property/'.$property_id.'" onclick="return confirm(\'Do you want to activate '.$property_name.'?\');" title="Activate '.$property_name.'"><i class="fa fa-thumbs-up"></i></a>';
				}
				//create activated status display
				else if($property_status == 1)
				{
					$status = '<span class="label label-success">Active</span>';
					$button = '<a class="btn btn-default" href="'.site_url().'property-manager/properties/deactivate-property/'.$property_id.'" onclick="return confirm(\'Do you want to deactivate '.$property_name.'?\');" title="Deactivate '.$property_name.'"><i class="fa fa-thumbs-down"></i></a>';
				}
				else if($property_status == 2)
				{
					$status = '<span class="label label-success">Active</span>';
					$button = '<a class="btn btn-info" href="'.site_url().'property-manager/properties/activate-property/'.$property_id.'" onclick="return confirm(\'Do you want to activate '.$property_name.'?\');" title="Activate '.$property_name.'"><i class="fa fa-thumbs-up"></i></a>';
				}
				else
				{
					$status = '<span class="label label-danger">Agreement Terminated</span>';
					$button = '';
				}


				$count++;
				$result .=
				'
					<tr>
						<td>'.$count.'</td>
						<td class="'.$color.'">'.$property_code.'</td>
						<td class="'.$color.'">'.$property_name.'</td>
						<td class="'.$color.'">'.$property_owner_name.'</td>
						<td class="'.$color.'">'.$property_type_name.'</td>
						<td class="'.$color.'">'.$total_units.'</td>
						<td class="'.$color.'">'.$property_location.'</td>
						<td>'.$status.'</td>
						<td class="'.$color.'"><a href="'.site_url().'property-rental-units/'.$property_id.'" class="btn btn-sm btn-success" title="Edit '.$property_name.'"><i class="fa fa-home"></i> Rental Units</a></td>
						<td><a href="'.site_url().'property-manager/properties/edit-property/'.$property_id.'" class="btn btn-sm btn-success" title="Edit '.$property_name.'"><i class="fa fa-pencil"></i></a></td>
						<td><a href="'.site_url().'admin/delete-property/'.$property_id.'" class="btn btn-sm btn-danger" onclick="return confirm(\'Do you really want to delete '.$property_name.'?\');" title="Delete '.$property_name.'"><i class="fa fa-trash"></i></a></td>
						<td>'.$button.'</td>

					</tr>
				';
			}

			$result .=
			'
						  </tbody>
						</table>
			';
		}

		else
		{
			$result .= "There are no properties added";
		}
?>
					<div class="box">
		                <div class="box-header with-border">
		                  <h3 class="box-title"><?php echo $title;?></h3>

		                  <div class="box-tools pull-right">
		                  	<a href="<?php echo site_url();?>property-manager/properties/export-properties" class="btn btn-sm btn-success " target="_blank"><i class="fa fa-plus"></i> Export</a>
		                     <a href="<?php echo site_url();?>property-manager/properties/add-property" class="btn btn-sm btn-info "><i class="fa fa-plus"></i> Add Property</a>
		                  </div>
		                </div>
		                <div class="box-body">
                            	<?php
                                $success = $this->session->userdata('success_message');

								if(!empty($success))
								{
									echo '<div class="alert alert-success"> <strong>Success!</strong> '.$success.' </div>';
									$this->session->unset_userdata('success_message');
								}

								$error = $this->session->userdata('error_message');

								if(!empty($error))
								{
									echo '<div class="alert alert-danger"> <strong>Oh snap!</strong> '.$error.' </div>';
									$this->session->unset_userdata('error_message');
								}
								$search =  $this->session->userdata('properties_search');
								if(!empty($search))
								{
									echo '<a href="'.site_url().'real_estate_administration/property/close_property_search" class="btn btn-sm btn-warning">Close Search</a>';
								}
								?>

								<div class="table-responsive">

									<?php echo $result;?>

                                </div>
                                <?php if(isset($links)){echo $links;}?>
							</div>
                            <div class="panel-footer">

                            </div>
						</div>
