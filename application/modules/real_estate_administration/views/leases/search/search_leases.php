<div class="box">
    <div class="box-header with-border">
      <h3 class="box-title">Search</h3>

      <div class="box-tools pull-right">
      </div>
    </div>
    <div class="box-body">
	<?php
    echo form_open("search-leases", array("class" => "form-horizontal"));
    ?>
    <div class="row">
     <div class="col-md-4">
            <input type="hidden" name="redirect_url" value="<?php echo $this->uri->uri_string()?>">
            <div class="form-group">
                <label class="col-lg-4 control-label">Contract No: </label>

	            <div class="col-lg-8">
	            	<input type="text" class="form-control" name="contract_no" placeholder="Contract No" >
	            </div>
            </div>
            <div class="form-group">
                <label class="col-lg-4 control-label">Tenant Name: </label>

                <div class="col-lg-8">
                    <input type="text" class="form-control" name="tenant_name" placeholder="Tenant Name" >
                </div>
            </div>

        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label class="col-lg-4 control-label">Unit Name: </label>

                <div class="col-lg-8">
                	<input type="text" class="form-control" name="rental_unit_name" placeholder="Unit Name i.e. ASHT001" >
                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-4 control-label">Tenant Number: </label>

                <div class="col-lg-8">
                    <input type="text" class="form-control" name="tenant_number" placeholder="TXXXX" >
                </div>
            </div>

        </div>



        <div class="col-md-4">

            <div class="form-group">
                <div class="col-lg-12">
                   <select  name='property_id' class='form-control select2'>
                      <option value=''>None - Please Select a property</option>
                      <?php echo $property_list;?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <!-- <div class="row"> -->
                    <div class="text-center">
                        <button type="submit" class="btn btn-info ">SEARCH LIST</button>
                    </div>
                <!-- </div> -->
            </div>


        </div>
    </div>



    <?php
    echo form_close();
    ?>
  </div>
</div>
