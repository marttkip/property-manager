<?php
	$result = '';
	if($properties->num_rows() > 0)
	{
		$count = 0;
			
		$result .= 
		'
		<br/>
		<table class="table table-bordered table-striped table-condensed">
			<thead>
				<tr>
					<th>#</th>
					<th>Level</th>
					<th>Property</th>
					<th>Owner</th>
					<th>Last editted</th>
					<th colspan="1">Actions</th>
				</tr>
			</thead>
			  <tbody>
			  
		';
		
		foreach ($properties->result() as $row)
		{
			$personnel_properties_id = $row->personnel_properties_id;
			$level_id = $row->level_id;
			$property_owner_name = $row->property_owner_name;
			$personnel_properties_status = $row->personnel_properties_status;
			$property_name = $row->property_name;
			$created = date('jS M Y H:i a',strtotime($row->created));
			$last_modified = date('jS M Y H:i a',strtotime($row->last_modified));

			if($level_id == 1)
			{
				$job = 'Property Manager';
			}
			else
			{
				$job = 'Property Manager';
			}
			
			//create deactivated status display
			if($personnel_properties_status == 0)
			{
				$status = '<span class="label label-danger">Deactivated</span>';
				$button = '<a class="btn btn-sm btn-info" href="'.site_url().'human-resource/activate-position/'.$personnel_properties_id.'/'.$personnel_id.'" onclick="return confirm(\'Do you want to activate '.$job.'?\');" title="Activate '.$job.'"><i class="fa fa-thumbs-up"></i></a>';
			}
			//create activated status display
			else if($personnel_properties_status == 1)
			{
				$status = '<span class="label label-success">Active</span>';
				$button = '<a class="btn btn-sm btn-default" href="'.site_url().'human-resource/deactivate-position/'.$personnel_properties_id.'/'.$personnel_id.'" onclick="return confirm(\'Do you want to deactivate '.$job.'?\');" title="Deactivate '.$job.'"><i class="fa fa-thumbs-down"></i></a>';
			}
			
			$count++;
			$result .= 
			'
				<tr>
					<td>'.$count.'</td>
					<td>'.$job.'</td>
					<td>'.$property_name.'</td>
					<td>'.$property_owner_name.'</td>
					<td>'.$last_modified.'</td>
					<td>'.$status.'</td>
				</tr> 
			';
		}
		
		$result .= 
		'
					  </tbody>
					</table>
		';
	}
	
	else
	{
		$result = "No properties have been assigned";
	}
	

//repopulate data if validation errors occur
$validation_error = validation_errors();
				
if(!empty($validation_error))
{
	$level_id = set_value('level_id');
	$property_id = set_value('property_id');
	$job_commencement_date = set_value('job_commencement_date');
}

else
{
	$level_id = '';
	$property_id = '';
	$job_commencement_date = '';
}
?>
<div class="box">
    <div class="box-header with-border">
      <h3 class="box-title">Job history</h3>

      <div class="box-tools pull-right">
      </div>
    </div>
    <div class="box-body">
            <?php
            if(isset($error)){
                echo '<div class="alert alert-danger"> Oh snap! Change a few things up and try submitting again. </div>';
            }
            if(!empty($validation_errors))
            {
                echo '<div class="alert alert-danger"> Oh snap! '.$validation_errors.' </div>';
            }
			
            ?>
            
            <?php echo form_open('human-resource/add-personnel-property/'.$personnel_id, array("class" => "form-horizontal", "role" => "form"));?>
				<div class="row">
					<div class="col-md-5">
				        
				        <div class="form-group">
				            <label class="col-md-3 control-label">Level: </label>
				            
				            <div class="col-md-9">
				            	<select class="form-control" name="level_id">
				                	<option value="0">--Select position--</option>
				                	<option value="1">Property Manager</option>
				                	
				                </select>
				            </div>
				        </div>
				   	</div>
					<div class="col-md-5">
				        
				        <div class="form-group">
				            <label class="col-md-3 control-label">Property: </label>
				            
				            <div class="col-md-9">
				            	<select class="form-control select2" name="property_id">
				                	<option value="">--Select property--</option>
				                	<?php
				                    	if($properties_query->num_rows() > 0)
										{	
											foreach($properties_query->result() as $res)
											{
												$property_id = $res->property_id;
												$property_name = $res->property_name;
												
												if($property_id == $property_id)
												{
													echo '<option value="'.$property_id.'" selected>'.$property_name.'</option>';
												}
												
												else
												{
													echo '<option value="'.$property_id.'">'.$property_name.'</option>';
												}
											}
										}
									?>
				                </select>
				            </div>
				        </div>
				   	</div>
				   	<div class="col-md-3">
				   		<div class="form-group">
				            <div class="col-md-12">
				            	<div class="form-actions center-align">
				                    <button class="btn btn-primary" type="submit">
				                        Allocate property
				                    </button>
				                </div>
				            </div>
				        </div>
				   	</div>
				</div>

            <?php echo form_close();?>
            <?php echo $result;?>
                </div>
            </div>