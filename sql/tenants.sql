CREATE OR REPLACE VIEW  v_lease_total_invoice as 
select sum(`invoice`.`invoice_amount`) AS `total_invoice`,`lease_invoice`.`lease_id` AS `lease_id` 
			from (`invoice` join `lease_invoice`) 
			where ((`lease_invoice`.`lease_invoice_id` = `invoice`.`lease_invoice_id`) 
				and (`lease_invoice`.`invoice_deleted` = 0) 
				and (`lease_invoice`.`invoice_date` <> '0000-00-00')) 
			group by `lease_invoice`.`lease_id`;

CREATE OR REPLACE VIEW v_lease_total_payments as 
select sum(`payments`.`amount_paid`) AS `total_amount_paid`,`payments`.`lease_id` AS `lease_id` 
	from `payments` 
	where ((`payments`.`payment_status` = 1) 
		and (`payments`.`cancel` = 0)) 
	group by `payments`.`lease_id`;

CREATE OR REPLACE VIEW v_lease_total_pardon as 
select sum(`credit_note_item`.`credit_note_amount`) AS `total_pardon`,`credit_notes`.`lease_id` AS `lease_id`
	from (`credit_note_item` join `credit_notes`) 
	where ((`credit_notes`.`credit_note_id` = `credit_note_item`.`credit_note_id`)  
		and (`credit_notes`.`credit_note_date` <> '0000-00-00')) 
	group by `credit_notes`.`lease_id`;

CREATE OR REPLACE VIEW v_lease_total_debits as 
select sum(`debit_note_item`.`debit_note_amount`) AS `total_debit`,`debit_notes`.`lease_id` AS `lease_id`
	from (`debit_note_item` join `debit_notes`) 
	where ((`debit_notes`.`debit_note_id` = `debit_note_item`.`debit_note_id`)  
		and (`debit_notes`.`debit_note_date` <> '0000-00-00')) 
	group by `debit_notes`.`lease_id`;

CREATE OR REPLACE VIEW  v_lease_account AS 
			select 
			ifnull(`v_lease_total_invoice`.`total_invoice`,0) AS `total_invoice_amount`,
			ifnull(`v_lease_total_payments`.`total_amount_paid`,0) AS `total_paid_amount`,
			ifnull(`v_lease_total_pardon`.`total_pardon`,0) AS `total_waived_amount`,
			ifnull(`v_lease_total_debits`.`total_debit`,0) AS `total_debited_amount`,
			`leases`.`lease_number` AS `lease_number` ,
			`leases`.`lease_id` AS `lease_id` 
			from (((`leases` left join `v_lease_total_payments` on((`v_lease_total_payments`.`lease_id` = `leases`.`lease_id`))) 
				left join `v_lease_total_invoice` on((`v_lease_total_invoice`.`lease_id` = `leases`.`lease_id`))) 
				left join `v_lease_total_pardon` on((`v_lease_total_pardon`.`lease_id` = `leases`.`lease_id`))
				left join `v_lease_total_debits` on((`v_lease_total_debits`.`lease_id` = `leases`.`lease_id`))) 
			where (`leases`.`lease_deleted` = 0);

CREATE OR REPLACE VIEW v_lease_balances as 
select 
	`v_lease_account`.`total_invoice_amount` AS `total_invoice_amount`,
	`v_lease_account`.`total_paid_amount` AS `total_paid_amount`,
	`v_lease_account`.`total_waived_amount` AS `total_waived_amount`,
	`v_lease_account`.`total_debited_amount` AS `total_debited_amount`,
	((`v_lease_account`.`total_invoice_amount` - `v_lease_account`.`total_debited_amount`) - (`v_lease_account`.`total_paid_amount` + `v_lease_account`.`total_waived_amount`)) AS `balance`,
	`v_lease_account`.`lease_number` AS `lease_number`, 
	`v_lease_account`.`lease_id` AS `lease_id` 
	from `v_lease_account`;


CREATE OR REPLACE VIEW v_mpesa_transactions AS
SELECT mpesa_transactions.*,COALESCE(SUM(payment_item.amount_paid),0) AS recon_amount from mpesa_transactions
LEFT JOIN payment_item ON payment_item.mpesa_id = mpesa_transactions.mpesa_id AND payment_item.payment_id > 0
WHERE mpesa_transactions.mpesa_id > 0
GROUP BY mpesa_transactions.mpesa_id
