CREATE OR REPLACE VIEW v_transactions AS
SELECT
`invoice`.`invoice_id` AS `transactionId`,
-- invoice itemid
`invoice`.`lease_invoice_id` AS `referenceId`,
-- invoice id
`lease_invoice`.`document_number` AS `referenceCode`,
-- invoice number
'' AS `transactionCode`,
-- lase invoice id
`invoice`.`lease_id` AS `lease_id`,
`lease_invoice`.`account_id` AS `lease_number`,
-- lease_id
-- invoice type
'Payable' AS `accountsclassfication`,
-- income
`invoice`.`invoice_type` AS `accountId`,
-- invoice type
`invoice_type`.`invoice_type_name` AS `accountName`,
-- invoice type name
`invoice`.`remarks` AS `transactionDescription`,
-- remarks
`invoice`.`invoice_amount` AS `dr_amount`,
-- if invoice 0
'0' AS `cr_amount`,
CONCAT(`invoice`.`year`,'-',invoice.month,'-','01') AS `transactionDate`,
-- created date
`invoice`.`created` AS `createdAt`,
-- created at
`invoice`.`invoice_item_deleted` AS `status`,
`invoice`.`year` AS `transaction_year`,
`invoice`.`month` AS `transaction_month`,

'Outgoing Invoice' AS `transactionCategory`,
-- income or expense
'invoice' AS `transactionTable`,
'lease_invoice' AS `referenceTable`
FROM
`invoice`,`lease_invoice`,leases,invoice_type

WHERE
lease_invoice.invoice_deleted = 0
AND `invoice`.`lease_invoice_id` = `lease_invoice`.`lease_invoice_id`
AND `invoice`.`invoice_type` = `invoice_type`.`invoice_type_id`
AND lease_invoice.lease_id = leases.lease_id
AND (lease_invoice.invoice_date <= leases.notice_date OR leases.notice_date IS NULL)

-- invoice end
UNION ALL
-- payments viewport
SELECT
`payments`.`payment_id` AS `transactionId`,
-- invoice itemid
`payment_item`.`payment_id` AS `referenceId`,
-- invoice id
`payments`.`document_number` AS `referenceCode`,
-- invoice number
'' AS `transactionCode`,
-- lase invoice id
`payments`.`lease_id` AS `lease_id`,
`payments`.`account_id` AS `lease_number`,
-- lease_id
-- invoice type
'Invoice Payments' AS `accountsclassfication`,
-- income
`payment_item`.`invoice_type_id` AS `accountId`,
-- invoice type
`invoice_type`.`invoice_type_name` AS `accountName`,
-- invoice type name
CONCAT('Payment for ',invoice_type.invoice_type_name) AS `transactionDescription`,
-- remarks
'0' AS `dr_amount`,
-- if invoice 0
`payment_item`.`amount_paid` AS `cr_amount`,
CONCAT(`payment_item`.`payment_year`,'-',payment_item.payment_month,'-','01') AS `transactionDate`,
-- created date
`payment_item`.`payment_item_created` AS `createdAt`,
-- created at
`payment_item`.`payment_item_status` AS `status`,
`payment_item`.`payment_year` AS `transaction_year`,
`payment_item`.`payment_month` AS `transaction_month`,

'Tenant Payments' AS `transactionCategory`,
-- income or expense
'payment_item' AS `transactionTable`,
'payments' AS `referenceTable`
FROM
`payment_item` JOIN `payments` ON  `payment_item`.`payment_id` = `payments`.`payment_id`
JOIN `invoice_type` ON `payment_item`.`invoice_type_id` = `invoice_type`.`invoice_type_id`
WHERE payments.cancel = 0

UNION ALL

SELECT
`credit_notes`.`credit_note_id` AS `transactionId`,
-- invoice itemid
`credit_note_item`.`credit_note_id` AS `referenceId`,
-- invoice id
`credit_notes`.`document_number` AS `referenceCode`,
-- invoice number
'' AS `transactionCode`,
-- lase invoice id
`credit_notes`.`lease_id` AS `lease_id`,
`credit_notes`.`account_id` AS `lease_number`,
-- lease_id
-- invoice type
'Credit Note' AS `accountsclassfication`,
-- income
`credit_note_item`.`invoice_type_id` AS `accountId`,
-- invoice type
`invoice_type`.`invoice_type_name` AS `accountName`,
-- invoice type name
CONCAT('Credit for ',invoice_type.invoice_type_name)  AS `transactionDescription`,
-- remarks
'0' AS `dr_amount`,
-- if invoice 0
`credit_note_item`.`credit_note_amount` AS `cr_amount`,
CONCAT(`credit_note_item`.`credit_note_year`,'-',credit_note_item.credit_note_month,'-','01')  AS `transactionDate`,
-- created date
`credit_note_item`.`credit_note_item_created` AS `createdAt`,
-- created at
`credit_note_item`.`credit_note_item_status` AS `status`,
`credit_note_item`.`credit_note_year` AS `transaction_year`,
`credit_note_item`.`credit_note_month` AS `transaction_month`,


'Credit Note' AS `transactionCategory`,
-- income or expense
'credit_note_item' AS `transactionTable`,
'credit_notes' AS `referenceTable`
FROM
`credit_note_item` JOIN `credit_notes` ON  `credit_note_item`.`credit_note_id` = `credit_notes`.`credit_note_id`
JOIN `invoice_type` ON `credit_note_item`.`invoice_type_id` = `invoice_type`.`invoice_type_id`
WHERE credit_notes.cancel = 0


UNION ALL

SELECT
`debit_notes`.`debit_note_id` AS `transactionId`,
-- invoice itemid
`debit_note_item`.`debit_note_id` AS `referenceId`,
-- invoice id
`debit_notes`.`document_number` AS `referenceCode`,
-- invoice number
'' AS `transactionCode`,
-- lase invoice id
`debit_notes`.`lease_id` AS `lease_id`,
`debit_notes`.`account_id` AS `lease_number`,
-- lease_id
-- invoice type
'Debit Note' AS `accountsclassfication`,
-- income
`debit_note_item`.`invoice_type_id` AS `accountId`,
-- invoice type
`invoice_type`.`invoice_type_name` AS `accountName`,
-- invoice type name
'Debit' AS `transactionDescription`,
-- remarks
`debit_note_item`.`debit_note_amount` AS `dr_amount`,
-- if invoice 0
'0' AS `cr_amount`,
CONCAT(`debit_note_item`.`debit_note_year`,'-',debit_note_item.debit_note_month,'-','01')  AS `transactionDate`,
-- created date
`debit_note_item`.`debit_note_item_created` AS `createdAt`,
-- created at
`debit_note_item`.`debit_note_item_status` AS `status`,
`debit_note_item`.`debit_note_year` AS `transaction_year`,
`debit_note_item`.`debit_note_month` AS `transaction_month`,
'Debit Note' AS `transactionCategory`,
-- income or expense
'debit_note_item' AS `transactionTable`,
'debit_notes' AS `referenceTable`
FROM
`debit_note_item` JOIN `debit_notes` ON  `debit_note_item`.`debit_note_id` = `debit_notes`.`debit_note_id`
JOIN `invoice_type` ON `debit_note_item`.`invoice_type_id` = `invoice_type`.`invoice_type_id`
WHERE debit_notes.cancel = 0
;
-- end of payments

CREATE OR REPLACE VIEW v_transactions_by_date AS SELECT * FROM v_transactions   ORDER BY transactionDate;


CREATE OR REPLACE VIEW  v_tenant_leases AS  select `tenants`.`tenant_name` AS `tenant_name`,`tenants`.`tenant_phone_number` AS `tenant_phone_number`,
`tenants`.`tenant_id` AS `tenant_id`,
`rental_unit`.`property_id` AS `property_id`,
`leases`.`rental_unit_id` AS `rental_unit_id`,
`leases`.`lease_id` AS `lease_id`,
`property_billing`.`property_billing_deleted`,
`property_billing`.`property_billing_id`,
`property_billing`.`billing_amount` AS `billing_amount`,
`tenants`.`tenant_number` AS `tenant_number`,
`leases`.`lease_number` AS `lease_number`,
`leases`.`notice_date` AS `notice_date`,
`leases`.`vacated_on` AS `vacated_on`

from (((`leases`,property_billing)
left join `tenants` on((`tenants`.`tenant_id` = `leases`.`tenant_id`)))
left join `tenant_unit` on((`tenant_unit`.`tenant_unit_id` = `leases`.`tenant_unit_id`))
left join `rental_unit` on((`leases`.`rental_unit_id` = `rental_unit`.`rental_unit_id`)))
WHERE `property_billing`.`lease_id` = `leases`.`lease_id` AND property_billing.invoice_type_id = 1 
AND property_billing.property_billing_deleted = 0
group by `leases`.`lease_id`;


CREATE OR REPLACE VIEW v_active_leases AS SELECT `tenants`.tenant_code,tenants.tenant_id,tenants.tenant_name,tenants.tenant_national_id,tenants.tenant_phone_number,tenants.created AS tenant_created_on, `rental_unit`.`rental_unit_id` AS unit_id,
`rental_unit`.rental_unit_name, rental_unit.created as rental_unit_created_on,rental_unit_code,
`property`.property_id,property.property_name,property.created as property_created_on,property_code,
leases.lease_number,`leases`.lease_id,leases.notice_date,leases.created AS lease_created_on,leases.lease_status,lease_start_date,lease_end_date
FROM
(`tenants`, `tenant_unit`, `rental_unit`, `leases`, `property`)
WHERE
tenants.tenant_id = tenant_unit.tenant_id
AND leases.rental_unit_id = rental_unit.rental_unit_id
AND tenant_unit.tenant_unit_id = leases.tenant_unit_id
AND leases.lease_status < 4
AND property.property_id = rental_unit.property_id
GROUP BY leases.lease_id
ORDER BY `rental_unit`.`rental_unit_id`, `rental_unit`.`rental_unit_name`, `property`.`property_name` ASC;




CREATE OR REPLACE VIEW v_tenants_tickets AS select `tenants`.`tenant_code` AS `tenant_code`,`tenants`.`tenant_id` AS `tenant_id`,`tenants`.`tenant_name` AS `tenant_name`,
`tenants`.`tenant_national_id` AS `tenant_national_id`,`tenants`.`tenant_phone_number` AS `tenant_phone_number`,
`tenants`.`created` AS `tenant_created_on`,`rental_unit`.`rental_unit_id` AS `unit_id`,`rental_unit`.`rental_unit_name` AS `rental_unit_name`,`rental_unit`.`created`
AS `rental_unit_created_on`,`rental_unit`.`rental_unit_code` AS `rental_unit_code`,`property`.`property_id` AS `property_id`,`property`.`property_name` AS `property_name`,
`property`.`created` AS `property_created_on`,`property`.`property_code` AS `property_code`,`leases`.`lease_number` AS `lease_number`,`leases`.`lease_id` AS `lease_id`,`leases`.`notice_date`
 AS `notice_date`,`leases`.`created` AS `lease_created_on`,`leases`.`lease_status` AS `lease_status`,`leases`.`lease_start_date` AS `lease_start_date`,`leases`.`lease_end_date` AS `lease_end_date`,
ticket.created as ticket_created,ticket.ticket_description,ticket_status,ticket.ticket_number,ticket.ticket_status_id,ticket.ticket_id
from (((((`ticket` join `tenants`) join `tenant_unit`) join `rental_unit`) join `leases`) join `property`)
where ((`tenants`.`tenant_id` = `tenant_unit`.`tenant_id`)
 and (`tenant_unit`.`rental_unit_id` = `rental_unit`.`rental_unit_id`)
and (`tenant_unit`.`tenant_unit_id` = `leases`.`tenant_unit_id`)
and (`ticket`.`lease_id` = `leases`.`lease_id`)
and (`property`.`property_id` = `rental_unit`.`property_id`))
order by ticket.ticket_number ASC;
