CREATE OR REPLACE VIEW v_payable_ledger AS
SELECT
	`creditor_invoice_item`.`creditor_invoice_item_id` AS `transactionId`,
	`creditor_invoice`.`creditor_invoice_id` AS `referenceId`,
	`creditor_invoice`.`invoice_number` AS `referenceCode`,
	'' AS `transactionCode`,
	`creditor_invoice`.`property_id` AS `projectId`,
    `creditor_invoice`.`creditor_id` AS `recepientId`,
	`account`.`parent_account` AS `accountParentId`,
	`account_type`.`account_type_name` AS `accountsclassfication`,
	`creditor_invoice_item`.`account_to_id` AS `accountId`,
	`account`.`account_name` AS `accountName`,
	`creditor_invoice_item`.`item_description` AS `transactionName`,
	`creditor_invoice_item`.`item_description` AS `transactionDescription`,
	`creditor_invoice_item`.`total_amount` AS `dr_amount`,
	'0' AS `cr_amount`,
	`creditor_invoice`.`transaction_date` AS `transactionDate`,
	`creditor_invoice`.`created` AS `createdAt`,
	`creditor_invoice_item`.`creditor_invoice_item_status` AS `status`,
	'Expense' AS `transactionCategory`,
	'Creditors Invoices' AS `transactionClassification`,
	'creditor_invoice_item' AS `transactionTable`,
	'creditor_invoice' AS `referenceTable`
FROM
	(
		(
			(
				`creditor_invoice_item`
				JOIN `creditor_invoice` ON(
					(
						creditor_invoice.creditor_invoice_id = creditor_invoice_item.creditor_invoice_id
					)
				)
			)
			JOIN account ON(
				(
					account.account_id = creditor_invoice_item.account_to_id
				)
			)
		)
		JOIN `account_type` ON(
			(
				account_type.account_type_id = account.account_type_id
			)
		)
	)

  UNION ALL
-- credit invoice payments

  SELECT
	`creditor_payment_item`.`creditor_payment_item_id` AS `transactionId`,
	`creditor_payment`.`creditor_payment_id` AS `referenceId`,
	`creditor_payment`.`reference_number` AS `referenceCode`,
	`creditor_payment_item`.`creditor_invoice_id` AS `transactionCode`,
	'' AS `projectId`,
  `creditor_payment_item`.`creditor_id` AS `recepientId`,
	`account`.`parent_account` AS `accountParentId`,
	`account_type`.`account_type_name` AS `accountsclassfication`,
	`creditor_payment`.`account_from_id` AS `accountId`,
	`account`.`account_name` AS `accountName`,
	`creditor_payment_item`.`description` AS `transactionName`,
	`creditor_payment_item`.`description` AS `transactionDescription`,
	0 AS `dr_amount`,
	`creditor_payment_item`.`amount_paid` AS `cr_amount`,
	`creditor_payment`.`transaction_date` AS `transactionDate`,
	`creditor_payment`.`created` AS `createdAt`,
	`creditor_payment_item`.`creditor_payment_item_status` AS `status`,
	'Expense Payment' AS `transactionCategory`,
	'Creditors Invoices Payments' AS `transactionClassification`,
	'creditor_payment' AS `transactionTable`,
	'creditor_payment_item' AS `referenceTable`
FROM
	(
		(
			(
				`creditor_payment_item`
				JOIN `creditor_payment` ON(
					(
						creditor_payment.creditor_payment_id = creditor_payment_item.creditor_payment_id
					)
				)
			)
			JOIN account ON(
				(
					account.account_id = creditor_payment.account_from_id
				)
			)
		)
		JOIN `account_type` ON(
			(
				account_type.account_type_id = account.account_type_id
			)
		)
	)

UNION ALL

SELECT
	`creditor_credit_note_item`.`creditor_credit_note_item_id` AS `transactionId`,
	`creditor_credit_note`.`creditor_credit_note_id` AS `referenceId`,
	`creditor_credit_note`.`invoice_number` AS `referenceCode`,
	`creditor_credit_note_item`.`creditor_invoice_id` AS `transactionCode`,
	'' AS `projectId`,
  '' AS `recepientId`,
	`account`.`parent_account` AS `accountParentId`,
	`account_type`.`account_type_name` AS `accountsclassfication`,
	`creditor_credit_note`.`account_from_id` AS `accountId`,
	`account`.`account_name` AS `accountName`,
	`creditor_credit_note_item`.`description` AS `transactionName`,
	`creditor_credit_note_item`.`description` AS `transactionDescription`,
	0 AS `dr_amount`,
	`creditor_credit_note_item`.`credit_note_amount` AS `cr_amount`,
	`creditor_credit_note`.`transaction_date` AS `transactionDate`,
	`creditor_credit_note`.`created` AS `createdAt`,
	`creditor_credit_note_item`.`creditor_credit_note_item_status` AS `status`,
	'Expense Payment' AS `transactionCategory`,
	'Creditors Credit Notes' AS `transactionClassification`,
	'creditor_credit_note' AS `transactionTable`,
	'creditor_credit_note_item' AS `referenceTable`
FROM
	(
		(
			(
				`creditor_credit_note_item`
				JOIN `creditor_credit_note` ON(
					(
						creditor_credit_note.creditor_credit_note_id = creditor_credit_note_item.creditor_credit_note_id
					)
				)
			)
			JOIN account ON(
				(
					account.account_id = creditor_credit_note.account_from_id
				)
			)
		)
		JOIN `account_type` ON(
			(
				account_type.account_type_id = account.account_type_id
			)
		)
	)

UNION ALL

SELECT
	`finance_purchase`.`finance_purchase_id` AS `transactionId`,
	'' AS `referenceId`,
	'' AS `referenceCode`,
	`finance_purchase`.`transaction_number` AS `transactionCode`,
	`finance_purchase`.`property_id` AS `projectId`,
	`finance_purchase`.`creditor_id` AS `recepientId`,
	`account`.`parent_account` AS `accountParentId`,
	`account_type`.`account_type_name` AS `accountsclassfication`,
	`finance_purchase`.`account_to_id` AS `accountId`,
	`account`.`account_name` AS `accountName`,
	`finance_purchase`.`finance_purchase_description` AS `transactionName`,
	`finance_purchase`.`finance_purchase_description` AS `transactionDescription`,
	`finance_purchase`.`finance_purchase_amount` AS `dr_amount`,
	0 AS `cr_amount`,
	`finance_purchase`.`transaction_date` AS `transactionDate`,
	`finance_purchase`.`created` AS `createdAt`,
	`finance_purchase`.`finance_purchase_status` AS `status`,
	'Expense' AS `transactionCategory`,
	'Purchases' AS `transactionClassification`,
	'finance_purchase' AS `transactionTable`,
	'' AS `referenceTable`
FROM
	(
		(
			(
				`finance_purchase`
				JOIN account ON(
					(
						account.account_id = finance_purchase.account_to_id
					)
				)
			)

		)
		JOIN `account_type` ON(
			(
				account_type.account_type_id = account.account_type_id
			)
		)
	)

UNION ALL

SELECT
	`finance_purchase_payment`.`finance_purchase_payment_id` AS `transactionId`,
	`finance_purchase`.`finance_purchase_id` AS `referenceId`,
	`finance_purchase`.`document_number` AS `referenceCode`,
	`finance_purchase`.`transaction_number` AS `transactionCode`,
	finance_purchase.property_id AS `projectId`,
  finance_purchase.creditor_id AS `recepientId`,
	`account`.`parent_account` AS `accountParentId`,
	`account_type`.`account_type_name` AS `accountsclassfication`,
	`finance_purchase_payment`.`account_from_id` AS `accountId`,
	`account`.`account_name` AS `accountName`,
	'' AS `transactionName`,
	CONCAT(`account`.`account_name`, ' paying for invoice ',`finance_purchase`.`transaction_number`) AS `transactionDescription`,
	0 AS `dr_amount`,
	`finance_purchase_payment`.`amount_paid` AS `cr_amount`,
	`finance_purchase`.`transaction_date` AS `transactionDate`,
	`finance_purchase`.`created` AS `createdAt`,
	`finance_purchase_payment`.`finance_purchase_payment_status` AS `status`,
	'Expense Payment' AS `transactionCategory`,
	'Purchase Payment' AS `transactionClassification`,
	'finance_purchase' AS `transactionTable`,
	'finance_purchase_payment' AS `referenceTable`
FROM
	(
		(
			(
				`finance_purchase_payment`
				JOIN `finance_purchase` ON(
					(
						finance_purchase.finance_purchase_id = finance_purchase_payment.finance_purchase_id
					)
				)
			)
			JOIN account ON(
				(
					account.account_id = finance_purchase_payment.account_from_id
				)
			)
		)
		JOIN `account_type` ON(
			(
				account_type.account_type_id = account.account_type_id
			)
		)
	);

  CREATE OR REPLACE VIEW v_payable_ledger_by_date AS select * from v_payable_ledger ORDER BY transactionDate;

  CREATE OR REPLACE VIEW v_aged_payables AS
	SELECT
		creditor.creditor_id,
		creditor.creditor_name as payables,
	  (
	    CASE
	      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_payable_ledger.transactionDate ) )  = 0, v_payable_ledger.dr_amount, 0 )) = 0 -- condtion to see if the date has no bills
	       THEN 0 -- Output
	      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_payable_ledger.transactionDate ) )  = 0, v_payable_ledger.dr_amount, 0 )) > 0 -- Checking if the value of bills in the date range is greater than Zero
	       THEN
	       -- Add the paymensts made to the specifc invoice
	        Sum(IF( DATEDIFF( CURDATE( ), date( v_payable_ledger.transactionDate ) )  = 0, v_payable_ledger.dr_amount, 0 ))
	        - Sum(IF( DATEDIFF( CURDATE( ), date( v_payable_ledger.transactionDate ) )  = 0, v_payable_ledger.cr_amount, 0 ))
	        END
	  ) AS `coming_due`,
		(
	    CASE
	      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_payable_ledger.transactionDate ) ) BETWEEN 1 AND 30, v_payable_ledger.dr_amount, 0 )) = 0 -- condtion to see if the date has no bills
	       THEN 0 -- Output
	      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_payable_ledger.transactionDate ) ) BETWEEN 1 AND 30, v_payable_ledger.dr_amount, 0 )) > 0 -- Checking if the value of bills in the date range is greater than Zero
	       THEN
	       -- Add the paymensts made to the specifc invoice
	        Sum(IF( DATEDIFF( CURDATE( ), date( v_payable_ledger.transactionDate ) ) BETWEEN 1 AND 30, v_payable_ledger.dr_amount, 0 ))
					  - (SELECT COALESCE ( sum( v_payable_ledger.cr_amount ), 0 ) FROM v_payable_ledger WHERE ( v_payable_ledger.recepientId = creditor.creditor_id ))
				END
	  ) AS `thirty_days`,
	  (
	    CASE
	      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_payable_ledger.transactionDate ) ) BETWEEN 31 AND 60, v_payable_ledger.dr_amount, 0 )) = 0 -- condtion to see if the date has no bills
	       THEN 0 -- Output
	      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_payable_ledger.transactionDate ) ) BETWEEN 31 AND 60, v_payable_ledger.dr_amount, 0 )) > 0 -- Checking if the value of bills in the date range is greater than Zero
	       THEN
	       -- Add the paymensts made to the specifc invoice
	        Sum(IF( DATEDIFF( CURDATE( ), date( v_payable_ledger.transactionDate ) ) BETWEEN 31 AND 60, v_payable_ledger.dr_amount, 0 ))
					   - (SELECT COALESCE ( sum( v_payable_ledger.cr_amount ), 0 ) FROM v_payable_ledger WHERE ( v_payable_ledger.recepientId = creditor.creditor_id ))
				END
	  ) AS `sixty_days`,
	  (
	    CASE
	      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_payable_ledger.transactionDate ) ) BETWEEN 61 AND 90, v_payable_ledger.dr_amount, 0 )) = 0 -- condtion to see if the date has no bills
	       THEN 0 -- Output
	      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_payable_ledger.transactionDate ) ) BETWEEN 61 AND 90, v_payable_ledger.dr_amount, 0 )) > 0 -- Checking if the value of bills in the date range is greater than Zero
	       THEN
	       -- Add the paymensts made to the specifc invoice
	        Sum(IF( DATEDIFF( CURDATE( ), date( v_payable_ledger.transactionDate ) ) BETWEEN 61 AND 90, v_payable_ledger.dr_amount, 0 ))
					   - (SELECT COALESCE ( sum( v_payable_ledger.cr_amount ), 0 ) FROM v_payable_ledger WHERE ( v_payable_ledger.recepientId = creditor.creditor_id ))
			END
	  ) AS `ninety_days`,
	  (
	    CASE
	      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_payable_ledger.transactionDate ) ) >90, v_payable_ledger.dr_amount, 0 )) = 0 -- condtion to see if the date has no bills
	       THEN 0 -- Output
	      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_payable_ledger.transactionDate ) ) >90, v_payable_ledger.dr_amount, 0 )) > 0 -- Checking if the value of bills in the date range is greater than Zero
	       THEN
	       -- Add the paymensts made to the specifc invoice
	        Sum(IF( DATEDIFF( CURDATE( ), date( v_payable_ledger.transactionDate ) ) >90, v_payable_ledger.dr_amount, 0 ))
					 - (SELECT COALESCE ( sum( v_payable_ledger.cr_amount ), 0 ) FROM v_payable_ledger WHERE ( v_payable_ledger.recepientId = creditor.creditor_id ))
				END
	  ) AS `over_ninety_days`,

	  (

	    (
	      CASE
	        WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_payable_ledger.transactionDate ) )  = 0, v_payable_ledger.dr_amount, 0 )) = 0 -- condtion to see if the date has no bills
	         THEN 0 -- Output
	        WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_payable_ledger.transactionDate ) )  = 0, v_payable_ledger.dr_amount, 0 )) > 0 -- Checking if the value of bills in the date range is greater than Zero
	         THEN
	         -- Add the paymensts made to the specifc invoice
	          Sum(IF( DATEDIFF( CURDATE( ), date( v_payable_ledger.transactionDate ) )  = 0, v_payable_ledger.dr_amount, 0 ))
						 - (SELECT COALESCE ( sum( v_payable_ledger.cr_amount ), 0 ) FROM v_payable_ledger WHERE ( v_payable_ledger.recepientId = creditor.creditor_id ))
	  		END
	    )-- Getting the Value for 0 Days
	    + (
	      CASE
	        WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_payable_ledger.transactionDate ) ) BETWEEN 1 AND 30, v_payable_ledger.dr_amount, 0 )) = 0 -- condtion to see if the date has no bills
	         THEN 0 -- Output
	        WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_payable_ledger.transactionDate ) ) BETWEEN 1 AND 30, v_payable_ledger.dr_amount, 0 )) > 0 -- Checking if the value of bills in the date range is greater than Zero
	         THEN
	         -- Add the paymensts made to the specifc invoice
	          Sum(IF( DATEDIFF( CURDATE( ), date( v_payable_ledger.transactionDate ) ) BETWEEN 1 AND 30, v_payable_ledger.dr_amount, 0 ))
						- (SELECT COALESCE ( sum( v_payable_ledger.cr_amount ), 0 ) FROM v_payable_ledger WHERE ( v_payable_ledger.recepientId = creditor.creditor_id ))
	  		END
	    ) --  AS `1-30 Days`
	    +(
	      CASE
	        WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_payable_ledger.transactionDate ) ) BETWEEN 31 AND 60, v_payable_ledger.dr_amount, 0 )) = 0 -- condtion to see if the date has no bills
	         THEN 0 -- Output
	        WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_payable_ledger.transactionDate ) ) BETWEEN 31 AND 60, v_payable_ledger.dr_amount, 0 )) > 0 -- Checking if the value of bills in the date range is greater than Zero
	         THEN
	         -- Add the paymensts made to the specifc invoice
	          Sum(IF( DATEDIFF( CURDATE( ), date( v_payable_ledger.transactionDate ) ) BETWEEN 31 AND 60, v_payable_ledger.dr_amount, 0 ))
					   - (SELECT COALESCE ( sum( v_payable_ledger.cr_amount ), 0 ) FROM v_payable_ledger WHERE ( v_payable_ledger.recepientId = creditor.creditor_id ))
					END
	    ) -- AS `31-60 Days`
	    +(
	      CASE
	        WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_payable_ledger.transactionDate ) ) BETWEEN 61 AND 90, v_payable_ledger.dr_amount, 0 )) = 0 -- condtion to see if the date has no bills
	         THEN 0 -- Output
	        WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_payable_ledger.transactionDate ) ) BETWEEN 61 AND 90, v_payable_ledger.dr_amount, 0 )) > 0 -- Checking if the value of bills in the date range is greater than Zero
	         THEN
	         -- Add the paymensts made to the specifc invoice
	          Sum(IF( DATEDIFF( CURDATE( ), date( v_payable_ledger.transactionDate ) ) BETWEEN 61 AND 90, v_payable_ledger.dr_amount, 0 ))
					   - (SELECT COALESCE ( sum( v_payable_ledger.cr_amount ), 0 ) FROM v_payable_ledger WHERE ( v_payable_ledger.recepientId = creditor.creditor_id ))
	  		END
	    ) -- AS `61-90 Days`
	    +(
	      CASE
	        WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_payable_ledger.transactionDate ) ) >90, v_payable_ledger.dr_amount, 0 )) = 0 -- condtion to see if the date has no bills
	         THEN 0 -- Output
	        WHEN Sum(IF( DATEDIFF( CURDATE( ), date( v_payable_ledger.transactionDate ) ) >90, v_payable_ledger.dr_amount, 0 )) > 0 -- Checking if the value of bills in the date range is greater than Zero
	         THEN
	         -- Add the paymensts made to the specifc invoice
	          Sum(IF( DATEDIFF( CURDATE( ), date( v_payable_ledger.transactionDate ) ) >90, v_payable_ledger.dr_amount, 0 ))
					  - (SELECT COALESCE ( sum( v_payable_ledger.cr_amount ), 0 ) FROM v_payable_ledger WHERE ( v_payable_ledger.recepientId = creditor.creditor_id ))
	  		END
	    ) -- AS `>90 Days`
	  ) AS `Total`

		FROM
			creditor
		LEFT JOIN v_payable_ledger ON v_payable_ledger.recepientId = creditor.creditor_id
		 GROUP BY creditor.creditor_id;

	CREATE OR REPLACE VIEW v_creditors_invoice_balances AS
SELECT
`creditor_invoice`.`creditor_id`,
`creditor_invoice`.`creditor_invoice_id`,
`creditor_invoice`.`invoice_number`,
'Creditor Bills' AS creditor_invoice_type,
`creditor_invoice`.`total_amount` ,
COALESCE (SUM(`creditor_credit_note_item`.`credit_note_amount`),0) AS total_credit_note,
COALESCE (SUM(`creditor_payment_item`.`amount_paid`),0) AS total_payments,
(`creditor_invoice`.`total_amount` - COALESCE (SUM(`creditor_credit_note_item`.`credit_note_amount`),0) - COALESCE (SUM(`creditor_payment_item`.`amount_paid`),0)) AS balance
FROM (`creditor_invoice`)
LEFT JOIN `creditor_credit_note_item` ON `creditor_credit_note_item`.`creditor_invoice_id` = `creditor_invoice`.`creditor_invoice_id`
LEFT JOIN `creditor_payment_item` ON `creditor_payment_item`.`creditor_invoice_id` = `creditor_invoice`.`creditor_invoice_id`
GROUP BY `creditor_invoice`.`creditor_id`;
